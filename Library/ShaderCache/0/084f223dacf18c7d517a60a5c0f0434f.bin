�A                         LIGHTMAP_ON    LIGHTPROBE_SH   �  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    half4 unity_SHAr;
    half4 unity_SHAg;
    half4 unity_SHAb;
    half4 unity_SHBr;
    half4 unity_SHBg;
    half4 unity_SHBb;
    half4 unity_SHC;
    half4 unity_Lightmap_HDR;
    half _OcclusionStrength;
    half4 _EmissionColor;
    half4 _ColorR;
    half4 _ColorG;
    half4 _ColorB;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
    half4 SV_Target1 [[ color(xlt_remap_o[1]) ]];
    half4 SV_Target2 [[ color(xlt_remap_o[2]) ]];
    half4 SV_Target3 [[ color(xlt_remap_o[3]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler samplerunity_Lightmap [[ sampler (0) ]],
    sampler sampler_MainTex [[ sampler (1) ]],
    sampler sampler_Mask [[ sampler (2) ]],
    sampler sampler_Normal [[ sampler (3) ]],
    sampler sampler_Spec [[ sampler (4) ]],
    sampler sampler_Emission [[ sampler (5) ]],
    sampler sampler_OcclusionMap [[ sampler (6) ]],
    texture2d<float, access::sample > _Mask [[ texture(0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture(1) ]] ,
    texture2d<half, access::sample > _OcclusionMap [[ texture(2) ]] ,
    texture2d<half, access::sample > _Normal [[ texture(3) ]] ,
    texture2d<half, access::sample > _Spec [[ texture(4) ]] ,
    texture2d<half, access::sample > _Emission [[ texture(5) ]] ,
    texture2d<half, access::sample > unity_Lightmap [[ texture(6) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    half4 u_xlat16_0;
    half3 u_xlat10_0;
    float4 u_xlat1;
    half4 u_xlat16_1;
    half3 u_xlat16_2;
    float3 u_xlat3;
    half4 u_xlat16_3;
    half3 u_xlat16_4;
    half3 u_xlat16_5;
    half3 u_xlat16_6;
    float u_xlat24;
    u_xlat10_0.xyz = half3(_Mask.sample(sampler_Mask, input.TEXCOORD0.xy).xyz);
    u_xlat16_1.xyz = half3(float3(u_xlat10_0.yyy) * float3(FGlobals._ColorG.xyz));
    u_xlat16_1.xyz = half3(fma(float3(FGlobals._ColorR.xyz), float3(u_xlat10_0.xxx), float3(u_xlat16_1.xyz)));
    u_xlat16_1.xyz = half3(fma(float3(FGlobals._ColorB.xyz), float3(u_xlat10_0.zzz), float3(u_xlat16_1.xyz)));
    u_xlat16_1.xyz = clamp(u_xlat16_1.xyz, 0.0h, 1.0h);
    u_xlat16_0.xyz = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy).xyz;
    u_xlat16_0.xyz = u_xlat16_1.xyz * u_xlat16_0.xyz;
    u_xlat16_1 = _Spec.sample(sampler_Spec, input.TEXCOORD0.xy);
    u_xlat16_2.x = max(u_xlat16_1.y, u_xlat16_1.x);
    u_xlat16_2.x = max(u_xlat16_1.z, u_xlat16_2.x);
    output.SV_Target1 = u_xlat16_1;
    u_xlat16_2.x = (-u_xlat16_2.x) + half(1.0);
    u_xlat16_0.xyz = u_xlat16_0.xyz * u_xlat16_2.xxx;
    u_xlat16_3.x = _OcclusionMap.sample(sampler_OcclusionMap, input.TEXCOORD0.xy).y;
    u_xlat16_2.x = (-FGlobals._OcclusionStrength) + half(1.0);
    u_xlat16_0.w = fma(u_xlat16_3.x, FGlobals._OcclusionStrength, u_xlat16_2.x);
    output.SV_Target0 = u_xlat16_0;
    u_xlat16_3.xyz = _Normal.sample(sampler_Normal, input.TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = fma(u_xlat16_3.xyz, half3(2.0, 2.0, 2.0), half3(-1.0, -1.0, -1.0));
    u_xlat3.x = dot(input.TEXCOORD1.xyz, float3(u_xlat16_2.xyz));
    u_xlat3.y = dot(input.TEXCOORD2.xyz, float3(u_xlat16_2.xyz));
    u_xlat3.z = dot(input.TEXCOORD3.xyz, float3(u_xlat16_2.xyz));
    u_xlat24 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat24 = rsqrt(u_xlat24);
    u_xlat1.xyz = float3(u_xlat24) * u_xlat3.xyz;
    u_xlat3.xyz = fma(u_xlat1.xyz, float3(0.5, 0.5, 0.5), float3(0.5, 0.5, 0.5));
    output.SV_Target2.xyz = half3(u_xlat3.xyz);
    output.SV_Target2.w = half(1.0);
    u_xlat16_2.x = half(u_xlat1.y * u_xlat1.y);
    u_xlat16_2.x = half(fma(u_xlat1.x, u_xlat1.x, (-float(u_xlat16_2.x))));
    u_xlat16_3 = half4(u_xlat1.yzzx * u_xlat1.xyzz);
    u_xlat16_4.x = dot(FGlobals.unity_SHBr, u_xlat16_3);
    u_xlat16_4.y = dot(FGlobals.unity_SHBg, u_xlat16_3);
    u_xlat16_4.z = dot(FGlobals.unity_SHBb, u_xlat16_3);
    u_xlat16_2.xyz = fma(FGlobals.unity_SHC.xyz, u_xlat16_2.xxx, u_xlat16_4.xyz);
    u_xlat1.w = 1.0;
    u_xlat16_4.x = half(dot(float4(FGlobals.unity_SHAr), u_xlat1));
    u_xlat16_4.y = half(dot(float4(FGlobals.unity_SHAg), u_xlat1));
    u_xlat16_4.z = half(dot(float4(FGlobals.unity_SHAb), u_xlat1));
    u_xlat16_2.xyz = u_xlat16_2.xyz + u_xlat16_4.xyz;
    u_xlat16_2.xyz = max(u_xlat16_2.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_5.xyz = log2(u_xlat16_2.xyz);
    u_xlat16_5.xyz = u_xlat16_5.xyz * half3(0.416666657, 0.416666657, 0.416666657);
    u_xlat16_5.xyz = exp2(u_xlat16_5.xyz);
    u_xlat16_5.xyz = fma(u_xlat16_5.xyz, half3(1.05499995, 1.05499995, 1.05499995), half3(-0.0549999997, -0.0549999997, -0.0549999997));
    u_xlat16_5.xyz = max(u_xlat16_5.xyz, half3(0.0, 0.0, 0.0));
    u_xlat16_6.xyz = unity_Lightmap.sample(samplerunity_Lightmap, input.TEXCOORD5.xy).xyz;
    u_xlat16_2.xyz = fma(FGlobals.unity_Lightmap_HDR.xxx, u_xlat16_6.xyz, u_xlat16_5.xyz);
    u_xlat16_2.xyz = u_xlat16_0.www * u_xlat16_2.xyz;
    u_xlat16_2.xyz = u_xlat16_0.xyz * u_xlat16_2.xyz;
    u_xlat16_5.xyz = _Emission.sample(sampler_Emission, input.TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = fma(FGlobals._EmissionColor.xyz, u_xlat16_5.xyz, u_xlat16_2.xyz);
    output.SV_Target3.xyz = exp2((-u_xlat16_2.xyz));
    output.SV_Target3.w = half(1.0);
    return output;
}
                              FGlobalsh      
   unity_SHAr                       
   unity_SHAg                      
   unity_SHAb                      
   unity_SHBr                      
   unity_SHBg                       
   unity_SHBb                   (   	   unity_SHC                    0      unity_Lightmap_HDR                   8      _OcclusionStrength                   @      _EmissionColor                   H      _ColorR                  P      _ColorG                  X      _ColorB                  `             _Mask                    _MainTex                _OcclusionMap                   _Normal                 _Spec                	   _Emission                   unity_Lightmap                   FGlobals           