�A                         POINT   �  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float3 _WorldSpaceCameraPos;
    float4 _WorldSpaceLightPos0;
    half4 _LightColor0;
    float4 hlslcc_mtx4x4unity_WorldToLight[4];
    half4 _ColorR;
    half4 _ColorG;
    half4 _ColorB;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float3 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float3 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float3 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float3 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
};

struct Mtl_FragmentOut
{
    half4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_LightTexture0 [[ sampler (0) ]],
    sampler sampler_MainTex [[ sampler (1) ]],
    sampler sampler_Mask [[ sampler (2) ]],
    sampler sampler_Normal [[ sampler (3) ]],
    sampler sampler_Spec [[ sampler (4) ]],
    texture2d<float, access::sample > _Mask [[ texture(0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture(1) ]] ,
    texture2d<half, access::sample > _Normal [[ texture(2) ]] ,
    texture2d<half, access::sample > _Spec [[ texture(3) ]] ,
    texture2d<float, access::sample > _LightTexture0 [[ texture(4) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    half3 u_xlat16_0;
    half4 u_xlat16_1;
    float3 u_xlat2;
    half3 u_xlat16_2;
    half3 u_xlat10_2;
    float3 u_xlat3;
    half3 u_xlat16_4;
    float3 u_xlat5;
    float u_xlat10;
    float u_xlat15;
    half u_xlat16_15;
    float u_xlat17;
    u_xlat16_0.xyz = _Normal.sample(sampler_Normal, input.TEXCOORD0.xy).xyz;
    u_xlat16_1.xyz = fma(u_xlat16_0.xyz, half3(2.0, 2.0, 2.0), half3(-1.0, -1.0, -1.0));
    u_xlat0.x = dot(input.TEXCOORD1.xyz, float3(u_xlat16_1.xyz));
    u_xlat0.y = dot(input.TEXCOORD2.xyz, float3(u_xlat16_1.xyz));
    u_xlat0.z = dot(input.TEXCOORD3.xyz, float3(u_xlat16_1.xyz));
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat0.xyz = float3(u_xlat15) * u_xlat0.xyz;
    u_xlat2.xyz = (-input.TEXCOORD4.xyz) + FGlobals._WorldSpaceCameraPos.xyzx.xyz;
    u_xlat15 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat3.xyz = (-input.TEXCOORD4.xyz) + FGlobals._WorldSpaceLightPos0.xyz;
    u_xlat17 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat17 = rsqrt(u_xlat17);
    u_xlat3.xyz = float3(u_xlat17) * u_xlat3.xyz;
    u_xlat2.xyz = fma(u_xlat2.xyz, float3(u_xlat15), u_xlat3.xyz);
    u_xlat15 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat15 = max(u_xlat15, 0.00100000005);
    u_xlat15 = rsqrt(u_xlat15);
    u_xlat2.xyz = float3(u_xlat15) * u_xlat2.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat15 = clamp(u_xlat15, 0.0f, 1.0f);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat3.xyz);
    u_xlat0.x = clamp(u_xlat0.x, 0.0f, 1.0f);
    u_xlat5.x = dot(u_xlat3.xyz, u_xlat2.xyz);
    u_xlat5.x = clamp(u_xlat5.x, 0.0f, 1.0f);
    u_xlat5.x = max(u_xlat5.x, 0.319999993);
    u_xlat10 = u_xlat15 * u_xlat15;
    u_xlat16_1 = _Spec.sample(sampler_Spec, input.TEXCOORD0.xy);
    u_xlat16_15 = (-u_xlat16_1.w) + half(1.0);
    u_xlat16_2.x = u_xlat16_15 * u_xlat16_15;
    u_xlat16_15 = fma(u_xlat16_15, u_xlat16_15, half(1.5));
    u_xlat5.x = float(u_xlat16_15) * u_xlat5.x;
    u_xlat16_15 = fma(u_xlat16_2.x, u_xlat16_2.x, half(-1.0));
    u_xlat10 = fma(u_xlat10, float(u_xlat16_15), 1.00001001);
    u_xlat5.x = u_xlat10 * u_xlat5.x;
    u_xlat5.x = float(u_xlat16_2.x) / u_xlat5.x;
    u_xlat5.x = u_xlat5.x + -9.99999975e-05;
    u_xlat5.x = max(u_xlat5.x, 0.0);
    u_xlat5.x = min(u_xlat5.x, 100.0);
    u_xlat5.xyz = float3(u_xlat16_1.xyz) * u_xlat5.xxx;
    u_xlat10_2.xyz = half3(_Mask.sample(sampler_Mask, input.TEXCOORD0.xy).xyz);
    u_xlat16_4.xyz = half3(float3(u_xlat10_2.yyy) * float3(FGlobals._ColorG.xyz));
    u_xlat16_4.xyz = half3(fma(float3(FGlobals._ColorR.xyz), float3(u_xlat10_2.xxx), float3(u_xlat16_4.xyz)));
    u_xlat16_4.xyz = half3(fma(float3(FGlobals._ColorB.xyz), float3(u_xlat10_2.zzz), float3(u_xlat16_4.xyz)));
    u_xlat16_4.xyz = clamp(u_xlat16_4.xyz, 0.0h, 1.0h);
    u_xlat16_2.xyz = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy).xyz;
    u_xlat16_2.xyz = u_xlat16_4.xyz * u_xlat16_2.xyz;
    u_xlat16_4.x = max(u_xlat16_1.y, u_xlat16_1.x);
    u_xlat16_4.x = max(u_xlat16_1.z, u_xlat16_4.x);
    u_xlat16_4.x = (-u_xlat16_4.x) + half(1.0);
    u_xlat5.xyz = fma(float3(u_xlat16_2.xyz), float3(u_xlat16_4.xxx), u_xlat5.xyz);
    u_xlat2.xyz = input.TEXCOORD4.yyy * FGlobals.hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[0].xyz, input.TEXCOORD4.xxx, u_xlat2.xyz);
    u_xlat2.xyz = fma(FGlobals.hlslcc_mtx4x4unity_WorldToLight[2].xyz, input.TEXCOORD4.zzz, u_xlat2.xyz);
    u_xlat2.xyz = u_xlat2.xyz + FGlobals.hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlat2.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat2.x = _LightTexture0.sample(sampler_LightTexture0, u_xlat2.xx).x;
    u_xlat16_4.xyz = half3(u_xlat2.xxx * float3(FGlobals._LightColor0.xyz));
    u_xlat5.xyz = u_xlat5.xyz * float3(u_xlat16_4.xyz);
    u_xlat0.xyz = u_xlat0.xxx * u_xlat5.xyz;
    output.SV_Target0.xyz = half3(u_xlat0.xyz);
    output.SV_Target0.w = half(1.0);
    return output;
}
                                FGlobals�         _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        _LightColor0                        _ColorR                  p      _ColorG                  x      _ColorB                  �      unity_WorldToLight                   0             _Mask                    _MainTex                _Normal                 _Spec                   _LightTexture0                   FGlobals           