�A                       �#  ���$      0                       xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _MainTex_TexelSize;
    float4 _CameraDepthTexture_TexelSize;
    float2 _Jitter;
    float4 _SharpenParameters;
    float4 _FinalBlendParameters;
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
    float4 SV_Target1 [[ color(xlt_remap_o[1]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_MainTex [[ sampler (0) ]],
    sampler sampler_HistoryTex [[ sampler (1) ]],
    sampler sampler_CameraMotionVectorsTexture [[ sampler (2) ]],
    sampler sampler_CameraDepthTexture [[ sampler (3) ]],
    texture2d<half, access::sample > _CameraMotionVectorsTexture [[ texture(0) ]] ,
    texture2d<half, access::sample > _MainTex [[ texture(1) ]] ,
    texture2d<half, access::sample > _HistoryTex [[ texture(2) ]] ,
    texture2d<float, access::sample > _CameraDepthTexture [[ texture(3) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    half4 u_xlat16_0;
    bool u_xlatb0;
    float4 u_xlat1;
    float4 u_xlat2;
    float4 u_xlat3;
    half3 u_xlat16_3;
    float4 u_xlat4;
    half3 u_xlat16_4;
    float4 u_xlat5;
    half3 u_xlat16_5;
    half3 u_xlat16_6;
    half u_xlat16_7;
    float u_xlat8;
    float3 u_xlat10;
    half3 u_xlat16_10;
    bool u_xlatb10;
    half u_xlat16_11;
    half u_xlat16_15;
    float2 u_xlat16;
    bool u_xlatb16;
    float2 u_xlat18;
    half u_xlat16_19;
    float u_xlat24;
    bool u_xlatb24;
    half u_xlat16_26;
    float u_xlat27;
    bool u_xlatb27;
    u_xlat0.x = float(1.0);
    u_xlat0.y = float(-1.0);
    u_xlat1.x = float(0.0);
    u_xlat1.y = float(0.0);
    u_xlat1.z = _CameraDepthTexture.sample(sampler_CameraDepthTexture, input.TEXCOORD0.zw).x;
    u_xlat2.x = float(-1.0);
    u_xlat2.y = float(-1.0);
    u_xlat3 = _CameraDepthTexture.gather(sampler_CameraDepthTexture, input.TEXCOORD0.zw, int2(1, 1));
    u_xlat2.z = u_xlat3.x;
    u_xlat2.xyz = (-u_xlat1.yyz) + u_xlat2.xyz;
    u_xlatb24 = u_xlat1.z>=u_xlat3.x;
    u_xlat24 = u_xlatb24 ? 1.0 : float(0.0);
    u_xlat1.xyz = fma(float3(u_xlat24), u_xlat2.xyz, u_xlat1.xyz);
    u_xlat0.z = u_xlat3.y;
    u_xlat0.xyz = u_xlat0.xyz + (-u_xlat1.yyz);
    u_xlatb24 = u_xlat1.z>=u_xlat3.y;
    u_xlat24 = u_xlatb24 ? 1.0 : float(0.0);
    u_xlat0.xyz = fma(float3(u_xlat24), u_xlat0.xyz, u_xlat1.xyz);
    u_xlat3.x = float(-1.0);
    u_xlat3.y = float(1.0);
    u_xlat1.xyz = (-u_xlat0.xyz) + u_xlat3.xyz;
    u_xlatb24 = u_xlat0.z>=u_xlat3.z;
    u_xlat24 = u_xlatb24 ? 1.0 : float(0.0);
    u_xlat0.xyz = fma(float3(u_xlat24), u_xlat1.xyz, u_xlat0.xyz);
    u_xlatb16 = u_xlat0.z>=u_xlat3.w;
    u_xlat16.x = u_xlatb16 ? 1.0 : float(0.0);
    u_xlat1.xy = (-u_xlat0.xy) + float2(1.0, 1.0);
    u_xlat0.xy = fma(u_xlat16.xx, u_xlat1.xy, u_xlat0.xy);
    u_xlat0.xy = fma(u_xlat0.xy, FGlobals._CameraDepthTexture_TexelSize.xy, input.TEXCOORD0.zw);
    u_xlat16_0.xy = _CameraMotionVectorsTexture.sample(sampler_CameraMotionVectorsTexture, u_xlat0.xy).xy;
    u_xlat16.xy = (-float2(u_xlat16_0.xy)) + input.TEXCOORD0.zw;
    u_xlat0.xy = float2(u_xlat16_0.xy) * FGlobals._FinalBlendParameters.zz;
    u_xlat0.x = dot(u_xlat0.xy, u_xlat0.xy);
    u_xlat1.w = sqrt(u_xlat0.x);
    u_xlat16_0 = _HistoryTex.sample(sampler_HistoryTex, u_xlat16.xy);
    u_xlat2.x = fma((-FGlobals._MainTex_TexelSize.z), 0.0500000007, float(u_xlat16_0.w));
    u_xlat10.x = FGlobals._MainTex_TexelSize.z * 0.00499999896;
    u_xlat10.x = float(1.0) / u_xlat10.x;
    u_xlat2.x = u_xlat10.x * u_xlat2.x;
    u_xlat2.x = clamp(u_xlat2.x, 0.0f, 1.0f);
    u_xlat10.x = fma(u_xlat2.x, -2.0, 3.0);
    u_xlat2.x = u_xlat2.x * u_xlat2.x;
    u_xlat2.x = u_xlat2.x * u_xlat10.x;
    u_xlat2.x = min(u_xlat2.x, 1.0);
    u_xlat2.x = fma(u_xlat2.x, -5.78318548, 6.28318548);
    u_xlatb10 = FGlobals._MainTex_TexelSize.y<0.0;
    u_xlat18.xy = FGlobals._Jitter.xyxx.xy * float2(1.0, -1.0);
    u_xlat10.xy = (bool(u_xlatb10)) ? u_xlat18.xy : FGlobals._Jitter.xyxx.xy;
    u_xlat10.xy = (-u_xlat10.xy) + input.TEXCOORD0.xy;
    u_xlat16_3.xyz = _MainTex.sample(sampler_MainTex, u_xlat10.xy).xyz;
    u_xlat16_4.xyz = u_xlat16_3.xyz + u_xlat16_3.xyz;
    u_xlat5.xy = fma((-FGlobals._MainTex_TexelSize.xy), float2(0.5, 0.5), u_xlat10.xy);
    u_xlat10.xy = fma(FGlobals._MainTex_TexelSize.xy, float2(0.5, 0.5), u_xlat10.xy);
    u_xlat16_10.xyz = _MainTex.sample(sampler_MainTex, u_xlat10.xy).xyz;
    u_xlat16_5.xyz = _MainTex.sample(sampler_MainTex, u_xlat5.xy).xyz;
    u_xlat16_6.xyz = u_xlat16_10.xyz + u_xlat16_5.xyz;
    u_xlat16_4.xyz = fma(u_xlat16_6.xyz, half3(4.0, 4.0, 4.0), (-u_xlat16_4.xyz));
    u_xlat16_6.xyz = u_xlat16_6.xyz * half3(0.5, 0.5, 0.5);
    u_xlat16_4.xyz = fma((-u_xlat16_4.xyz), half3(0.166666999, 0.166666999, 0.166666999), u_xlat16_3.xyz);
    u_xlat4.xyz = float3(u_xlat16_4.xyz) * FGlobals._SharpenParameters.xxx;
    u_xlat3.xyz = fma(u_xlat4.xyz, float3(2.71828198, 2.71828198, 2.71828198), float3(u_xlat16_3.xyz));
    u_xlat3.xyz = max(u_xlat3.xyz, float3(0.0, 0.0, 0.0));
    u_xlat27 = max(u_xlat3.y, u_xlat3.x);
    u_xlat27 = max(u_xlat3.z, u_xlat27);
    u_xlat27 = u_xlat27 + 1.0;
    u_xlat27 = float(1.0) / u_xlat27;
    u_xlat1.xyz = float3(u_xlat27) * u_xlat3.xyz;
    u_xlat16_7 = dot(u_xlat1.xyz, float3(0.219999999, 0.707000017, 0.0710000023));
    u_xlat16_3.x = max(u_xlat16_6.y, u_xlat16_6.x);
    u_xlat16_3.x = max(u_xlat16_6.z, u_xlat16_3.x);
    u_xlat16_3.x = u_xlat16_3.x + half(1.0);
    u_xlat16_3.x = half(1.0) / u_xlat16_3.x;
    u_xlat16_3.xyz = u_xlat16_3.xxx * u_xlat16_6.xyz;
    u_xlat16_15 = dot(u_xlat16_3.xyz, half3(0.219999999, 0.707000017, 0.0710000023));
    u_xlat16_3.x = (-u_xlat16_7) + u_xlat16_15;
    u_xlat16_11 = max(u_xlat16_5.y, u_xlat16_5.x);
    u_xlat16_11 = max(u_xlat16_5.z, u_xlat16_11);
    u_xlat16_11 = u_xlat16_11 + half(1.0);
    u_xlat16_11 = half(1.0) / u_xlat16_11;
    u_xlat16_4.xyz = half3(u_xlat16_11) * u_xlat16_5.xyz;
    u_xlat16_7 = dot(u_xlat16_4.xyz, half3(0.219999999, 0.707000017, 0.0710000023));
    u_xlat16_19 = max(u_xlat16_10.y, u_xlat16_10.x);
    u_xlat16_19 = max(u_xlat16_10.z, u_xlat16_19);
    u_xlat16_19 = u_xlat16_19 + half(1.0);
    u_xlat16_19 = half(1.0) / u_xlat16_19;
    u_xlat16_6.xyz = u_xlat16_10.xyz * half3(u_xlat16_19);
    u_xlat16_10.xyz = fma(u_xlat16_10.xyz, half3(u_xlat16_19), (-u_xlat16_4.xyz));
    u_xlat16_15 = dot(u_xlat16_6.xyz, half3(0.219999999, 0.707000017, 0.0710000023));
    u_xlat16_19 = (-u_xlat16_15) + u_xlat16_7;
    u_xlatb27 = u_xlat16_15>=u_xlat16_7;
    u_xlat27 = u_xlatb27 ? 1.0 : float(0.0);
    u_xlat16_3.x = max(abs(u_xlat16_19), abs(u_xlat16_3.x));
    u_xlat16_5.xyz = fma(u_xlat16_5.xyz, half3(u_xlat16_11), (-u_xlat16_6.xyz));
    u_xlat5.xyz = fma(float3(u_xlat27), float3(u_xlat16_5.xyz), float3(u_xlat16_6.xyz));
    u_xlat10.xyz = fma(float3(u_xlat27), float3(u_xlat16_10.xyz), float3(u_xlat16_4.xyz));
    u_xlat10.xyz = fma(u_xlat2.xxx, float3(u_xlat16_3.xxx), u_xlat10.xyz);
    u_xlat3.xyz = fma((-u_xlat2.xxx), float3(u_xlat16_3.xxx), u_xlat5.xyz);
    u_xlat4.xyz = u_xlat10.xyz + u_xlat3.xyz;
    u_xlat2.xyz = u_xlat10.xyz + (-u_xlat3.xyz);
    u_xlat2.xyz = u_xlat2.xyz * float3(0.5, 0.5, 0.5);
    u_xlat3.xyz = u_xlat4.xyz * float3(0.5, 0.5, 0.5);
    u_xlat16_26 = max(u_xlat16_0.y, u_xlat16_0.x);
    u_xlat16_26 = max(u_xlat16_0.z, u_xlat16_26);
    u_xlat16_26 = u_xlat16_26 + half(1.0);
    u_xlat16_26 = half(1.0) / u_xlat16_26;
    u_xlat4.xyz = fma(float3(u_xlat16_0.xyz), float3(u_xlat16_26), (-u_xlat3.xyz));
    u_xlat5.xyz = float3(u_xlat16_0.xyz) * float3(u_xlat16_26);
    u_xlat3.w = float(u_xlat16_0.w);
    u_xlat0.xyz = u_xlat4.xyz / u_xlat2.xyz;
    u_xlat8 = max(abs(u_xlat0.z), abs(u_xlat0.y));
    u_xlat0.x = max(u_xlat8, abs(u_xlat0.x));
    u_xlat4.w = 0.0;
    u_xlat2 = u_xlat4 / u_xlat0.xxxx;
    u_xlatb0 = 1.0<u_xlat0.x;
    u_xlat2 = u_xlat2 + u_xlat3;
    u_xlat5.w = u_xlat3.w;
    u_xlat0 = (bool(u_xlatb0)) ? u_xlat2 : u_xlat5;
    u_xlat2 = (-u_xlat1) + u_xlat0;
    u_xlat0.x = (-FGlobals._FinalBlendParameters.x) + FGlobals._FinalBlendParameters.y;
    u_xlat0.x = fma(u_xlat0.w, u_xlat0.x, FGlobals._FinalBlendParameters.x);
    u_xlat0.x = max(u_xlat0.x, FGlobals._FinalBlendParameters.y);
    u_xlat0.x = min(u_xlat0.x, FGlobals._FinalBlendParameters.x);
    u_xlat0 = fma(u_xlat0.xxxx, u_xlat2, u_xlat1);
    u_xlat1.x = max(u_xlat0.y, u_xlat0.x);
    u_xlat1.x = max(u_xlat0.z, u_xlat1.x);
    u_xlat1.x = (-u_xlat1.x) + 1.0;
    u_xlat1.x = float(1.0) / u_xlat1.x;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xxx;
    output.SV_Target1.xyz = u_xlat0.xyz;
    output.SV_Target0 = u_xlat0;
    output.SV_Target1.w = u_xlat0.w * 0.949999988;
    return output;
}
                               FGlobalsP         _MainTex_TexelSize                           _CameraDepthTexture_TexelSize                           _Jitter                          _SharpenParameters                    0      _FinalBlendParameters                     @             _CameraMotionVectorsTexture                  _MainTex                 _HistoryTex                 _CameraDepthTexture                 FGlobals           