﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>
struct PopsicleList_1_t2780837186;
// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct PopsicleList_1_t1233628674;
// Gvr.Internal.ControllerState
struct ControllerState_t3401244786;
// Gvr.Internal.IControllerProvider
struct IControllerProvider_t2738644831;
// Gvr.Internal.IHeadsetProvider
struct IHeadsetProvider_t3991216071;
// GvrAllEventsTrigger/TriggerEvent
struct TriggerEvent_t1225966107;
// GvrBaseArmModel
struct GvrBaseArmModel_t3382200842;
// GvrBasePointer
struct GvrBasePointer_t822782720;
// GvrControllerHand[]
struct GvrControllerHandU5BU5D_t2583795907;
// GvrControllerInput/OnStateChangedEvent
struct OnStateChangedEvent_t3148566047;
// GvrControllerInputDevice
struct GvrControllerInputDevice_t3709172113;
// GvrControllerInputDevice[]
struct GvrControllerInputDeviceU5BU5D_t1273413900;
// GvrControllerReticleVisual
struct GvrControllerReticleVisual_t1615767572;
// GvrEventExecutor
struct GvrEventExecutor_t2551165091;
// GvrHeadset/OnRecenterEvent
struct OnRecenterEvent_t1755824842;
// GvrHeadset/OnSafetyRegionEvent
struct OnSafetyRegionEvent_t980662704;
// GvrLaserVisual
struct GvrLaserVisual_t3710127448;
// GvrLaserVisual/GetPointForDistanceDelegate
struct GetPointForDistanceDelegate_t92913150;
// GvrPointerInputModuleImpl
struct GvrPointerInputModuleImpl_t2260544298;
// GvrPointerPhysicsRaycaster/HitComparer
struct HitComparer_t1984117280;
// GvrPointerScrollInput
struct GvrPointerScrollInput_t3738414627;
// IGvrEventExecutor
struct IGvrEventExecutor_t2029948192;
// IGvrInputModuleController
struct IGvrInputModuleController_t3115474459;
// IGvrScrollSettings
struct IGvrScrollSettings_t3099753915;
// System.Action
struct Action_t1264377477;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Type,GvrEventExecutor/EventDelegate>
struct Dictionary_2_t2925553320;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,GvrPointerScrollInput/ScrollInfo>
struct Dictionary_2_t3565263950;
// System.Collections.Generic.List`1<Gvr.Internal.EmulatorTouchEvent/Pointer>
struct List_1_t697575126;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct List_1_t3132410353;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t1435266790;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Void
struct Void_t1185182177;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t4028559421;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler>
struct EventFunction_1_t2876286272;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.LineRenderer
struct LineRenderer_t3154350270;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// UnityEngine.MeshFilter
struct MeshFilter_t3523625662;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1314943911;
// proto.PhoneEvent
struct PhoneEvent_t1358418708;
// proto.PhoneEvent/Types/AccelerometerEvent
struct AccelerometerEvent_t1394922645;
// proto.PhoneEvent/Types/DepthMapEvent
struct DepthMapEvent_t729886054;
// proto.PhoneEvent/Types/GyroscopeEvent
struct GyroscopeEvent_t127774954;
// proto.PhoneEvent/Types/KeyEvent
struct KeyEvent_t1937114521;
// proto.PhoneEvent/Types/MotionEvent
struct MotionEvent_t3121383016;
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer
struct Pointer_t4145025558;
// proto.PhoneEvent/Types/OrientationEvent
struct OrientationEvent_t158247624;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTRACTBUILDERLITE_2_T1175377521_H
#define ABSTRACTBUILDERLITE_2_T1175377521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent,proto.PhoneEvent/Builder>
struct  AbstractBuilderLite_2_t1175377521  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T1175377521_H
#ifndef ABSTRACTBUILDERLITE_2_T2114494517_H
#define ABSTRACTBUILDERLITE_2_T2114494517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/AccelerometerEvent,proto.PhoneEvent/Types/AccelerometerEvent/Builder>
struct  AbstractBuilderLite_2_t2114494517  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T2114494517_H
#ifndef ABSTRACTBUILDERLITE_2_T1753171341_H
#define ABSTRACTBUILDERLITE_2_T1753171341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct  AbstractBuilderLite_2_t1753171341  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T1753171341_H
#ifndef ABSTRACTBUILDERLITE_2_T522313195_H
#define ABSTRACTBUILDERLITE_2_T522313195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct  AbstractBuilderLite_2_t522313195  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T522313195_H
#ifndef ABSTRACTBUILDERLITE_2_T4063219799_H
#define ABSTRACTBUILDERLITE_2_T4063219799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  AbstractBuilderLite_2_t4063219799  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T4063219799_H
#ifndef ABSTRACTBUILDERLITE_2_T258149081_H
#define ABSTRACTBUILDERLITE_2_T258149081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct  AbstractBuilderLite_2_t258149081  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T258149081_H
#ifndef ABSTRACTBUILDERLITE_2_T1643049470_H
#define ABSTRACTBUILDERLITE_2_T1643049470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct  AbstractBuilderLite_2_t1643049470  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T1643049470_H
#ifndef ABSTRACTBUILDERLITE_2_T4034615238_H
#define ABSTRACTBUILDERLITE_2_T4034615238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  AbstractBuilderLite_2_t4034615238  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T4034615238_H
#ifndef ABSTRACTMESSAGELITE_2_T3099436496_H
#define ABSTRACTMESSAGELITE_2_T3099436496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent,proto.PhoneEvent/Builder>
struct  AbstractMessageLite_2_t3099436496  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T3099436496_H
#ifndef ABSTRACTMESSAGELITE_2_T4038553492_H
#define ABSTRACTMESSAGELITE_2_T4038553492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/AccelerometerEvent,proto.PhoneEvent/Types/AccelerometerEvent/Builder>
struct  AbstractMessageLite_2_t4038553492  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T4038553492_H
#ifndef ABSTRACTMESSAGELITE_2_T3677230316_H
#define ABSTRACTMESSAGELITE_2_T3677230316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct  AbstractMessageLite_2_t3677230316  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T3677230316_H
#ifndef ABSTRACTMESSAGELITE_2_T2446372170_H
#define ABSTRACTMESSAGELITE_2_T2446372170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct  AbstractMessageLite_2_t2446372170  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T2446372170_H
#ifndef ABSTRACTMESSAGELITE_2_T1692311478_H
#define ABSTRACTMESSAGELITE_2_T1692311478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  AbstractMessageLite_2_t1692311478  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T1692311478_H
#ifndef ABSTRACTMESSAGELITE_2_T2182208056_H
#define ABSTRACTMESSAGELITE_2_T2182208056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct  AbstractMessageLite_2_t2182208056  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T2182208056_H
#ifndef ABSTRACTMESSAGELITE_2_T3567108445_H
#define ABSTRACTMESSAGELITE_2_T3567108445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct  AbstractMessageLite_2_t3567108445  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T3567108445_H
#ifndef ABSTRACTMESSAGELITE_2_T1663706917_H
#define ABSTRACTMESSAGELITE_2_T1663706917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  AbstractMessageLite_2_t1663706917  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T1663706917_H
#ifndef CONTROLLERPROVIDERFACTORY_T1243328180_H
#define CONTROLLERPROVIDERFACTORY_T1243328180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.ControllerProviderFactory
struct  ControllerProviderFactory_t1243328180  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERPROVIDERFACTORY_T1243328180_H
#ifndef CONTROLLERUTILS_T1159150490_H
#define CONTROLLERUTILS_T1159150490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.ControllerUtils
struct  ControllerUtils_t1159150490  : public RuntimeObject
{
public:

public:
};

struct ControllerUtils_t1159150490_StaticFields
{
public:
	// GvrControllerHand[] Gvr.Internal.ControllerUtils::AllHands
	GvrControllerHandU5BU5D_t2583795907* ___AllHands_0;

public:
	inline static int32_t get_offset_of_AllHands_0() { return static_cast<int32_t>(offsetof(ControllerUtils_t1159150490_StaticFields, ___AllHands_0)); }
	inline GvrControllerHandU5BU5D_t2583795907* get_AllHands_0() const { return ___AllHands_0; }
	inline GvrControllerHandU5BU5D_t2583795907** get_address_of_AllHands_0() { return &___AllHands_0; }
	inline void set_AllHands_0(GvrControllerHandU5BU5D_t2583795907* value)
	{
		___AllHands_0 = value;
		Il2CppCodeGenWriteBarrier((&___AllHands_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERUTILS_T1159150490_H
#ifndef DUMMYCONTROLLERPROVIDER_T2905932083_H
#define DUMMYCONTROLLERPROVIDER_T2905932083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.DummyControllerProvider
struct  DummyControllerProvider_t2905932083  : public RuntimeObject
{
public:
	// Gvr.Internal.ControllerState Gvr.Internal.DummyControllerProvider::dummyState
	ControllerState_t3401244786 * ___dummyState_0;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(DummyControllerProvider_t2905932083, ___dummyState_0)); }
	inline ControllerState_t3401244786 * get_dummyState_0() const { return ___dummyState_0; }
	inline ControllerState_t3401244786 ** get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(ControllerState_t3401244786 * value)
	{
		___dummyState_0 = value;
		Il2CppCodeGenWriteBarrier((&___dummyState_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYCONTROLLERPROVIDER_T2905932083_H
#ifndef GVRCARDBOARDHELPERS_T3063197799_H
#define GVRCARDBOARDHELPERS_T3063197799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrCardboardHelpers
struct  GvrCardboardHelpers_t3063197799  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCARDBOARDHELPERS_T3063197799_H
#ifndef GVREVENTEXECUTOR_T2551165091_H
#define GVREVENTEXECUTOR_T2551165091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventExecutor
struct  GvrEventExecutor_t2551165091  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,GvrEventExecutor/EventDelegate> GvrEventExecutor::eventTable
	Dictionary_2_t2925553320 * ___eventTable_0;

public:
	inline static int32_t get_offset_of_eventTable_0() { return static_cast<int32_t>(offsetof(GvrEventExecutor_t2551165091, ___eventTable_0)); }
	inline Dictionary_2_t2925553320 * get_eventTable_0() const { return ___eventTable_0; }
	inline Dictionary_2_t2925553320 ** get_address_of_eventTable_0() { return &___eventTable_0; }
	inline void set_eventTable_0(Dictionary_2_t2925553320 * value)
	{
		___eventTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREVENTEXECUTOR_T2551165091_H
#ifndef GVREXECUTEEVENTSEXTENSION_T3940832397_H
#define GVREXECUTEEVENTSEXTENSION_T3940832397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrExecuteEventsExtension
struct  GvrExecuteEventsExtension_t3940832397  : public RuntimeObject
{
public:

public:
};

struct GvrExecuteEventsExtension_t3940832397_StaticFields
{
public:
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler> GvrExecuteEventsExtension::s_HoverHandler
	EventFunction_1_t2876286272 * ___s_HoverHandler_0;
	// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<IGvrPointerHoverHandler> GvrExecuteEventsExtension::<>f__mg$cache0
	EventFunction_1_t2876286272 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_HoverHandler_0() { return static_cast<int32_t>(offsetof(GvrExecuteEventsExtension_t3940832397_StaticFields, ___s_HoverHandler_0)); }
	inline EventFunction_1_t2876286272 * get_s_HoverHandler_0() const { return ___s_HoverHandler_0; }
	inline EventFunction_1_t2876286272 ** get_address_of_s_HoverHandler_0() { return &___s_HoverHandler_0; }
	inline void set_s_HoverHandler_0(EventFunction_1_t2876286272 * value)
	{
		___s_HoverHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_HoverHandler_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(GvrExecuteEventsExtension_t3940832397_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline EventFunction_1_t2876286272 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline EventFunction_1_t2876286272 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(EventFunction_1_t2876286272 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREXECUTEEVENTSEXTENSION_T3940832397_H
#ifndef HITCOMPARER_T1984117280_H
#define HITCOMPARER_T1984117280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerPhysicsRaycaster/HitComparer
struct  HitComparer_t1984117280  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITCOMPARER_T1984117280_H
#ifndef GVRPOINTERSCROLLINPUT_T3738414627_H
#define GVRPOINTERSCROLLINPUT_T3738414627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerScrollInput
struct  GvrPointerScrollInput_t3738414627  : public RuntimeObject
{
public:
	// System.Boolean GvrPointerScrollInput::inertia
	bool ___inertia_2;
	// System.Single GvrPointerScrollInput::decelerationRate
	float ___decelerationRate_3;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,GvrPointerScrollInput/ScrollInfo> GvrPointerScrollInput::scrollHandlers
	Dictionary_2_t3565263950 * ___scrollHandlers_15;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> GvrPointerScrollInput::scrollingObjects
	List_1_t2585711361 * ___scrollingObjects_16;

public:
	inline static int32_t get_offset_of_inertia_2() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_t3738414627, ___inertia_2)); }
	inline bool get_inertia_2() const { return ___inertia_2; }
	inline bool* get_address_of_inertia_2() { return &___inertia_2; }
	inline void set_inertia_2(bool value)
	{
		___inertia_2 = value;
	}

	inline static int32_t get_offset_of_decelerationRate_3() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_t3738414627, ___decelerationRate_3)); }
	inline float get_decelerationRate_3() const { return ___decelerationRate_3; }
	inline float* get_address_of_decelerationRate_3() { return &___decelerationRate_3; }
	inline void set_decelerationRate_3(float value)
	{
		___decelerationRate_3 = value;
	}

	inline static int32_t get_offset_of_scrollHandlers_15() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_t3738414627, ___scrollHandlers_15)); }
	inline Dictionary_2_t3565263950 * get_scrollHandlers_15() const { return ___scrollHandlers_15; }
	inline Dictionary_2_t3565263950 ** get_address_of_scrollHandlers_15() { return &___scrollHandlers_15; }
	inline void set_scrollHandlers_15(Dictionary_2_t3565263950 * value)
	{
		___scrollHandlers_15 = value;
		Il2CppCodeGenWriteBarrier((&___scrollHandlers_15), value);
	}

	inline static int32_t get_offset_of_scrollingObjects_16() { return static_cast<int32_t>(offsetof(GvrPointerScrollInput_t3738414627, ___scrollingObjects_16)); }
	inline List_1_t2585711361 * get_scrollingObjects_16() const { return ___scrollingObjects_16; }
	inline List_1_t2585711361 ** get_address_of_scrollingObjects_16() { return &___scrollingObjects_16; }
	inline void set_scrollingObjects_16(List_1_t2585711361 * value)
	{
		___scrollingObjects_16 = value;
		Il2CppCodeGenWriteBarrier((&___scrollingObjects_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERSCROLLINPUT_T3738414627_H
#ifndef GVRSETTINGS_T2768269135_H
#define GVRSETTINGS_T2768269135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings
struct  GvrSettings_t2768269135  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSETTINGS_T2768269135_H
#ifndef GVRUNITYSDKVERSION_T2768813923_H
#define GVRUNITYSDKVERSION_T2768813923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrUnitySdkVersion
struct  GvrUnitySdkVersion_t2768813923  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRUNITYSDKVERSION_T2768813923_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef TYPES_T1964849426_H
#define TYPES_T1964849426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types
struct  Types_t1964849426  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPES_T1964849426_H
#ifndef TYPES_T363369143_H
#define TYPES_T363369143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Types
struct  Types_t363369143  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPES_T363369143_H
#ifndef PHONEEVENT_T3448566392_H
#define PHONEEVENT_T3448566392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.Proto.PhoneEvent
struct  PhoneEvent_t3448566392  : public RuntimeObject
{
public:

public:
};

struct PhoneEvent_t3448566392_StaticFields
{
public:
	// System.Object proto.Proto.PhoneEvent::Descriptor
	RuntimeObject * ___Descriptor_0;

public:
	inline static int32_t get_offset_of_Descriptor_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t3448566392_StaticFields, ___Descriptor_0)); }
	inline RuntimeObject * get_Descriptor_0() const { return ___Descriptor_0; }
	inline RuntimeObject ** get_address_of_Descriptor_0() { return &___Descriptor_0; }
	inline void set_Descriptor_0(RuntimeObject * value)
	{
		___Descriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___Descriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHONEEVENT_T3448566392_H
#ifndef GENERATEDBUILDERLITE_2_T283610718_H
#define GENERATEDBUILDERLITE_2_T283610718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent,proto.PhoneEvent/Builder>
struct  GeneratedBuilderLite_2_t283610718  : public AbstractBuilderLite_2_t1175377521
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T283610718_H
#ifndef GENERATEDBUILDERLITE_2_T1222727714_H
#define GENERATEDBUILDERLITE_2_T1222727714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/AccelerometerEvent,proto.PhoneEvent/Types/AccelerometerEvent/Builder>
struct  GeneratedBuilderLite_2_t1222727714  : public AbstractBuilderLite_2_t2114494517
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T1222727714_H
#ifndef GENERATEDBUILDERLITE_2_T861404538_H
#define GENERATEDBUILDERLITE_2_T861404538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct  GeneratedBuilderLite_2_t861404538  : public AbstractBuilderLite_2_t1753171341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T861404538_H
#ifndef GENERATEDBUILDERLITE_2_T3925513688_H
#define GENERATEDBUILDERLITE_2_T3925513688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct  GeneratedBuilderLite_2_t3925513688  : public AbstractBuilderLite_2_t522313195
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3925513688_H
#ifndef GENERATEDBUILDERLITE_2_T3171452996_H
#define GENERATEDBUILDERLITE_2_T3171452996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  GeneratedBuilderLite_2_t3171452996  : public AbstractBuilderLite_2_t4063219799
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3171452996_H
#ifndef GENERATEDBUILDERLITE_2_T3661349574_H
#define GENERATEDBUILDERLITE_2_T3661349574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct  GeneratedBuilderLite_2_t3661349574  : public AbstractBuilderLite_2_t258149081
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3661349574_H
#ifndef GENERATEDBUILDERLITE_2_T751282667_H
#define GENERATEDBUILDERLITE_2_T751282667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct  GeneratedBuilderLite_2_t751282667  : public AbstractBuilderLite_2_t1643049470
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T751282667_H
#ifndef GENERATEDBUILDERLITE_2_T3142848435_H
#define GENERATEDBUILDERLITE_2_T3142848435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  GeneratedBuilderLite_2_t3142848435  : public AbstractBuilderLite_2_t4034615238
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3142848435_H
#ifndef GENERATEDMESSAGELITE_2_T893745300_H
#define GENERATEDMESSAGELITE_2_T893745300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent,proto.PhoneEvent/Builder>
struct  GeneratedMessageLite_2_t893745300  : public AbstractMessageLite_2_t3099436496
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T893745300_H
#ifndef GENERATEDMESSAGELITE_2_T1832862296_H
#define GENERATEDMESSAGELITE_2_T1832862296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/AccelerometerEvent,proto.PhoneEvent/Types/AccelerometerEvent/Builder>
struct  GeneratedMessageLite_2_t1832862296  : public AbstractMessageLite_2_t4038553492
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T1832862296_H
#ifndef GENERATEDMESSAGELITE_2_T1471539120_H
#define GENERATEDMESSAGELITE_2_T1471539120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct  GeneratedMessageLite_2_t1471539120  : public AbstractMessageLite_2_t3677230316
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T1471539120_H
#ifndef GENERATEDMESSAGELITE_2_T240680974_H
#define GENERATEDMESSAGELITE_2_T240680974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct  GeneratedMessageLite_2_t240680974  : public AbstractMessageLite_2_t2446372170
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T240680974_H
#ifndef GENERATEDMESSAGELITE_2_T3781587578_H
#define GENERATEDMESSAGELITE_2_T3781587578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  GeneratedMessageLite_2_t3781587578  : public AbstractMessageLite_2_t1692311478
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T3781587578_H
#ifndef GENERATEDMESSAGELITE_2_T4271484156_H
#define GENERATEDMESSAGELITE_2_T4271484156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct  GeneratedMessageLite_2_t4271484156  : public AbstractMessageLite_2_t2182208056
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T4271484156_H
#ifndef GENERATEDMESSAGELITE_2_T1361417249_H
#define GENERATEDMESSAGELITE_2_T1361417249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct  GeneratedMessageLite_2_t1361417249  : public AbstractMessageLite_2_t3567108445
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T1361417249_H
#ifndef GENERATEDMESSAGELITE_2_T3752983017_H
#define GENERATEDMESSAGELITE_2_T3752983017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  GeneratedMessageLite_2_t3752983017  : public AbstractMessageLite_2_t1663706917
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T3752983017_H
#ifndef EMULATORTOUCHEVENT_T2277405062_H
#define EMULATORTOUCHEVENT_T2277405062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorTouchEvent
struct  EmulatorTouchEvent_t2277405062 
{
public:
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::action
	int32_t ___action_0;
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::relativeTimestamp
	int32_t ___relativeTimestamp_1;
	// System.Collections.Generic.List`1<Gvr.Internal.EmulatorTouchEvent/Pointer> Gvr.Internal.EmulatorTouchEvent::pointers
	List_1_t697575126 * ___pointers_2;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t2277405062, ___action_0)); }
	inline int32_t get_action_0() const { return ___action_0; }
	inline int32_t* get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(int32_t value)
	{
		___action_0 = value;
	}

	inline static int32_t get_offset_of_relativeTimestamp_1() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t2277405062, ___relativeTimestamp_1)); }
	inline int32_t get_relativeTimestamp_1() const { return ___relativeTimestamp_1; }
	inline int32_t* get_address_of_relativeTimestamp_1() { return &___relativeTimestamp_1; }
	inline void set_relativeTimestamp_1(int32_t value)
	{
		___relativeTimestamp_1 = value;
	}

	inline static int32_t get_offset_of_pointers_2() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t2277405062, ___pointers_2)); }
	inline List_1_t697575126 * get_pointers_2() const { return ___pointers_2; }
	inline List_1_t697575126 ** get_address_of_pointers_2() { return &___pointers_2; }
	inline void set_pointers_2(List_1_t697575126 * value)
	{
		___pointers_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointers_2), value);
	}
};

struct EmulatorTouchEvent_t2277405062_StaticFields
{
public:
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::ACTION_POINTER_INDEX_SHIFT
	int32_t ___ACTION_POINTER_INDEX_SHIFT_3;
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::ACTION_POINTER_INDEX_MASK
	int32_t ___ACTION_POINTER_INDEX_MASK_4;
	// System.Int32 Gvr.Internal.EmulatorTouchEvent::ACTION_MASK
	int32_t ___ACTION_MASK_5;

public:
	inline static int32_t get_offset_of_ACTION_POINTER_INDEX_SHIFT_3() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t2277405062_StaticFields, ___ACTION_POINTER_INDEX_SHIFT_3)); }
	inline int32_t get_ACTION_POINTER_INDEX_SHIFT_3() const { return ___ACTION_POINTER_INDEX_SHIFT_3; }
	inline int32_t* get_address_of_ACTION_POINTER_INDEX_SHIFT_3() { return &___ACTION_POINTER_INDEX_SHIFT_3; }
	inline void set_ACTION_POINTER_INDEX_SHIFT_3(int32_t value)
	{
		___ACTION_POINTER_INDEX_SHIFT_3 = value;
	}

	inline static int32_t get_offset_of_ACTION_POINTER_INDEX_MASK_4() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t2277405062_StaticFields, ___ACTION_POINTER_INDEX_MASK_4)); }
	inline int32_t get_ACTION_POINTER_INDEX_MASK_4() const { return ___ACTION_POINTER_INDEX_MASK_4; }
	inline int32_t* get_address_of_ACTION_POINTER_INDEX_MASK_4() { return &___ACTION_POINTER_INDEX_MASK_4; }
	inline void set_ACTION_POINTER_INDEX_MASK_4(int32_t value)
	{
		___ACTION_POINTER_INDEX_MASK_4 = value;
	}

	inline static int32_t get_offset_of_ACTION_MASK_5() { return static_cast<int32_t>(offsetof(EmulatorTouchEvent_t2277405062_StaticFields, ___ACTION_MASK_5)); }
	inline int32_t get_ACTION_MASK_5() const { return ___ACTION_MASK_5; }
	inline int32_t* get_address_of_ACTION_MASK_5() { return &___ACTION_MASK_5; }
	inline void set_ACTION_MASK_5(int32_t value)
	{
		___ACTION_MASK_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Gvr.Internal.EmulatorTouchEvent
struct EmulatorTouchEvent_t2277405062_marshaled_pinvoke
{
	int32_t ___action_0;
	int32_t ___relativeTimestamp_1;
	List_1_t697575126 * ___pointers_2;
};
// Native definition for COM marshalling of Gvr.Internal.EmulatorTouchEvent
struct EmulatorTouchEvent_t2277405062_marshaled_com
{
	int32_t ___action_0;
	int32_t ___relativeTimestamp_1;
	List_1_t697575126 * ___pointers_2;
};
#endif // EMULATORTOUCHEVENT_T2277405062_H
#ifndef POINTER_T3520467680_H
#define POINTER_T3520467680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorTouchEvent/Pointer
struct  Pointer_t3520467680 
{
public:
	// System.Int32 Gvr.Internal.EmulatorTouchEvent/Pointer::fingerId
	int32_t ___fingerId_0;
	// System.Single Gvr.Internal.EmulatorTouchEvent/Pointer::normalizedX
	float ___normalizedX_1;
	// System.Single Gvr.Internal.EmulatorTouchEvent/Pointer::normalizedY
	float ___normalizedY_2;

public:
	inline static int32_t get_offset_of_fingerId_0() { return static_cast<int32_t>(offsetof(Pointer_t3520467680, ___fingerId_0)); }
	inline int32_t get_fingerId_0() const { return ___fingerId_0; }
	inline int32_t* get_address_of_fingerId_0() { return &___fingerId_0; }
	inline void set_fingerId_0(int32_t value)
	{
		___fingerId_0 = value;
	}

	inline static int32_t get_offset_of_normalizedX_1() { return static_cast<int32_t>(offsetof(Pointer_t3520467680, ___normalizedX_1)); }
	inline float get_normalizedX_1() const { return ___normalizedX_1; }
	inline float* get_address_of_normalizedX_1() { return &___normalizedX_1; }
	inline void set_normalizedX_1(float value)
	{
		___normalizedX_1 = value;
	}

	inline static int32_t get_offset_of_normalizedY_2() { return static_cast<int32_t>(offsetof(Pointer_t3520467680, ___normalizedY_2)); }
	inline float get_normalizedY_2() const { return ___normalizedY_2; }
	inline float* get_address_of_normalizedY_2() { return &___normalizedY_2; }
	inline void set_normalizedY_2(float value)
	{
		___normalizedY_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTER_T3520467680_H
#ifndef FACECAMERADATA_T2570986833_H
#define FACECAMERADATA_T2570986833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerReticleVisual/FaceCameraData
struct  FaceCameraData_t2570986833 
{
public:
	// System.Boolean GvrControllerReticleVisual/FaceCameraData::alongXAxis
	bool ___alongXAxis_0;
	// System.Boolean GvrControllerReticleVisual/FaceCameraData::alongYAxis
	bool ___alongYAxis_1;
	// System.Boolean GvrControllerReticleVisual/FaceCameraData::alongZAxis
	bool ___alongZAxis_2;

public:
	inline static int32_t get_offset_of_alongXAxis_0() { return static_cast<int32_t>(offsetof(FaceCameraData_t2570986833, ___alongXAxis_0)); }
	inline bool get_alongXAxis_0() const { return ___alongXAxis_0; }
	inline bool* get_address_of_alongXAxis_0() { return &___alongXAxis_0; }
	inline void set_alongXAxis_0(bool value)
	{
		___alongXAxis_0 = value;
	}

	inline static int32_t get_offset_of_alongYAxis_1() { return static_cast<int32_t>(offsetof(FaceCameraData_t2570986833, ___alongYAxis_1)); }
	inline bool get_alongYAxis_1() const { return ___alongYAxis_1; }
	inline bool* get_address_of_alongYAxis_1() { return &___alongYAxis_1; }
	inline void set_alongYAxis_1(bool value)
	{
		___alongYAxis_1 = value;
	}

	inline static int32_t get_offset_of_alongZAxis_2() { return static_cast<int32_t>(offsetof(FaceCameraData_t2570986833, ___alongZAxis_2)); }
	inline bool get_alongZAxis_2() const { return ___alongZAxis_2; }
	inline bool* get_address_of_alongZAxis_2() { return &___alongZAxis_2; }
	inline void set_alongZAxis_2(bool value)
	{
		___alongZAxis_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GvrControllerReticleVisual/FaceCameraData
struct FaceCameraData_t2570986833_marshaled_pinvoke
{
	int32_t ___alongXAxis_0;
	int32_t ___alongYAxis_1;
	int32_t ___alongZAxis_2;
};
// Native definition for COM marshalling of GvrControllerReticleVisual/FaceCameraData
struct FaceCameraData_t2570986833_marshaled_com
{
	int32_t ___alongXAxis_0;
	int32_t ___alongYAxis_1;
	int32_t ___alongZAxis_2;
};
#endif // FACECAMERADATA_T2570986833_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef UNITYEVENT_2_T1826723146_H
#define UNITYEVENT_2_T1826723146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<UnityEngine.GameObject,UnityEngine.EventSystems.PointerEventData>
struct  UnityEvent_2_t1826723146  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t1826723146, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T1826723146_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef EMULATORACCELEVENT_T3193952569_H
#define EMULATORACCELEVENT_T3193952569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorAccelEvent
struct  EmulatorAccelEvent_t3193952569 
{
public:
	// System.Int64 Gvr.Internal.EmulatorAccelEvent::timestamp
	int64_t ___timestamp_0;
	// UnityEngine.Vector3 Gvr.Internal.EmulatorAccelEvent::value
	Vector3_t3722313464  ___value_1;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(EmulatorAccelEvent_t3193952569, ___timestamp_0)); }
	inline int64_t get_timestamp_0() const { return ___timestamp_0; }
	inline int64_t* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(int64_t value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(EmulatorAccelEvent_t3193952569, ___value_1)); }
	inline Vector3_t3722313464  get_value_1() const { return ___value_1; }
	inline Vector3_t3722313464 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector3_t3722313464  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORACCELEVENT_T3193952569_H
#ifndef BUTTONCODE_T712346414_H
#define BUTTONCODE_T712346414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorButtonEvent/ButtonCode
struct  ButtonCode_t712346414 
{
public:
	// System.Int32 Gvr.Internal.EmulatorButtonEvent/ButtonCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ButtonCode_t712346414, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONCODE_T712346414_H
#ifndef MODE_T2693297866_H
#define MODE_T2693297866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorConfig/Mode
struct  Mode_t2693297866 
{
public:
	// System.Int32 Gvr.Internal.EmulatorConfig/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t2693297866, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2693297866_H
#ifndef EMULATORGYROEVENT_T3120581941_H
#define EMULATORGYROEVENT_T3120581941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorGyroEvent
struct  EmulatorGyroEvent_t3120581941 
{
public:
	// System.Int64 Gvr.Internal.EmulatorGyroEvent::timestamp
	int64_t ___timestamp_0;
	// UnityEngine.Vector3 Gvr.Internal.EmulatorGyroEvent::value
	Vector3_t3722313464  ___value_1;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(EmulatorGyroEvent_t3120581941, ___timestamp_0)); }
	inline int64_t get_timestamp_0() const { return ___timestamp_0; }
	inline int64_t* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(int64_t value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(EmulatorGyroEvent_t3120581941, ___value_1)); }
	inline Vector3_t3722313464  get_value_1() const { return ___value_1; }
	inline Vector3_t3722313464 * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Vector3_t3722313464  value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORGYROEVENT_T3120581941_H
#ifndef EMULATORORIENTATIONEVENT_T3113283059_H
#define EMULATORORIENTATIONEVENT_T3113283059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorOrientationEvent
struct  EmulatorOrientationEvent_t3113283059 
{
public:
	// System.Int64 Gvr.Internal.EmulatorOrientationEvent::timestamp
	int64_t ___timestamp_0;
	// UnityEngine.Quaternion Gvr.Internal.EmulatorOrientationEvent::orientation
	Quaternion_t2301928331  ___orientation_1;

public:
	inline static int32_t get_offset_of_timestamp_0() { return static_cast<int32_t>(offsetof(EmulatorOrientationEvent_t3113283059, ___timestamp_0)); }
	inline int64_t get_timestamp_0() const { return ___timestamp_0; }
	inline int64_t* get_address_of_timestamp_0() { return &___timestamp_0; }
	inline void set_timestamp_0(int64_t value)
	{
		___timestamp_0 = value;
	}

	inline static int32_t get_offset_of_orientation_1() { return static_cast<int32_t>(offsetof(EmulatorOrientationEvent_t3113283059, ___orientation_1)); }
	inline Quaternion_t2301928331  get_orientation_1() const { return ___orientation_1; }
	inline Quaternion_t2301928331 * get_address_of_orientation_1() { return &___orientation_1; }
	inline void set_orientation_1(Quaternion_t2301928331  value)
	{
		___orientation_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORORIENTATIONEVENT_T3113283059_H
#ifndef ACTION_T2046604437_H
#define ACTION_T2046604437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorTouchEvent/Action
struct  Action_t2046604437 
{
public:
	// System.Int32 Gvr.Internal.EmulatorTouchEvent/Action::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Action_t2046604437, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T2046604437_H
#ifndef TRIGGEREVENT_T1225966107_H
#define TRIGGEREVENT_T1225966107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAllEventsTrigger/TriggerEvent
struct  TriggerEvent_t1225966107  : public UnityEvent_2_t1826723146
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEREVENT_T1225966107_H
#ifndef SURFACEMATERIAL_T4254821637_H
#define SURFACEMATERIAL_T4254821637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioRoom/SurfaceMaterial
struct  SurfaceMaterial_t4254821637 
{
public:
	// System.Int32 GvrAudioRoom/SurfaceMaterial::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SurfaceMaterial_t4254821637, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEMATERIAL_T4254821637_H
#ifndef RAYCASTMODE_T1421504155_H
#define RAYCASTMODE_T1421504155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer/RaycastMode
struct  RaycastMode_t1421504155 
{
public:
	// System.Int32 GvrBasePointer/RaycastMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RaycastMode_t1421504155, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTMODE_T1421504155_H
#ifndef GVRCONNECTIONSTATE_T641662995_H
#define GVRCONNECTIONSTATE_T641662995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrConnectionState
struct  GvrConnectionState_t641662995 
{
public:
	// System.Int32 GvrConnectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrConnectionState_t641662995, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONNECTIONSTATE_T641662995_H
#ifndef GVRCONTROLLERAPISTATUS_T2615797932_H
#define GVRCONTROLLERAPISTATUS_T2615797932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerApiStatus
struct  GvrControllerApiStatus_t2615797932 
{
public:
	// System.Int32 GvrControllerApiStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrControllerApiStatus_t2615797932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERAPISTATUS_T2615797932_H
#ifndef GVRCONTROLLERBATTERYLEVEL_T3390817147_H
#define GVRCONTROLLERBATTERYLEVEL_T3390817147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerBatteryLevel
struct  GvrControllerBatteryLevel_t3390817147 
{
public:
	// System.Int32 GvrControllerBatteryLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrControllerBatteryLevel_t3390817147, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERBATTERYLEVEL_T3390817147_H
#ifndef GVRCONTROLLERBUTTON_T87476287_H
#define GVRCONTROLLERBUTTON_T87476287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerButton
struct  GvrControllerButton_t87476287 
{
public:
	// System.Int32 GvrControllerButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrControllerButton_t87476287, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERBUTTON_T87476287_H
#ifndef GVRCONTROLLERHAND_T2517129446_H
#define GVRCONTROLLERHAND_T2517129446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerHand
struct  GvrControllerHand_t2517129446 
{
public:
	// System.Int32 GvrControllerHand::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrControllerHand_t2517129446, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERHAND_T2517129446_H
#ifndef EMULATORCONNECTIONMODE_T110584383_H
#define EMULATORCONNECTIONMODE_T110584383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerInput/EmulatorConnectionMode
struct  EmulatorConnectionMode_t110584383 
{
public:
	// System.Int32 GvrControllerInput/EmulatorConnectionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EmulatorConnectionMode_t110584383, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORCONNECTIONMODE_T110584383_H
#ifndef GVRCONTROLLERINPUTDEVICE_T3709172113_H
#define GVRCONTROLLERINPUTDEVICE_T3709172113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerInputDevice
struct  GvrControllerInputDevice_t3709172113  : public RuntimeObject
{
public:
	// Gvr.Internal.IControllerProvider GvrControllerInputDevice::controllerProvider
	RuntimeObject* ___controllerProvider_0;
	// System.Int32 GvrControllerInputDevice::controllerId
	int32_t ___controllerId_1;
	// Gvr.Internal.ControllerState GvrControllerInputDevice::controllerState
	ControllerState_t3401244786 * ___controllerState_2;
	// UnityEngine.Vector2 GvrControllerInputDevice::touchPosCentered
	Vector2_t2156229523  ___touchPosCentered_3;
	// System.Int32 GvrControllerInputDevice::lastUpdatedFrameCount
	int32_t ___lastUpdatedFrameCount_4;
	// System.Boolean GvrControllerInputDevice::valid
	bool ___valid_5;
	// GvrControllerInput/OnStateChangedEvent GvrControllerInputDevice::OnStateChanged
	OnStateChangedEvent_t3148566047 * ___OnStateChanged_6;

public:
	inline static int32_t get_offset_of_controllerProvider_0() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t3709172113, ___controllerProvider_0)); }
	inline RuntimeObject* get_controllerProvider_0() const { return ___controllerProvider_0; }
	inline RuntimeObject** get_address_of_controllerProvider_0() { return &___controllerProvider_0; }
	inline void set_controllerProvider_0(RuntimeObject* value)
	{
		___controllerProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&___controllerProvider_0), value);
	}

	inline static int32_t get_offset_of_controllerId_1() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t3709172113, ___controllerId_1)); }
	inline int32_t get_controllerId_1() const { return ___controllerId_1; }
	inline int32_t* get_address_of_controllerId_1() { return &___controllerId_1; }
	inline void set_controllerId_1(int32_t value)
	{
		___controllerId_1 = value;
	}

	inline static int32_t get_offset_of_controllerState_2() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t3709172113, ___controllerState_2)); }
	inline ControllerState_t3401244786 * get_controllerState_2() const { return ___controllerState_2; }
	inline ControllerState_t3401244786 ** get_address_of_controllerState_2() { return &___controllerState_2; }
	inline void set_controllerState_2(ControllerState_t3401244786 * value)
	{
		___controllerState_2 = value;
		Il2CppCodeGenWriteBarrier((&___controllerState_2), value);
	}

	inline static int32_t get_offset_of_touchPosCentered_3() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t3709172113, ___touchPosCentered_3)); }
	inline Vector2_t2156229523  get_touchPosCentered_3() const { return ___touchPosCentered_3; }
	inline Vector2_t2156229523 * get_address_of_touchPosCentered_3() { return &___touchPosCentered_3; }
	inline void set_touchPosCentered_3(Vector2_t2156229523  value)
	{
		___touchPosCentered_3 = value;
	}

	inline static int32_t get_offset_of_lastUpdatedFrameCount_4() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t3709172113, ___lastUpdatedFrameCount_4)); }
	inline int32_t get_lastUpdatedFrameCount_4() const { return ___lastUpdatedFrameCount_4; }
	inline int32_t* get_address_of_lastUpdatedFrameCount_4() { return &___lastUpdatedFrameCount_4; }
	inline void set_lastUpdatedFrameCount_4(int32_t value)
	{
		___lastUpdatedFrameCount_4 = value;
	}

	inline static int32_t get_offset_of_valid_5() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t3709172113, ___valid_5)); }
	inline bool get_valid_5() const { return ___valid_5; }
	inline bool* get_address_of_valid_5() { return &___valid_5; }
	inline void set_valid_5(bool value)
	{
		___valid_5 = value;
	}

	inline static int32_t get_offset_of_OnStateChanged_6() { return static_cast<int32_t>(offsetof(GvrControllerInputDevice_t3709172113, ___OnStateChanged_6)); }
	inline OnStateChangedEvent_t3148566047 * get_OnStateChanged_6() const { return ___OnStateChanged_6; }
	inline OnStateChangedEvent_t3148566047 ** get_address_of_OnStateChanged_6() { return &___OnStateChanged_6; }
	inline void set_OnStateChanged_6(OnStateChangedEvent_t3148566047 * value)
	{
		___OnStateChanged_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnStateChanged_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERINPUTDEVICE_T3709172113_H
#ifndef GVREVENTTYPE_T1628138291_H
#define GVREVENTTYPE_T1628138291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventType
struct  GvrEventType_t1628138291 
{
public:
	// System.Int32 GvrEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrEventType_t1628138291, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREVENTTYPE_T1628138291_H
#ifndef BLOCKINGOBJECTS_T813457210_H
#define BLOCKINGOBJECTS_T813457210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerGraphicRaycaster/BlockingObjects
struct  BlockingObjects_t813457210 
{
public:
	// System.Int32 GvrPointerGraphicRaycaster/BlockingObjects::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlockingObjects_t813457210, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKINGOBJECTS_T813457210_H
#ifndef GVRPOINTERINPUTMODULEIMPL_T2260544298_H
#define GVRPOINTERINPUTMODULEIMPL_T2260544298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerInputModuleImpl
struct  GvrPointerInputModuleImpl_t2260544298  : public RuntimeObject
{
public:
	// IGvrInputModuleController GvrPointerInputModuleImpl::<ModuleController>k__BackingField
	RuntimeObject* ___U3CModuleControllerU3Ek__BackingField_0;
	// IGvrEventExecutor GvrPointerInputModuleImpl::<EventExecutor>k__BackingField
	RuntimeObject* ___U3CEventExecutorU3Ek__BackingField_1;
	// System.Boolean GvrPointerInputModuleImpl::<VrModeOnly>k__BackingField
	bool ___U3CVrModeOnlyU3Ek__BackingField_2;
	// GvrPointerScrollInput GvrPointerInputModuleImpl::<ScrollInput>k__BackingField
	GvrPointerScrollInput_t3738414627 * ___U3CScrollInputU3Ek__BackingField_3;
	// UnityEngine.EventSystems.PointerEventData GvrPointerInputModuleImpl::<CurrentEventData>k__BackingField
	PointerEventData_t3807901092 * ___U3CCurrentEventDataU3Ek__BackingField_4;
	// GvrBasePointer GvrPointerInputModuleImpl::pointer
	GvrBasePointer_t822782720 * ___pointer_5;
	// UnityEngine.Vector2 GvrPointerInputModuleImpl::lastPose
	Vector2_t2156229523  ___lastPose_6;
	// System.Boolean GvrPointerInputModuleImpl::isPointerHovering
	bool ___isPointerHovering_7;
	// System.Boolean GvrPointerInputModuleImpl::isActive
	bool ___isActive_8;

public:
	inline static int32_t get_offset_of_U3CModuleControllerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CModuleControllerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CModuleControllerU3Ek__BackingField_0() const { return ___U3CModuleControllerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CModuleControllerU3Ek__BackingField_0() { return &___U3CModuleControllerU3Ek__BackingField_0; }
	inline void set_U3CModuleControllerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CModuleControllerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModuleControllerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CEventExecutorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CEventExecutorU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CEventExecutorU3Ek__BackingField_1() const { return ___U3CEventExecutorU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CEventExecutorU3Ek__BackingField_1() { return &___U3CEventExecutorU3Ek__BackingField_1; }
	inline void set_U3CEventExecutorU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CEventExecutorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventExecutorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CVrModeOnlyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CVrModeOnlyU3Ek__BackingField_2)); }
	inline bool get_U3CVrModeOnlyU3Ek__BackingField_2() const { return ___U3CVrModeOnlyU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CVrModeOnlyU3Ek__BackingField_2() { return &___U3CVrModeOnlyU3Ek__BackingField_2; }
	inline void set_U3CVrModeOnlyU3Ek__BackingField_2(bool value)
	{
		___U3CVrModeOnlyU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CScrollInputU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CScrollInputU3Ek__BackingField_3)); }
	inline GvrPointerScrollInput_t3738414627 * get_U3CScrollInputU3Ek__BackingField_3() const { return ___U3CScrollInputU3Ek__BackingField_3; }
	inline GvrPointerScrollInput_t3738414627 ** get_address_of_U3CScrollInputU3Ek__BackingField_3() { return &___U3CScrollInputU3Ek__BackingField_3; }
	inline void set_U3CScrollInputU3Ek__BackingField_3(GvrPointerScrollInput_t3738414627 * value)
	{
		___U3CScrollInputU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScrollInputU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCurrentEventDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___U3CCurrentEventDataU3Ek__BackingField_4)); }
	inline PointerEventData_t3807901092 * get_U3CCurrentEventDataU3Ek__BackingField_4() const { return ___U3CCurrentEventDataU3Ek__BackingField_4; }
	inline PointerEventData_t3807901092 ** get_address_of_U3CCurrentEventDataU3Ek__BackingField_4() { return &___U3CCurrentEventDataU3Ek__BackingField_4; }
	inline void set_U3CCurrentEventDataU3Ek__BackingField_4(PointerEventData_t3807901092 * value)
	{
		___U3CCurrentEventDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentEventDataU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_pointer_5() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___pointer_5)); }
	inline GvrBasePointer_t822782720 * get_pointer_5() const { return ___pointer_5; }
	inline GvrBasePointer_t822782720 ** get_address_of_pointer_5() { return &___pointer_5; }
	inline void set_pointer_5(GvrBasePointer_t822782720 * value)
	{
		___pointer_5 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_5), value);
	}

	inline static int32_t get_offset_of_lastPose_6() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___lastPose_6)); }
	inline Vector2_t2156229523  get_lastPose_6() const { return ___lastPose_6; }
	inline Vector2_t2156229523 * get_address_of_lastPose_6() { return &___lastPose_6; }
	inline void set_lastPose_6(Vector2_t2156229523  value)
	{
		___lastPose_6 = value;
	}

	inline static int32_t get_offset_of_isPointerHovering_7() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___isPointerHovering_7)); }
	inline bool get_isPointerHovering_7() const { return ___isPointerHovering_7; }
	inline bool* get_address_of_isPointerHovering_7() { return &___isPointerHovering_7; }
	inline void set_isPointerHovering_7(bool value)
	{
		___isPointerHovering_7 = value;
	}

	inline static int32_t get_offset_of_isActive_8() { return static_cast<int32_t>(offsetof(GvrPointerInputModuleImpl_t2260544298, ___isActive_8)); }
	inline bool get_isActive_8() const { return ___isActive_8; }
	inline bool* get_address_of_isActive_8() { return &___isActive_8; }
	inline void set_isActive_8(bool value)
	{
		___isActive_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERINPUTMODULEIMPL_T2260544298_H
#ifndef SCROLLINFO_T1733197845_H
#define SCROLLINFO_T1733197845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerScrollInput/ScrollInfo
struct  ScrollInfo_t1733197845  : public RuntimeObject
{
public:
	// System.Boolean GvrPointerScrollInput/ScrollInfo::isScrollingX
	bool ___isScrollingX_0;
	// System.Boolean GvrPointerScrollInput/ScrollInfo::isScrollingY
	bool ___isScrollingY_1;
	// UnityEngine.Vector2 GvrPointerScrollInput/ScrollInfo::initScroll
	Vector2_t2156229523  ___initScroll_2;
	// UnityEngine.Vector2 GvrPointerScrollInput/ScrollInfo::lastScroll
	Vector2_t2156229523  ___lastScroll_3;
	// UnityEngine.Vector2 GvrPointerScrollInput/ScrollInfo::scrollVelocity
	Vector2_t2156229523  ___scrollVelocity_4;
	// IGvrScrollSettings GvrPointerScrollInput/ScrollInfo::scrollSettings
	RuntimeObject* ___scrollSettings_5;

public:
	inline static int32_t get_offset_of_isScrollingX_0() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___isScrollingX_0)); }
	inline bool get_isScrollingX_0() const { return ___isScrollingX_0; }
	inline bool* get_address_of_isScrollingX_0() { return &___isScrollingX_0; }
	inline void set_isScrollingX_0(bool value)
	{
		___isScrollingX_0 = value;
	}

	inline static int32_t get_offset_of_isScrollingY_1() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___isScrollingY_1)); }
	inline bool get_isScrollingY_1() const { return ___isScrollingY_1; }
	inline bool* get_address_of_isScrollingY_1() { return &___isScrollingY_1; }
	inline void set_isScrollingY_1(bool value)
	{
		___isScrollingY_1 = value;
	}

	inline static int32_t get_offset_of_initScroll_2() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___initScroll_2)); }
	inline Vector2_t2156229523  get_initScroll_2() const { return ___initScroll_2; }
	inline Vector2_t2156229523 * get_address_of_initScroll_2() { return &___initScroll_2; }
	inline void set_initScroll_2(Vector2_t2156229523  value)
	{
		___initScroll_2 = value;
	}

	inline static int32_t get_offset_of_lastScroll_3() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___lastScroll_3)); }
	inline Vector2_t2156229523  get_lastScroll_3() const { return ___lastScroll_3; }
	inline Vector2_t2156229523 * get_address_of_lastScroll_3() { return &___lastScroll_3; }
	inline void set_lastScroll_3(Vector2_t2156229523  value)
	{
		___lastScroll_3 = value;
	}

	inline static int32_t get_offset_of_scrollVelocity_4() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___scrollVelocity_4)); }
	inline Vector2_t2156229523  get_scrollVelocity_4() const { return ___scrollVelocity_4; }
	inline Vector2_t2156229523 * get_address_of_scrollVelocity_4() { return &___scrollVelocity_4; }
	inline void set_scrollVelocity_4(Vector2_t2156229523  value)
	{
		___scrollVelocity_4 = value;
	}

	inline static int32_t get_offset_of_scrollSettings_5() { return static_cast<int32_t>(offsetof(ScrollInfo_t1733197845, ___scrollSettings_5)); }
	inline RuntimeObject* get_scrollSettings_5() const { return ___scrollSettings_5; }
	inline RuntimeObject** get_address_of_scrollSettings_5() { return &___scrollSettings_5; }
	inline void set_scrollSettings_5(RuntimeObject* value)
	{
		___scrollSettings_5 = value;
		Il2CppCodeGenWriteBarrier((&___scrollSettings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLINFO_T1733197845_H
#ifndef GVRRECENTEREVENTTYPE_T513699134_H
#define GVRRECENTEREVENTTYPE_T513699134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterEventType
struct  GvrRecenterEventType_t513699134 
{
public:
	// System.Int32 GvrRecenterEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrRecenterEventType_t513699134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTEREVENTTYPE_T513699134_H
#ifndef GVRRECENTERFLAGS_T370890970_H
#define GVRRECENTERFLAGS_T370890970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterFlags
struct  GvrRecenterFlags_t370890970 
{
public:
	// System.Int32 GvrRecenterFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrRecenterFlags_t370890970, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTERFLAGS_T370890970_H
#ifndef USERPREFSHANDEDNESS_T3496962503_H
#define USERPREFSHANDEDNESS_T3496962503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings/UserPrefsHandedness
struct  UserPrefsHandedness_t3496962503 
{
public:
	// System.Int32 GvrSettings/UserPrefsHandedness::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UserPrefsHandedness_t3496962503, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERPREFSHANDEDNESS_T3496962503_H
#ifndef VIEWERPLATFORMTYPE_T1613185256_H
#define VIEWERPLATFORMTYPE_T1613185256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSettings/ViewerPlatformType
struct  ViewerPlatformType_t1613185256 
{
public:
	// System.Int32 GvrSettings/ViewerPlatformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ViewerPlatformType_t1613185256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERPLATFORMTYPE_T1613185256_H
#ifndef LOCATION_T4049147969_H
#define LOCATION_T4049147969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrTooltip/Location
struct  Location_t4049147969 
{
public:
	// System.Int32 GvrTooltip/Location::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Location_t4049147969, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T4049147969_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef AUDIOROLLOFFMODE_T832723327_H
#define AUDIOROLLOFFMODE_T832723327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioRolloffMode
struct  AudioRolloffMode_t832723327 
{
public:
	// System.Int32 UnityEngine.AudioRolloffMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AudioRolloffMode_t832723327, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOROLLOFFMODE_T832723327_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef BUILDER_T895705486_H
#define BUILDER_T895705486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Builder
struct  Builder_t895705486  : public GeneratedBuilderLite_2_t283610718
{
public:
	// System.Boolean proto.PhoneEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent proto.PhoneEvent/Builder::result
	PhoneEvent_t1358418708 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t895705486, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t895705486, ___result_1)); }
	inline PhoneEvent_t1358418708 * get_result_1() const { return ___result_1; }
	inline PhoneEvent_t1358418708 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(PhoneEvent_t1358418708 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T895705486_H
#ifndef ACCELEROMETEREVENT_T1394922645_H
#define ACCELEROMETEREVENT_T1394922645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/AccelerometerEvent
struct  AccelerometerEvent_t1394922645  : public GeneratedMessageLite_2_t1832862296
{
public:
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/AccelerometerEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent/Types/AccelerometerEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent/Types/AccelerometerEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent/Types/AccelerometerEvent::z_
	float ___z__14;
	// System.Int32 proto.PhoneEvent/Types/AccelerometerEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_15;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_15() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645, ___memoizedSerializedSize_15)); }
	inline int32_t get_memoizedSerializedSize_15() const { return ___memoizedSerializedSize_15; }
	inline int32_t* get_address_of_memoizedSerializedSize_15() { return &___memoizedSerializedSize_15; }
	inline void set_memoizedSerializedSize_15(int32_t value)
	{
		___memoizedSerializedSize_15 = value;
	}
};

struct AccelerometerEvent_t1394922645_StaticFields
{
public:
	// proto.PhoneEvent/Types/AccelerometerEvent proto.PhoneEvent/Types/AccelerometerEvent::defaultInstance
	AccelerometerEvent_t1394922645 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/AccelerometerEvent::_accelerometerEventFieldNames
	StringU5BU5D_t1281789340* ____accelerometerEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/AccelerometerEvent::_accelerometerEventFieldTags
	UInt32U5BU5D_t2770800703* ____accelerometerEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645_StaticFields, ___defaultInstance_0)); }
	inline AccelerometerEvent_t1394922645 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline AccelerometerEvent_t1394922645 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(AccelerometerEvent_t1394922645 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__accelerometerEventFieldNames_1() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645_StaticFields, ____accelerometerEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__accelerometerEventFieldNames_1() const { return ____accelerometerEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__accelerometerEventFieldNames_1() { return &____accelerometerEventFieldNames_1; }
	inline void set__accelerometerEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____accelerometerEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____accelerometerEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__accelerometerEventFieldTags_2() { return static_cast<int32_t>(offsetof(AccelerometerEvent_t1394922645_StaticFields, ____accelerometerEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__accelerometerEventFieldTags_2() const { return ____accelerometerEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__accelerometerEventFieldTags_2() { return &____accelerometerEventFieldTags_2; }
	inline void set__accelerometerEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____accelerometerEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____accelerometerEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCELEROMETEREVENT_T1394922645_H
#ifndef BUILDER_T2179940343_H
#define BUILDER_T2179940343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/AccelerometerEvent/Builder
struct  Builder_t2179940343  : public GeneratedBuilderLite_2_t1222727714
{
public:
	// System.Boolean proto.PhoneEvent/Types/AccelerometerEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/AccelerometerEvent proto.PhoneEvent/Types/AccelerometerEvent/Builder::result
	AccelerometerEvent_t1394922645 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t2179940343, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t2179940343, ___result_1)); }
	inline AccelerometerEvent_t1394922645 * get_result_1() const { return ___result_1; }
	inline AccelerometerEvent_t1394922645 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(AccelerometerEvent_t1394922645 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2179940343_H
#ifndef DEPTHMAPEVENT_T729886054_H
#define DEPTHMAPEVENT_T729886054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/DepthMapEvent
struct  DepthMapEvent_t729886054  : public GeneratedMessageLite_2_t1471539120
{
public:
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/DepthMapEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::hasWidth
	bool ___hasWidth_7;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::width_
	int32_t ___width__8;
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::hasHeight
	bool ___hasHeight_10;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::height_
	int32_t ___height__11;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::zDistancesMemoizedSerializedSize
	int32_t ___zDistancesMemoizedSerializedSize_13;
	// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single> proto.PhoneEvent/Types/DepthMapEvent::zDistances_
	PopsicleList_1_t2780837186 * ___zDistances__14;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_15;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasWidth_7() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___hasWidth_7)); }
	inline bool get_hasWidth_7() const { return ___hasWidth_7; }
	inline bool* get_address_of_hasWidth_7() { return &___hasWidth_7; }
	inline void set_hasWidth_7(bool value)
	{
		___hasWidth_7 = value;
	}

	inline static int32_t get_offset_of_width__8() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___width__8)); }
	inline int32_t get_width__8() const { return ___width__8; }
	inline int32_t* get_address_of_width__8() { return &___width__8; }
	inline void set_width__8(int32_t value)
	{
		___width__8 = value;
	}

	inline static int32_t get_offset_of_hasHeight_10() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___hasHeight_10)); }
	inline bool get_hasHeight_10() const { return ___hasHeight_10; }
	inline bool* get_address_of_hasHeight_10() { return &___hasHeight_10; }
	inline void set_hasHeight_10(bool value)
	{
		___hasHeight_10 = value;
	}

	inline static int32_t get_offset_of_height__11() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___height__11)); }
	inline int32_t get_height__11() const { return ___height__11; }
	inline int32_t* get_address_of_height__11() { return &___height__11; }
	inline void set_height__11(int32_t value)
	{
		___height__11 = value;
	}

	inline static int32_t get_offset_of_zDistancesMemoizedSerializedSize_13() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___zDistancesMemoizedSerializedSize_13)); }
	inline int32_t get_zDistancesMemoizedSerializedSize_13() const { return ___zDistancesMemoizedSerializedSize_13; }
	inline int32_t* get_address_of_zDistancesMemoizedSerializedSize_13() { return &___zDistancesMemoizedSerializedSize_13; }
	inline void set_zDistancesMemoizedSerializedSize_13(int32_t value)
	{
		___zDistancesMemoizedSerializedSize_13 = value;
	}

	inline static int32_t get_offset_of_zDistances__14() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___zDistances__14)); }
	inline PopsicleList_1_t2780837186 * get_zDistances__14() const { return ___zDistances__14; }
	inline PopsicleList_1_t2780837186 ** get_address_of_zDistances__14() { return &___zDistances__14; }
	inline void set_zDistances__14(PopsicleList_1_t2780837186 * value)
	{
		___zDistances__14 = value;
		Il2CppCodeGenWriteBarrier((&___zDistances__14), value);
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_15() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___memoizedSerializedSize_15)); }
	inline int32_t get_memoizedSerializedSize_15() const { return ___memoizedSerializedSize_15; }
	inline int32_t* get_address_of_memoizedSerializedSize_15() { return &___memoizedSerializedSize_15; }
	inline void set_memoizedSerializedSize_15(int32_t value)
	{
		___memoizedSerializedSize_15 = value;
	}
};

struct DepthMapEvent_t729886054_StaticFields
{
public:
	// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent::defaultInstance
	DepthMapEvent_t729886054 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/DepthMapEvent::_depthMapEventFieldNames
	StringU5BU5D_t1281789340* ____depthMapEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/DepthMapEvent::_depthMapEventFieldTags
	UInt32U5BU5D_t2770800703* ____depthMapEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054_StaticFields, ___defaultInstance_0)); }
	inline DepthMapEvent_t729886054 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline DepthMapEvent_t729886054 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(DepthMapEvent_t729886054 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__depthMapEventFieldNames_1() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054_StaticFields, ____depthMapEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__depthMapEventFieldNames_1() const { return ____depthMapEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__depthMapEventFieldNames_1() { return &____depthMapEventFieldNames_1; }
	inline void set__depthMapEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____depthMapEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____depthMapEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__depthMapEventFieldTags_2() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054_StaticFields, ____depthMapEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__depthMapEventFieldTags_2() const { return ____depthMapEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__depthMapEventFieldTags_2() { return &____depthMapEventFieldTags_2; }
	inline void set__depthMapEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____depthMapEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____depthMapEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHMAPEVENT_T729886054_H
#ifndef BUILDER_T1293396100_H
#define BUILDER_T1293396100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/DepthMapEvent/Builder
struct  Builder_t1293396100  : public GeneratedBuilderLite_2_t861404538
{
public:
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent/Builder::result
	DepthMapEvent_t729886054 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t1293396100, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t1293396100, ___result_1)); }
	inline DepthMapEvent_t729886054 * get_result_1() const { return ___result_1; }
	inline DepthMapEvent_t729886054 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(DepthMapEvent_t729886054 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T1293396100_H
#ifndef GYROSCOPEEVENT_T127774954_H
#define GYROSCOPEEVENT_T127774954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/GyroscopeEvent
struct  GyroscopeEvent_t127774954  : public GeneratedMessageLite_2_t240680974
{
public:
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/GyroscopeEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::z_
	float ___z__14;
	// System.Int32 proto.PhoneEvent/Types/GyroscopeEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_15;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_15() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___memoizedSerializedSize_15)); }
	inline int32_t get_memoizedSerializedSize_15() const { return ___memoizedSerializedSize_15; }
	inline int32_t* get_address_of_memoizedSerializedSize_15() { return &___memoizedSerializedSize_15; }
	inline void set_memoizedSerializedSize_15(int32_t value)
	{
		___memoizedSerializedSize_15 = value;
	}
};

struct GyroscopeEvent_t127774954_StaticFields
{
public:
	// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::defaultInstance
	GyroscopeEvent_t127774954 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/GyroscopeEvent::_gyroscopeEventFieldNames
	StringU5BU5D_t1281789340* ____gyroscopeEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/GyroscopeEvent::_gyroscopeEventFieldTags
	UInt32U5BU5D_t2770800703* ____gyroscopeEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954_StaticFields, ___defaultInstance_0)); }
	inline GyroscopeEvent_t127774954 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline GyroscopeEvent_t127774954 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(GyroscopeEvent_t127774954 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__gyroscopeEventFieldNames_1() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954_StaticFields, ____gyroscopeEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__gyroscopeEventFieldNames_1() const { return ____gyroscopeEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__gyroscopeEventFieldNames_1() { return &____gyroscopeEventFieldNames_1; }
	inline void set__gyroscopeEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____gyroscopeEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____gyroscopeEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__gyroscopeEventFieldTags_2() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954_StaticFields, ____gyroscopeEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__gyroscopeEventFieldTags_2() const { return ____gyroscopeEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__gyroscopeEventFieldTags_2() { return &____gyroscopeEventFieldTags_2; }
	inline void set__gyroscopeEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____gyroscopeEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____gyroscopeEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROSCOPEEVENT_T127774954_H
#ifndef BUILDER_T3442751222_H
#define BUILDER_T3442751222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/GyroscopeEvent/Builder
struct  Builder_t3442751222  : public GeneratedBuilderLite_2_t3925513688
{
public:
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent/Builder::result
	GyroscopeEvent_t127774954 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t3442751222, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t3442751222, ___result_1)); }
	inline GyroscopeEvent_t127774954 * get_result_1() const { return ___result_1; }
	inline GyroscopeEvent_t127774954 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(GyroscopeEvent_t127774954 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T3442751222_H
#ifndef KEYEVENT_T1937114521_H
#define KEYEVENT_T1937114521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/KeyEvent
struct  KeyEvent_t1937114521  : public GeneratedMessageLite_2_t3781587578
{
public:
	// System.Boolean proto.PhoneEvent/Types/KeyEvent::hasAction
	bool ___hasAction_4;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::action_
	int32_t ___action__5;
	// System.Boolean proto.PhoneEvent/Types/KeyEvent::hasCode
	bool ___hasCode_7;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::code_
	int32_t ___code__8;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_9;

public:
	inline static int32_t get_offset_of_hasAction_4() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___hasAction_4)); }
	inline bool get_hasAction_4() const { return ___hasAction_4; }
	inline bool* get_address_of_hasAction_4() { return &___hasAction_4; }
	inline void set_hasAction_4(bool value)
	{
		___hasAction_4 = value;
	}

	inline static int32_t get_offset_of_action__5() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___action__5)); }
	inline int32_t get_action__5() const { return ___action__5; }
	inline int32_t* get_address_of_action__5() { return &___action__5; }
	inline void set_action__5(int32_t value)
	{
		___action__5 = value;
	}

	inline static int32_t get_offset_of_hasCode_7() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___hasCode_7)); }
	inline bool get_hasCode_7() const { return ___hasCode_7; }
	inline bool* get_address_of_hasCode_7() { return &___hasCode_7; }
	inline void set_hasCode_7(bool value)
	{
		___hasCode_7 = value;
	}

	inline static int32_t get_offset_of_code__8() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___code__8)); }
	inline int32_t get_code__8() const { return ___code__8; }
	inline int32_t* get_address_of_code__8() { return &___code__8; }
	inline void set_code__8(int32_t value)
	{
		___code__8 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_9() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___memoizedSerializedSize_9)); }
	inline int32_t get_memoizedSerializedSize_9() const { return ___memoizedSerializedSize_9; }
	inline int32_t* get_address_of_memoizedSerializedSize_9() { return &___memoizedSerializedSize_9; }
	inline void set_memoizedSerializedSize_9(int32_t value)
	{
		___memoizedSerializedSize_9 = value;
	}
};

struct KeyEvent_t1937114521_StaticFields
{
public:
	// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::defaultInstance
	KeyEvent_t1937114521 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/KeyEvent::_keyEventFieldNames
	StringU5BU5D_t1281789340* ____keyEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/KeyEvent::_keyEventFieldTags
	UInt32U5BU5D_t2770800703* ____keyEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521_StaticFields, ___defaultInstance_0)); }
	inline KeyEvent_t1937114521 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline KeyEvent_t1937114521 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(KeyEvent_t1937114521 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__keyEventFieldNames_1() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521_StaticFields, ____keyEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__keyEventFieldNames_1() const { return ____keyEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__keyEventFieldNames_1() { return &____keyEventFieldNames_1; }
	inline void set__keyEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____keyEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____keyEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__keyEventFieldTags_2() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521_StaticFields, ____keyEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__keyEventFieldTags_2() const { return ____keyEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__keyEventFieldTags_2() { return &____keyEventFieldTags_2; }
	inline void set__keyEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____keyEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEVENT_T1937114521_H
#ifndef BUILDER_T2712992173_H
#define BUILDER_T2712992173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/KeyEvent/Builder
struct  Builder_t2712992173  : public GeneratedBuilderLite_2_t3171452996
{
public:
	// System.Boolean proto.PhoneEvent/Types/KeyEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent/Builder::result
	KeyEvent_t1937114521 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t2712992173, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t2712992173, ___result_1)); }
	inline KeyEvent_t1937114521 * get_result_1() const { return ___result_1; }
	inline KeyEvent_t1937114521 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(KeyEvent_t1937114521 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2712992173_H
#ifndef MOTIONEVENT_T3121383016_H
#define MOTIONEVENT_T3121383016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent
struct  MotionEvent_t3121383016  : public GeneratedMessageLite_2_t4271484156
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/MotionEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent::hasAction
	bool ___hasAction_7;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent::action_
	int32_t ___action__8;
	// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer> proto.PhoneEvent/Types/MotionEvent::pointers_
	PopsicleList_1_t1233628674 * ___pointers__10;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_11;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasAction_7() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___hasAction_7)); }
	inline bool get_hasAction_7() const { return ___hasAction_7; }
	inline bool* get_address_of_hasAction_7() { return &___hasAction_7; }
	inline void set_hasAction_7(bool value)
	{
		___hasAction_7 = value;
	}

	inline static int32_t get_offset_of_action__8() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___action__8)); }
	inline int32_t get_action__8() const { return ___action__8; }
	inline int32_t* get_address_of_action__8() { return &___action__8; }
	inline void set_action__8(int32_t value)
	{
		___action__8 = value;
	}

	inline static int32_t get_offset_of_pointers__10() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___pointers__10)); }
	inline PopsicleList_1_t1233628674 * get_pointers__10() const { return ___pointers__10; }
	inline PopsicleList_1_t1233628674 ** get_address_of_pointers__10() { return &___pointers__10; }
	inline void set_pointers__10(PopsicleList_1_t1233628674 * value)
	{
		___pointers__10 = value;
		Il2CppCodeGenWriteBarrier((&___pointers__10), value);
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_11() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___memoizedSerializedSize_11)); }
	inline int32_t get_memoizedSerializedSize_11() const { return ___memoizedSerializedSize_11; }
	inline int32_t* get_address_of_memoizedSerializedSize_11() { return &___memoizedSerializedSize_11; }
	inline void set_memoizedSerializedSize_11(int32_t value)
	{
		___memoizedSerializedSize_11 = value;
	}
};

struct MotionEvent_t3121383016_StaticFields
{
public:
	// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::defaultInstance
	MotionEvent_t3121383016 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/MotionEvent::_motionEventFieldNames
	StringU5BU5D_t1281789340* ____motionEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/MotionEvent::_motionEventFieldTags
	UInt32U5BU5D_t2770800703* ____motionEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016_StaticFields, ___defaultInstance_0)); }
	inline MotionEvent_t3121383016 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline MotionEvent_t3121383016 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(MotionEvent_t3121383016 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__motionEventFieldNames_1() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016_StaticFields, ____motionEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__motionEventFieldNames_1() const { return ____motionEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__motionEventFieldNames_1() { return &____motionEventFieldNames_1; }
	inline void set__motionEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____motionEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____motionEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__motionEventFieldTags_2() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016_StaticFields, ____motionEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__motionEventFieldTags_2() const { return ____motionEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__motionEventFieldTags_2() { return &____motionEventFieldTags_2; }
	inline void set__motionEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____motionEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____motionEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONEVENT_T3121383016_H
#ifndef BUILDER_T2961114394_H
#define BUILDER_T2961114394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Builder
struct  Builder_t2961114394  : public GeneratedBuilderLite_2_t3661349574
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent/Builder::result
	MotionEvent_t3121383016 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t2961114394, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t2961114394, ___result_1)); }
	inline MotionEvent_t3121383016 * get_result_1() const { return ___result_1; }
	inline MotionEvent_t3121383016 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(MotionEvent_t3121383016 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2961114394_H
#ifndef POINTER_T4145025558_H
#define POINTER_T4145025558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Types/Pointer
struct  Pointer_t4145025558  : public GeneratedMessageLite_2_t1361417249
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::hasId
	bool ___hasId_4;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::id_
	int32_t ___id__5;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::hasNormalizedX
	bool ___hasNormalizedX_7;
	// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::normalizedX_
	float ___normalizedX__8;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::hasNormalizedY
	bool ___hasNormalizedY_10;
	// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::normalizedY_
	float ___normalizedY__11;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_12;

public:
	inline static int32_t get_offset_of_hasId_4() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___hasId_4)); }
	inline bool get_hasId_4() const { return ___hasId_4; }
	inline bool* get_address_of_hasId_4() { return &___hasId_4; }
	inline void set_hasId_4(bool value)
	{
		___hasId_4 = value;
	}

	inline static int32_t get_offset_of_id__5() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___id__5)); }
	inline int32_t get_id__5() const { return ___id__5; }
	inline int32_t* get_address_of_id__5() { return &___id__5; }
	inline void set_id__5(int32_t value)
	{
		___id__5 = value;
	}

	inline static int32_t get_offset_of_hasNormalizedX_7() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___hasNormalizedX_7)); }
	inline bool get_hasNormalizedX_7() const { return ___hasNormalizedX_7; }
	inline bool* get_address_of_hasNormalizedX_7() { return &___hasNormalizedX_7; }
	inline void set_hasNormalizedX_7(bool value)
	{
		___hasNormalizedX_7 = value;
	}

	inline static int32_t get_offset_of_normalizedX__8() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___normalizedX__8)); }
	inline float get_normalizedX__8() const { return ___normalizedX__8; }
	inline float* get_address_of_normalizedX__8() { return &___normalizedX__8; }
	inline void set_normalizedX__8(float value)
	{
		___normalizedX__8 = value;
	}

	inline static int32_t get_offset_of_hasNormalizedY_10() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___hasNormalizedY_10)); }
	inline bool get_hasNormalizedY_10() const { return ___hasNormalizedY_10; }
	inline bool* get_address_of_hasNormalizedY_10() { return &___hasNormalizedY_10; }
	inline void set_hasNormalizedY_10(bool value)
	{
		___hasNormalizedY_10 = value;
	}

	inline static int32_t get_offset_of_normalizedY__11() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___normalizedY__11)); }
	inline float get_normalizedY__11() const { return ___normalizedY__11; }
	inline float* get_address_of_normalizedY__11() { return &___normalizedY__11; }
	inline void set_normalizedY__11(float value)
	{
		___normalizedY__11 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_12() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___memoizedSerializedSize_12)); }
	inline int32_t get_memoizedSerializedSize_12() const { return ___memoizedSerializedSize_12; }
	inline int32_t* get_address_of_memoizedSerializedSize_12() { return &___memoizedSerializedSize_12; }
	inline void set_memoizedSerializedSize_12(int32_t value)
	{
		___memoizedSerializedSize_12 = value;
	}
};

struct Pointer_t4145025558_StaticFields
{
public:
	// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::defaultInstance
	Pointer_t4145025558 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/MotionEvent/Types/Pointer::_pointerFieldNames
	StringU5BU5D_t1281789340* ____pointerFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/MotionEvent/Types/Pointer::_pointerFieldTags
	UInt32U5BU5D_t2770800703* ____pointerFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(Pointer_t4145025558_StaticFields, ___defaultInstance_0)); }
	inline Pointer_t4145025558 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline Pointer_t4145025558 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(Pointer_t4145025558 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__pointerFieldNames_1() { return static_cast<int32_t>(offsetof(Pointer_t4145025558_StaticFields, ____pointerFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__pointerFieldNames_1() const { return ____pointerFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__pointerFieldNames_1() { return &____pointerFieldNames_1; }
	inline void set__pointerFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____pointerFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____pointerFieldNames_1), value);
	}

	inline static int32_t get_offset_of__pointerFieldTags_2() { return static_cast<int32_t>(offsetof(Pointer_t4145025558_StaticFields, ____pointerFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__pointerFieldTags_2() const { return ____pointerFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__pointerFieldTags_2() { return &____pointerFieldTags_2; }
	inline void set__pointerFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____pointerFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____pointerFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTER_T4145025558_H
#ifndef BUILDER_T582111845_H
#define BUILDER_T582111845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder
struct  Builder_t582111845  : public GeneratedBuilderLite_2_t751282667
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::result
	Pointer_t4145025558 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t582111845, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t582111845, ___result_1)); }
	inline Pointer_t4145025558 * get_result_1() const { return ___result_1; }
	inline Pointer_t4145025558 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(Pointer_t4145025558 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T582111845_H
#ifndef ORIENTATIONEVENT_T158247624_H
#define ORIENTATIONEVENT_T158247624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/OrientationEvent
struct  OrientationEvent_t158247624  : public GeneratedMessageLite_2_t3752983017
{
public:
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/OrientationEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::z_
	float ___z__14;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasW
	bool ___hasW_16;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::w_
	float ___w__17;
	// System.Int32 proto.PhoneEvent/Types/OrientationEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_18;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_hasW_16() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasW_16)); }
	inline bool get_hasW_16() const { return ___hasW_16; }
	inline bool* get_address_of_hasW_16() { return &___hasW_16; }
	inline void set_hasW_16(bool value)
	{
		___hasW_16 = value;
	}

	inline static int32_t get_offset_of_w__17() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___w__17)); }
	inline float get_w__17() const { return ___w__17; }
	inline float* get_address_of_w__17() { return &___w__17; }
	inline void set_w__17(float value)
	{
		___w__17 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_18() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___memoizedSerializedSize_18)); }
	inline int32_t get_memoizedSerializedSize_18() const { return ___memoizedSerializedSize_18; }
	inline int32_t* get_address_of_memoizedSerializedSize_18() { return &___memoizedSerializedSize_18; }
	inline void set_memoizedSerializedSize_18(int32_t value)
	{
		___memoizedSerializedSize_18 = value;
	}
};

struct OrientationEvent_t158247624_StaticFields
{
public:
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::defaultInstance
	OrientationEvent_t158247624 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/OrientationEvent::_orientationEventFieldNames
	StringU5BU5D_t1281789340* ____orientationEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/OrientationEvent::_orientationEventFieldTags
	UInt32U5BU5D_t2770800703* ____orientationEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624_StaticFields, ___defaultInstance_0)); }
	inline OrientationEvent_t158247624 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline OrientationEvent_t158247624 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(OrientationEvent_t158247624 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__orientationEventFieldNames_1() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624_StaticFields, ____orientationEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__orientationEventFieldNames_1() const { return ____orientationEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__orientationEventFieldNames_1() { return &____orientationEventFieldNames_1; }
	inline void set__orientationEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____orientationEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____orientationEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__orientationEventFieldTags_2() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624_StaticFields, ____orientationEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__orientationEventFieldTags_2() const { return ____orientationEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__orientationEventFieldTags_2() { return &____orientationEventFieldTags_2; }
	inline void set__orientationEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____orientationEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____orientationEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONEVENT_T158247624_H
#ifndef BUILDER_T2279437287_H
#define BUILDER_T2279437287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/OrientationEvent/Builder
struct  Builder_t2279437287  : public GeneratedBuilderLite_2_t3142848435
{
public:
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::result
	OrientationEvent_t158247624 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t2279437287, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t2279437287, ___result_1)); }
	inline OrientationEvent_t158247624 * get_result_1() const { return ___result_1; }
	inline OrientationEvent_t158247624 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(OrientationEvent_t158247624 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2279437287_H
#ifndef TYPE_T1244512943_H
#define TYPE_T1244512943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/Type
struct  Type_t1244512943 
{
public:
	// System.Int32 proto.PhoneEvent/Types/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1244512943, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1244512943_H
#ifndef CONTROLLERSTATE_T3401244786_H
#define CONTROLLERSTATE_T3401244786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.ControllerState
struct  ControllerState_t3401244786  : public RuntimeObject
{
public:
	// GvrConnectionState Gvr.Internal.ControllerState::connectionState
	int32_t ___connectionState_0;
	// GvrControllerApiStatus Gvr.Internal.ControllerState::apiStatus
	int32_t ___apiStatus_1;
	// UnityEngine.Quaternion Gvr.Internal.ControllerState::orientation
	Quaternion_t2301928331  ___orientation_2;
	// UnityEngine.Vector3 Gvr.Internal.ControllerState::position
	Vector3_t3722313464  ___position_3;
	// UnityEngine.Vector3 Gvr.Internal.ControllerState::gyro
	Vector3_t3722313464  ___gyro_4;
	// UnityEngine.Vector3 Gvr.Internal.ControllerState::accel
	Vector3_t3722313464  ___accel_5;
	// UnityEngine.Vector2 Gvr.Internal.ControllerState::touchPos
	Vector2_t2156229523  ___touchPos_6;
	// System.Boolean Gvr.Internal.ControllerState::recentered
	bool ___recentered_7;
	// GvrControllerButton Gvr.Internal.ControllerState::buttonsState
	int32_t ___buttonsState_8;
	// GvrControllerButton Gvr.Internal.ControllerState::buttonsDown
	int32_t ___buttonsDown_9;
	// GvrControllerButton Gvr.Internal.ControllerState::buttonsUp
	int32_t ___buttonsUp_10;
	// System.String Gvr.Internal.ControllerState::errorDetails
	String_t* ___errorDetails_11;
	// System.IntPtr Gvr.Internal.ControllerState::gvrPtr
	intptr_t ___gvrPtr_12;
	// System.Boolean Gvr.Internal.ControllerState::isCharging
	bool ___isCharging_13;
	// GvrControllerBatteryLevel Gvr.Internal.ControllerState::batteryLevel
	int32_t ___batteryLevel_14;

public:
	inline static int32_t get_offset_of_connectionState_0() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___connectionState_0)); }
	inline int32_t get_connectionState_0() const { return ___connectionState_0; }
	inline int32_t* get_address_of_connectionState_0() { return &___connectionState_0; }
	inline void set_connectionState_0(int32_t value)
	{
		___connectionState_0 = value;
	}

	inline static int32_t get_offset_of_apiStatus_1() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___apiStatus_1)); }
	inline int32_t get_apiStatus_1() const { return ___apiStatus_1; }
	inline int32_t* get_address_of_apiStatus_1() { return &___apiStatus_1; }
	inline void set_apiStatus_1(int32_t value)
	{
		___apiStatus_1 = value;
	}

	inline static int32_t get_offset_of_orientation_2() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___orientation_2)); }
	inline Quaternion_t2301928331  get_orientation_2() const { return ___orientation_2; }
	inline Quaternion_t2301928331 * get_address_of_orientation_2() { return &___orientation_2; }
	inline void set_orientation_2(Quaternion_t2301928331  value)
	{
		___orientation_2 = value;
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___position_3)); }
	inline Vector3_t3722313464  get_position_3() const { return ___position_3; }
	inline Vector3_t3722313464 * get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(Vector3_t3722313464  value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_gyro_4() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___gyro_4)); }
	inline Vector3_t3722313464  get_gyro_4() const { return ___gyro_4; }
	inline Vector3_t3722313464 * get_address_of_gyro_4() { return &___gyro_4; }
	inline void set_gyro_4(Vector3_t3722313464  value)
	{
		___gyro_4 = value;
	}

	inline static int32_t get_offset_of_accel_5() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___accel_5)); }
	inline Vector3_t3722313464  get_accel_5() const { return ___accel_5; }
	inline Vector3_t3722313464 * get_address_of_accel_5() { return &___accel_5; }
	inline void set_accel_5(Vector3_t3722313464  value)
	{
		___accel_5 = value;
	}

	inline static int32_t get_offset_of_touchPos_6() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___touchPos_6)); }
	inline Vector2_t2156229523  get_touchPos_6() const { return ___touchPos_6; }
	inline Vector2_t2156229523 * get_address_of_touchPos_6() { return &___touchPos_6; }
	inline void set_touchPos_6(Vector2_t2156229523  value)
	{
		___touchPos_6 = value;
	}

	inline static int32_t get_offset_of_recentered_7() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___recentered_7)); }
	inline bool get_recentered_7() const { return ___recentered_7; }
	inline bool* get_address_of_recentered_7() { return &___recentered_7; }
	inline void set_recentered_7(bool value)
	{
		___recentered_7 = value;
	}

	inline static int32_t get_offset_of_buttonsState_8() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___buttonsState_8)); }
	inline int32_t get_buttonsState_8() const { return ___buttonsState_8; }
	inline int32_t* get_address_of_buttonsState_8() { return &___buttonsState_8; }
	inline void set_buttonsState_8(int32_t value)
	{
		___buttonsState_8 = value;
	}

	inline static int32_t get_offset_of_buttonsDown_9() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___buttonsDown_9)); }
	inline int32_t get_buttonsDown_9() const { return ___buttonsDown_9; }
	inline int32_t* get_address_of_buttonsDown_9() { return &___buttonsDown_9; }
	inline void set_buttonsDown_9(int32_t value)
	{
		___buttonsDown_9 = value;
	}

	inline static int32_t get_offset_of_buttonsUp_10() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___buttonsUp_10)); }
	inline int32_t get_buttonsUp_10() const { return ___buttonsUp_10; }
	inline int32_t* get_address_of_buttonsUp_10() { return &___buttonsUp_10; }
	inline void set_buttonsUp_10(int32_t value)
	{
		___buttonsUp_10 = value;
	}

	inline static int32_t get_offset_of_errorDetails_11() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___errorDetails_11)); }
	inline String_t* get_errorDetails_11() const { return ___errorDetails_11; }
	inline String_t** get_address_of_errorDetails_11() { return &___errorDetails_11; }
	inline void set_errorDetails_11(String_t* value)
	{
		___errorDetails_11 = value;
		Il2CppCodeGenWriteBarrier((&___errorDetails_11), value);
	}

	inline static int32_t get_offset_of_gvrPtr_12() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___gvrPtr_12)); }
	inline intptr_t get_gvrPtr_12() const { return ___gvrPtr_12; }
	inline intptr_t* get_address_of_gvrPtr_12() { return &___gvrPtr_12; }
	inline void set_gvrPtr_12(intptr_t value)
	{
		___gvrPtr_12 = value;
	}

	inline static int32_t get_offset_of_isCharging_13() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___isCharging_13)); }
	inline bool get_isCharging_13() const { return ___isCharging_13; }
	inline bool* get_address_of_isCharging_13() { return &___isCharging_13; }
	inline void set_isCharging_13(bool value)
	{
		___isCharging_13 = value;
	}

	inline static int32_t get_offset_of_batteryLevel_14() { return static_cast<int32_t>(offsetof(ControllerState_t3401244786, ___batteryLevel_14)); }
	inline int32_t get_batteryLevel_14() const { return ___batteryLevel_14; }
	inline int32_t* get_address_of_batteryLevel_14() { return &___batteryLevel_14; }
	inline void set_batteryLevel_14(int32_t value)
	{
		___batteryLevel_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLERSTATE_T3401244786_H
#ifndef EMULATORBUTTONEVENT_T938855819_H
#define EMULATORBUTTONEVENT_T938855819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorButtonEvent
struct  EmulatorButtonEvent_t938855819 
{
public:
	// Gvr.Internal.EmulatorButtonEvent/ButtonCode Gvr.Internal.EmulatorButtonEvent::code
	int32_t ___code_0;
	// System.Boolean Gvr.Internal.EmulatorButtonEvent::down
	bool ___down_1;

public:
	inline static int32_t get_offset_of_code_0() { return static_cast<int32_t>(offsetof(EmulatorButtonEvent_t938855819, ___code_0)); }
	inline int32_t get_code_0() const { return ___code_0; }
	inline int32_t* get_address_of_code_0() { return &___code_0; }
	inline void set_code_0(int32_t value)
	{
		___code_0 = value;
	}

	inline static int32_t get_offset_of_down_1() { return static_cast<int32_t>(offsetof(EmulatorButtonEvent_t938855819, ___down_1)); }
	inline bool get_down_1() const { return ___down_1; }
	inline bool* get_address_of_down_1() { return &___down_1; }
	inline void set_down_1(bool value)
	{
		___down_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Gvr.Internal.EmulatorButtonEvent
struct EmulatorButtonEvent_t938855819_marshaled_pinvoke
{
	int32_t ___code_0;
	int32_t ___down_1;
};
// Native definition for COM marshalling of Gvr.Internal.EmulatorButtonEvent
struct EmulatorButtonEvent_t938855819_marshaled_com
{
	int32_t ___code_0;
	int32_t ___down_1;
};
#endif // EMULATORBUTTONEVENT_T938855819_H
#ifndef HEADSETSTATE_T378905490_H
#define HEADSETSTATE_T378905490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.HeadsetState
struct  HeadsetState_t378905490 
{
public:
	// GvrEventType Gvr.Internal.HeadsetState::eventType
	int32_t ___eventType_0;
	// System.Int32 Gvr.Internal.HeadsetState::eventFlags
	int32_t ___eventFlags_1;
	// System.Int64 Gvr.Internal.HeadsetState::eventTimestampNs
	int64_t ___eventTimestampNs_2;
	// GvrRecenterEventType Gvr.Internal.HeadsetState::recenterEventType
	int32_t ___recenterEventType_3;
	// System.UInt32 Gvr.Internal.HeadsetState::recenterEventFlags
	uint32_t ___recenterEventFlags_4;
	// UnityEngine.Vector3 Gvr.Internal.HeadsetState::recenteredPosition
	Vector3_t3722313464  ___recenteredPosition_5;
	// UnityEngine.Quaternion Gvr.Internal.HeadsetState::recenteredRotation
	Quaternion_t2301928331  ___recenteredRotation_6;

public:
	inline static int32_t get_offset_of_eventType_0() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventType_0)); }
	inline int32_t get_eventType_0() const { return ___eventType_0; }
	inline int32_t* get_address_of_eventType_0() { return &___eventType_0; }
	inline void set_eventType_0(int32_t value)
	{
		___eventType_0 = value;
	}

	inline static int32_t get_offset_of_eventFlags_1() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventFlags_1)); }
	inline int32_t get_eventFlags_1() const { return ___eventFlags_1; }
	inline int32_t* get_address_of_eventFlags_1() { return &___eventFlags_1; }
	inline void set_eventFlags_1(int32_t value)
	{
		___eventFlags_1 = value;
	}

	inline static int32_t get_offset_of_eventTimestampNs_2() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventTimestampNs_2)); }
	inline int64_t get_eventTimestampNs_2() const { return ___eventTimestampNs_2; }
	inline int64_t* get_address_of_eventTimestampNs_2() { return &___eventTimestampNs_2; }
	inline void set_eventTimestampNs_2(int64_t value)
	{
		___eventTimestampNs_2 = value;
	}

	inline static int32_t get_offset_of_recenterEventType_3() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenterEventType_3)); }
	inline int32_t get_recenterEventType_3() const { return ___recenterEventType_3; }
	inline int32_t* get_address_of_recenterEventType_3() { return &___recenterEventType_3; }
	inline void set_recenterEventType_3(int32_t value)
	{
		___recenterEventType_3 = value;
	}

	inline static int32_t get_offset_of_recenterEventFlags_4() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenterEventFlags_4)); }
	inline uint32_t get_recenterEventFlags_4() const { return ___recenterEventFlags_4; }
	inline uint32_t* get_address_of_recenterEventFlags_4() { return &___recenterEventFlags_4; }
	inline void set_recenterEventFlags_4(uint32_t value)
	{
		___recenterEventFlags_4 = value;
	}

	inline static int32_t get_offset_of_recenteredPosition_5() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenteredPosition_5)); }
	inline Vector3_t3722313464  get_recenteredPosition_5() const { return ___recenteredPosition_5; }
	inline Vector3_t3722313464 * get_address_of_recenteredPosition_5() { return &___recenteredPosition_5; }
	inline void set_recenteredPosition_5(Vector3_t3722313464  value)
	{
		___recenteredPosition_5 = value;
	}

	inline static int32_t get_offset_of_recenteredRotation_6() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenteredRotation_6)); }
	inline Quaternion_t2301928331  get_recenteredRotation_6() const { return ___recenteredRotation_6; }
	inline Quaternion_t2301928331 * get_address_of_recenteredRotation_6() { return &___recenteredRotation_6; }
	inline void set_recenteredRotation_6(Quaternion_t2301928331  value)
	{
		___recenteredRotation_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETSTATE_T378905490_H
#ifndef MOUSECONTROLLERPROVIDER_T1264821694_H
#define MOUSECONTROLLERPROVIDER_T1264821694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.MouseControllerProvider
struct  MouseControllerProvider_t1264821694  : public RuntimeObject
{
public:
	// Gvr.Internal.ControllerState Gvr.Internal.MouseControllerProvider::state
	ControllerState_t3401244786 * ___state_2;
	// UnityEngine.Vector2 Gvr.Internal.MouseControllerProvider::mouseDelta
	Vector2_t2156229523  ___mouseDelta_3;
	// System.Boolean Gvr.Internal.MouseControllerProvider::wasTouching
	bool ___wasTouching_4;
	// GvrControllerButton Gvr.Internal.MouseControllerProvider::lastButtonsState
	int32_t ___lastButtonsState_5;

public:
	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t1264821694, ___state_2)); }
	inline ControllerState_t3401244786 * get_state_2() const { return ___state_2; }
	inline ControllerState_t3401244786 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(ControllerState_t3401244786 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}

	inline static int32_t get_offset_of_mouseDelta_3() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t1264821694, ___mouseDelta_3)); }
	inline Vector2_t2156229523  get_mouseDelta_3() const { return ___mouseDelta_3; }
	inline Vector2_t2156229523 * get_address_of_mouseDelta_3() { return &___mouseDelta_3; }
	inline void set_mouseDelta_3(Vector2_t2156229523  value)
	{
		___mouseDelta_3 = value;
	}

	inline static int32_t get_offset_of_wasTouching_4() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t1264821694, ___wasTouching_4)); }
	inline bool get_wasTouching_4() const { return ___wasTouching_4; }
	inline bool* get_address_of_wasTouching_4() { return &___wasTouching_4; }
	inline void set_wasTouching_4(bool value)
	{
		___wasTouching_4 = value;
	}

	inline static int32_t get_offset_of_lastButtonsState_5() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t1264821694, ___lastButtonsState_5)); }
	inline int32_t get_lastButtonsState_5() const { return ___lastButtonsState_5; }
	inline int32_t* get_address_of_lastButtonsState_5() { return &___lastButtonsState_5; }
	inline void set_lastButtonsState_5(int32_t value)
	{
		___lastButtonsState_5 = value;
	}
};

struct MouseControllerProvider_t1264821694_StaticFields
{
public:
	// UnityEngine.Vector3 Gvr.Internal.MouseControllerProvider::INVERT_Y
	Vector3_t3722313464  ___INVERT_Y_8;

public:
	inline static int32_t get_offset_of_INVERT_Y_8() { return static_cast<int32_t>(offsetof(MouseControllerProvider_t1264821694_StaticFields, ___INVERT_Y_8)); }
	inline Vector3_t3722313464  get_INVERT_Y_8() const { return ___INVERT_Y_8; }
	inline Vector3_t3722313464 * get_address_of_INVERT_Y_8() { return &___INVERT_Y_8; }
	inline void set_INVERT_Y_8(Vector3_t3722313464  value)
	{
		___INVERT_Y_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOUSECONTROLLERPROVIDER_T1264821694_H
#ifndef POINTERRAY_T3451016640_H
#define POINTERRAY_T3451016640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer/PointerRay
struct  PointerRay_t3451016640 
{
public:
	// UnityEngine.Ray GvrBasePointer/PointerRay::ray
	Ray_t3785851493  ___ray_0;
	// System.Single GvrBasePointer/PointerRay::distanceFromStart
	float ___distanceFromStart_1;
	// System.Single GvrBasePointer/PointerRay::distance
	float ___distance_2;

public:
	inline static int32_t get_offset_of_ray_0() { return static_cast<int32_t>(offsetof(PointerRay_t3451016640, ___ray_0)); }
	inline Ray_t3785851493  get_ray_0() const { return ___ray_0; }
	inline Ray_t3785851493 * get_address_of_ray_0() { return &___ray_0; }
	inline void set_ray_0(Ray_t3785851493  value)
	{
		___ray_0 = value;
	}

	inline static int32_t get_offset_of_distanceFromStart_1() { return static_cast<int32_t>(offsetof(PointerRay_t3451016640, ___distanceFromStart_1)); }
	inline float get_distanceFromStart_1() const { return ___distanceFromStart_1; }
	inline float* get_address_of_distanceFromStart_1() { return &___distanceFromStart_1; }
	inline void set_distanceFromStart_1(float value)
	{
		___distanceFromStart_1 = value;
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(PointerRay_t3451016640, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTERRAY_T3451016640_H
#ifndef CONTROLLERDISPLAYSTATE_T972313457_H
#define CONTROLLERDISPLAYSTATE_T972313457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerVisual/ControllerDisplayState
struct  ControllerDisplayState_t972313457 
{
public:
	// GvrControllerBatteryLevel GvrControllerVisual/ControllerDisplayState::batteryLevel
	int32_t ___batteryLevel_0;
	// System.Boolean GvrControllerVisual/ControllerDisplayState::batteryCharging
	bool ___batteryCharging_1;
	// System.Boolean GvrControllerVisual/ControllerDisplayState::clickButton
	bool ___clickButton_2;
	// System.Boolean GvrControllerVisual/ControllerDisplayState::appButton
	bool ___appButton_3;
	// System.Boolean GvrControllerVisual/ControllerDisplayState::homeButton
	bool ___homeButton_4;
	// System.Boolean GvrControllerVisual/ControllerDisplayState::touching
	bool ___touching_5;
	// UnityEngine.Vector2 GvrControllerVisual/ControllerDisplayState::touchPos
	Vector2_t2156229523  ___touchPos_6;

public:
	inline static int32_t get_offset_of_batteryLevel_0() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t972313457, ___batteryLevel_0)); }
	inline int32_t get_batteryLevel_0() const { return ___batteryLevel_0; }
	inline int32_t* get_address_of_batteryLevel_0() { return &___batteryLevel_0; }
	inline void set_batteryLevel_0(int32_t value)
	{
		___batteryLevel_0 = value;
	}

	inline static int32_t get_offset_of_batteryCharging_1() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t972313457, ___batteryCharging_1)); }
	inline bool get_batteryCharging_1() const { return ___batteryCharging_1; }
	inline bool* get_address_of_batteryCharging_1() { return &___batteryCharging_1; }
	inline void set_batteryCharging_1(bool value)
	{
		___batteryCharging_1 = value;
	}

	inline static int32_t get_offset_of_clickButton_2() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t972313457, ___clickButton_2)); }
	inline bool get_clickButton_2() const { return ___clickButton_2; }
	inline bool* get_address_of_clickButton_2() { return &___clickButton_2; }
	inline void set_clickButton_2(bool value)
	{
		___clickButton_2 = value;
	}

	inline static int32_t get_offset_of_appButton_3() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t972313457, ___appButton_3)); }
	inline bool get_appButton_3() const { return ___appButton_3; }
	inline bool* get_address_of_appButton_3() { return &___appButton_3; }
	inline void set_appButton_3(bool value)
	{
		___appButton_3 = value;
	}

	inline static int32_t get_offset_of_homeButton_4() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t972313457, ___homeButton_4)); }
	inline bool get_homeButton_4() const { return ___homeButton_4; }
	inline bool* get_address_of_homeButton_4() { return &___homeButton_4; }
	inline void set_homeButton_4(bool value)
	{
		___homeButton_4 = value;
	}

	inline static int32_t get_offset_of_touching_5() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t972313457, ___touching_5)); }
	inline bool get_touching_5() const { return ___touching_5; }
	inline bool* get_address_of_touching_5() { return &___touching_5; }
	inline void set_touching_5(bool value)
	{
		___touching_5 = value;
	}

	inline static int32_t get_offset_of_touchPos_6() { return static_cast<int32_t>(offsetof(ControllerDisplayState_t972313457, ___touchPos_6)); }
	inline Vector2_t2156229523  get_touchPos_6() const { return ___touchPos_6; }
	inline Vector2_t2156229523 * get_address_of_touchPos_6() { return &___touchPos_6; }
	inline void set_touchPos_6(Vector2_t2156229523  value)
	{
		___touchPos_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of GvrControllerVisual/ControllerDisplayState
struct ControllerDisplayState_t972313457_marshaled_pinvoke
{
	int32_t ___batteryLevel_0;
	int32_t ___batteryCharging_1;
	int32_t ___clickButton_2;
	int32_t ___appButton_3;
	int32_t ___homeButton_4;
	int32_t ___touching_5;
	Vector2_t2156229523  ___touchPos_6;
};
// Native definition for COM marshalling of GvrControllerVisual/ControllerDisplayState
struct ControllerDisplayState_t972313457_marshaled_com
{
	int32_t ___batteryLevel_0;
	int32_t ___batteryCharging_1;
	int32_t ___clickButton_2;
	int32_t ___appButton_3;
	int32_t ___homeButton_4;
	int32_t ___touching_5;
	Vector2_t2156229523  ___touchPos_6;
};
#endif // CONTROLLERDISPLAYSTATE_T972313457_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef PHONEEVENT_T1358418708_H
#define PHONEEVENT_T1358418708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent
struct  PhoneEvent_t1358418708  : public GeneratedMessageLite_2_t893745300
{
public:
	// System.Boolean proto.PhoneEvent::hasType
	bool ___hasType_4;
	// proto.PhoneEvent/Types/Type proto.PhoneEvent::type_
	int32_t ___type__5;
	// System.Boolean proto.PhoneEvent::hasMotionEvent
	bool ___hasMotionEvent_7;
	// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent::motionEvent_
	MotionEvent_t3121383016 * ___motionEvent__8;
	// System.Boolean proto.PhoneEvent::hasGyroscopeEvent
	bool ___hasGyroscopeEvent_10;
	// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent::gyroscopeEvent_
	GyroscopeEvent_t127774954 * ___gyroscopeEvent__11;
	// System.Boolean proto.PhoneEvent::hasAccelerometerEvent
	bool ___hasAccelerometerEvent_13;
	// proto.PhoneEvent/Types/AccelerometerEvent proto.PhoneEvent::accelerometerEvent_
	AccelerometerEvent_t1394922645 * ___accelerometerEvent__14;
	// System.Boolean proto.PhoneEvent::hasDepthMapEvent
	bool ___hasDepthMapEvent_16;
	// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent::depthMapEvent_
	DepthMapEvent_t729886054 * ___depthMapEvent__17;
	// System.Boolean proto.PhoneEvent::hasOrientationEvent
	bool ___hasOrientationEvent_19;
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent::orientationEvent_
	OrientationEvent_t158247624 * ___orientationEvent__20;
	// System.Boolean proto.PhoneEvent::hasKeyEvent
	bool ___hasKeyEvent_22;
	// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent::keyEvent_
	KeyEvent_t1937114521 * ___keyEvent__23;
	// System.Int32 proto.PhoneEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_24;

public:
	inline static int32_t get_offset_of_hasType_4() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___hasType_4)); }
	inline bool get_hasType_4() const { return ___hasType_4; }
	inline bool* get_address_of_hasType_4() { return &___hasType_4; }
	inline void set_hasType_4(bool value)
	{
		___hasType_4 = value;
	}

	inline static int32_t get_offset_of_type__5() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___type__5)); }
	inline int32_t get_type__5() const { return ___type__5; }
	inline int32_t* get_address_of_type__5() { return &___type__5; }
	inline void set_type__5(int32_t value)
	{
		___type__5 = value;
	}

	inline static int32_t get_offset_of_hasMotionEvent_7() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___hasMotionEvent_7)); }
	inline bool get_hasMotionEvent_7() const { return ___hasMotionEvent_7; }
	inline bool* get_address_of_hasMotionEvent_7() { return &___hasMotionEvent_7; }
	inline void set_hasMotionEvent_7(bool value)
	{
		___hasMotionEvent_7 = value;
	}

	inline static int32_t get_offset_of_motionEvent__8() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___motionEvent__8)); }
	inline MotionEvent_t3121383016 * get_motionEvent__8() const { return ___motionEvent__8; }
	inline MotionEvent_t3121383016 ** get_address_of_motionEvent__8() { return &___motionEvent__8; }
	inline void set_motionEvent__8(MotionEvent_t3121383016 * value)
	{
		___motionEvent__8 = value;
		Il2CppCodeGenWriteBarrier((&___motionEvent__8), value);
	}

	inline static int32_t get_offset_of_hasGyroscopeEvent_10() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___hasGyroscopeEvent_10)); }
	inline bool get_hasGyroscopeEvent_10() const { return ___hasGyroscopeEvent_10; }
	inline bool* get_address_of_hasGyroscopeEvent_10() { return &___hasGyroscopeEvent_10; }
	inline void set_hasGyroscopeEvent_10(bool value)
	{
		___hasGyroscopeEvent_10 = value;
	}

	inline static int32_t get_offset_of_gyroscopeEvent__11() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___gyroscopeEvent__11)); }
	inline GyroscopeEvent_t127774954 * get_gyroscopeEvent__11() const { return ___gyroscopeEvent__11; }
	inline GyroscopeEvent_t127774954 ** get_address_of_gyroscopeEvent__11() { return &___gyroscopeEvent__11; }
	inline void set_gyroscopeEvent__11(GyroscopeEvent_t127774954 * value)
	{
		___gyroscopeEvent__11 = value;
		Il2CppCodeGenWriteBarrier((&___gyroscopeEvent__11), value);
	}

	inline static int32_t get_offset_of_hasAccelerometerEvent_13() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___hasAccelerometerEvent_13)); }
	inline bool get_hasAccelerometerEvent_13() const { return ___hasAccelerometerEvent_13; }
	inline bool* get_address_of_hasAccelerometerEvent_13() { return &___hasAccelerometerEvent_13; }
	inline void set_hasAccelerometerEvent_13(bool value)
	{
		___hasAccelerometerEvent_13 = value;
	}

	inline static int32_t get_offset_of_accelerometerEvent__14() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___accelerometerEvent__14)); }
	inline AccelerometerEvent_t1394922645 * get_accelerometerEvent__14() const { return ___accelerometerEvent__14; }
	inline AccelerometerEvent_t1394922645 ** get_address_of_accelerometerEvent__14() { return &___accelerometerEvent__14; }
	inline void set_accelerometerEvent__14(AccelerometerEvent_t1394922645 * value)
	{
		___accelerometerEvent__14 = value;
		Il2CppCodeGenWriteBarrier((&___accelerometerEvent__14), value);
	}

	inline static int32_t get_offset_of_hasDepthMapEvent_16() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___hasDepthMapEvent_16)); }
	inline bool get_hasDepthMapEvent_16() const { return ___hasDepthMapEvent_16; }
	inline bool* get_address_of_hasDepthMapEvent_16() { return &___hasDepthMapEvent_16; }
	inline void set_hasDepthMapEvent_16(bool value)
	{
		___hasDepthMapEvent_16 = value;
	}

	inline static int32_t get_offset_of_depthMapEvent__17() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___depthMapEvent__17)); }
	inline DepthMapEvent_t729886054 * get_depthMapEvent__17() const { return ___depthMapEvent__17; }
	inline DepthMapEvent_t729886054 ** get_address_of_depthMapEvent__17() { return &___depthMapEvent__17; }
	inline void set_depthMapEvent__17(DepthMapEvent_t729886054 * value)
	{
		___depthMapEvent__17 = value;
		Il2CppCodeGenWriteBarrier((&___depthMapEvent__17), value);
	}

	inline static int32_t get_offset_of_hasOrientationEvent_19() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___hasOrientationEvent_19)); }
	inline bool get_hasOrientationEvent_19() const { return ___hasOrientationEvent_19; }
	inline bool* get_address_of_hasOrientationEvent_19() { return &___hasOrientationEvent_19; }
	inline void set_hasOrientationEvent_19(bool value)
	{
		___hasOrientationEvent_19 = value;
	}

	inline static int32_t get_offset_of_orientationEvent__20() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___orientationEvent__20)); }
	inline OrientationEvent_t158247624 * get_orientationEvent__20() const { return ___orientationEvent__20; }
	inline OrientationEvent_t158247624 ** get_address_of_orientationEvent__20() { return &___orientationEvent__20; }
	inline void set_orientationEvent__20(OrientationEvent_t158247624 * value)
	{
		___orientationEvent__20 = value;
		Il2CppCodeGenWriteBarrier((&___orientationEvent__20), value);
	}

	inline static int32_t get_offset_of_hasKeyEvent_22() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___hasKeyEvent_22)); }
	inline bool get_hasKeyEvent_22() const { return ___hasKeyEvent_22; }
	inline bool* get_address_of_hasKeyEvent_22() { return &___hasKeyEvent_22; }
	inline void set_hasKeyEvent_22(bool value)
	{
		___hasKeyEvent_22 = value;
	}

	inline static int32_t get_offset_of_keyEvent__23() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___keyEvent__23)); }
	inline KeyEvent_t1937114521 * get_keyEvent__23() const { return ___keyEvent__23; }
	inline KeyEvent_t1937114521 ** get_address_of_keyEvent__23() { return &___keyEvent__23; }
	inline void set_keyEvent__23(KeyEvent_t1937114521 * value)
	{
		___keyEvent__23 = value;
		Il2CppCodeGenWriteBarrier((&___keyEvent__23), value);
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_24() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708, ___memoizedSerializedSize_24)); }
	inline int32_t get_memoizedSerializedSize_24() const { return ___memoizedSerializedSize_24; }
	inline int32_t* get_address_of_memoizedSerializedSize_24() { return &___memoizedSerializedSize_24; }
	inline void set_memoizedSerializedSize_24(int32_t value)
	{
		___memoizedSerializedSize_24 = value;
	}
};

struct PhoneEvent_t1358418708_StaticFields
{
public:
	// proto.PhoneEvent proto.PhoneEvent::defaultInstance
	PhoneEvent_t1358418708 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent::_phoneEventFieldNames
	StringU5BU5D_t1281789340* ____phoneEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent::_phoneEventFieldTags
	UInt32U5BU5D_t2770800703* ____phoneEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708_StaticFields, ___defaultInstance_0)); }
	inline PhoneEvent_t1358418708 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline PhoneEvent_t1358418708 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(PhoneEvent_t1358418708 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__phoneEventFieldNames_1() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708_StaticFields, ____phoneEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__phoneEventFieldNames_1() const { return ____phoneEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__phoneEventFieldNames_1() { return &____phoneEventFieldNames_1; }
	inline void set__phoneEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____phoneEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____phoneEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__phoneEventFieldTags_2() { return static_cast<int32_t>(offsetof(PhoneEvent_t1358418708_StaticFields, ____phoneEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__phoneEventFieldTags_2() const { return ____phoneEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__phoneEventFieldTags_2() { return &____phoneEventFieldTags_2; }
	inline void set__phoneEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____phoneEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____phoneEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHONEEVENT_T1358418708_H
#ifndef ONSTATECHANGEDEVENT_T3148566047_H
#define ONSTATECHANGEDEVENT_T3148566047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerInput/OnStateChangedEvent
struct  OnStateChangedEvent_t3148566047  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTATECHANGEDEVENT_T3148566047_H
#ifndef EVENTDELEGATE_T481206256_H
#define EVENTDELEGATE_T481206256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventExecutor/EventDelegate
struct  EventDelegate_t481206256  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDELEGATE_T481206256_H
#ifndef ONRECENTEREVENT_T1755824842_H
#define ONRECENTEREVENT_T1755824842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset/OnRecenterEvent
struct  OnRecenterEvent_t1755824842  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONRECENTEREVENT_T1755824842_H
#ifndef ONSAFETYREGIONEVENT_T980662704_H
#define ONSAFETYREGIONEVENT_T980662704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset/OnSafetyRegionEvent
struct  OnSafetyRegionEvent_t980662704  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSAFETYREGIONEVENT_T980662704_H
#ifndef GETPOINTFORDISTANCEDELEGATE_T92913150_H
#define GETPOINTFORDISTANCEDELEGATE_T92913150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrLaserVisual/GetPointForDistanceDelegate
struct  GetPointForDistanceDelegate_t92913150  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETPOINTFORDISTANCEDELEGATE_T92913150_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef EMULATORCONFIG_T3196994574_H
#define EMULATORCONFIG_T3196994574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorConfig
struct  EmulatorConfig_t3196994574  : public MonoBehaviour_t3962482529
{
public:
	// Gvr.Internal.EmulatorConfig/Mode Gvr.Internal.EmulatorConfig::PHONE_EVENT_MODE
	int32_t ___PHONE_EVENT_MODE_5;

public:
	inline static int32_t get_offset_of_PHONE_EVENT_MODE_5() { return static_cast<int32_t>(offsetof(EmulatorConfig_t3196994574, ___PHONE_EVENT_MODE_5)); }
	inline int32_t get_PHONE_EVENT_MODE_5() const { return ___PHONE_EVENT_MODE_5; }
	inline int32_t* get_address_of_PHONE_EVENT_MODE_5() { return &___PHONE_EVENT_MODE_5; }
	inline void set_PHONE_EVENT_MODE_5(int32_t value)
	{
		___PHONE_EVENT_MODE_5 = value;
	}
};

struct EmulatorConfig_t3196994574_StaticFields
{
public:
	// Gvr.Internal.EmulatorConfig Gvr.Internal.EmulatorConfig::instance
	EmulatorConfig_t3196994574 * ___instance_4;
	// System.String Gvr.Internal.EmulatorConfig::USB_SERVER_IP
	String_t* ___USB_SERVER_IP_6;
	// System.String Gvr.Internal.EmulatorConfig::WIFI_SERVER_IP
	String_t* ___WIFI_SERVER_IP_7;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(EmulatorConfig_t3196994574_StaticFields, ___instance_4)); }
	inline EmulatorConfig_t3196994574 * get_instance_4() const { return ___instance_4; }
	inline EmulatorConfig_t3196994574 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(EmulatorConfig_t3196994574 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_USB_SERVER_IP_6() { return static_cast<int32_t>(offsetof(EmulatorConfig_t3196994574_StaticFields, ___USB_SERVER_IP_6)); }
	inline String_t* get_USB_SERVER_IP_6() const { return ___USB_SERVER_IP_6; }
	inline String_t** get_address_of_USB_SERVER_IP_6() { return &___USB_SERVER_IP_6; }
	inline void set_USB_SERVER_IP_6(String_t* value)
	{
		___USB_SERVER_IP_6 = value;
		Il2CppCodeGenWriteBarrier((&___USB_SERVER_IP_6), value);
	}

	inline static int32_t get_offset_of_WIFI_SERVER_IP_7() { return static_cast<int32_t>(offsetof(EmulatorConfig_t3196994574_StaticFields, ___WIFI_SERVER_IP_7)); }
	inline String_t* get_WIFI_SERVER_IP_7() const { return ___WIFI_SERVER_IP_7; }
	inline String_t** get_address_of_WIFI_SERVER_IP_7() { return &___WIFI_SERVER_IP_7; }
	inline void set_WIFI_SERVER_IP_7(String_t* value)
	{
		___WIFI_SERVER_IP_7 = value;
		Il2CppCodeGenWriteBarrier((&___WIFI_SERVER_IP_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORCONFIG_T3196994574_H
#ifndef GVRALLEVENTSTRIGGER_T2174159095_H
#define GVRALLEVENTSTRIGGER_T2174159095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAllEventsTrigger
struct  GvrAllEventsTrigger_t2174159095  : public MonoBehaviour_t3962482529
{
public:
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerClick
	TriggerEvent_t1225966107 * ___OnPointerClick_4;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerDown
	TriggerEvent_t1225966107 * ___OnPointerDown_5;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerUp
	TriggerEvent_t1225966107 * ___OnPointerUp_6;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerEnter
	TriggerEvent_t1225966107 * ___OnPointerEnter_7;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnPointerExit
	TriggerEvent_t1225966107 * ___OnPointerExit_8;
	// GvrAllEventsTrigger/TriggerEvent GvrAllEventsTrigger::OnScroll
	TriggerEvent_t1225966107 * ___OnScroll_9;
	// System.Boolean GvrAllEventsTrigger::listenersAdded
	bool ___listenersAdded_10;

public:
	inline static int32_t get_offset_of_OnPointerClick_4() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerClick_4)); }
	inline TriggerEvent_t1225966107 * get_OnPointerClick_4() const { return ___OnPointerClick_4; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerClick_4() { return &___OnPointerClick_4; }
	inline void set_OnPointerClick_4(TriggerEvent_t1225966107 * value)
	{
		___OnPointerClick_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerClick_4), value);
	}

	inline static int32_t get_offset_of_OnPointerDown_5() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerDown_5)); }
	inline TriggerEvent_t1225966107 * get_OnPointerDown_5() const { return ___OnPointerDown_5; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerDown_5() { return &___OnPointerDown_5; }
	inline void set_OnPointerDown_5(TriggerEvent_t1225966107 * value)
	{
		___OnPointerDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerDown_5), value);
	}

	inline static int32_t get_offset_of_OnPointerUp_6() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerUp_6)); }
	inline TriggerEvent_t1225966107 * get_OnPointerUp_6() const { return ___OnPointerUp_6; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerUp_6() { return &___OnPointerUp_6; }
	inline void set_OnPointerUp_6(TriggerEvent_t1225966107 * value)
	{
		___OnPointerUp_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerUp_6), value);
	}

	inline static int32_t get_offset_of_OnPointerEnter_7() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerEnter_7)); }
	inline TriggerEvent_t1225966107 * get_OnPointerEnter_7() const { return ___OnPointerEnter_7; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerEnter_7() { return &___OnPointerEnter_7; }
	inline void set_OnPointerEnter_7(TriggerEvent_t1225966107 * value)
	{
		___OnPointerEnter_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerEnter_7), value);
	}

	inline static int32_t get_offset_of_OnPointerExit_8() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnPointerExit_8)); }
	inline TriggerEvent_t1225966107 * get_OnPointerExit_8() const { return ___OnPointerExit_8; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnPointerExit_8() { return &___OnPointerExit_8; }
	inline void set_OnPointerExit_8(TriggerEvent_t1225966107 * value)
	{
		___OnPointerExit_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnPointerExit_8), value);
	}

	inline static int32_t get_offset_of_OnScroll_9() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___OnScroll_9)); }
	inline TriggerEvent_t1225966107 * get_OnScroll_9() const { return ___OnScroll_9; }
	inline TriggerEvent_t1225966107 ** get_address_of_OnScroll_9() { return &___OnScroll_9; }
	inline void set_OnScroll_9(TriggerEvent_t1225966107 * value)
	{
		___OnScroll_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnScroll_9), value);
	}

	inline static int32_t get_offset_of_listenersAdded_10() { return static_cast<int32_t>(offsetof(GvrAllEventsTrigger_t2174159095, ___listenersAdded_10)); }
	inline bool get_listenersAdded_10() const { return ___listenersAdded_10; }
	inline bool* get_address_of_listenersAdded_10() { return &___listenersAdded_10; }
	inline void set_listenersAdded_10(bool value)
	{
		___listenersAdded_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRALLEVENTSTRIGGER_T2174159095_H
#ifndef GVRAUDIOROOM_T1033997160_H
#define GVRAUDIOROOM_T1033997160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioRoom
struct  GvrAudioRoom_t1033997160  : public MonoBehaviour_t3962482529
{
public:
	// GvrAudioRoom/SurfaceMaterial GvrAudioRoom::leftWall
	int32_t ___leftWall_4;
	// GvrAudioRoom/SurfaceMaterial GvrAudioRoom::rightWall
	int32_t ___rightWall_5;
	// GvrAudioRoom/SurfaceMaterial GvrAudioRoom::floor
	int32_t ___floor_6;
	// GvrAudioRoom/SurfaceMaterial GvrAudioRoom::ceiling
	int32_t ___ceiling_7;
	// GvrAudioRoom/SurfaceMaterial GvrAudioRoom::backWall
	int32_t ___backWall_8;
	// GvrAudioRoom/SurfaceMaterial GvrAudioRoom::frontWall
	int32_t ___frontWall_9;
	// System.Single GvrAudioRoom::reflectivity
	float ___reflectivity_10;
	// System.Single GvrAudioRoom::reverbGainDb
	float ___reverbGainDb_11;
	// System.Single GvrAudioRoom::reverbBrightness
	float ___reverbBrightness_12;
	// System.Single GvrAudioRoom::reverbTime
	float ___reverbTime_13;
	// UnityEngine.Vector3 GvrAudioRoom::size
	Vector3_t3722313464  ___size_14;

public:
	inline static int32_t get_offset_of_leftWall_4() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___leftWall_4)); }
	inline int32_t get_leftWall_4() const { return ___leftWall_4; }
	inline int32_t* get_address_of_leftWall_4() { return &___leftWall_4; }
	inline void set_leftWall_4(int32_t value)
	{
		___leftWall_4 = value;
	}

	inline static int32_t get_offset_of_rightWall_5() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___rightWall_5)); }
	inline int32_t get_rightWall_5() const { return ___rightWall_5; }
	inline int32_t* get_address_of_rightWall_5() { return &___rightWall_5; }
	inline void set_rightWall_5(int32_t value)
	{
		___rightWall_5 = value;
	}

	inline static int32_t get_offset_of_floor_6() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___floor_6)); }
	inline int32_t get_floor_6() const { return ___floor_6; }
	inline int32_t* get_address_of_floor_6() { return &___floor_6; }
	inline void set_floor_6(int32_t value)
	{
		___floor_6 = value;
	}

	inline static int32_t get_offset_of_ceiling_7() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___ceiling_7)); }
	inline int32_t get_ceiling_7() const { return ___ceiling_7; }
	inline int32_t* get_address_of_ceiling_7() { return &___ceiling_7; }
	inline void set_ceiling_7(int32_t value)
	{
		___ceiling_7 = value;
	}

	inline static int32_t get_offset_of_backWall_8() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___backWall_8)); }
	inline int32_t get_backWall_8() const { return ___backWall_8; }
	inline int32_t* get_address_of_backWall_8() { return &___backWall_8; }
	inline void set_backWall_8(int32_t value)
	{
		___backWall_8 = value;
	}

	inline static int32_t get_offset_of_frontWall_9() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___frontWall_9)); }
	inline int32_t get_frontWall_9() const { return ___frontWall_9; }
	inline int32_t* get_address_of_frontWall_9() { return &___frontWall_9; }
	inline void set_frontWall_9(int32_t value)
	{
		___frontWall_9 = value;
	}

	inline static int32_t get_offset_of_reflectivity_10() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___reflectivity_10)); }
	inline float get_reflectivity_10() const { return ___reflectivity_10; }
	inline float* get_address_of_reflectivity_10() { return &___reflectivity_10; }
	inline void set_reflectivity_10(float value)
	{
		___reflectivity_10 = value;
	}

	inline static int32_t get_offset_of_reverbGainDb_11() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___reverbGainDb_11)); }
	inline float get_reverbGainDb_11() const { return ___reverbGainDb_11; }
	inline float* get_address_of_reverbGainDb_11() { return &___reverbGainDb_11; }
	inline void set_reverbGainDb_11(float value)
	{
		___reverbGainDb_11 = value;
	}

	inline static int32_t get_offset_of_reverbBrightness_12() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___reverbBrightness_12)); }
	inline float get_reverbBrightness_12() const { return ___reverbBrightness_12; }
	inline float* get_address_of_reverbBrightness_12() { return &___reverbBrightness_12; }
	inline void set_reverbBrightness_12(float value)
	{
		___reverbBrightness_12 = value;
	}

	inline static int32_t get_offset_of_reverbTime_13() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___reverbTime_13)); }
	inline float get_reverbTime_13() const { return ___reverbTime_13; }
	inline float* get_address_of_reverbTime_13() { return &___reverbTime_13; }
	inline void set_reverbTime_13(float value)
	{
		___reverbTime_13 = value;
	}

	inline static int32_t get_offset_of_size_14() { return static_cast<int32_t>(offsetof(GvrAudioRoom_t1033997160, ___size_14)); }
	inline Vector3_t3722313464  get_size_14() const { return ___size_14; }
	inline Vector3_t3722313464 * get_address_of_size_14() { return &___size_14; }
	inline void set_size_14(Vector3_t3722313464  value)
	{
		___size_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIOROOM_T1033997160_H
#ifndef GVRAUDIOSOUNDFIELD_T3422980884_H
#define GVRAUDIOSOUNDFIELD_T3422980884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioSoundfield
struct  GvrAudioSoundfield_t3422980884  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GvrAudioSoundfield::bypassRoomEffects
	bool ___bypassRoomEffects_4;
	// System.Single GvrAudioSoundfield::gainDb
	float ___gainDb_5;
	// System.Boolean GvrAudioSoundfield::playOnAwake
	bool ___playOnAwake_6;
	// UnityEngine.AudioClip GvrAudioSoundfield::soundfieldClip0102
	AudioClip_t3680889665 * ___soundfieldClip0102_7;
	// UnityEngine.AudioClip GvrAudioSoundfield::soundfieldClip0304
	AudioClip_t3680889665 * ___soundfieldClip0304_8;
	// System.Boolean GvrAudioSoundfield::soundfieldLoop
	bool ___soundfieldLoop_9;
	// System.Boolean GvrAudioSoundfield::soundfieldMute
	bool ___soundfieldMute_10;
	// System.Single GvrAudioSoundfield::soundfieldPitch
	float ___soundfieldPitch_11;
	// System.Int32 GvrAudioSoundfield::soundfieldPriority
	int32_t ___soundfieldPriority_12;
	// System.Single GvrAudioSoundfield::soundfieldSpatialBlend
	float ___soundfieldSpatialBlend_13;
	// System.Single GvrAudioSoundfield::soundfieldDopplerLevel
	float ___soundfieldDopplerLevel_14;
	// System.Single GvrAudioSoundfield::soundfieldVolume
	float ___soundfieldVolume_15;
	// UnityEngine.AudioRolloffMode GvrAudioSoundfield::soundfieldRolloffMode
	int32_t ___soundfieldRolloffMode_16;
	// System.Single GvrAudioSoundfield::soundfieldMaxDistance
	float ___soundfieldMaxDistance_17;
	// System.Single GvrAudioSoundfield::soundfieldMinDistance
	float ___soundfieldMinDistance_18;
	// System.Int32 GvrAudioSoundfield::id
	int32_t ___id_19;
	// UnityEngine.AudioSource[] GvrAudioSoundfield::audioSources
	AudioSourceU5BU5D_t4028559421* ___audioSources_20;
	// System.Boolean GvrAudioSoundfield::isPaused
	bool ___isPaused_21;

public:
	inline static int32_t get_offset_of_bypassRoomEffects_4() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___bypassRoomEffects_4)); }
	inline bool get_bypassRoomEffects_4() const { return ___bypassRoomEffects_4; }
	inline bool* get_address_of_bypassRoomEffects_4() { return &___bypassRoomEffects_4; }
	inline void set_bypassRoomEffects_4(bool value)
	{
		___bypassRoomEffects_4 = value;
	}

	inline static int32_t get_offset_of_gainDb_5() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___gainDb_5)); }
	inline float get_gainDb_5() const { return ___gainDb_5; }
	inline float* get_address_of_gainDb_5() { return &___gainDb_5; }
	inline void set_gainDb_5(float value)
	{
		___gainDb_5 = value;
	}

	inline static int32_t get_offset_of_playOnAwake_6() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___playOnAwake_6)); }
	inline bool get_playOnAwake_6() const { return ___playOnAwake_6; }
	inline bool* get_address_of_playOnAwake_6() { return &___playOnAwake_6; }
	inline void set_playOnAwake_6(bool value)
	{
		___playOnAwake_6 = value;
	}

	inline static int32_t get_offset_of_soundfieldClip0102_7() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldClip0102_7)); }
	inline AudioClip_t3680889665 * get_soundfieldClip0102_7() const { return ___soundfieldClip0102_7; }
	inline AudioClip_t3680889665 ** get_address_of_soundfieldClip0102_7() { return &___soundfieldClip0102_7; }
	inline void set_soundfieldClip0102_7(AudioClip_t3680889665 * value)
	{
		___soundfieldClip0102_7 = value;
		Il2CppCodeGenWriteBarrier((&___soundfieldClip0102_7), value);
	}

	inline static int32_t get_offset_of_soundfieldClip0304_8() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldClip0304_8)); }
	inline AudioClip_t3680889665 * get_soundfieldClip0304_8() const { return ___soundfieldClip0304_8; }
	inline AudioClip_t3680889665 ** get_address_of_soundfieldClip0304_8() { return &___soundfieldClip0304_8; }
	inline void set_soundfieldClip0304_8(AudioClip_t3680889665 * value)
	{
		___soundfieldClip0304_8 = value;
		Il2CppCodeGenWriteBarrier((&___soundfieldClip0304_8), value);
	}

	inline static int32_t get_offset_of_soundfieldLoop_9() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldLoop_9)); }
	inline bool get_soundfieldLoop_9() const { return ___soundfieldLoop_9; }
	inline bool* get_address_of_soundfieldLoop_9() { return &___soundfieldLoop_9; }
	inline void set_soundfieldLoop_9(bool value)
	{
		___soundfieldLoop_9 = value;
	}

	inline static int32_t get_offset_of_soundfieldMute_10() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldMute_10)); }
	inline bool get_soundfieldMute_10() const { return ___soundfieldMute_10; }
	inline bool* get_address_of_soundfieldMute_10() { return &___soundfieldMute_10; }
	inline void set_soundfieldMute_10(bool value)
	{
		___soundfieldMute_10 = value;
	}

	inline static int32_t get_offset_of_soundfieldPitch_11() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldPitch_11)); }
	inline float get_soundfieldPitch_11() const { return ___soundfieldPitch_11; }
	inline float* get_address_of_soundfieldPitch_11() { return &___soundfieldPitch_11; }
	inline void set_soundfieldPitch_11(float value)
	{
		___soundfieldPitch_11 = value;
	}

	inline static int32_t get_offset_of_soundfieldPriority_12() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldPriority_12)); }
	inline int32_t get_soundfieldPriority_12() const { return ___soundfieldPriority_12; }
	inline int32_t* get_address_of_soundfieldPriority_12() { return &___soundfieldPriority_12; }
	inline void set_soundfieldPriority_12(int32_t value)
	{
		___soundfieldPriority_12 = value;
	}

	inline static int32_t get_offset_of_soundfieldSpatialBlend_13() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldSpatialBlend_13)); }
	inline float get_soundfieldSpatialBlend_13() const { return ___soundfieldSpatialBlend_13; }
	inline float* get_address_of_soundfieldSpatialBlend_13() { return &___soundfieldSpatialBlend_13; }
	inline void set_soundfieldSpatialBlend_13(float value)
	{
		___soundfieldSpatialBlend_13 = value;
	}

	inline static int32_t get_offset_of_soundfieldDopplerLevel_14() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldDopplerLevel_14)); }
	inline float get_soundfieldDopplerLevel_14() const { return ___soundfieldDopplerLevel_14; }
	inline float* get_address_of_soundfieldDopplerLevel_14() { return &___soundfieldDopplerLevel_14; }
	inline void set_soundfieldDopplerLevel_14(float value)
	{
		___soundfieldDopplerLevel_14 = value;
	}

	inline static int32_t get_offset_of_soundfieldVolume_15() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldVolume_15)); }
	inline float get_soundfieldVolume_15() const { return ___soundfieldVolume_15; }
	inline float* get_address_of_soundfieldVolume_15() { return &___soundfieldVolume_15; }
	inline void set_soundfieldVolume_15(float value)
	{
		___soundfieldVolume_15 = value;
	}

	inline static int32_t get_offset_of_soundfieldRolloffMode_16() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldRolloffMode_16)); }
	inline int32_t get_soundfieldRolloffMode_16() const { return ___soundfieldRolloffMode_16; }
	inline int32_t* get_address_of_soundfieldRolloffMode_16() { return &___soundfieldRolloffMode_16; }
	inline void set_soundfieldRolloffMode_16(int32_t value)
	{
		___soundfieldRolloffMode_16 = value;
	}

	inline static int32_t get_offset_of_soundfieldMaxDistance_17() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldMaxDistance_17)); }
	inline float get_soundfieldMaxDistance_17() const { return ___soundfieldMaxDistance_17; }
	inline float* get_address_of_soundfieldMaxDistance_17() { return &___soundfieldMaxDistance_17; }
	inline void set_soundfieldMaxDistance_17(float value)
	{
		___soundfieldMaxDistance_17 = value;
	}

	inline static int32_t get_offset_of_soundfieldMinDistance_18() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___soundfieldMinDistance_18)); }
	inline float get_soundfieldMinDistance_18() const { return ___soundfieldMinDistance_18; }
	inline float* get_address_of_soundfieldMinDistance_18() { return &___soundfieldMinDistance_18; }
	inline void set_soundfieldMinDistance_18(float value)
	{
		___soundfieldMinDistance_18 = value;
	}

	inline static int32_t get_offset_of_id_19() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___id_19)); }
	inline int32_t get_id_19() const { return ___id_19; }
	inline int32_t* get_address_of_id_19() { return &___id_19; }
	inline void set_id_19(int32_t value)
	{
		___id_19 = value;
	}

	inline static int32_t get_offset_of_audioSources_20() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___audioSources_20)); }
	inline AudioSourceU5BU5D_t4028559421* get_audioSources_20() const { return ___audioSources_20; }
	inline AudioSourceU5BU5D_t4028559421** get_address_of_audioSources_20() { return &___audioSources_20; }
	inline void set_audioSources_20(AudioSourceU5BU5D_t4028559421* value)
	{
		___audioSources_20 = value;
		Il2CppCodeGenWriteBarrier((&___audioSources_20), value);
	}

	inline static int32_t get_offset_of_isPaused_21() { return static_cast<int32_t>(offsetof(GvrAudioSoundfield_t3422980884, ___isPaused_21)); }
	inline bool get_isPaused_21() const { return ___isPaused_21; }
	inline bool* get_address_of_isPaused_21() { return &___isPaused_21; }
	inline void set_isPaused_21(bool value)
	{
		___isPaused_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIOSOUNDFIELD_T3422980884_H
#ifndef GVRAUDIOSOURCE_T775302101_H
#define GVRAUDIOSOURCE_T775302101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioSource
struct  GvrAudioSource_t775302101  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GvrAudioSource::bypassRoomEffects
	bool ___bypassRoomEffects_4;
	// System.Single GvrAudioSource::directivityAlpha
	float ___directivityAlpha_5;
	// System.Single GvrAudioSource::directivitySharpness
	float ___directivitySharpness_6;
	// System.Single GvrAudioSource::listenerDirectivityAlpha
	float ___listenerDirectivityAlpha_7;
	// System.Single GvrAudioSource::listenerDirectivitySharpness
	float ___listenerDirectivitySharpness_8;
	// System.Single GvrAudioSource::gainDb
	float ___gainDb_9;
	// System.Boolean GvrAudioSource::occlusionEnabled
	bool ___occlusionEnabled_10;
	// System.Boolean GvrAudioSource::playOnAwake
	bool ___playOnAwake_11;
	// UnityEngine.AudioClip GvrAudioSource::sourceClip
	AudioClip_t3680889665 * ___sourceClip_12;
	// System.Boolean GvrAudioSource::sourceLoop
	bool ___sourceLoop_13;
	// System.Boolean GvrAudioSource::sourceMute
	bool ___sourceMute_14;
	// System.Single GvrAudioSource::sourcePitch
	float ___sourcePitch_15;
	// System.Int32 GvrAudioSource::sourcePriority
	int32_t ___sourcePriority_16;
	// System.Single GvrAudioSource::sourceSpatialBlend
	float ___sourceSpatialBlend_17;
	// System.Single GvrAudioSource::sourceDopplerLevel
	float ___sourceDopplerLevel_18;
	// System.Single GvrAudioSource::sourceSpread
	float ___sourceSpread_19;
	// System.Single GvrAudioSource::sourceVolume
	float ___sourceVolume_20;
	// UnityEngine.AudioRolloffMode GvrAudioSource::sourceRolloffMode
	int32_t ___sourceRolloffMode_21;
	// System.Single GvrAudioSource::sourceMaxDistance
	float ___sourceMaxDistance_22;
	// System.Single GvrAudioSource::sourceMinDistance
	float ___sourceMinDistance_23;
	// System.Boolean GvrAudioSource::hrtfEnabled
	bool ___hrtfEnabled_24;
	// UnityEngine.AudioSource GvrAudioSource::audioSource
	AudioSource_t3935305588 * ___audioSource_25;
	// System.Int32 GvrAudioSource::id
	int32_t ___id_26;
	// System.Single GvrAudioSource::currentOcclusion
	float ___currentOcclusion_27;
	// System.Single GvrAudioSource::nextOcclusionUpdate
	float ___nextOcclusionUpdate_28;
	// System.Boolean GvrAudioSource::isPaused
	bool ___isPaused_29;

public:
	inline static int32_t get_offset_of_bypassRoomEffects_4() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___bypassRoomEffects_4)); }
	inline bool get_bypassRoomEffects_4() const { return ___bypassRoomEffects_4; }
	inline bool* get_address_of_bypassRoomEffects_4() { return &___bypassRoomEffects_4; }
	inline void set_bypassRoomEffects_4(bool value)
	{
		___bypassRoomEffects_4 = value;
	}

	inline static int32_t get_offset_of_directivityAlpha_5() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___directivityAlpha_5)); }
	inline float get_directivityAlpha_5() const { return ___directivityAlpha_5; }
	inline float* get_address_of_directivityAlpha_5() { return &___directivityAlpha_5; }
	inline void set_directivityAlpha_5(float value)
	{
		___directivityAlpha_5 = value;
	}

	inline static int32_t get_offset_of_directivitySharpness_6() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___directivitySharpness_6)); }
	inline float get_directivitySharpness_6() const { return ___directivitySharpness_6; }
	inline float* get_address_of_directivitySharpness_6() { return &___directivitySharpness_6; }
	inline void set_directivitySharpness_6(float value)
	{
		___directivitySharpness_6 = value;
	}

	inline static int32_t get_offset_of_listenerDirectivityAlpha_7() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___listenerDirectivityAlpha_7)); }
	inline float get_listenerDirectivityAlpha_7() const { return ___listenerDirectivityAlpha_7; }
	inline float* get_address_of_listenerDirectivityAlpha_7() { return &___listenerDirectivityAlpha_7; }
	inline void set_listenerDirectivityAlpha_7(float value)
	{
		___listenerDirectivityAlpha_7 = value;
	}

	inline static int32_t get_offset_of_listenerDirectivitySharpness_8() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___listenerDirectivitySharpness_8)); }
	inline float get_listenerDirectivitySharpness_8() const { return ___listenerDirectivitySharpness_8; }
	inline float* get_address_of_listenerDirectivitySharpness_8() { return &___listenerDirectivitySharpness_8; }
	inline void set_listenerDirectivitySharpness_8(float value)
	{
		___listenerDirectivitySharpness_8 = value;
	}

	inline static int32_t get_offset_of_gainDb_9() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___gainDb_9)); }
	inline float get_gainDb_9() const { return ___gainDb_9; }
	inline float* get_address_of_gainDb_9() { return &___gainDb_9; }
	inline void set_gainDb_9(float value)
	{
		___gainDb_9 = value;
	}

	inline static int32_t get_offset_of_occlusionEnabled_10() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___occlusionEnabled_10)); }
	inline bool get_occlusionEnabled_10() const { return ___occlusionEnabled_10; }
	inline bool* get_address_of_occlusionEnabled_10() { return &___occlusionEnabled_10; }
	inline void set_occlusionEnabled_10(bool value)
	{
		___occlusionEnabled_10 = value;
	}

	inline static int32_t get_offset_of_playOnAwake_11() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___playOnAwake_11)); }
	inline bool get_playOnAwake_11() const { return ___playOnAwake_11; }
	inline bool* get_address_of_playOnAwake_11() { return &___playOnAwake_11; }
	inline void set_playOnAwake_11(bool value)
	{
		___playOnAwake_11 = value;
	}

	inline static int32_t get_offset_of_sourceClip_12() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceClip_12)); }
	inline AudioClip_t3680889665 * get_sourceClip_12() const { return ___sourceClip_12; }
	inline AudioClip_t3680889665 ** get_address_of_sourceClip_12() { return &___sourceClip_12; }
	inline void set_sourceClip_12(AudioClip_t3680889665 * value)
	{
		___sourceClip_12 = value;
		Il2CppCodeGenWriteBarrier((&___sourceClip_12), value);
	}

	inline static int32_t get_offset_of_sourceLoop_13() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceLoop_13)); }
	inline bool get_sourceLoop_13() const { return ___sourceLoop_13; }
	inline bool* get_address_of_sourceLoop_13() { return &___sourceLoop_13; }
	inline void set_sourceLoop_13(bool value)
	{
		___sourceLoop_13 = value;
	}

	inline static int32_t get_offset_of_sourceMute_14() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceMute_14)); }
	inline bool get_sourceMute_14() const { return ___sourceMute_14; }
	inline bool* get_address_of_sourceMute_14() { return &___sourceMute_14; }
	inline void set_sourceMute_14(bool value)
	{
		___sourceMute_14 = value;
	}

	inline static int32_t get_offset_of_sourcePitch_15() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourcePitch_15)); }
	inline float get_sourcePitch_15() const { return ___sourcePitch_15; }
	inline float* get_address_of_sourcePitch_15() { return &___sourcePitch_15; }
	inline void set_sourcePitch_15(float value)
	{
		___sourcePitch_15 = value;
	}

	inline static int32_t get_offset_of_sourcePriority_16() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourcePriority_16)); }
	inline int32_t get_sourcePriority_16() const { return ___sourcePriority_16; }
	inline int32_t* get_address_of_sourcePriority_16() { return &___sourcePriority_16; }
	inline void set_sourcePriority_16(int32_t value)
	{
		___sourcePriority_16 = value;
	}

	inline static int32_t get_offset_of_sourceSpatialBlend_17() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceSpatialBlend_17)); }
	inline float get_sourceSpatialBlend_17() const { return ___sourceSpatialBlend_17; }
	inline float* get_address_of_sourceSpatialBlend_17() { return &___sourceSpatialBlend_17; }
	inline void set_sourceSpatialBlend_17(float value)
	{
		___sourceSpatialBlend_17 = value;
	}

	inline static int32_t get_offset_of_sourceDopplerLevel_18() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceDopplerLevel_18)); }
	inline float get_sourceDopplerLevel_18() const { return ___sourceDopplerLevel_18; }
	inline float* get_address_of_sourceDopplerLevel_18() { return &___sourceDopplerLevel_18; }
	inline void set_sourceDopplerLevel_18(float value)
	{
		___sourceDopplerLevel_18 = value;
	}

	inline static int32_t get_offset_of_sourceSpread_19() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceSpread_19)); }
	inline float get_sourceSpread_19() const { return ___sourceSpread_19; }
	inline float* get_address_of_sourceSpread_19() { return &___sourceSpread_19; }
	inline void set_sourceSpread_19(float value)
	{
		___sourceSpread_19 = value;
	}

	inline static int32_t get_offset_of_sourceVolume_20() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceVolume_20)); }
	inline float get_sourceVolume_20() const { return ___sourceVolume_20; }
	inline float* get_address_of_sourceVolume_20() { return &___sourceVolume_20; }
	inline void set_sourceVolume_20(float value)
	{
		___sourceVolume_20 = value;
	}

	inline static int32_t get_offset_of_sourceRolloffMode_21() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceRolloffMode_21)); }
	inline int32_t get_sourceRolloffMode_21() const { return ___sourceRolloffMode_21; }
	inline int32_t* get_address_of_sourceRolloffMode_21() { return &___sourceRolloffMode_21; }
	inline void set_sourceRolloffMode_21(int32_t value)
	{
		___sourceRolloffMode_21 = value;
	}

	inline static int32_t get_offset_of_sourceMaxDistance_22() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceMaxDistance_22)); }
	inline float get_sourceMaxDistance_22() const { return ___sourceMaxDistance_22; }
	inline float* get_address_of_sourceMaxDistance_22() { return &___sourceMaxDistance_22; }
	inline void set_sourceMaxDistance_22(float value)
	{
		___sourceMaxDistance_22 = value;
	}

	inline static int32_t get_offset_of_sourceMinDistance_23() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___sourceMinDistance_23)); }
	inline float get_sourceMinDistance_23() const { return ___sourceMinDistance_23; }
	inline float* get_address_of_sourceMinDistance_23() { return &___sourceMinDistance_23; }
	inline void set_sourceMinDistance_23(float value)
	{
		___sourceMinDistance_23 = value;
	}

	inline static int32_t get_offset_of_hrtfEnabled_24() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___hrtfEnabled_24)); }
	inline bool get_hrtfEnabled_24() const { return ___hrtfEnabled_24; }
	inline bool* get_address_of_hrtfEnabled_24() { return &___hrtfEnabled_24; }
	inline void set_hrtfEnabled_24(bool value)
	{
		___hrtfEnabled_24 = value;
	}

	inline static int32_t get_offset_of_audioSource_25() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___audioSource_25)); }
	inline AudioSource_t3935305588 * get_audioSource_25() const { return ___audioSource_25; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_25() { return &___audioSource_25; }
	inline void set_audioSource_25(AudioSource_t3935305588 * value)
	{
		___audioSource_25 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_25), value);
	}

	inline static int32_t get_offset_of_id_26() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___id_26)); }
	inline int32_t get_id_26() const { return ___id_26; }
	inline int32_t* get_address_of_id_26() { return &___id_26; }
	inline void set_id_26(int32_t value)
	{
		___id_26 = value;
	}

	inline static int32_t get_offset_of_currentOcclusion_27() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___currentOcclusion_27)); }
	inline float get_currentOcclusion_27() const { return ___currentOcclusion_27; }
	inline float* get_address_of_currentOcclusion_27() { return &___currentOcclusion_27; }
	inline void set_currentOcclusion_27(float value)
	{
		___currentOcclusion_27 = value;
	}

	inline static int32_t get_offset_of_nextOcclusionUpdate_28() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___nextOcclusionUpdate_28)); }
	inline float get_nextOcclusionUpdate_28() const { return ___nextOcclusionUpdate_28; }
	inline float* get_address_of_nextOcclusionUpdate_28() { return &___nextOcclusionUpdate_28; }
	inline void set_nextOcclusionUpdate_28(float value)
	{
		___nextOcclusionUpdate_28 = value;
	}

	inline static int32_t get_offset_of_isPaused_29() { return static_cast<int32_t>(offsetof(GvrAudioSource_t775302101, ___isPaused_29)); }
	inline bool get_isPaused_29() const { return ___isPaused_29; }
	inline bool* get_address_of_isPaused_29() { return &___isPaused_29; }
	inline void set_isPaused_29(bool value)
	{
		___isPaused_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIOSOURCE_T775302101_H
#ifndef GVRBASEARMMODEL_T3382200842_H
#define GVRBASEARMMODEL_T3382200842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBaseArmModel
struct  GvrBaseArmModel_t3382200842  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRBASEARMMODEL_T3382200842_H
#ifndef GVRBASEPOINTER_T822782720_H
#define GVRBASEPOINTER_T822782720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointer
struct  GvrBasePointer_t822782720  : public MonoBehaviour_t3962482529
{
public:
	// GvrBasePointer/RaycastMode GvrBasePointer::raycastMode
	int32_t ___raycastMode_4;
	// UnityEngine.Camera GvrBasePointer::overridePointerCamera
	Camera_t4157153871 * ___overridePointerCamera_5;
	// System.Boolean GvrBasePointer::<ShouldUseExitRadiusForRaycast>k__BackingField
	bool ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6;
	// GvrControllerInputDevice GvrBasePointer::<ControllerInputDevice>k__BackingField
	GvrControllerInputDevice_t3709172113 * ___U3CControllerInputDeviceU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_raycastMode_4() { return static_cast<int32_t>(offsetof(GvrBasePointer_t822782720, ___raycastMode_4)); }
	inline int32_t get_raycastMode_4() const { return ___raycastMode_4; }
	inline int32_t* get_address_of_raycastMode_4() { return &___raycastMode_4; }
	inline void set_raycastMode_4(int32_t value)
	{
		___raycastMode_4 = value;
	}

	inline static int32_t get_offset_of_overridePointerCamera_5() { return static_cast<int32_t>(offsetof(GvrBasePointer_t822782720, ___overridePointerCamera_5)); }
	inline Camera_t4157153871 * get_overridePointerCamera_5() const { return ___overridePointerCamera_5; }
	inline Camera_t4157153871 ** get_address_of_overridePointerCamera_5() { return &___overridePointerCamera_5; }
	inline void set_overridePointerCamera_5(Camera_t4157153871 * value)
	{
		___overridePointerCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___overridePointerCamera_5), value);
	}

	inline static int32_t get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GvrBasePointer_t822782720, ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6)); }
	inline bool get_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6() const { return ___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6() { return &___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6; }
	inline void set_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6(bool value)
	{
		___U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GvrBasePointer_t822782720, ___U3CControllerInputDeviceU3Ek__BackingField_7)); }
	inline GvrControllerInputDevice_t3709172113 * get_U3CControllerInputDeviceU3Ek__BackingField_7() const { return ___U3CControllerInputDeviceU3Ek__BackingField_7; }
	inline GvrControllerInputDevice_t3709172113 ** get_address_of_U3CControllerInputDeviceU3Ek__BackingField_7() { return &___U3CControllerInputDeviceU3Ek__BackingField_7; }
	inline void set_U3CControllerInputDeviceU3Ek__BackingField_7(GvrControllerInputDevice_t3709172113 * value)
	{
		___U3CControllerInputDeviceU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerInputDeviceU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRBASEPOINTER_T822782720_H
#ifndef GVRCONTROLLERINPUT_T3301899046_H
#define GVRCONTROLLERINPUT_T3301899046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerInput
struct  GvrControllerInput_t3301899046  : public MonoBehaviour_t3962482529
{
public:
	// GvrControllerInput/EmulatorConnectionMode GvrControllerInput::emulatorConnectionMode
	int32_t ___emulatorConnectionMode_10;

public:
	inline static int32_t get_offset_of_emulatorConnectionMode_10() { return static_cast<int32_t>(offsetof(GvrControllerInput_t3301899046, ___emulatorConnectionMode_10)); }
	inline int32_t get_emulatorConnectionMode_10() const { return ___emulatorConnectionMode_10; }
	inline int32_t* get_address_of_emulatorConnectionMode_10() { return &___emulatorConnectionMode_10; }
	inline void set_emulatorConnectionMode_10(int32_t value)
	{
		___emulatorConnectionMode_10 = value;
	}
};

struct GvrControllerInput_t3301899046_StaticFields
{
public:
	// GvrControllerInputDevice[] GvrControllerInput::instances
	GvrControllerInputDeviceU5BU5D_t1273413900* ___instances_4;
	// Gvr.Internal.IControllerProvider GvrControllerInput::controllerProvider
	RuntimeObject* ___controllerProvider_5;
	// GvrSettings/UserPrefsHandedness GvrControllerInput::handedness
	int32_t ___handedness_6;
	// System.Action GvrControllerInput::onDevicesChangedInternal
	Action_t1264377477 * ___onDevicesChangedInternal_7;
	// System.Action GvrControllerInput::OnControllerInputUpdated
	Action_t1264377477 * ___OnControllerInputUpdated_8;
	// System.Action GvrControllerInput::OnPostControllerInputUpdated
	Action_t1264377477 * ___OnPostControllerInputUpdated_9;

public:
	inline static int32_t get_offset_of_instances_4() { return static_cast<int32_t>(offsetof(GvrControllerInput_t3301899046_StaticFields, ___instances_4)); }
	inline GvrControllerInputDeviceU5BU5D_t1273413900* get_instances_4() const { return ___instances_4; }
	inline GvrControllerInputDeviceU5BU5D_t1273413900** get_address_of_instances_4() { return &___instances_4; }
	inline void set_instances_4(GvrControllerInputDeviceU5BU5D_t1273413900* value)
	{
		___instances_4 = value;
		Il2CppCodeGenWriteBarrier((&___instances_4), value);
	}

	inline static int32_t get_offset_of_controllerProvider_5() { return static_cast<int32_t>(offsetof(GvrControllerInput_t3301899046_StaticFields, ___controllerProvider_5)); }
	inline RuntimeObject* get_controllerProvider_5() const { return ___controllerProvider_5; }
	inline RuntimeObject** get_address_of_controllerProvider_5() { return &___controllerProvider_5; }
	inline void set_controllerProvider_5(RuntimeObject* value)
	{
		___controllerProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___controllerProvider_5), value);
	}

	inline static int32_t get_offset_of_handedness_6() { return static_cast<int32_t>(offsetof(GvrControllerInput_t3301899046_StaticFields, ___handedness_6)); }
	inline int32_t get_handedness_6() const { return ___handedness_6; }
	inline int32_t* get_address_of_handedness_6() { return &___handedness_6; }
	inline void set_handedness_6(int32_t value)
	{
		___handedness_6 = value;
	}

	inline static int32_t get_offset_of_onDevicesChangedInternal_7() { return static_cast<int32_t>(offsetof(GvrControllerInput_t3301899046_StaticFields, ___onDevicesChangedInternal_7)); }
	inline Action_t1264377477 * get_onDevicesChangedInternal_7() const { return ___onDevicesChangedInternal_7; }
	inline Action_t1264377477 ** get_address_of_onDevicesChangedInternal_7() { return &___onDevicesChangedInternal_7; }
	inline void set_onDevicesChangedInternal_7(Action_t1264377477 * value)
	{
		___onDevicesChangedInternal_7 = value;
		Il2CppCodeGenWriteBarrier((&___onDevicesChangedInternal_7), value);
	}

	inline static int32_t get_offset_of_OnControllerInputUpdated_8() { return static_cast<int32_t>(offsetof(GvrControllerInput_t3301899046_StaticFields, ___OnControllerInputUpdated_8)); }
	inline Action_t1264377477 * get_OnControllerInputUpdated_8() const { return ___OnControllerInputUpdated_8; }
	inline Action_t1264377477 ** get_address_of_OnControllerInputUpdated_8() { return &___OnControllerInputUpdated_8; }
	inline void set_OnControllerInputUpdated_8(Action_t1264377477 * value)
	{
		___OnControllerInputUpdated_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnControllerInputUpdated_8), value);
	}

	inline static int32_t get_offset_of_OnPostControllerInputUpdated_9() { return static_cast<int32_t>(offsetof(GvrControllerInput_t3301899046_StaticFields, ___OnPostControllerInputUpdated_9)); }
	inline Action_t1264377477 * get_OnPostControllerInputUpdated_9() const { return ___OnPostControllerInputUpdated_9; }
	inline Action_t1264377477 ** get_address_of_OnPostControllerInputUpdated_9() { return &___OnPostControllerInputUpdated_9; }
	inline void set_OnPostControllerInputUpdated_9(Action_t1264377477 * value)
	{
		___OnPostControllerInputUpdated_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostControllerInputUpdated_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERINPUT_T3301899046_H
#ifndef GVRCONTROLLERRETICLEVISUAL_T1615767572_H
#define GVRCONTROLLERRETICLEVISUAL_T1615767572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerReticleVisual
struct  GvrControllerReticleVisual_t1615767572  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GvrControllerReticleVisual::isSizeBasedOnCameraDistance
	bool ___isSizeBasedOnCameraDistance_4;
	// System.Single GvrControllerReticleVisual::sizeMeters
	float ___sizeMeters_5;
	// GvrControllerReticleVisual/FaceCameraData GvrControllerReticleVisual::doesReticleFaceCamera
	FaceCameraData_t2570986833  ___doesReticleFaceCamera_6;
	// System.Int32 GvrControllerReticleVisual::sortingOrder
	int32_t ___sortingOrder_7;
	// System.Single GvrControllerReticleVisual::<ReticleMeshSizeMeters>k__BackingField
	float ___U3CReticleMeshSizeMetersU3Ek__BackingField_8;
	// System.Single GvrControllerReticleVisual::<ReticleMeshSizeRatio>k__BackingField
	float ___U3CReticleMeshSizeRatioU3Ek__BackingField_9;
	// UnityEngine.MeshRenderer GvrControllerReticleVisual::meshRenderer
	MeshRenderer_t587009260 * ___meshRenderer_10;
	// UnityEngine.MeshFilter GvrControllerReticleVisual::meshFilter
	MeshFilter_t3523625662 * ___meshFilter_11;
	// UnityEngine.Vector3 GvrControllerReticleVisual::preRenderLocalScale
	Vector3_t3722313464  ___preRenderLocalScale_12;
	// UnityEngine.Quaternion GvrControllerReticleVisual::preRenderLocalRotation
	Quaternion_t2301928331  ___preRenderLocalRotation_13;

public:
	inline static int32_t get_offset_of_isSizeBasedOnCameraDistance_4() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___isSizeBasedOnCameraDistance_4)); }
	inline bool get_isSizeBasedOnCameraDistance_4() const { return ___isSizeBasedOnCameraDistance_4; }
	inline bool* get_address_of_isSizeBasedOnCameraDistance_4() { return &___isSizeBasedOnCameraDistance_4; }
	inline void set_isSizeBasedOnCameraDistance_4(bool value)
	{
		___isSizeBasedOnCameraDistance_4 = value;
	}

	inline static int32_t get_offset_of_sizeMeters_5() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___sizeMeters_5)); }
	inline float get_sizeMeters_5() const { return ___sizeMeters_5; }
	inline float* get_address_of_sizeMeters_5() { return &___sizeMeters_5; }
	inline void set_sizeMeters_5(float value)
	{
		___sizeMeters_5 = value;
	}

	inline static int32_t get_offset_of_doesReticleFaceCamera_6() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___doesReticleFaceCamera_6)); }
	inline FaceCameraData_t2570986833  get_doesReticleFaceCamera_6() const { return ___doesReticleFaceCamera_6; }
	inline FaceCameraData_t2570986833 * get_address_of_doesReticleFaceCamera_6() { return &___doesReticleFaceCamera_6; }
	inline void set_doesReticleFaceCamera_6(FaceCameraData_t2570986833  value)
	{
		___doesReticleFaceCamera_6 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_7() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___sortingOrder_7)); }
	inline int32_t get_sortingOrder_7() const { return ___sortingOrder_7; }
	inline int32_t* get_address_of_sortingOrder_7() { return &___sortingOrder_7; }
	inline void set_sortingOrder_7(int32_t value)
	{
		___sortingOrder_7 = value;
	}

	inline static int32_t get_offset_of_U3CReticleMeshSizeMetersU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___U3CReticleMeshSizeMetersU3Ek__BackingField_8)); }
	inline float get_U3CReticleMeshSizeMetersU3Ek__BackingField_8() const { return ___U3CReticleMeshSizeMetersU3Ek__BackingField_8; }
	inline float* get_address_of_U3CReticleMeshSizeMetersU3Ek__BackingField_8() { return &___U3CReticleMeshSizeMetersU3Ek__BackingField_8; }
	inline void set_U3CReticleMeshSizeMetersU3Ek__BackingField_8(float value)
	{
		___U3CReticleMeshSizeMetersU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CReticleMeshSizeRatioU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___U3CReticleMeshSizeRatioU3Ek__BackingField_9)); }
	inline float get_U3CReticleMeshSizeRatioU3Ek__BackingField_9() const { return ___U3CReticleMeshSizeRatioU3Ek__BackingField_9; }
	inline float* get_address_of_U3CReticleMeshSizeRatioU3Ek__BackingField_9() { return &___U3CReticleMeshSizeRatioU3Ek__BackingField_9; }
	inline void set_U3CReticleMeshSizeRatioU3Ek__BackingField_9(float value)
	{
		___U3CReticleMeshSizeRatioU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_meshRenderer_10() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___meshRenderer_10)); }
	inline MeshRenderer_t587009260 * get_meshRenderer_10() const { return ___meshRenderer_10; }
	inline MeshRenderer_t587009260 ** get_address_of_meshRenderer_10() { return &___meshRenderer_10; }
	inline void set_meshRenderer_10(MeshRenderer_t587009260 * value)
	{
		___meshRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_10), value);
	}

	inline static int32_t get_offset_of_meshFilter_11() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___meshFilter_11)); }
	inline MeshFilter_t3523625662 * get_meshFilter_11() const { return ___meshFilter_11; }
	inline MeshFilter_t3523625662 ** get_address_of_meshFilter_11() { return &___meshFilter_11; }
	inline void set_meshFilter_11(MeshFilter_t3523625662 * value)
	{
		___meshFilter_11 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_11), value);
	}

	inline static int32_t get_offset_of_preRenderLocalScale_12() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___preRenderLocalScale_12)); }
	inline Vector3_t3722313464  get_preRenderLocalScale_12() const { return ___preRenderLocalScale_12; }
	inline Vector3_t3722313464 * get_address_of_preRenderLocalScale_12() { return &___preRenderLocalScale_12; }
	inline void set_preRenderLocalScale_12(Vector3_t3722313464  value)
	{
		___preRenderLocalScale_12 = value;
	}

	inline static int32_t get_offset_of_preRenderLocalRotation_13() { return static_cast<int32_t>(offsetof(GvrControllerReticleVisual_t1615767572, ___preRenderLocalRotation_13)); }
	inline Quaternion_t2301928331  get_preRenderLocalRotation_13() const { return ___preRenderLocalRotation_13; }
	inline Quaternion_t2301928331 * get_address_of_preRenderLocalRotation_13() { return &___preRenderLocalRotation_13; }
	inline void set_preRenderLocalRotation_13(Quaternion_t2301928331  value)
	{
		___preRenderLocalRotation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERRETICLEVISUAL_T1615767572_H
#ifndef GVRCONTROLLERTOOLTIPSSIMPLE_T2073124053_H
#define GVRCONTROLLERTOOLTIPSSIMPLE_T2073124053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerTooltipsSimple
struct  GvrControllerTooltipsSimple_t2073124053  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshRenderer GvrControllerTooltipsSimple::tooltipRenderer
	MeshRenderer_t587009260 * ___tooltipRenderer_4;
	// GvrBaseArmModel GvrControllerTooltipsSimple::<ArmModel>k__BackingField
	GvrBaseArmModel_t3382200842 * ___U3CArmModelU3Ek__BackingField_5;
	// UnityEngine.MaterialPropertyBlock GvrControllerTooltipsSimple::materialPropertyBlock
	MaterialPropertyBlock_t3213117958 * ___materialPropertyBlock_6;
	// System.Int32 GvrControllerTooltipsSimple::colorId
	int32_t ___colorId_7;

public:
	inline static int32_t get_offset_of_tooltipRenderer_4() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t2073124053, ___tooltipRenderer_4)); }
	inline MeshRenderer_t587009260 * get_tooltipRenderer_4() const { return ___tooltipRenderer_4; }
	inline MeshRenderer_t587009260 ** get_address_of_tooltipRenderer_4() { return &___tooltipRenderer_4; }
	inline void set_tooltipRenderer_4(MeshRenderer_t587009260 * value)
	{
		___tooltipRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___tooltipRenderer_4), value);
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t2073124053, ___U3CArmModelU3Ek__BackingField_5)); }
	inline GvrBaseArmModel_t3382200842 * get_U3CArmModelU3Ek__BackingField_5() const { return ___U3CArmModelU3Ek__BackingField_5; }
	inline GvrBaseArmModel_t3382200842 ** get_address_of_U3CArmModelU3Ek__BackingField_5() { return &___U3CArmModelU3Ek__BackingField_5; }
	inline void set_U3CArmModelU3Ek__BackingField_5(GvrBaseArmModel_t3382200842 * value)
	{
		___U3CArmModelU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_materialPropertyBlock_6() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t2073124053, ___materialPropertyBlock_6)); }
	inline MaterialPropertyBlock_t3213117958 * get_materialPropertyBlock_6() const { return ___materialPropertyBlock_6; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_materialPropertyBlock_6() { return &___materialPropertyBlock_6; }
	inline void set_materialPropertyBlock_6(MaterialPropertyBlock_t3213117958 * value)
	{
		___materialPropertyBlock_6 = value;
		Il2CppCodeGenWriteBarrier((&___materialPropertyBlock_6), value);
	}

	inline static int32_t get_offset_of_colorId_7() { return static_cast<int32_t>(offsetof(GvrControllerTooltipsSimple_t2073124053, ___colorId_7)); }
	inline int32_t get_colorId_7() const { return ___colorId_7; }
	inline int32_t* get_address_of_colorId_7() { return &___colorId_7; }
	inline void set_colorId_7(int32_t value)
	{
		___colorId_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERTOOLTIPSSIMPLE_T2073124053_H
#ifndef GVRCONTROLLERVISUAL_T3828962282_H
#define GVRCONTROLLERVISUAL_T3828962282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerVisual
struct  GvrControllerVisual_t3828962282  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] GvrControllerVisual::attachmentPrefabs
	GameObjectU5BU5D_t3328599146* ___attachmentPrefabs_4;
	// UnityEngine.Color GvrControllerVisual::touchPadColor
	Color_t2555686324  ___touchPadColor_5;
	// UnityEngine.Color GvrControllerVisual::appButtonColor
	Color_t2555686324  ___appButtonColor_6;
	// UnityEngine.Color GvrControllerVisual::systemButtonColor
	Color_t2555686324  ___systemButtonColor_7;
	// System.Boolean GvrControllerVisual::readControllerState
	bool ___readControllerState_8;
	// GvrControllerVisual/ControllerDisplayState GvrControllerVisual::displayState
	ControllerDisplayState_t972313457  ___displayState_9;
	// System.Single GvrControllerVisual::maximumAlpha
	float ___maximumAlpha_10;
	// GvrBaseArmModel GvrControllerVisual::<ArmModel>k__BackingField
	GvrBaseArmModel_t3382200842 * ___U3CArmModelU3Ek__BackingField_11;
	// GvrControllerInputDevice GvrControllerVisual::<ControllerInputDevice>k__BackingField
	GvrControllerInputDevice_t3709172113 * ___U3CControllerInputDeviceU3Ek__BackingField_12;
	// UnityEngine.Renderer GvrControllerVisual::controllerRenderer
	Renderer_t2627027031 * ___controllerRenderer_13;
	// UnityEngine.MaterialPropertyBlock GvrControllerVisual::materialPropertyBlock
	MaterialPropertyBlock_t3213117958 * ___materialPropertyBlock_14;
	// System.Int32 GvrControllerVisual::alphaId
	int32_t ___alphaId_15;
	// System.Int32 GvrControllerVisual::touchId
	int32_t ___touchId_16;
	// System.Int32 GvrControllerVisual::touchPadId
	int32_t ___touchPadId_17;
	// System.Int32 GvrControllerVisual::appButtonId
	int32_t ___appButtonId_18;
	// System.Int32 GvrControllerVisual::systemButtonId
	int32_t ___systemButtonId_19;
	// System.Int32 GvrControllerVisual::batteryColorId
	int32_t ___batteryColorId_20;
	// System.Boolean GvrControllerVisual::wasTouching
	bool ___wasTouching_21;
	// System.Single GvrControllerVisual::touchTime
	float ___touchTime_22;
	// UnityEngine.Vector4 GvrControllerVisual::controllerShaderData
	Vector4_t3319028937  ___controllerShaderData_23;
	// UnityEngine.Vector4 GvrControllerVisual::controllerShaderData2
	Vector4_t3319028937  ___controllerShaderData2_24;
	// UnityEngine.Color GvrControllerVisual::currentBatteryColor
	Color_t2555686324  ___currentBatteryColor_25;
	// UnityEngine.Color GvrControllerVisual::GVR_BATTERY_CRITICAL_COLOR
	Color_t2555686324  ___GVR_BATTERY_CRITICAL_COLOR_40;
	// UnityEngine.Color GvrControllerVisual::GVR_BATTERY_LOW_COLOR
	Color_t2555686324  ___GVR_BATTERY_LOW_COLOR_41;
	// UnityEngine.Color GvrControllerVisual::GVR_BATTERY_MED_COLOR
	Color_t2555686324  ___GVR_BATTERY_MED_COLOR_42;
	// UnityEngine.Color GvrControllerVisual::GVR_BATTERY_FULL_COLOR
	Color_t2555686324  ___GVR_BATTERY_FULL_COLOR_43;

public:
	inline static int32_t get_offset_of_attachmentPrefabs_4() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___attachmentPrefabs_4)); }
	inline GameObjectU5BU5D_t3328599146* get_attachmentPrefabs_4() const { return ___attachmentPrefabs_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_attachmentPrefabs_4() { return &___attachmentPrefabs_4; }
	inline void set_attachmentPrefabs_4(GameObjectU5BU5D_t3328599146* value)
	{
		___attachmentPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentPrefabs_4), value);
	}

	inline static int32_t get_offset_of_touchPadColor_5() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___touchPadColor_5)); }
	inline Color_t2555686324  get_touchPadColor_5() const { return ___touchPadColor_5; }
	inline Color_t2555686324 * get_address_of_touchPadColor_5() { return &___touchPadColor_5; }
	inline void set_touchPadColor_5(Color_t2555686324  value)
	{
		___touchPadColor_5 = value;
	}

	inline static int32_t get_offset_of_appButtonColor_6() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___appButtonColor_6)); }
	inline Color_t2555686324  get_appButtonColor_6() const { return ___appButtonColor_6; }
	inline Color_t2555686324 * get_address_of_appButtonColor_6() { return &___appButtonColor_6; }
	inline void set_appButtonColor_6(Color_t2555686324  value)
	{
		___appButtonColor_6 = value;
	}

	inline static int32_t get_offset_of_systemButtonColor_7() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___systemButtonColor_7)); }
	inline Color_t2555686324  get_systemButtonColor_7() const { return ___systemButtonColor_7; }
	inline Color_t2555686324 * get_address_of_systemButtonColor_7() { return &___systemButtonColor_7; }
	inline void set_systemButtonColor_7(Color_t2555686324  value)
	{
		___systemButtonColor_7 = value;
	}

	inline static int32_t get_offset_of_readControllerState_8() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___readControllerState_8)); }
	inline bool get_readControllerState_8() const { return ___readControllerState_8; }
	inline bool* get_address_of_readControllerState_8() { return &___readControllerState_8; }
	inline void set_readControllerState_8(bool value)
	{
		___readControllerState_8 = value;
	}

	inline static int32_t get_offset_of_displayState_9() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___displayState_9)); }
	inline ControllerDisplayState_t972313457  get_displayState_9() const { return ___displayState_9; }
	inline ControllerDisplayState_t972313457 * get_address_of_displayState_9() { return &___displayState_9; }
	inline void set_displayState_9(ControllerDisplayState_t972313457  value)
	{
		___displayState_9 = value;
	}

	inline static int32_t get_offset_of_maximumAlpha_10() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___maximumAlpha_10)); }
	inline float get_maximumAlpha_10() const { return ___maximumAlpha_10; }
	inline float* get_address_of_maximumAlpha_10() { return &___maximumAlpha_10; }
	inline void set_maximumAlpha_10(float value)
	{
		___maximumAlpha_10 = value;
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___U3CArmModelU3Ek__BackingField_11)); }
	inline GvrBaseArmModel_t3382200842 * get_U3CArmModelU3Ek__BackingField_11() const { return ___U3CArmModelU3Ek__BackingField_11; }
	inline GvrBaseArmModel_t3382200842 ** get_address_of_U3CArmModelU3Ek__BackingField_11() { return &___U3CArmModelU3Ek__BackingField_11; }
	inline void set_U3CArmModelU3Ek__BackingField_11(GvrBaseArmModel_t3382200842 * value)
	{
		___U3CArmModelU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___U3CControllerInputDeviceU3Ek__BackingField_12)); }
	inline GvrControllerInputDevice_t3709172113 * get_U3CControllerInputDeviceU3Ek__BackingField_12() const { return ___U3CControllerInputDeviceU3Ek__BackingField_12; }
	inline GvrControllerInputDevice_t3709172113 ** get_address_of_U3CControllerInputDeviceU3Ek__BackingField_12() { return &___U3CControllerInputDeviceU3Ek__BackingField_12; }
	inline void set_U3CControllerInputDeviceU3Ek__BackingField_12(GvrControllerInputDevice_t3709172113 * value)
	{
		___U3CControllerInputDeviceU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerInputDeviceU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_controllerRenderer_13() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___controllerRenderer_13)); }
	inline Renderer_t2627027031 * get_controllerRenderer_13() const { return ___controllerRenderer_13; }
	inline Renderer_t2627027031 ** get_address_of_controllerRenderer_13() { return &___controllerRenderer_13; }
	inline void set_controllerRenderer_13(Renderer_t2627027031 * value)
	{
		___controllerRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((&___controllerRenderer_13), value);
	}

	inline static int32_t get_offset_of_materialPropertyBlock_14() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___materialPropertyBlock_14)); }
	inline MaterialPropertyBlock_t3213117958 * get_materialPropertyBlock_14() const { return ___materialPropertyBlock_14; }
	inline MaterialPropertyBlock_t3213117958 ** get_address_of_materialPropertyBlock_14() { return &___materialPropertyBlock_14; }
	inline void set_materialPropertyBlock_14(MaterialPropertyBlock_t3213117958 * value)
	{
		___materialPropertyBlock_14 = value;
		Il2CppCodeGenWriteBarrier((&___materialPropertyBlock_14), value);
	}

	inline static int32_t get_offset_of_alphaId_15() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___alphaId_15)); }
	inline int32_t get_alphaId_15() const { return ___alphaId_15; }
	inline int32_t* get_address_of_alphaId_15() { return &___alphaId_15; }
	inline void set_alphaId_15(int32_t value)
	{
		___alphaId_15 = value;
	}

	inline static int32_t get_offset_of_touchId_16() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___touchId_16)); }
	inline int32_t get_touchId_16() const { return ___touchId_16; }
	inline int32_t* get_address_of_touchId_16() { return &___touchId_16; }
	inline void set_touchId_16(int32_t value)
	{
		___touchId_16 = value;
	}

	inline static int32_t get_offset_of_touchPadId_17() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___touchPadId_17)); }
	inline int32_t get_touchPadId_17() const { return ___touchPadId_17; }
	inline int32_t* get_address_of_touchPadId_17() { return &___touchPadId_17; }
	inline void set_touchPadId_17(int32_t value)
	{
		___touchPadId_17 = value;
	}

	inline static int32_t get_offset_of_appButtonId_18() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___appButtonId_18)); }
	inline int32_t get_appButtonId_18() const { return ___appButtonId_18; }
	inline int32_t* get_address_of_appButtonId_18() { return &___appButtonId_18; }
	inline void set_appButtonId_18(int32_t value)
	{
		___appButtonId_18 = value;
	}

	inline static int32_t get_offset_of_systemButtonId_19() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___systemButtonId_19)); }
	inline int32_t get_systemButtonId_19() const { return ___systemButtonId_19; }
	inline int32_t* get_address_of_systemButtonId_19() { return &___systemButtonId_19; }
	inline void set_systemButtonId_19(int32_t value)
	{
		___systemButtonId_19 = value;
	}

	inline static int32_t get_offset_of_batteryColorId_20() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___batteryColorId_20)); }
	inline int32_t get_batteryColorId_20() const { return ___batteryColorId_20; }
	inline int32_t* get_address_of_batteryColorId_20() { return &___batteryColorId_20; }
	inline void set_batteryColorId_20(int32_t value)
	{
		___batteryColorId_20 = value;
	}

	inline static int32_t get_offset_of_wasTouching_21() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___wasTouching_21)); }
	inline bool get_wasTouching_21() const { return ___wasTouching_21; }
	inline bool* get_address_of_wasTouching_21() { return &___wasTouching_21; }
	inline void set_wasTouching_21(bool value)
	{
		___wasTouching_21 = value;
	}

	inline static int32_t get_offset_of_touchTime_22() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___touchTime_22)); }
	inline float get_touchTime_22() const { return ___touchTime_22; }
	inline float* get_address_of_touchTime_22() { return &___touchTime_22; }
	inline void set_touchTime_22(float value)
	{
		___touchTime_22 = value;
	}

	inline static int32_t get_offset_of_controllerShaderData_23() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___controllerShaderData_23)); }
	inline Vector4_t3319028937  get_controllerShaderData_23() const { return ___controllerShaderData_23; }
	inline Vector4_t3319028937 * get_address_of_controllerShaderData_23() { return &___controllerShaderData_23; }
	inline void set_controllerShaderData_23(Vector4_t3319028937  value)
	{
		___controllerShaderData_23 = value;
	}

	inline static int32_t get_offset_of_controllerShaderData2_24() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___controllerShaderData2_24)); }
	inline Vector4_t3319028937  get_controllerShaderData2_24() const { return ___controllerShaderData2_24; }
	inline Vector4_t3319028937 * get_address_of_controllerShaderData2_24() { return &___controllerShaderData2_24; }
	inline void set_controllerShaderData2_24(Vector4_t3319028937  value)
	{
		___controllerShaderData2_24 = value;
	}

	inline static int32_t get_offset_of_currentBatteryColor_25() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___currentBatteryColor_25)); }
	inline Color_t2555686324  get_currentBatteryColor_25() const { return ___currentBatteryColor_25; }
	inline Color_t2555686324 * get_address_of_currentBatteryColor_25() { return &___currentBatteryColor_25; }
	inline void set_currentBatteryColor_25(Color_t2555686324  value)
	{
		___currentBatteryColor_25 = value;
	}

	inline static int32_t get_offset_of_GVR_BATTERY_CRITICAL_COLOR_40() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___GVR_BATTERY_CRITICAL_COLOR_40)); }
	inline Color_t2555686324  get_GVR_BATTERY_CRITICAL_COLOR_40() const { return ___GVR_BATTERY_CRITICAL_COLOR_40; }
	inline Color_t2555686324 * get_address_of_GVR_BATTERY_CRITICAL_COLOR_40() { return &___GVR_BATTERY_CRITICAL_COLOR_40; }
	inline void set_GVR_BATTERY_CRITICAL_COLOR_40(Color_t2555686324  value)
	{
		___GVR_BATTERY_CRITICAL_COLOR_40 = value;
	}

	inline static int32_t get_offset_of_GVR_BATTERY_LOW_COLOR_41() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___GVR_BATTERY_LOW_COLOR_41)); }
	inline Color_t2555686324  get_GVR_BATTERY_LOW_COLOR_41() const { return ___GVR_BATTERY_LOW_COLOR_41; }
	inline Color_t2555686324 * get_address_of_GVR_BATTERY_LOW_COLOR_41() { return &___GVR_BATTERY_LOW_COLOR_41; }
	inline void set_GVR_BATTERY_LOW_COLOR_41(Color_t2555686324  value)
	{
		___GVR_BATTERY_LOW_COLOR_41 = value;
	}

	inline static int32_t get_offset_of_GVR_BATTERY_MED_COLOR_42() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___GVR_BATTERY_MED_COLOR_42)); }
	inline Color_t2555686324  get_GVR_BATTERY_MED_COLOR_42() const { return ___GVR_BATTERY_MED_COLOR_42; }
	inline Color_t2555686324 * get_address_of_GVR_BATTERY_MED_COLOR_42() { return &___GVR_BATTERY_MED_COLOR_42; }
	inline void set_GVR_BATTERY_MED_COLOR_42(Color_t2555686324  value)
	{
		___GVR_BATTERY_MED_COLOR_42 = value;
	}

	inline static int32_t get_offset_of_GVR_BATTERY_FULL_COLOR_43() { return static_cast<int32_t>(offsetof(GvrControllerVisual_t3828962282, ___GVR_BATTERY_FULL_COLOR_43)); }
	inline Color_t2555686324  get_GVR_BATTERY_FULL_COLOR_43() const { return ___GVR_BATTERY_FULL_COLOR_43; }
	inline Color_t2555686324 * get_address_of_GVR_BATTERY_FULL_COLOR_43() { return &___GVR_BATTERY_FULL_COLOR_43; }
	inline void set_GVR_BATTERY_FULL_COLOR_43(Color_t2555686324  value)
	{
		___GVR_BATTERY_FULL_COLOR_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERVISUAL_T3828962282_H
#ifndef GVREDITOREMULATOR_T2879390294_H
#define GVREDITOREMULATOR_T2879390294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEditorEmulator
struct  GvrEditorEmulator_t2879390294  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREDITOREMULATOR_T2879390294_H
#ifndef GVRHEADSET_T1874684537_H
#define GVRHEADSET_T1874684537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset
struct  GvrHeadset_t1874684537  : public MonoBehaviour_t3962482529
{
public:
	// Gvr.Internal.IHeadsetProvider GvrHeadset::headsetProvider
	RuntimeObject* ___headsetProvider_5;
	// Gvr.Internal.HeadsetState GvrHeadset::headsetState
	HeadsetState_t378905490  ___headsetState_6;
	// System.Collections.IEnumerator GvrHeadset::standaloneUpdate
	RuntimeObject* ___standaloneUpdate_7;
	// UnityEngine.WaitForEndOfFrame GvrHeadset::waitForEndOfFrame
	WaitForEndOfFrame_t1314943911 * ___waitForEndOfFrame_8;
	// GvrHeadset/OnSafetyRegionEvent GvrHeadset::safetyRegionDelegate
	OnSafetyRegionEvent_t980662704 * ___safetyRegionDelegate_9;
	// GvrHeadset/OnRecenterEvent GvrHeadset::recenterDelegate
	OnRecenterEvent_t1755824842 * ___recenterDelegate_10;

public:
	inline static int32_t get_offset_of_headsetProvider_5() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___headsetProvider_5)); }
	inline RuntimeObject* get_headsetProvider_5() const { return ___headsetProvider_5; }
	inline RuntimeObject** get_address_of_headsetProvider_5() { return &___headsetProvider_5; }
	inline void set_headsetProvider_5(RuntimeObject* value)
	{
		___headsetProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___headsetProvider_5), value);
	}

	inline static int32_t get_offset_of_headsetState_6() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___headsetState_6)); }
	inline HeadsetState_t378905490  get_headsetState_6() const { return ___headsetState_6; }
	inline HeadsetState_t378905490 * get_address_of_headsetState_6() { return &___headsetState_6; }
	inline void set_headsetState_6(HeadsetState_t378905490  value)
	{
		___headsetState_6 = value;
	}

	inline static int32_t get_offset_of_standaloneUpdate_7() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___standaloneUpdate_7)); }
	inline RuntimeObject* get_standaloneUpdate_7() const { return ___standaloneUpdate_7; }
	inline RuntimeObject** get_address_of_standaloneUpdate_7() { return &___standaloneUpdate_7; }
	inline void set_standaloneUpdate_7(RuntimeObject* value)
	{
		___standaloneUpdate_7 = value;
		Il2CppCodeGenWriteBarrier((&___standaloneUpdate_7), value);
	}

	inline static int32_t get_offset_of_waitForEndOfFrame_8() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___waitForEndOfFrame_8)); }
	inline WaitForEndOfFrame_t1314943911 * get_waitForEndOfFrame_8() const { return ___waitForEndOfFrame_8; }
	inline WaitForEndOfFrame_t1314943911 ** get_address_of_waitForEndOfFrame_8() { return &___waitForEndOfFrame_8; }
	inline void set_waitForEndOfFrame_8(WaitForEndOfFrame_t1314943911 * value)
	{
		___waitForEndOfFrame_8 = value;
		Il2CppCodeGenWriteBarrier((&___waitForEndOfFrame_8), value);
	}

	inline static int32_t get_offset_of_safetyRegionDelegate_9() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___safetyRegionDelegate_9)); }
	inline OnSafetyRegionEvent_t980662704 * get_safetyRegionDelegate_9() const { return ___safetyRegionDelegate_9; }
	inline OnSafetyRegionEvent_t980662704 ** get_address_of_safetyRegionDelegate_9() { return &___safetyRegionDelegate_9; }
	inline void set_safetyRegionDelegate_9(OnSafetyRegionEvent_t980662704 * value)
	{
		___safetyRegionDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((&___safetyRegionDelegate_9), value);
	}

	inline static int32_t get_offset_of_recenterDelegate_10() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537, ___recenterDelegate_10)); }
	inline OnRecenterEvent_t1755824842 * get_recenterDelegate_10() const { return ___recenterDelegate_10; }
	inline OnRecenterEvent_t1755824842 ** get_address_of_recenterDelegate_10() { return &___recenterDelegate_10; }
	inline void set_recenterDelegate_10(OnRecenterEvent_t1755824842 * value)
	{
		___recenterDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((&___recenterDelegate_10), value);
	}
};

struct GvrHeadset_t1874684537_StaticFields
{
public:
	// GvrHeadset GvrHeadset::instance
	GvrHeadset_t1874684537 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GvrHeadset_t1874684537_StaticFields, ___instance_4)); }
	inline GvrHeadset_t1874684537 * get_instance_4() const { return ___instance_4; }
	inline GvrHeadset_t1874684537 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GvrHeadset_t1874684537 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRHEADSET_T1874684537_H
#ifndef GVRLASERVISUAL_T3710127448_H
#define GVRLASERVISUAL_T3710127448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrLaserVisual
struct  GvrLaserVisual_t3710127448  : public MonoBehaviour_t3962482529
{
public:
	// GvrControllerReticleVisual GvrLaserVisual::reticle
	GvrControllerReticleVisual_t1615767572 * ___reticle_4;
	// UnityEngine.Transform GvrLaserVisual::controller
	Transform_t3600365921 * ___controller_5;
	// UnityEngine.Color GvrLaserVisual::laserColor
	Color_t2555686324  ___laserColor_6;
	// UnityEngine.Color GvrLaserVisual::laserColorEnd
	Color_t2555686324  ___laserColorEnd_7;
	// System.Single GvrLaserVisual::maxLaserDistance
	float ___maxLaserDistance_8;
	// System.Single GvrLaserVisual::lerpSpeed
	float ___lerpSpeed_9;
	// System.Single GvrLaserVisual::lerpThreshold
	float ___lerpThreshold_10;
	// System.Boolean GvrLaserVisual::shrinkLaser
	bool ___shrinkLaser_11;
	// System.Single GvrLaserVisual::shrunkScale
	float ___shrunkScale_12;
	// System.Single GvrLaserVisual::beginShrinkAngleDegrees
	float ___beginShrinkAngleDegrees_13;
	// System.Single GvrLaserVisual::endShrinkAngleDegrees
	float ___endShrinkAngleDegrees_14;
	// GvrBaseArmModel GvrLaserVisual::<ArmModel>k__BackingField
	GvrBaseArmModel_t3382200842 * ___U3CArmModelU3Ek__BackingField_16;
	// UnityEngine.LineRenderer GvrLaserVisual::<Laser>k__BackingField
	LineRenderer_t3154350270 * ___U3CLaserU3Ek__BackingField_17;
	// GvrLaserVisual/GetPointForDistanceDelegate GvrLaserVisual::<GetPointForDistanceFunction>k__BackingField
	GetPointForDistanceDelegate_t92913150 * ___U3CGetPointForDistanceFunctionU3Ek__BackingField_18;
	// System.Single GvrLaserVisual::shrinkRatio
	float ___shrinkRatio_19;
	// System.Single GvrLaserVisual::targetDistance
	float ___targetDistance_20;
	// System.Single GvrLaserVisual::currentDistance
	float ___currentDistance_21;
	// UnityEngine.Vector3 GvrLaserVisual::currentPosition
	Vector3_t3722313464  ___currentPosition_22;
	// UnityEngine.Vector3 GvrLaserVisual::currentLocalPosition
	Vector3_t3722313464  ___currentLocalPosition_23;
	// UnityEngine.Quaternion GvrLaserVisual::currentLocalRotation
	Quaternion_t2301928331  ___currentLocalRotation_24;

public:
	inline static int32_t get_offset_of_reticle_4() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___reticle_4)); }
	inline GvrControllerReticleVisual_t1615767572 * get_reticle_4() const { return ___reticle_4; }
	inline GvrControllerReticleVisual_t1615767572 ** get_address_of_reticle_4() { return &___reticle_4; }
	inline void set_reticle_4(GvrControllerReticleVisual_t1615767572 * value)
	{
		___reticle_4 = value;
		Il2CppCodeGenWriteBarrier((&___reticle_4), value);
	}

	inline static int32_t get_offset_of_controller_5() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___controller_5)); }
	inline Transform_t3600365921 * get_controller_5() const { return ___controller_5; }
	inline Transform_t3600365921 ** get_address_of_controller_5() { return &___controller_5; }
	inline void set_controller_5(Transform_t3600365921 * value)
	{
		___controller_5 = value;
		Il2CppCodeGenWriteBarrier((&___controller_5), value);
	}

	inline static int32_t get_offset_of_laserColor_6() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___laserColor_6)); }
	inline Color_t2555686324  get_laserColor_6() const { return ___laserColor_6; }
	inline Color_t2555686324 * get_address_of_laserColor_6() { return &___laserColor_6; }
	inline void set_laserColor_6(Color_t2555686324  value)
	{
		___laserColor_6 = value;
	}

	inline static int32_t get_offset_of_laserColorEnd_7() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___laserColorEnd_7)); }
	inline Color_t2555686324  get_laserColorEnd_7() const { return ___laserColorEnd_7; }
	inline Color_t2555686324 * get_address_of_laserColorEnd_7() { return &___laserColorEnd_7; }
	inline void set_laserColorEnd_7(Color_t2555686324  value)
	{
		___laserColorEnd_7 = value;
	}

	inline static int32_t get_offset_of_maxLaserDistance_8() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___maxLaserDistance_8)); }
	inline float get_maxLaserDistance_8() const { return ___maxLaserDistance_8; }
	inline float* get_address_of_maxLaserDistance_8() { return &___maxLaserDistance_8; }
	inline void set_maxLaserDistance_8(float value)
	{
		___maxLaserDistance_8 = value;
	}

	inline static int32_t get_offset_of_lerpSpeed_9() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___lerpSpeed_9)); }
	inline float get_lerpSpeed_9() const { return ___lerpSpeed_9; }
	inline float* get_address_of_lerpSpeed_9() { return &___lerpSpeed_9; }
	inline void set_lerpSpeed_9(float value)
	{
		___lerpSpeed_9 = value;
	}

	inline static int32_t get_offset_of_lerpThreshold_10() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___lerpThreshold_10)); }
	inline float get_lerpThreshold_10() const { return ___lerpThreshold_10; }
	inline float* get_address_of_lerpThreshold_10() { return &___lerpThreshold_10; }
	inline void set_lerpThreshold_10(float value)
	{
		___lerpThreshold_10 = value;
	}

	inline static int32_t get_offset_of_shrinkLaser_11() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___shrinkLaser_11)); }
	inline bool get_shrinkLaser_11() const { return ___shrinkLaser_11; }
	inline bool* get_address_of_shrinkLaser_11() { return &___shrinkLaser_11; }
	inline void set_shrinkLaser_11(bool value)
	{
		___shrinkLaser_11 = value;
	}

	inline static int32_t get_offset_of_shrunkScale_12() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___shrunkScale_12)); }
	inline float get_shrunkScale_12() const { return ___shrunkScale_12; }
	inline float* get_address_of_shrunkScale_12() { return &___shrunkScale_12; }
	inline void set_shrunkScale_12(float value)
	{
		___shrunkScale_12 = value;
	}

	inline static int32_t get_offset_of_beginShrinkAngleDegrees_13() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___beginShrinkAngleDegrees_13)); }
	inline float get_beginShrinkAngleDegrees_13() const { return ___beginShrinkAngleDegrees_13; }
	inline float* get_address_of_beginShrinkAngleDegrees_13() { return &___beginShrinkAngleDegrees_13; }
	inline void set_beginShrinkAngleDegrees_13(float value)
	{
		___beginShrinkAngleDegrees_13 = value;
	}

	inline static int32_t get_offset_of_endShrinkAngleDegrees_14() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___endShrinkAngleDegrees_14)); }
	inline float get_endShrinkAngleDegrees_14() const { return ___endShrinkAngleDegrees_14; }
	inline float* get_address_of_endShrinkAngleDegrees_14() { return &___endShrinkAngleDegrees_14; }
	inline void set_endShrinkAngleDegrees_14(float value)
	{
		___endShrinkAngleDegrees_14 = value;
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___U3CArmModelU3Ek__BackingField_16)); }
	inline GvrBaseArmModel_t3382200842 * get_U3CArmModelU3Ek__BackingField_16() const { return ___U3CArmModelU3Ek__BackingField_16; }
	inline GvrBaseArmModel_t3382200842 ** get_address_of_U3CArmModelU3Ek__BackingField_16() { return &___U3CArmModelU3Ek__BackingField_16; }
	inline void set_U3CArmModelU3Ek__BackingField_16(GvrBaseArmModel_t3382200842 * value)
	{
		___U3CArmModelU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CLaserU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___U3CLaserU3Ek__BackingField_17)); }
	inline LineRenderer_t3154350270 * get_U3CLaserU3Ek__BackingField_17() const { return ___U3CLaserU3Ek__BackingField_17; }
	inline LineRenderer_t3154350270 ** get_address_of_U3CLaserU3Ek__BackingField_17() { return &___U3CLaserU3Ek__BackingField_17; }
	inline void set_U3CLaserU3Ek__BackingField_17(LineRenderer_t3154350270 * value)
	{
		___U3CLaserU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLaserU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CGetPointForDistanceFunctionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___U3CGetPointForDistanceFunctionU3Ek__BackingField_18)); }
	inline GetPointForDistanceDelegate_t92913150 * get_U3CGetPointForDistanceFunctionU3Ek__BackingField_18() const { return ___U3CGetPointForDistanceFunctionU3Ek__BackingField_18; }
	inline GetPointForDistanceDelegate_t92913150 ** get_address_of_U3CGetPointForDistanceFunctionU3Ek__BackingField_18() { return &___U3CGetPointForDistanceFunctionU3Ek__BackingField_18; }
	inline void set_U3CGetPointForDistanceFunctionU3Ek__BackingField_18(GetPointForDistanceDelegate_t92913150 * value)
	{
		___U3CGetPointForDistanceFunctionU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetPointForDistanceFunctionU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_shrinkRatio_19() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___shrinkRatio_19)); }
	inline float get_shrinkRatio_19() const { return ___shrinkRatio_19; }
	inline float* get_address_of_shrinkRatio_19() { return &___shrinkRatio_19; }
	inline void set_shrinkRatio_19(float value)
	{
		___shrinkRatio_19 = value;
	}

	inline static int32_t get_offset_of_targetDistance_20() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___targetDistance_20)); }
	inline float get_targetDistance_20() const { return ___targetDistance_20; }
	inline float* get_address_of_targetDistance_20() { return &___targetDistance_20; }
	inline void set_targetDistance_20(float value)
	{
		___targetDistance_20 = value;
	}

	inline static int32_t get_offset_of_currentDistance_21() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___currentDistance_21)); }
	inline float get_currentDistance_21() const { return ___currentDistance_21; }
	inline float* get_address_of_currentDistance_21() { return &___currentDistance_21; }
	inline void set_currentDistance_21(float value)
	{
		___currentDistance_21 = value;
	}

	inline static int32_t get_offset_of_currentPosition_22() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___currentPosition_22)); }
	inline Vector3_t3722313464  get_currentPosition_22() const { return ___currentPosition_22; }
	inline Vector3_t3722313464 * get_address_of_currentPosition_22() { return &___currentPosition_22; }
	inline void set_currentPosition_22(Vector3_t3722313464  value)
	{
		___currentPosition_22 = value;
	}

	inline static int32_t get_offset_of_currentLocalPosition_23() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___currentLocalPosition_23)); }
	inline Vector3_t3722313464  get_currentLocalPosition_23() const { return ___currentLocalPosition_23; }
	inline Vector3_t3722313464 * get_address_of_currentLocalPosition_23() { return &___currentLocalPosition_23; }
	inline void set_currentLocalPosition_23(Vector3_t3722313464  value)
	{
		___currentLocalPosition_23 = value;
	}

	inline static int32_t get_offset_of_currentLocalRotation_24() { return static_cast<int32_t>(offsetof(GvrLaserVisual_t3710127448, ___currentLocalRotation_24)); }
	inline Quaternion_t2301928331  get_currentLocalRotation_24() const { return ___currentLocalRotation_24; }
	inline Quaternion_t2301928331 * get_address_of_currentLocalRotation_24() { return &___currentLocalRotation_24; }
	inline void set_currentLocalRotation_24(Quaternion_t2301928331  value)
	{
		___currentLocalRotation_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRLASERVISUAL_T3710127448_H
#ifndef GVRPOINTERMANAGER_T310390967_H
#define GVRPOINTERMANAGER_T310390967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerManager
struct  GvrPointerManager_t310390967  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERMANAGER_T310390967_H
#ifndef GVRRECENTERONLYCONTROLLER_T1827745746_H
#define GVRRECENTERONLYCONTROLLER_T1827745746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterOnlyController
struct  GvrRecenterOnlyController_t1827745746  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Quaternion GvrRecenterOnlyController::lastAppliedYawCorrection
	Quaternion_t2301928331  ___lastAppliedYawCorrection_4;
	// UnityEngine.Quaternion GvrRecenterOnlyController::yawCorrection
	Quaternion_t2301928331  ___yawCorrection_5;

public:
	inline static int32_t get_offset_of_lastAppliedYawCorrection_4() { return static_cast<int32_t>(offsetof(GvrRecenterOnlyController_t1827745746, ___lastAppliedYawCorrection_4)); }
	inline Quaternion_t2301928331  get_lastAppliedYawCorrection_4() const { return ___lastAppliedYawCorrection_4; }
	inline Quaternion_t2301928331 * get_address_of_lastAppliedYawCorrection_4() { return &___lastAppliedYawCorrection_4; }
	inline void set_lastAppliedYawCorrection_4(Quaternion_t2301928331  value)
	{
		___lastAppliedYawCorrection_4 = value;
	}

	inline static int32_t get_offset_of_yawCorrection_5() { return static_cast<int32_t>(offsetof(GvrRecenterOnlyController_t1827745746, ___yawCorrection_5)); }
	inline Quaternion_t2301928331  get_yawCorrection_5() const { return ___yawCorrection_5; }
	inline Quaternion_t2301928331 * get_address_of_yawCorrection_5() { return &___yawCorrection_5; }
	inline void set_yawCorrection_5(Quaternion_t2301928331  value)
	{
		___yawCorrection_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTERONLYCONTROLLER_T1827745746_H
#ifndef GVRSCROLLSETTINGS_T33837608_H
#define GVRSCROLLSETTINGS_T33837608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrScrollSettings
struct  GvrScrollSettings_t33837608  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GvrScrollSettings::inertiaOverride
	bool ___inertiaOverride_4;
	// System.Single GvrScrollSettings::decelerationRateOverride
	float ___decelerationRateOverride_5;

public:
	inline static int32_t get_offset_of_inertiaOverride_4() { return static_cast<int32_t>(offsetof(GvrScrollSettings_t33837608, ___inertiaOverride_4)); }
	inline bool get_inertiaOverride_4() const { return ___inertiaOverride_4; }
	inline bool* get_address_of_inertiaOverride_4() { return &___inertiaOverride_4; }
	inline void set_inertiaOverride_4(bool value)
	{
		___inertiaOverride_4 = value;
	}

	inline static int32_t get_offset_of_decelerationRateOverride_5() { return static_cast<int32_t>(offsetof(GvrScrollSettings_t33837608, ___decelerationRateOverride_5)); }
	inline float get_decelerationRateOverride_5() const { return ___decelerationRateOverride_5; }
	inline float* get_address_of_decelerationRateOverride_5() { return &___decelerationRateOverride_5; }
	inline void set_decelerationRateOverride_5(float value)
	{
		___decelerationRateOverride_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSCROLLSETTINGS_T33837608_H
#ifndef GVRTOOLTIP_T491261851_H
#define GVRTOOLTIP_T491261851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrTooltip
struct  GvrTooltip_t491261851  : public MonoBehaviour_t3962482529
{
public:
	// GvrTooltip/Location GvrTooltip::location
	int32_t ___location_11;
	// UnityEngine.UI.Text GvrTooltip::text
	Text_t1901882714 * ___text_12;
	// System.Boolean GvrTooltip::alwaysVisible
	bool ___alwaysVisible_13;
	// System.Boolean GvrTooltip::isOnLeft
	bool ___isOnLeft_14;
	// UnityEngine.RectTransform GvrTooltip::rectTransform
	RectTransform_t3704657025 * ___rectTransform_15;
	// UnityEngine.CanvasGroup GvrTooltip::canvasGroup
	CanvasGroup_t4083511760 * ___canvasGroup_16;
	// GvrBaseArmModel GvrTooltip::<ArmModel>k__BackingField
	GvrBaseArmModel_t3382200842 * ___U3CArmModelU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_location_11() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___location_11)); }
	inline int32_t get_location_11() const { return ___location_11; }
	inline int32_t* get_address_of_location_11() { return &___location_11; }
	inline void set_location_11(int32_t value)
	{
		___location_11 = value;
	}

	inline static int32_t get_offset_of_text_12() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___text_12)); }
	inline Text_t1901882714 * get_text_12() const { return ___text_12; }
	inline Text_t1901882714 ** get_address_of_text_12() { return &___text_12; }
	inline void set_text_12(Text_t1901882714 * value)
	{
		___text_12 = value;
		Il2CppCodeGenWriteBarrier((&___text_12), value);
	}

	inline static int32_t get_offset_of_alwaysVisible_13() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___alwaysVisible_13)); }
	inline bool get_alwaysVisible_13() const { return ___alwaysVisible_13; }
	inline bool* get_address_of_alwaysVisible_13() { return &___alwaysVisible_13; }
	inline void set_alwaysVisible_13(bool value)
	{
		___alwaysVisible_13 = value;
	}

	inline static int32_t get_offset_of_isOnLeft_14() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___isOnLeft_14)); }
	inline bool get_isOnLeft_14() const { return ___isOnLeft_14; }
	inline bool* get_address_of_isOnLeft_14() { return &___isOnLeft_14; }
	inline void set_isOnLeft_14(bool value)
	{
		___isOnLeft_14 = value;
	}

	inline static int32_t get_offset_of_rectTransform_15() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___rectTransform_15)); }
	inline RectTransform_t3704657025 * get_rectTransform_15() const { return ___rectTransform_15; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_15() { return &___rectTransform_15; }
	inline void set_rectTransform_15(RectTransform_t3704657025 * value)
	{
		___rectTransform_15 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_15), value);
	}

	inline static int32_t get_offset_of_canvasGroup_16() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___canvasGroup_16)); }
	inline CanvasGroup_t4083511760 * get_canvasGroup_16() const { return ___canvasGroup_16; }
	inline CanvasGroup_t4083511760 ** get_address_of_canvasGroup_16() { return &___canvasGroup_16; }
	inline void set_canvasGroup_16(CanvasGroup_t4083511760 * value)
	{
		___canvasGroup_16 = value;
		Il2CppCodeGenWriteBarrier((&___canvasGroup_16), value);
	}

	inline static int32_t get_offset_of_U3CArmModelU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851, ___U3CArmModelU3Ek__BackingField_17)); }
	inline GvrBaseArmModel_t3382200842 * get_U3CArmModelU3Ek__BackingField_17() const { return ___U3CArmModelU3Ek__BackingField_17; }
	inline GvrBaseArmModel_t3382200842 ** get_address_of_U3CArmModelU3Ek__BackingField_17() { return &___U3CArmModelU3Ek__BackingField_17; }
	inline void set_U3CArmModelU3Ek__BackingField_17(GvrBaseArmModel_t3382200842 * value)
	{
		___U3CArmModelU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArmModelU3Ek__BackingField_17), value);
	}
};

struct GvrTooltip_t491261851_StaticFields
{
public:
	// UnityEngine.Quaternion GvrTooltip::RIGHT_SIDE_ROTATION
	Quaternion_t2301928331  ___RIGHT_SIDE_ROTATION_4;
	// UnityEngine.Quaternion GvrTooltip::LEFT_SIDE_ROTATION
	Quaternion_t2301928331  ___LEFT_SIDE_ROTATION_5;
	// UnityEngine.Vector2 GvrTooltip::SQUARE_CENTER
	Vector2_t2156229523  ___SQUARE_CENTER_6;
	// UnityEngine.Vector2 GvrTooltip::PIVOT
	Vector2_t2156229523  ___PIVOT_7;

public:
	inline static int32_t get_offset_of_RIGHT_SIDE_ROTATION_4() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851_StaticFields, ___RIGHT_SIDE_ROTATION_4)); }
	inline Quaternion_t2301928331  get_RIGHT_SIDE_ROTATION_4() const { return ___RIGHT_SIDE_ROTATION_4; }
	inline Quaternion_t2301928331 * get_address_of_RIGHT_SIDE_ROTATION_4() { return &___RIGHT_SIDE_ROTATION_4; }
	inline void set_RIGHT_SIDE_ROTATION_4(Quaternion_t2301928331  value)
	{
		___RIGHT_SIDE_ROTATION_4 = value;
	}

	inline static int32_t get_offset_of_LEFT_SIDE_ROTATION_5() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851_StaticFields, ___LEFT_SIDE_ROTATION_5)); }
	inline Quaternion_t2301928331  get_LEFT_SIDE_ROTATION_5() const { return ___LEFT_SIDE_ROTATION_5; }
	inline Quaternion_t2301928331 * get_address_of_LEFT_SIDE_ROTATION_5() { return &___LEFT_SIDE_ROTATION_5; }
	inline void set_LEFT_SIDE_ROTATION_5(Quaternion_t2301928331  value)
	{
		___LEFT_SIDE_ROTATION_5 = value;
	}

	inline static int32_t get_offset_of_SQUARE_CENTER_6() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851_StaticFields, ___SQUARE_CENTER_6)); }
	inline Vector2_t2156229523  get_SQUARE_CENTER_6() const { return ___SQUARE_CENTER_6; }
	inline Vector2_t2156229523 * get_address_of_SQUARE_CENTER_6() { return &___SQUARE_CENTER_6; }
	inline void set_SQUARE_CENTER_6(Vector2_t2156229523  value)
	{
		___SQUARE_CENTER_6 = value;
	}

	inline static int32_t get_offset_of_PIVOT_7() { return static_cast<int32_t>(offsetof(GvrTooltip_t491261851_StaticFields, ___PIVOT_7)); }
	inline Vector2_t2156229523  get_PIVOT_7() const { return ___PIVOT_7; }
	inline Vector2_t2156229523 * get_address_of_PIVOT_7() { return &___PIVOT_7; }
	inline void set_PIVOT_7(Vector2_t2156229523  value)
	{
		___PIVOT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRTOOLTIP_T491261851_H
#ifndef GVRTRACKEDCONTROLLER_T2964803478_H
#define GVRTRACKEDCONTROLLER_T2964803478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrTrackedController
struct  GvrTrackedController_t2964803478  : public MonoBehaviour_t3962482529
{
public:
	// GvrBaseArmModel GvrTrackedController::armModel
	GvrBaseArmModel_t3382200842 * ___armModel_4;
	// GvrControllerInputDevice GvrTrackedController::controllerInputDevice
	GvrControllerInputDevice_t3709172113 * ___controllerInputDevice_5;
	// System.Boolean GvrTrackedController::isDeactivatedWhenDisconnected
	bool ___isDeactivatedWhenDisconnected_6;
	// GvrControllerHand GvrTrackedController::controllerHand
	int32_t ___controllerHand_7;

public:
	inline static int32_t get_offset_of_armModel_4() { return static_cast<int32_t>(offsetof(GvrTrackedController_t2964803478, ___armModel_4)); }
	inline GvrBaseArmModel_t3382200842 * get_armModel_4() const { return ___armModel_4; }
	inline GvrBaseArmModel_t3382200842 ** get_address_of_armModel_4() { return &___armModel_4; }
	inline void set_armModel_4(GvrBaseArmModel_t3382200842 * value)
	{
		___armModel_4 = value;
		Il2CppCodeGenWriteBarrier((&___armModel_4), value);
	}

	inline static int32_t get_offset_of_controllerInputDevice_5() { return static_cast<int32_t>(offsetof(GvrTrackedController_t2964803478, ___controllerInputDevice_5)); }
	inline GvrControllerInputDevice_t3709172113 * get_controllerInputDevice_5() const { return ___controllerInputDevice_5; }
	inline GvrControllerInputDevice_t3709172113 ** get_address_of_controllerInputDevice_5() { return &___controllerInputDevice_5; }
	inline void set_controllerInputDevice_5(GvrControllerInputDevice_t3709172113 * value)
	{
		___controllerInputDevice_5 = value;
		Il2CppCodeGenWriteBarrier((&___controllerInputDevice_5), value);
	}

	inline static int32_t get_offset_of_isDeactivatedWhenDisconnected_6() { return static_cast<int32_t>(offsetof(GvrTrackedController_t2964803478, ___isDeactivatedWhenDisconnected_6)); }
	inline bool get_isDeactivatedWhenDisconnected_6() const { return ___isDeactivatedWhenDisconnected_6; }
	inline bool* get_address_of_isDeactivatedWhenDisconnected_6() { return &___isDeactivatedWhenDisconnected_6; }
	inline void set_isDeactivatedWhenDisconnected_6(bool value)
	{
		___isDeactivatedWhenDisconnected_6 = value;
	}

	inline static int32_t get_offset_of_controllerHand_7() { return static_cast<int32_t>(offsetof(GvrTrackedController_t2964803478, ___controllerHand_7)); }
	inline int32_t get_controllerHand_7() const { return ___controllerHand_7; }
	inline int32_t* get_address_of_controllerHand_7() { return &___controllerHand_7; }
	inline void set_controllerHand_7(int32_t value)
	{
		___controllerHand_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRTRACKEDCONTROLLER_T2964803478_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef GVRARMMODEL_T2461504920_H
#define GVRARMMODEL_T2461504920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrArmModel
struct  GvrArmModel_t2461504920  : public GvrBaseArmModel_t3382200842
{
public:
	// UnityEngine.Vector3 GvrArmModel::elbowRestPosition
	Vector3_t3722313464  ___elbowRestPosition_4;
	// UnityEngine.Vector3 GvrArmModel::wristRestPosition
	Vector3_t3722313464  ___wristRestPosition_5;
	// UnityEngine.Vector3 GvrArmModel::controllerRestPosition
	Vector3_t3722313464  ___controllerRestPosition_6;
	// UnityEngine.Vector3 GvrArmModel::armExtensionOffset
	Vector3_t3722313464  ___armExtensionOffset_7;
	// System.Single GvrArmModel::elbowBendRatio
	float ___elbowBendRatio_8;
	// System.Single GvrArmModel::fadeControllerOffset
	float ___fadeControllerOffset_9;
	// System.Single GvrArmModel::fadeDistanceFromHeadForward
	float ___fadeDistanceFromHeadForward_10;
	// System.Single GvrArmModel::fadeDistanceFromHeadSide
	float ___fadeDistanceFromHeadSide_11;
	// System.Single GvrArmModel::tooltipMinDistanceFromFace
	float ___tooltipMinDistanceFromFace_12;
	// System.Int32 GvrArmModel::tooltipMaxAngleFromCamera
	int32_t ___tooltipMaxAngleFromCamera_13;
	// System.Boolean GvrArmModel::isLockedToNeck
	bool ___isLockedToNeck_14;
	// GvrControllerInputDevice GvrArmModel::<ControllerInputDevice>k__BackingField
	GvrControllerInputDevice_t3709172113 * ___U3CControllerInputDeviceU3Ek__BackingField_15;
	// UnityEngine.Vector3 GvrArmModel::neckPosition
	Vector3_t3722313464  ___neckPosition_16;
	// UnityEngine.Vector3 GvrArmModel::elbowPosition
	Vector3_t3722313464  ___elbowPosition_17;
	// UnityEngine.Quaternion GvrArmModel::elbowRotation
	Quaternion_t2301928331  ___elbowRotation_18;
	// UnityEngine.Vector3 GvrArmModel::wristPosition
	Vector3_t3722313464  ___wristPosition_19;
	// UnityEngine.Quaternion GvrArmModel::wristRotation
	Quaternion_t2301928331  ___wristRotation_20;
	// UnityEngine.Vector3 GvrArmModel::controllerPosition
	Vector3_t3722313464  ___controllerPosition_21;
	// UnityEngine.Quaternion GvrArmModel::controllerRotation
	Quaternion_t2301928331  ___controllerRotation_22;
	// System.Single GvrArmModel::preferredAlpha
	float ___preferredAlpha_23;
	// System.Single GvrArmModel::tooltipAlphaValue
	float ___tooltipAlphaValue_24;
	// UnityEngine.Vector3 GvrArmModel::handedMultiplier
	Vector3_t3722313464  ___handedMultiplier_25;
	// UnityEngine.Vector3 GvrArmModel::torsoDirection
	Vector3_t3722313464  ___torsoDirection_26;
	// UnityEngine.Quaternion GvrArmModel::torsoRotation
	Quaternion_t2301928331  ___torsoRotation_27;

public:
	inline static int32_t get_offset_of_elbowRestPosition_4() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___elbowRestPosition_4)); }
	inline Vector3_t3722313464  get_elbowRestPosition_4() const { return ___elbowRestPosition_4; }
	inline Vector3_t3722313464 * get_address_of_elbowRestPosition_4() { return &___elbowRestPosition_4; }
	inline void set_elbowRestPosition_4(Vector3_t3722313464  value)
	{
		___elbowRestPosition_4 = value;
	}

	inline static int32_t get_offset_of_wristRestPosition_5() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___wristRestPosition_5)); }
	inline Vector3_t3722313464  get_wristRestPosition_5() const { return ___wristRestPosition_5; }
	inline Vector3_t3722313464 * get_address_of_wristRestPosition_5() { return &___wristRestPosition_5; }
	inline void set_wristRestPosition_5(Vector3_t3722313464  value)
	{
		___wristRestPosition_5 = value;
	}

	inline static int32_t get_offset_of_controllerRestPosition_6() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___controllerRestPosition_6)); }
	inline Vector3_t3722313464  get_controllerRestPosition_6() const { return ___controllerRestPosition_6; }
	inline Vector3_t3722313464 * get_address_of_controllerRestPosition_6() { return &___controllerRestPosition_6; }
	inline void set_controllerRestPosition_6(Vector3_t3722313464  value)
	{
		___controllerRestPosition_6 = value;
	}

	inline static int32_t get_offset_of_armExtensionOffset_7() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___armExtensionOffset_7)); }
	inline Vector3_t3722313464  get_armExtensionOffset_7() const { return ___armExtensionOffset_7; }
	inline Vector3_t3722313464 * get_address_of_armExtensionOffset_7() { return &___armExtensionOffset_7; }
	inline void set_armExtensionOffset_7(Vector3_t3722313464  value)
	{
		___armExtensionOffset_7 = value;
	}

	inline static int32_t get_offset_of_elbowBendRatio_8() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___elbowBendRatio_8)); }
	inline float get_elbowBendRatio_8() const { return ___elbowBendRatio_8; }
	inline float* get_address_of_elbowBendRatio_8() { return &___elbowBendRatio_8; }
	inline void set_elbowBendRatio_8(float value)
	{
		___elbowBendRatio_8 = value;
	}

	inline static int32_t get_offset_of_fadeControllerOffset_9() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___fadeControllerOffset_9)); }
	inline float get_fadeControllerOffset_9() const { return ___fadeControllerOffset_9; }
	inline float* get_address_of_fadeControllerOffset_9() { return &___fadeControllerOffset_9; }
	inline void set_fadeControllerOffset_9(float value)
	{
		___fadeControllerOffset_9 = value;
	}

	inline static int32_t get_offset_of_fadeDistanceFromHeadForward_10() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___fadeDistanceFromHeadForward_10)); }
	inline float get_fadeDistanceFromHeadForward_10() const { return ___fadeDistanceFromHeadForward_10; }
	inline float* get_address_of_fadeDistanceFromHeadForward_10() { return &___fadeDistanceFromHeadForward_10; }
	inline void set_fadeDistanceFromHeadForward_10(float value)
	{
		___fadeDistanceFromHeadForward_10 = value;
	}

	inline static int32_t get_offset_of_fadeDistanceFromHeadSide_11() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___fadeDistanceFromHeadSide_11)); }
	inline float get_fadeDistanceFromHeadSide_11() const { return ___fadeDistanceFromHeadSide_11; }
	inline float* get_address_of_fadeDistanceFromHeadSide_11() { return &___fadeDistanceFromHeadSide_11; }
	inline void set_fadeDistanceFromHeadSide_11(float value)
	{
		___fadeDistanceFromHeadSide_11 = value;
	}

	inline static int32_t get_offset_of_tooltipMinDistanceFromFace_12() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___tooltipMinDistanceFromFace_12)); }
	inline float get_tooltipMinDistanceFromFace_12() const { return ___tooltipMinDistanceFromFace_12; }
	inline float* get_address_of_tooltipMinDistanceFromFace_12() { return &___tooltipMinDistanceFromFace_12; }
	inline void set_tooltipMinDistanceFromFace_12(float value)
	{
		___tooltipMinDistanceFromFace_12 = value;
	}

	inline static int32_t get_offset_of_tooltipMaxAngleFromCamera_13() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___tooltipMaxAngleFromCamera_13)); }
	inline int32_t get_tooltipMaxAngleFromCamera_13() const { return ___tooltipMaxAngleFromCamera_13; }
	inline int32_t* get_address_of_tooltipMaxAngleFromCamera_13() { return &___tooltipMaxAngleFromCamera_13; }
	inline void set_tooltipMaxAngleFromCamera_13(int32_t value)
	{
		___tooltipMaxAngleFromCamera_13 = value;
	}

	inline static int32_t get_offset_of_isLockedToNeck_14() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___isLockedToNeck_14)); }
	inline bool get_isLockedToNeck_14() const { return ___isLockedToNeck_14; }
	inline bool* get_address_of_isLockedToNeck_14() { return &___isLockedToNeck_14; }
	inline void set_isLockedToNeck_14(bool value)
	{
		___isLockedToNeck_14 = value;
	}

	inline static int32_t get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___U3CControllerInputDeviceU3Ek__BackingField_15)); }
	inline GvrControllerInputDevice_t3709172113 * get_U3CControllerInputDeviceU3Ek__BackingField_15() const { return ___U3CControllerInputDeviceU3Ek__BackingField_15; }
	inline GvrControllerInputDevice_t3709172113 ** get_address_of_U3CControllerInputDeviceU3Ek__BackingField_15() { return &___U3CControllerInputDeviceU3Ek__BackingField_15; }
	inline void set_U3CControllerInputDeviceU3Ek__BackingField_15(GvrControllerInputDevice_t3709172113 * value)
	{
		___U3CControllerInputDeviceU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CControllerInputDeviceU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_neckPosition_16() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___neckPosition_16)); }
	inline Vector3_t3722313464  get_neckPosition_16() const { return ___neckPosition_16; }
	inline Vector3_t3722313464 * get_address_of_neckPosition_16() { return &___neckPosition_16; }
	inline void set_neckPosition_16(Vector3_t3722313464  value)
	{
		___neckPosition_16 = value;
	}

	inline static int32_t get_offset_of_elbowPosition_17() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___elbowPosition_17)); }
	inline Vector3_t3722313464  get_elbowPosition_17() const { return ___elbowPosition_17; }
	inline Vector3_t3722313464 * get_address_of_elbowPosition_17() { return &___elbowPosition_17; }
	inline void set_elbowPosition_17(Vector3_t3722313464  value)
	{
		___elbowPosition_17 = value;
	}

	inline static int32_t get_offset_of_elbowRotation_18() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___elbowRotation_18)); }
	inline Quaternion_t2301928331  get_elbowRotation_18() const { return ___elbowRotation_18; }
	inline Quaternion_t2301928331 * get_address_of_elbowRotation_18() { return &___elbowRotation_18; }
	inline void set_elbowRotation_18(Quaternion_t2301928331  value)
	{
		___elbowRotation_18 = value;
	}

	inline static int32_t get_offset_of_wristPosition_19() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___wristPosition_19)); }
	inline Vector3_t3722313464  get_wristPosition_19() const { return ___wristPosition_19; }
	inline Vector3_t3722313464 * get_address_of_wristPosition_19() { return &___wristPosition_19; }
	inline void set_wristPosition_19(Vector3_t3722313464  value)
	{
		___wristPosition_19 = value;
	}

	inline static int32_t get_offset_of_wristRotation_20() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___wristRotation_20)); }
	inline Quaternion_t2301928331  get_wristRotation_20() const { return ___wristRotation_20; }
	inline Quaternion_t2301928331 * get_address_of_wristRotation_20() { return &___wristRotation_20; }
	inline void set_wristRotation_20(Quaternion_t2301928331  value)
	{
		___wristRotation_20 = value;
	}

	inline static int32_t get_offset_of_controllerPosition_21() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___controllerPosition_21)); }
	inline Vector3_t3722313464  get_controllerPosition_21() const { return ___controllerPosition_21; }
	inline Vector3_t3722313464 * get_address_of_controllerPosition_21() { return &___controllerPosition_21; }
	inline void set_controllerPosition_21(Vector3_t3722313464  value)
	{
		___controllerPosition_21 = value;
	}

	inline static int32_t get_offset_of_controllerRotation_22() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___controllerRotation_22)); }
	inline Quaternion_t2301928331  get_controllerRotation_22() const { return ___controllerRotation_22; }
	inline Quaternion_t2301928331 * get_address_of_controllerRotation_22() { return &___controllerRotation_22; }
	inline void set_controllerRotation_22(Quaternion_t2301928331  value)
	{
		___controllerRotation_22 = value;
	}

	inline static int32_t get_offset_of_preferredAlpha_23() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___preferredAlpha_23)); }
	inline float get_preferredAlpha_23() const { return ___preferredAlpha_23; }
	inline float* get_address_of_preferredAlpha_23() { return &___preferredAlpha_23; }
	inline void set_preferredAlpha_23(float value)
	{
		___preferredAlpha_23 = value;
	}

	inline static int32_t get_offset_of_tooltipAlphaValue_24() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___tooltipAlphaValue_24)); }
	inline float get_tooltipAlphaValue_24() const { return ___tooltipAlphaValue_24; }
	inline float* get_address_of_tooltipAlphaValue_24() { return &___tooltipAlphaValue_24; }
	inline void set_tooltipAlphaValue_24(float value)
	{
		___tooltipAlphaValue_24 = value;
	}

	inline static int32_t get_offset_of_handedMultiplier_25() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___handedMultiplier_25)); }
	inline Vector3_t3722313464  get_handedMultiplier_25() const { return ___handedMultiplier_25; }
	inline Vector3_t3722313464 * get_address_of_handedMultiplier_25() { return &___handedMultiplier_25; }
	inline void set_handedMultiplier_25(Vector3_t3722313464  value)
	{
		___handedMultiplier_25 = value;
	}

	inline static int32_t get_offset_of_torsoDirection_26() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___torsoDirection_26)); }
	inline Vector3_t3722313464  get_torsoDirection_26() const { return ___torsoDirection_26; }
	inline Vector3_t3722313464 * get_address_of_torsoDirection_26() { return &___torsoDirection_26; }
	inline void set_torsoDirection_26(Vector3_t3722313464  value)
	{
		___torsoDirection_26 = value;
	}

	inline static int32_t get_offset_of_torsoRotation_27() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920, ___torsoRotation_27)); }
	inline Quaternion_t2301928331  get_torsoRotation_27() const { return ___torsoRotation_27; }
	inline Quaternion_t2301928331 * get_address_of_torsoRotation_27() { return &___torsoRotation_27; }
	inline void set_torsoRotation_27(Quaternion_t2301928331  value)
	{
		___torsoRotation_27 = value;
	}
};

struct GvrArmModel_t2461504920_StaticFields
{
public:
	// UnityEngine.Vector3 GvrArmModel::DEFAULT_ELBOW_REST_POSITION
	Vector3_t3722313464  ___DEFAULT_ELBOW_REST_POSITION_28;
	// UnityEngine.Vector3 GvrArmModel::DEFAULT_WRIST_REST_POSITION
	Vector3_t3722313464  ___DEFAULT_WRIST_REST_POSITION_29;
	// UnityEngine.Vector3 GvrArmModel::DEFAULT_CONTROLLER_REST_POSITION
	Vector3_t3722313464  ___DEFAULT_CONTROLLER_REST_POSITION_30;
	// UnityEngine.Vector3 GvrArmModel::DEFAULT_ARM_EXTENSION_OFFSET
	Vector3_t3722313464  ___DEFAULT_ARM_EXTENSION_OFFSET_31;
	// UnityEngine.Vector3 GvrArmModel::SHOULDER_POSITION
	Vector3_t3722313464  ___SHOULDER_POSITION_34;
	// UnityEngine.Vector3 GvrArmModel::NECK_OFFSET
	Vector3_t3722313464  ___NECK_OFFSET_35;

public:
	inline static int32_t get_offset_of_DEFAULT_ELBOW_REST_POSITION_28() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920_StaticFields, ___DEFAULT_ELBOW_REST_POSITION_28)); }
	inline Vector3_t3722313464  get_DEFAULT_ELBOW_REST_POSITION_28() const { return ___DEFAULT_ELBOW_REST_POSITION_28; }
	inline Vector3_t3722313464 * get_address_of_DEFAULT_ELBOW_REST_POSITION_28() { return &___DEFAULT_ELBOW_REST_POSITION_28; }
	inline void set_DEFAULT_ELBOW_REST_POSITION_28(Vector3_t3722313464  value)
	{
		___DEFAULT_ELBOW_REST_POSITION_28 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_WRIST_REST_POSITION_29() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920_StaticFields, ___DEFAULT_WRIST_REST_POSITION_29)); }
	inline Vector3_t3722313464  get_DEFAULT_WRIST_REST_POSITION_29() const { return ___DEFAULT_WRIST_REST_POSITION_29; }
	inline Vector3_t3722313464 * get_address_of_DEFAULT_WRIST_REST_POSITION_29() { return &___DEFAULT_WRIST_REST_POSITION_29; }
	inline void set_DEFAULT_WRIST_REST_POSITION_29(Vector3_t3722313464  value)
	{
		___DEFAULT_WRIST_REST_POSITION_29 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_CONTROLLER_REST_POSITION_30() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920_StaticFields, ___DEFAULT_CONTROLLER_REST_POSITION_30)); }
	inline Vector3_t3722313464  get_DEFAULT_CONTROLLER_REST_POSITION_30() const { return ___DEFAULT_CONTROLLER_REST_POSITION_30; }
	inline Vector3_t3722313464 * get_address_of_DEFAULT_CONTROLLER_REST_POSITION_30() { return &___DEFAULT_CONTROLLER_REST_POSITION_30; }
	inline void set_DEFAULT_CONTROLLER_REST_POSITION_30(Vector3_t3722313464  value)
	{
		___DEFAULT_CONTROLLER_REST_POSITION_30 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_ARM_EXTENSION_OFFSET_31() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920_StaticFields, ___DEFAULT_ARM_EXTENSION_OFFSET_31)); }
	inline Vector3_t3722313464  get_DEFAULT_ARM_EXTENSION_OFFSET_31() const { return ___DEFAULT_ARM_EXTENSION_OFFSET_31; }
	inline Vector3_t3722313464 * get_address_of_DEFAULT_ARM_EXTENSION_OFFSET_31() { return &___DEFAULT_ARM_EXTENSION_OFFSET_31; }
	inline void set_DEFAULT_ARM_EXTENSION_OFFSET_31(Vector3_t3722313464  value)
	{
		___DEFAULT_ARM_EXTENSION_OFFSET_31 = value;
	}

	inline static int32_t get_offset_of_SHOULDER_POSITION_34() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920_StaticFields, ___SHOULDER_POSITION_34)); }
	inline Vector3_t3722313464  get_SHOULDER_POSITION_34() const { return ___SHOULDER_POSITION_34; }
	inline Vector3_t3722313464 * get_address_of_SHOULDER_POSITION_34() { return &___SHOULDER_POSITION_34; }
	inline void set_SHOULDER_POSITION_34(Vector3_t3722313464  value)
	{
		___SHOULDER_POSITION_34 = value;
	}

	inline static int32_t get_offset_of_NECK_OFFSET_35() { return static_cast<int32_t>(offsetof(GvrArmModel_t2461504920_StaticFields, ___NECK_OFFSET_35)); }
	inline Vector3_t3722313464  get_NECK_OFFSET_35() const { return ___NECK_OFFSET_35; }
	inline Vector3_t3722313464 * get_address_of_NECK_OFFSET_35() { return &___NECK_OFFSET_35; }
	inline void set_NECK_OFFSET_35(Vector3_t3722313464  value)
	{
		___NECK_OFFSET_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRARMMODEL_T2461504920_H
#ifndef GVRCONTROLLER_T660780660_H
#define GVRCONTROLLER_T660780660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrController
struct  GvrController_t660780660  : public GvrControllerInput_t3301899046
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLER_T660780660_H
#ifndef GVRCONTROLLERVISUALMANAGER_T2721511270_H
#define GVRCONTROLLERVISUALMANAGER_T2721511270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrControllerVisualManager
struct  GvrControllerVisualManager_t2721511270  : public GvrTrackedController_t2964803478
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCONTROLLERVISUALMANAGER_T2721511270_H
#ifndef GVRLASERPOINTER_T1349299755_H
#define GVRLASERPOINTER_T1349299755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrLaserPointer
struct  GvrLaserPointer_t1349299755  : public GvrBasePointer_t822782720
{
public:
	// System.Single GvrLaserPointer::maxPointerDistance
	float ___maxPointerDistance_8;
	// System.Single GvrLaserPointer::defaultReticleDistance
	float ___defaultReticleDistance_9;
	// System.Single GvrLaserPointer::overrideCameraRayIntersectionDistance
	float ___overrideCameraRayIntersectionDistance_10;
	// GvrLaserVisual GvrLaserPointer::<LaserVisual>k__BackingField
	GvrLaserVisual_t3710127448 * ___U3CLaserVisualU3Ek__BackingField_12;
	// System.Boolean GvrLaserPointer::isHittingTarget
	bool ___isHittingTarget_13;

public:
	inline static int32_t get_offset_of_maxPointerDistance_8() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t1349299755, ___maxPointerDistance_8)); }
	inline float get_maxPointerDistance_8() const { return ___maxPointerDistance_8; }
	inline float* get_address_of_maxPointerDistance_8() { return &___maxPointerDistance_8; }
	inline void set_maxPointerDistance_8(float value)
	{
		___maxPointerDistance_8 = value;
	}

	inline static int32_t get_offset_of_defaultReticleDistance_9() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t1349299755, ___defaultReticleDistance_9)); }
	inline float get_defaultReticleDistance_9() const { return ___defaultReticleDistance_9; }
	inline float* get_address_of_defaultReticleDistance_9() { return &___defaultReticleDistance_9; }
	inline void set_defaultReticleDistance_9(float value)
	{
		___defaultReticleDistance_9 = value;
	}

	inline static int32_t get_offset_of_overrideCameraRayIntersectionDistance_10() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t1349299755, ___overrideCameraRayIntersectionDistance_10)); }
	inline float get_overrideCameraRayIntersectionDistance_10() const { return ___overrideCameraRayIntersectionDistance_10; }
	inline float* get_address_of_overrideCameraRayIntersectionDistance_10() { return &___overrideCameraRayIntersectionDistance_10; }
	inline void set_overrideCameraRayIntersectionDistance_10(float value)
	{
		___overrideCameraRayIntersectionDistance_10 = value;
	}

	inline static int32_t get_offset_of_U3CLaserVisualU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t1349299755, ___U3CLaserVisualU3Ek__BackingField_12)); }
	inline GvrLaserVisual_t3710127448 * get_U3CLaserVisualU3Ek__BackingField_12() const { return ___U3CLaserVisualU3Ek__BackingField_12; }
	inline GvrLaserVisual_t3710127448 ** get_address_of_U3CLaserVisualU3Ek__BackingField_12() { return &___U3CLaserVisualU3Ek__BackingField_12; }
	inline void set_U3CLaserVisualU3Ek__BackingField_12(GvrLaserVisual_t3710127448 * value)
	{
		___U3CLaserVisualU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLaserVisualU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_isHittingTarget_13() { return static_cast<int32_t>(offsetof(GvrLaserPointer_t1349299755, ___isHittingTarget_13)); }
	inline bool get_isHittingTarget_13() const { return ___isHittingTarget_13; }
	inline bool* get_address_of_isHittingTarget_13() { return &___isHittingTarget_13; }
	inline void set_isHittingTarget_13(bool value)
	{
		___isHittingTarget_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRLASERPOINTER_T1349299755_H
#ifndef GVRRETICLEPOINTER_T1894921418_H
#define GVRRETICLEPOINTER_T1894921418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrReticlePointer
struct  GvrReticlePointer_t1894921418  : public GvrBasePointer_t822782720
{
public:
	// System.Single GvrReticlePointer::maxReticleDistance
	float ___maxReticleDistance_12;
	// System.Int32 GvrReticlePointer::reticleSegments
	int32_t ___reticleSegments_13;
	// System.Single GvrReticlePointer::reticleGrowthSpeed
	float ___reticleGrowthSpeed_14;
	// System.Int32 GvrReticlePointer::reticleSortingOrder
	int32_t ___reticleSortingOrder_15;
	// UnityEngine.Material GvrReticlePointer::<MaterialComp>k__BackingField
	Material_t340375123 * ___U3CMaterialCompU3Ek__BackingField_16;
	// System.Single GvrReticlePointer::<ReticleInnerAngle>k__BackingField
	float ___U3CReticleInnerAngleU3Ek__BackingField_17;
	// System.Single GvrReticlePointer::<ReticleOuterAngle>k__BackingField
	float ___U3CReticleOuterAngleU3Ek__BackingField_18;
	// System.Single GvrReticlePointer::<ReticleDistanceInMeters>k__BackingField
	float ___U3CReticleDistanceInMetersU3Ek__BackingField_19;
	// System.Single GvrReticlePointer::<ReticleInnerDiameter>k__BackingField
	float ___U3CReticleInnerDiameterU3Ek__BackingField_20;
	// System.Single GvrReticlePointer::<ReticleOuterDiameter>k__BackingField
	float ___U3CReticleOuterDiameterU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_maxReticleDistance_12() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___maxReticleDistance_12)); }
	inline float get_maxReticleDistance_12() const { return ___maxReticleDistance_12; }
	inline float* get_address_of_maxReticleDistance_12() { return &___maxReticleDistance_12; }
	inline void set_maxReticleDistance_12(float value)
	{
		___maxReticleDistance_12 = value;
	}

	inline static int32_t get_offset_of_reticleSegments_13() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___reticleSegments_13)); }
	inline int32_t get_reticleSegments_13() const { return ___reticleSegments_13; }
	inline int32_t* get_address_of_reticleSegments_13() { return &___reticleSegments_13; }
	inline void set_reticleSegments_13(int32_t value)
	{
		___reticleSegments_13 = value;
	}

	inline static int32_t get_offset_of_reticleGrowthSpeed_14() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___reticleGrowthSpeed_14)); }
	inline float get_reticleGrowthSpeed_14() const { return ___reticleGrowthSpeed_14; }
	inline float* get_address_of_reticleGrowthSpeed_14() { return &___reticleGrowthSpeed_14; }
	inline void set_reticleGrowthSpeed_14(float value)
	{
		___reticleGrowthSpeed_14 = value;
	}

	inline static int32_t get_offset_of_reticleSortingOrder_15() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___reticleSortingOrder_15)); }
	inline int32_t get_reticleSortingOrder_15() const { return ___reticleSortingOrder_15; }
	inline int32_t* get_address_of_reticleSortingOrder_15() { return &___reticleSortingOrder_15; }
	inline void set_reticleSortingOrder_15(int32_t value)
	{
		___reticleSortingOrder_15 = value;
	}

	inline static int32_t get_offset_of_U3CMaterialCompU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___U3CMaterialCompU3Ek__BackingField_16)); }
	inline Material_t340375123 * get_U3CMaterialCompU3Ek__BackingField_16() const { return ___U3CMaterialCompU3Ek__BackingField_16; }
	inline Material_t340375123 ** get_address_of_U3CMaterialCompU3Ek__BackingField_16() { return &___U3CMaterialCompU3Ek__BackingField_16; }
	inline void set_U3CMaterialCompU3Ek__BackingField_16(Material_t340375123 * value)
	{
		___U3CMaterialCompU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMaterialCompU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CReticleInnerAngleU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___U3CReticleInnerAngleU3Ek__BackingField_17)); }
	inline float get_U3CReticleInnerAngleU3Ek__BackingField_17() const { return ___U3CReticleInnerAngleU3Ek__BackingField_17; }
	inline float* get_address_of_U3CReticleInnerAngleU3Ek__BackingField_17() { return &___U3CReticleInnerAngleU3Ek__BackingField_17; }
	inline void set_U3CReticleInnerAngleU3Ek__BackingField_17(float value)
	{
		___U3CReticleInnerAngleU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CReticleOuterAngleU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___U3CReticleOuterAngleU3Ek__BackingField_18)); }
	inline float get_U3CReticleOuterAngleU3Ek__BackingField_18() const { return ___U3CReticleOuterAngleU3Ek__BackingField_18; }
	inline float* get_address_of_U3CReticleOuterAngleU3Ek__BackingField_18() { return &___U3CReticleOuterAngleU3Ek__BackingField_18; }
	inline void set_U3CReticleOuterAngleU3Ek__BackingField_18(float value)
	{
		___U3CReticleOuterAngleU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CReticleDistanceInMetersU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___U3CReticleDistanceInMetersU3Ek__BackingField_19)); }
	inline float get_U3CReticleDistanceInMetersU3Ek__BackingField_19() const { return ___U3CReticleDistanceInMetersU3Ek__BackingField_19; }
	inline float* get_address_of_U3CReticleDistanceInMetersU3Ek__BackingField_19() { return &___U3CReticleDistanceInMetersU3Ek__BackingField_19; }
	inline void set_U3CReticleDistanceInMetersU3Ek__BackingField_19(float value)
	{
		___U3CReticleDistanceInMetersU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CReticleInnerDiameterU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___U3CReticleInnerDiameterU3Ek__BackingField_20)); }
	inline float get_U3CReticleInnerDiameterU3Ek__BackingField_20() const { return ___U3CReticleInnerDiameterU3Ek__BackingField_20; }
	inline float* get_address_of_U3CReticleInnerDiameterU3Ek__BackingField_20() { return &___U3CReticleInnerDiameterU3Ek__BackingField_20; }
	inline void set_U3CReticleInnerDiameterU3Ek__BackingField_20(float value)
	{
		___U3CReticleInnerDiameterU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CReticleOuterDiameterU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(GvrReticlePointer_t1894921418, ___U3CReticleOuterDiameterU3Ek__BackingField_21)); }
	inline float get_U3CReticleOuterDiameterU3Ek__BackingField_21() const { return ___U3CReticleOuterDiameterU3Ek__BackingField_21; }
	inline float* get_address_of_U3CReticleOuterDiameterU3Ek__BackingField_21() { return &___U3CReticleOuterDiameterU3Ek__BackingField_21; }
	inline void set_U3CReticleOuterDiameterU3Ek__BackingField_21(float value)
	{
		___U3CReticleOuterDiameterU3Ek__BackingField_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRETICLEPOINTER_T1894921418_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_4;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_5;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_6;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_7;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_8;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_9;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_4)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_4() const { return ___m_RaycastResultCache_4; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_4() { return &___m_RaycastResultCache_4; }
	inline void set_m_RaycastResultCache_4(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_4), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_5)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_5() const { return ___m_AxisEventData_5; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_5() { return &___m_AxisEventData_5; }
	inline void set_m_AxisEventData_5(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_5), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_6)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_6() const { return ___m_EventSystem_6; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_6() { return &___m_EventSystem_6; }
	inline void set_m_EventSystem_6(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_6), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_7)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_7() const { return ___m_BaseEventData_7; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_7() { return &___m_BaseEventData_7; }
	inline void set_m_BaseEventData_7(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_7), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_8() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_8)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_8() const { return ___m_InputOverride_8; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_8() { return &___m_InputOverride_8; }
	inline void set_m_InputOverride_8(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_8), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_9() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_9)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_9() const { return ___m_DefaultInput_9; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_9() { return &___m_DefaultInput_9; }
	inline void set_m_DefaultInput_9(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef BASERAYCASTER_T4150874583_H
#define BASERAYCASTER_T4150874583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseRaycaster
struct  BaseRaycaster_t4150874583  : public UIBehaviour_t3495933518
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASERAYCASTER_T4150874583_H
#ifndef GVRBASEPOINTERRAYCASTER_T3158030611_H
#define GVRBASEPOINTERRAYCASTER_T3158030611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrBasePointerRaycaster
struct  GvrBasePointerRaycaster_t3158030611  : public BaseRaycaster_t4150874583
{
public:
	// GvrBasePointer/PointerRay GvrBasePointerRaycaster::lastRay
	PointerRay_t3451016640  ___lastRay_4;
	// GvrBasePointer/RaycastMode GvrBasePointerRaycaster::<CurrentRaycastModeForHybrid>k__BackingField
	int32_t ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_lastRay_4() { return static_cast<int32_t>(offsetof(GvrBasePointerRaycaster_t3158030611, ___lastRay_4)); }
	inline PointerRay_t3451016640  get_lastRay_4() const { return ___lastRay_4; }
	inline PointerRay_t3451016640 * get_address_of_lastRay_4() { return &___lastRay_4; }
	inline void set_lastRay_4(PointerRay_t3451016640  value)
	{
		___lastRay_4 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GvrBasePointerRaycaster_t3158030611, ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5)); }
	inline int32_t get_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5() const { return ___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5() { return &___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5; }
	inline void set_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5(int32_t value)
	{
		___U3CCurrentRaycastModeForHybridU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRBASEPOINTERRAYCASTER_T3158030611_H
#ifndef GVRPOINTERINPUTMODULE_T4124566278_H
#define GVRPOINTERINPUTMODULE_T4124566278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerInputModule
struct  GvrPointerInputModule_t4124566278  : public BaseInputModule_t2019268878
{
public:
	// System.Boolean GvrPointerInputModule::vrModeOnly
	bool ___vrModeOnly_10;
	// GvrPointerScrollInput GvrPointerInputModule::scrollInput
	GvrPointerScrollInput_t3738414627 * ___scrollInput_11;
	// GvrPointerInputModuleImpl GvrPointerInputModule::<Impl>k__BackingField
	GvrPointerInputModuleImpl_t2260544298 * ___U3CImplU3Ek__BackingField_12;
	// GvrEventExecutor GvrPointerInputModule::<EventExecutor>k__BackingField
	GvrEventExecutor_t2551165091 * ___U3CEventExecutorU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_vrModeOnly_10() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t4124566278, ___vrModeOnly_10)); }
	inline bool get_vrModeOnly_10() const { return ___vrModeOnly_10; }
	inline bool* get_address_of_vrModeOnly_10() { return &___vrModeOnly_10; }
	inline void set_vrModeOnly_10(bool value)
	{
		___vrModeOnly_10 = value;
	}

	inline static int32_t get_offset_of_scrollInput_11() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t4124566278, ___scrollInput_11)); }
	inline GvrPointerScrollInput_t3738414627 * get_scrollInput_11() const { return ___scrollInput_11; }
	inline GvrPointerScrollInput_t3738414627 ** get_address_of_scrollInput_11() { return &___scrollInput_11; }
	inline void set_scrollInput_11(GvrPointerScrollInput_t3738414627 * value)
	{
		___scrollInput_11 = value;
		Il2CppCodeGenWriteBarrier((&___scrollInput_11), value);
	}

	inline static int32_t get_offset_of_U3CImplU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t4124566278, ___U3CImplU3Ek__BackingField_12)); }
	inline GvrPointerInputModuleImpl_t2260544298 * get_U3CImplU3Ek__BackingField_12() const { return ___U3CImplU3Ek__BackingField_12; }
	inline GvrPointerInputModuleImpl_t2260544298 ** get_address_of_U3CImplU3Ek__BackingField_12() { return &___U3CImplU3Ek__BackingField_12; }
	inline void set_U3CImplU3Ek__BackingField_12(GvrPointerInputModuleImpl_t2260544298 * value)
	{
		___U3CImplU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImplU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CEventExecutorU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(GvrPointerInputModule_t4124566278, ___U3CEventExecutorU3Ek__BackingField_13)); }
	inline GvrEventExecutor_t2551165091 * get_U3CEventExecutorU3Ek__BackingField_13() const { return ___U3CEventExecutorU3Ek__BackingField_13; }
	inline GvrEventExecutor_t2551165091 ** get_address_of_U3CEventExecutorU3Ek__BackingField_13() { return &___U3CEventExecutorU3Ek__BackingField_13; }
	inline void set_U3CEventExecutorU3Ek__BackingField_13(GvrEventExecutor_t2551165091 * value)
	{
		___U3CEventExecutorU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEventExecutorU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERINPUTMODULE_T4124566278_H
#ifndef GVRPOINTERGRAPHICRAYCASTER_T348070255_H
#define GVRPOINTERGRAPHICRAYCASTER_T348070255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerGraphicRaycaster
struct  GvrPointerGraphicRaycaster_t348070255  : public GvrBasePointerRaycaster_t3158030611
{
public:
	// System.Boolean GvrPointerGraphicRaycaster::ignoreReversedGraphics
	bool ___ignoreReversedGraphics_7;
	// GvrPointerGraphicRaycaster/BlockingObjects GvrPointerGraphicRaycaster::blockingObjects
	int32_t ___blockingObjects_8;
	// UnityEngine.LayerMask GvrPointerGraphicRaycaster::blockingMask
	LayerMask_t3493934918  ___blockingMask_9;
	// UnityEngine.Canvas GvrPointerGraphicRaycaster::targetCanvas
	Canvas_t3310196443 * ___targetCanvas_10;
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster::raycastResults
	List_1_t3132410353 * ___raycastResults_11;
	// UnityEngine.Camera GvrPointerGraphicRaycaster::cachedPointerEventCamera
	Camera_t4157153871 * ___cachedPointerEventCamera_12;

public:
	inline static int32_t get_offset_of_ignoreReversedGraphics_7() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___ignoreReversedGraphics_7)); }
	inline bool get_ignoreReversedGraphics_7() const { return ___ignoreReversedGraphics_7; }
	inline bool* get_address_of_ignoreReversedGraphics_7() { return &___ignoreReversedGraphics_7; }
	inline void set_ignoreReversedGraphics_7(bool value)
	{
		___ignoreReversedGraphics_7 = value;
	}

	inline static int32_t get_offset_of_blockingObjects_8() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___blockingObjects_8)); }
	inline int32_t get_blockingObjects_8() const { return ___blockingObjects_8; }
	inline int32_t* get_address_of_blockingObjects_8() { return &___blockingObjects_8; }
	inline void set_blockingObjects_8(int32_t value)
	{
		___blockingObjects_8 = value;
	}

	inline static int32_t get_offset_of_blockingMask_9() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___blockingMask_9)); }
	inline LayerMask_t3493934918  get_blockingMask_9() const { return ___blockingMask_9; }
	inline LayerMask_t3493934918 * get_address_of_blockingMask_9() { return &___blockingMask_9; }
	inline void set_blockingMask_9(LayerMask_t3493934918  value)
	{
		___blockingMask_9 = value;
	}

	inline static int32_t get_offset_of_targetCanvas_10() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___targetCanvas_10)); }
	inline Canvas_t3310196443 * get_targetCanvas_10() const { return ___targetCanvas_10; }
	inline Canvas_t3310196443 ** get_address_of_targetCanvas_10() { return &___targetCanvas_10; }
	inline void set_targetCanvas_10(Canvas_t3310196443 * value)
	{
		___targetCanvas_10 = value;
		Il2CppCodeGenWriteBarrier((&___targetCanvas_10), value);
	}

	inline static int32_t get_offset_of_raycastResults_11() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___raycastResults_11)); }
	inline List_1_t3132410353 * get_raycastResults_11() const { return ___raycastResults_11; }
	inline List_1_t3132410353 ** get_address_of_raycastResults_11() { return &___raycastResults_11; }
	inline void set_raycastResults_11(List_1_t3132410353 * value)
	{
		___raycastResults_11 = value;
		Il2CppCodeGenWriteBarrier((&___raycastResults_11), value);
	}

	inline static int32_t get_offset_of_cachedPointerEventCamera_12() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255, ___cachedPointerEventCamera_12)); }
	inline Camera_t4157153871 * get_cachedPointerEventCamera_12() const { return ___cachedPointerEventCamera_12; }
	inline Camera_t4157153871 ** get_address_of_cachedPointerEventCamera_12() { return &___cachedPointerEventCamera_12; }
	inline void set_cachedPointerEventCamera_12(Camera_t4157153871 * value)
	{
		___cachedPointerEventCamera_12 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPointerEventCamera_12), value);
	}
};

struct GvrPointerGraphicRaycaster_t348070255_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster::sortedGraphics
	List_1_t3132410353 * ___sortedGraphics_13;
	// System.Comparison`1<UnityEngine.UI.Graphic> GvrPointerGraphicRaycaster::<>f__am$cache0
	Comparison_1_t1435266790 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_sortedGraphics_13() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255_StaticFields, ___sortedGraphics_13)); }
	inline List_1_t3132410353 * get_sortedGraphics_13() const { return ___sortedGraphics_13; }
	inline List_1_t3132410353 ** get_address_of_sortedGraphics_13() { return &___sortedGraphics_13; }
	inline void set_sortedGraphics_13(List_1_t3132410353 * value)
	{
		___sortedGraphics_13 = value;
		Il2CppCodeGenWriteBarrier((&___sortedGraphics_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(GvrPointerGraphicRaycaster_t348070255_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Comparison_1_t1435266790 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Comparison_1_t1435266790 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Comparison_1_t1435266790 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERGRAPHICRAYCASTER_T348070255_H
#ifndef GVRPOINTERPHYSICSRAYCASTER_T304947617_H
#define GVRPOINTERPHYSICSRAYCASTER_T304947617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrPointerPhysicsRaycaster
struct  GvrPointerPhysicsRaycaster_t304947617  : public GvrBasePointerRaycaster_t3158030611
{
public:
	// UnityEngine.LayerMask GvrPointerPhysicsRaycaster::raycasterEventMask
	LayerMask_t3493934918  ___raycasterEventMask_8;
	// System.Int32 GvrPointerPhysicsRaycaster::maxRaycastHits
	int32_t ___maxRaycastHits_9;
	// UnityEngine.RaycastHit[] GvrPointerPhysicsRaycaster::hits
	RaycastHitU5BU5D_t1690781147* ___hits_10;
	// GvrPointerPhysicsRaycaster/HitComparer GvrPointerPhysicsRaycaster::hitComparer
	HitComparer_t1984117280 * ___hitComparer_11;

public:
	inline static int32_t get_offset_of_raycasterEventMask_8() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_t304947617, ___raycasterEventMask_8)); }
	inline LayerMask_t3493934918  get_raycasterEventMask_8() const { return ___raycasterEventMask_8; }
	inline LayerMask_t3493934918 * get_address_of_raycasterEventMask_8() { return &___raycasterEventMask_8; }
	inline void set_raycasterEventMask_8(LayerMask_t3493934918  value)
	{
		___raycasterEventMask_8 = value;
	}

	inline static int32_t get_offset_of_maxRaycastHits_9() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_t304947617, ___maxRaycastHits_9)); }
	inline int32_t get_maxRaycastHits_9() const { return ___maxRaycastHits_9; }
	inline int32_t* get_address_of_maxRaycastHits_9() { return &___maxRaycastHits_9; }
	inline void set_maxRaycastHits_9(int32_t value)
	{
		___maxRaycastHits_9 = value;
	}

	inline static int32_t get_offset_of_hits_10() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_t304947617, ___hits_10)); }
	inline RaycastHitU5BU5D_t1690781147* get_hits_10() const { return ___hits_10; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_hits_10() { return &___hits_10; }
	inline void set_hits_10(RaycastHitU5BU5D_t1690781147* value)
	{
		___hits_10 = value;
		Il2CppCodeGenWriteBarrier((&___hits_10), value);
	}

	inline static int32_t get_offset_of_hitComparer_11() { return static_cast<int32_t>(offsetof(GvrPointerPhysicsRaycaster_t304947617, ___hitComparer_11)); }
	inline HitComparer_t1984117280 * get_hitComparer_11() const { return ___hitComparer_11; }
	inline HitComparer_t1984117280 ** get_address_of_hitComparer_11() { return &___hitComparer_11; }
	inline void set_hitComparer_11(HitComparer_t1984117280 * value)
	{
		___hitComparer_11 = value;
		Il2CppCodeGenWriteBarrier((&___hitComparer_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRPOINTERPHYSICSRAYCASTER_T304947617_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (GvrAudioRoom_t1033997160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[11] = 
{
	GvrAudioRoom_t1033997160::get_offset_of_leftWall_4(),
	GvrAudioRoom_t1033997160::get_offset_of_rightWall_5(),
	GvrAudioRoom_t1033997160::get_offset_of_floor_6(),
	GvrAudioRoom_t1033997160::get_offset_of_ceiling_7(),
	GvrAudioRoom_t1033997160::get_offset_of_backWall_8(),
	GvrAudioRoom_t1033997160::get_offset_of_frontWall_9(),
	GvrAudioRoom_t1033997160::get_offset_of_reflectivity_10(),
	GvrAudioRoom_t1033997160::get_offset_of_reverbGainDb_11(),
	GvrAudioRoom_t1033997160::get_offset_of_reverbBrightness_12(),
	GvrAudioRoom_t1033997160::get_offset_of_reverbTime_13(),
	GvrAudioRoom_t1033997160::get_offset_of_size_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (SurfaceMaterial_t4254821637)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2401[24] = 
{
	SurfaceMaterial_t4254821637::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (GvrAudioSoundfield_t3422980884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[18] = 
{
	GvrAudioSoundfield_t3422980884::get_offset_of_bypassRoomEffects_4(),
	GvrAudioSoundfield_t3422980884::get_offset_of_gainDb_5(),
	GvrAudioSoundfield_t3422980884::get_offset_of_playOnAwake_6(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldClip0102_7(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldClip0304_8(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldLoop_9(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldMute_10(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldPitch_11(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldPriority_12(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldSpatialBlend_13(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldDopplerLevel_14(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldVolume_15(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldRolloffMode_16(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldMaxDistance_17(),
	GvrAudioSoundfield_t3422980884::get_offset_of_soundfieldMinDistance_18(),
	GvrAudioSoundfield_t3422980884::get_offset_of_id_19(),
	GvrAudioSoundfield_t3422980884::get_offset_of_audioSources_20(),
	GvrAudioSoundfield_t3422980884::get_offset_of_isPaused_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (GvrAudioSource_t775302101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2403[26] = 
{
	GvrAudioSource_t775302101::get_offset_of_bypassRoomEffects_4(),
	GvrAudioSource_t775302101::get_offset_of_directivityAlpha_5(),
	GvrAudioSource_t775302101::get_offset_of_directivitySharpness_6(),
	GvrAudioSource_t775302101::get_offset_of_listenerDirectivityAlpha_7(),
	GvrAudioSource_t775302101::get_offset_of_listenerDirectivitySharpness_8(),
	GvrAudioSource_t775302101::get_offset_of_gainDb_9(),
	GvrAudioSource_t775302101::get_offset_of_occlusionEnabled_10(),
	GvrAudioSource_t775302101::get_offset_of_playOnAwake_11(),
	GvrAudioSource_t775302101::get_offset_of_sourceClip_12(),
	GvrAudioSource_t775302101::get_offset_of_sourceLoop_13(),
	GvrAudioSource_t775302101::get_offset_of_sourceMute_14(),
	GvrAudioSource_t775302101::get_offset_of_sourcePitch_15(),
	GvrAudioSource_t775302101::get_offset_of_sourcePriority_16(),
	GvrAudioSource_t775302101::get_offset_of_sourceSpatialBlend_17(),
	GvrAudioSource_t775302101::get_offset_of_sourceDopplerLevel_18(),
	GvrAudioSource_t775302101::get_offset_of_sourceSpread_19(),
	GvrAudioSource_t775302101::get_offset_of_sourceVolume_20(),
	GvrAudioSource_t775302101::get_offset_of_sourceRolloffMode_21(),
	GvrAudioSource_t775302101::get_offset_of_sourceMaxDistance_22(),
	GvrAudioSource_t775302101::get_offset_of_sourceMinDistance_23(),
	GvrAudioSource_t775302101::get_offset_of_hrtfEnabled_24(),
	GvrAudioSource_t775302101::get_offset_of_audioSource_25(),
	GvrAudioSource_t775302101::get_offset_of_id_26(),
	GvrAudioSource_t775302101::get_offset_of_currentOcclusion_27(),
	GvrAudioSource_t775302101::get_offset_of_nextOcclusionUpdate_28(),
	GvrAudioSource_t775302101::get_offset_of_isPaused_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (GvrController_t660780660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (GvrControllerVisualManager_t2721511270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (GvrPointerManager_t310390967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (GvrReticlePointer_t1894921418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[14] = 
{
	0,
	0,
	0,
	0,
	GvrReticlePointer_t1894921418::get_offset_of_maxReticleDistance_12(),
	GvrReticlePointer_t1894921418::get_offset_of_reticleSegments_13(),
	GvrReticlePointer_t1894921418::get_offset_of_reticleGrowthSpeed_14(),
	GvrReticlePointer_t1894921418::get_offset_of_reticleSortingOrder_15(),
	GvrReticlePointer_t1894921418::get_offset_of_U3CMaterialCompU3Ek__BackingField_16(),
	GvrReticlePointer_t1894921418::get_offset_of_U3CReticleInnerAngleU3Ek__BackingField_17(),
	GvrReticlePointer_t1894921418::get_offset_of_U3CReticleOuterAngleU3Ek__BackingField_18(),
	GvrReticlePointer_t1894921418::get_offset_of_U3CReticleDistanceInMetersU3Ek__BackingField_19(),
	GvrReticlePointer_t1894921418::get_offset_of_U3CReticleInnerDiameterU3Ek__BackingField_20(),
	GvrReticlePointer_t1894921418::get_offset_of_U3CReticleOuterDiameterU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (GvrArmModel_t2461504920), -1, sizeof(GvrArmModel_t2461504920_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2408[35] = 
{
	GvrArmModel_t2461504920::get_offset_of_elbowRestPosition_4(),
	GvrArmModel_t2461504920::get_offset_of_wristRestPosition_5(),
	GvrArmModel_t2461504920::get_offset_of_controllerRestPosition_6(),
	GvrArmModel_t2461504920::get_offset_of_armExtensionOffset_7(),
	GvrArmModel_t2461504920::get_offset_of_elbowBendRatio_8(),
	GvrArmModel_t2461504920::get_offset_of_fadeControllerOffset_9(),
	GvrArmModel_t2461504920::get_offset_of_fadeDistanceFromHeadForward_10(),
	GvrArmModel_t2461504920::get_offset_of_fadeDistanceFromHeadSide_11(),
	GvrArmModel_t2461504920::get_offset_of_tooltipMinDistanceFromFace_12(),
	GvrArmModel_t2461504920::get_offset_of_tooltipMaxAngleFromCamera_13(),
	GvrArmModel_t2461504920::get_offset_of_isLockedToNeck_14(),
	GvrArmModel_t2461504920::get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_15(),
	GvrArmModel_t2461504920::get_offset_of_neckPosition_16(),
	GvrArmModel_t2461504920::get_offset_of_elbowPosition_17(),
	GvrArmModel_t2461504920::get_offset_of_elbowRotation_18(),
	GvrArmModel_t2461504920::get_offset_of_wristPosition_19(),
	GvrArmModel_t2461504920::get_offset_of_wristRotation_20(),
	GvrArmModel_t2461504920::get_offset_of_controllerPosition_21(),
	GvrArmModel_t2461504920::get_offset_of_controllerRotation_22(),
	GvrArmModel_t2461504920::get_offset_of_preferredAlpha_23(),
	GvrArmModel_t2461504920::get_offset_of_tooltipAlphaValue_24(),
	GvrArmModel_t2461504920::get_offset_of_handedMultiplier_25(),
	GvrArmModel_t2461504920::get_offset_of_torsoDirection_26(),
	GvrArmModel_t2461504920::get_offset_of_torsoRotation_27(),
	GvrArmModel_t2461504920_StaticFields::get_offset_of_DEFAULT_ELBOW_REST_POSITION_28(),
	GvrArmModel_t2461504920_StaticFields::get_offset_of_DEFAULT_WRIST_REST_POSITION_29(),
	GvrArmModel_t2461504920_StaticFields::get_offset_of_DEFAULT_CONTROLLER_REST_POSITION_30(),
	GvrArmModel_t2461504920_StaticFields::get_offset_of_DEFAULT_ARM_EXTENSION_OFFSET_31(),
	0,
	0,
	GvrArmModel_t2461504920_StaticFields::get_offset_of_SHOULDER_POSITION_34(),
	GvrArmModel_t2461504920_StaticFields::get_offset_of_NECK_OFFSET_35(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (GvrBaseArmModel_t3382200842), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (GvrConnectionState_t641662995)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2411[6] = 
{
	GvrConnectionState_t641662995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (GvrControllerApiStatus_t2615797932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2412[9] = 
{
	GvrControllerApiStatus_t2615797932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (GvrControllerBatteryLevel_t3390817147)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2413[8] = 
{
	GvrControllerBatteryLevel_t3390817147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (GvrControllerButton_t87476287)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2414[8] = 
{
	GvrControllerButton_t87476287::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (GvrControllerHand_t2517129446)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2415[5] = 
{
	GvrControllerHand_t2517129446::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (GvrControllerInput_t3301899046), -1, sizeof(GvrControllerInput_t3301899046_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2416[7] = 
{
	GvrControllerInput_t3301899046_StaticFields::get_offset_of_instances_4(),
	GvrControllerInput_t3301899046_StaticFields::get_offset_of_controllerProvider_5(),
	GvrControllerInput_t3301899046_StaticFields::get_offset_of_handedness_6(),
	GvrControllerInput_t3301899046_StaticFields::get_offset_of_onDevicesChangedInternal_7(),
	GvrControllerInput_t3301899046_StaticFields::get_offset_of_OnControllerInputUpdated_8(),
	GvrControllerInput_t3301899046_StaticFields::get_offset_of_OnPostControllerInputUpdated_9(),
	GvrControllerInput_t3301899046::get_offset_of_emulatorConnectionMode_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (OnStateChangedEvent_t3148566047), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (EmulatorConnectionMode_t110584383)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2418[4] = 
{
	EmulatorConnectionMode_t110584383::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (GvrControllerInputDevice_t3709172113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[7] = 
{
	GvrControllerInputDevice_t3709172113::get_offset_of_controllerProvider_0(),
	GvrControllerInputDevice_t3709172113::get_offset_of_controllerId_1(),
	GvrControllerInputDevice_t3709172113::get_offset_of_controllerState_2(),
	GvrControllerInputDevice_t3709172113::get_offset_of_touchPosCentered_3(),
	GvrControllerInputDevice_t3709172113::get_offset_of_lastUpdatedFrameCount_4(),
	GvrControllerInputDevice_t3709172113::get_offset_of_valid_5(),
	GvrControllerInputDevice_t3709172113::get_offset_of_OnStateChanged_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (GvrControllerReticleVisual_t1615767572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[10] = 
{
	GvrControllerReticleVisual_t1615767572::get_offset_of_isSizeBasedOnCameraDistance_4(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_sizeMeters_5(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_doesReticleFaceCamera_6(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_sortingOrder_7(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_U3CReticleMeshSizeMetersU3Ek__BackingField_8(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_U3CReticleMeshSizeRatioU3Ek__BackingField_9(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_meshRenderer_10(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_meshFilter_11(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_preRenderLocalScale_12(),
	GvrControllerReticleVisual_t1615767572::get_offset_of_preRenderLocalRotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (FaceCameraData_t2570986833)+ sizeof (RuntimeObject), sizeof(FaceCameraData_t2570986833_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2421[3] = 
{
	FaceCameraData_t2570986833::get_offset_of_alongXAxis_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceCameraData_t2570986833::get_offset_of_alongYAxis_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FaceCameraData_t2570986833::get_offset_of_alongZAxis_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (GvrControllerVisual_t3828962282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2422[41] = 
{
	GvrControllerVisual_t3828962282::get_offset_of_attachmentPrefabs_4(),
	GvrControllerVisual_t3828962282::get_offset_of_touchPadColor_5(),
	GvrControllerVisual_t3828962282::get_offset_of_appButtonColor_6(),
	GvrControllerVisual_t3828962282::get_offset_of_systemButtonColor_7(),
	GvrControllerVisual_t3828962282::get_offset_of_readControllerState_8(),
	GvrControllerVisual_t3828962282::get_offset_of_displayState_9(),
	GvrControllerVisual_t3828962282::get_offset_of_maximumAlpha_10(),
	GvrControllerVisual_t3828962282::get_offset_of_U3CArmModelU3Ek__BackingField_11(),
	GvrControllerVisual_t3828962282::get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_12(),
	GvrControllerVisual_t3828962282::get_offset_of_controllerRenderer_13(),
	GvrControllerVisual_t3828962282::get_offset_of_materialPropertyBlock_14(),
	GvrControllerVisual_t3828962282::get_offset_of_alphaId_15(),
	GvrControllerVisual_t3828962282::get_offset_of_touchId_16(),
	GvrControllerVisual_t3828962282::get_offset_of_touchPadId_17(),
	GvrControllerVisual_t3828962282::get_offset_of_appButtonId_18(),
	GvrControllerVisual_t3828962282::get_offset_of_systemButtonId_19(),
	GvrControllerVisual_t3828962282::get_offset_of_batteryColorId_20(),
	GvrControllerVisual_t3828962282::get_offset_of_wasTouching_21(),
	GvrControllerVisual_t3828962282::get_offset_of_touchTime_22(),
	GvrControllerVisual_t3828962282::get_offset_of_controllerShaderData_23(),
	GvrControllerVisual_t3828962282::get_offset_of_controllerShaderData2_24(),
	GvrControllerVisual_t3828962282::get_offset_of_currentBatteryColor_25(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrControllerVisual_t3828962282::get_offset_of_GVR_BATTERY_CRITICAL_COLOR_40(),
	GvrControllerVisual_t3828962282::get_offset_of_GVR_BATTERY_LOW_COLOR_41(),
	GvrControllerVisual_t3828962282::get_offset_of_GVR_BATTERY_MED_COLOR_42(),
	GvrControllerVisual_t3828962282::get_offset_of_GVR_BATTERY_FULL_COLOR_43(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (ControllerDisplayState_t972313457)+ sizeof (RuntimeObject), sizeof(ControllerDisplayState_t972313457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2423[7] = 
{
	ControllerDisplayState_t972313457::get_offset_of_batteryLevel_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t972313457::get_offset_of_batteryCharging_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t972313457::get_offset_of_clickButton_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t972313457::get_offset_of_appButton_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t972313457::get_offset_of_homeButton_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t972313457::get_offset_of_touching_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControllerDisplayState_t972313457::get_offset_of_touchPos_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (GvrLaserPointer_t1349299755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[6] = 
{
	GvrLaserPointer_t1349299755::get_offset_of_maxPointerDistance_8(),
	GvrLaserPointer_t1349299755::get_offset_of_defaultReticleDistance_9(),
	GvrLaserPointer_t1349299755::get_offset_of_overrideCameraRayIntersectionDistance_10(),
	0,
	GvrLaserPointer_t1349299755::get_offset_of_U3CLaserVisualU3Ek__BackingField_12(),
	GvrLaserPointer_t1349299755::get_offset_of_isHittingTarget_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (GvrLaserVisual_t3710127448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[21] = 
{
	GvrLaserVisual_t3710127448::get_offset_of_reticle_4(),
	GvrLaserVisual_t3710127448::get_offset_of_controller_5(),
	GvrLaserVisual_t3710127448::get_offset_of_laserColor_6(),
	GvrLaserVisual_t3710127448::get_offset_of_laserColorEnd_7(),
	GvrLaserVisual_t3710127448::get_offset_of_maxLaserDistance_8(),
	GvrLaserVisual_t3710127448::get_offset_of_lerpSpeed_9(),
	GvrLaserVisual_t3710127448::get_offset_of_lerpThreshold_10(),
	GvrLaserVisual_t3710127448::get_offset_of_shrinkLaser_11(),
	GvrLaserVisual_t3710127448::get_offset_of_shrunkScale_12(),
	GvrLaserVisual_t3710127448::get_offset_of_beginShrinkAngleDegrees_13(),
	GvrLaserVisual_t3710127448::get_offset_of_endShrinkAngleDegrees_14(),
	0,
	GvrLaserVisual_t3710127448::get_offset_of_U3CArmModelU3Ek__BackingField_16(),
	GvrLaserVisual_t3710127448::get_offset_of_U3CLaserU3Ek__BackingField_17(),
	GvrLaserVisual_t3710127448::get_offset_of_U3CGetPointForDistanceFunctionU3Ek__BackingField_18(),
	GvrLaserVisual_t3710127448::get_offset_of_shrinkRatio_19(),
	GvrLaserVisual_t3710127448::get_offset_of_targetDistance_20(),
	GvrLaserVisual_t3710127448::get_offset_of_currentDistance_21(),
	GvrLaserVisual_t3710127448::get_offset_of_currentPosition_22(),
	GvrLaserVisual_t3710127448::get_offset_of_currentLocalPosition_23(),
	GvrLaserVisual_t3710127448::get_offset_of_currentLocalRotation_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (GetPointForDistanceDelegate_t92913150), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (GvrRecenterOnlyController_t1827745746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[2] = 
{
	GvrRecenterOnlyController_t1827745746::get_offset_of_lastAppliedYawCorrection_4(),
	GvrRecenterOnlyController_t1827745746::get_offset_of_yawCorrection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (GvrTrackedController_t2964803478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[4] = 
{
	GvrTrackedController_t2964803478::get_offset_of_armModel_4(),
	GvrTrackedController_t2964803478::get_offset_of_controllerInputDevice_5(),
	GvrTrackedController_t2964803478::get_offset_of_isDeactivatedWhenDisconnected_6(),
	GvrTrackedController_t2964803478::get_offset_of_controllerHand_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (ControllerProviderFactory_t1243328180), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (DummyControllerProvider_t2905932083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2431[1] = 
{
	DummyControllerProvider_t2905932083::get_offset_of_dummyState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (MouseControllerProvider_t1264821694), -1, sizeof(MouseControllerProvider_t1264821694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[9] = 
{
	0,
	0,
	MouseControllerProvider_t1264821694::get_offset_of_state_2(),
	MouseControllerProvider_t1264821694::get_offset_of_mouseDelta_3(),
	MouseControllerProvider_t1264821694::get_offset_of_wasTouching_4(),
	MouseControllerProvider_t1264821694::get_offset_of_lastButtonsState_5(),
	0,
	0,
	MouseControllerProvider_t1264821694_StaticFields::get_offset_of_INVERT_Y_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (ControllerState_t3401244786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[15] = 
{
	ControllerState_t3401244786::get_offset_of_connectionState_0(),
	ControllerState_t3401244786::get_offset_of_apiStatus_1(),
	ControllerState_t3401244786::get_offset_of_orientation_2(),
	ControllerState_t3401244786::get_offset_of_position_3(),
	ControllerState_t3401244786::get_offset_of_gyro_4(),
	ControllerState_t3401244786::get_offset_of_accel_5(),
	ControllerState_t3401244786::get_offset_of_touchPos_6(),
	ControllerState_t3401244786::get_offset_of_recentered_7(),
	ControllerState_t3401244786::get_offset_of_buttonsState_8(),
	ControllerState_t3401244786::get_offset_of_buttonsDown_9(),
	ControllerState_t3401244786::get_offset_of_buttonsUp_10(),
	ControllerState_t3401244786::get_offset_of_errorDetails_11(),
	ControllerState_t3401244786::get_offset_of_gvrPtr_12(),
	ControllerState_t3401244786::get_offset_of_isCharging_13(),
	ControllerState_t3401244786::get_offset_of_batteryLevel_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (ControllerUtils_t1159150490), -1, sizeof(ControllerUtils_t1159150490_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2434[1] = 
{
	ControllerUtils_t1159150490_StaticFields::get_offset_of_AllHands_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (EmulatorConfig_t3196994574), -1, sizeof(EmulatorConfig_t3196994574_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2435[4] = 
{
	EmulatorConfig_t3196994574_StaticFields::get_offset_of_instance_4(),
	EmulatorConfig_t3196994574::get_offset_of_PHONE_EVENT_MODE_5(),
	EmulatorConfig_t3196994574_StaticFields::get_offset_of_USB_SERVER_IP_6(),
	EmulatorConfig_t3196994574_StaticFields::get_offset_of_WIFI_SERVER_IP_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (Mode_t2693297866)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2436[4] = 
{
	Mode_t2693297866::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (EmulatorGyroEvent_t3120581941)+ sizeof (RuntimeObject), sizeof(EmulatorGyroEvent_t3120581941 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2437[2] = 
{
	EmulatorGyroEvent_t3120581941::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorGyroEvent_t3120581941::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (EmulatorAccelEvent_t3193952569)+ sizeof (RuntimeObject), sizeof(EmulatorAccelEvent_t3193952569 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2438[2] = 
{
	EmulatorAccelEvent_t3193952569::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorAccelEvent_t3193952569::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (EmulatorTouchEvent_t2277405062)+ sizeof (RuntimeObject), -1, sizeof(EmulatorTouchEvent_t2277405062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2439[6] = 
{
	EmulatorTouchEvent_t2277405062::get_offset_of_action_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorTouchEvent_t2277405062::get_offset_of_relativeTimestamp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorTouchEvent_t2277405062::get_offset_of_pointers_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorTouchEvent_t2277405062_StaticFields::get_offset_of_ACTION_POINTER_INDEX_SHIFT_3(),
	EmulatorTouchEvent_t2277405062_StaticFields::get_offset_of_ACTION_POINTER_INDEX_MASK_4(),
	EmulatorTouchEvent_t2277405062_StaticFields::get_offset_of_ACTION_MASK_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (Action_t2046604437)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2440[10] = 
{
	Action_t2046604437::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (Pointer_t3520467680)+ sizeof (RuntimeObject), sizeof(Pointer_t3520467680 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2441[3] = 
{
	Pointer_t3520467680::get_offset_of_fingerId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Pointer_t3520467680::get_offset_of_normalizedX_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Pointer_t3520467680::get_offset_of_normalizedY_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (EmulatorOrientationEvent_t3113283059)+ sizeof (RuntimeObject), sizeof(EmulatorOrientationEvent_t3113283059 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2442[2] = 
{
	EmulatorOrientationEvent_t3113283059::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorOrientationEvent_t3113283059::get_offset_of_orientation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (EmulatorButtonEvent_t938855819)+ sizeof (RuntimeObject), sizeof(EmulatorButtonEvent_t938855819_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2443[2] = 
{
	EmulatorButtonEvent_t938855819::get_offset_of_code_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EmulatorButtonEvent_t938855819::get_offset_of_down_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (ButtonCode_t712346414)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2444[7] = 
{
	ButtonCode_t712346414::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (PhoneEvent_t3448566392), -1, sizeof(PhoneEvent_t3448566392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2445[1] = 
{
	PhoneEvent_t3448566392_StaticFields::get_offset_of_Descriptor_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (PhoneEvent_t1358418708), -1, sizeof(PhoneEvent_t1358418708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2446[25] = 
{
	PhoneEvent_t1358418708_StaticFields::get_offset_of_defaultInstance_0(),
	PhoneEvent_t1358418708_StaticFields::get_offset_of__phoneEventFieldNames_1(),
	PhoneEvent_t1358418708_StaticFields::get_offset_of__phoneEventFieldTags_2(),
	0,
	PhoneEvent_t1358418708::get_offset_of_hasType_4(),
	PhoneEvent_t1358418708::get_offset_of_type__5(),
	0,
	PhoneEvent_t1358418708::get_offset_of_hasMotionEvent_7(),
	PhoneEvent_t1358418708::get_offset_of_motionEvent__8(),
	0,
	PhoneEvent_t1358418708::get_offset_of_hasGyroscopeEvent_10(),
	PhoneEvent_t1358418708::get_offset_of_gyroscopeEvent__11(),
	0,
	PhoneEvent_t1358418708::get_offset_of_hasAccelerometerEvent_13(),
	PhoneEvent_t1358418708::get_offset_of_accelerometerEvent__14(),
	0,
	PhoneEvent_t1358418708::get_offset_of_hasDepthMapEvent_16(),
	PhoneEvent_t1358418708::get_offset_of_depthMapEvent__17(),
	0,
	PhoneEvent_t1358418708::get_offset_of_hasOrientationEvent_19(),
	PhoneEvent_t1358418708::get_offset_of_orientationEvent__20(),
	0,
	PhoneEvent_t1358418708::get_offset_of_hasKeyEvent_22(),
	PhoneEvent_t1358418708::get_offset_of_keyEvent__23(),
	PhoneEvent_t1358418708::get_offset_of_memoizedSerializedSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (Types_t1964849426), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (Type_t1244512943)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2448[7] = 
{
	Type_t1244512943::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (MotionEvent_t3121383016), -1, sizeof(MotionEvent_t3121383016_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2449[12] = 
{
	MotionEvent_t3121383016_StaticFields::get_offset_of_defaultInstance_0(),
	MotionEvent_t3121383016_StaticFields::get_offset_of__motionEventFieldNames_1(),
	MotionEvent_t3121383016_StaticFields::get_offset_of__motionEventFieldTags_2(),
	0,
	MotionEvent_t3121383016::get_offset_of_hasTimestamp_4(),
	MotionEvent_t3121383016::get_offset_of_timestamp__5(),
	0,
	MotionEvent_t3121383016::get_offset_of_hasAction_7(),
	MotionEvent_t3121383016::get_offset_of_action__8(),
	0,
	MotionEvent_t3121383016::get_offset_of_pointers__10(),
	MotionEvent_t3121383016::get_offset_of_memoizedSerializedSize_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (Types_t363369143), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (Pointer_t4145025558), -1, sizeof(Pointer_t4145025558_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2451[13] = 
{
	Pointer_t4145025558_StaticFields::get_offset_of_defaultInstance_0(),
	Pointer_t4145025558_StaticFields::get_offset_of__pointerFieldNames_1(),
	Pointer_t4145025558_StaticFields::get_offset_of__pointerFieldTags_2(),
	0,
	Pointer_t4145025558::get_offset_of_hasId_4(),
	Pointer_t4145025558::get_offset_of_id__5(),
	0,
	Pointer_t4145025558::get_offset_of_hasNormalizedX_7(),
	Pointer_t4145025558::get_offset_of_normalizedX__8(),
	0,
	Pointer_t4145025558::get_offset_of_hasNormalizedY_10(),
	Pointer_t4145025558::get_offset_of_normalizedY__11(),
	Pointer_t4145025558::get_offset_of_memoizedSerializedSize_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (Builder_t582111845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[2] = 
{
	Builder_t582111845::get_offset_of_resultIsReadOnly_0(),
	Builder_t582111845::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (Builder_t2961114394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[2] = 
{
	Builder_t2961114394::get_offset_of_resultIsReadOnly_0(),
	Builder_t2961114394::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (GyroscopeEvent_t127774954), -1, sizeof(GyroscopeEvent_t127774954_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[16] = 
{
	GyroscopeEvent_t127774954_StaticFields::get_offset_of_defaultInstance_0(),
	GyroscopeEvent_t127774954_StaticFields::get_offset_of__gyroscopeEventFieldNames_1(),
	GyroscopeEvent_t127774954_StaticFields::get_offset_of__gyroscopeEventFieldTags_2(),
	0,
	GyroscopeEvent_t127774954::get_offset_of_hasTimestamp_4(),
	GyroscopeEvent_t127774954::get_offset_of_timestamp__5(),
	0,
	GyroscopeEvent_t127774954::get_offset_of_hasX_7(),
	GyroscopeEvent_t127774954::get_offset_of_x__8(),
	0,
	GyroscopeEvent_t127774954::get_offset_of_hasY_10(),
	GyroscopeEvent_t127774954::get_offset_of_y__11(),
	0,
	GyroscopeEvent_t127774954::get_offset_of_hasZ_13(),
	GyroscopeEvent_t127774954::get_offset_of_z__14(),
	GyroscopeEvent_t127774954::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (Builder_t3442751222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[2] = 
{
	Builder_t3442751222::get_offset_of_resultIsReadOnly_0(),
	Builder_t3442751222::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (AccelerometerEvent_t1394922645), -1, sizeof(AccelerometerEvent_t1394922645_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2456[16] = 
{
	AccelerometerEvent_t1394922645_StaticFields::get_offset_of_defaultInstance_0(),
	AccelerometerEvent_t1394922645_StaticFields::get_offset_of__accelerometerEventFieldNames_1(),
	AccelerometerEvent_t1394922645_StaticFields::get_offset_of__accelerometerEventFieldTags_2(),
	0,
	AccelerometerEvent_t1394922645::get_offset_of_hasTimestamp_4(),
	AccelerometerEvent_t1394922645::get_offset_of_timestamp__5(),
	0,
	AccelerometerEvent_t1394922645::get_offset_of_hasX_7(),
	AccelerometerEvent_t1394922645::get_offset_of_x__8(),
	0,
	AccelerometerEvent_t1394922645::get_offset_of_hasY_10(),
	AccelerometerEvent_t1394922645::get_offset_of_y__11(),
	0,
	AccelerometerEvent_t1394922645::get_offset_of_hasZ_13(),
	AccelerometerEvent_t1394922645::get_offset_of_z__14(),
	AccelerometerEvent_t1394922645::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (Builder_t2179940343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[2] = 
{
	Builder_t2179940343::get_offset_of_resultIsReadOnly_0(),
	Builder_t2179940343::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (DepthMapEvent_t729886054), -1, sizeof(DepthMapEvent_t729886054_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2458[16] = 
{
	DepthMapEvent_t729886054_StaticFields::get_offset_of_defaultInstance_0(),
	DepthMapEvent_t729886054_StaticFields::get_offset_of__depthMapEventFieldNames_1(),
	DepthMapEvent_t729886054_StaticFields::get_offset_of__depthMapEventFieldTags_2(),
	0,
	DepthMapEvent_t729886054::get_offset_of_hasTimestamp_4(),
	DepthMapEvent_t729886054::get_offset_of_timestamp__5(),
	0,
	DepthMapEvent_t729886054::get_offset_of_hasWidth_7(),
	DepthMapEvent_t729886054::get_offset_of_width__8(),
	0,
	DepthMapEvent_t729886054::get_offset_of_hasHeight_10(),
	DepthMapEvent_t729886054::get_offset_of_height__11(),
	0,
	DepthMapEvent_t729886054::get_offset_of_zDistancesMemoizedSerializedSize_13(),
	DepthMapEvent_t729886054::get_offset_of_zDistances__14(),
	DepthMapEvent_t729886054::get_offset_of_memoizedSerializedSize_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (Builder_t1293396100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[2] = 
{
	Builder_t1293396100::get_offset_of_resultIsReadOnly_0(),
	Builder_t1293396100::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (OrientationEvent_t158247624), -1, sizeof(OrientationEvent_t158247624_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2460[19] = 
{
	OrientationEvent_t158247624_StaticFields::get_offset_of_defaultInstance_0(),
	OrientationEvent_t158247624_StaticFields::get_offset_of__orientationEventFieldNames_1(),
	OrientationEvent_t158247624_StaticFields::get_offset_of__orientationEventFieldTags_2(),
	0,
	OrientationEvent_t158247624::get_offset_of_hasTimestamp_4(),
	OrientationEvent_t158247624::get_offset_of_timestamp__5(),
	0,
	OrientationEvent_t158247624::get_offset_of_hasX_7(),
	OrientationEvent_t158247624::get_offset_of_x__8(),
	0,
	OrientationEvent_t158247624::get_offset_of_hasY_10(),
	OrientationEvent_t158247624::get_offset_of_y__11(),
	0,
	OrientationEvent_t158247624::get_offset_of_hasZ_13(),
	OrientationEvent_t158247624::get_offset_of_z__14(),
	0,
	OrientationEvent_t158247624::get_offset_of_hasW_16(),
	OrientationEvent_t158247624::get_offset_of_w__17(),
	OrientationEvent_t158247624::get_offset_of_memoizedSerializedSize_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (Builder_t2279437287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[2] = 
{
	Builder_t2279437287::get_offset_of_resultIsReadOnly_0(),
	Builder_t2279437287::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (KeyEvent_t1937114521), -1, sizeof(KeyEvent_t1937114521_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2462[10] = 
{
	KeyEvent_t1937114521_StaticFields::get_offset_of_defaultInstance_0(),
	KeyEvent_t1937114521_StaticFields::get_offset_of__keyEventFieldNames_1(),
	KeyEvent_t1937114521_StaticFields::get_offset_of__keyEventFieldTags_2(),
	0,
	KeyEvent_t1937114521::get_offset_of_hasAction_4(),
	KeyEvent_t1937114521::get_offset_of_action__5(),
	0,
	KeyEvent_t1937114521::get_offset_of_hasCode_7(),
	KeyEvent_t1937114521::get_offset_of_code__8(),
	KeyEvent_t1937114521::get_offset_of_memoizedSerializedSize_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (Builder_t2712992173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[2] = 
{
	Builder_t2712992173::get_offset_of_resultIsReadOnly_0(),
	Builder_t2712992173::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (Builder_t895705486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[2] = 
{
	Builder_t895705486::get_offset_of_resultIsReadOnly_0(),
	Builder_t895705486::get_offset_of_result_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (GvrControllerTooltipsSimple_t2073124053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2466[4] = 
{
	GvrControllerTooltipsSimple_t2073124053::get_offset_of_tooltipRenderer_4(),
	GvrControllerTooltipsSimple_t2073124053::get_offset_of_U3CArmModelU3Ek__BackingField_5(),
	GvrControllerTooltipsSimple_t2073124053::get_offset_of_materialPropertyBlock_6(),
	GvrControllerTooltipsSimple_t2073124053::get_offset_of_colorId_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (GvrTooltip_t491261851), -1, sizeof(GvrTooltip_t491261851_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2467[14] = 
{
	GvrTooltip_t491261851_StaticFields::get_offset_of_RIGHT_SIDE_ROTATION_4(),
	GvrTooltip_t491261851_StaticFields::get_offset_of_LEFT_SIDE_ROTATION_5(),
	GvrTooltip_t491261851_StaticFields::get_offset_of_SQUARE_CENTER_6(),
	GvrTooltip_t491261851_StaticFields::get_offset_of_PIVOT_7(),
	0,
	0,
	0,
	GvrTooltip_t491261851::get_offset_of_location_11(),
	GvrTooltip_t491261851::get_offset_of_text_12(),
	GvrTooltip_t491261851::get_offset_of_alwaysVisible_13(),
	GvrTooltip_t491261851::get_offset_of_isOnLeft_14(),
	GvrTooltip_t491261851::get_offset_of_rectTransform_15(),
	GvrTooltip_t491261851::get_offset_of_canvasGroup_16(),
	GvrTooltip_t491261851::get_offset_of_U3CArmModelU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (Location_t4049147969)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2468[6] = 
{
	Location_t4049147969::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (GvrBasePointer_t822782720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[4] = 
{
	GvrBasePointer_t822782720::get_offset_of_raycastMode_4(),
	GvrBasePointer_t822782720::get_offset_of_overridePointerCamera_5(),
	GvrBasePointer_t822782720::get_offset_of_U3CShouldUseExitRadiusForRaycastU3Ek__BackingField_6(),
	GvrBasePointer_t822782720::get_offset_of_U3CControllerInputDeviceU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (RaycastMode_t1421504155)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2470[4] = 
{
	RaycastMode_t1421504155::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (PointerRay_t3451016640)+ sizeof (RuntimeObject), sizeof(PointerRay_t3451016640 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2471[3] = 
{
	PointerRay_t3451016640::get_offset_of_ray_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointerRay_t3451016640::get_offset_of_distanceFromStart_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PointerRay_t3451016640::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (GvrBasePointerRaycaster_t3158030611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[2] = 
{
	GvrBasePointerRaycaster_t3158030611::get_offset_of_lastRay_4(),
	GvrBasePointerRaycaster_t3158030611::get_offset_of_U3CCurrentRaycastModeForHybridU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (GvrExecuteEventsExtension_t3940832397), -1, sizeof(GvrExecuteEventsExtension_t3940832397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2474[2] = 
{
	GvrExecuteEventsExtension_t3940832397_StaticFields::get_offset_of_s_HoverHandler_0(),
	GvrExecuteEventsExtension_t3940832397_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (GvrPointerGraphicRaycaster_t348070255), -1, sizeof(GvrPointerGraphicRaycaster_t348070255_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2475[9] = 
{
	0,
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_ignoreReversedGraphics_7(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_blockingObjects_8(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_blockingMask_9(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_targetCanvas_10(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_raycastResults_11(),
	GvrPointerGraphicRaycaster_t348070255::get_offset_of_cachedPointerEventCamera_12(),
	GvrPointerGraphicRaycaster_t348070255_StaticFields::get_offset_of_sortedGraphics_13(),
	GvrPointerGraphicRaycaster_t348070255_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (BlockingObjects_t813457210)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2476[5] = 
{
	BlockingObjects_t813457210::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (GvrPointerPhysicsRaycaster_t304947617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[6] = 
{
	0,
	0,
	GvrPointerPhysicsRaycaster_t304947617::get_offset_of_raycasterEventMask_8(),
	GvrPointerPhysicsRaycaster_t304947617::get_offset_of_maxRaycastHits_9(),
	GvrPointerPhysicsRaycaster_t304947617::get_offset_of_hits_10(),
	GvrPointerPhysicsRaycaster_t304947617::get_offset_of_hitComparer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (HitComparer_t1984117280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (GvrPointerScrollInput_t3738414627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[17] = 
{
	0,
	0,
	GvrPointerScrollInput_t3738414627::get_offset_of_inertia_2(),
	GvrPointerScrollInput_t3738414627::get_offset_of_decelerationRate_3(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrPointerScrollInput_t3738414627::get_offset_of_scrollHandlers_15(),
	GvrPointerScrollInput_t3738414627::get_offset_of_scrollingObjects_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (ScrollInfo_t1733197845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[6] = 
{
	ScrollInfo_t1733197845::get_offset_of_isScrollingX_0(),
	ScrollInfo_t1733197845::get_offset_of_isScrollingY_1(),
	ScrollInfo_t1733197845::get_offset_of_initScroll_2(),
	ScrollInfo_t1733197845::get_offset_of_lastScroll_3(),
	ScrollInfo_t1733197845::get_offset_of_scrollVelocity_4(),
	ScrollInfo_t1733197845::get_offset_of_scrollSettings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (GvrScrollSettings_t33837608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[2] = 
{
	GvrScrollSettings_t33837608::get_offset_of_inertiaOverride_4(),
	GvrScrollSettings_t33837608::get_offset_of_decelerationRateOverride_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (GvrAllEventsTrigger_t2174159095), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[7] = 
{
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerClick_4(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerDown_5(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerUp_6(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerEnter_7(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnPointerExit_8(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_OnScroll_9(),
	GvrAllEventsTrigger_t2174159095::get_offset_of_listenersAdded_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (TriggerEvent_t1225966107), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (GvrEventExecutor_t2551165091), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[1] = 
{
	GvrEventExecutor_t2551165091::get_offset_of_eventTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (EventDelegate_t481206256), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (GvrPointerInputModule_t4124566278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[4] = 
{
	GvrPointerInputModule_t4124566278::get_offset_of_vrModeOnly_10(),
	GvrPointerInputModule_t4124566278::get_offset_of_scrollInput_11(),
	GvrPointerInputModule_t4124566278::get_offset_of_U3CImplU3Ek__BackingField_12(),
	GvrPointerInputModule_t4124566278::get_offset_of_U3CEventExecutorU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (GvrPointerInputModuleImpl_t2260544298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[9] = 
{
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CModuleControllerU3Ek__BackingField_0(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CEventExecutorU3Ek__BackingField_1(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CVrModeOnlyU3Ek__BackingField_2(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CScrollInputU3Ek__BackingField_3(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_U3CCurrentEventDataU3Ek__BackingField_4(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_pointer_5(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_lastPose_6(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_isPointerHovering_7(),
	GvrPointerInputModuleImpl_t2260544298::get_offset_of_isActive_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (GvrCardboardHelpers_t3063197799), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (GvrEditorEmulator_t2879390294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (GvrSettings_t2768269135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (ViewerPlatformType_t1613185256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2494[4] = 
{
	ViewerPlatformType_t1613185256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (UserPrefsHandedness_t3496962503)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2495[4] = 
{
	UserPrefsHandedness_t3496962503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (GvrUnitySdkVersion_t2768813923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (GvrHeadset_t1874684537), -1, sizeof(GvrHeadset_t1874684537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2497[7] = 
{
	GvrHeadset_t1874684537_StaticFields::get_offset_of_instance_4(),
	GvrHeadset_t1874684537::get_offset_of_headsetProvider_5(),
	GvrHeadset_t1874684537::get_offset_of_headsetState_6(),
	GvrHeadset_t1874684537::get_offset_of_standaloneUpdate_7(),
	GvrHeadset_t1874684537::get_offset_of_waitForEndOfFrame_8(),
	GvrHeadset_t1874684537::get_offset_of_safetyRegionDelegate_9(),
	GvrHeadset_t1874684537::get_offset_of_recenterDelegate_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (OnSafetyRegionEvent_t980662704), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (OnRecenterEvent_t1755824842), sizeof(Il2CppMethodPointer), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
