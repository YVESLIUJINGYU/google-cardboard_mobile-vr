﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// GoogleVR.Demos.DemoInputManager
struct DemoInputManager_t2406750443;
// GoogleVR.HelloVR.HeadsetDemoManager
struct HeadsetDemoManager_t2048377926;
// GoogleVR.VideoDemo.ButtonEvent
struct ButtonEvent_t4044041544;
// GoogleVR.VideoDemo.MenuHandler
struct MenuHandler_t3978103572;
// GoogleVR.VideoDemo.VideoControlsManager
struct VideoControlsManager_t1556912537;
// GvrControllerHand[]
struct GvrControllerHandU5BU5D_t2583795907;
// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t3546202735;
// System.Action
struct Action_t1264377477;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// System.Collections.Generic.List`1<GvrAudioRoom>
struct List_1_t2506071902;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<TMPro.KerningPair>
struct List_1_t3742930331;
// System.Collections.Generic.List`1<TMPro.TMP_Text>
struct List_1_t4071693616;
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t3593973608;
// System.EventHandler
struct EventHandler_t1348719766;
// System.Func`2<TMPro.KerningPair,System.UInt32>
struct Func_2_t2163425431;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Predicate`1<System.String>
struct Predicate_1_t2672744813;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// TMPro.FastAction
struct FastAction_t3491443480;
// TMPro.FastAction`1<System.Boolean>
struct FastAction_1_t3758825850;
// TMPro.FastAction`1<TMPro.TMP_ColorGradient>
struct FastAction_1_t3044626357;
// TMPro.FastAction`1<UnityEngine.Object>
struct FastAction_1_t4292545838;
// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset>
struct FastAction_2_t2230967576;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro>
struct FastAction_2_t4260179116;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI>
struct FastAction_2_t2395899227;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Material>
struct FastAction_2_t2206961073;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Object>
struct FastAction_2_t2497593903;
// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs>
struct FastAction_2_t2884460249;
// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>
struct FastAction_3_t58649212;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t648826345;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t1930184704;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t3678055768;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2496920137;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_Glyph
struct TMP_Glyph_t581847833;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t4120149533;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t3558768157;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t3365986247;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_t2463031060;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2836635477;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// TMPro.TMP_TextElement
struct TMP_TextElement_t129727469;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t3766301798;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t3552942253;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t284240280;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t1785403678;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// VRFocusMarkerBase
struct VRFocusMarkerBase_t1705746600;
// VRFocusRayCaster
struct VRFocusRayCaster_t1481207659;
// VRInput
struct VRInput_t441285841;
// VRInteractableBase
struct VRInteractableBase_t641439594;




#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSTATUSUPDATELOOPU3EC__ITERATOR0_T1308120617_H
#define U3CSTATUSUPDATELOOPU3EC__ITERATOR0_T1308120617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.HelloVR.HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0
struct  U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617  : public RuntimeObject
{
public:
	// GoogleVR.HelloVR.HeadsetDemoManager GoogleVR.HelloVR.HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0::$this
	HeadsetDemoManager_t2048377926 * ___U24this_0;
	// System.Object GoogleVR.HelloVR.HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GoogleVR.HelloVR.HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GoogleVR.HelloVR.HeadsetDemoManager/<StatusUpdateLoop>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617, ___U24this_0)); }
	inline HeadsetDemoManager_t2048377926 * get_U24this_0() const { return ___U24this_0; }
	inline HeadsetDemoManager_t2048377926 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(HeadsetDemoManager_t2048377926 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTATUSUPDATELOOPU3EC__ITERATOR0_T1308120617_H
#ifndef U3CDOAPPEARU3EC__ITERATOR0_T1351014511_H
#define U3CDOAPPEARU3EC__ITERATOR0_T1351014511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.MenuHandler/<DoAppear>c__Iterator0
struct  U3CDoAppearU3Ec__Iterator0_t1351014511  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup GoogleVR.VideoDemo.MenuHandler/<DoAppear>c__Iterator0::<cg>__0
	CanvasGroup_t4083511760 * ___U3CcgU3E__0_0;
	// GoogleVR.VideoDemo.MenuHandler GoogleVR.VideoDemo.MenuHandler/<DoAppear>c__Iterator0::$this
	MenuHandler_t3978103572 * ___U24this_1;
	// System.Object GoogleVR.VideoDemo.MenuHandler/<DoAppear>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GoogleVR.VideoDemo.MenuHandler/<DoAppear>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GoogleVR.VideoDemo.MenuHandler/<DoAppear>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcgU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1351014511, ___U3CcgU3E__0_0)); }
	inline CanvasGroup_t4083511760 * get_U3CcgU3E__0_0() const { return ___U3CcgU3E__0_0; }
	inline CanvasGroup_t4083511760 ** get_address_of_U3CcgU3E__0_0() { return &___U3CcgU3E__0_0; }
	inline void set_U3CcgU3E__0_0(CanvasGroup_t4083511760 * value)
	{
		___U3CcgU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcgU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1351014511, ___U24this_1)); }
	inline MenuHandler_t3978103572 * get_U24this_1() const { return ___U24this_1; }
	inline MenuHandler_t3978103572 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MenuHandler_t3978103572 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1351014511, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1351014511, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1351014511, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOAPPEARU3EC__ITERATOR0_T1351014511_H
#ifndef U3CDOFADEU3EC__ITERATOR1_T1581559377_H
#define U3CDOFADEU3EC__ITERATOR1_T1581559377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.MenuHandler/<DoFade>c__Iterator1
struct  U3CDoFadeU3Ec__Iterator1_t1581559377  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup GoogleVR.VideoDemo.MenuHandler/<DoFade>c__Iterator1::<cg>__0
	CanvasGroup_t4083511760 * ___U3CcgU3E__0_0;
	// GoogleVR.VideoDemo.MenuHandler GoogleVR.VideoDemo.MenuHandler/<DoFade>c__Iterator1::$this
	MenuHandler_t3978103572 * ___U24this_1;
	// System.Object GoogleVR.VideoDemo.MenuHandler/<DoFade>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GoogleVR.VideoDemo.MenuHandler/<DoFade>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 GoogleVR.VideoDemo.MenuHandler/<DoFade>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcgU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t1581559377, ___U3CcgU3E__0_0)); }
	inline CanvasGroup_t4083511760 * get_U3CcgU3E__0_0() const { return ___U3CcgU3E__0_0; }
	inline CanvasGroup_t4083511760 ** get_address_of_U3CcgU3E__0_0() { return &___U3CcgU3E__0_0; }
	inline void set_U3CcgU3E__0_0(CanvasGroup_t4083511760 * value)
	{
		___U3CcgU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcgU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t1581559377, ___U24this_1)); }
	inline MenuHandler_t3978103572 * get_U24this_1() const { return ___U24this_1; }
	inline MenuHandler_t3978103572 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MenuHandler_t3978103572 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t1581559377, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t1581559377, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t1581559377, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ITERATOR1_T1581559377_H
#ifndef U3CDOAPPEARU3EC__ITERATOR0_T1765908247_H
#define U3CDOAPPEARU3EC__ITERATOR0_T1765908247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.VideoControlsManager/<DoAppear>c__Iterator0
struct  U3CDoAppearU3Ec__Iterator0_t1765908247  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup GoogleVR.VideoDemo.VideoControlsManager/<DoAppear>c__Iterator0::<cg>__0
	CanvasGroup_t4083511760 * ___U3CcgU3E__0_0;
	// GoogleVR.VideoDemo.VideoControlsManager GoogleVR.VideoDemo.VideoControlsManager/<DoAppear>c__Iterator0::$this
	VideoControlsManager_t1556912537 * ___U24this_1;
	// System.Object GoogleVR.VideoDemo.VideoControlsManager/<DoAppear>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GoogleVR.VideoDemo.VideoControlsManager/<DoAppear>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 GoogleVR.VideoDemo.VideoControlsManager/<DoAppear>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcgU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1765908247, ___U3CcgU3E__0_0)); }
	inline CanvasGroup_t4083511760 * get_U3CcgU3E__0_0() const { return ___U3CcgU3E__0_0; }
	inline CanvasGroup_t4083511760 ** get_address_of_U3CcgU3E__0_0() { return &___U3CcgU3E__0_0; }
	inline void set_U3CcgU3E__0_0(CanvasGroup_t4083511760 * value)
	{
		___U3CcgU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcgU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1765908247, ___U24this_1)); }
	inline VideoControlsManager_t1556912537 * get_U24this_1() const { return ___U24this_1; }
	inline VideoControlsManager_t1556912537 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(VideoControlsManager_t1556912537 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1765908247, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1765908247, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoAppearU3Ec__Iterator0_t1765908247, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOAPPEARU3EC__ITERATOR0_T1765908247_H
#ifndef U3CDOFADEU3EC__ITERATOR1_T3288079470_H
#define U3CDOFADEU3EC__ITERATOR1_T3288079470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.VideoControlsManager/<DoFade>c__Iterator1
struct  U3CDoFadeU3Ec__Iterator1_t3288079470  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup GoogleVR.VideoDemo.VideoControlsManager/<DoFade>c__Iterator1::<cg>__0
	CanvasGroup_t4083511760 * ___U3CcgU3E__0_0;
	// GoogleVR.VideoDemo.VideoControlsManager GoogleVR.VideoDemo.VideoControlsManager/<DoFade>c__Iterator1::$this
	VideoControlsManager_t1556912537 * ___U24this_1;
	// System.Object GoogleVR.VideoDemo.VideoControlsManager/<DoFade>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean GoogleVR.VideoDemo.VideoControlsManager/<DoFade>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 GoogleVR.VideoDemo.VideoControlsManager/<DoFade>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcgU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t3288079470, ___U3CcgU3E__0_0)); }
	inline CanvasGroup_t4083511760 * get_U3CcgU3E__0_0() const { return ___U3CcgU3E__0_0; }
	inline CanvasGroup_t4083511760 ** get_address_of_U3CcgU3E__0_0() { return &___U3CcgU3E__0_0; }
	inline void set_U3CcgU3E__0_0(CanvasGroup_t4083511760 * value)
	{
		___U3CcgU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcgU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t3288079470, ___U24this_1)); }
	inline VideoControlsManager_t1556912537 * get_U24this_1() const { return ___U24this_1; }
	inline VideoControlsManager_t1556912537 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(VideoControlsManager_t1556912537 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t3288079470, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t3288079470, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDoFadeU3Ec__Iterator1_t3288079470, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOFADEU3EC__ITERATOR1_T3288079470_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef FACEINFO_T2243299176_H
#define FACEINFO_T2243299176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FaceInfo
struct  FaceInfo_t2243299176  : public RuntimeObject
{
public:
	// System.String TMPro.FaceInfo::Name
	String_t* ___Name_0;
	// System.Single TMPro.FaceInfo::PointSize
	float ___PointSize_1;
	// System.Single TMPro.FaceInfo::Scale
	float ___Scale_2;
	// System.Int32 TMPro.FaceInfo::CharacterCount
	int32_t ___CharacterCount_3;
	// System.Single TMPro.FaceInfo::LineHeight
	float ___LineHeight_4;
	// System.Single TMPro.FaceInfo::Baseline
	float ___Baseline_5;
	// System.Single TMPro.FaceInfo::Ascender
	float ___Ascender_6;
	// System.Single TMPro.FaceInfo::CapHeight
	float ___CapHeight_7;
	// System.Single TMPro.FaceInfo::Descender
	float ___Descender_8;
	// System.Single TMPro.FaceInfo::CenterLine
	float ___CenterLine_9;
	// System.Single TMPro.FaceInfo::SuperscriptOffset
	float ___SuperscriptOffset_10;
	// System.Single TMPro.FaceInfo::SubscriptOffset
	float ___SubscriptOffset_11;
	// System.Single TMPro.FaceInfo::SubSize
	float ___SubSize_12;
	// System.Single TMPro.FaceInfo::Underline
	float ___Underline_13;
	// System.Single TMPro.FaceInfo::UnderlineThickness
	float ___UnderlineThickness_14;
	// System.Single TMPro.FaceInfo::strikethrough
	float ___strikethrough_15;
	// System.Single TMPro.FaceInfo::strikethroughThickness
	float ___strikethroughThickness_16;
	// System.Single TMPro.FaceInfo::TabWidth
	float ___TabWidth_17;
	// System.Single TMPro.FaceInfo::Padding
	float ___Padding_18;
	// System.Single TMPro.FaceInfo::AtlasWidth
	float ___AtlasWidth_19;
	// System.Single TMPro.FaceInfo::AtlasHeight
	float ___AtlasHeight_20;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_PointSize_1() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___PointSize_1)); }
	inline float get_PointSize_1() const { return ___PointSize_1; }
	inline float* get_address_of_PointSize_1() { return &___PointSize_1; }
	inline void set_PointSize_1(float value)
	{
		___PointSize_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_CharacterCount_3() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CharacterCount_3)); }
	inline int32_t get_CharacterCount_3() const { return ___CharacterCount_3; }
	inline int32_t* get_address_of_CharacterCount_3() { return &___CharacterCount_3; }
	inline void set_CharacterCount_3(int32_t value)
	{
		___CharacterCount_3 = value;
	}

	inline static int32_t get_offset_of_LineHeight_4() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___LineHeight_4)); }
	inline float get_LineHeight_4() const { return ___LineHeight_4; }
	inline float* get_address_of_LineHeight_4() { return &___LineHeight_4; }
	inline void set_LineHeight_4(float value)
	{
		___LineHeight_4 = value;
	}

	inline static int32_t get_offset_of_Baseline_5() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Baseline_5)); }
	inline float get_Baseline_5() const { return ___Baseline_5; }
	inline float* get_address_of_Baseline_5() { return &___Baseline_5; }
	inline void set_Baseline_5(float value)
	{
		___Baseline_5 = value;
	}

	inline static int32_t get_offset_of_Ascender_6() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Ascender_6)); }
	inline float get_Ascender_6() const { return ___Ascender_6; }
	inline float* get_address_of_Ascender_6() { return &___Ascender_6; }
	inline void set_Ascender_6(float value)
	{
		___Ascender_6 = value;
	}

	inline static int32_t get_offset_of_CapHeight_7() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CapHeight_7)); }
	inline float get_CapHeight_7() const { return ___CapHeight_7; }
	inline float* get_address_of_CapHeight_7() { return &___CapHeight_7; }
	inline void set_CapHeight_7(float value)
	{
		___CapHeight_7 = value;
	}

	inline static int32_t get_offset_of_Descender_8() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Descender_8)); }
	inline float get_Descender_8() const { return ___Descender_8; }
	inline float* get_address_of_Descender_8() { return &___Descender_8; }
	inline void set_Descender_8(float value)
	{
		___Descender_8 = value;
	}

	inline static int32_t get_offset_of_CenterLine_9() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___CenterLine_9)); }
	inline float get_CenterLine_9() const { return ___CenterLine_9; }
	inline float* get_address_of_CenterLine_9() { return &___CenterLine_9; }
	inline void set_CenterLine_9(float value)
	{
		___CenterLine_9 = value;
	}

	inline static int32_t get_offset_of_SuperscriptOffset_10() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SuperscriptOffset_10)); }
	inline float get_SuperscriptOffset_10() const { return ___SuperscriptOffset_10; }
	inline float* get_address_of_SuperscriptOffset_10() { return &___SuperscriptOffset_10; }
	inline void set_SuperscriptOffset_10(float value)
	{
		___SuperscriptOffset_10 = value;
	}

	inline static int32_t get_offset_of_SubscriptOffset_11() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SubscriptOffset_11)); }
	inline float get_SubscriptOffset_11() const { return ___SubscriptOffset_11; }
	inline float* get_address_of_SubscriptOffset_11() { return &___SubscriptOffset_11; }
	inline void set_SubscriptOffset_11(float value)
	{
		___SubscriptOffset_11 = value;
	}

	inline static int32_t get_offset_of_SubSize_12() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___SubSize_12)); }
	inline float get_SubSize_12() const { return ___SubSize_12; }
	inline float* get_address_of_SubSize_12() { return &___SubSize_12; }
	inline void set_SubSize_12(float value)
	{
		___SubSize_12 = value;
	}

	inline static int32_t get_offset_of_Underline_13() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Underline_13)); }
	inline float get_Underline_13() const { return ___Underline_13; }
	inline float* get_address_of_Underline_13() { return &___Underline_13; }
	inline void set_Underline_13(float value)
	{
		___Underline_13 = value;
	}

	inline static int32_t get_offset_of_UnderlineThickness_14() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___UnderlineThickness_14)); }
	inline float get_UnderlineThickness_14() const { return ___UnderlineThickness_14; }
	inline float* get_address_of_UnderlineThickness_14() { return &___UnderlineThickness_14; }
	inline void set_UnderlineThickness_14(float value)
	{
		___UnderlineThickness_14 = value;
	}

	inline static int32_t get_offset_of_strikethrough_15() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___strikethrough_15)); }
	inline float get_strikethrough_15() const { return ___strikethrough_15; }
	inline float* get_address_of_strikethrough_15() { return &___strikethrough_15; }
	inline void set_strikethrough_15(float value)
	{
		___strikethrough_15 = value;
	}

	inline static int32_t get_offset_of_strikethroughThickness_16() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___strikethroughThickness_16)); }
	inline float get_strikethroughThickness_16() const { return ___strikethroughThickness_16; }
	inline float* get_address_of_strikethroughThickness_16() { return &___strikethroughThickness_16; }
	inline void set_strikethroughThickness_16(float value)
	{
		___strikethroughThickness_16 = value;
	}

	inline static int32_t get_offset_of_TabWidth_17() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___TabWidth_17)); }
	inline float get_TabWidth_17() const { return ___TabWidth_17; }
	inline float* get_address_of_TabWidth_17() { return &___TabWidth_17; }
	inline void set_TabWidth_17(float value)
	{
		___TabWidth_17 = value;
	}

	inline static int32_t get_offset_of_Padding_18() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___Padding_18)); }
	inline float get_Padding_18() const { return ___Padding_18; }
	inline float* get_address_of_Padding_18() { return &___Padding_18; }
	inline void set_Padding_18(float value)
	{
		___Padding_18 = value;
	}

	inline static int32_t get_offset_of_AtlasWidth_19() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___AtlasWidth_19)); }
	inline float get_AtlasWidth_19() const { return ___AtlasWidth_19; }
	inline float* get_address_of_AtlasWidth_19() { return &___AtlasWidth_19; }
	inline void set_AtlasWidth_19(float value)
	{
		___AtlasWidth_19 = value;
	}

	inline static int32_t get_offset_of_AtlasHeight_20() { return static_cast<int32_t>(offsetof(FaceInfo_t2243299176, ___AtlasHeight_20)); }
	inline float get_AtlasHeight_20() const { return ___AtlasHeight_20; }
	inline float* get_address_of_AtlasHeight_20() { return &___AtlasHeight_20; }
	inline void set_AtlasHeight_20(float value)
	{
		___AtlasHeight_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEINFO_T2243299176_H
#ifndef KERNINGTABLE_T2322366871_H
#define KERNINGTABLE_T2322366871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable
struct  KerningTable_t2322366871  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.KerningPair> TMPro.KerningTable::kerningPairs
	List_1_t3742930331 * ___kerningPairs_0;

public:
	inline static int32_t get_offset_of_kerningPairs_0() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871, ___kerningPairs_0)); }
	inline List_1_t3742930331 * get_kerningPairs_0() const { return ___kerningPairs_0; }
	inline List_1_t3742930331 ** get_address_of_kerningPairs_0() { return &___kerningPairs_0; }
	inline void set_kerningPairs_0(List_1_t3742930331 * value)
	{
		___kerningPairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___kerningPairs_0), value);
	}
};

struct KerningTable_t2322366871_StaticFields
{
public:
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache0
	Func_2_t2163425431 * ___U3CU3Ef__amU24cache0_1;
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache1
	Func_2_t2163425431 * ___U3CU3Ef__amU24cache1_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t2163425431 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t2163425431 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t2163425431 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(KerningTable_t2322366871_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Func_2_t2163425431 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Func_2_t2163425431 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Func_2_t2163425431 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGTABLE_T2322366871_H
#ifndef U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#define U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1
struct  U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T3257710262_H
#ifndef U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#define U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddKerningPair>c__AnonStorey0
struct  U3CAddKerningPairU3Ec__AnonStorey0_t2688361982  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t2688361982, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t2688361982, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2688361982_H
#ifndef U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#define U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2
struct  U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T1355166074_H
#ifndef SHADERUTILITIES_T714255158_H
#define SHADERUTILITIES_T714255158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t714255158  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t714255158_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_Padding
	int32_t ___ID_Padding_16;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_18;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_19;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_20;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_21;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_22;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_23;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_26;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_27;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_28;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_30;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_31;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_32;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_33;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_34;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_35;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_36;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_37;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_38;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_42;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_43;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_46;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_47;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_48;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_49;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_50;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_51;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_53;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_54;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_55;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_56;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_57;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_58;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_59;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_Padding_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_Padding_16)); }
	inline int32_t get_ID_Padding_16() const { return ___ID_Padding_16; }
	inline int32_t* get_address_of_ID_Padding_16() { return &___ID_Padding_16; }
	inline void set_ID_Padding_16(int32_t value)
	{
		___ID_Padding_16 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GradientScale_17)); }
	inline int32_t get_ID_GradientScale_17() const { return ___ID_GradientScale_17; }
	inline int32_t* get_address_of_ID_GradientScale_17() { return &___ID_GradientScale_17; }
	inline void set_ID_GradientScale_17(int32_t value)
	{
		___ID_GradientScale_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleX_18)); }
	inline int32_t get_ID_ScaleX_18() const { return ___ID_ScaleX_18; }
	inline int32_t* get_address_of_ID_ScaleX_18() { return &___ID_ScaleX_18; }
	inline void set_ID_ScaleX_18(int32_t value)
	{
		___ID_ScaleX_18 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleY_19)); }
	inline int32_t get_ID_ScaleY_19() const { return ___ID_ScaleY_19; }
	inline int32_t* get_address_of_ID_ScaleY_19() { return &___ID_ScaleY_19; }
	inline void set_ID_ScaleY_19(int32_t value)
	{
		___ID_ScaleY_19 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_PerspectiveFilter_20)); }
	inline int32_t get_ID_PerspectiveFilter_20() const { return ___ID_PerspectiveFilter_20; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_20() { return &___ID_PerspectiveFilter_20; }
	inline void set_ID_PerspectiveFilter_20(int32_t value)
	{
		___ID_PerspectiveFilter_20 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureWidth_21)); }
	inline int32_t get_ID_TextureWidth_21() const { return ___ID_TextureWidth_21; }
	inline int32_t* get_address_of_ID_TextureWidth_21() { return &___ID_TextureWidth_21; }
	inline void set_ID_TextureWidth_21(int32_t value)
	{
		___ID_TextureWidth_21 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_TextureHeight_22)); }
	inline int32_t get_ID_TextureHeight_22() const { return ___ID_TextureHeight_22; }
	inline int32_t* get_address_of_ID_TextureHeight_22() { return &___ID_TextureHeight_22; }
	inline void set_ID_TextureHeight_22(int32_t value)
	{
		___ID_TextureHeight_22 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_BevelAmount_23)); }
	inline int32_t get_ID_BevelAmount_23() const { return ___ID_BevelAmount_23; }
	inline int32_t* get_address_of_ID_BevelAmount_23() { return &___ID_BevelAmount_23; }
	inline void set_ID_BevelAmount_23(int32_t value)
	{
		___ID_BevelAmount_23 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowColor_24)); }
	inline int32_t get_ID_GlowColor_24() const { return ___ID_GlowColor_24; }
	inline int32_t* get_address_of_ID_GlowColor_24() { return &___ID_GlowColor_24; }
	inline void set_ID_GlowColor_24(int32_t value)
	{
		___ID_GlowColor_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOffset_25)); }
	inline int32_t get_ID_GlowOffset_25() const { return ___ID_GlowOffset_25; }
	inline int32_t* get_address_of_ID_GlowOffset_25() { return &___ID_GlowOffset_25; }
	inline void set_ID_GlowOffset_25(int32_t value)
	{
		___ID_GlowOffset_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowPower_26)); }
	inline int32_t get_ID_GlowPower_26() const { return ___ID_GlowPower_26; }
	inline int32_t* get_address_of_ID_GlowPower_26() { return &___ID_GlowPower_26; }
	inline void set_ID_GlowPower_26(int32_t value)
	{
		___ID_GlowPower_26 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_GlowOuter_27)); }
	inline int32_t get_ID_GlowOuter_27() const { return ___ID_GlowOuter_27; }
	inline int32_t* get_address_of_ID_GlowOuter_27() { return &___ID_GlowOuter_27; }
	inline void set_ID_GlowOuter_27(int32_t value)
	{
		___ID_GlowOuter_27 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_LightAngle_28)); }
	inline int32_t get_ID_LightAngle_28() const { return ___ID_LightAngle_28; }
	inline int32_t* get_address_of_ID_LightAngle_28() { return &___ID_LightAngle_28; }
	inline void set_ID_LightAngle_28(int32_t value)
	{
		___ID_LightAngle_28 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMap_29)); }
	inline int32_t get_ID_EnvMap_29() const { return ___ID_EnvMap_29; }
	inline int32_t* get_address_of_ID_EnvMap_29() { return &___ID_EnvMap_29; }
	inline void set_ID_EnvMap_29(int32_t value)
	{
		___ID_EnvMap_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrix_30)); }
	inline int32_t get_ID_EnvMatrix_30() const { return ___ID_EnvMatrix_30; }
	inline int32_t* get_address_of_ID_EnvMatrix_30() { return &___ID_EnvMatrix_30; }
	inline void set_ID_EnvMatrix_30(int32_t value)
	{
		___ID_EnvMatrix_30 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_EnvMatrixRotation_31)); }
	inline int32_t get_ID_EnvMatrixRotation_31() const { return ___ID_EnvMatrixRotation_31; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_31() { return &___ID_EnvMatrixRotation_31; }
	inline void set_ID_EnvMatrixRotation_31(int32_t value)
	{
		___ID_EnvMatrixRotation_31 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskCoord_32)); }
	inline int32_t get_ID_MaskCoord_32() const { return ___ID_MaskCoord_32; }
	inline int32_t* get_address_of_ID_MaskCoord_32() { return &___ID_MaskCoord_32; }
	inline void set_ID_MaskCoord_32(int32_t value)
	{
		___ID_MaskCoord_32 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ClipRect_33)); }
	inline int32_t get_ID_ClipRect_33() const { return ___ID_ClipRect_33; }
	inline int32_t* get_address_of_ID_ClipRect_33() { return &___ID_ClipRect_33; }
	inline void set_ID_ClipRect_33(int32_t value)
	{
		___ID_ClipRect_33 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessX_34)); }
	inline int32_t get_ID_MaskSoftnessX_34() const { return ___ID_MaskSoftnessX_34; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_34() { return &___ID_MaskSoftnessX_34; }
	inline void set_ID_MaskSoftnessX_34(int32_t value)
	{
		___ID_MaskSoftnessX_34 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_MaskSoftnessY_35)); }
	inline int32_t get_ID_MaskSoftnessY_35() const { return ___ID_MaskSoftnessY_35; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_35() { return &___ID_MaskSoftnessY_35; }
	inline void set_ID_MaskSoftnessY_35(int32_t value)
	{
		___ID_MaskSoftnessY_35 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetX_36)); }
	inline int32_t get_ID_VertexOffsetX_36() const { return ___ID_VertexOffsetX_36; }
	inline int32_t* get_address_of_ID_VertexOffsetX_36() { return &___ID_VertexOffsetX_36; }
	inline void set_ID_VertexOffsetX_36(int32_t value)
	{
		___ID_VertexOffsetX_36 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_VertexOffsetY_37)); }
	inline int32_t get_ID_VertexOffsetY_37() const { return ___ID_VertexOffsetY_37; }
	inline int32_t* get_address_of_ID_VertexOffsetY_37() { return &___ID_VertexOffsetY_37; }
	inline void set_ID_VertexOffsetY_37(int32_t value)
	{
		___ID_VertexOffsetY_37 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_UseClipRect_38)); }
	inline int32_t get_ID_UseClipRect_38() const { return ___ID_UseClipRect_38; }
	inline int32_t* get_address_of_ID_UseClipRect_38() { return &___ID_UseClipRect_38; }
	inline void set_ID_UseClipRect_38(int32_t value)
	{
		___ID_UseClipRect_38 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilID_39)); }
	inline int32_t get_ID_StencilID_39() const { return ___ID_StencilID_39; }
	inline int32_t* get_address_of_ID_StencilID_39() { return &___ID_StencilID_39; }
	inline void set_ID_StencilID_39(int32_t value)
	{
		___ID_StencilID_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilOp_40)); }
	inline int32_t get_ID_StencilOp_40() const { return ___ID_StencilOp_40; }
	inline int32_t* get_address_of_ID_StencilOp_40() { return &___ID_StencilOp_40; }
	inline void set_ID_StencilOp_40(int32_t value)
	{
		___ID_StencilOp_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilComp_41)); }
	inline int32_t get_ID_StencilComp_41() const { return ___ID_StencilComp_41; }
	inline int32_t* get_address_of_ID_StencilComp_41() { return &___ID_StencilComp_41; }
	inline void set_ID_StencilComp_41(int32_t value)
	{
		___ID_StencilComp_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilReadMask_42)); }
	inline int32_t get_ID_StencilReadMask_42() const { return ___ID_StencilReadMask_42; }
	inline int32_t* get_address_of_ID_StencilReadMask_42() { return &___ID_StencilReadMask_42; }
	inline void set_ID_StencilReadMask_42(int32_t value)
	{
		___ID_StencilReadMask_42 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_StencilWriteMask_43)); }
	inline int32_t get_ID_StencilWriteMask_43() const { return ___ID_StencilWriteMask_43; }
	inline int32_t* get_address_of_ID_StencilWriteMask_43() { return &___ID_StencilWriteMask_43; }
	inline void set_ID_StencilWriteMask_43(int32_t value)
	{
		___ID_StencilWriteMask_43 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ShaderFlags_44)); }
	inline int32_t get_ID_ShaderFlags_44() const { return ___ID_ShaderFlags_44; }
	inline int32_t* get_address_of_ID_ShaderFlags_44() { return &___ID_ShaderFlags_44; }
	inline void set_ID_ShaderFlags_44(int32_t value)
	{
		___ID_ShaderFlags_44 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_A_45)); }
	inline int32_t get_ID_ScaleRatio_A_45() const { return ___ID_ScaleRatio_A_45; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_45() { return &___ID_ScaleRatio_A_45; }
	inline void set_ID_ScaleRatio_A_45(int32_t value)
	{
		___ID_ScaleRatio_A_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_B_46)); }
	inline int32_t get_ID_ScaleRatio_B_46() const { return ___ID_ScaleRatio_B_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_46() { return &___ID_ScaleRatio_B_46; }
	inline void set_ID_ScaleRatio_B_46(int32_t value)
	{
		___ID_ScaleRatio_B_46 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ID_ScaleRatio_C_47)); }
	inline int32_t get_ID_ScaleRatio_C_47() const { return ___ID_ScaleRatio_C_47; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_47() { return &___ID_ScaleRatio_C_47; }
	inline void set_ID_ScaleRatio_C_47(int32_t value)
	{
		___ID_ScaleRatio_C_47 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Bevel_48)); }
	inline String_t* get_Keyword_Bevel_48() const { return ___Keyword_Bevel_48; }
	inline String_t** get_address_of_Keyword_Bevel_48() { return &___Keyword_Bevel_48; }
	inline void set_Keyword_Bevel_48(String_t* value)
	{
		___Keyword_Bevel_48 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_48), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Glow_49)); }
	inline String_t* get_Keyword_Glow_49() const { return ___Keyword_Glow_49; }
	inline String_t** get_address_of_Keyword_Glow_49() { return &___Keyword_Glow_49; }
	inline void set_Keyword_Glow_49(String_t* value)
	{
		___Keyword_Glow_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Underlay_50)); }
	inline String_t* get_Keyword_Underlay_50() const { return ___Keyword_Underlay_50; }
	inline String_t** get_address_of_Keyword_Underlay_50() { return &___Keyword_Underlay_50; }
	inline void set_Keyword_Underlay_50(String_t* value)
	{
		___Keyword_Underlay_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_50), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Ratios_51)); }
	inline String_t* get_Keyword_Ratios_51() const { return ___Keyword_Ratios_51; }
	inline String_t** get_address_of_Keyword_Ratios_51() { return &___Keyword_Ratios_51; }
	inline void set_Keyword_Ratios_51(String_t* value)
	{
		___Keyword_Ratios_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_51), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_SOFT_52)); }
	inline String_t* get_Keyword_MASK_SOFT_52() const { return ___Keyword_MASK_SOFT_52; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_52() { return &___Keyword_MASK_SOFT_52; }
	inline void set_Keyword_MASK_SOFT_52(String_t* value)
	{
		___Keyword_MASK_SOFT_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_HARD_53)); }
	inline String_t* get_Keyword_MASK_HARD_53() const { return ___Keyword_MASK_HARD_53; }
	inline String_t** get_address_of_Keyword_MASK_HARD_53() { return &___Keyword_MASK_HARD_53; }
	inline void set_Keyword_MASK_HARD_53(String_t* value)
	{
		___Keyword_MASK_HARD_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_53), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_MASK_TEX_54)); }
	inline String_t* get_Keyword_MASK_TEX_54() const { return ___Keyword_MASK_TEX_54; }
	inline String_t** get_address_of_Keyword_MASK_TEX_54() { return &___Keyword_MASK_TEX_54; }
	inline void set_Keyword_MASK_TEX_54(String_t* value)
	{
		___Keyword_MASK_TEX_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_54), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___Keyword_Outline_55)); }
	inline String_t* get_Keyword_Outline_55() const { return ___Keyword_Outline_55; }
	inline String_t** get_address_of_Keyword_Outline_55() { return &___Keyword_Outline_55; }
	inline void set_Keyword_Outline_55(String_t* value)
	{
		___Keyword_Outline_55 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_55), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_ZTestMode_56)); }
	inline String_t* get_ShaderTag_ZTestMode_56() const { return ___ShaderTag_ZTestMode_56; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_56() { return &___ShaderTag_ZTestMode_56; }
	inline void set_ShaderTag_ZTestMode_56(String_t* value)
	{
		___ShaderTag_ZTestMode_56 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_56), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___ShaderTag_CullMode_57)); }
	inline String_t* get_ShaderTag_CullMode_57() const { return ___ShaderTag_CullMode_57; }
	inline String_t** get_address_of_ShaderTag_CullMode_57() { return &___ShaderTag_CullMode_57; }
	inline void set_ShaderTag_CullMode_57(String_t* value)
	{
		___ShaderTag_CullMode_57 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_57), value);
	}

	inline static int32_t get_offset_of_m_clamp_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___m_clamp_58)); }
	inline float get_m_clamp_58() const { return ___m_clamp_58; }
	inline float* get_address_of_m_clamp_58() { return &___m_clamp_58; }
	inline void set_m_clamp_58(float value)
	{
		___m_clamp_58 = value;
	}

	inline static int32_t get_offset_of_isInitialized_59() { return static_cast<int32_t>(offsetof(ShaderUtilities_t714255158_StaticFields, ___isInitialized_59)); }
	inline bool get_isInitialized_59() const { return ___isInitialized_59; }
	inline bool* get_address_of_isInitialized_59() { return &___isInitialized_59; }
	inline void set_isInitialized_59(bool value)
	{
		___isInitialized_59 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T714255158_H
#ifndef TMP_FONTUTILITIES_T2599150238_H
#define TMP_FONTUTILITIES_T2599150238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontUtilities
struct  TMP_FontUtilities_t2599150238  : public RuntimeObject
{
public:

public:
};

struct TMP_FontUtilities_t2599150238_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontUtilities::k_searchedFontAssets
	List_1_t128053199 * ___k_searchedFontAssets_0;

public:
	inline static int32_t get_offset_of_k_searchedFontAssets_0() { return static_cast<int32_t>(offsetof(TMP_FontUtilities_t2599150238_StaticFields, ___k_searchedFontAssets_0)); }
	inline List_1_t128053199 * get_k_searchedFontAssets_0() const { return ___k_searchedFontAssets_0; }
	inline List_1_t128053199 ** get_address_of_k_searchedFontAssets_0() { return &___k_searchedFontAssets_0; }
	inline void set_k_searchedFontAssets_0(List_1_t128053199 * value)
	{
		___k_searchedFontAssets_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedFontAssets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTUTILITIES_T2599150238_H
#ifndef TMP_TEXTELEMENT_T129727469_H
#define TMP_TEXTELEMENT_T129727469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t129727469  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t129727469, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T129727469_H
#ifndef TMP_TEXTUTILITIES_T2105690005_H
#define TMP_TEXTUTILITIES_T2105690005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities
struct  TMP_TextUtilities_t2105690005  : public RuntimeObject
{
public:

public:
};

struct TMP_TextUtilities_t2105690005_StaticFields
{
public:
	// UnityEngine.Vector3[] TMPro.TMP_TextUtilities::m_rectWorldCorners
	Vector3U5BU5D_t1718750761* ___m_rectWorldCorners_0;

public:
	inline static int32_t get_offset_of_m_rectWorldCorners_0() { return static_cast<int32_t>(offsetof(TMP_TextUtilities_t2105690005_StaticFields, ___m_rectWorldCorners_0)); }
	inline Vector3U5BU5D_t1718750761* get_m_rectWorldCorners_0() const { return ___m_rectWorldCorners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_rectWorldCorners_0() { return &___m_rectWorldCorners_0; }
	inline void set_m_rectWorldCorners_0(Vector3U5BU5D_t1718750761* value)
	{
		___m_rectWorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectWorldCorners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTUTILITIES_T2105690005_H
#ifndef TMP_UPDATEMANAGER_T4114267509_H
#define TMP_UPDATEMANAGER_T4114267509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateManager
struct  TMP_UpdateManager_t4114267509  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_LayoutRebuildQueue
	List_1_t4071693616 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_GraphicRebuildQueue
	List_1_t4071693616 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t4071693616 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t4071693616 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t4071693616 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t4071693616 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t4071693616 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t4071693616 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateManager_t4114267509_StaticFields
{
public:
	// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::s_Instance
	TMP_UpdateManager_t4114267509 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t4114267509_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateManager_t4114267509 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateManager_t4114267509 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateManager_t4114267509 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEMANAGER_T4114267509_H
#ifndef TMP_UPDATEREGISTRY_T461608481_H
#define TMP_UPDATEREGISTRY_T461608481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateRegistry
struct  TMP_UpdateRegistry_t461608481  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_LayoutRebuildQueue
	List_1_t3593973608 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_LayoutQueueLookup
	Dictionary_2_t1839659084 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_GraphicRebuildQueue
	List_1_t3593973608 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_GraphicQueueLookup
	Dictionary_2_t1839659084 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t3593973608 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t3593973608 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t3593973608 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1839659084 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1839659084 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t3593973608 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t3593973608 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t3593973608 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1839659084 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1839659084 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateRegistry_t461608481_StaticFields
{
public:
	// TMPro.TMP_UpdateRegistry TMPro.TMP_UpdateRegistry::s_Instance
	TMP_UpdateRegistry_t461608481 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t461608481_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateRegistry_t461608481 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateRegistry_t461608481 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateRegistry_t461608481 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEREGISTRY_T461608481_H
#ifndef TMPRO_EVENTMANAGER_T712497257_H
#define TMPRO_EVENTMANAGER_T712497257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_EventManager
struct  TMPro_EventManager_t712497257  : public RuntimeObject
{
public:

public:
};

struct TMPro_EventManager_t712497257_StaticFields
{
public:
	// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs> TMPro.TMPro_EventManager::COMPUTE_DT_EVENT
	FastAction_2_t2884460249 * ___COMPUTE_DT_EVENT_0;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Material> TMPro.TMPro_EventManager::MATERIAL_PROPERTY_EVENT
	FastAction_2_t2206961073 * ___MATERIAL_PROPERTY_EVENT_1;
	// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset> TMPro.TMPro_EventManager::FONT_PROPERTY_EVENT
	FastAction_2_t2230967576 * ___FONT_PROPERTY_EVENT_2;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Object> TMPro.TMPro_EventManager::SPRITE_ASSET_PROPERTY_EVENT
	FastAction_2_t2497593903 * ___SPRITE_ASSET_PROPERTY_EVENT_3;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro> TMPro.TMPro_EventManager::TEXTMESHPRO_PROPERTY_EVENT
	FastAction_2_t4260179116 * ___TEXTMESHPRO_PROPERTY_EVENT_4;
	// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material> TMPro.TMPro_EventManager::DRAG_AND_DROP_MATERIAL_EVENT
	FastAction_3_t58649212 * ___DRAG_AND_DROP_MATERIAL_EVENT_5;
	// TMPro.FastAction`1<System.Boolean> TMPro.TMPro_EventManager::TEXT_STYLE_PROPERTY_EVENT
	FastAction_1_t3758825850 * ___TEXT_STYLE_PROPERTY_EVENT_6;
	// TMPro.FastAction`1<TMPro.TMP_ColorGradient> TMPro.TMPro_EventManager::COLOR_GRADIENT_PROPERTY_EVENT
	FastAction_1_t3044626357 * ___COLOR_GRADIENT_PROPERTY_EVENT_7;
	// TMPro.FastAction TMPro.TMPro_EventManager::TMP_SETTINGS_PROPERTY_EVENT
	FastAction_t3491443480 * ___TMP_SETTINGS_PROPERTY_EVENT_8;
	// TMPro.FastAction TMPro.TMPro_EventManager::RESOURCE_LOAD_EVENT
	FastAction_t3491443480 * ___RESOURCE_LOAD_EVENT_9;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI> TMPro.TMPro_EventManager::TEXTMESHPRO_UGUI_PROPERTY_EVENT
	FastAction_2_t2395899227 * ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10;
	// TMPro.FastAction TMPro.TMPro_EventManager::OnPreRenderObject_Event
	FastAction_t3491443480 * ___OnPreRenderObject_Event_11;
	// TMPro.FastAction`1<UnityEngine.Object> TMPro.TMPro_EventManager::TEXT_CHANGED_EVENT
	FastAction_1_t4292545838 * ___TEXT_CHANGED_EVENT_12;

public:
	inline static int32_t get_offset_of_COMPUTE_DT_EVENT_0() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___COMPUTE_DT_EVENT_0)); }
	inline FastAction_2_t2884460249 * get_COMPUTE_DT_EVENT_0() const { return ___COMPUTE_DT_EVENT_0; }
	inline FastAction_2_t2884460249 ** get_address_of_COMPUTE_DT_EVENT_0() { return &___COMPUTE_DT_EVENT_0; }
	inline void set_COMPUTE_DT_EVENT_0(FastAction_2_t2884460249 * value)
	{
		___COMPUTE_DT_EVENT_0 = value;
		Il2CppCodeGenWriteBarrier((&___COMPUTE_DT_EVENT_0), value);
	}

	inline static int32_t get_offset_of_MATERIAL_PROPERTY_EVENT_1() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___MATERIAL_PROPERTY_EVENT_1)); }
	inline FastAction_2_t2206961073 * get_MATERIAL_PROPERTY_EVENT_1() const { return ___MATERIAL_PROPERTY_EVENT_1; }
	inline FastAction_2_t2206961073 ** get_address_of_MATERIAL_PROPERTY_EVENT_1() { return &___MATERIAL_PROPERTY_EVENT_1; }
	inline void set_MATERIAL_PROPERTY_EVENT_1(FastAction_2_t2206961073 * value)
	{
		___MATERIAL_PROPERTY_EVENT_1 = value;
		Il2CppCodeGenWriteBarrier((&___MATERIAL_PROPERTY_EVENT_1), value);
	}

	inline static int32_t get_offset_of_FONT_PROPERTY_EVENT_2() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___FONT_PROPERTY_EVENT_2)); }
	inline FastAction_2_t2230967576 * get_FONT_PROPERTY_EVENT_2() const { return ___FONT_PROPERTY_EVENT_2; }
	inline FastAction_2_t2230967576 ** get_address_of_FONT_PROPERTY_EVENT_2() { return &___FONT_PROPERTY_EVENT_2; }
	inline void set_FONT_PROPERTY_EVENT_2(FastAction_2_t2230967576 * value)
	{
		___FONT_PROPERTY_EVENT_2 = value;
		Il2CppCodeGenWriteBarrier((&___FONT_PROPERTY_EVENT_2), value);
	}

	inline static int32_t get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___SPRITE_ASSET_PROPERTY_EVENT_3)); }
	inline FastAction_2_t2497593903 * get_SPRITE_ASSET_PROPERTY_EVENT_3() const { return ___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline FastAction_2_t2497593903 ** get_address_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return &___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline void set_SPRITE_ASSET_PROPERTY_EVENT_3(FastAction_2_t2497593903 * value)
	{
		___SPRITE_ASSET_PROPERTY_EVENT_3 = value;
		Il2CppCodeGenWriteBarrier((&___SPRITE_ASSET_PROPERTY_EVENT_3), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXTMESHPRO_PROPERTY_EVENT_4)); }
	inline FastAction_2_t4260179116 * get_TEXTMESHPRO_PROPERTY_EVENT_4() const { return ___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline FastAction_2_t4260179116 ** get_address_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return &___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline void set_TEXTMESHPRO_PROPERTY_EVENT_4(FastAction_2_t4260179116 * value)
	{
		___TEXTMESHPRO_PROPERTY_EVENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_PROPERTY_EVENT_4), value);
	}

	inline static int32_t get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___DRAG_AND_DROP_MATERIAL_EVENT_5)); }
	inline FastAction_3_t58649212 * get_DRAG_AND_DROP_MATERIAL_EVENT_5() const { return ___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline FastAction_3_t58649212 ** get_address_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return &___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline void set_DRAG_AND_DROP_MATERIAL_EVENT_5(FastAction_3_t58649212 * value)
	{
		___DRAG_AND_DROP_MATERIAL_EVENT_5 = value;
		Il2CppCodeGenWriteBarrier((&___DRAG_AND_DROP_MATERIAL_EVENT_5), value);
	}

	inline static int32_t get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXT_STYLE_PROPERTY_EVENT_6)); }
	inline FastAction_1_t3758825850 * get_TEXT_STYLE_PROPERTY_EVENT_6() const { return ___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline FastAction_1_t3758825850 ** get_address_of_TEXT_STYLE_PROPERTY_EVENT_6() { return &___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline void set_TEXT_STYLE_PROPERTY_EVENT_6(FastAction_1_t3758825850 * value)
	{
		___TEXT_STYLE_PROPERTY_EVENT_6 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_STYLE_PROPERTY_EVENT_6), value);
	}

	inline static int32_t get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___COLOR_GRADIENT_PROPERTY_EVENT_7)); }
	inline FastAction_1_t3044626357 * get_COLOR_GRADIENT_PROPERTY_EVENT_7() const { return ___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline FastAction_1_t3044626357 ** get_address_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return &___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline void set_COLOR_GRADIENT_PROPERTY_EVENT_7(FastAction_1_t3044626357 * value)
	{
		___COLOR_GRADIENT_PROPERTY_EVENT_7 = value;
		Il2CppCodeGenWriteBarrier((&___COLOR_GRADIENT_PROPERTY_EVENT_7), value);
	}

	inline static int32_t get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TMP_SETTINGS_PROPERTY_EVENT_8)); }
	inline FastAction_t3491443480 * get_TMP_SETTINGS_PROPERTY_EVENT_8() const { return ___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline FastAction_t3491443480 ** get_address_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return &___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline void set_TMP_SETTINGS_PROPERTY_EVENT_8(FastAction_t3491443480 * value)
	{
		___TMP_SETTINGS_PROPERTY_EVENT_8 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_SETTINGS_PROPERTY_EVENT_8), value);
	}

	inline static int32_t get_offset_of_RESOURCE_LOAD_EVENT_9() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___RESOURCE_LOAD_EVENT_9)); }
	inline FastAction_t3491443480 * get_RESOURCE_LOAD_EVENT_9() const { return ___RESOURCE_LOAD_EVENT_9; }
	inline FastAction_t3491443480 ** get_address_of_RESOURCE_LOAD_EVENT_9() { return &___RESOURCE_LOAD_EVENT_9; }
	inline void set_RESOURCE_LOAD_EVENT_9(FastAction_t3491443480 * value)
	{
		___RESOURCE_LOAD_EVENT_9 = value;
		Il2CppCodeGenWriteBarrier((&___RESOURCE_LOAD_EVENT_9), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10)); }
	inline FastAction_2_t2395899227 * get_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() const { return ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline FastAction_2_t2395899227 ** get_address_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return &___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline void set_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(FastAction_2_t2395899227 * value)
	{
		___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10), value);
	}

	inline static int32_t get_offset_of_OnPreRenderObject_Event_11() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___OnPreRenderObject_Event_11)); }
	inline FastAction_t3491443480 * get_OnPreRenderObject_Event_11() const { return ___OnPreRenderObject_Event_11; }
	inline FastAction_t3491443480 ** get_address_of_OnPreRenderObject_Event_11() { return &___OnPreRenderObject_Event_11; }
	inline void set_OnPreRenderObject_Event_11(FastAction_t3491443480 * value)
	{
		___OnPreRenderObject_Event_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnPreRenderObject_Event_11), value);
	}

	inline static int32_t get_offset_of_TEXT_CHANGED_EVENT_12() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t712497257_StaticFields, ___TEXT_CHANGED_EVENT_12)); }
	inline FastAction_1_t4292545838 * get_TEXT_CHANGED_EVENT_12() const { return ___TEXT_CHANGED_EVENT_12; }
	inline FastAction_1_t4292545838 ** get_address_of_TEXT_CHANGED_EVENT_12() { return &___TEXT_CHANGED_EVENT_12; }
	inline void set_TEXT_CHANGED_EVENT_12(FastAction_1_t4292545838 * value)
	{
		___TEXT_CHANGED_EVENT_12 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_CHANGED_EVENT_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EVENTMANAGER_T712497257_H
#ifndef TMPRO_EXTENSIONMETHODS_T1453208966_H
#define TMPRO_EXTENSIONMETHODS_T1453208966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_ExtensionMethods
struct  TMPro_ExtensionMethods_t1453208966  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EXTENSIONMETHODS_T1453208966_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef U24ARRAYTYPEU3D40_T2865632059_H
#define U24ARRAYTYPEU3D40_T2865632059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_t2865632059 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_t2865632059__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_T2865632059_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef FONTASSETCREATIONSETTINGS_T359369028_H
#define FONTASSETCREATIONSETTINGS_T359369028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontAssetCreationSettings
struct  FontAssetCreationSettings_t359369028 
{
public:
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileName
	String_t* ___sourceFontFileName_0;
	// System.String TMPro.FontAssetCreationSettings::sourceFontFileGUID
	String_t* ___sourceFontFileGUID_1;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSizeSamplingMode
	int32_t ___pointSizeSamplingMode_2;
	// System.Int32 TMPro.FontAssetCreationSettings::pointSize
	int32_t ___pointSize_3;
	// System.Int32 TMPro.FontAssetCreationSettings::padding
	int32_t ___padding_4;
	// System.Int32 TMPro.FontAssetCreationSettings::packingMode
	int32_t ___packingMode_5;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasWidth
	int32_t ___atlasWidth_6;
	// System.Int32 TMPro.FontAssetCreationSettings::atlasHeight
	int32_t ___atlasHeight_7;
	// System.Int32 TMPro.FontAssetCreationSettings::characterSetSelectionMode
	int32_t ___characterSetSelectionMode_8;
	// System.String TMPro.FontAssetCreationSettings::characterSequence
	String_t* ___characterSequence_9;
	// System.String TMPro.FontAssetCreationSettings::referencedFontAssetGUID
	String_t* ___referencedFontAssetGUID_10;
	// System.String TMPro.FontAssetCreationSettings::referencedTextAssetGUID
	String_t* ___referencedTextAssetGUID_11;
	// System.Int32 TMPro.FontAssetCreationSettings::fontStyle
	int32_t ___fontStyle_12;
	// System.Single TMPro.FontAssetCreationSettings::fontStyleModifier
	float ___fontStyleModifier_13;
	// System.Int32 TMPro.FontAssetCreationSettings::renderMode
	int32_t ___renderMode_14;
	// System.Boolean TMPro.FontAssetCreationSettings::includeFontFeatures
	bool ___includeFontFeatures_15;

public:
	inline static int32_t get_offset_of_sourceFontFileName_0() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___sourceFontFileName_0)); }
	inline String_t* get_sourceFontFileName_0() const { return ___sourceFontFileName_0; }
	inline String_t** get_address_of_sourceFontFileName_0() { return &___sourceFontFileName_0; }
	inline void set_sourceFontFileName_0(String_t* value)
	{
		___sourceFontFileName_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileName_0), value);
	}

	inline static int32_t get_offset_of_sourceFontFileGUID_1() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___sourceFontFileGUID_1)); }
	inline String_t* get_sourceFontFileGUID_1() const { return ___sourceFontFileGUID_1; }
	inline String_t** get_address_of_sourceFontFileGUID_1() { return &___sourceFontFileGUID_1; }
	inline void set_sourceFontFileGUID_1(String_t* value)
	{
		___sourceFontFileGUID_1 = value;
		Il2CppCodeGenWriteBarrier((&___sourceFontFileGUID_1), value);
	}

	inline static int32_t get_offset_of_pointSizeSamplingMode_2() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___pointSizeSamplingMode_2)); }
	inline int32_t get_pointSizeSamplingMode_2() const { return ___pointSizeSamplingMode_2; }
	inline int32_t* get_address_of_pointSizeSamplingMode_2() { return &___pointSizeSamplingMode_2; }
	inline void set_pointSizeSamplingMode_2(int32_t value)
	{
		___pointSizeSamplingMode_2 = value;
	}

	inline static int32_t get_offset_of_pointSize_3() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___pointSize_3)); }
	inline int32_t get_pointSize_3() const { return ___pointSize_3; }
	inline int32_t* get_address_of_pointSize_3() { return &___pointSize_3; }
	inline void set_pointSize_3(int32_t value)
	{
		___pointSize_3 = value;
	}

	inline static int32_t get_offset_of_padding_4() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___padding_4)); }
	inline int32_t get_padding_4() const { return ___padding_4; }
	inline int32_t* get_address_of_padding_4() { return &___padding_4; }
	inline void set_padding_4(int32_t value)
	{
		___padding_4 = value;
	}

	inline static int32_t get_offset_of_packingMode_5() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___packingMode_5)); }
	inline int32_t get_packingMode_5() const { return ___packingMode_5; }
	inline int32_t* get_address_of_packingMode_5() { return &___packingMode_5; }
	inline void set_packingMode_5(int32_t value)
	{
		___packingMode_5 = value;
	}

	inline static int32_t get_offset_of_atlasWidth_6() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___atlasWidth_6)); }
	inline int32_t get_atlasWidth_6() const { return ___atlasWidth_6; }
	inline int32_t* get_address_of_atlasWidth_6() { return &___atlasWidth_6; }
	inline void set_atlasWidth_6(int32_t value)
	{
		___atlasWidth_6 = value;
	}

	inline static int32_t get_offset_of_atlasHeight_7() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___atlasHeight_7)); }
	inline int32_t get_atlasHeight_7() const { return ___atlasHeight_7; }
	inline int32_t* get_address_of_atlasHeight_7() { return &___atlasHeight_7; }
	inline void set_atlasHeight_7(int32_t value)
	{
		___atlasHeight_7 = value;
	}

	inline static int32_t get_offset_of_characterSetSelectionMode_8() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___characterSetSelectionMode_8)); }
	inline int32_t get_characterSetSelectionMode_8() const { return ___characterSetSelectionMode_8; }
	inline int32_t* get_address_of_characterSetSelectionMode_8() { return &___characterSetSelectionMode_8; }
	inline void set_characterSetSelectionMode_8(int32_t value)
	{
		___characterSetSelectionMode_8 = value;
	}

	inline static int32_t get_offset_of_characterSequence_9() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___characterSequence_9)); }
	inline String_t* get_characterSequence_9() const { return ___characterSequence_9; }
	inline String_t** get_address_of_characterSequence_9() { return &___characterSequence_9; }
	inline void set_characterSequence_9(String_t* value)
	{
		___characterSequence_9 = value;
		Il2CppCodeGenWriteBarrier((&___characterSequence_9), value);
	}

	inline static int32_t get_offset_of_referencedFontAssetGUID_10() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___referencedFontAssetGUID_10)); }
	inline String_t* get_referencedFontAssetGUID_10() const { return ___referencedFontAssetGUID_10; }
	inline String_t** get_address_of_referencedFontAssetGUID_10() { return &___referencedFontAssetGUID_10; }
	inline void set_referencedFontAssetGUID_10(String_t* value)
	{
		___referencedFontAssetGUID_10 = value;
		Il2CppCodeGenWriteBarrier((&___referencedFontAssetGUID_10), value);
	}

	inline static int32_t get_offset_of_referencedTextAssetGUID_11() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___referencedTextAssetGUID_11)); }
	inline String_t* get_referencedTextAssetGUID_11() const { return ___referencedTextAssetGUID_11; }
	inline String_t** get_address_of_referencedTextAssetGUID_11() { return &___referencedTextAssetGUID_11; }
	inline void set_referencedTextAssetGUID_11(String_t* value)
	{
		___referencedTextAssetGUID_11 = value;
		Il2CppCodeGenWriteBarrier((&___referencedTextAssetGUID_11), value);
	}

	inline static int32_t get_offset_of_fontStyle_12() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___fontStyle_12)); }
	inline int32_t get_fontStyle_12() const { return ___fontStyle_12; }
	inline int32_t* get_address_of_fontStyle_12() { return &___fontStyle_12; }
	inline void set_fontStyle_12(int32_t value)
	{
		___fontStyle_12 = value;
	}

	inline static int32_t get_offset_of_fontStyleModifier_13() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___fontStyleModifier_13)); }
	inline float get_fontStyleModifier_13() const { return ___fontStyleModifier_13; }
	inline float* get_address_of_fontStyleModifier_13() { return &___fontStyleModifier_13; }
	inline void set_fontStyleModifier_13(float value)
	{
		___fontStyleModifier_13 = value;
	}

	inline static int32_t get_offset_of_renderMode_14() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___renderMode_14)); }
	inline int32_t get_renderMode_14() const { return ___renderMode_14; }
	inline int32_t* get_address_of_renderMode_14() { return &___renderMode_14; }
	inline void set_renderMode_14(int32_t value)
	{
		___renderMode_14 = value;
	}

	inline static int32_t get_offset_of_includeFontFeatures_15() { return static_cast<int32_t>(offsetof(FontAssetCreationSettings_t359369028, ___includeFontFeatures_15)); }
	inline bool get_includeFontFeatures_15() const { return ___includeFontFeatures_15; }
	inline bool* get_address_of_includeFontFeatures_15() { return &___includeFontFeatures_15; }
	inline void set_includeFontFeatures_15(bool value)
	{
		___includeFontFeatures_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_t359369028_marshaled_pinvoke
{
	char* ___sourceFontFileName_0;
	char* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	char* ___characterSequence_9;
	char* ___referencedFontAssetGUID_10;
	char* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
// Native definition for COM marshalling of TMPro.FontAssetCreationSettings
struct FontAssetCreationSettings_t359369028_marshaled_com
{
	Il2CppChar* ___sourceFontFileName_0;
	Il2CppChar* ___sourceFontFileGUID_1;
	int32_t ___pointSizeSamplingMode_2;
	int32_t ___pointSize_3;
	int32_t ___padding_4;
	int32_t ___packingMode_5;
	int32_t ___atlasWidth_6;
	int32_t ___atlasHeight_7;
	int32_t ___characterSetSelectionMode_8;
	Il2CppChar* ___characterSequence_9;
	Il2CppChar* ___referencedFontAssetGUID_10;
	Il2CppChar* ___referencedTextAssetGUID_11;
	int32_t ___fontStyle_12;
	float ___fontStyleModifier_13;
	int32_t ___renderMode_14;
	int32_t ___includeFontFeatures_15;
};
#endif // FONTASSETCREATIONSETTINGS_T359369028_H
#ifndef GLYPHVALUERECORD_T4065874512_H
#define GLYPHVALUERECORD_T4065874512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.GlyphValueRecord
struct  GlyphValueRecord_t4065874512 
{
public:
	// System.Single TMPro.GlyphValueRecord::xPlacement
	float ___xPlacement_0;
	// System.Single TMPro.GlyphValueRecord::yPlacement
	float ___yPlacement_1;
	// System.Single TMPro.GlyphValueRecord::xAdvance
	float ___xAdvance_2;
	// System.Single TMPro.GlyphValueRecord::yAdvance
	float ___yAdvance_3;

public:
	inline static int32_t get_offset_of_xPlacement_0() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___xPlacement_0)); }
	inline float get_xPlacement_0() const { return ___xPlacement_0; }
	inline float* get_address_of_xPlacement_0() { return &___xPlacement_0; }
	inline void set_xPlacement_0(float value)
	{
		___xPlacement_0 = value;
	}

	inline static int32_t get_offset_of_yPlacement_1() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___yPlacement_1)); }
	inline float get_yPlacement_1() const { return ___yPlacement_1; }
	inline float* get_address_of_yPlacement_1() { return &___yPlacement_1; }
	inline void set_yPlacement_1(float value)
	{
		___yPlacement_1 = value;
	}

	inline static int32_t get_offset_of_xAdvance_2() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___xAdvance_2)); }
	inline float get_xAdvance_2() const { return ___xAdvance_2; }
	inline float* get_address_of_xAdvance_2() { return &___xAdvance_2; }
	inline void set_xAdvance_2(float value)
	{
		___xAdvance_2 = value;
	}

	inline static int32_t get_offset_of_yAdvance_3() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t4065874512, ___yAdvance_3)); }
	inline float get_yAdvance_3() const { return ___yAdvance_3; }
	inline float* get_address_of_yAdvance_3() { return &___yAdvance_3; }
	inline void set_yAdvance_3(float value)
	{
		___yAdvance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHVALUERECORD_T4065874512_H
#ifndef KERNINGPAIRKEY_T536493877_H
#define KERNINGPAIRKEY_T536493877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPairKey
struct  KerningPairKey_t536493877 
{
public:
	// System.UInt32 TMPro.KerningPairKey::ascii_Left
	uint32_t ___ascii_Left_0;
	// System.UInt32 TMPro.KerningPairKey::ascii_Right
	uint32_t ___ascii_Right_1;
	// System.UInt32 TMPro.KerningPairKey::key
	uint32_t ___key_2;

public:
	inline static int32_t get_offset_of_ascii_Left_0() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Left_0)); }
	inline uint32_t get_ascii_Left_0() const { return ___ascii_Left_0; }
	inline uint32_t* get_address_of_ascii_Left_0() { return &___ascii_Left_0; }
	inline void set_ascii_Left_0(uint32_t value)
	{
		___ascii_Left_0 = value;
	}

	inline static int32_t get_offset_of_ascii_Right_1() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___ascii_Right_1)); }
	inline uint32_t get_ascii_Right_1() const { return ___ascii_Right_1; }
	inline uint32_t* get_address_of_ascii_Right_1() { return &___ascii_Right_1; }
	inline void set_ascii_Right_1(uint32_t value)
	{
		___ascii_Right_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KerningPairKey_t536493877, ___key_2)); }
	inline uint32_t get_key_2() const { return ___key_2; }
	inline uint32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(uint32_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIRKEY_T536493877_H
#ifndef MATERIALREFERENCE_T1952344632_H
#define MATERIALREFERENCE_T1952344632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t1952344632 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t340375123 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t340375123 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fontAsset_1)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___material_3)); }
	inline Material_t340375123 * get_material_3() const { return ___material_3; }
	inline Material_t340375123 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t340375123 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___fallbackMaterial_6)); }
	inline Material_t340375123 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t340375123 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t340375123 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t1952344632, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t1952344632_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t364381626 * ___fontAsset_1;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_2;
	Material_t340375123 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t340375123 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T1952344632_H
#ifndef TMP_BASICXMLTAGSTACK_T2962628096_H
#define TMP_BASICXMLTAGSTACK_T2962628096_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t2962628096 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t2962628096, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T2962628096_H
#ifndef TMP_GLYPH_T581847833_H
#define TMP_GLYPH_T581847833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Glyph
struct  TMP_Glyph_t581847833  : public TMP_TextElement_t129727469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPH_T581847833_H
#ifndef TMP_LINKINFO_T1092083476_H
#define TMP_LINKINFO_T1092083476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t1092083476 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t3528271667* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t1092083476, ___linkID_6)); }
	inline CharU5BU5D_t3528271667* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t3528271667** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t3528271667* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t1092083476_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T1092083476_H
#ifndef TMP_PAGEINFO_T2608430633_H
#define TMP_PAGEINFO_T2608430633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t2608430633 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t2608430633, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T2608430633_H
#ifndef TMP_SPRITEINFO_T2726321384_H
#define TMP_SPRITEINFO_T2726321384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t2726321384 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t2726321384, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T2726321384_H
#ifndef TMP_WORDINFO_T3331066303_H
#define TMP_WORDINFO_T3331066303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t3331066303 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___textComponent_0)); }
	inline TMP_Text_t2599618874 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t2599618874 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3331066303, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_pinvoke
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3331066303_marshaled_com
{
	TMP_Text_t2599618874 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T3331066303_H
#ifndef TMP_XMLTAGSTACK_1_T2514600297_H
#define TMP_XMLTAGSTACK_1_T2514600297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2514600297 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t385246372* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___itemStack_0)); }
	inline Int32U5BU5D_t385246372* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t385246372** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t385246372* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2514600297, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2514600297_H
#ifndef TMP_XMLTAGSTACK_1_T960921318_H
#define TMP_XMLTAGSTACK_1_T960921318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t960921318 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t1444911251* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___itemStack_0)); }
	inline SingleU5BU5D_t1444911251* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t1444911251* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t960921318, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T960921318_H
#ifndef TMP_XMLTAGSTACK_1_T3241710312_H
#define TMP_XMLTAGSTACK_1_T3241710312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t3241710312 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2496920137* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_t3678055768 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t2496920137* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t2496920137** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t2496920137* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3241710312, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_t3678055768 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_t3678055768 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3241710312_H
#ifndef TAGATTRIBUTE_T688278634_H
#define TAGATTRIBUTE_T688278634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t688278634 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t688278634, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T688278634_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef UNITYEVENT_1_T978947469_H
#define UNITYEVENT_1_T978947469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t978947469  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t978947469, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T978947469_H
#ifndef UNITYEVENT_1_T2278926278_H
#define UNITYEVENT_1_T2278926278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2278926278  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2278926278, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2278926278_H
#ifndef UNITYEVENT_1_T1995296123_H
#define UNITYEVENT_1_T1995296123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.GameObject>
struct  UnityEvent_1_t1995296123  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1995296123, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1995296123_H
#ifndef UNITYEVENT_1_T187058129_H
#define UNITYEVENT_1_T187058129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Transform>
struct  UnityEvent_1_t187058129  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t187058129, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T187058129_H
#ifndef UNITYEVENT_1_T3037889027_H
#define UNITYEVENT_1_T3037889027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t3037889027  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3037889027, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3037889027_H
#ifndef UNITYEVENT_1_T309005672_H
#define UNITYEVENT_1_T309005672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector3>
struct  UnityEvent_1_t309005672  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t309005672, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T309005672_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-9E6378168821DBABB7EE3D0154346480FAC8AEF1
	U24ArrayTypeU3D40_t2865632059  ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline U24ArrayTypeU3D40_t2865632059  get_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline U24ArrayTypeU3D40_t2865632059 * get_address_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(U24ArrayTypeU3D40_t2865632059  value)
	{
		___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef EMULATEDPLATFORMTYPE_T2164579554_H
#define EMULATEDPLATFORMTYPE_T2164579554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.Demos.DemoInputManager/EmulatedPlatformType
struct  EmulatedPlatformType_t2164579554 
{
public:
	// System.Int32 GoogleVR.Demos.DemoInputManager/EmulatedPlatformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EmulatedPlatformType_t2164579554, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATEDPLATFORMTYPE_T2164579554_H
#ifndef BOOLEVENT_T738798084_H
#define BOOLEVENT_T738798084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.BoolEvent
struct  BoolEvent_t738798084  : public UnityEvent_1_t978947469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEVENT_T738798084_H
#ifndef BUTTONEVENT_T4044041544_H
#define BUTTONEVENT_T4044041544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.ButtonEvent
struct  ButtonEvent_t4044041544  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTONEVENT_T4044041544_H
#ifndef FLOATEVENT_T620910342_H
#define FLOATEVENT_T620910342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.FloatEvent
struct  FloatEvent_t620910342  : public UnityEvent_1_t2278926278
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATEVENT_T620910342_H
#ifndef GAMEOBJECTEVENT_T2012864557_H
#define GAMEOBJECTEVENT_T2012864557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.GameObjectEvent
struct  GameObjectEvent_t2012864557  : public UnityEvent_1_t1995296123
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEVENT_T2012864557_H
#ifndef TOUCHPADEVENT_T2738674555_H
#define TOUCHPADEVENT_T2738674555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.TouchPadEvent
struct  TouchPadEvent_t2738674555  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPADEVENT_T2738674555_H
#ifndef TRANSFORMEVENT_T114658240_H
#define TRANSFORMEVENT_T114658240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.TransformEvent
struct  TransformEvent_t114658240  : public UnityEvent_1_t187058129
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEVENT_T114658240_H
#ifndef VECTOR2EVENT_T2715870356_H
#define VECTOR2EVENT_T2715870356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.Vector2Event
struct  Vector2Event_t2715870356  : public UnityEvent_1_t3037889027
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2EVENT_T2715870356_H
#ifndef VECTOR3EVENT_T2701124756_H
#define VECTOR3EVENT_T2701124756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.Vector3Event
struct  Vector3Event_t2701124756  : public UnityEvent_1_t309005672
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3EVENT_T2701124756_H
#ifndef QUALITY_T1508087645_H
#define QUALITY_T1508087645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio/Quality
struct  Quality_t1508087645 
{
public:
	// System.Int32 GvrAudio/Quality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Quality_t1508087645, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITY_T1508087645_H
#ifndef SPATIALIZERDATA_T1601606274_H
#define SPATIALIZERDATA_T1601606274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio/SpatializerData
struct  SpatializerData_t1601606274 
{
public:
	// System.Int32 GvrAudio/SpatializerData::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpatializerData_t1601606274, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALIZERDATA_T1601606274_H
#ifndef SPATIALIZERTYPE_T42254374_H
#define SPATIALIZERTYPE_T42254374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio/SpatializerType
struct  SpatializerType_t42254374 
{
public:
	// System.Int32 GvrAudio/SpatializerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpatializerType_t42254374, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPATIALIZERTYPE_T42254374_H
#ifndef SURFACEMATERIAL_T4254821637_H
#define SURFACEMATERIAL_T4254821637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioRoom/SurfaceMaterial
struct  SurfaceMaterial_t4254821637 
{
public:
	// System.Int32 GvrAudioRoom/SurfaceMaterial::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SurfaceMaterial_t4254821637, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SURFACEMATERIAL_T4254821637_H
#ifndef CARETPOSITION_T3997512201_H
#define CARETPOSITION_T3997512201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretPosition
struct  CaretPosition_t3997512201 
{
public:
	// System.Int32 TMPro.CaretPosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaretPosition_t3997512201, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETPOSITION_T3997512201_H
#ifndef COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#define COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DistanceTransform_EventTypes
struct  Compute_DistanceTransform_EventTypes_t2554612104 
{
public:
	// System.Int32 TMPro.Compute_DistanceTransform_EventTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Compute_DistanceTransform_EventTypes_t2554612104, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T2554612104_H
#ifndef EXTENTS_T3837212874_H
#define EXTENTS_T3837212874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3837212874 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3837212874, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3837212874_H
#ifndef FONTSTYLES_T3828945032_H
#define FONTSTYLES_T3828945032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3828945032 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3828945032, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3828945032_H
#ifndef FONTWEIGHTS_T3122883458_H
#define FONTWEIGHTS_T3122883458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeights
struct  FontWeights_t3122883458 
{
public:
	// System.Int32 TMPro.FontWeights::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontWeights_t3122883458, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHTS_T3122883458_H
#ifndef KERNINGPAIR_T2270855589_H
#define KERNINGPAIR_T2270855589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPair
struct  KerningPair_t2270855589  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningPair::m_FirstGlyph
	uint32_t ___m_FirstGlyph_0;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_FirstGlyphAdjustments
	GlyphValueRecord_t4065874512  ___m_FirstGlyphAdjustments_1;
	// System.UInt32 TMPro.KerningPair::m_SecondGlyph
	uint32_t ___m_SecondGlyph_2;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_SecondGlyphAdjustments
	GlyphValueRecord_t4065874512  ___m_SecondGlyphAdjustments_3;
	// System.Single TMPro.KerningPair::xOffset
	float ___xOffset_4;

public:
	inline static int32_t get_offset_of_m_FirstGlyph_0() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_FirstGlyph_0)); }
	inline uint32_t get_m_FirstGlyph_0() const { return ___m_FirstGlyph_0; }
	inline uint32_t* get_address_of_m_FirstGlyph_0() { return &___m_FirstGlyph_0; }
	inline void set_m_FirstGlyph_0(uint32_t value)
	{
		___m_FirstGlyph_0 = value;
	}

	inline static int32_t get_offset_of_m_FirstGlyphAdjustments_1() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_FirstGlyphAdjustments_1)); }
	inline GlyphValueRecord_t4065874512  get_m_FirstGlyphAdjustments_1() const { return ___m_FirstGlyphAdjustments_1; }
	inline GlyphValueRecord_t4065874512 * get_address_of_m_FirstGlyphAdjustments_1() { return &___m_FirstGlyphAdjustments_1; }
	inline void set_m_FirstGlyphAdjustments_1(GlyphValueRecord_t4065874512  value)
	{
		___m_FirstGlyphAdjustments_1 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyph_2() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_SecondGlyph_2)); }
	inline uint32_t get_m_SecondGlyph_2() const { return ___m_SecondGlyph_2; }
	inline uint32_t* get_address_of_m_SecondGlyph_2() { return &___m_SecondGlyph_2; }
	inline void set_m_SecondGlyph_2(uint32_t value)
	{
		___m_SecondGlyph_2 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyphAdjustments_3() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___m_SecondGlyphAdjustments_3)); }
	inline GlyphValueRecord_t4065874512  get_m_SecondGlyphAdjustments_3() const { return ___m_SecondGlyphAdjustments_3; }
	inline GlyphValueRecord_t4065874512 * get_address_of_m_SecondGlyphAdjustments_3() { return &___m_SecondGlyphAdjustments_3; }
	inline void set_m_SecondGlyphAdjustments_3(GlyphValueRecord_t4065874512  value)
	{
		___m_SecondGlyphAdjustments_3 = value;
	}

	inline static int32_t get_offset_of_xOffset_4() { return static_cast<int32_t>(offsetof(KerningPair_t2270855589, ___xOffset_4)); }
	inline float get_xOffset_4() const { return ___xOffset_4; }
	inline float* get_address_of_xOffset_4() { return &___xOffset_4; }
	inline void set_xOffset_4(float value)
	{
		___xOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIR_T2270855589_H
#ifndef MASKINGOFFSETMODE_T2266644590_H
#define MASKINGOFFSETMODE_T2266644590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingOffsetMode
struct  MaskingOffsetMode_t2266644590 
{
public:
	// System.Int32 TMPro.MaskingOffsetMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingOffsetMode_t2266644590, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGOFFSETMODE_T2266644590_H
#ifndef MASKINGTYPES_T3687969768_H
#define MASKINGTYPES_T3687969768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t3687969768 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingTypes_t3687969768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T3687969768_H
#ifndef MESH_EXTENTS_T3388355125_H
#define MESH_EXTENTS_T3388355125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t3388355125 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_t2156229523  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_t2156229523  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___min_0)); }
	inline Vector2_t2156229523  get_min_0() const { return ___min_0; }
	inline Vector2_t2156229523 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2156229523  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3388355125, ___max_1)); }
	inline Vector2_t2156229523  get_max_1() const { return ___max_1; }
	inline Vector2_t2156229523 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2156229523  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T3388355125_H
#ifndef TMP_MATH_T624304809_H
#define TMP_MATH_T624304809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Math
struct  TMP_Math_t624304809  : public RuntimeObject
{
public:

public:
};

struct TMP_Math_t624304809_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_Math::MAX_16BIT
	Vector2_t2156229523  ___MAX_16BIT_6;
	// UnityEngine.Vector2 TMPro.TMP_Math::MIN_16BIT
	Vector2_t2156229523  ___MIN_16BIT_7;

public:
	inline static int32_t get_offset_of_MAX_16BIT_6() { return static_cast<int32_t>(offsetof(TMP_Math_t624304809_StaticFields, ___MAX_16BIT_6)); }
	inline Vector2_t2156229523  get_MAX_16BIT_6() const { return ___MAX_16BIT_6; }
	inline Vector2_t2156229523 * get_address_of_MAX_16BIT_6() { return &___MAX_16BIT_6; }
	inline void set_MAX_16BIT_6(Vector2_t2156229523  value)
	{
		___MAX_16BIT_6 = value;
	}

	inline static int32_t get_offset_of_MIN_16BIT_7() { return static_cast<int32_t>(offsetof(TMP_Math_t624304809_StaticFields, ___MIN_16BIT_7)); }
	inline Vector2_t2156229523  get_MIN_16BIT_7() const { return ___MIN_16BIT_7; }
	inline Vector2_t2156229523 * get_address_of_MIN_16BIT_7() { return &___MIN_16BIT_7; }
	inline void set_MIN_16BIT_7(Vector2_t2156229523  value)
	{
		___MIN_16BIT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATH_T624304809_H
#ifndef TEXTINPUTSOURCES_T1522115805_H
#define TEXTINPUTSOURCES_T1522115805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text/TextInputSources
struct  TextInputSources_t1522115805 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextInputSources_t1522115805, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T1522115805_H
#ifndef TMP_TEXTELEMENTTYPE_T1276645592_H
#define TMP_TEXTELEMENTTYPE_T1276645592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1276645592 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1276645592, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1276645592_H
#ifndef TMP_TEXTINFO_T3598145122_H
#define TMP_TEXTINFO_T3598145122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_t3598145122  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t2599618874 * ___textComponent_2;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_4;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_5;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_6;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_7;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_8;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_9;
	// System.Int32 TMPro.TMP_TextInfo::materialCount
	int32_t ___materialCount_10;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___characterInfo_11;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t3766301798* ___wordInfo_12;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t3558768157* ___linkInfo_13;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t4120149533* ___lineInfo_14;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_t2463031060* ___pageInfo_15;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___meshInfo_16;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::m_CachedMeshInfo
	TMP_MeshInfoU5BU5D_t3365986247* ___m_CachedMeshInfo_17;

public:
	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___textComponent_2)); }
	inline TMP_Text_t2599618874 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t2599618874 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t2599618874 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_2), value);
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}

	inline static int32_t get_offset_of_spriteCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spriteCount_4)); }
	inline int32_t get_spriteCount_4() const { return ___spriteCount_4; }
	inline int32_t* get_address_of_spriteCount_4() { return &___spriteCount_4; }
	inline void set_spriteCount_4(int32_t value)
	{
		___spriteCount_4 = value;
	}

	inline static int32_t get_offset_of_spaceCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___spaceCount_5)); }
	inline int32_t get_spaceCount_5() const { return ___spaceCount_5; }
	inline int32_t* get_address_of_spaceCount_5() { return &___spaceCount_5; }
	inline void set_spaceCount_5(int32_t value)
	{
		___spaceCount_5 = value;
	}

	inline static int32_t get_offset_of_wordCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordCount_6)); }
	inline int32_t get_wordCount_6() const { return ___wordCount_6; }
	inline int32_t* get_address_of_wordCount_6() { return &___wordCount_6; }
	inline void set_wordCount_6(int32_t value)
	{
		___wordCount_6 = value;
	}

	inline static int32_t get_offset_of_linkCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkCount_7)); }
	inline int32_t get_linkCount_7() const { return ___linkCount_7; }
	inline int32_t* get_address_of_linkCount_7() { return &___linkCount_7; }
	inline void set_linkCount_7(int32_t value)
	{
		___linkCount_7 = value;
	}

	inline static int32_t get_offset_of_lineCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineCount_8)); }
	inline int32_t get_lineCount_8() const { return ___lineCount_8; }
	inline int32_t* get_address_of_lineCount_8() { return &___lineCount_8; }
	inline void set_lineCount_8(int32_t value)
	{
		___lineCount_8 = value;
	}

	inline static int32_t get_offset_of_pageCount_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageCount_9)); }
	inline int32_t get_pageCount_9() const { return ___pageCount_9; }
	inline int32_t* get_address_of_pageCount_9() { return &___pageCount_9; }
	inline void set_pageCount_9(int32_t value)
	{
		___pageCount_9 = value;
	}

	inline static int32_t get_offset_of_materialCount_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___materialCount_10)); }
	inline int32_t get_materialCount_10() const { return ___materialCount_10; }
	inline int32_t* get_address_of_materialCount_10() { return &___materialCount_10; }
	inline void set_materialCount_10(int32_t value)
	{
		___materialCount_10 = value;
	}

	inline static int32_t get_offset_of_characterInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___characterInfo_11)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_characterInfo_11() const { return ___characterInfo_11; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_characterInfo_11() { return &___characterInfo_11; }
	inline void set_characterInfo_11(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___characterInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_11), value);
	}

	inline static int32_t get_offset_of_wordInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___wordInfo_12)); }
	inline TMP_WordInfoU5BU5D_t3766301798* get_wordInfo_12() const { return ___wordInfo_12; }
	inline TMP_WordInfoU5BU5D_t3766301798** get_address_of_wordInfo_12() { return &___wordInfo_12; }
	inline void set_wordInfo_12(TMP_WordInfoU5BU5D_t3766301798* value)
	{
		___wordInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___wordInfo_12), value);
	}

	inline static int32_t get_offset_of_linkInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___linkInfo_13)); }
	inline TMP_LinkInfoU5BU5D_t3558768157* get_linkInfo_13() const { return ___linkInfo_13; }
	inline TMP_LinkInfoU5BU5D_t3558768157** get_address_of_linkInfo_13() { return &___linkInfo_13; }
	inline void set_linkInfo_13(TMP_LinkInfoU5BU5D_t3558768157* value)
	{
		___linkInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___linkInfo_13), value);
	}

	inline static int32_t get_offset_of_lineInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___lineInfo_14)); }
	inline TMP_LineInfoU5BU5D_t4120149533* get_lineInfo_14() const { return ___lineInfo_14; }
	inline TMP_LineInfoU5BU5D_t4120149533** get_address_of_lineInfo_14() { return &___lineInfo_14; }
	inline void set_lineInfo_14(TMP_LineInfoU5BU5D_t4120149533* value)
	{
		___lineInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_14), value);
	}

	inline static int32_t get_offset_of_pageInfo_15() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___pageInfo_15)); }
	inline TMP_PageInfoU5BU5D_t2463031060* get_pageInfo_15() const { return ___pageInfo_15; }
	inline TMP_PageInfoU5BU5D_t2463031060** get_address_of_pageInfo_15() { return &___pageInfo_15; }
	inline void set_pageInfo_15(TMP_PageInfoU5BU5D_t2463031060* value)
	{
		___pageInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_15), value);
	}

	inline static int32_t get_offset_of_meshInfo_16() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___meshInfo_16)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_meshInfo_16() const { return ___meshInfo_16; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_meshInfo_16() { return &___meshInfo_16; }
	inline void set_meshInfo_16(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___meshInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshInfo_16), value);
	}

	inline static int32_t get_offset_of_m_CachedMeshInfo_17() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122, ___m_CachedMeshInfo_17)); }
	inline TMP_MeshInfoU5BU5D_t3365986247* get_m_CachedMeshInfo_17() const { return ___m_CachedMeshInfo_17; }
	inline TMP_MeshInfoU5BU5D_t3365986247** get_address_of_m_CachedMeshInfo_17() { return &___m_CachedMeshInfo_17; }
	inline void set_m_CachedMeshInfo_17(TMP_MeshInfoU5BU5D_t3365986247* value)
	{
		___m_CachedMeshInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedMeshInfo_17), value);
	}
};

struct TMP_TextInfo_t3598145122_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorPositive
	Vector2_t2156229523  ___k_InfinityVectorPositive_0;
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorNegative
	Vector2_t2156229523  ___k_InfinityVectorNegative_1;

public:
	inline static int32_t get_offset_of_k_InfinityVectorPositive_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorPositive_0)); }
	inline Vector2_t2156229523  get_k_InfinityVectorPositive_0() const { return ___k_InfinityVectorPositive_0; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorPositive_0() { return &___k_InfinityVectorPositive_0; }
	inline void set_k_InfinityVectorPositive_0(Vector2_t2156229523  value)
	{
		___k_InfinityVectorPositive_0 = value;
	}

	inline static int32_t get_offset_of_k_InfinityVectorNegative_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t3598145122_StaticFields, ___k_InfinityVectorNegative_1)); }
	inline Vector2_t2156229523  get_k_InfinityVectorNegative_1() const { return ___k_InfinityVectorNegative_1; }
	inline Vector2_t2156229523 * get_address_of_k_InfinityVectorNegative_1() { return &___k_InfinityVectorNegative_1; }
	inline void set_k_InfinityVectorNegative_1(Vector2_t2156229523  value)
	{
		___k_InfinityVectorNegative_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFO_T3598145122_H
#ifndef LINESEGMENT_T1526544958_H
#define LINESEGMENT_T1526544958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities/LineSegment
struct  LineSegment_t1526544958 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point1
	Vector3_t3722313464  ___Point1_0;
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point2
	Vector3_t3722313464  ___Point2_1;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point1_0)); }
	inline Vector3_t3722313464  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_t3722313464 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_t3722313464  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(LineSegment_t1526544958, ___Point2_1)); }
	inline Vector3_t3722313464  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_t3722313464 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_t3722313464  value)
	{
		___Point2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESEGMENT_T1526544958_H
#ifndef TMP_VERTEX_T2404176824_H
#define TMP_VERTEX_T2404176824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t2404176824 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t3722313464  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2156229523  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2156229523  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2156229523  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t2600501292  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___position_0)); }
	inline Vector3_t3722313464  get_position_0() const { return ___position_0; }
	inline Vector3_t3722313464 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t3722313464  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv_1)); }
	inline Vector2_t2156229523  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2156229523 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2156229523  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv2_2)); }
	inline Vector2_t2156229523  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2156229523 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2156229523  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___uv4_3)); }
	inline Vector2_t2156229523  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2156229523 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2156229523  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t2404176824, ___color_4)); }
	inline Color32_t2600501292  get_color_4() const { return ___color_4; }
	inline Color32_t2600501292 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t2600501292  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T2404176824_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#define TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_t388000256 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_t388000256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_T388000256_H
#ifndef TMP_XMLTAGSTACK_1_T1515999176_H
#define TMP_XMLTAGSTACK_1_T1515999176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t1515999176 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t648826345* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t1952344632  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t648826345* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t648826345* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1515999176, ___m_defaultItem_3)); }
	inline MaterialReference_t1952344632  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t1952344632 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t1952344632  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1515999176_H
#ifndef TMP_XMLTAGSTACK_1_T2164155836_H
#define TMP_XMLTAGSTACK_1_T2164155836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t2164155836 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t3850468773* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t2600501292  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___itemStack_0)); }
	inline Color32U5BU5D_t3850468773* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t3850468773** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t3850468773* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2164155836, ___m_defaultItem_3)); }
	inline Color32_t2600501292  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t2600501292 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t2600501292  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2164155836_H
#ifndef TAGTYPE_T123236451_H
#define TAGTYPE_T123236451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t123236451 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t123236451, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T123236451_H
#ifndef TAGUNITS_T1169424683_H
#define TAGUNITS_T1169424683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnits
struct  TagUnits_t1169424683 
{
public:
	// System.Int32 TMPro.TagUnits::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagUnits_t1169424683, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITS_T1169424683_H
#ifndef TEXTALIGNMENTOPTIONS_T4036791236_H
#define TEXTALIGNMENTOPTIONS_T4036791236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4036791236 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4036791236, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4036791236_H
#ifndef TEXTOVERFLOWMODES_T1430035314_H
#define TEXTOVERFLOWMODES_T1430035314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t1430035314 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextOverflowModes_t1430035314, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T1430035314_H
#ifndef TEXTRENDERFLAGS_T2418684345_H
#define TEXTRENDERFLAGS_T2418684345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t2418684345 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextRenderFlags_t2418684345, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T2418684345_H
#ifndef TEXTUREMAPPINGOPTIONS_T270963663_H
#define TEXTUREMAPPINGOPTIONS_T270963663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t270963663 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t270963663, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T270963663_H
#ifndef VERTEXGRADIENT_T345148380_H
#define VERTEXGRADIENT_T345148380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t345148380 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2555686324  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2555686324  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2555686324  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2555686324  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topLeft_0)); }
	inline Color_t2555686324  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2555686324 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2555686324  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___topRight_1)); }
	inline Color_t2555686324  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2555686324 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2555686324  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomLeft_2)); }
	inline Color_t2555686324  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2555686324 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2555686324  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t345148380, ___bottomRight_3)); }
	inline Color_t2555686324  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2555686324 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2555686324  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T345148380_H
#ifndef VERTEXSORTINGORDER_T2659893934_H
#define VERTEXSORTINGORDER_T2659893934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2659893934 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2659893934, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2659893934_H
#ifndef _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#define _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._HorizontalAlignmentOptions
struct  _HorizontalAlignmentOptions_t2379014378 
{
public:
	// System.Int32 TMPro._HorizontalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_HorizontalAlignmentOptions_t2379014378, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _HORIZONTALALIGNMENTOPTIONS_T2379014378_H
#ifndef _VERTICALALIGNMENTOPTIONS_T2825528260_H
#define _VERTICALALIGNMENTOPTIONS_T2825528260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._VerticalAlignmentOptions
struct  _VerticalAlignmentOptions_t2825528260 
{
public:
	// System.Int32 TMPro._VerticalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_VerticalAlignmentOptions_t2825528260, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _VERTICALALIGNMENTOPTIONS_T2825528260_H
#ifndef BOUNDS_T2266837910_H
#define BOUNDS_T2266837910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t2266837910 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t3722313464  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t3722313464  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Center_0)); }
	inline Vector3_t3722313464  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t3722313464 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t3722313464  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t2266837910, ___m_Extents_1)); }
	inline Vector3_t3722313464  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t3722313464 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t3722313464  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T2266837910_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef GVRAUDIO_T1993875383_H
#define GVRAUDIO_T1993875383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio
struct  GvrAudio_t1993875383  : public RuntimeObject
{
public:

public:
};

struct GvrAudio_t1993875383_StaticFields
{
public:
	// System.Int32 GvrAudio::sampleRate
	int32_t ___sampleRate_0;
	// System.Int32 GvrAudio::numChannels
	int32_t ___numChannels_1;
	// System.Int32 GvrAudio::framesPerBuffer
	int32_t ___framesPerBuffer_2;
	// UnityEngine.Color GvrAudio::listenerDirectivityColor
	Color_t2555686324  ___listenerDirectivityColor_3;
	// UnityEngine.Color GvrAudio::sourceDirectivityColor
	Color_t2555686324  ___sourceDirectivityColor_4;
	// UnityEngine.Bounds GvrAudio::bounds
	Bounds_t2266837910  ___bounds_17;
	// System.Collections.Generic.List`1<GvrAudioRoom> GvrAudio::enabledRooms
	List_1_t2506071902 * ___enabledRooms_18;
	// System.Boolean GvrAudio::initialized
	bool ___initialized_19;
	// UnityEngine.Transform GvrAudio::listenerTransform
	Transform_t3600365921 * ___listenerTransform_20;
	// UnityEngine.RaycastHit[] GvrAudio::occlusionHits
	RaycastHitU5BU5D_t1690781147* ___occlusionHits_21;
	// System.Int32 GvrAudio::occlusionMaskValue
	int32_t ___occlusionMaskValue_22;
	// UnityEngine.Matrix4x4 GvrAudio::transformMatrix
	Matrix4x4_t1817901843  ___transformMatrix_23;

public:
	inline static int32_t get_offset_of_sampleRate_0() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___sampleRate_0)); }
	inline int32_t get_sampleRate_0() const { return ___sampleRate_0; }
	inline int32_t* get_address_of_sampleRate_0() { return &___sampleRate_0; }
	inline void set_sampleRate_0(int32_t value)
	{
		___sampleRate_0 = value;
	}

	inline static int32_t get_offset_of_numChannels_1() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___numChannels_1)); }
	inline int32_t get_numChannels_1() const { return ___numChannels_1; }
	inline int32_t* get_address_of_numChannels_1() { return &___numChannels_1; }
	inline void set_numChannels_1(int32_t value)
	{
		___numChannels_1 = value;
	}

	inline static int32_t get_offset_of_framesPerBuffer_2() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___framesPerBuffer_2)); }
	inline int32_t get_framesPerBuffer_2() const { return ___framesPerBuffer_2; }
	inline int32_t* get_address_of_framesPerBuffer_2() { return &___framesPerBuffer_2; }
	inline void set_framesPerBuffer_2(int32_t value)
	{
		___framesPerBuffer_2 = value;
	}

	inline static int32_t get_offset_of_listenerDirectivityColor_3() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___listenerDirectivityColor_3)); }
	inline Color_t2555686324  get_listenerDirectivityColor_3() const { return ___listenerDirectivityColor_3; }
	inline Color_t2555686324 * get_address_of_listenerDirectivityColor_3() { return &___listenerDirectivityColor_3; }
	inline void set_listenerDirectivityColor_3(Color_t2555686324  value)
	{
		___listenerDirectivityColor_3 = value;
	}

	inline static int32_t get_offset_of_sourceDirectivityColor_4() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___sourceDirectivityColor_4)); }
	inline Color_t2555686324  get_sourceDirectivityColor_4() const { return ___sourceDirectivityColor_4; }
	inline Color_t2555686324 * get_address_of_sourceDirectivityColor_4() { return &___sourceDirectivityColor_4; }
	inline void set_sourceDirectivityColor_4(Color_t2555686324  value)
	{
		___sourceDirectivityColor_4 = value;
	}

	inline static int32_t get_offset_of_bounds_17() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___bounds_17)); }
	inline Bounds_t2266837910  get_bounds_17() const { return ___bounds_17; }
	inline Bounds_t2266837910 * get_address_of_bounds_17() { return &___bounds_17; }
	inline void set_bounds_17(Bounds_t2266837910  value)
	{
		___bounds_17 = value;
	}

	inline static int32_t get_offset_of_enabledRooms_18() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___enabledRooms_18)); }
	inline List_1_t2506071902 * get_enabledRooms_18() const { return ___enabledRooms_18; }
	inline List_1_t2506071902 ** get_address_of_enabledRooms_18() { return &___enabledRooms_18; }
	inline void set_enabledRooms_18(List_1_t2506071902 * value)
	{
		___enabledRooms_18 = value;
		Il2CppCodeGenWriteBarrier((&___enabledRooms_18), value);
	}

	inline static int32_t get_offset_of_initialized_19() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___initialized_19)); }
	inline bool get_initialized_19() const { return ___initialized_19; }
	inline bool* get_address_of_initialized_19() { return &___initialized_19; }
	inline void set_initialized_19(bool value)
	{
		___initialized_19 = value;
	}

	inline static int32_t get_offset_of_listenerTransform_20() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___listenerTransform_20)); }
	inline Transform_t3600365921 * get_listenerTransform_20() const { return ___listenerTransform_20; }
	inline Transform_t3600365921 ** get_address_of_listenerTransform_20() { return &___listenerTransform_20; }
	inline void set_listenerTransform_20(Transform_t3600365921 * value)
	{
		___listenerTransform_20 = value;
		Il2CppCodeGenWriteBarrier((&___listenerTransform_20), value);
	}

	inline static int32_t get_offset_of_occlusionHits_21() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___occlusionHits_21)); }
	inline RaycastHitU5BU5D_t1690781147* get_occlusionHits_21() const { return ___occlusionHits_21; }
	inline RaycastHitU5BU5D_t1690781147** get_address_of_occlusionHits_21() { return &___occlusionHits_21; }
	inline void set_occlusionHits_21(RaycastHitU5BU5D_t1690781147* value)
	{
		___occlusionHits_21 = value;
		Il2CppCodeGenWriteBarrier((&___occlusionHits_21), value);
	}

	inline static int32_t get_offset_of_occlusionMaskValue_22() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___occlusionMaskValue_22)); }
	inline int32_t get_occlusionMaskValue_22() const { return ___occlusionMaskValue_22; }
	inline int32_t* get_address_of_occlusionMaskValue_22() { return &___occlusionMaskValue_22; }
	inline void set_occlusionMaskValue_22(int32_t value)
	{
		___occlusionMaskValue_22 = value;
	}

	inline static int32_t get_offset_of_transformMatrix_23() { return static_cast<int32_t>(offsetof(GvrAudio_t1993875383_StaticFields, ___transformMatrix_23)); }
	inline Matrix4x4_t1817901843  get_transformMatrix_23() const { return ___transformMatrix_23; }
	inline Matrix4x4_t1817901843 * get_address_of_transformMatrix_23() { return &___transformMatrix_23; }
	inline void set_transformMatrix_23(Matrix4x4_t1817901843  value)
	{
		___transformMatrix_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIO_T1993875383_H
#ifndef ROOMPROPERTIES_T228572612_H
#define ROOMPROPERTIES_T228572612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudio/RoomProperties
struct  RoomProperties_t228572612 
{
public:
	// System.Single GvrAudio/RoomProperties::positionX
	float ___positionX_0;
	// System.Single GvrAudio/RoomProperties::positionY
	float ___positionY_1;
	// System.Single GvrAudio/RoomProperties::positionZ
	float ___positionZ_2;
	// System.Single GvrAudio/RoomProperties::rotationX
	float ___rotationX_3;
	// System.Single GvrAudio/RoomProperties::rotationY
	float ___rotationY_4;
	// System.Single GvrAudio/RoomProperties::rotationZ
	float ___rotationZ_5;
	// System.Single GvrAudio/RoomProperties::rotationW
	float ___rotationW_6;
	// System.Single GvrAudio/RoomProperties::dimensionsX
	float ___dimensionsX_7;
	// System.Single GvrAudio/RoomProperties::dimensionsY
	float ___dimensionsY_8;
	// System.Single GvrAudio/RoomProperties::dimensionsZ
	float ___dimensionsZ_9;
	// GvrAudioRoom/SurfaceMaterial GvrAudio/RoomProperties::materialLeft
	int32_t ___materialLeft_10;
	// GvrAudioRoom/SurfaceMaterial GvrAudio/RoomProperties::materialRight
	int32_t ___materialRight_11;
	// GvrAudioRoom/SurfaceMaterial GvrAudio/RoomProperties::materialBottom
	int32_t ___materialBottom_12;
	// GvrAudioRoom/SurfaceMaterial GvrAudio/RoomProperties::materialTop
	int32_t ___materialTop_13;
	// GvrAudioRoom/SurfaceMaterial GvrAudio/RoomProperties::materialFront
	int32_t ___materialFront_14;
	// GvrAudioRoom/SurfaceMaterial GvrAudio/RoomProperties::materialBack
	int32_t ___materialBack_15;
	// System.Single GvrAudio/RoomProperties::reflectionScalar
	float ___reflectionScalar_16;
	// System.Single GvrAudio/RoomProperties::reverbGain
	float ___reverbGain_17;
	// System.Single GvrAudio/RoomProperties::reverbTime
	float ___reverbTime_18;
	// System.Single GvrAudio/RoomProperties::reverbBrightness
	float ___reverbBrightness_19;

public:
	inline static int32_t get_offset_of_positionX_0() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___positionX_0)); }
	inline float get_positionX_0() const { return ___positionX_0; }
	inline float* get_address_of_positionX_0() { return &___positionX_0; }
	inline void set_positionX_0(float value)
	{
		___positionX_0 = value;
	}

	inline static int32_t get_offset_of_positionY_1() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___positionY_1)); }
	inline float get_positionY_1() const { return ___positionY_1; }
	inline float* get_address_of_positionY_1() { return &___positionY_1; }
	inline void set_positionY_1(float value)
	{
		___positionY_1 = value;
	}

	inline static int32_t get_offset_of_positionZ_2() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___positionZ_2)); }
	inline float get_positionZ_2() const { return ___positionZ_2; }
	inline float* get_address_of_positionZ_2() { return &___positionZ_2; }
	inline void set_positionZ_2(float value)
	{
		___positionZ_2 = value;
	}

	inline static int32_t get_offset_of_rotationX_3() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___rotationX_3)); }
	inline float get_rotationX_3() const { return ___rotationX_3; }
	inline float* get_address_of_rotationX_3() { return &___rotationX_3; }
	inline void set_rotationX_3(float value)
	{
		___rotationX_3 = value;
	}

	inline static int32_t get_offset_of_rotationY_4() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___rotationY_4)); }
	inline float get_rotationY_4() const { return ___rotationY_4; }
	inline float* get_address_of_rotationY_4() { return &___rotationY_4; }
	inline void set_rotationY_4(float value)
	{
		___rotationY_4 = value;
	}

	inline static int32_t get_offset_of_rotationZ_5() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___rotationZ_5)); }
	inline float get_rotationZ_5() const { return ___rotationZ_5; }
	inline float* get_address_of_rotationZ_5() { return &___rotationZ_5; }
	inline void set_rotationZ_5(float value)
	{
		___rotationZ_5 = value;
	}

	inline static int32_t get_offset_of_rotationW_6() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___rotationW_6)); }
	inline float get_rotationW_6() const { return ___rotationW_6; }
	inline float* get_address_of_rotationW_6() { return &___rotationW_6; }
	inline void set_rotationW_6(float value)
	{
		___rotationW_6 = value;
	}

	inline static int32_t get_offset_of_dimensionsX_7() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___dimensionsX_7)); }
	inline float get_dimensionsX_7() const { return ___dimensionsX_7; }
	inline float* get_address_of_dimensionsX_7() { return &___dimensionsX_7; }
	inline void set_dimensionsX_7(float value)
	{
		___dimensionsX_7 = value;
	}

	inline static int32_t get_offset_of_dimensionsY_8() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___dimensionsY_8)); }
	inline float get_dimensionsY_8() const { return ___dimensionsY_8; }
	inline float* get_address_of_dimensionsY_8() { return &___dimensionsY_8; }
	inline void set_dimensionsY_8(float value)
	{
		___dimensionsY_8 = value;
	}

	inline static int32_t get_offset_of_dimensionsZ_9() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___dimensionsZ_9)); }
	inline float get_dimensionsZ_9() const { return ___dimensionsZ_9; }
	inline float* get_address_of_dimensionsZ_9() { return &___dimensionsZ_9; }
	inline void set_dimensionsZ_9(float value)
	{
		___dimensionsZ_9 = value;
	}

	inline static int32_t get_offset_of_materialLeft_10() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___materialLeft_10)); }
	inline int32_t get_materialLeft_10() const { return ___materialLeft_10; }
	inline int32_t* get_address_of_materialLeft_10() { return &___materialLeft_10; }
	inline void set_materialLeft_10(int32_t value)
	{
		___materialLeft_10 = value;
	}

	inline static int32_t get_offset_of_materialRight_11() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___materialRight_11)); }
	inline int32_t get_materialRight_11() const { return ___materialRight_11; }
	inline int32_t* get_address_of_materialRight_11() { return &___materialRight_11; }
	inline void set_materialRight_11(int32_t value)
	{
		___materialRight_11 = value;
	}

	inline static int32_t get_offset_of_materialBottom_12() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___materialBottom_12)); }
	inline int32_t get_materialBottom_12() const { return ___materialBottom_12; }
	inline int32_t* get_address_of_materialBottom_12() { return &___materialBottom_12; }
	inline void set_materialBottom_12(int32_t value)
	{
		___materialBottom_12 = value;
	}

	inline static int32_t get_offset_of_materialTop_13() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___materialTop_13)); }
	inline int32_t get_materialTop_13() const { return ___materialTop_13; }
	inline int32_t* get_address_of_materialTop_13() { return &___materialTop_13; }
	inline void set_materialTop_13(int32_t value)
	{
		___materialTop_13 = value;
	}

	inline static int32_t get_offset_of_materialFront_14() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___materialFront_14)); }
	inline int32_t get_materialFront_14() const { return ___materialFront_14; }
	inline int32_t* get_address_of_materialFront_14() { return &___materialFront_14; }
	inline void set_materialFront_14(int32_t value)
	{
		___materialFront_14 = value;
	}

	inline static int32_t get_offset_of_materialBack_15() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___materialBack_15)); }
	inline int32_t get_materialBack_15() const { return ___materialBack_15; }
	inline int32_t* get_address_of_materialBack_15() { return &___materialBack_15; }
	inline void set_materialBack_15(int32_t value)
	{
		___materialBack_15 = value;
	}

	inline static int32_t get_offset_of_reflectionScalar_16() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___reflectionScalar_16)); }
	inline float get_reflectionScalar_16() const { return ___reflectionScalar_16; }
	inline float* get_address_of_reflectionScalar_16() { return &___reflectionScalar_16; }
	inline void set_reflectionScalar_16(float value)
	{
		___reflectionScalar_16 = value;
	}

	inline static int32_t get_offset_of_reverbGain_17() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___reverbGain_17)); }
	inline float get_reverbGain_17() const { return ___reverbGain_17; }
	inline float* get_address_of_reverbGain_17() { return &___reverbGain_17; }
	inline void set_reverbGain_17(float value)
	{
		___reverbGain_17 = value;
	}

	inline static int32_t get_offset_of_reverbTime_18() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___reverbTime_18)); }
	inline float get_reverbTime_18() const { return ___reverbTime_18; }
	inline float* get_address_of_reverbTime_18() { return &___reverbTime_18; }
	inline void set_reverbTime_18(float value)
	{
		___reverbTime_18 = value;
	}

	inline static int32_t get_offset_of_reverbBrightness_19() { return static_cast<int32_t>(offsetof(RoomProperties_t228572612, ___reverbBrightness_19)); }
	inline float get_reverbBrightness_19() const { return ___reverbBrightness_19; }
	inline float* get_address_of_reverbBrightness_19() { return &___reverbBrightness_19; }
	inline void set_reverbBrightness_19(float value)
	{
		___reverbBrightness_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOMPROPERTIES_T228572612_H
#ifndef CARETINFO_T841780893_H
#define CARETINFO_T841780893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretInfo
struct  CaretInfo_t841780893 
{
public:
	// System.Int32 TMPro.CaretInfo::index
	int32_t ___index_0;
	// TMPro.CaretPosition TMPro.CaretInfo::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(CaretInfo_t841780893, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETINFO_T841780893_H
#ifndef COMPUTE_DT_EVENTARGS_T1071353166_H
#define COMPUTE_DT_EVENTARGS_T1071353166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DT_EventArgs
struct  Compute_DT_EventArgs_t1071353166  : public RuntimeObject
{
public:
	// TMPro.Compute_DistanceTransform_EventTypes TMPro.Compute_DT_EventArgs::EventType
	int32_t ___EventType_0;
	// System.Single TMPro.Compute_DT_EventArgs::ProgressPercentage
	float ___ProgressPercentage_1;
	// UnityEngine.Color[] TMPro.Compute_DT_EventArgs::Colors
	ColorU5BU5D_t941916413* ___Colors_2;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_ProgressPercentage_1() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___ProgressPercentage_1)); }
	inline float get_ProgressPercentage_1() const { return ___ProgressPercentage_1; }
	inline float* get_address_of_ProgressPercentage_1() { return &___ProgressPercentage_1; }
	inline void set_ProgressPercentage_1(float value)
	{
		___ProgressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_Colors_2() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t1071353166, ___Colors_2)); }
	inline ColorU5BU5D_t941916413* get_Colors_2() const { return ___Colors_2; }
	inline ColorU5BU5D_t941916413** get_address_of_Colors_2() { return &___Colors_2; }
	inline void set_Colors_2(ColorU5BU5D_t941916413* value)
	{
		___Colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DT_EVENTARGS_T1071353166_H
#ifndef TMP_CHARACTERINFO_T3185626797_H
#define TMP_CHARACTERINFO_T3185626797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t3185626797 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t129727469 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t340375123 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t3722313464  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t3722313464  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t3722313464  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t3722313464  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t2600501292  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___textElement_3)); }
	inline TMP_TextElement_t129727469 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t129727469 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t129727469 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___fontAsset_4)); }
	inline TMP_FontAsset_t364381626 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t364381626 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t484820633 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t484820633 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___material_7)); }
	inline Material_t340375123 * get_material_7() const { return ___material_7; }
	inline Material_t340375123 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t340375123 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___pageNumber_12)); }
	inline int32_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int32_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int32_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TL_14)); }
	inline TMP_Vertex_t2404176824  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t2404176824  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BL_15)); }
	inline TMP_Vertex_t2404176824  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t2404176824  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_TR_16)); }
	inline TMP_Vertex_t2404176824  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t2404176824  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___vertex_BR_17)); }
	inline TMP_Vertex_t2404176824  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t2404176824 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t2404176824  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topLeft_18)); }
	inline Vector3_t3722313464  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t3722313464 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t3722313464  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomLeft_19)); }
	inline Vector3_t3722313464  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t3722313464 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t3722313464  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___topRight_20)); }
	inline Vector3_t3722313464  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t3722313464 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t3722313464  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___bottomRight_21)); }
	inline Vector3_t3722313464  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t3722313464 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t3722313464  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___color_29)); }
	inline Color32_t2600501292  get_color_29() const { return ___color_29; }
	inline Color32_t2600501292 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t2600501292  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t3185626797, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t3185626797_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t129727469 * ___textElement_3;
	TMP_FontAsset_t364381626 * ___fontAsset_4;
	TMP_SpriteAsset_t484820633 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t340375123 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t2404176824  ___vertex_TL_14;
	TMP_Vertex_t2404176824  ___vertex_BL_15;
	TMP_Vertex_t2404176824  ___vertex_TR_16;
	TMP_Vertex_t2404176824  ___vertex_BR_17;
	Vector3_t3722313464  ___topLeft_18;
	Vector3_t3722313464  ___bottomLeft_19;
	Vector3_t3722313464  ___topRight_20;
	Vector3_t3722313464  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t2600501292  ___color_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T3185626797_H
#ifndef TMP_LINEINFO_T1079631636_H
#define TMP_LINEINFO_T1079631636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t1079631636 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3837212874  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t1079631636, ___lineExtents_19)); }
	inline Extents_t3837212874  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_t3837212874 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_t3837212874  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T1079631636_H
#ifndef TMP_XMLTAGSTACK_1_T3600445780_H
#define TMP_XMLTAGSTACK_1_T3600445780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t3600445780 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t3552942253* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t3552942253* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t3552942253** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t3552942253* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3600445780, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3600445780_H
#ifndef XML_TAGATTRIBUTE_T1174424309_H
#define XML_TAGATTRIBUTE_T1174424309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_t1174424309 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// TMPro.TagType TMPro.XML_TagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueType_1)); }
	inline int32_t get_valueType_1() const { return ___valueType_1; }
	inline int32_t* get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(int32_t value)
	{
		___valueType_1 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueStartIndex_2)); }
	inline int32_t get_valueStartIndex_2() const { return ___valueStartIndex_2; }
	inline int32_t* get_address_of_valueStartIndex_2() { return &___valueStartIndex_2; }
	inline void set_valueStartIndex_2(int32_t value)
	{
		___valueStartIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1174424309, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XML_TAGATTRIBUTE_T1174424309_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef WORDWRAPSTATE_T341939652_H
#define WORDWRAPSTATE_T341939652_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t3241710312  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t3241710312  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_50)); }
	inline Material_t340375123 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_t340375123 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_t340375123 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_52)); }
	inline Extents_t3837212874  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_t3837212874 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_t3837212874  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t3241710312  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_39;
	TMP_XmlTagStack_1_t960921318  ___indentStack_40;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_42;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_43;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_44;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t364381626 * ___currentFontAsset_48;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_49;
	Material_t340375123 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3837212874  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T341939652_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CAMERACONTROLLER_T3346819214_H
#define CAMERACONTROLLER_T3346819214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraController
struct  CameraController_t3346819214  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CameraController::speedH
	float ___speedH_4;
	// System.Single CameraController::speedV
	float ___speedV_5;
	// System.Single CameraController::yaw
	float ___yaw_6;
	// System.Single CameraController::pitch
	float ___pitch_7;
	// UnityEngine.Transform CameraController::robotKyle
	Transform_t3600365921 * ___robotKyle_8;

public:
	inline static int32_t get_offset_of_speedH_4() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___speedH_4)); }
	inline float get_speedH_4() const { return ___speedH_4; }
	inline float* get_address_of_speedH_4() { return &___speedH_4; }
	inline void set_speedH_4(float value)
	{
		___speedH_4 = value;
	}

	inline static int32_t get_offset_of_speedV_5() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___speedV_5)); }
	inline float get_speedV_5() const { return ___speedV_5; }
	inline float* get_address_of_speedV_5() { return &___speedV_5; }
	inline void set_speedV_5(float value)
	{
		___speedV_5 = value;
	}

	inline static int32_t get_offset_of_yaw_6() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___yaw_6)); }
	inline float get_yaw_6() const { return ___yaw_6; }
	inline float* get_address_of_yaw_6() { return &___yaw_6; }
	inline void set_yaw_6(float value)
	{
		___yaw_6 = value;
	}

	inline static int32_t get_offset_of_pitch_7() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___pitch_7)); }
	inline float get_pitch_7() const { return ___pitch_7; }
	inline float* get_address_of_pitch_7() { return &___pitch_7; }
	inline void set_pitch_7(float value)
	{
		___pitch_7 = value;
	}

	inline static int32_t get_offset_of_robotKyle_8() { return static_cast<int32_t>(offsetof(CameraController_t3346819214, ___robotKyle_8)); }
	inline Transform_t3600365921 * get_robotKyle_8() const { return ___robotKyle_8; }
	inline Transform_t3600365921 ** get_address_of_robotKyle_8() { return &___robotKyle_8; }
	inline void set_robotKyle_8(Transform_t3600365921 * value)
	{
		___robotKyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___robotKyle_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T3346819214_H
#ifndef DEMOINPUTMANAGER_T2406750443_H
#define DEMOINPUTMANAGER_T2406750443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.Demos.DemoInputManager
struct  DemoInputManager_t2406750443  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GoogleVR.Demos.DemoInputManager::isDaydream
	bool ___isDaydream_18;
	// System.Int32 GoogleVR.Demos.DemoInputManager::activeControllerPointer
	int32_t ___activeControllerPointer_19;
	// UnityEngine.GameObject GoogleVR.Demos.DemoInputManager::controllerMain
	GameObject_t1113636619 * ___controllerMain_21;
	// UnityEngine.GameObject[] GoogleVR.Demos.DemoInputManager::controllerPointers
	GameObjectU5BU5D_t3328599146* ___controllerPointers_23;
	// UnityEngine.GameObject GoogleVR.Demos.DemoInputManager::reticlePointer
	GameObject_t1113636619 * ___reticlePointer_25;
	// UnityEngine.GameObject GoogleVR.Demos.DemoInputManager::messageCanvas
	GameObject_t1113636619 * ___messageCanvas_27;
	// UnityEngine.UI.Text GoogleVR.Demos.DemoInputManager::messageText
	Text_t1901882714 * ___messageText_28;
	// GoogleVR.Demos.DemoInputManager/EmulatedPlatformType GoogleVR.Demos.DemoInputManager::gvrEmulatedPlatformType
	int32_t ___gvrEmulatedPlatformType_29;

public:
	inline static int32_t get_offset_of_isDaydream_18() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443, ___isDaydream_18)); }
	inline bool get_isDaydream_18() const { return ___isDaydream_18; }
	inline bool* get_address_of_isDaydream_18() { return &___isDaydream_18; }
	inline void set_isDaydream_18(bool value)
	{
		___isDaydream_18 = value;
	}

	inline static int32_t get_offset_of_activeControllerPointer_19() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443, ___activeControllerPointer_19)); }
	inline int32_t get_activeControllerPointer_19() const { return ___activeControllerPointer_19; }
	inline int32_t* get_address_of_activeControllerPointer_19() { return &___activeControllerPointer_19; }
	inline void set_activeControllerPointer_19(int32_t value)
	{
		___activeControllerPointer_19 = value;
	}

	inline static int32_t get_offset_of_controllerMain_21() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443, ___controllerMain_21)); }
	inline GameObject_t1113636619 * get_controllerMain_21() const { return ___controllerMain_21; }
	inline GameObject_t1113636619 ** get_address_of_controllerMain_21() { return &___controllerMain_21; }
	inline void set_controllerMain_21(GameObject_t1113636619 * value)
	{
		___controllerMain_21 = value;
		Il2CppCodeGenWriteBarrier((&___controllerMain_21), value);
	}

	inline static int32_t get_offset_of_controllerPointers_23() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443, ___controllerPointers_23)); }
	inline GameObjectU5BU5D_t3328599146* get_controllerPointers_23() const { return ___controllerPointers_23; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_controllerPointers_23() { return &___controllerPointers_23; }
	inline void set_controllerPointers_23(GameObjectU5BU5D_t3328599146* value)
	{
		___controllerPointers_23 = value;
		Il2CppCodeGenWriteBarrier((&___controllerPointers_23), value);
	}

	inline static int32_t get_offset_of_reticlePointer_25() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443, ___reticlePointer_25)); }
	inline GameObject_t1113636619 * get_reticlePointer_25() const { return ___reticlePointer_25; }
	inline GameObject_t1113636619 ** get_address_of_reticlePointer_25() { return &___reticlePointer_25; }
	inline void set_reticlePointer_25(GameObject_t1113636619 * value)
	{
		___reticlePointer_25 = value;
		Il2CppCodeGenWriteBarrier((&___reticlePointer_25), value);
	}

	inline static int32_t get_offset_of_messageCanvas_27() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443, ___messageCanvas_27)); }
	inline GameObject_t1113636619 * get_messageCanvas_27() const { return ___messageCanvas_27; }
	inline GameObject_t1113636619 ** get_address_of_messageCanvas_27() { return &___messageCanvas_27; }
	inline void set_messageCanvas_27(GameObject_t1113636619 * value)
	{
		___messageCanvas_27 = value;
		Il2CppCodeGenWriteBarrier((&___messageCanvas_27), value);
	}

	inline static int32_t get_offset_of_messageText_28() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443, ___messageText_28)); }
	inline Text_t1901882714 * get_messageText_28() const { return ___messageText_28; }
	inline Text_t1901882714 ** get_address_of_messageText_28() { return &___messageText_28; }
	inline void set_messageText_28(Text_t1901882714 * value)
	{
		___messageText_28 = value;
		Il2CppCodeGenWriteBarrier((&___messageText_28), value);
	}

	inline static int32_t get_offset_of_gvrEmulatedPlatformType_29() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443, ___gvrEmulatedPlatformType_29)); }
	inline int32_t get_gvrEmulatedPlatformType_29() const { return ___gvrEmulatedPlatformType_29; }
	inline int32_t* get_address_of_gvrEmulatedPlatformType_29() { return &___gvrEmulatedPlatformType_29; }
	inline void set_gvrEmulatedPlatformType_29(int32_t value)
	{
		___gvrEmulatedPlatformType_29 = value;
	}
};

struct DemoInputManager_t2406750443_StaticFields
{
public:
	// GvrControllerHand[] GoogleVR.Demos.DemoInputManager::AllHands
	GvrControllerHandU5BU5D_t2583795907* ___AllHands_20;
	// System.String GoogleVR.Demos.DemoInputManager::CONTROLLER_MAIN_PROP_NAME
	String_t* ___CONTROLLER_MAIN_PROP_NAME_22;
	// System.String GoogleVR.Demos.DemoInputManager::CONTROLLER_POINTER_PROP_NAME
	String_t* ___CONTROLLER_POINTER_PROP_NAME_24;
	// System.String GoogleVR.Demos.DemoInputManager::RETICLE_POINTER_PROP_NAME
	String_t* ___RETICLE_POINTER_PROP_NAME_26;
	// System.String GoogleVR.Demos.DemoInputManager::EMULATED_PLATFORM_PROP_NAME
	String_t* ___EMULATED_PLATFORM_PROP_NAME_30;
	// System.Predicate`1<System.String> GoogleVR.Demos.DemoInputManager::<>f__am$cache0
	Predicate_1_t2672744813 * ___U3CU3Ef__amU24cache0_31;
	// System.Predicate`1<System.String> GoogleVR.Demos.DemoInputManager::<>f__am$cache1
	Predicate_1_t2672744813 * ___U3CU3Ef__amU24cache1_32;

public:
	inline static int32_t get_offset_of_AllHands_20() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443_StaticFields, ___AllHands_20)); }
	inline GvrControllerHandU5BU5D_t2583795907* get_AllHands_20() const { return ___AllHands_20; }
	inline GvrControllerHandU5BU5D_t2583795907** get_address_of_AllHands_20() { return &___AllHands_20; }
	inline void set_AllHands_20(GvrControllerHandU5BU5D_t2583795907* value)
	{
		___AllHands_20 = value;
		Il2CppCodeGenWriteBarrier((&___AllHands_20), value);
	}

	inline static int32_t get_offset_of_CONTROLLER_MAIN_PROP_NAME_22() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443_StaticFields, ___CONTROLLER_MAIN_PROP_NAME_22)); }
	inline String_t* get_CONTROLLER_MAIN_PROP_NAME_22() const { return ___CONTROLLER_MAIN_PROP_NAME_22; }
	inline String_t** get_address_of_CONTROLLER_MAIN_PROP_NAME_22() { return &___CONTROLLER_MAIN_PROP_NAME_22; }
	inline void set_CONTROLLER_MAIN_PROP_NAME_22(String_t* value)
	{
		___CONTROLLER_MAIN_PROP_NAME_22 = value;
		Il2CppCodeGenWriteBarrier((&___CONTROLLER_MAIN_PROP_NAME_22), value);
	}

	inline static int32_t get_offset_of_CONTROLLER_POINTER_PROP_NAME_24() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443_StaticFields, ___CONTROLLER_POINTER_PROP_NAME_24)); }
	inline String_t* get_CONTROLLER_POINTER_PROP_NAME_24() const { return ___CONTROLLER_POINTER_PROP_NAME_24; }
	inline String_t** get_address_of_CONTROLLER_POINTER_PROP_NAME_24() { return &___CONTROLLER_POINTER_PROP_NAME_24; }
	inline void set_CONTROLLER_POINTER_PROP_NAME_24(String_t* value)
	{
		___CONTROLLER_POINTER_PROP_NAME_24 = value;
		Il2CppCodeGenWriteBarrier((&___CONTROLLER_POINTER_PROP_NAME_24), value);
	}

	inline static int32_t get_offset_of_RETICLE_POINTER_PROP_NAME_26() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443_StaticFields, ___RETICLE_POINTER_PROP_NAME_26)); }
	inline String_t* get_RETICLE_POINTER_PROP_NAME_26() const { return ___RETICLE_POINTER_PROP_NAME_26; }
	inline String_t** get_address_of_RETICLE_POINTER_PROP_NAME_26() { return &___RETICLE_POINTER_PROP_NAME_26; }
	inline void set_RETICLE_POINTER_PROP_NAME_26(String_t* value)
	{
		___RETICLE_POINTER_PROP_NAME_26 = value;
		Il2CppCodeGenWriteBarrier((&___RETICLE_POINTER_PROP_NAME_26), value);
	}

	inline static int32_t get_offset_of_EMULATED_PLATFORM_PROP_NAME_30() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443_StaticFields, ___EMULATED_PLATFORM_PROP_NAME_30)); }
	inline String_t* get_EMULATED_PLATFORM_PROP_NAME_30() const { return ___EMULATED_PLATFORM_PROP_NAME_30; }
	inline String_t** get_address_of_EMULATED_PLATFORM_PROP_NAME_30() { return &___EMULATED_PLATFORM_PROP_NAME_30; }
	inline void set_EMULATED_PLATFORM_PROP_NAME_30(String_t* value)
	{
		___EMULATED_PLATFORM_PROP_NAME_30 = value;
		Il2CppCodeGenWriteBarrier((&___EMULATED_PLATFORM_PROP_NAME_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_31() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443_StaticFields, ___U3CU3Ef__amU24cache0_31)); }
	inline Predicate_1_t2672744813 * get_U3CU3Ef__amU24cache0_31() const { return ___U3CU3Ef__amU24cache0_31; }
	inline Predicate_1_t2672744813 ** get_address_of_U3CU3Ef__amU24cache0_31() { return &___U3CU3Ef__amU24cache0_31; }
	inline void set_U3CU3Ef__amU24cache0_31(Predicate_1_t2672744813 * value)
	{
		___U3CU3Ef__amU24cache0_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_32() { return static_cast<int32_t>(offsetof(DemoInputManager_t2406750443_StaticFields, ___U3CU3Ef__amU24cache1_32)); }
	inline Predicate_1_t2672744813 * get_U3CU3Ef__amU24cache1_32() const { return ___U3CU3Ef__amU24cache1_32; }
	inline Predicate_1_t2672744813 ** get_address_of_U3CU3Ef__amU24cache1_32() { return &___U3CU3Ef__amU24cache1_32; }
	inline void set_U3CU3Ef__amU24cache1_32(Predicate_1_t2672744813 * value)
	{
		___U3CU3Ef__amU24cache1_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOINPUTMANAGER_T2406750443_H
#ifndef DEMOSCENEMANAGER_T3984584607_H
#define DEMOSCENEMANAGER_T3984584607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.Demos.DemoSceneManager
struct  DemoSceneManager_t3984584607  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCENEMANAGER_T3984584607_H
#ifndef HEADSETDEMOMANAGER_T2048377926_H
#define HEADSETDEMOMANAGER_T2048377926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.HelloVR.HeadsetDemoManager
struct  HeadsetDemoManager_t2048377926  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GoogleVR.HelloVR.HeadsetDemoManager::safetyRing
	GameObject_t1113636619 * ___safetyRing_4;
	// System.Boolean GoogleVR.HelloVR.HeadsetDemoManager::enableDebugLog
	bool ___enableDebugLog_5;
	// UnityEngine.WaitForSeconds GoogleVR.HelloVR.HeadsetDemoManager::waitFourSeconds
	WaitForSeconds_t1699091251 * ___waitFourSeconds_6;

public:
	inline static int32_t get_offset_of_safetyRing_4() { return static_cast<int32_t>(offsetof(HeadsetDemoManager_t2048377926, ___safetyRing_4)); }
	inline GameObject_t1113636619 * get_safetyRing_4() const { return ___safetyRing_4; }
	inline GameObject_t1113636619 ** get_address_of_safetyRing_4() { return &___safetyRing_4; }
	inline void set_safetyRing_4(GameObject_t1113636619 * value)
	{
		___safetyRing_4 = value;
		Il2CppCodeGenWriteBarrier((&___safetyRing_4), value);
	}

	inline static int32_t get_offset_of_enableDebugLog_5() { return static_cast<int32_t>(offsetof(HeadsetDemoManager_t2048377926, ___enableDebugLog_5)); }
	inline bool get_enableDebugLog_5() const { return ___enableDebugLog_5; }
	inline bool* get_address_of_enableDebugLog_5() { return &___enableDebugLog_5; }
	inline void set_enableDebugLog_5(bool value)
	{
		___enableDebugLog_5 = value;
	}

	inline static int32_t get_offset_of_waitFourSeconds_6() { return static_cast<int32_t>(offsetof(HeadsetDemoManager_t2048377926, ___waitFourSeconds_6)); }
	inline WaitForSeconds_t1699091251 * get_waitFourSeconds_6() const { return ___waitFourSeconds_6; }
	inline WaitForSeconds_t1699091251 ** get_address_of_waitFourSeconds_6() { return &___waitFourSeconds_6; }
	inline void set_waitFourSeconds_6(WaitForSeconds_t1699091251 * value)
	{
		___waitFourSeconds_6 = value;
		Il2CppCodeGenWriteBarrier((&___waitFourSeconds_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETDEMOMANAGER_T2048377926_H
#ifndef HELLOVRMANAGER_T3122911991_H
#define HELLOVRMANAGER_T3122911991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.HelloVR.HelloVRManager
struct  HelloVRManager_t3122911991  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GoogleVR.HelloVR.HelloVRManager::m_launchVrHomeButton
	GameObject_t1113636619 * ___m_launchVrHomeButton_4;
	// GoogleVR.Demos.DemoInputManager GoogleVR.HelloVR.HelloVRManager::m_demoInputManager
	DemoInputManager_t2406750443 * ___m_demoInputManager_5;

public:
	inline static int32_t get_offset_of_m_launchVrHomeButton_4() { return static_cast<int32_t>(offsetof(HelloVRManager_t3122911991, ___m_launchVrHomeButton_4)); }
	inline GameObject_t1113636619 * get_m_launchVrHomeButton_4() const { return ___m_launchVrHomeButton_4; }
	inline GameObject_t1113636619 ** get_address_of_m_launchVrHomeButton_4() { return &___m_launchVrHomeButton_4; }
	inline void set_m_launchVrHomeButton_4(GameObject_t1113636619 * value)
	{
		___m_launchVrHomeButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_launchVrHomeButton_4), value);
	}

	inline static int32_t get_offset_of_m_demoInputManager_5() { return static_cast<int32_t>(offsetof(HelloVRManager_t3122911991, ___m_demoInputManager_5)); }
	inline DemoInputManager_t2406750443 * get_m_demoInputManager_5() const { return ___m_demoInputManager_5; }
	inline DemoInputManager_t2406750443 ** get_address_of_m_demoInputManager_5() { return &___m_demoInputManager_5; }
	inline void set_m_demoInputManager_5(DemoInputManager_t2406750443 * value)
	{
		___m_demoInputManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_demoInputManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HELLOVRMANAGER_T3122911991_H
#ifndef OBJECTCONTROLLER_T1463023822_H
#define OBJECTCONTROLLER_T1463023822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.HelloVR.ObjectController
struct  ObjectController_t1463023822  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 GoogleVR.HelloVR.ObjectController::startingPosition
	Vector3_t3722313464  ___startingPosition_4;
	// UnityEngine.Renderer GoogleVR.HelloVR.ObjectController::myRenderer
	Renderer_t2627027031 * ___myRenderer_5;
	// UnityEngine.Material GoogleVR.HelloVR.ObjectController::inactiveMaterial
	Material_t340375123 * ___inactiveMaterial_6;
	// UnityEngine.Material GoogleVR.HelloVR.ObjectController::gazedAtMaterial
	Material_t340375123 * ___gazedAtMaterial_7;

public:
	inline static int32_t get_offset_of_startingPosition_4() { return static_cast<int32_t>(offsetof(ObjectController_t1463023822, ___startingPosition_4)); }
	inline Vector3_t3722313464  get_startingPosition_4() const { return ___startingPosition_4; }
	inline Vector3_t3722313464 * get_address_of_startingPosition_4() { return &___startingPosition_4; }
	inline void set_startingPosition_4(Vector3_t3722313464  value)
	{
		___startingPosition_4 = value;
	}

	inline static int32_t get_offset_of_myRenderer_5() { return static_cast<int32_t>(offsetof(ObjectController_t1463023822, ___myRenderer_5)); }
	inline Renderer_t2627027031 * get_myRenderer_5() const { return ___myRenderer_5; }
	inline Renderer_t2627027031 ** get_address_of_myRenderer_5() { return &___myRenderer_5; }
	inline void set_myRenderer_5(Renderer_t2627027031 * value)
	{
		___myRenderer_5 = value;
		Il2CppCodeGenWriteBarrier((&___myRenderer_5), value);
	}

	inline static int32_t get_offset_of_inactiveMaterial_6() { return static_cast<int32_t>(offsetof(ObjectController_t1463023822, ___inactiveMaterial_6)); }
	inline Material_t340375123 * get_inactiveMaterial_6() const { return ___inactiveMaterial_6; }
	inline Material_t340375123 ** get_address_of_inactiveMaterial_6() { return &___inactiveMaterial_6; }
	inline void set_inactiveMaterial_6(Material_t340375123 * value)
	{
		___inactiveMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___inactiveMaterial_6), value);
	}

	inline static int32_t get_offset_of_gazedAtMaterial_7() { return static_cast<int32_t>(offsetof(ObjectController_t1463023822, ___gazedAtMaterial_7)); }
	inline Material_t340375123 * get_gazedAtMaterial_7() const { return ___gazedAtMaterial_7; }
	inline Material_t340375123 ** get_address_of_gazedAtMaterial_7() { return &___gazedAtMaterial_7; }
	inline void set_gazedAtMaterial_7(Material_t340375123 * value)
	{
		___gazedAtMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___gazedAtMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCONTROLLER_T1463023822_H
#ifndef APPBUTTONINPUT_T1611596170_H
#define APPBUTTONINPUT_T1611596170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.AppButtonInput
struct  AppButtonInput_t1611596170  : public MonoBehaviour_t3962482529
{
public:
	// GoogleVR.VideoDemo.ButtonEvent GoogleVR.VideoDemo.AppButtonInput::OnAppUp
	ButtonEvent_t4044041544 * ___OnAppUp_4;
	// GoogleVR.VideoDemo.ButtonEvent GoogleVR.VideoDemo.AppButtonInput::OnAppDown
	ButtonEvent_t4044041544 * ___OnAppDown_5;

public:
	inline static int32_t get_offset_of_OnAppUp_4() { return static_cast<int32_t>(offsetof(AppButtonInput_t1611596170, ___OnAppUp_4)); }
	inline ButtonEvent_t4044041544 * get_OnAppUp_4() const { return ___OnAppUp_4; }
	inline ButtonEvent_t4044041544 ** get_address_of_OnAppUp_4() { return &___OnAppUp_4; }
	inline void set_OnAppUp_4(ButtonEvent_t4044041544 * value)
	{
		___OnAppUp_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnAppUp_4), value);
	}

	inline static int32_t get_offset_of_OnAppDown_5() { return static_cast<int32_t>(offsetof(AppButtonInput_t1611596170, ___OnAppDown_5)); }
	inline ButtonEvent_t4044041544 * get_OnAppDown_5() const { return ___OnAppDown_5; }
	inline ButtonEvent_t4044041544 ** get_address_of_OnAppDown_5() { return &___OnAppDown_5; }
	inline void set_OnAppDown_5(ButtonEvent_t4044041544 * value)
	{
		___OnAppDown_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnAppDown_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPBUTTONINPUT_T1611596170_H
#ifndef AUTOPLAYVIDEO_T2664495001_H
#define AUTOPLAYVIDEO_T2664495001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.AutoPlayVideo
struct  AutoPlayVideo_t2664495001  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean GoogleVR.VideoDemo.AutoPlayVideo::done
	bool ___done_4;
	// System.Single GoogleVR.VideoDemo.AutoPlayVideo::t
	float ___t_5;
	// GvrVideoPlayerTexture GoogleVR.VideoDemo.AutoPlayVideo::player
	GvrVideoPlayerTexture_t3546202735 * ___player_6;
	// System.Single GoogleVR.VideoDemo.AutoPlayVideo::delay
	float ___delay_7;
	// System.Boolean GoogleVR.VideoDemo.AutoPlayVideo::loop
	bool ___loop_8;

public:
	inline static int32_t get_offset_of_done_4() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t2664495001, ___done_4)); }
	inline bool get_done_4() const { return ___done_4; }
	inline bool* get_address_of_done_4() { return &___done_4; }
	inline void set_done_4(bool value)
	{
		___done_4 = value;
	}

	inline static int32_t get_offset_of_t_5() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t2664495001, ___t_5)); }
	inline float get_t_5() const { return ___t_5; }
	inline float* get_address_of_t_5() { return &___t_5; }
	inline void set_t_5(float value)
	{
		___t_5 = value;
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t2664495001, ___player_6)); }
	inline GvrVideoPlayerTexture_t3546202735 * get_player_6() const { return ___player_6; }
	inline GvrVideoPlayerTexture_t3546202735 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(GvrVideoPlayerTexture_t3546202735 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t2664495001, ___delay_7)); }
	inline float get_delay_7() const { return ___delay_7; }
	inline float* get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(float value)
	{
		___delay_7 = value;
	}

	inline static int32_t get_offset_of_loop_8() { return static_cast<int32_t>(offsetof(AutoPlayVideo_t2664495001, ___loop_8)); }
	inline bool get_loop_8() const { return ___loop_8; }
	inline bool* get_address_of_loop_8() { return &___loop_8; }
	inline void set_loop_8(bool value)
	{
		___loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAYVIDEO_T2664495001_H
#ifndef MENUHANDLER_T3978103572_H
#define MENUHANDLER_T3978103572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.MenuHandler
struct  MenuHandler_t3978103572  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] GoogleVR.VideoDemo.MenuHandler::menuObjects
	GameObjectU5BU5D_t3328599146* ___menuObjects_4;

public:
	inline static int32_t get_offset_of_menuObjects_4() { return static_cast<int32_t>(offsetof(MenuHandler_t3978103572, ___menuObjects_4)); }
	inline GameObjectU5BU5D_t3328599146* get_menuObjects_4() const { return ___menuObjects_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_menuObjects_4() { return &___menuObjects_4; }
	inline void set_menuObjects_4(GameObjectU5BU5D_t3328599146* value)
	{
		___menuObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuObjects_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUHANDLER_T3978103572_H
#ifndef POSITIONSWAPPER_T3174547502_H
#define POSITIONSWAPPER_T3174547502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.PositionSwapper
struct  PositionSwapper_t3174547502  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GoogleVR.VideoDemo.PositionSwapper::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.Vector3[] GoogleVR.VideoDemo.PositionSwapper::Positions
	Vector3U5BU5D_t1718750761* ___Positions_5;

public:
	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(PositionSwapper_t3174547502, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_Positions_5() { return static_cast<int32_t>(offsetof(PositionSwapper_t3174547502, ___Positions_5)); }
	inline Vector3U5BU5D_t1718750761* get_Positions_5() const { return ___Positions_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_Positions_5() { return &___Positions_5; }
	inline void set_Positions_5(Vector3U5BU5D_t1718750761* value)
	{
		___Positions_5 = value;
		Il2CppCodeGenWriteBarrier((&___Positions_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONSWAPPER_T3174547502_H
#ifndef SCRUBBEREVENTS_T2166768381_H
#define SCRUBBEREVENTS_T2166768381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.ScrubberEvents
struct  ScrubberEvents_t2166768381  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GoogleVR.VideoDemo.ScrubberEvents::newPositionHandle
	GameObject_t1113636619 * ___newPositionHandle_4;
	// UnityEngine.Vector3[] GoogleVR.VideoDemo.ScrubberEvents::corners
	Vector3U5BU5D_t1718750761* ___corners_5;
	// UnityEngine.UI.Slider GoogleVR.VideoDemo.ScrubberEvents::slider
	Slider_t3903728902 * ___slider_6;
	// GoogleVR.VideoDemo.VideoControlsManager GoogleVR.VideoDemo.ScrubberEvents::mgr
	VideoControlsManager_t1556912537 * ___mgr_7;

public:
	inline static int32_t get_offset_of_newPositionHandle_4() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2166768381, ___newPositionHandle_4)); }
	inline GameObject_t1113636619 * get_newPositionHandle_4() const { return ___newPositionHandle_4; }
	inline GameObject_t1113636619 ** get_address_of_newPositionHandle_4() { return &___newPositionHandle_4; }
	inline void set_newPositionHandle_4(GameObject_t1113636619 * value)
	{
		___newPositionHandle_4 = value;
		Il2CppCodeGenWriteBarrier((&___newPositionHandle_4), value);
	}

	inline static int32_t get_offset_of_corners_5() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2166768381, ___corners_5)); }
	inline Vector3U5BU5D_t1718750761* get_corners_5() const { return ___corners_5; }
	inline Vector3U5BU5D_t1718750761** get_address_of_corners_5() { return &___corners_5; }
	inline void set_corners_5(Vector3U5BU5D_t1718750761* value)
	{
		___corners_5 = value;
		Il2CppCodeGenWriteBarrier((&___corners_5), value);
	}

	inline static int32_t get_offset_of_slider_6() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2166768381, ___slider_6)); }
	inline Slider_t3903728902 * get_slider_6() const { return ___slider_6; }
	inline Slider_t3903728902 ** get_address_of_slider_6() { return &___slider_6; }
	inline void set_slider_6(Slider_t3903728902 * value)
	{
		___slider_6 = value;
		Il2CppCodeGenWriteBarrier((&___slider_6), value);
	}

	inline static int32_t get_offset_of_mgr_7() { return static_cast<int32_t>(offsetof(ScrubberEvents_t2166768381, ___mgr_7)); }
	inline VideoControlsManager_t1556912537 * get_mgr_7() const { return ___mgr_7; }
	inline VideoControlsManager_t1556912537 ** get_address_of_mgr_7() { return &___mgr_7; }
	inline void set_mgr_7(VideoControlsManager_t1556912537 * value)
	{
		___mgr_7 = value;
		Il2CppCodeGenWriteBarrier((&___mgr_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRUBBEREVENTS_T2166768381_H
#ifndef SWITCHVIDEOS_T1699469881_H
#define SWITCHVIDEOS_T1699469881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.SwitchVideos
struct  SwitchVideos_t1699469881  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GoogleVR.VideoDemo.SwitchVideos::localVideoSample
	GameObject_t1113636619 * ___localVideoSample_4;
	// UnityEngine.GameObject GoogleVR.VideoDemo.SwitchVideos::dashVideoSample
	GameObject_t1113636619 * ___dashVideoSample_5;
	// UnityEngine.GameObject GoogleVR.VideoDemo.SwitchVideos::panoVideoSample
	GameObject_t1113636619 * ___panoVideoSample_6;
	// UnityEngine.GameObject[] GoogleVR.VideoDemo.SwitchVideos::videoSamples
	GameObjectU5BU5D_t3328599146* ___videoSamples_7;
	// UnityEngine.UI.Text GoogleVR.VideoDemo.SwitchVideos::missingLibText
	Text_t1901882714 * ___missingLibText_8;

public:
	inline static int32_t get_offset_of_localVideoSample_4() { return static_cast<int32_t>(offsetof(SwitchVideos_t1699469881, ___localVideoSample_4)); }
	inline GameObject_t1113636619 * get_localVideoSample_4() const { return ___localVideoSample_4; }
	inline GameObject_t1113636619 ** get_address_of_localVideoSample_4() { return &___localVideoSample_4; }
	inline void set_localVideoSample_4(GameObject_t1113636619 * value)
	{
		___localVideoSample_4 = value;
		Il2CppCodeGenWriteBarrier((&___localVideoSample_4), value);
	}

	inline static int32_t get_offset_of_dashVideoSample_5() { return static_cast<int32_t>(offsetof(SwitchVideos_t1699469881, ___dashVideoSample_5)); }
	inline GameObject_t1113636619 * get_dashVideoSample_5() const { return ___dashVideoSample_5; }
	inline GameObject_t1113636619 ** get_address_of_dashVideoSample_5() { return &___dashVideoSample_5; }
	inline void set_dashVideoSample_5(GameObject_t1113636619 * value)
	{
		___dashVideoSample_5 = value;
		Il2CppCodeGenWriteBarrier((&___dashVideoSample_5), value);
	}

	inline static int32_t get_offset_of_panoVideoSample_6() { return static_cast<int32_t>(offsetof(SwitchVideos_t1699469881, ___panoVideoSample_6)); }
	inline GameObject_t1113636619 * get_panoVideoSample_6() const { return ___panoVideoSample_6; }
	inline GameObject_t1113636619 ** get_address_of_panoVideoSample_6() { return &___panoVideoSample_6; }
	inline void set_panoVideoSample_6(GameObject_t1113636619 * value)
	{
		___panoVideoSample_6 = value;
		Il2CppCodeGenWriteBarrier((&___panoVideoSample_6), value);
	}

	inline static int32_t get_offset_of_videoSamples_7() { return static_cast<int32_t>(offsetof(SwitchVideos_t1699469881, ___videoSamples_7)); }
	inline GameObjectU5BU5D_t3328599146* get_videoSamples_7() const { return ___videoSamples_7; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_videoSamples_7() { return &___videoSamples_7; }
	inline void set_videoSamples_7(GameObjectU5BU5D_t3328599146* value)
	{
		___videoSamples_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoSamples_7), value);
	}

	inline static int32_t get_offset_of_missingLibText_8() { return static_cast<int32_t>(offsetof(SwitchVideos_t1699469881, ___missingLibText_8)); }
	inline Text_t1901882714 * get_missingLibText_8() const { return ___missingLibText_8; }
	inline Text_t1901882714 ** get_address_of_missingLibText_8() { return &___missingLibText_8; }
	inline void set_missingLibText_8(Text_t1901882714 * value)
	{
		___missingLibText_8 = value;
		Il2CppCodeGenWriteBarrier((&___missingLibText_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHVIDEOS_T1699469881_H
#ifndef TOGGLEACTION_T3185694766_H
#define TOGGLEACTION_T3185694766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.ToggleAction
struct  ToggleAction_t3185694766  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GoogleVR.VideoDemo.ToggleAction::lastUsage
	float ___lastUsage_4;
	// System.Boolean GoogleVR.VideoDemo.ToggleAction::on
	bool ___on_5;
	// UnityEngine.Events.UnityEvent GoogleVR.VideoDemo.ToggleAction::OnToggleOn
	UnityEvent_t2581268647 * ___OnToggleOn_6;
	// UnityEngine.Events.UnityEvent GoogleVR.VideoDemo.ToggleAction::OnToggleOff
	UnityEvent_t2581268647 * ___OnToggleOff_7;
	// System.Boolean GoogleVR.VideoDemo.ToggleAction::InitialState
	bool ___InitialState_8;
	// System.Boolean GoogleVR.VideoDemo.ToggleAction::RaiseEventForInitialState
	bool ___RaiseEventForInitialState_9;
	// System.Single GoogleVR.VideoDemo.ToggleAction::Cooldown
	float ___Cooldown_10;

public:
	inline static int32_t get_offset_of_lastUsage_4() { return static_cast<int32_t>(offsetof(ToggleAction_t3185694766, ___lastUsage_4)); }
	inline float get_lastUsage_4() const { return ___lastUsage_4; }
	inline float* get_address_of_lastUsage_4() { return &___lastUsage_4; }
	inline void set_lastUsage_4(float value)
	{
		___lastUsage_4 = value;
	}

	inline static int32_t get_offset_of_on_5() { return static_cast<int32_t>(offsetof(ToggleAction_t3185694766, ___on_5)); }
	inline bool get_on_5() const { return ___on_5; }
	inline bool* get_address_of_on_5() { return &___on_5; }
	inline void set_on_5(bool value)
	{
		___on_5 = value;
	}

	inline static int32_t get_offset_of_OnToggleOn_6() { return static_cast<int32_t>(offsetof(ToggleAction_t3185694766, ___OnToggleOn_6)); }
	inline UnityEvent_t2581268647 * get_OnToggleOn_6() const { return ___OnToggleOn_6; }
	inline UnityEvent_t2581268647 ** get_address_of_OnToggleOn_6() { return &___OnToggleOn_6; }
	inline void set_OnToggleOn_6(UnityEvent_t2581268647 * value)
	{
		___OnToggleOn_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnToggleOn_6), value);
	}

	inline static int32_t get_offset_of_OnToggleOff_7() { return static_cast<int32_t>(offsetof(ToggleAction_t3185694766, ___OnToggleOff_7)); }
	inline UnityEvent_t2581268647 * get_OnToggleOff_7() const { return ___OnToggleOff_7; }
	inline UnityEvent_t2581268647 ** get_address_of_OnToggleOff_7() { return &___OnToggleOff_7; }
	inline void set_OnToggleOff_7(UnityEvent_t2581268647 * value)
	{
		___OnToggleOff_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnToggleOff_7), value);
	}

	inline static int32_t get_offset_of_InitialState_8() { return static_cast<int32_t>(offsetof(ToggleAction_t3185694766, ___InitialState_8)); }
	inline bool get_InitialState_8() const { return ___InitialState_8; }
	inline bool* get_address_of_InitialState_8() { return &___InitialState_8; }
	inline void set_InitialState_8(bool value)
	{
		___InitialState_8 = value;
	}

	inline static int32_t get_offset_of_RaiseEventForInitialState_9() { return static_cast<int32_t>(offsetof(ToggleAction_t3185694766, ___RaiseEventForInitialState_9)); }
	inline bool get_RaiseEventForInitialState_9() const { return ___RaiseEventForInitialState_9; }
	inline bool* get_address_of_RaiseEventForInitialState_9() { return &___RaiseEventForInitialState_9; }
	inline void set_RaiseEventForInitialState_9(bool value)
	{
		___RaiseEventForInitialState_9 = value;
	}

	inline static int32_t get_offset_of_Cooldown_10() { return static_cast<int32_t>(offsetof(ToggleAction_t3185694766, ___Cooldown_10)); }
	inline float get_Cooldown_10() const { return ___Cooldown_10; }
	inline float* get_address_of_Cooldown_10() { return &___Cooldown_10; }
	inline void set_Cooldown_10(float value)
	{
		___Cooldown_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEACTION_T3185694766_H
#ifndef VIDEOCONTROLSMANAGER_T1556912537_H
#define VIDEOCONTROLSMANAGER_T1556912537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.VideoControlsManager
struct  VideoControlsManager_t1556912537  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::pauseSprite
	GameObject_t1113636619 * ___pauseSprite_4;
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::playSprite
	GameObject_t1113636619 * ___playSprite_5;
	// UnityEngine.UI.Slider GoogleVR.VideoDemo.VideoControlsManager::videoScrubber
	Slider_t3903728902 * ___videoScrubber_6;
	// UnityEngine.UI.Slider GoogleVR.VideoDemo.VideoControlsManager::volumeSlider
	Slider_t3903728902 * ___volumeSlider_7;
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::volumeWidget
	GameObject_t1113636619 * ___volumeWidget_8;
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::settingsPanel
	GameObject_t1113636619 * ___settingsPanel_9;
	// UnityEngine.GameObject GoogleVR.VideoDemo.VideoControlsManager::bufferedBackground
	GameObject_t1113636619 * ___bufferedBackground_10;
	// UnityEngine.Vector3 GoogleVR.VideoDemo.VideoControlsManager::basePosition
	Vector3_t3722313464  ___basePosition_11;
	// UnityEngine.UI.Text GoogleVR.VideoDemo.VideoControlsManager::videoPosition
	Text_t1901882714 * ___videoPosition_12;
	// UnityEngine.UI.Text GoogleVR.VideoDemo.VideoControlsManager::videoDuration
	Text_t1901882714 * ___videoDuration_13;
	// GvrVideoPlayerTexture GoogleVR.VideoDemo.VideoControlsManager::<Player>k__BackingField
	GvrVideoPlayerTexture_t3546202735 * ___U3CPlayerU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_pauseSprite_4() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___pauseSprite_4)); }
	inline GameObject_t1113636619 * get_pauseSprite_4() const { return ___pauseSprite_4; }
	inline GameObject_t1113636619 ** get_address_of_pauseSprite_4() { return &___pauseSprite_4; }
	inline void set_pauseSprite_4(GameObject_t1113636619 * value)
	{
		___pauseSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___pauseSprite_4), value);
	}

	inline static int32_t get_offset_of_playSprite_5() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___playSprite_5)); }
	inline GameObject_t1113636619 * get_playSprite_5() const { return ___playSprite_5; }
	inline GameObject_t1113636619 ** get_address_of_playSprite_5() { return &___playSprite_5; }
	inline void set_playSprite_5(GameObject_t1113636619 * value)
	{
		___playSprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___playSprite_5), value);
	}

	inline static int32_t get_offset_of_videoScrubber_6() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___videoScrubber_6)); }
	inline Slider_t3903728902 * get_videoScrubber_6() const { return ___videoScrubber_6; }
	inline Slider_t3903728902 ** get_address_of_videoScrubber_6() { return &___videoScrubber_6; }
	inline void set_videoScrubber_6(Slider_t3903728902 * value)
	{
		___videoScrubber_6 = value;
		Il2CppCodeGenWriteBarrier((&___videoScrubber_6), value);
	}

	inline static int32_t get_offset_of_volumeSlider_7() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___volumeSlider_7)); }
	inline Slider_t3903728902 * get_volumeSlider_7() const { return ___volumeSlider_7; }
	inline Slider_t3903728902 ** get_address_of_volumeSlider_7() { return &___volumeSlider_7; }
	inline void set_volumeSlider_7(Slider_t3903728902 * value)
	{
		___volumeSlider_7 = value;
		Il2CppCodeGenWriteBarrier((&___volumeSlider_7), value);
	}

	inline static int32_t get_offset_of_volumeWidget_8() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___volumeWidget_8)); }
	inline GameObject_t1113636619 * get_volumeWidget_8() const { return ___volumeWidget_8; }
	inline GameObject_t1113636619 ** get_address_of_volumeWidget_8() { return &___volumeWidget_8; }
	inline void set_volumeWidget_8(GameObject_t1113636619 * value)
	{
		___volumeWidget_8 = value;
		Il2CppCodeGenWriteBarrier((&___volumeWidget_8), value);
	}

	inline static int32_t get_offset_of_settingsPanel_9() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___settingsPanel_9)); }
	inline GameObject_t1113636619 * get_settingsPanel_9() const { return ___settingsPanel_9; }
	inline GameObject_t1113636619 ** get_address_of_settingsPanel_9() { return &___settingsPanel_9; }
	inline void set_settingsPanel_9(GameObject_t1113636619 * value)
	{
		___settingsPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___settingsPanel_9), value);
	}

	inline static int32_t get_offset_of_bufferedBackground_10() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___bufferedBackground_10)); }
	inline GameObject_t1113636619 * get_bufferedBackground_10() const { return ___bufferedBackground_10; }
	inline GameObject_t1113636619 ** get_address_of_bufferedBackground_10() { return &___bufferedBackground_10; }
	inline void set_bufferedBackground_10(GameObject_t1113636619 * value)
	{
		___bufferedBackground_10 = value;
		Il2CppCodeGenWriteBarrier((&___bufferedBackground_10), value);
	}

	inline static int32_t get_offset_of_basePosition_11() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___basePosition_11)); }
	inline Vector3_t3722313464  get_basePosition_11() const { return ___basePosition_11; }
	inline Vector3_t3722313464 * get_address_of_basePosition_11() { return &___basePosition_11; }
	inline void set_basePosition_11(Vector3_t3722313464  value)
	{
		___basePosition_11 = value;
	}

	inline static int32_t get_offset_of_videoPosition_12() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___videoPosition_12)); }
	inline Text_t1901882714 * get_videoPosition_12() const { return ___videoPosition_12; }
	inline Text_t1901882714 ** get_address_of_videoPosition_12() { return &___videoPosition_12; }
	inline void set_videoPosition_12(Text_t1901882714 * value)
	{
		___videoPosition_12 = value;
		Il2CppCodeGenWriteBarrier((&___videoPosition_12), value);
	}

	inline static int32_t get_offset_of_videoDuration_13() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___videoDuration_13)); }
	inline Text_t1901882714 * get_videoDuration_13() const { return ___videoDuration_13; }
	inline Text_t1901882714 ** get_address_of_videoDuration_13() { return &___videoDuration_13; }
	inline void set_videoDuration_13(Text_t1901882714 * value)
	{
		___videoDuration_13 = value;
		Il2CppCodeGenWriteBarrier((&___videoDuration_13), value);
	}

	inline static int32_t get_offset_of_U3CPlayerU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(VideoControlsManager_t1556912537, ___U3CPlayerU3Ek__BackingField_14)); }
	inline GvrVideoPlayerTexture_t3546202735 * get_U3CPlayerU3Ek__BackingField_14() const { return ___U3CPlayerU3Ek__BackingField_14; }
	inline GvrVideoPlayerTexture_t3546202735 ** get_address_of_U3CPlayerU3Ek__BackingField_14() { return &___U3CPlayerU3Ek__BackingField_14; }
	inline void set_U3CPlayerU3Ek__BackingField_14(GvrVideoPlayerTexture_t3546202735 * value)
	{
		___U3CPlayerU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlayerU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCONTROLSMANAGER_T1556912537_H
#ifndef VIDEOPLAYERREFERENCE_T625508773_H
#define VIDEOPLAYERREFERENCE_T625508773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.VideoDemo.VideoPlayerReference
struct  VideoPlayerReference_t625508773  : public MonoBehaviour_t3962482529
{
public:
	// GvrVideoPlayerTexture GoogleVR.VideoDemo.VideoPlayerReference::player
	GvrVideoPlayerTexture_t3546202735 * ___player_4;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(VideoPlayerReference_t625508773, ___player_4)); }
	inline GvrVideoPlayerTexture_t3546202735 * get_player_4() const { return ___player_4; }
	inline GvrVideoPlayerTexture_t3546202735 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GvrVideoPlayerTexture_t3546202735 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYERREFERENCE_T625508773_H
#ifndef GVRAUDIOLISTENER_T82459280_H
#define GVRAUDIOLISTENER_T82459280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrAudioListener
struct  GvrAudioListener_t82459280  : public MonoBehaviour_t3962482529
{
public:
	// System.Single GvrAudioListener::globalGainDb
	float ___globalGainDb_4;
	// UnityEngine.LayerMask GvrAudioListener::occlusionMask
	LayerMask_t3493934918  ___occlusionMask_5;
	// GvrAudio/Quality GvrAudioListener::quality
	int32_t ___quality_6;

public:
	inline static int32_t get_offset_of_globalGainDb_4() { return static_cast<int32_t>(offsetof(GvrAudioListener_t82459280, ___globalGainDb_4)); }
	inline float get_globalGainDb_4() const { return ___globalGainDb_4; }
	inline float* get_address_of_globalGainDb_4() { return &___globalGainDb_4; }
	inline void set_globalGainDb_4(float value)
	{
		___globalGainDb_4 = value;
	}

	inline static int32_t get_offset_of_occlusionMask_5() { return static_cast<int32_t>(offsetof(GvrAudioListener_t82459280, ___occlusionMask_5)); }
	inline LayerMask_t3493934918  get_occlusionMask_5() const { return ___occlusionMask_5; }
	inline LayerMask_t3493934918 * get_address_of_occlusionMask_5() { return &___occlusionMask_5; }
	inline void set_occlusionMask_5(LayerMask_t3493934918  value)
	{
		___occlusionMask_5 = value;
	}

	inline static int32_t get_offset_of_quality_6() { return static_cast<int32_t>(offsetof(GvrAudioListener_t82459280, ___quality_6)); }
	inline int32_t get_quality_6() const { return ___quality_6; }
	inline int32_t* get_address_of_quality_6() { return &___quality_6; }
	inline void set_quality_6(int32_t value)
	{
		___quality_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRAUDIOLISTENER_T82459280_H
#ifndef GVRKEYBOARDDELEGATEBASE_T30895224_H
#define GVRKEYBOARDDELEGATEBASE_T30895224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardDelegateBase
struct  GvrKeyboardDelegateBase_t30895224  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDDELEGATEBASE_T30895224_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef VRFOCUSMARKERBASE_T1705746600_H
#define VRFOCUSMARKERBASE_T1705746600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRFocusMarkerBase
struct  VRFocusMarkerBase_t1705746600  : public MonoBehaviour_t3962482529
{
public:
	// VRFocusRayCaster VRFocusMarkerBase::vrRayCaster
	VRFocusRayCaster_t1481207659 * ___vrRayCaster_4;
	// System.Boolean VRFocusMarkerBase::isFocused
	bool ___isFocused_5;
	// System.Single VRFocusMarkerBase::processingTime
	float ___processingTime_6;
	// System.Single VRFocusMarkerBase::focusTime
	float ___focusTime_7;

public:
	inline static int32_t get_offset_of_vrRayCaster_4() { return static_cast<int32_t>(offsetof(VRFocusMarkerBase_t1705746600, ___vrRayCaster_4)); }
	inline VRFocusRayCaster_t1481207659 * get_vrRayCaster_4() const { return ___vrRayCaster_4; }
	inline VRFocusRayCaster_t1481207659 ** get_address_of_vrRayCaster_4() { return &___vrRayCaster_4; }
	inline void set_vrRayCaster_4(VRFocusRayCaster_t1481207659 * value)
	{
		___vrRayCaster_4 = value;
		Il2CppCodeGenWriteBarrier((&___vrRayCaster_4), value);
	}

	inline static int32_t get_offset_of_isFocused_5() { return static_cast<int32_t>(offsetof(VRFocusMarkerBase_t1705746600, ___isFocused_5)); }
	inline bool get_isFocused_5() const { return ___isFocused_5; }
	inline bool* get_address_of_isFocused_5() { return &___isFocused_5; }
	inline void set_isFocused_5(bool value)
	{
		___isFocused_5 = value;
	}

	inline static int32_t get_offset_of_processingTime_6() { return static_cast<int32_t>(offsetof(VRFocusMarkerBase_t1705746600, ___processingTime_6)); }
	inline float get_processingTime_6() const { return ___processingTime_6; }
	inline float* get_address_of_processingTime_6() { return &___processingTime_6; }
	inline void set_processingTime_6(float value)
	{
		___processingTime_6 = value;
	}

	inline static int32_t get_offset_of_focusTime_7() { return static_cast<int32_t>(offsetof(VRFocusMarkerBase_t1705746600, ___focusTime_7)); }
	inline float get_focusTime_7() const { return ___focusTime_7; }
	inline float* get_address_of_focusTime_7() { return &___focusTime_7; }
	inline void set_focusTime_7(float value)
	{
		___focusTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRFOCUSMARKERBASE_T1705746600_H
#ifndef VRFOCUSRAYCASTER_T1481207659_H
#define VRFOCUSRAYCASTER_T1481207659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRFocusRayCaster
struct  VRFocusRayCaster_t1481207659  : public MonoBehaviour_t3962482529
{
public:
	// VRInput VRFocusRayCaster::vrInput
	VRInput_t441285841 * ___vrInput_4;
	// VRFocusMarkerBase VRFocusRayCaster::vrFocusMarker
	VRFocusMarkerBase_t1705746600 * ___vrFocusMarker_5;
	// UnityEngine.GameObject VRFocusRayCaster::focusCamera
	GameObject_t1113636619 * ___focusCamera_6;
	// VRInteractableBase VRFocusRayCaster::currentInteractable
	VRInteractableBase_t641439594 * ___currentInteractable_7;

public:
	inline static int32_t get_offset_of_vrInput_4() { return static_cast<int32_t>(offsetof(VRFocusRayCaster_t1481207659, ___vrInput_4)); }
	inline VRInput_t441285841 * get_vrInput_4() const { return ___vrInput_4; }
	inline VRInput_t441285841 ** get_address_of_vrInput_4() { return &___vrInput_4; }
	inline void set_vrInput_4(VRInput_t441285841 * value)
	{
		___vrInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___vrInput_4), value);
	}

	inline static int32_t get_offset_of_vrFocusMarker_5() { return static_cast<int32_t>(offsetof(VRFocusRayCaster_t1481207659, ___vrFocusMarker_5)); }
	inline VRFocusMarkerBase_t1705746600 * get_vrFocusMarker_5() const { return ___vrFocusMarker_5; }
	inline VRFocusMarkerBase_t1705746600 ** get_address_of_vrFocusMarker_5() { return &___vrFocusMarker_5; }
	inline void set_vrFocusMarker_5(VRFocusMarkerBase_t1705746600 * value)
	{
		___vrFocusMarker_5 = value;
		Il2CppCodeGenWriteBarrier((&___vrFocusMarker_5), value);
	}

	inline static int32_t get_offset_of_focusCamera_6() { return static_cast<int32_t>(offsetof(VRFocusRayCaster_t1481207659, ___focusCamera_6)); }
	inline GameObject_t1113636619 * get_focusCamera_6() const { return ___focusCamera_6; }
	inline GameObject_t1113636619 ** get_address_of_focusCamera_6() { return &___focusCamera_6; }
	inline void set_focusCamera_6(GameObject_t1113636619 * value)
	{
		___focusCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___focusCamera_6), value);
	}

	inline static int32_t get_offset_of_currentInteractable_7() { return static_cast<int32_t>(offsetof(VRFocusRayCaster_t1481207659, ___currentInteractable_7)); }
	inline VRInteractableBase_t641439594 * get_currentInteractable_7() const { return ___currentInteractable_7; }
	inline VRInteractableBase_t641439594 ** get_address_of_currentInteractable_7() { return &___currentInteractable_7; }
	inline void set_currentInteractable_7(VRInteractableBase_t641439594 * value)
	{
		___currentInteractable_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentInteractable_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRFOCUSRAYCASTER_T1481207659_H
#ifndef VRINPUT_T441285841_H
#define VRINPUT_T441285841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRInput
struct  VRInput_t441285841  : public MonoBehaviour_t3962482529
{
public:
	// System.Action VRInput::OnDown
	Action_t1264377477 * ___OnDown_4;
	// System.Action VRInput::OnUp
	Action_t1264377477 * ___OnUp_5;

public:
	inline static int32_t get_offset_of_OnDown_4() { return static_cast<int32_t>(offsetof(VRInput_t441285841, ___OnDown_4)); }
	inline Action_t1264377477 * get_OnDown_4() const { return ___OnDown_4; }
	inline Action_t1264377477 ** get_address_of_OnDown_4() { return &___OnDown_4; }
	inline void set_OnDown_4(Action_t1264377477 * value)
	{
		___OnDown_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnDown_4), value);
	}

	inline static int32_t get_offset_of_OnUp_5() { return static_cast<int32_t>(offsetof(VRInput_t441285841, ___OnUp_5)); }
	inline Action_t1264377477 * get_OnUp_5() const { return ___OnUp_5; }
	inline Action_t1264377477 ** get_address_of_OnUp_5() { return &___OnUp_5; }
	inline void set_OnUp_5(Action_t1264377477 * value)
	{
		___OnUp_5 = value;
		Il2CppCodeGenWriteBarrier((&___OnUp_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRINPUT_T441285841_H
#ifndef VRINTERACTABLEBASE_T641439594_H
#define VRINTERACTABLEBASE_T641439594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VRInteractableBase
struct  VRInteractableBase_t641439594  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRINTERACTABLEBASE_T641439594_H
#ifndef KEYBOARDDELEGATEEXAMPLE_T2997392785_H
#define KEYBOARDDELEGATEEXAMPLE_T2997392785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoogleVR.KeyboardDemo.KeyboardDelegateExample
struct  KeyboardDelegateExample_t2997392785  : public GvrKeyboardDelegateBase_t30895224
{
public:
	// UnityEngine.UI.Text GoogleVR.KeyboardDemo.KeyboardDelegateExample::KeyboardText
	Text_t1901882714 * ___KeyboardText_4;
	// UnityEngine.Canvas GoogleVR.KeyboardDemo.KeyboardDelegateExample::UpdateCanvas
	Canvas_t3310196443 * ___UpdateCanvas_5;
	// System.EventHandler GoogleVR.KeyboardDemo.KeyboardDelegateExample::KeyboardHidden
	EventHandler_t1348719766 * ___KeyboardHidden_6;
	// System.EventHandler GoogleVR.KeyboardDemo.KeyboardDelegateExample::KeyboardShown
	EventHandler_t1348719766 * ___KeyboardShown_7;

public:
	inline static int32_t get_offset_of_KeyboardText_4() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t2997392785, ___KeyboardText_4)); }
	inline Text_t1901882714 * get_KeyboardText_4() const { return ___KeyboardText_4; }
	inline Text_t1901882714 ** get_address_of_KeyboardText_4() { return &___KeyboardText_4; }
	inline void set_KeyboardText_4(Text_t1901882714 * value)
	{
		___KeyboardText_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardText_4), value);
	}

	inline static int32_t get_offset_of_UpdateCanvas_5() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t2997392785, ___UpdateCanvas_5)); }
	inline Canvas_t3310196443 * get_UpdateCanvas_5() const { return ___UpdateCanvas_5; }
	inline Canvas_t3310196443 ** get_address_of_UpdateCanvas_5() { return &___UpdateCanvas_5; }
	inline void set_UpdateCanvas_5(Canvas_t3310196443 * value)
	{
		___UpdateCanvas_5 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateCanvas_5), value);
	}

	inline static int32_t get_offset_of_KeyboardHidden_6() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t2997392785, ___KeyboardHidden_6)); }
	inline EventHandler_t1348719766 * get_KeyboardHidden_6() const { return ___KeyboardHidden_6; }
	inline EventHandler_t1348719766 ** get_address_of_KeyboardHidden_6() { return &___KeyboardHidden_6; }
	inline void set_KeyboardHidden_6(EventHandler_t1348719766 * value)
	{
		___KeyboardHidden_6 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardHidden_6), value);
	}

	inline static int32_t get_offset_of_KeyboardShown_7() { return static_cast<int32_t>(offsetof(KeyboardDelegateExample_t2997392785, ___KeyboardShown_7)); }
	inline EventHandler_t1348719766 * get_KeyboardShown_7() const { return ___KeyboardShown_7; }
	inline EventHandler_t1348719766 ** get_address_of_KeyboardShown_7() { return &___KeyboardShown_7; }
	inline void set_KeyboardShown_7(EventHandler_t1348719766 * value)
	{
		___KeyboardShown_7 = value;
		Il2CppCodeGenWriteBarrier((&___KeyboardShown_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDDELEGATEEXAMPLE_T2997392785_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TMP_TEXT_T2599618874_H
#define TMP_TEXT_T2599618874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t2599618874  : public MaskableGraphic_t3839221559
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_30;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_31;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t364381626 * ___m_fontAsset_32;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t364381626 * ___m_currentFontAsset_33;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t340375123 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t340375123 * ___m_currentMaterial_36;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t648826345* ___m_materialReferences_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t1839659084 * ___m_materialReferenceIndexLookup_38;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___m_materialReferenceStack_39;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t561872642* ___m_fontSharedMaterials_41;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t340375123 * ___m_fontMaterial_42;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t561872642* ___m_fontMaterials_43;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t2600501292  ___m_fontColor32_45;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t2555686324  ___m_fontColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t2600501292  ___m_underlineColor_48;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t2600501292  ___m_strikethroughColor_49;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t2600501292  ___m_highlightColor_50;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_51;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t345148380  ___m_fontColorGradient_52;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_fontColorGradientPreset_53;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_54;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_55;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t2600501292  ___m_spriteColor_57;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_58;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t2600501292  ___m_faceColor_59;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t2600501292  ___m_outlineColor_60;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_61;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_62;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_63;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_64;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t960921318  ___m_sizeStack_65;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_66;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_67;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___m_fontWeightStack_68;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_69;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_70;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_71;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_72;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_73;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_74;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_75;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___m_fontStyleStack_76;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_77;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_78;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_79;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___m_lineJustificationStack_80;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t1718750761* ___m_textContainerLocalCorners_81;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_82;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_83;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_84;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_85;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_86;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_87;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_88;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_89;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_90;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_91;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_92;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_93;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_94;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_95;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_96;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_97;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_98;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_99;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_100;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t2599618874 * ___m_linkedTextComponent_101;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_102;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_103;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_104;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_105;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_106;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_107;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_108;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_109;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_110;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_111;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_112;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_113;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_114;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_115;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_116;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_117;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_118;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_119;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_120;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_121;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_122;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_123;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_124;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_125;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t3319028937  ___m_margin_126;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_127;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_128;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_129;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_130;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_131;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t3598145122 * ___m_textInfo_132;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_133;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_134;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t3600365921 * ___m_transform_135;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t3704657025 * ___m_rectTransform_136;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_137;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_138;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t3648964284 * ___m_mesh_139;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_140;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2836635477 * ___m_spriteAnimator_141;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_142;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_143;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_144;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_145;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_146;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_147;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_t1785403678 * ___m_LayoutElement_148;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_149;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_150;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_151;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_152;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_153;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_154;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_155;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_156;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_157;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_158;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_159;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_160;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_161;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_162;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_163;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_164;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_165;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_166;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_167;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_168;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t3528271667* ___m_htmlTag_169;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t284240280* ___m_xmlAttribute_170;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t1444911251* ___m_attributeParameterValues_171;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_172;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_173;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t960921318  ___m_indentStack_174;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_175;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_176;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t1817901843  ___m_FXMatrix_177;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_178;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t385246372* ___m_char_buffer_179;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t1930184704* ___m_internalCharacterInfo_180;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t3528271667* ___m_input_CharArray_181;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_182;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_183;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t341939652  ___m_SavedWordWrapState_184;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t341939652  ___m_SavedLineState_185;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_186;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_187;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_188;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_189;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_190;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_191;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_192;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_193;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_194;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_195;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_196;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_197;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_198;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_199;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_200;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t3837212874  ___m_meshExtents_201;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t2600501292  ___m_htmlColor_202;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t2164155836  ___m_colorStack_203;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_underlineColorStack_204;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_strikethroughColorStack_205;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___m_highlightColorStack_206;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t3678055768 * ___m_colorGradientPreset_207;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_XmlTagStack_1_t3241710312  ___m_colorGradientStack_208;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_209;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_210;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2514600297  ___m_styleStack_211;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2514600297  ___m_actionStack_212;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_213;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_214;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t960921318  ___m_baselineOffsetStack_215;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_216;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_217;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t129727469 * ___m_cached_TextElement_218;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Underline_GlyphInfo_219;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_t581847833 * ___m_cached_Ellipsis_GlyphInfo_220;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_221;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_currentSpriteAsset_222;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_223;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_224;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_225;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_226;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t1444911251* ___k_Power_227;

public:
	inline static int32_t get_offset_of_m_text_30() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_text_30)); }
	inline String_t* get_m_text_30() const { return ___m_text_30; }
	inline String_t** get_address_of_m_text_30() { return &___m_text_30; }
	inline void set_m_text_30(String_t* value)
	{
		___m_text_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_30), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_31() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRightToLeft_31)); }
	inline bool get_m_isRightToLeft_31() const { return ___m_isRightToLeft_31; }
	inline bool* get_address_of_m_isRightToLeft_31() { return &___m_isRightToLeft_31; }
	inline void set_m_isRightToLeft_31(bool value)
	{
		___m_isRightToLeft_31 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_32() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontAsset_32)); }
	inline TMP_FontAsset_t364381626 * get_m_fontAsset_32() const { return ___m_fontAsset_32; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_fontAsset_32() { return &___m_fontAsset_32; }
	inline void set_m_fontAsset_32(TMP_FontAsset_t364381626 * value)
	{
		___m_fontAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_32), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_33() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontAsset_33)); }
	inline TMP_FontAsset_t364381626 * get_m_currentFontAsset_33() const { return ___m_currentFontAsset_33; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_currentFontAsset_33() { return &___m_currentFontAsset_33; }
	inline void set_m_currentFontAsset_33(TMP_FontAsset_t364381626 * value)
	{
		___m_currentFontAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_33), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_34() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isSDFShader_34)); }
	inline bool get_m_isSDFShader_34() const { return ___m_isSDFShader_34; }
	inline bool* get_address_of_m_isSDFShader_34() { return &___m_isSDFShader_34; }
	inline void set_m_isSDFShader_34(bool value)
	{
		___m_isSDFShader_34 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sharedMaterial_35)); }
	inline Material_t340375123 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_t340375123 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_t340375123 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterial_36)); }
	inline Material_t340375123 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_t340375123 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_t340375123 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_37() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferences_37)); }
	inline MaterialReferenceU5BU5D_t648826345* get_m_materialReferences_37() const { return ___m_materialReferences_37; }
	inline MaterialReferenceU5BU5D_t648826345** get_address_of_m_materialReferences_37() { return &___m_materialReferences_37; }
	inline void set_m_materialReferences_37(MaterialReferenceU5BU5D_t648826345* value)
	{
		___m_materialReferences_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_37), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_38() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceIndexLookup_38)); }
	inline Dictionary_2_t1839659084 * get_m_materialReferenceIndexLookup_38() const { return ___m_materialReferenceIndexLookup_38; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_materialReferenceIndexLookup_38() { return &___m_materialReferenceIndexLookup_38; }
	inline void set_m_materialReferenceIndexLookup_38(Dictionary_2_t1839659084 * value)
	{
		___m_materialReferenceIndexLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_38), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_39() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_materialReferenceStack_39)); }
	inline TMP_XmlTagStack_1_t1515999176  get_m_materialReferenceStack_39() const { return ___m_materialReferenceStack_39; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_m_materialReferenceStack_39() { return &___m_materialReferenceStack_39; }
	inline void set_m_materialReferenceStack_39(TMP_XmlTagStack_1_t1515999176  value)
	{
		___m_materialReferenceStack_39 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_40() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentMaterialIndex_40)); }
	inline int32_t get_m_currentMaterialIndex_40() const { return ___m_currentMaterialIndex_40; }
	inline int32_t* get_address_of_m_currentMaterialIndex_40() { return &___m_currentMaterialIndex_40; }
	inline void set_m_currentMaterialIndex_40(int32_t value)
	{
		___m_currentMaterialIndex_40 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSharedMaterials_41)); }
	inline MaterialU5BU5D_t561872642* get_m_fontSharedMaterials_41() const { return ___m_fontSharedMaterials_41; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontSharedMaterials_41() { return &___m_fontSharedMaterials_41; }
	inline void set_m_fontSharedMaterials_41(MaterialU5BU5D_t561872642* value)
	{
		___m_fontSharedMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_42() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterial_42)); }
	inline Material_t340375123 * get_m_fontMaterial_42() const { return ___m_fontMaterial_42; }
	inline Material_t340375123 ** get_address_of_m_fontMaterial_42() { return &___m_fontMaterial_42; }
	inline void set_m_fontMaterial_42(Material_t340375123 * value)
	{
		___m_fontMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_42), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_43() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontMaterials_43)); }
	inline MaterialU5BU5D_t561872642* get_m_fontMaterials_43() const { return ___m_fontMaterials_43; }
	inline MaterialU5BU5D_t561872642** get_address_of_m_fontMaterials_43() { return &___m_fontMaterials_43; }
	inline void set_m_fontMaterials_43(MaterialU5BU5D_t561872642* value)
	{
		___m_fontMaterials_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_43), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_44() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isMaterialDirty_44)); }
	inline bool get_m_isMaterialDirty_44() const { return ___m_isMaterialDirty_44; }
	inline bool* get_address_of_m_isMaterialDirty_44() { return &___m_isMaterialDirty_44; }
	inline void set_m_isMaterialDirty_44(bool value)
	{
		___m_isMaterialDirty_44 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_45() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor32_45)); }
	inline Color32_t2600501292  get_m_fontColor32_45() const { return ___m_fontColor32_45; }
	inline Color32_t2600501292 * get_address_of_m_fontColor32_45() { return &___m_fontColor32_45; }
	inline void set_m_fontColor32_45(Color32_t2600501292  value)
	{
		___m_fontColor32_45 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColor_46)); }
	inline Color_t2555686324  get_m_fontColor_46() const { return ___m_fontColor_46; }
	inline Color_t2555686324 * get_address_of_m_fontColor_46() { return &___m_fontColor_46; }
	inline void set_m_fontColor_46(Color_t2555686324  value)
	{
		___m_fontColor_46 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColor_48)); }
	inline Color32_t2600501292  get_m_underlineColor_48() const { return ___m_underlineColor_48; }
	inline Color32_t2600501292 * get_address_of_m_underlineColor_48() { return &___m_underlineColor_48; }
	inline void set_m_underlineColor_48(Color32_t2600501292  value)
	{
		___m_underlineColor_48 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_49() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColor_49)); }
	inline Color32_t2600501292  get_m_strikethroughColor_49() const { return ___m_strikethroughColor_49; }
	inline Color32_t2600501292 * get_address_of_m_strikethroughColor_49() { return &___m_strikethroughColor_49; }
	inline void set_m_strikethroughColor_49(Color32_t2600501292  value)
	{
		___m_strikethroughColor_49 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_50() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColor_50)); }
	inline Color32_t2600501292  get_m_highlightColor_50() const { return ___m_highlightColor_50; }
	inline Color32_t2600501292 * get_address_of_m_highlightColor_50() { return &___m_highlightColor_50; }
	inline void set_m_highlightColor_50(Color32_t2600501292  value)
	{
		___m_highlightColor_50 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_51() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableVertexGradient_51)); }
	inline bool get_m_enableVertexGradient_51() const { return ___m_enableVertexGradient_51; }
	inline bool* get_address_of_m_enableVertexGradient_51() { return &___m_enableVertexGradient_51; }
	inline void set_m_enableVertexGradient_51(bool value)
	{
		___m_enableVertexGradient_51 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_52() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradient_52)); }
	inline VertexGradient_t345148380  get_m_fontColorGradient_52() const { return ___m_fontColorGradient_52; }
	inline VertexGradient_t345148380 * get_address_of_m_fontColorGradient_52() { return &___m_fontColorGradient_52; }
	inline void set_m_fontColorGradient_52(VertexGradient_t345148380  value)
	{
		___m_fontColorGradient_52 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_53() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontColorGradientPreset_53)); }
	inline TMP_ColorGradient_t3678055768 * get_m_fontColorGradientPreset_53() const { return ___m_fontColorGradientPreset_53; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_fontColorGradientPreset_53() { return &___m_fontColorGradientPreset_53; }
	inline void set_m_fontColorGradientPreset_53(TMP_ColorGradient_t3678055768 * value)
	{
		___m_fontColorGradientPreset_53 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_53), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_54() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAsset_54)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_54() const { return ___m_spriteAsset_54; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_54() { return &___m_spriteAsset_54; }
	inline void set_m_spriteAsset_54(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_54 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_54), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_55() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintAllSprites_55)); }
	inline bool get_m_tintAllSprites_55() const { return ___m_tintAllSprites_55; }
	inline bool* get_address_of_m_tintAllSprites_55() { return &___m_tintAllSprites_55; }
	inline void set_m_tintAllSprites_55(bool value)
	{
		___m_tintAllSprites_55 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_56() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tintSprite_56)); }
	inline bool get_m_tintSprite_56() const { return ___m_tintSprite_56; }
	inline bool* get_address_of_m_tintSprite_56() { return &___m_tintSprite_56; }
	inline void set_m_tintSprite_56(bool value)
	{
		___m_tintSprite_56 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteColor_57)); }
	inline Color32_t2600501292  get_m_spriteColor_57() const { return ___m_spriteColor_57; }
	inline Color32_t2600501292 * get_address_of_m_spriteColor_57() { return &___m_spriteColor_57; }
	inline void set_m_spriteColor_57(Color32_t2600501292  value)
	{
		___m_spriteColor_57 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_58() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overrideHtmlColors_58)); }
	inline bool get_m_overrideHtmlColors_58() const { return ___m_overrideHtmlColors_58; }
	inline bool* get_address_of_m_overrideHtmlColors_58() { return &___m_overrideHtmlColors_58; }
	inline void set_m_overrideHtmlColors_58(bool value)
	{
		___m_overrideHtmlColors_58 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_59() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_faceColor_59)); }
	inline Color32_t2600501292  get_m_faceColor_59() const { return ___m_faceColor_59; }
	inline Color32_t2600501292 * get_address_of_m_faceColor_59() { return &___m_faceColor_59; }
	inline void set_m_faceColor_59(Color32_t2600501292  value)
	{
		___m_faceColor_59 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_60() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineColor_60)); }
	inline Color32_t2600501292  get_m_outlineColor_60() const { return ___m_outlineColor_60; }
	inline Color32_t2600501292 * get_address_of_m_outlineColor_60() { return &___m_outlineColor_60; }
	inline void set_m_outlineColor_60(Color32_t2600501292  value)
	{
		___m_outlineColor_60 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_61() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_outlineWidth_61)); }
	inline float get_m_outlineWidth_61() const { return ___m_outlineWidth_61; }
	inline float* get_address_of_m_outlineWidth_61() { return &___m_outlineWidth_61; }
	inline void set_m_outlineWidth_61(float value)
	{
		___m_outlineWidth_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_62() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSize_62)); }
	inline float get_m_fontSize_62() const { return ___m_fontSize_62; }
	inline float* get_address_of_m_fontSize_62() { return &___m_fontSize_62; }
	inline void set_m_fontSize_62(float value)
	{
		___m_fontSize_62 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_63() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentFontSize_63)); }
	inline float get_m_currentFontSize_63() const { return ___m_currentFontSize_63; }
	inline float* get_address_of_m_currentFontSize_63() { return &___m_currentFontSize_63; }
	inline void set_m_currentFontSize_63(float value)
	{
		___m_currentFontSize_63 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_64() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeBase_64)); }
	inline float get_m_fontSizeBase_64() const { return ___m_fontSizeBase_64; }
	inline float* get_address_of_m_fontSizeBase_64() { return &___m_fontSizeBase_64; }
	inline void set_m_fontSizeBase_64(float value)
	{
		___m_fontSizeBase_64 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_65() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_sizeStack_65)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_sizeStack_65() const { return ___m_sizeStack_65; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_sizeStack_65() { return &___m_sizeStack_65; }
	inline void set_m_sizeStack_65(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_sizeStack_65 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_66() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeight_66)); }
	inline int32_t get_m_fontWeight_66() const { return ___m_fontWeight_66; }
	inline int32_t* get_address_of_m_fontWeight_66() { return &___m_fontWeight_66; }
	inline void set_m_fontWeight_66(int32_t value)
	{
		___m_fontWeight_66 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_67() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightInternal_67)); }
	inline int32_t get_m_fontWeightInternal_67() const { return ___m_fontWeightInternal_67; }
	inline int32_t* get_address_of_m_fontWeightInternal_67() { return &___m_fontWeightInternal_67; }
	inline void set_m_fontWeightInternal_67(int32_t value)
	{
		___m_fontWeightInternal_67 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_68() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontWeightStack_68)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_fontWeightStack_68() const { return ___m_fontWeightStack_68; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_fontWeightStack_68() { return &___m_fontWeightStack_68; }
	inline void set_m_fontWeightStack_68(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_fontWeightStack_68 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_69() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableAutoSizing_69)); }
	inline bool get_m_enableAutoSizing_69() const { return ___m_enableAutoSizing_69; }
	inline bool* get_address_of_m_enableAutoSizing_69() { return &___m_enableAutoSizing_69; }
	inline void set_m_enableAutoSizing_69(bool value)
	{
		___m_enableAutoSizing_69 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_70() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxFontSize_70)); }
	inline float get_m_maxFontSize_70() const { return ___m_maxFontSize_70; }
	inline float* get_address_of_m_maxFontSize_70() { return &___m_maxFontSize_70; }
	inline void set_m_maxFontSize_70(float value)
	{
		___m_maxFontSize_70 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_71() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minFontSize_71)); }
	inline float get_m_minFontSize_71() const { return ___m_minFontSize_71; }
	inline float* get_address_of_m_minFontSize_71() { return &___m_minFontSize_71; }
	inline void set_m_minFontSize_71(float value)
	{
		___m_minFontSize_71 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_72() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMin_72)); }
	inline float get_m_fontSizeMin_72() const { return ___m_fontSizeMin_72; }
	inline float* get_address_of_m_fontSizeMin_72() { return &___m_fontSizeMin_72; }
	inline void set_m_fontSizeMin_72(float value)
	{
		___m_fontSizeMin_72 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_73() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontSizeMax_73)); }
	inline float get_m_fontSizeMax_73() const { return ___m_fontSizeMax_73; }
	inline float* get_address_of_m_fontSizeMax_73() { return &___m_fontSizeMax_73; }
	inline void set_m_fontSizeMax_73(float value)
	{
		___m_fontSizeMax_73 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_74() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyle_74)); }
	inline int32_t get_m_fontStyle_74() const { return ___m_fontStyle_74; }
	inline int32_t* get_address_of_m_fontStyle_74() { return &___m_fontStyle_74; }
	inline void set_m_fontStyle_74(int32_t value)
	{
		___m_fontStyle_74 = value;
	}

	inline static int32_t get_offset_of_m_style_75() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_style_75)); }
	inline int32_t get_m_style_75() const { return ___m_style_75; }
	inline int32_t* get_address_of_m_style_75() { return &___m_style_75; }
	inline void set_m_style_75(int32_t value)
	{
		___m_style_75 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_76() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontStyleStack_76)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_m_fontStyleStack_76() const { return ___m_fontStyleStack_76; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_m_fontStyleStack_76() { return &___m_fontStyleStack_76; }
	inline void set_m_fontStyleStack_76(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___m_fontStyleStack_76 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_77() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingBold_77)); }
	inline bool get_m_isUsingBold_77() const { return ___m_isUsingBold_77; }
	inline bool* get_address_of_m_isUsingBold_77() { return &___m_isUsingBold_77; }
	inline void set_m_isUsingBold_77(bool value)
	{
		___m_isUsingBold_77 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_78() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textAlignment_78)); }
	inline int32_t get_m_textAlignment_78() const { return ___m_textAlignment_78; }
	inline int32_t* get_address_of_m_textAlignment_78() { return &___m_textAlignment_78; }
	inline void set_m_textAlignment_78(int32_t value)
	{
		___m_textAlignment_78 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_79() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustification_79)); }
	inline int32_t get_m_lineJustification_79() const { return ___m_lineJustification_79; }
	inline int32_t* get_address_of_m_lineJustification_79() { return &___m_lineJustification_79; }
	inline void set_m_lineJustification_79(int32_t value)
	{
		___m_lineJustification_79 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_80() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineJustificationStack_80)); }
	inline TMP_XmlTagStack_1_t3600445780  get_m_lineJustificationStack_80() const { return ___m_lineJustificationStack_80; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_m_lineJustificationStack_80() { return &___m_lineJustificationStack_80; }
	inline void set_m_lineJustificationStack_80(TMP_XmlTagStack_1_t3600445780  value)
	{
		___m_lineJustificationStack_80 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_81() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textContainerLocalCorners_81)); }
	inline Vector3U5BU5D_t1718750761* get_m_textContainerLocalCorners_81() const { return ___m_textContainerLocalCorners_81; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_textContainerLocalCorners_81() { return &___m_textContainerLocalCorners_81; }
	inline void set_m_textContainerLocalCorners_81(Vector3U5BU5D_t1718750761* value)
	{
		___m_textContainerLocalCorners_81 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_81), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_82() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAlignmentEnumConverted_82)); }
	inline bool get_m_isAlignmentEnumConverted_82() const { return ___m_isAlignmentEnumConverted_82; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_82() { return &___m_isAlignmentEnumConverted_82; }
	inline void set_m_isAlignmentEnumConverted_82(bool value)
	{
		___m_isAlignmentEnumConverted_82 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_83() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterSpacing_83)); }
	inline float get_m_characterSpacing_83() const { return ___m_characterSpacing_83; }
	inline float* get_address_of_m_characterSpacing_83() { return &___m_characterSpacing_83; }
	inline void set_m_characterSpacing_83(float value)
	{
		___m_characterSpacing_83 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cSpacing_84)); }
	inline float get_m_cSpacing_84() const { return ___m_cSpacing_84; }
	inline float* get_address_of_m_cSpacing_84() { return &___m_cSpacing_84; }
	inline void set_m_cSpacing_84(float value)
	{
		___m_cSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_monoSpacing_85)); }
	inline float get_m_monoSpacing_85() const { return ___m_monoSpacing_85; }
	inline float* get_address_of_m_monoSpacing_85() { return &___m_monoSpacing_85; }
	inline void set_m_monoSpacing_85(float value)
	{
		___m_monoSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_86() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordSpacing_86)); }
	inline float get_m_wordSpacing_86() const { return ___m_wordSpacing_86; }
	inline float* get_address_of_m_wordSpacing_86() { return &___m_wordSpacing_86; }
	inline void set_m_wordSpacing_86(float value)
	{
		___m_wordSpacing_86 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_87() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacing_87)); }
	inline float get_m_lineSpacing_87() const { return ___m_lineSpacing_87; }
	inline float* get_address_of_m_lineSpacing_87() { return &___m_lineSpacing_87; }
	inline void set_m_lineSpacing_87(float value)
	{
		___m_lineSpacing_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_88() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingDelta_88)); }
	inline float get_m_lineSpacingDelta_88() const { return ___m_lineSpacingDelta_88; }
	inline float* get_address_of_m_lineSpacingDelta_88() { return &___m_lineSpacingDelta_88; }
	inline void set_m_lineSpacingDelta_88(float value)
	{
		___m_lineSpacingDelta_88 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_89() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineHeight_89)); }
	inline float get_m_lineHeight_89() const { return ___m_lineHeight_89; }
	inline float* get_address_of_m_lineHeight_89() { return &___m_lineHeight_89; }
	inline void set_m_lineHeight_89(float value)
	{
		___m_lineHeight_89 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_90() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineSpacingMax_90)); }
	inline float get_m_lineSpacingMax_90() const { return ___m_lineSpacingMax_90; }
	inline float* get_address_of_m_lineSpacingMax_90() { return &___m_lineSpacingMax_90; }
	inline void set_m_lineSpacingMax_90(float value)
	{
		___m_lineSpacingMax_90 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_91() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_paragraphSpacing_91)); }
	inline float get_m_paragraphSpacing_91() const { return ___m_paragraphSpacing_91; }
	inline float* get_address_of_m_paragraphSpacing_91() { return &___m_paragraphSpacing_91; }
	inline void set_m_paragraphSpacing_91(float value)
	{
		___m_paragraphSpacing_91 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_92() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthMaxAdj_92)); }
	inline float get_m_charWidthMaxAdj_92() const { return ___m_charWidthMaxAdj_92; }
	inline float* get_address_of_m_charWidthMaxAdj_92() { return &___m_charWidthMaxAdj_92; }
	inline void set_m_charWidthMaxAdj_92(float value)
	{
		___m_charWidthMaxAdj_92 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_93() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charWidthAdjDelta_93)); }
	inline float get_m_charWidthAdjDelta_93() const { return ___m_charWidthAdjDelta_93; }
	inline float* get_address_of_m_charWidthAdjDelta_93() { return &___m_charWidthAdjDelta_93; }
	inline void set_m_charWidthAdjDelta_93(float value)
	{
		___m_charWidthAdjDelta_93 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_94() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableWordWrapping_94)); }
	inline bool get_m_enableWordWrapping_94() const { return ___m_enableWordWrapping_94; }
	inline bool* get_address_of_m_enableWordWrapping_94() { return &___m_enableWordWrapping_94; }
	inline void set_m_enableWordWrapping_94(bool value)
	{
		___m_enableWordWrapping_94 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_95() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCharacterWrappingEnabled_95)); }
	inline bool get_m_isCharacterWrappingEnabled_95() const { return ___m_isCharacterWrappingEnabled_95; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_95() { return &___m_isCharacterWrappingEnabled_95; }
	inline void set_m_isCharacterWrappingEnabled_95(bool value)
	{
		___m_isCharacterWrappingEnabled_95 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_96() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNonBreakingSpace_96)); }
	inline bool get_m_isNonBreakingSpace_96() const { return ___m_isNonBreakingSpace_96; }
	inline bool* get_address_of_m_isNonBreakingSpace_96() { return &___m_isNonBreakingSpace_96; }
	inline void set_m_isNonBreakingSpace_96(bool value)
	{
		___m_isNonBreakingSpace_96 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_97() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isIgnoringAlignment_97)); }
	inline bool get_m_isIgnoringAlignment_97() const { return ___m_isIgnoringAlignment_97; }
	inline bool* get_address_of_m_isIgnoringAlignment_97() { return &___m_isIgnoringAlignment_97; }
	inline void set_m_isIgnoringAlignment_97(bool value)
	{
		___m_isIgnoringAlignment_97 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_98() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_wordWrappingRatios_98)); }
	inline float get_m_wordWrappingRatios_98() const { return ___m_wordWrappingRatios_98; }
	inline float* get_address_of_m_wordWrappingRatios_98() { return &___m_wordWrappingRatios_98; }
	inline void set_m_wordWrappingRatios_98(float value)
	{
		___m_wordWrappingRatios_98 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_99() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_overflowMode_99)); }
	inline int32_t get_m_overflowMode_99() const { return ___m_overflowMode_99; }
	inline int32_t* get_address_of_m_overflowMode_99() { return &___m_overflowMode_99; }
	inline void set_m_overflowMode_99(int32_t value)
	{
		___m_overflowMode_99 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_100() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstOverflowCharacterIndex_100)); }
	inline int32_t get_m_firstOverflowCharacterIndex_100() const { return ___m_firstOverflowCharacterIndex_100; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_100() { return &___m_firstOverflowCharacterIndex_100; }
	inline void set_m_firstOverflowCharacterIndex_100(int32_t value)
	{
		___m_firstOverflowCharacterIndex_100 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_101() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_linkedTextComponent_101)); }
	inline TMP_Text_t2599618874 * get_m_linkedTextComponent_101() const { return ___m_linkedTextComponent_101; }
	inline TMP_Text_t2599618874 ** get_address_of_m_linkedTextComponent_101() { return &___m_linkedTextComponent_101; }
	inline void set_m_linkedTextComponent_101(TMP_Text_t2599618874 * value)
	{
		___m_linkedTextComponent_101 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_101), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_102() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLinkedTextComponent_102)); }
	inline bool get_m_isLinkedTextComponent_102() const { return ___m_isLinkedTextComponent_102; }
	inline bool* get_address_of_m_isLinkedTextComponent_102() { return &___m_isLinkedTextComponent_102; }
	inline void set_m_isLinkedTextComponent_102(bool value)
	{
		___m_isLinkedTextComponent_102 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_103() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isTextTruncated_103)); }
	inline bool get_m_isTextTruncated_103() const { return ___m_isTextTruncated_103; }
	inline bool* get_address_of_m_isTextTruncated_103() { return &___m_isTextTruncated_103; }
	inline void set_m_isTextTruncated_103(bool value)
	{
		___m_isTextTruncated_103 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_104() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableKerning_104)); }
	inline bool get_m_enableKerning_104() const { return ___m_enableKerning_104; }
	inline bool* get_address_of_m_enableKerning_104() { return &___m_enableKerning_104; }
	inline void set_m_enableKerning_104(bool value)
	{
		___m_enableKerning_104 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_105() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_enableExtraPadding_105)); }
	inline bool get_m_enableExtraPadding_105() const { return ___m_enableExtraPadding_105; }
	inline bool* get_address_of_m_enableExtraPadding_105() { return &___m_enableExtraPadding_105; }
	inline void set_m_enableExtraPadding_105(bool value)
	{
		___m_enableExtraPadding_105 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_106() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___checkPaddingRequired_106)); }
	inline bool get_checkPaddingRequired_106() const { return ___checkPaddingRequired_106; }
	inline bool* get_address_of_checkPaddingRequired_106() { return &___checkPaddingRequired_106; }
	inline void set_checkPaddingRequired_106(bool value)
	{
		___checkPaddingRequired_106 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_107() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isRichText_107)); }
	inline bool get_m_isRichText_107() const { return ___m_isRichText_107; }
	inline bool* get_address_of_m_isRichText_107() { return &___m_isRichText_107; }
	inline void set_m_isRichText_107(bool value)
	{
		___m_isRichText_107 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_108() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_parseCtrlCharacters_108)); }
	inline bool get_m_parseCtrlCharacters_108() const { return ___m_parseCtrlCharacters_108; }
	inline bool* get_address_of_m_parseCtrlCharacters_108() { return &___m_parseCtrlCharacters_108; }
	inline void set_m_parseCtrlCharacters_108(bool value)
	{
		___m_parseCtrlCharacters_108 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_109() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOverlay_109)); }
	inline bool get_m_isOverlay_109() const { return ___m_isOverlay_109; }
	inline bool* get_address_of_m_isOverlay_109() { return &___m_isOverlay_109; }
	inline void set_m_isOverlay_109(bool value)
	{
		___m_isOverlay_109 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_110() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isOrthographic_110)); }
	inline bool get_m_isOrthographic_110() const { return ___m_isOrthographic_110; }
	inline bool* get_address_of_m_isOrthographic_110() { return &___m_isOrthographic_110; }
	inline void set_m_isOrthographic_110(bool value)
	{
		___m_isOrthographic_110 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_111() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCullingEnabled_111)); }
	inline bool get_m_isCullingEnabled_111() const { return ___m_isCullingEnabled_111; }
	inline bool* get_address_of_m_isCullingEnabled_111() { return &___m_isCullingEnabled_111; }
	inline void set_m_isCullingEnabled_111(bool value)
	{
		___m_isCullingEnabled_111 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_112() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreRectMaskCulling_112)); }
	inline bool get_m_ignoreRectMaskCulling_112() const { return ___m_ignoreRectMaskCulling_112; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_112() { return &___m_ignoreRectMaskCulling_112; }
	inline void set_m_ignoreRectMaskCulling_112(bool value)
	{
		___m_ignoreRectMaskCulling_112 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_113() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreCulling_113)); }
	inline bool get_m_ignoreCulling_113() const { return ___m_ignoreCulling_113; }
	inline bool* get_address_of_m_ignoreCulling_113() { return &___m_ignoreCulling_113; }
	inline void set_m_ignoreCulling_113(bool value)
	{
		___m_ignoreCulling_113 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_114() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_horizontalMapping_114)); }
	inline int32_t get_m_horizontalMapping_114() const { return ___m_horizontalMapping_114; }
	inline int32_t* get_address_of_m_horizontalMapping_114() { return &___m_horizontalMapping_114; }
	inline void set_m_horizontalMapping_114(int32_t value)
	{
		___m_horizontalMapping_114 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_115() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticalMapping_115)); }
	inline int32_t get_m_verticalMapping_115() const { return ___m_verticalMapping_115; }
	inline int32_t* get_address_of_m_verticalMapping_115() { return &___m_verticalMapping_115; }
	inline void set_m_verticalMapping_115(int32_t value)
	{
		___m_verticalMapping_115 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_116() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_uvLineOffset_116)); }
	inline float get_m_uvLineOffset_116() const { return ___m_uvLineOffset_116; }
	inline float* get_address_of_m_uvLineOffset_116() { return &___m_uvLineOffset_116; }
	inline void set_m_uvLineOffset_116(float value)
	{
		___m_uvLineOffset_116 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_117() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderMode_117)); }
	inline int32_t get_m_renderMode_117() const { return ___m_renderMode_117; }
	inline int32_t* get_address_of_m_renderMode_117() { return &___m_renderMode_117; }
	inline void set_m_renderMode_117(int32_t value)
	{
		___m_renderMode_117 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_118() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_geometrySortingOrder_118)); }
	inline int32_t get_m_geometrySortingOrder_118() const { return ___m_geometrySortingOrder_118; }
	inline int32_t* get_address_of_m_geometrySortingOrder_118() { return &___m_geometrySortingOrder_118; }
	inline void set_m_geometrySortingOrder_118(int32_t value)
	{
		___m_geometrySortingOrder_118 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_119() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacter_119)); }
	inline int32_t get_m_firstVisibleCharacter_119() const { return ___m_firstVisibleCharacter_119; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_119() { return &___m_firstVisibleCharacter_119; }
	inline void set_m_firstVisibleCharacter_119(int32_t value)
	{
		___m_firstVisibleCharacter_119 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_120() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleCharacters_120)); }
	inline int32_t get_m_maxVisibleCharacters_120() const { return ___m_maxVisibleCharacters_120; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_120() { return &___m_maxVisibleCharacters_120; }
	inline void set_m_maxVisibleCharacters_120(int32_t value)
	{
		___m_maxVisibleCharacters_120 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_121() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleWords_121)); }
	inline int32_t get_m_maxVisibleWords_121() const { return ___m_maxVisibleWords_121; }
	inline int32_t* get_address_of_m_maxVisibleWords_121() { return &___m_maxVisibleWords_121; }
	inline void set_m_maxVisibleWords_121(int32_t value)
	{
		___m_maxVisibleWords_121 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_122() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxVisibleLines_122)); }
	inline int32_t get_m_maxVisibleLines_122() const { return ___m_maxVisibleLines_122; }
	inline int32_t* get_address_of_m_maxVisibleLines_122() { return &___m_maxVisibleLines_122; }
	inline void set_m_maxVisibleLines_122(int32_t value)
	{
		___m_maxVisibleLines_122 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_123() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_useMaxVisibleDescender_123)); }
	inline bool get_m_useMaxVisibleDescender_123() const { return ___m_useMaxVisibleDescender_123; }
	inline bool* get_address_of_m_useMaxVisibleDescender_123() { return &___m_useMaxVisibleDescender_123; }
	inline void set_m_useMaxVisibleDescender_123(bool value)
	{
		___m_useMaxVisibleDescender_123 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_124() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageToDisplay_124)); }
	inline int32_t get_m_pageToDisplay_124() const { return ___m_pageToDisplay_124; }
	inline int32_t* get_address_of_m_pageToDisplay_124() { return &___m_pageToDisplay_124; }
	inline void set_m_pageToDisplay_124(int32_t value)
	{
		___m_pageToDisplay_124 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_125() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isNewPage_125)); }
	inline bool get_m_isNewPage_125() const { return ___m_isNewPage_125; }
	inline bool* get_address_of_m_isNewPage_125() { return &___m_isNewPage_125; }
	inline void set_m_isNewPage_125(bool value)
	{
		___m_isNewPage_125 = value;
	}

	inline static int32_t get_offset_of_m_margin_126() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_margin_126)); }
	inline Vector4_t3319028937  get_m_margin_126() const { return ___m_margin_126; }
	inline Vector4_t3319028937 * get_address_of_m_margin_126() { return &___m_margin_126; }
	inline void set_m_margin_126(Vector4_t3319028937  value)
	{
		___m_margin_126 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_127() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginLeft_127)); }
	inline float get_m_marginLeft_127() const { return ___m_marginLeft_127; }
	inline float* get_address_of_m_marginLeft_127() { return &___m_marginLeft_127; }
	inline void set_m_marginLeft_127(float value)
	{
		___m_marginLeft_127 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_128() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginRight_128)); }
	inline float get_m_marginRight_128() const { return ___m_marginRight_128; }
	inline float* get_address_of_m_marginRight_128() { return &___m_marginRight_128; }
	inline void set_m_marginRight_128(float value)
	{
		___m_marginRight_128 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_129() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginWidth_129)); }
	inline float get_m_marginWidth_129() const { return ___m_marginWidth_129; }
	inline float* get_address_of_m_marginWidth_129() { return &___m_marginWidth_129; }
	inline void set_m_marginWidth_129(float value)
	{
		___m_marginWidth_129 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_130() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_marginHeight_130)); }
	inline float get_m_marginHeight_130() const { return ___m_marginHeight_130; }
	inline float* get_address_of_m_marginHeight_130() { return &___m_marginHeight_130; }
	inline void set_m_marginHeight_130(float value)
	{
		___m_marginHeight_130 = value;
	}

	inline static int32_t get_offset_of_m_width_131() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_width_131)); }
	inline float get_m_width_131() const { return ___m_width_131; }
	inline float* get_address_of_m_width_131() { return &___m_width_131; }
	inline void set_m_width_131(float value)
	{
		___m_width_131 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_132() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textInfo_132)); }
	inline TMP_TextInfo_t3598145122 * get_m_textInfo_132() const { return ___m_textInfo_132; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_m_textInfo_132() { return &___m_textInfo_132; }
	inline void set_m_textInfo_132(TMP_TextInfo_t3598145122 * value)
	{
		___m_textInfo_132 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_132), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_133() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_havePropertiesChanged_133)); }
	inline bool get_m_havePropertiesChanged_133() const { return ___m_havePropertiesChanged_133; }
	inline bool* get_address_of_m_havePropertiesChanged_133() { return &___m_havePropertiesChanged_133; }
	inline void set_m_havePropertiesChanged_133(bool value)
	{
		___m_havePropertiesChanged_133 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_134() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isUsingLegacyAnimationComponent_134)); }
	inline bool get_m_isUsingLegacyAnimationComponent_134() const { return ___m_isUsingLegacyAnimationComponent_134; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_134() { return &___m_isUsingLegacyAnimationComponent_134; }
	inline void set_m_isUsingLegacyAnimationComponent_134(bool value)
	{
		___m_isUsingLegacyAnimationComponent_134 = value;
	}

	inline static int32_t get_offset_of_m_transform_135() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_transform_135)); }
	inline Transform_t3600365921 * get_m_transform_135() const { return ___m_transform_135; }
	inline Transform_t3600365921 ** get_address_of_m_transform_135() { return &___m_transform_135; }
	inline void set_m_transform_135(Transform_t3600365921 * value)
	{
		___m_transform_135 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_135), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_136() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_rectTransform_136)); }
	inline RectTransform_t3704657025 * get_m_rectTransform_136() const { return ___m_rectTransform_136; }
	inline RectTransform_t3704657025 ** get_address_of_m_rectTransform_136() { return &___m_rectTransform_136; }
	inline void set_m_rectTransform_136(RectTransform_t3704657025 * value)
	{
		___m_rectTransform_136 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_136), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_137() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___U3CautoSizeTextContainerU3Ek__BackingField_137)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_137() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_137; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_137() { return &___U3CautoSizeTextContainerU3Ek__BackingField_137; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_137(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_137 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_138() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_autoSizeTextContainer_138)); }
	inline bool get_m_autoSizeTextContainer_138() const { return ___m_autoSizeTextContainer_138; }
	inline bool* get_address_of_m_autoSizeTextContainer_138() { return &___m_autoSizeTextContainer_138; }
	inline void set_m_autoSizeTextContainer_138(bool value)
	{
		___m_autoSizeTextContainer_138 = value;
	}

	inline static int32_t get_offset_of_m_mesh_139() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_mesh_139)); }
	inline Mesh_t3648964284 * get_m_mesh_139() const { return ___m_mesh_139; }
	inline Mesh_t3648964284 ** get_address_of_m_mesh_139() { return &___m_mesh_139; }
	inline void set_m_mesh_139(Mesh_t3648964284 * value)
	{
		___m_mesh_139 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_139), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_140() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isVolumetricText_140)); }
	inline bool get_m_isVolumetricText_140() const { return ___m_isVolumetricText_140; }
	inline bool* get_address_of_m_isVolumetricText_140() { return &___m_isVolumetricText_140; }
	inline void set_m_isVolumetricText_140(bool value)
	{
		___m_isVolumetricText_140 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_141() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimator_141)); }
	inline TMP_SpriteAnimator_t2836635477 * get_m_spriteAnimator_141() const { return ___m_spriteAnimator_141; }
	inline TMP_SpriteAnimator_t2836635477 ** get_address_of_m_spriteAnimator_141() { return &___m_spriteAnimator_141; }
	inline void set_m_spriteAnimator_141(TMP_SpriteAnimator_t2836635477 * value)
	{
		___m_spriteAnimator_141 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_141), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_142() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleHeight_142)); }
	inline float get_m_flexibleHeight_142() const { return ___m_flexibleHeight_142; }
	inline float* get_address_of_m_flexibleHeight_142() { return &___m_flexibleHeight_142; }
	inline void set_m_flexibleHeight_142(float value)
	{
		___m_flexibleHeight_142 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_143() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_flexibleWidth_143)); }
	inline float get_m_flexibleWidth_143() const { return ___m_flexibleWidth_143; }
	inline float* get_address_of_m_flexibleWidth_143() { return &___m_flexibleWidth_143; }
	inline void set_m_flexibleWidth_143(float value)
	{
		___m_flexibleWidth_143 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minWidth_144)); }
	inline float get_m_minWidth_144() const { return ___m_minWidth_144; }
	inline float* get_address_of_m_minWidth_144() { return &___m_minWidth_144; }
	inline void set_m_minWidth_144(float value)
	{
		___m_minWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_145() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_minHeight_145)); }
	inline float get_m_minHeight_145() const { return ___m_minHeight_145; }
	inline float* get_address_of_m_minHeight_145() { return &___m_minHeight_145; }
	inline void set_m_minHeight_145(float value)
	{
		___m_minHeight_145 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_146() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxWidth_146)); }
	inline float get_m_maxWidth_146() const { return ___m_maxWidth_146; }
	inline float* get_address_of_m_maxWidth_146() { return &___m_maxWidth_146; }
	inline void set_m_maxWidth_146(float value)
	{
		___m_maxWidth_146 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_147() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxHeight_147)); }
	inline float get_m_maxHeight_147() const { return ___m_maxHeight_147; }
	inline float* get_address_of_m_maxHeight_147() { return &___m_maxHeight_147; }
	inline void set_m_maxHeight_147(float value)
	{
		___m_maxHeight_147 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_148() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_LayoutElement_148)); }
	inline LayoutElement_t1785403678 * get_m_LayoutElement_148() const { return ___m_LayoutElement_148; }
	inline LayoutElement_t1785403678 ** get_address_of_m_LayoutElement_148() { return &___m_LayoutElement_148; }
	inline void set_m_LayoutElement_148(LayoutElement_t1785403678 * value)
	{
		___m_LayoutElement_148 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_148), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_149() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredWidth_149)); }
	inline float get_m_preferredWidth_149() const { return ___m_preferredWidth_149; }
	inline float* get_address_of_m_preferredWidth_149() { return &___m_preferredWidth_149; }
	inline void set_m_preferredWidth_149(float value)
	{
		___m_preferredWidth_149 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_150() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedWidth_150)); }
	inline float get_m_renderedWidth_150() const { return ___m_renderedWidth_150; }
	inline float* get_address_of_m_renderedWidth_150() { return &___m_renderedWidth_150; }
	inline void set_m_renderedWidth_150(float value)
	{
		___m_renderedWidth_150 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_151() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredWidthDirty_151)); }
	inline bool get_m_isPreferredWidthDirty_151() const { return ___m_isPreferredWidthDirty_151; }
	inline bool* get_address_of_m_isPreferredWidthDirty_151() { return &___m_isPreferredWidthDirty_151; }
	inline void set_m_isPreferredWidthDirty_151(bool value)
	{
		___m_isPreferredWidthDirty_151 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_152() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_preferredHeight_152)); }
	inline float get_m_preferredHeight_152() const { return ___m_preferredHeight_152; }
	inline float* get_address_of_m_preferredHeight_152() { return &___m_preferredHeight_152; }
	inline void set_m_preferredHeight_152(float value)
	{
		___m_preferredHeight_152 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_153() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_renderedHeight_153)); }
	inline float get_m_renderedHeight_153() const { return ___m_renderedHeight_153; }
	inline float* get_address_of_m_renderedHeight_153() { return &___m_renderedHeight_153; }
	inline void set_m_renderedHeight_153(float value)
	{
		___m_renderedHeight_153 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_154() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isPreferredHeightDirty_154)); }
	inline bool get_m_isPreferredHeightDirty_154() const { return ___m_isPreferredHeightDirty_154; }
	inline bool* get_address_of_m_isPreferredHeightDirty_154() { return &___m_isPreferredHeightDirty_154; }
	inline void set_m_isPreferredHeightDirty_154(bool value)
	{
		___m_isPreferredHeightDirty_154 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_155() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculatingPreferredValues_155)); }
	inline bool get_m_isCalculatingPreferredValues_155() const { return ___m_isCalculatingPreferredValues_155; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_155() { return &___m_isCalculatingPreferredValues_155; }
	inline void set_m_isCalculatingPreferredValues_155(bool value)
	{
		___m_isCalculatingPreferredValues_155 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_156() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_recursiveCount_156)); }
	inline int32_t get_m_recursiveCount_156() const { return ___m_recursiveCount_156; }
	inline int32_t* get_address_of_m_recursiveCount_156() { return &___m_recursiveCount_156; }
	inline void set_m_recursiveCount_156(int32_t value)
	{
		___m_recursiveCount_156 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_157() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutPriority_157)); }
	inline int32_t get_m_layoutPriority_157() const { return ___m_layoutPriority_157; }
	inline int32_t* get_address_of_m_layoutPriority_157() { return &___m_layoutPriority_157; }
	inline void set_m_layoutPriority_157(int32_t value)
	{
		___m_layoutPriority_157 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_158() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isCalculateSizeRequired_158)); }
	inline bool get_m_isCalculateSizeRequired_158() const { return ___m_isCalculateSizeRequired_158; }
	inline bool* get_address_of_m_isCalculateSizeRequired_158() { return &___m_isCalculateSizeRequired_158; }
	inline void set_m_isCalculateSizeRequired_158(bool value)
	{
		___m_isCalculateSizeRequired_158 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_159() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isLayoutDirty_159)); }
	inline bool get_m_isLayoutDirty_159() const { return ___m_isLayoutDirty_159; }
	inline bool* get_address_of_m_isLayoutDirty_159() { return &___m_isLayoutDirty_159; }
	inline void set_m_isLayoutDirty_159(bool value)
	{
		___m_isLayoutDirty_159 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_160() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_verticesAlreadyDirty_160)); }
	inline bool get_m_verticesAlreadyDirty_160() const { return ___m_verticesAlreadyDirty_160; }
	inline bool* get_address_of_m_verticesAlreadyDirty_160() { return &___m_verticesAlreadyDirty_160; }
	inline void set_m_verticesAlreadyDirty_160(bool value)
	{
		___m_verticesAlreadyDirty_160 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_161() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_layoutAlreadyDirty_161)); }
	inline bool get_m_layoutAlreadyDirty_161() const { return ___m_layoutAlreadyDirty_161; }
	inline bool* get_address_of_m_layoutAlreadyDirty_161() { return &___m_layoutAlreadyDirty_161; }
	inline void set_m_layoutAlreadyDirty_161(bool value)
	{
		___m_layoutAlreadyDirty_161 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_162() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isAwake_162)); }
	inline bool get_m_isAwake_162() const { return ___m_isAwake_162; }
	inline bool* get_address_of_m_isAwake_162() { return &___m_isAwake_162; }
	inline void set_m_isAwake_162(bool value)
	{
		___m_isAwake_162 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_163() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isWaitingOnResourceLoad_163)); }
	inline bool get_m_isWaitingOnResourceLoad_163() const { return ___m_isWaitingOnResourceLoad_163; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_163() { return &___m_isWaitingOnResourceLoad_163; }
	inline void set_m_isWaitingOnResourceLoad_163(bool value)
	{
		___m_isWaitingOnResourceLoad_163 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_164() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isInputParsingRequired_164)); }
	inline bool get_m_isInputParsingRequired_164() const { return ___m_isInputParsingRequired_164; }
	inline bool* get_address_of_m_isInputParsingRequired_164() { return &___m_isInputParsingRequired_164; }
	inline void set_m_isInputParsingRequired_164(bool value)
	{
		___m_isInputParsingRequired_164 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_165() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_inputSource_165)); }
	inline int32_t get_m_inputSource_165() const { return ___m_inputSource_165; }
	inline int32_t* get_address_of_m_inputSource_165() { return &___m_inputSource_165; }
	inline void set_m_inputSource_165(int32_t value)
	{
		___m_inputSource_165 = value;
	}

	inline static int32_t get_offset_of_old_text_166() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___old_text_166)); }
	inline String_t* get_old_text_166() const { return ___old_text_166; }
	inline String_t** get_address_of_old_text_166() { return &___old_text_166; }
	inline void set_old_text_166(String_t* value)
	{
		___old_text_166 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_166), value);
	}

	inline static int32_t get_offset_of_m_fontScale_167() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScale_167)); }
	inline float get_m_fontScale_167() const { return ___m_fontScale_167; }
	inline float* get_address_of_m_fontScale_167() { return &___m_fontScale_167; }
	inline void set_m_fontScale_167(float value)
	{
		___m_fontScale_167 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_168() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_fontScaleMultiplier_168)); }
	inline float get_m_fontScaleMultiplier_168() const { return ___m_fontScaleMultiplier_168; }
	inline float* get_address_of_m_fontScaleMultiplier_168() { return &___m_fontScaleMultiplier_168; }
	inline void set_m_fontScaleMultiplier_168(float value)
	{
		___m_fontScaleMultiplier_168 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_169() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlTag_169)); }
	inline CharU5BU5D_t3528271667* get_m_htmlTag_169() const { return ___m_htmlTag_169; }
	inline CharU5BU5D_t3528271667** get_address_of_m_htmlTag_169() { return &___m_htmlTag_169; }
	inline void set_m_htmlTag_169(CharU5BU5D_t3528271667* value)
	{
		___m_htmlTag_169 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_169), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_170() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xmlAttribute_170)); }
	inline XML_TagAttributeU5BU5D_t284240280* get_m_xmlAttribute_170() const { return ___m_xmlAttribute_170; }
	inline XML_TagAttributeU5BU5D_t284240280** get_address_of_m_xmlAttribute_170() { return &___m_xmlAttribute_170; }
	inline void set_m_xmlAttribute_170(XML_TagAttributeU5BU5D_t284240280* value)
	{
		___m_xmlAttribute_170 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_170), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_171() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_attributeParameterValues_171)); }
	inline SingleU5BU5D_t1444911251* get_m_attributeParameterValues_171() const { return ___m_attributeParameterValues_171; }
	inline SingleU5BU5D_t1444911251** get_address_of_m_attributeParameterValues_171() { return &___m_attributeParameterValues_171; }
	inline void set_m_attributeParameterValues_171(SingleU5BU5D_t1444911251* value)
	{
		___m_attributeParameterValues_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_171), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_172() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_LineIndent_172)); }
	inline float get_tag_LineIndent_172() const { return ___tag_LineIndent_172; }
	inline float* get_address_of_tag_LineIndent_172() { return &___tag_LineIndent_172; }
	inline void set_tag_LineIndent_172(float value)
	{
		___tag_LineIndent_172 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_173() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_Indent_173)); }
	inline float get_tag_Indent_173() const { return ___tag_Indent_173; }
	inline float* get_address_of_tag_Indent_173() { return &___tag_Indent_173; }
	inline void set_tag_Indent_173(float value)
	{
		___tag_Indent_173 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_174() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_indentStack_174)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_indentStack_174() const { return ___m_indentStack_174; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_indentStack_174() { return &___m_indentStack_174; }
	inline void set_m_indentStack_174(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_indentStack_174 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_175() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___tag_NoParsing_175)); }
	inline bool get_tag_NoParsing_175() const { return ___tag_NoParsing_175; }
	inline bool* get_address_of_tag_NoParsing_175() { return &___tag_NoParsing_175; }
	inline void set_tag_NoParsing_175(bool value)
	{
		___tag_NoParsing_175 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_176() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isParsingText_176)); }
	inline bool get_m_isParsingText_176() const { return ___m_isParsingText_176; }
	inline bool* get_address_of_m_isParsingText_176() { return &___m_isParsingText_176; }
	inline void set_m_isParsingText_176(bool value)
	{
		___m_isParsingText_176 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_177() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_FXMatrix_177)); }
	inline Matrix4x4_t1817901843  get_m_FXMatrix_177() const { return ___m_FXMatrix_177; }
	inline Matrix4x4_t1817901843 * get_address_of_m_FXMatrix_177() { return &___m_FXMatrix_177; }
	inline void set_m_FXMatrix_177(Matrix4x4_t1817901843  value)
	{
		___m_FXMatrix_177 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_178() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_isFXMatrixSet_178)); }
	inline bool get_m_isFXMatrixSet_178() const { return ___m_isFXMatrixSet_178; }
	inline bool* get_address_of_m_isFXMatrixSet_178() { return &___m_isFXMatrixSet_178; }
	inline void set_m_isFXMatrixSet_178(bool value)
	{
		___m_isFXMatrixSet_178 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_179() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_char_buffer_179)); }
	inline Int32U5BU5D_t385246372* get_m_char_buffer_179() const { return ___m_char_buffer_179; }
	inline Int32U5BU5D_t385246372** get_address_of_m_char_buffer_179() { return &___m_char_buffer_179; }
	inline void set_m_char_buffer_179(Int32U5BU5D_t385246372* value)
	{
		___m_char_buffer_179 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_179), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_180() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_internalCharacterInfo_180)); }
	inline TMP_CharacterInfoU5BU5D_t1930184704* get_m_internalCharacterInfo_180() const { return ___m_internalCharacterInfo_180; }
	inline TMP_CharacterInfoU5BU5D_t1930184704** get_address_of_m_internalCharacterInfo_180() { return &___m_internalCharacterInfo_180; }
	inline void set_m_internalCharacterInfo_180(TMP_CharacterInfoU5BU5D_t1930184704* value)
	{
		___m_internalCharacterInfo_180 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_180), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_181() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_input_CharArray_181)); }
	inline CharU5BU5D_t3528271667* get_m_input_CharArray_181() const { return ___m_input_CharArray_181; }
	inline CharU5BU5D_t3528271667** get_address_of_m_input_CharArray_181() { return &___m_input_CharArray_181; }
	inline void set_m_input_CharArray_181(CharU5BU5D_t3528271667* value)
	{
		___m_input_CharArray_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_181), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_182() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_charArray_Length_182)); }
	inline int32_t get_m_charArray_Length_182() const { return ___m_charArray_Length_182; }
	inline int32_t* get_address_of_m_charArray_Length_182() { return &___m_charArray_Length_182; }
	inline void set_m_charArray_Length_182(int32_t value)
	{
		___m_charArray_Length_182 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_183() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_totalCharacterCount_183)); }
	inline int32_t get_m_totalCharacterCount_183() const { return ___m_totalCharacterCount_183; }
	inline int32_t* get_address_of_m_totalCharacterCount_183() { return &___m_totalCharacterCount_183; }
	inline void set_m_totalCharacterCount_183(int32_t value)
	{
		___m_totalCharacterCount_183 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_184() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedWordWrapState_184)); }
	inline WordWrapState_t341939652  get_m_SavedWordWrapState_184() const { return ___m_SavedWordWrapState_184; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedWordWrapState_184() { return &___m_SavedWordWrapState_184; }
	inline void set_m_SavedWordWrapState_184(WordWrapState_t341939652  value)
	{
		___m_SavedWordWrapState_184 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_185() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_SavedLineState_185)); }
	inline WordWrapState_t341939652  get_m_SavedLineState_185() const { return ___m_SavedLineState_185; }
	inline WordWrapState_t341939652 * get_address_of_m_SavedLineState_185() { return &___m_SavedLineState_185; }
	inline void set_m_SavedLineState_185(WordWrapState_t341939652  value)
	{
		___m_SavedLineState_185 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_186() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_characterCount_186)); }
	inline int32_t get_m_characterCount_186() const { return ___m_characterCount_186; }
	inline int32_t* get_address_of_m_characterCount_186() { return &___m_characterCount_186; }
	inline void set_m_characterCount_186(int32_t value)
	{
		___m_characterCount_186 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_187() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstCharacterOfLine_187)); }
	inline int32_t get_m_firstCharacterOfLine_187() const { return ___m_firstCharacterOfLine_187; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_187() { return &___m_firstCharacterOfLine_187; }
	inline void set_m_firstCharacterOfLine_187(int32_t value)
	{
		___m_firstCharacterOfLine_187 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_188() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_firstVisibleCharacterOfLine_188)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_188() const { return ___m_firstVisibleCharacterOfLine_188; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_188() { return &___m_firstVisibleCharacterOfLine_188; }
	inline void set_m_firstVisibleCharacterOfLine_188(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_188 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_189() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastCharacterOfLine_189)); }
	inline int32_t get_m_lastCharacterOfLine_189() const { return ___m_lastCharacterOfLine_189; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_189() { return &___m_lastCharacterOfLine_189; }
	inline void set_m_lastCharacterOfLine_189(int32_t value)
	{
		___m_lastCharacterOfLine_189 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_190() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lastVisibleCharacterOfLine_190)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_190() const { return ___m_lastVisibleCharacterOfLine_190; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_190() { return &___m_lastVisibleCharacterOfLine_190; }
	inline void set_m_lastVisibleCharacterOfLine_190(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_190 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_191() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineNumber_191)); }
	inline int32_t get_m_lineNumber_191() const { return ___m_lineNumber_191; }
	inline int32_t* get_address_of_m_lineNumber_191() { return &___m_lineNumber_191; }
	inline void set_m_lineNumber_191(int32_t value)
	{
		___m_lineNumber_191 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_192() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineVisibleCharacterCount_192)); }
	inline int32_t get_m_lineVisibleCharacterCount_192() const { return ___m_lineVisibleCharacterCount_192; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_192() { return &___m_lineVisibleCharacterCount_192; }
	inline void set_m_lineVisibleCharacterCount_192(int32_t value)
	{
		___m_lineVisibleCharacterCount_192 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_193() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_pageNumber_193)); }
	inline int32_t get_m_pageNumber_193() const { return ___m_pageNumber_193; }
	inline int32_t* get_address_of_m_pageNumber_193() { return &___m_pageNumber_193; }
	inline void set_m_pageNumber_193(int32_t value)
	{
		___m_pageNumber_193 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_194() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxAscender_194)); }
	inline float get_m_maxAscender_194() const { return ___m_maxAscender_194; }
	inline float* get_address_of_m_maxAscender_194() { return &___m_maxAscender_194; }
	inline void set_m_maxAscender_194(float value)
	{
		___m_maxAscender_194 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_195() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxCapHeight_195)); }
	inline float get_m_maxCapHeight_195() const { return ___m_maxCapHeight_195; }
	inline float* get_address_of_m_maxCapHeight_195() { return &___m_maxCapHeight_195; }
	inline void set_m_maxCapHeight_195(float value)
	{
		___m_maxCapHeight_195 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxDescender_196)); }
	inline float get_m_maxDescender_196() const { return ___m_maxDescender_196; }
	inline float* get_address_of_m_maxDescender_196() { return &___m_maxDescender_196; }
	inline void set_m_maxDescender_196(float value)
	{
		___m_maxDescender_196 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_197() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineAscender_197)); }
	inline float get_m_maxLineAscender_197() const { return ___m_maxLineAscender_197; }
	inline float* get_address_of_m_maxLineAscender_197() { return &___m_maxLineAscender_197; }
	inline void set_m_maxLineAscender_197(float value)
	{
		___m_maxLineAscender_197 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_198() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_maxLineDescender_198)); }
	inline float get_m_maxLineDescender_198() const { return ___m_maxLineDescender_198; }
	inline float* get_address_of_m_maxLineDescender_198() { return &___m_maxLineDescender_198; }
	inline void set_m_maxLineDescender_198(float value)
	{
		___m_maxLineDescender_198 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_199() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_startOfLineAscender_199)); }
	inline float get_m_startOfLineAscender_199() const { return ___m_startOfLineAscender_199; }
	inline float* get_address_of_m_startOfLineAscender_199() { return &___m_startOfLineAscender_199; }
	inline void set_m_startOfLineAscender_199(float value)
	{
		___m_startOfLineAscender_199 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_200() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_lineOffset_200)); }
	inline float get_m_lineOffset_200() const { return ___m_lineOffset_200; }
	inline float* get_address_of_m_lineOffset_200() { return &___m_lineOffset_200; }
	inline void set_m_lineOffset_200(float value)
	{
		___m_lineOffset_200 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_201() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_meshExtents_201)); }
	inline Extents_t3837212874  get_m_meshExtents_201() const { return ___m_meshExtents_201; }
	inline Extents_t3837212874 * get_address_of_m_meshExtents_201() { return &___m_meshExtents_201; }
	inline void set_m_meshExtents_201(Extents_t3837212874  value)
	{
		___m_meshExtents_201 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_202() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_htmlColor_202)); }
	inline Color32_t2600501292  get_m_htmlColor_202() const { return ___m_htmlColor_202; }
	inline Color32_t2600501292 * get_address_of_m_htmlColor_202() { return &___m_htmlColor_202; }
	inline void set_m_htmlColor_202(Color32_t2600501292  value)
	{
		___m_htmlColor_202 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_203() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorStack_203)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_colorStack_203() const { return ___m_colorStack_203; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_colorStack_203() { return &___m_colorStack_203; }
	inline void set_m_colorStack_203(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_colorStack_203 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_204() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_underlineColorStack_204)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_underlineColorStack_204() const { return ___m_underlineColorStack_204; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_underlineColorStack_204() { return &___m_underlineColorStack_204; }
	inline void set_m_underlineColorStack_204(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_underlineColorStack_204 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_strikethroughColorStack_205)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_strikethroughColorStack_205() const { return ___m_strikethroughColorStack_205; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_strikethroughColorStack_205() { return &___m_strikethroughColorStack_205; }
	inline void set_m_strikethroughColorStack_205(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_strikethroughColorStack_205 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_highlightColorStack_206)); }
	inline TMP_XmlTagStack_1_t2164155836  get_m_highlightColorStack_206() const { return ___m_highlightColorStack_206; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_m_highlightColorStack_206() { return &___m_highlightColorStack_206; }
	inline void set_m_highlightColorStack_206(TMP_XmlTagStack_1_t2164155836  value)
	{
		___m_highlightColorStack_206 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_207() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientPreset_207)); }
	inline TMP_ColorGradient_t3678055768 * get_m_colorGradientPreset_207() const { return ___m_colorGradientPreset_207; }
	inline TMP_ColorGradient_t3678055768 ** get_address_of_m_colorGradientPreset_207() { return &___m_colorGradientPreset_207; }
	inline void set_m_colorGradientPreset_207(TMP_ColorGradient_t3678055768 * value)
	{
		___m_colorGradientPreset_207 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_207), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_colorGradientStack_208)); }
	inline TMP_XmlTagStack_1_t3241710312  get_m_colorGradientStack_208() const { return ___m_colorGradientStack_208; }
	inline TMP_XmlTagStack_1_t3241710312 * get_address_of_m_colorGradientStack_208() { return &___m_colorGradientStack_208; }
	inline void set_m_colorGradientStack_208(TMP_XmlTagStack_1_t3241710312  value)
	{
		___m_colorGradientStack_208 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_209() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_tabSpacing_209)); }
	inline float get_m_tabSpacing_209() const { return ___m_tabSpacing_209; }
	inline float* get_address_of_m_tabSpacing_209() { return &___m_tabSpacing_209; }
	inline void set_m_tabSpacing_209(float value)
	{
		___m_tabSpacing_209 = value;
	}

	inline static int32_t get_offset_of_m_spacing_210() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spacing_210)); }
	inline float get_m_spacing_210() const { return ___m_spacing_210; }
	inline float* get_address_of_m_spacing_210() { return &___m_spacing_210; }
	inline void set_m_spacing_210(float value)
	{
		___m_spacing_210 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_211() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_styleStack_211)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_styleStack_211() const { return ___m_styleStack_211; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_styleStack_211() { return &___m_styleStack_211; }
	inline void set_m_styleStack_211(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_styleStack_211 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_212() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_actionStack_212)); }
	inline TMP_XmlTagStack_1_t2514600297  get_m_actionStack_212() const { return ___m_actionStack_212; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_m_actionStack_212() { return &___m_actionStack_212; }
	inline void set_m_actionStack_212(TMP_XmlTagStack_1_t2514600297  value)
	{
		___m_actionStack_212 = value;
	}

	inline static int32_t get_offset_of_m_padding_213() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_padding_213)); }
	inline float get_m_padding_213() const { return ___m_padding_213; }
	inline float* get_address_of_m_padding_213() { return &___m_padding_213; }
	inline void set_m_padding_213(float value)
	{
		___m_padding_213 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_214() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffset_214)); }
	inline float get_m_baselineOffset_214() const { return ___m_baselineOffset_214; }
	inline float* get_address_of_m_baselineOffset_214() { return &___m_baselineOffset_214; }
	inline void set_m_baselineOffset_214(float value)
	{
		___m_baselineOffset_214 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_215() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_baselineOffsetStack_215)); }
	inline TMP_XmlTagStack_1_t960921318  get_m_baselineOffsetStack_215() const { return ___m_baselineOffsetStack_215; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_m_baselineOffsetStack_215() { return &___m_baselineOffsetStack_215; }
	inline void set_m_baselineOffsetStack_215(TMP_XmlTagStack_1_t960921318  value)
	{
		___m_baselineOffsetStack_215 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_216() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_xAdvance_216)); }
	inline float get_m_xAdvance_216() const { return ___m_xAdvance_216; }
	inline float* get_address_of_m_xAdvance_216() { return &___m_xAdvance_216; }
	inline void set_m_xAdvance_216(float value)
	{
		___m_xAdvance_216 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_217() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_textElementType_217)); }
	inline int32_t get_m_textElementType_217() const { return ___m_textElementType_217; }
	inline int32_t* get_address_of_m_textElementType_217() { return &___m_textElementType_217; }
	inline void set_m_textElementType_217(int32_t value)
	{
		___m_textElementType_217 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_218() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_TextElement_218)); }
	inline TMP_TextElement_t129727469 * get_m_cached_TextElement_218() const { return ___m_cached_TextElement_218; }
	inline TMP_TextElement_t129727469 ** get_address_of_m_cached_TextElement_218() { return &___m_cached_TextElement_218; }
	inline void set_m_cached_TextElement_218(TMP_TextElement_t129727469 * value)
	{
		___m_cached_TextElement_218 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_218), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_219() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Underline_GlyphInfo_219)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Underline_GlyphInfo_219() const { return ___m_cached_Underline_GlyphInfo_219; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Underline_GlyphInfo_219() { return &___m_cached_Underline_GlyphInfo_219; }
	inline void set_m_cached_Underline_GlyphInfo_219(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Underline_GlyphInfo_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_219), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_220() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_cached_Ellipsis_GlyphInfo_220)); }
	inline TMP_Glyph_t581847833 * get_m_cached_Ellipsis_GlyphInfo_220() const { return ___m_cached_Ellipsis_GlyphInfo_220; }
	inline TMP_Glyph_t581847833 ** get_address_of_m_cached_Ellipsis_GlyphInfo_220() { return &___m_cached_Ellipsis_GlyphInfo_220; }
	inline void set_m_cached_Ellipsis_GlyphInfo_220(TMP_Glyph_t581847833 * value)
	{
		___m_cached_Ellipsis_GlyphInfo_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_220), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_221() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_defaultSpriteAsset_221)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_221() const { return ___m_defaultSpriteAsset_221; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_221() { return &___m_defaultSpriteAsset_221; }
	inline void set_m_defaultSpriteAsset_221(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_221), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_222() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_currentSpriteAsset_222)); }
	inline TMP_SpriteAsset_t484820633 * get_m_currentSpriteAsset_222() const { return ___m_currentSpriteAsset_222; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_currentSpriteAsset_222() { return &___m_currentSpriteAsset_222; }
	inline void set_m_currentSpriteAsset_222(TMP_SpriteAsset_t484820633 * value)
	{
		___m_currentSpriteAsset_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_222), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_223() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteCount_223)); }
	inline int32_t get_m_spriteCount_223() const { return ___m_spriteCount_223; }
	inline int32_t* get_address_of_m_spriteCount_223() { return &___m_spriteCount_223; }
	inline void set_m_spriteCount_223(int32_t value)
	{
		___m_spriteCount_223 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_224() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteIndex_224)); }
	inline int32_t get_m_spriteIndex_224() const { return ___m_spriteIndex_224; }
	inline int32_t* get_address_of_m_spriteIndex_224() { return &___m_spriteIndex_224; }
	inline void set_m_spriteIndex_224(int32_t value)
	{
		___m_spriteIndex_224 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_225() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_spriteAnimationID_225)); }
	inline int32_t get_m_spriteAnimationID_225() const { return ___m_spriteAnimationID_225; }
	inline int32_t* get_address_of_m_spriteAnimationID_225() { return &___m_spriteAnimationID_225; }
	inline void set_m_spriteAnimationID_225(int32_t value)
	{
		___m_spriteAnimationID_225 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_226() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___m_ignoreActiveState_226)); }
	inline bool get_m_ignoreActiveState_226() const { return ___m_ignoreActiveState_226; }
	inline bool* get_address_of_m_ignoreActiveState_226() { return &___m_ignoreActiveState_226; }
	inline void set_m_ignoreActiveState_226(bool value)
	{
		___m_ignoreActiveState_226 = value;
	}

	inline static int32_t get_offset_of_k_Power_227() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874, ___k_Power_227)); }
	inline SingleU5BU5D_t1444911251* get_k_Power_227() const { return ___k_Power_227; }
	inline SingleU5BU5D_t1444911251** get_address_of_k_Power_227() { return &___k_Power_227; }
	inline void set_k_Power_227(SingleU5BU5D_t1444911251* value)
	{
		___k_Power_227 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_227), value);
	}
};

struct TMP_Text_t2599618874_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t2600501292  ___s_colorWhite_47;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t2156229523  ___k_LargePositiveVector2_228;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t2156229523  ___k_LargeNegativeVector2_229;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_230;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_231;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_232;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_233;

public:
	inline static int32_t get_offset_of_s_colorWhite_47() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___s_colorWhite_47)); }
	inline Color32_t2600501292  get_s_colorWhite_47() const { return ___s_colorWhite_47; }
	inline Color32_t2600501292 * get_address_of_s_colorWhite_47() { return &___s_colorWhite_47; }
	inline void set_s_colorWhite_47(Color32_t2600501292  value)
	{
		___s_colorWhite_47 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_228() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveVector2_228)); }
	inline Vector2_t2156229523  get_k_LargePositiveVector2_228() const { return ___k_LargePositiveVector2_228; }
	inline Vector2_t2156229523 * get_address_of_k_LargePositiveVector2_228() { return &___k_LargePositiveVector2_228; }
	inline void set_k_LargePositiveVector2_228(Vector2_t2156229523  value)
	{
		___k_LargePositiveVector2_228 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_229() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeVector2_229)); }
	inline Vector2_t2156229523  get_k_LargeNegativeVector2_229() const { return ___k_LargeNegativeVector2_229; }
	inline Vector2_t2156229523 * get_address_of_k_LargeNegativeVector2_229() { return &___k_LargeNegativeVector2_229; }
	inline void set_k_LargeNegativeVector2_229(Vector2_t2156229523  value)
	{
		___k_LargeNegativeVector2_229 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_230() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveFloat_230)); }
	inline float get_k_LargePositiveFloat_230() const { return ___k_LargePositiveFloat_230; }
	inline float* get_address_of_k_LargePositiveFloat_230() { return &___k_LargePositiveFloat_230; }
	inline void set_k_LargePositiveFloat_230(float value)
	{
		___k_LargePositiveFloat_230 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_231() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeFloat_231)); }
	inline float get_k_LargeNegativeFloat_231() const { return ___k_LargeNegativeFloat_231; }
	inline float* get_address_of_k_LargeNegativeFloat_231() { return &___k_LargeNegativeFloat_231; }
	inline void set_k_LargeNegativeFloat_231(float value)
	{
		___k_LargeNegativeFloat_231 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_232() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargePositiveInt_232)); }
	inline int32_t get_k_LargePositiveInt_232() const { return ___k_LargePositiveInt_232; }
	inline int32_t* get_address_of_k_LargePositiveInt_232() { return &___k_LargePositiveInt_232; }
	inline void set_k_LargePositiveInt_232(int32_t value)
	{
		___k_LargePositiveInt_232 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_233() { return static_cast<int32_t>(offsetof(TMP_Text_t2599618874_StaticFields, ___k_LargeNegativeInt_233)); }
	inline int32_t get_k_LargeNegativeInt_233() const { return ___k_LargeNegativeInt_233; }
	inline int32_t* get_address_of_k_LargeNegativeInt_233() { return &___k_LargeNegativeInt_233; }
	inline void set_k_LargeNegativeInt_233(int32_t value)
	{
		___k_LargeNegativeInt_233 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T2599618874_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (TextAlignmentOptions_t4036791236)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2301[37] = 
{
	TextAlignmentOptions_t4036791236::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (_HorizontalAlignmentOptions_t2379014378)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2302[7] = 
{
	_HorizontalAlignmentOptions_t2379014378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (_VerticalAlignmentOptions_t2825528260)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2303[7] = 
{
	_VerticalAlignmentOptions_t2825528260::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (TextRenderFlags_t2418684345)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2304[3] = 
{
	TextRenderFlags_t2418684345::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (TMP_TextElementType_t1276645592)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2305[3] = 
{
	TMP_TextElementType_t1276645592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (MaskingTypes_t3687969768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2306[4] = 
{
	MaskingTypes_t3687969768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (TextOverflowModes_t1430035314)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2307[8] = 
{
	TextOverflowModes_t1430035314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (MaskingOffsetMode_t2266644590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2308[3] = 
{
	MaskingOffsetMode_t2266644590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (TextureMappingOptions_t270963663)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2309[5] = 
{
	TextureMappingOptions_t270963663::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (FontStyles_t3828945032)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2310[12] = 
{
	FontStyles_t3828945032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (FontWeights_t3122883458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2311[10] = 
{
	FontWeights_t3122883458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (TagUnits_t1169424683)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2312[4] = 
{
	TagUnits_t1169424683::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (TagType_t123236451)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[5] = 
{
	TagType_t123236451::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (TMP_Text_t2599618874), -1, sizeof(TMP_Text_t2599618874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2314[204] = 
{
	TMP_Text_t2599618874::get_offset_of_m_text_30(),
	TMP_Text_t2599618874::get_offset_of_m_isRightToLeft_31(),
	TMP_Text_t2599618874::get_offset_of_m_fontAsset_32(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontAsset_33(),
	TMP_Text_t2599618874::get_offset_of_m_isSDFShader_34(),
	TMP_Text_t2599618874::get_offset_of_m_sharedMaterial_35(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterial_36(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferences_37(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceIndexLookup_38(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceStack_39(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterialIndex_40(),
	TMP_Text_t2599618874::get_offset_of_m_fontSharedMaterials_41(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterial_42(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterials_43(),
	TMP_Text_t2599618874::get_offset_of_m_isMaterialDirty_44(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor32_45(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor_46(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_s_colorWhite_47(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColor_48(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColor_49(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColor_50(),
	TMP_Text_t2599618874::get_offset_of_m_enableVertexGradient_51(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradient_52(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradientPreset_53(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAsset_54(),
	TMP_Text_t2599618874::get_offset_of_m_tintAllSprites_55(),
	TMP_Text_t2599618874::get_offset_of_m_tintSprite_56(),
	TMP_Text_t2599618874::get_offset_of_m_spriteColor_57(),
	TMP_Text_t2599618874::get_offset_of_m_overrideHtmlColors_58(),
	TMP_Text_t2599618874::get_offset_of_m_faceColor_59(),
	TMP_Text_t2599618874::get_offset_of_m_outlineColor_60(),
	TMP_Text_t2599618874::get_offset_of_m_outlineWidth_61(),
	TMP_Text_t2599618874::get_offset_of_m_fontSize_62(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontSize_63(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeBase_64(),
	TMP_Text_t2599618874::get_offset_of_m_sizeStack_65(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeight_66(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightInternal_67(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightStack_68(),
	TMP_Text_t2599618874::get_offset_of_m_enableAutoSizing_69(),
	TMP_Text_t2599618874::get_offset_of_m_maxFontSize_70(),
	TMP_Text_t2599618874::get_offset_of_m_minFontSize_71(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMin_72(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMax_73(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyle_74(),
	TMP_Text_t2599618874::get_offset_of_m_style_75(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyleStack_76(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingBold_77(),
	TMP_Text_t2599618874::get_offset_of_m_textAlignment_78(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustification_79(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustificationStack_80(),
	TMP_Text_t2599618874::get_offset_of_m_textContainerLocalCorners_81(),
	TMP_Text_t2599618874::get_offset_of_m_isAlignmentEnumConverted_82(),
	TMP_Text_t2599618874::get_offset_of_m_characterSpacing_83(),
	TMP_Text_t2599618874::get_offset_of_m_cSpacing_84(),
	TMP_Text_t2599618874::get_offset_of_m_monoSpacing_85(),
	TMP_Text_t2599618874::get_offset_of_m_wordSpacing_86(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacing_87(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingDelta_88(),
	TMP_Text_t2599618874::get_offset_of_m_lineHeight_89(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingMax_90(),
	TMP_Text_t2599618874::get_offset_of_m_paragraphSpacing_91(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthMaxAdj_92(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthAdjDelta_93(),
	TMP_Text_t2599618874::get_offset_of_m_enableWordWrapping_94(),
	TMP_Text_t2599618874::get_offset_of_m_isCharacterWrappingEnabled_95(),
	TMP_Text_t2599618874::get_offset_of_m_isNonBreakingSpace_96(),
	TMP_Text_t2599618874::get_offset_of_m_isIgnoringAlignment_97(),
	TMP_Text_t2599618874::get_offset_of_m_wordWrappingRatios_98(),
	TMP_Text_t2599618874::get_offset_of_m_overflowMode_99(),
	TMP_Text_t2599618874::get_offset_of_m_firstOverflowCharacterIndex_100(),
	TMP_Text_t2599618874::get_offset_of_m_linkedTextComponent_101(),
	TMP_Text_t2599618874::get_offset_of_m_isLinkedTextComponent_102(),
	TMP_Text_t2599618874::get_offset_of_m_isTextTruncated_103(),
	TMP_Text_t2599618874::get_offset_of_m_enableKerning_104(),
	TMP_Text_t2599618874::get_offset_of_m_enableExtraPadding_105(),
	TMP_Text_t2599618874::get_offset_of_checkPaddingRequired_106(),
	TMP_Text_t2599618874::get_offset_of_m_isRichText_107(),
	TMP_Text_t2599618874::get_offset_of_m_parseCtrlCharacters_108(),
	TMP_Text_t2599618874::get_offset_of_m_isOverlay_109(),
	TMP_Text_t2599618874::get_offset_of_m_isOrthographic_110(),
	TMP_Text_t2599618874::get_offset_of_m_isCullingEnabled_111(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreRectMaskCulling_112(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreCulling_113(),
	TMP_Text_t2599618874::get_offset_of_m_horizontalMapping_114(),
	TMP_Text_t2599618874::get_offset_of_m_verticalMapping_115(),
	TMP_Text_t2599618874::get_offset_of_m_uvLineOffset_116(),
	TMP_Text_t2599618874::get_offset_of_m_renderMode_117(),
	TMP_Text_t2599618874::get_offset_of_m_geometrySortingOrder_118(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacter_119(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleCharacters_120(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleWords_121(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleLines_122(),
	TMP_Text_t2599618874::get_offset_of_m_useMaxVisibleDescender_123(),
	TMP_Text_t2599618874::get_offset_of_m_pageToDisplay_124(),
	TMP_Text_t2599618874::get_offset_of_m_isNewPage_125(),
	TMP_Text_t2599618874::get_offset_of_m_margin_126(),
	TMP_Text_t2599618874::get_offset_of_m_marginLeft_127(),
	TMP_Text_t2599618874::get_offset_of_m_marginRight_128(),
	TMP_Text_t2599618874::get_offset_of_m_marginWidth_129(),
	TMP_Text_t2599618874::get_offset_of_m_marginHeight_130(),
	TMP_Text_t2599618874::get_offset_of_m_width_131(),
	TMP_Text_t2599618874::get_offset_of_m_textInfo_132(),
	TMP_Text_t2599618874::get_offset_of_m_havePropertiesChanged_133(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingLegacyAnimationComponent_134(),
	TMP_Text_t2599618874::get_offset_of_m_transform_135(),
	TMP_Text_t2599618874::get_offset_of_m_rectTransform_136(),
	TMP_Text_t2599618874::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_137(),
	TMP_Text_t2599618874::get_offset_of_m_autoSizeTextContainer_138(),
	TMP_Text_t2599618874::get_offset_of_m_mesh_139(),
	TMP_Text_t2599618874::get_offset_of_m_isVolumetricText_140(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimator_141(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleHeight_142(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleWidth_143(),
	TMP_Text_t2599618874::get_offset_of_m_minWidth_144(),
	TMP_Text_t2599618874::get_offset_of_m_minHeight_145(),
	TMP_Text_t2599618874::get_offset_of_m_maxWidth_146(),
	TMP_Text_t2599618874::get_offset_of_m_maxHeight_147(),
	TMP_Text_t2599618874::get_offset_of_m_LayoutElement_148(),
	TMP_Text_t2599618874::get_offset_of_m_preferredWidth_149(),
	TMP_Text_t2599618874::get_offset_of_m_renderedWidth_150(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredWidthDirty_151(),
	TMP_Text_t2599618874::get_offset_of_m_preferredHeight_152(),
	TMP_Text_t2599618874::get_offset_of_m_renderedHeight_153(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredHeightDirty_154(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculatingPreferredValues_155(),
	TMP_Text_t2599618874::get_offset_of_m_recursiveCount_156(),
	TMP_Text_t2599618874::get_offset_of_m_layoutPriority_157(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculateSizeRequired_158(),
	TMP_Text_t2599618874::get_offset_of_m_isLayoutDirty_159(),
	TMP_Text_t2599618874::get_offset_of_m_verticesAlreadyDirty_160(),
	TMP_Text_t2599618874::get_offset_of_m_layoutAlreadyDirty_161(),
	TMP_Text_t2599618874::get_offset_of_m_isAwake_162(),
	TMP_Text_t2599618874::get_offset_of_m_isWaitingOnResourceLoad_163(),
	TMP_Text_t2599618874::get_offset_of_m_isInputParsingRequired_164(),
	TMP_Text_t2599618874::get_offset_of_m_inputSource_165(),
	TMP_Text_t2599618874::get_offset_of_old_text_166(),
	TMP_Text_t2599618874::get_offset_of_m_fontScale_167(),
	TMP_Text_t2599618874::get_offset_of_m_fontScaleMultiplier_168(),
	TMP_Text_t2599618874::get_offset_of_m_htmlTag_169(),
	TMP_Text_t2599618874::get_offset_of_m_xmlAttribute_170(),
	TMP_Text_t2599618874::get_offset_of_m_attributeParameterValues_171(),
	TMP_Text_t2599618874::get_offset_of_tag_LineIndent_172(),
	TMP_Text_t2599618874::get_offset_of_tag_Indent_173(),
	TMP_Text_t2599618874::get_offset_of_m_indentStack_174(),
	TMP_Text_t2599618874::get_offset_of_tag_NoParsing_175(),
	TMP_Text_t2599618874::get_offset_of_m_isParsingText_176(),
	TMP_Text_t2599618874::get_offset_of_m_FXMatrix_177(),
	TMP_Text_t2599618874::get_offset_of_m_isFXMatrixSet_178(),
	TMP_Text_t2599618874::get_offset_of_m_char_buffer_179(),
	TMP_Text_t2599618874::get_offset_of_m_internalCharacterInfo_180(),
	TMP_Text_t2599618874::get_offset_of_m_input_CharArray_181(),
	TMP_Text_t2599618874::get_offset_of_m_charArray_Length_182(),
	TMP_Text_t2599618874::get_offset_of_m_totalCharacterCount_183(),
	TMP_Text_t2599618874::get_offset_of_m_SavedWordWrapState_184(),
	TMP_Text_t2599618874::get_offset_of_m_SavedLineState_185(),
	TMP_Text_t2599618874::get_offset_of_m_characterCount_186(),
	TMP_Text_t2599618874::get_offset_of_m_firstCharacterOfLine_187(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacterOfLine_188(),
	TMP_Text_t2599618874::get_offset_of_m_lastCharacterOfLine_189(),
	TMP_Text_t2599618874::get_offset_of_m_lastVisibleCharacterOfLine_190(),
	TMP_Text_t2599618874::get_offset_of_m_lineNumber_191(),
	TMP_Text_t2599618874::get_offset_of_m_lineVisibleCharacterCount_192(),
	TMP_Text_t2599618874::get_offset_of_m_pageNumber_193(),
	TMP_Text_t2599618874::get_offset_of_m_maxAscender_194(),
	TMP_Text_t2599618874::get_offset_of_m_maxCapHeight_195(),
	TMP_Text_t2599618874::get_offset_of_m_maxDescender_196(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineAscender_197(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineDescender_198(),
	TMP_Text_t2599618874::get_offset_of_m_startOfLineAscender_199(),
	TMP_Text_t2599618874::get_offset_of_m_lineOffset_200(),
	TMP_Text_t2599618874::get_offset_of_m_meshExtents_201(),
	TMP_Text_t2599618874::get_offset_of_m_htmlColor_202(),
	TMP_Text_t2599618874::get_offset_of_m_colorStack_203(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColorStack_204(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColorStack_205(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColorStack_206(),
	TMP_Text_t2599618874::get_offset_of_m_colorGradientPreset_207(),
	TMP_Text_t2599618874::get_offset_of_m_colorGradientStack_208(),
	TMP_Text_t2599618874::get_offset_of_m_tabSpacing_209(),
	TMP_Text_t2599618874::get_offset_of_m_spacing_210(),
	TMP_Text_t2599618874::get_offset_of_m_styleStack_211(),
	TMP_Text_t2599618874::get_offset_of_m_actionStack_212(),
	TMP_Text_t2599618874::get_offset_of_m_padding_213(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffset_214(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffsetStack_215(),
	TMP_Text_t2599618874::get_offset_of_m_xAdvance_216(),
	TMP_Text_t2599618874::get_offset_of_m_textElementType_217(),
	TMP_Text_t2599618874::get_offset_of_m_cached_TextElement_218(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Underline_GlyphInfo_219(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Ellipsis_GlyphInfo_220(),
	TMP_Text_t2599618874::get_offset_of_m_defaultSpriteAsset_221(),
	TMP_Text_t2599618874::get_offset_of_m_currentSpriteAsset_222(),
	TMP_Text_t2599618874::get_offset_of_m_spriteCount_223(),
	TMP_Text_t2599618874::get_offset_of_m_spriteIndex_224(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimationID_225(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreActiveState_226(),
	TMP_Text_t2599618874::get_offset_of_k_Power_227(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveVector2_228(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeVector2_229(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveFloat_230(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeFloat_231(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveInt_232(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeInt_233(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (TextInputSources_t1522115805)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2315[5] = 
{
	TextInputSources_t1522115805::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (TMP_TextElement_t129727469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[9] = 
{
	TMP_TextElement_t129727469::get_offset_of_id_0(),
	TMP_TextElement_t129727469::get_offset_of_x_1(),
	TMP_TextElement_t129727469::get_offset_of_y_2(),
	TMP_TextElement_t129727469::get_offset_of_width_3(),
	TMP_TextElement_t129727469::get_offset_of_height_4(),
	TMP_TextElement_t129727469::get_offset_of_xOffset_5(),
	TMP_TextElement_t129727469::get_offset_of_yOffset_6(),
	TMP_TextElement_t129727469::get_offset_of_xAdvance_7(),
	TMP_TextElement_t129727469::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (TMP_TextInfo_t3598145122), -1, sizeof(TMP_TextInfo_t3598145122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2317[18] = 
{
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_t3598145122::get_offset_of_textComponent_2(),
	TMP_TextInfo_t3598145122::get_offset_of_characterCount_3(),
	TMP_TextInfo_t3598145122::get_offset_of_spriteCount_4(),
	TMP_TextInfo_t3598145122::get_offset_of_spaceCount_5(),
	TMP_TextInfo_t3598145122::get_offset_of_wordCount_6(),
	TMP_TextInfo_t3598145122::get_offset_of_linkCount_7(),
	TMP_TextInfo_t3598145122::get_offset_of_lineCount_8(),
	TMP_TextInfo_t3598145122::get_offset_of_pageCount_9(),
	TMP_TextInfo_t3598145122::get_offset_of_materialCount_10(),
	TMP_TextInfo_t3598145122::get_offset_of_characterInfo_11(),
	TMP_TextInfo_t3598145122::get_offset_of_wordInfo_12(),
	TMP_TextInfo_t3598145122::get_offset_of_linkInfo_13(),
	TMP_TextInfo_t3598145122::get_offset_of_lineInfo_14(),
	TMP_TextInfo_t3598145122::get_offset_of_pageInfo_15(),
	TMP_TextInfo_t3598145122::get_offset_of_meshInfo_16(),
	TMP_TextInfo_t3598145122::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (CaretPosition_t3997512201)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2318[4] = 
{
	CaretPosition_t3997512201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (CaretInfo_t841780893)+ sizeof (RuntimeObject), sizeof(CaretInfo_t841780893 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2319[2] = 
{
	CaretInfo_t841780893::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CaretInfo_t841780893::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (TMP_TextUtilities_t2105690005), -1, sizeof(TMP_TextUtilities_t2105690005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2320[3] = 
{
	TMP_TextUtilities_t2105690005_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (LineSegment_t1526544958)+ sizeof (RuntimeObject), sizeof(LineSegment_t1526544958 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2321[2] = 
{
	LineSegment_t1526544958::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineSegment_t1526544958::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (TMP_UpdateManager_t4114267509), -1, sizeof(TMP_UpdateManager_t4114267509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2322[5] = 
{
	TMP_UpdateManager_t4114267509_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (TMP_UpdateRegistry_t461608481), -1, sizeof(TMP_UpdateRegistry_t461608481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2323[5] = 
{
	TMP_UpdateRegistry_t461608481_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (TMP_BasicXmlTagStack_t2962628096)+ sizeof (RuntimeObject), sizeof(TMP_BasicXmlTagStack_t2962628096 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[10] = 
{
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (Compute_DistanceTransform_EventTypes_t2554612104)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2326[3] = 
{
	Compute_DistanceTransform_EventTypes_t2554612104::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (TMPro_EventManager_t712497257), -1, sizeof(TMPro_EventManager_t712497257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2327[13] = 
{
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_COMPUTE_DT_EVENT_0(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_MATERIAL_PROPERTY_EVENT_1(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_FONT_PROPERTY_EVENT_2(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_RESOURCE_LOAD_EVENT_9(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_OnPreRenderObject_Event_11(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXT_CHANGED_EVENT_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (Compute_DT_EventArgs_t1071353166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[3] = 
{
	Compute_DT_EventArgs_t1071353166::get_offset_of_EventType_0(),
	Compute_DT_EventArgs_t1071353166::get_offset_of_ProgressPercentage_1(),
	Compute_DT_EventArgs_t1071353166::get_offset_of_Colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (TMPro_ExtensionMethods_t1453208966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (TMP_Math_t624304809), -1, sizeof(TMP_Math_t624304809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2330[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TMP_Math_t624304809_StaticFields::get_offset_of_MAX_16BIT_6(),
	TMP_Math_t624304809_StaticFields::get_offset_of_MIN_16BIT_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (FaceInfo_t2243299176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[21] = 
{
	FaceInfo_t2243299176::get_offset_of_Name_0(),
	FaceInfo_t2243299176::get_offset_of_PointSize_1(),
	FaceInfo_t2243299176::get_offset_of_Scale_2(),
	FaceInfo_t2243299176::get_offset_of_CharacterCount_3(),
	FaceInfo_t2243299176::get_offset_of_LineHeight_4(),
	FaceInfo_t2243299176::get_offset_of_Baseline_5(),
	FaceInfo_t2243299176::get_offset_of_Ascender_6(),
	FaceInfo_t2243299176::get_offset_of_CapHeight_7(),
	FaceInfo_t2243299176::get_offset_of_Descender_8(),
	FaceInfo_t2243299176::get_offset_of_CenterLine_9(),
	FaceInfo_t2243299176::get_offset_of_SuperscriptOffset_10(),
	FaceInfo_t2243299176::get_offset_of_SubscriptOffset_11(),
	FaceInfo_t2243299176::get_offset_of_SubSize_12(),
	FaceInfo_t2243299176::get_offset_of_Underline_13(),
	FaceInfo_t2243299176::get_offset_of_UnderlineThickness_14(),
	FaceInfo_t2243299176::get_offset_of_strikethrough_15(),
	FaceInfo_t2243299176::get_offset_of_strikethroughThickness_16(),
	FaceInfo_t2243299176::get_offset_of_TabWidth_17(),
	FaceInfo_t2243299176::get_offset_of_Padding_18(),
	FaceInfo_t2243299176::get_offset_of_AtlasWidth_19(),
	FaceInfo_t2243299176::get_offset_of_AtlasHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (TMP_Glyph_t581847833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (FontAssetCreationSettings_t359369028)+ sizeof (RuntimeObject), sizeof(FontAssetCreationSettings_t359369028_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2333[16] = 
{
	FontAssetCreationSettings_t359369028::get_offset_of_sourceFontFileName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_sourceFontFileGUID_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_pointSizeSamplingMode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_pointSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_padding_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_packingMode_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_atlasWidth_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_atlasHeight_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_characterSetSelectionMode_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_characterSequence_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_referencedFontAssetGUID_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_referencedTextAssetGUID_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_fontStyle_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_fontStyleModifier_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_renderMode_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontAssetCreationSettings_t359369028::get_offset_of_includeFontFeatures_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (KerningPairKey_t536493877)+ sizeof (RuntimeObject), sizeof(KerningPairKey_t536493877 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2334[3] = 
{
	KerningPairKey_t536493877::get_offset_of_ascii_Left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_ascii_Right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t536493877::get_offset_of_key_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (GlyphValueRecord_t4065874512)+ sizeof (RuntimeObject), sizeof(GlyphValueRecord_t4065874512 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2335[4] = 
{
	GlyphValueRecord_t4065874512::get_offset_of_xPlacement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_yPlacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_xAdvance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t4065874512::get_offset_of_yAdvance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (KerningPair_t2270855589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[5] = 
{
	KerningPair_t2270855589::get_offset_of_m_FirstGlyph_0(),
	KerningPair_t2270855589::get_offset_of_m_FirstGlyphAdjustments_1(),
	KerningPair_t2270855589::get_offset_of_m_SecondGlyph_2(),
	KerningPair_t2270855589::get_offset_of_m_SecondGlyphAdjustments_3(),
	KerningPair_t2270855589::get_offset_of_xOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (KerningTable_t2322366871), -1, sizeof(KerningTable_t2322366871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2337[3] = 
{
	KerningTable_t2322366871::get_offset_of_kerningPairs_0(),
	KerningTable_t2322366871_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
	KerningTable_t2322366871_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (U3CAddKerningPairU3Ec__AnonStorey0_t2688361982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[2] = 
{
	U3CAddKerningPairU3Ec__AnonStorey0_t2688361982::get_offset_of_first_0(),
	U3CAddKerningPairU3Ec__AnonStorey0_t2688361982::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[2] = 
{
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262::get_offset_of_first_0(),
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t3257710262::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[2] = 
{
	U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074::get_offset_of_left_0(),
	U3CRemoveKerningPairU3Ec__AnonStorey2_t1355166074::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (TMP_FontUtilities_t2599150238), -1, sizeof(TMP_FontUtilities_t2599150238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2341[1] = 
{
	TMP_FontUtilities_t2599150238_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (TMP_VertexDataUpdateFlags_t388000256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2342[8] = 
{
	TMP_VertexDataUpdateFlags_t388000256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (TMP_CharacterInfo_t3185626797)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[35] = 
{
	TMP_CharacterInfo_t3185626797::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_material_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_color_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_style_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (TMP_Vertex_t2404176824)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t2404176824 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2344[5] = 
{
	TMP_Vertex_t2404176824::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t2404176824::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (VertexGradient_t345148380)+ sizeof (RuntimeObject), sizeof(VertexGradient_t345148380 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2345[4] = 
{
	VertexGradient_t345148380::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t345148380::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (TMP_PageInfo_t2608430633)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t2608430633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2346[5] = 
{
	TMP_PageInfo_t2608430633::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t2608430633::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (TMP_LinkInfo_t1092083476)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[7] = 
{
	TMP_LinkInfo_t1092083476::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (TMP_WordInfo_t3331066303)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[4] = 
{
	TMP_WordInfo_t3331066303::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3331066303::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (TMP_SpriteInfo_t2726321384)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t2726321384 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2349[3] = 
{
	TMP_SpriteInfo_t2726321384::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (Extents_t3837212874)+ sizeof (RuntimeObject), sizeof(Extents_t3837212874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2350[2] = 
{
	Extents_t3837212874::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_t3837212874::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (Mesh_Extents_t3388355125)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t3388355125 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2351[2] = 
{
	Mesh_Extents_t3388355125::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t3388355125::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (WordWrapState_t341939652)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[55] = 
{
	WordWrapState_t341939652::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t341939652::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (TagAttribute_t688278634)+ sizeof (RuntimeObject), sizeof(TagAttribute_t688278634 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2353[3] = 
{
	TagAttribute_t688278634::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t688278634::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (XML_TagAttribute_t1174424309)+ sizeof (RuntimeObject), sizeof(XML_TagAttribute_t1174424309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2354[5] = 
{
	XML_TagAttribute_t1174424309::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (ShaderUtilities_t714255158), -1, sizeof(ShaderUtilities_t714255158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2355[60] = 
{
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Padding_16(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GradientScale_17(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleX_18(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleY_19(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_PerspectiveFilter_20(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureWidth_21(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureHeight_22(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_BevelAmount_23(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowColor_24(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOffset_25(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowPower_26(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOuter_27(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_LightAngle_28(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMap_29(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrix_30(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrixRotation_31(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskCoord_32(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ClipRect_33(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessX_34(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessY_35(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetX_36(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetY_37(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UseClipRect_38(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilID_39(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilOp_40(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilComp_41(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilReadMask_42(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilWriteMask_43(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ShaderFlags_44(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_A_45(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_B_46(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_C_47(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Bevel_48(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Glow_49(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Underlay_50(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Ratios_51(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_SOFT_52(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_HARD_53(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_TEX_54(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Outline_55(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_ZTestMode_56(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_CullMode_57(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_m_clamp_58(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_isInitialized_59(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2356[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (U24ArrayTypeU3D40_t2865632059)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_t2865632059 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (CameraController_t3346819214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[5] = 
{
	CameraController_t3346819214::get_offset_of_speedH_4(),
	CameraController_t3346819214::get_offset_of_speedV_5(),
	CameraController_t3346819214::get_offset_of_yaw_6(),
	CameraController_t3346819214::get_offset_of_pitch_7(),
	CameraController_t3346819214::get_offset_of_robotKyle_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (VRFocusMarkerBase_t1705746600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[4] = 
{
	VRFocusMarkerBase_t1705746600::get_offset_of_vrRayCaster_4(),
	VRFocusMarkerBase_t1705746600::get_offset_of_isFocused_5(),
	VRFocusMarkerBase_t1705746600::get_offset_of_processingTime_6(),
	VRFocusMarkerBase_t1705746600::get_offset_of_focusTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (VRFocusRayCaster_t1481207659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[4] = 
{
	VRFocusRayCaster_t1481207659::get_offset_of_vrInput_4(),
	VRFocusRayCaster_t1481207659::get_offset_of_vrFocusMarker_5(),
	VRFocusRayCaster_t1481207659::get_offset_of_focusCamera_6(),
	VRFocusRayCaster_t1481207659::get_offset_of_currentInteractable_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (VRInput_t441285841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[2] = 
{
	VRInput_t441285841::get_offset_of_OnDown_4(),
	VRInput_t441285841::get_offset_of_OnUp_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (VRInteractableBase_t641439594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (DemoInputManager_t2406750443), -1, sizeof(DemoInputManager_t2406750443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2365[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	DemoInputManager_t2406750443::get_offset_of_isDaydream_18(),
	DemoInputManager_t2406750443::get_offset_of_activeControllerPointer_19(),
	DemoInputManager_t2406750443_StaticFields::get_offset_of_AllHands_20(),
	DemoInputManager_t2406750443::get_offset_of_controllerMain_21(),
	DemoInputManager_t2406750443_StaticFields::get_offset_of_CONTROLLER_MAIN_PROP_NAME_22(),
	DemoInputManager_t2406750443::get_offset_of_controllerPointers_23(),
	DemoInputManager_t2406750443_StaticFields::get_offset_of_CONTROLLER_POINTER_PROP_NAME_24(),
	DemoInputManager_t2406750443::get_offset_of_reticlePointer_25(),
	DemoInputManager_t2406750443_StaticFields::get_offset_of_RETICLE_POINTER_PROP_NAME_26(),
	DemoInputManager_t2406750443::get_offset_of_messageCanvas_27(),
	DemoInputManager_t2406750443::get_offset_of_messageText_28(),
	DemoInputManager_t2406750443::get_offset_of_gvrEmulatedPlatformType_29(),
	DemoInputManager_t2406750443_StaticFields::get_offset_of_EMULATED_PLATFORM_PROP_NAME_30(),
	DemoInputManager_t2406750443_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_31(),
	DemoInputManager_t2406750443_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (EmulatedPlatformType_t2164579554)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2366[3] = 
{
	EmulatedPlatformType_t2164579554::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (DemoSceneManager_t3984584607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (HeadsetDemoManager_t2048377926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[3] = 
{
	HeadsetDemoManager_t2048377926::get_offset_of_safetyRing_4(),
	HeadsetDemoManager_t2048377926::get_offset_of_enableDebugLog_5(),
	HeadsetDemoManager_t2048377926::get_offset_of_waitFourSeconds_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[4] = 
{
	U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617::get_offset_of_U24this_0(),
	U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617::get_offset_of_U24current_1(),
	U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617::get_offset_of_U24disposing_2(),
	U3CStatusUpdateLoopU3Ec__Iterator0_t1308120617::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (HelloVRManager_t3122911991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[2] = 
{
	HelloVRManager_t3122911991::get_offset_of_m_launchVrHomeButton_4(),
	HelloVRManager_t3122911991::get_offset_of_m_demoInputManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (ObjectController_t1463023822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[4] = 
{
	ObjectController_t1463023822::get_offset_of_startingPosition_4(),
	ObjectController_t1463023822::get_offset_of_myRenderer_5(),
	ObjectController_t1463023822::get_offset_of_inactiveMaterial_6(),
	ObjectController_t1463023822::get_offset_of_gazedAtMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (KeyboardDelegateExample_t2997392785), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[5] = 
{
	KeyboardDelegateExample_t2997392785::get_offset_of_KeyboardText_4(),
	KeyboardDelegateExample_t2997392785::get_offset_of_UpdateCanvas_5(),
	KeyboardDelegateExample_t2997392785::get_offset_of_KeyboardHidden_6(),
	KeyboardDelegateExample_t2997392785::get_offset_of_KeyboardShown_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (AppButtonInput_t1611596170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2373[2] = 
{
	AppButtonInput_t1611596170::get_offset_of_OnAppUp_4(),
	AppButtonInput_t1611596170::get_offset_of_OnAppDown_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (AutoPlayVideo_t2664495001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[5] = 
{
	AutoPlayVideo_t2664495001::get_offset_of_done_4(),
	AutoPlayVideo_t2664495001::get_offset_of_t_5(),
	AutoPlayVideo_t2664495001::get_offset_of_player_6(),
	AutoPlayVideo_t2664495001::get_offset_of_delay_7(),
	AutoPlayVideo_t2664495001::get_offset_of_loop_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (Vector3Event_t2701124756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (Vector2Event_t2715870356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (FloatEvent_t620910342), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (BoolEvent_t738798084), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (ButtonEvent_t4044041544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (TouchPadEvent_t2738674555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (TransformEvent_t114658240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (GameObjectEvent_t2012864557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (MenuHandler_t3978103572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[1] = 
{
	MenuHandler_t3978103572::get_offset_of_menuObjects_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (U3CDoAppearU3Ec__Iterator0_t1351014511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[5] = 
{
	U3CDoAppearU3Ec__Iterator0_t1351014511::get_offset_of_U3CcgU3E__0_0(),
	U3CDoAppearU3Ec__Iterator0_t1351014511::get_offset_of_U24this_1(),
	U3CDoAppearU3Ec__Iterator0_t1351014511::get_offset_of_U24current_2(),
	U3CDoAppearU3Ec__Iterator0_t1351014511::get_offset_of_U24disposing_3(),
	U3CDoAppearU3Ec__Iterator0_t1351014511::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (U3CDoFadeU3Ec__Iterator1_t1581559377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[5] = 
{
	U3CDoFadeU3Ec__Iterator1_t1581559377::get_offset_of_U3CcgU3E__0_0(),
	U3CDoFadeU3Ec__Iterator1_t1581559377::get_offset_of_U24this_1(),
	U3CDoFadeU3Ec__Iterator1_t1581559377::get_offset_of_U24current_2(),
	U3CDoFadeU3Ec__Iterator1_t1581559377::get_offset_of_U24disposing_3(),
	U3CDoFadeU3Ec__Iterator1_t1581559377::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (PositionSwapper_t3174547502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[2] = 
{
	PositionSwapper_t3174547502::get_offset_of_currentIndex_4(),
	PositionSwapper_t3174547502::get_offset_of_Positions_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (ScrubberEvents_t2166768381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[4] = 
{
	ScrubberEvents_t2166768381::get_offset_of_newPositionHandle_4(),
	ScrubberEvents_t2166768381::get_offset_of_corners_5(),
	ScrubberEvents_t2166768381::get_offset_of_slider_6(),
	ScrubberEvents_t2166768381::get_offset_of_mgr_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (SwitchVideos_t1699469881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[5] = 
{
	SwitchVideos_t1699469881::get_offset_of_localVideoSample_4(),
	SwitchVideos_t1699469881::get_offset_of_dashVideoSample_5(),
	SwitchVideos_t1699469881::get_offset_of_panoVideoSample_6(),
	SwitchVideos_t1699469881::get_offset_of_videoSamples_7(),
	SwitchVideos_t1699469881::get_offset_of_missingLibText_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (ToggleAction_t3185694766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[7] = 
{
	ToggleAction_t3185694766::get_offset_of_lastUsage_4(),
	ToggleAction_t3185694766::get_offset_of_on_5(),
	ToggleAction_t3185694766::get_offset_of_OnToggleOn_6(),
	ToggleAction_t3185694766::get_offset_of_OnToggleOff_7(),
	ToggleAction_t3185694766::get_offset_of_InitialState_8(),
	ToggleAction_t3185694766::get_offset_of_RaiseEventForInitialState_9(),
	ToggleAction_t3185694766::get_offset_of_Cooldown_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (VideoControlsManager_t1556912537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[11] = 
{
	VideoControlsManager_t1556912537::get_offset_of_pauseSprite_4(),
	VideoControlsManager_t1556912537::get_offset_of_playSprite_5(),
	VideoControlsManager_t1556912537::get_offset_of_videoScrubber_6(),
	VideoControlsManager_t1556912537::get_offset_of_volumeSlider_7(),
	VideoControlsManager_t1556912537::get_offset_of_volumeWidget_8(),
	VideoControlsManager_t1556912537::get_offset_of_settingsPanel_9(),
	VideoControlsManager_t1556912537::get_offset_of_bufferedBackground_10(),
	VideoControlsManager_t1556912537::get_offset_of_basePosition_11(),
	VideoControlsManager_t1556912537::get_offset_of_videoPosition_12(),
	VideoControlsManager_t1556912537::get_offset_of_videoDuration_13(),
	VideoControlsManager_t1556912537::get_offset_of_U3CPlayerU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (U3CDoAppearU3Ec__Iterator0_t1765908247), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[5] = 
{
	U3CDoAppearU3Ec__Iterator0_t1765908247::get_offset_of_U3CcgU3E__0_0(),
	U3CDoAppearU3Ec__Iterator0_t1765908247::get_offset_of_U24this_1(),
	U3CDoAppearU3Ec__Iterator0_t1765908247::get_offset_of_U24current_2(),
	U3CDoAppearU3Ec__Iterator0_t1765908247::get_offset_of_U24disposing_3(),
	U3CDoAppearU3Ec__Iterator0_t1765908247::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (U3CDoFadeU3Ec__Iterator1_t3288079470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[5] = 
{
	U3CDoFadeU3Ec__Iterator1_t3288079470::get_offset_of_U3CcgU3E__0_0(),
	U3CDoFadeU3Ec__Iterator1_t3288079470::get_offset_of_U24this_1(),
	U3CDoFadeU3Ec__Iterator1_t3288079470::get_offset_of_U24current_2(),
	U3CDoFadeU3Ec__Iterator1_t3288079470::get_offset_of_U24disposing_3(),
	U3CDoFadeU3Ec__Iterator1_t3288079470::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (VideoPlayerReference_t625508773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[1] = 
{
	VideoPlayerReference_t625508773::get_offset_of_player_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (GvrAudio_t1993875383), -1, sizeof(GvrAudio_t1993875383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2394[25] = 
{
	GvrAudio_t1993875383_StaticFields::get_offset_of_sampleRate_0(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_numChannels_1(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_framesPerBuffer_2(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_listenerDirectivityColor_3(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_sourceDirectivityColor_4(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	GvrAudio_t1993875383_StaticFields::get_offset_of_bounds_17(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_enabledRooms_18(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_initialized_19(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_listenerTransform_20(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_occlusionHits_21(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_occlusionMaskValue_22(),
	GvrAudio_t1993875383_StaticFields::get_offset_of_transformMatrix_23(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (Quality_t1508087645)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2395[4] = 
{
	Quality_t1508087645::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (SpatializerData_t1601606274)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2396[9] = 
{
	SpatializerData_t1601606274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (SpatializerType_t42254374)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2397[3] = 
{
	SpatializerType_t42254374::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (RoomProperties_t228572612)+ sizeof (RuntimeObject), sizeof(RoomProperties_t228572612 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2398[20] = 
{
	RoomProperties_t228572612::get_offset_of_positionX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_positionY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_positionZ_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_rotationX_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_rotationY_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_rotationZ_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_rotationW_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_dimensionsX_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_dimensionsY_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_dimensionsZ_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_materialLeft_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_materialRight_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_materialBottom_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_materialTop_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_materialFront_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_materialBack_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_reflectionScalar_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_reverbGain_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_reverbTime_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RoomProperties_t228572612::get_offset_of_reverbBrightness_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (GvrAudioListener_t82459280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2399[3] = 
{
	GvrAudioListener_t82459280::get_offset_of_globalGainDb_4(),
	GvrAudioListener_t82459280::get_offset_of_occlusionMask_5(),
	GvrAudioListener_t82459280::get_offset_of_quality_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
