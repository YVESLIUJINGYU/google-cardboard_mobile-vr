﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct VirtFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericInterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct GenericInterfaceActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};

// Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>
struct AbstractBuilderLite_2_t145292663;
// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct AbstractBuilderLite_2_t522313195;
// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct AbstractBuilderLite_2_t4063219799;
// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct AbstractBuilderLite_2_t258149081;
// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct AbstractBuilderLite_2_t1643049470;
// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct AbstractBuilderLite_2_t4034615238;
// Google.ProtocolBuffers.ByteString
struct ByteString_t35393593;
// Google.ProtocolBuffers.Collections.IPopsicleList`1<System.Single>
struct IPopsicleList_1_t2207714234;
// Google.ProtocolBuffers.Collections.IPopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct IPopsicleList_1_t660505722;
// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>
struct PopsicleList_1_t168709280;
// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>
struct PopsicleList_1_t2780837186;
// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct PopsicleList_1_t1233628674;
// Google.ProtocolBuffers.ExtensionRegistry
struct ExtensionRegistry_t4271428238;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>
struct GeneratedBuilderLite_2_t3548493156;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct GeneratedBuilderLite_2_t861404538;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct GeneratedBuilderLite_2_t3925513688;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct GeneratedBuilderLite_2_t3171452996;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct GeneratedBuilderLite_2_t3661349574;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct GeneratedBuilderLite_2_t751282667;
// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct GeneratedBuilderLite_2_t3142848435;
// Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>
struct GeneratedMessageLite_2_t4158627738;
// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct GeneratedMessageLite_2_t240680974;
// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct GeneratedMessageLite_2_t3781587578;
// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct GeneratedMessageLite_2_t4271484156;
// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct GeneratedMessageLite_2_t1361417249;
// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct GeneratedMessageLite_2_t3752983017;
// Google.ProtocolBuffers.ICodedInputStream
struct ICodedInputStream_t3221112298;
// Google.ProtocolBuffers.ICodedOutputStream
struct ICodedOutputStream_t1215615781;
// Google.ProtocolBuffers.IMessageLite
struct IMessageLite_t757992097;
// Google.ProtocolBuffers.InvalidProtocolBufferException
struct InvalidProtocolBufferException_t2498581859;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite>
struct Dictionary_2_t294577696;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,Google.ProtocolBuffers.IGeneratedExtensionLite>>
struct Dictionary_2_t3107168633;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_t39404347;
// System.Collections.Generic.IComparer`1<System.String>
struct IComparer_1_t3101716168;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t377119663;
// System.Collections.Generic.IEnumerable`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct IEnumerable_1_t3124878447;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t3512676632;
// System.Collections.Generic.IEnumerator`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct IEnumerator_1_t282628730;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// System.Collections.Generic.IList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct IList_1_t1665378045;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t2869341516;
// System.Collections.Generic.List`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct List_1_t1322133004;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.IFormatProvider
struct IFormatProvider_t2518567562;
// System.IO.Stream
struct Stream_t1273022909;
// System.IO.TextWriter
struct TextWriter_t3478189236;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.StringComparer
struct StringComparer_t3301955079;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Void
struct Void_t1185182177;
// proto.PhoneEvent/Types/DepthMapEvent
struct DepthMapEvent_t729886054;
// proto.PhoneEvent/Types/DepthMapEvent/Builder
struct Builder_t1293396100;
// proto.PhoneEvent/Types/GyroscopeEvent
struct GyroscopeEvent_t127774954;
// proto.PhoneEvent/Types/GyroscopeEvent/Builder
struct Builder_t3442751222;
// proto.PhoneEvent/Types/KeyEvent
struct KeyEvent_t1937114521;
// proto.PhoneEvent/Types/KeyEvent/Builder
struct Builder_t2712992173;
// proto.PhoneEvent/Types/MotionEvent
struct MotionEvent_t3121383016;
// proto.PhoneEvent/Types/MotionEvent/Builder
struct Builder_t2961114394;
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer
struct Pointer_t4145025558;
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder
struct Builder_t582111845;
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer[]
struct PointerU5BU5D_t2544723155;
// proto.PhoneEvent/Types/OrientationEvent
struct OrientationEvent_t158247624;
// proto.PhoneEvent/Types/OrientationEvent/Builder
struct Builder_t2279437287;

extern RuntimeClass* Builder_t1293396100_il2cpp_TypeInfo_var;
extern RuntimeClass* Builder_t2279437287_il2cpp_TypeInfo_var;
extern RuntimeClass* Builder_t2712992173_il2cpp_TypeInfo_var;
extern RuntimeClass* Builder_t2961114394_il2cpp_TypeInfo_var;
extern RuntimeClass* Builder_t3442751222_il2cpp_TypeInfo_var;
extern RuntimeClass* Builder_t582111845_il2cpp_TypeInfo_var;
extern RuntimeClass* CodedOutputStream_t1787628118_il2cpp_TypeInfo_var;
extern RuntimeClass* DepthMapEvent_t729886054_il2cpp_TypeInfo_var;
extern RuntimeClass* ExtensionRegistry_t4271428238_il2cpp_TypeInfo_var;
extern RuntimeClass* GyroscopeEvent_t127774954_il2cpp_TypeInfo_var;
extern RuntimeClass* ICodedInputStream_t3221112298_il2cpp_TypeInfo_var;
extern RuntimeClass* ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerable_1_t3124878447_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_1_t282628730_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* Int64_t3736567304_il2cpp_TypeInfo_var;
extern RuntimeClass* KeyEvent_t1937114521_il2cpp_TypeInfo_var;
extern RuntimeClass* MotionEvent_t3121383016_il2cpp_TypeInfo_var;
extern RuntimeClass* OrientationEvent_t158247624_il2cpp_TypeInfo_var;
extern RuntimeClass* PhoneEvent_t3448566392_il2cpp_TypeInfo_var;
extern RuntimeClass* Pointer_t4145025558_il2cpp_TypeInfo_var;
extern RuntimeClass* PopsicleList_1_t1233628674_il2cpp_TypeInfo_var;
extern RuntimeClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern RuntimeClass* StringComparer_t3301955079_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2_FieldInfo_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4_FieldInfo_var;
extern String_t* _stringLiteral1985170611;
extern String_t* _stringLiteral2365897554;
extern String_t* _stringLiteral3130993982;
extern String_t* _stringLiteral3166771228;
extern String_t* _stringLiteral3452614601;
extern String_t* _stringLiteral3452614614;
extern String_t* _stringLiteral3452614615;
extern String_t* _stringLiteral3452614616;
extern String_t* _stringLiteral3454449607;
extern String_t* _stringLiteral3493618073;
extern String_t* _stringLiteral4020252391;
extern String_t* _stringLiteral4026478820;
extern String_t* _stringLiteral792341822;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m1016734311_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m1106981_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m2055751663_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m25623811_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m2857252119_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m3541096835_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m3618057809_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m3997270878_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m4283379067_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeDelimitedFrom_m715217478_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m1101353770_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m1104280193_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m1124215902_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m1351836793_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m1442256499_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m14772483_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m1514029506_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m1962044158_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m2021870966_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m2051397694_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m2082277826_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m2211482326_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m269598545_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m2996271875_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3032596744_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3060350321_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3259242108_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3419181726_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3622339991_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3650812895_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3702155892_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3720688089_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3843169734_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m3965376215_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m4193624750_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m4289660591_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m541927886_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m776875282_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m870343857_RuntimeMethod_var;
extern const RuntimeMethod* AbstractBuilderLite_2_MergeFrom_m908247555_RuntimeMethod_var;
extern const RuntimeMethod* Array_BinarySearch_TisString_t_m2519237149_RuntimeMethod_var;
extern const RuntimeMethod* Builder_MergeFrom_m1132865163_RuntimeMethod_var;
extern const RuntimeMethod* Builder_MergeFrom_m160215804_RuntimeMethod_var;
extern const RuntimeMethod* Builder_MergeFrom_m2307689438_RuntimeMethod_var;
extern const RuntimeMethod* Builder_MergeFrom_m2313681359_RuntimeMethod_var;
extern const RuntimeMethod* Builder_MergeFrom_m2919572840_RuntimeMethod_var;
extern const RuntimeMethod* Builder_MergeFrom_m3447828463_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_MergeFrom_m1218719396_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_MergeFrom_m157053624_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_MergeFrom_m2246583089_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_MergeFrom_m4038465069_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_MergeFrom_m610274514_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2_MergeFrom_m629985948_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2__ctor_m1251714133_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2__ctor_m24974982_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2__ctor_m2782592139_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2__ctor_m3353608380_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2__ctor_m3477943950_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedBuilderLite_2__ctor_m4090092181_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2_PrintField_TisPointer_t4145025558_m3080746959_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2_PrintField_m1636826668_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2_PrintField_m2673392170_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2_PrintField_m3760789329_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2_PrintField_m4052720336_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2_PrintField_m4136168662_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2__ctor_m3200145457_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2__ctor_m326934031_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2__ctor_m3287578133_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2__ctor_m3776640171_RuntimeMethod_var;
extern const RuntimeMethod* GeneratedMessageLite_2__ctor_m647295579_RuntimeMethod_var;
extern const RuntimeMethod* ICodedInputStream_ReadMessageArray_TisPointer_t4145025558_m2307173454_RuntimeMethod_var;
extern const RuntimeMethod* ICodedOutputStream_WriteMessageArray_TisPointer_t4145025558_m3466852259_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_Add_m2039782584_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_Add_m2124609127_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_Add_m3166945360_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_Add_m3756973089_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_Clear_m2727107298_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_Clear_m485968702_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_GetEnumerator_m1740650653_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_MakeReadOnly_m1389303794_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1__ctor_m3092888001_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_get_Count_m2360652361_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_get_Count_m700411858_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_get_Item_m2708808148_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_set_Item_m2187003455_RuntimeMethod_var;
extern const RuntimeMethod* PopsicleList_1_set_Item_m2953605274_RuntimeMethod_var;
extern const uint32_t Builder_AddPointers_m1746726519_MetadataUsageId;
extern const uint32_t Builder_AddPointers_m871810046_MetadataUsageId;
extern const uint32_t Builder_AddRangePointers_m2760467942_MetadataUsageId;
extern const uint32_t Builder_AddRangeZDistances_m4278420723_MetadataUsageId;
extern const uint32_t Builder_AddZDistances_m4175806553_MetadataUsageId;
extern const uint32_t Builder_ClearPointers_m113524416_MetadataUsageId;
extern const uint32_t Builder_ClearZDistances_m2985824343_MetadataUsageId;
extern const uint32_t Builder_Clear_m1204017742_MetadataUsageId;
extern const uint32_t Builder_Clear_m1516654503_MetadataUsageId;
extern const uint32_t Builder_Clear_m3080749485_MetadataUsageId;
extern const uint32_t Builder_Clear_m3117102428_MetadataUsageId;
extern const uint32_t Builder_Clear_m3186867221_MetadataUsageId;
extern const uint32_t Builder_Clear_m3271464179_MetadataUsageId;
extern const uint32_t Builder_Clone_m1232704756_MetadataUsageId;
extern const uint32_t Builder_Clone_m1690141094_MetadataUsageId;
extern const uint32_t Builder_Clone_m2009059382_MetadataUsageId;
extern const uint32_t Builder_Clone_m306484013_MetadataUsageId;
extern const uint32_t Builder_Clone_m3442298780_MetadataUsageId;
extern const uint32_t Builder_Clone_m803526565_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m1018040705_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m1132865163_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m1260728313_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m1367107875_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m160215804_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m1700363822_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m1801103483_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m1875709835_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m1884232002_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m2307689438_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m2313681359_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m2566919961_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m2919572840_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m3196150861_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m3351516325_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m33684292_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m3447828463_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m3465110836_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m3488476321_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m3674657960_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m3801718990_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m3939827302_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m4190562827_MetadataUsageId;
extern const uint32_t Builder_MergeFrom_m877028760_MetadataUsageId;
extern const uint32_t Builder_PrepareBuilder_m1452950514_MetadataUsageId;
extern const uint32_t Builder_PrepareBuilder_m147557039_MetadataUsageId;
extern const uint32_t Builder_PrepareBuilder_m2114990423_MetadataUsageId;
extern const uint32_t Builder_PrepareBuilder_m2800481259_MetadataUsageId;
extern const uint32_t Builder_PrepareBuilder_m323407734_MetadataUsageId;
extern const uint32_t Builder_PrepareBuilder_m935363363_MetadataUsageId;
extern const uint32_t Builder_SetPointers_m247642983_MetadataUsageId;
extern const uint32_t Builder_SetPointers_m3282197334_MetadataUsageId;
extern const uint32_t Builder_SetZDistances_m3658819453_MetadataUsageId;
extern const uint32_t Builder__ctor_m2482629630_MetadataUsageId;
extern const uint32_t Builder__ctor_m2804606681_MetadataUsageId;
extern const uint32_t Builder__ctor_m2822184233_MetadataUsageId;
extern const uint32_t Builder__ctor_m2915391915_MetadataUsageId;
extern const uint32_t Builder__ctor_m3115778984_MetadataUsageId;
extern const uint32_t Builder__ctor_m3196691357_MetadataUsageId;
extern const uint32_t Builder__ctor_m3210742657_MetadataUsageId;
extern const uint32_t Builder__ctor_m3220974630_MetadataUsageId;
extern const uint32_t Builder__ctor_m3588482064_MetadataUsageId;
extern const uint32_t Builder__ctor_m3698740796_MetadataUsageId;
extern const uint32_t Builder__ctor_m4137946296_MetadataUsageId;
extern const uint32_t Builder__ctor_m4245920840_MetadataUsageId;
extern const uint32_t Builder_get_DefaultInstanceForType_m127672502_MetadataUsageId;
extern const uint32_t Builder_get_DefaultInstanceForType_m4198353571_MetadataUsageId;
extern const uint32_t Builder_get_DefaultInstanceForType_m492229874_MetadataUsageId;
extern const uint32_t Builder_get_DefaultInstanceForType_m643611210_MetadataUsageId;
extern const uint32_t Builder_get_DefaultInstanceForType_m699105530_MetadataUsageId;
extern const uint32_t Builder_get_DefaultInstanceForType_m980516419_MetadataUsageId;
extern const uint32_t GyroscopeEvent_CreateBuilderForType_m1472271432_MetadataUsageId;
extern const uint32_t GyroscopeEvent_CreateBuilder_m2664119612_MetadataUsageId;
extern const uint32_t GyroscopeEvent_CreateBuilder_m3306880792_MetadataUsageId;
extern const uint32_t GyroscopeEvent_Equals_m1952796240_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseDelimitedFrom_m2365112648_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseDelimitedFrom_m4013462724_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseFrom_m1012639160_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseFrom_m13958895_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseFrom_m223739709_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseFrom_m3246061752_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseFrom_m3862662208_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseFrom_m3978672244_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseFrom_m429130748_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ParseFrom_m903272835_MetadataUsageId;
extern const uint32_t GyroscopeEvent_PrintTo_m3575727316_MetadataUsageId;
extern const uint32_t GyroscopeEvent_ToBuilder_m569019558_MetadataUsageId;
extern const uint32_t GyroscopeEvent_WriteTo_m3982350230_MetadataUsageId;
extern const uint32_t GyroscopeEvent__cctor_m1473982519_MetadataUsageId;
extern const uint32_t GyroscopeEvent__ctor_m122134945_MetadataUsageId;
extern const uint32_t GyroscopeEvent_get_DefaultInstanceForType_m4240245561_MetadataUsageId;
extern const uint32_t GyroscopeEvent_get_DefaultInstance_m2890176487_MetadataUsageId;
extern const uint32_t GyroscopeEvent_get_SerializedSize_m2068668232_MetadataUsageId;
extern const uint32_t KeyEvent_CreateBuilderForType_m559879078_MetadataUsageId;
extern const uint32_t KeyEvent_CreateBuilder_m2320679930_MetadataUsageId;
extern const uint32_t KeyEvent_CreateBuilder_m83685355_MetadataUsageId;
extern const uint32_t KeyEvent_Equals_m3306860044_MetadataUsageId;
extern const uint32_t KeyEvent_ParseDelimitedFrom_m2737003127_MetadataUsageId;
extern const uint32_t KeyEvent_ParseDelimitedFrom_m3509952229_MetadataUsageId;
extern const uint32_t KeyEvent_ParseFrom_m2103175321_MetadataUsageId;
extern const uint32_t KeyEvent_ParseFrom_m2519169384_MetadataUsageId;
extern const uint32_t KeyEvent_ParseFrom_m3061039951_MetadataUsageId;
extern const uint32_t KeyEvent_ParseFrom_m3197497049_MetadataUsageId;
extern const uint32_t KeyEvent_ParseFrom_m3654509195_MetadataUsageId;
extern const uint32_t KeyEvent_ParseFrom_m3815981327_MetadataUsageId;
extern const uint32_t KeyEvent_ParseFrom_m3950290315_MetadataUsageId;
extern const uint32_t KeyEvent_ParseFrom_m476182167_MetadataUsageId;
extern const uint32_t KeyEvent_PrintTo_m3049625899_MetadataUsageId;
extern const uint32_t KeyEvent_ToBuilder_m2617464935_MetadataUsageId;
extern const uint32_t KeyEvent_WriteTo_m4091272948_MetadataUsageId;
extern const uint32_t KeyEvent__cctor_m3482539191_MetadataUsageId;
extern const uint32_t KeyEvent__ctor_m4217360548_MetadataUsageId;
extern const uint32_t KeyEvent_get_DefaultInstanceForType_m3895015777_MetadataUsageId;
extern const uint32_t KeyEvent_get_DefaultInstance_m740969952_MetadataUsageId;
extern const uint32_t KeyEvent_get_SerializedSize_m1954691348_MetadataUsageId;
extern const uint32_t MotionEvent_CreateBuilderForType_m512433045_MetadataUsageId;
extern const uint32_t MotionEvent_CreateBuilder_m1575032901_MetadataUsageId;
extern const uint32_t MotionEvent_CreateBuilder_m586676648_MetadataUsageId;
extern const uint32_t MotionEvent_Equals_m4061008759_MetadataUsageId;
extern const uint32_t MotionEvent_GetHashCode_m1629391598_MetadataUsageId;
extern const uint32_t MotionEvent_GetPointers_m3628348230_MetadataUsageId;
extern const uint32_t MotionEvent_MakeReadOnly_m1076849093_MetadataUsageId;
extern const uint32_t MotionEvent_ParseDelimitedFrom_m2503259629_MetadataUsageId;
extern const uint32_t MotionEvent_ParseDelimitedFrom_m3000926622_MetadataUsageId;
extern const uint32_t MotionEvent_ParseFrom_m1089706159_MetadataUsageId;
extern const uint32_t MotionEvent_ParseFrom_m2055058782_MetadataUsageId;
extern const uint32_t MotionEvent_ParseFrom_m2186337349_MetadataUsageId;
extern const uint32_t MotionEvent_ParseFrom_m3050040186_MetadataUsageId;
extern const uint32_t MotionEvent_ParseFrom_m3594203963_MetadataUsageId;
extern const uint32_t MotionEvent_ParseFrom_m4122922873_MetadataUsageId;
extern const uint32_t MotionEvent_ParseFrom_m871094949_MetadataUsageId;
extern const uint32_t MotionEvent_ParseFrom_m922347793_MetadataUsageId;
extern const uint32_t MotionEvent_PrintTo_m1843844315_MetadataUsageId;
extern const uint32_t MotionEvent_ToBuilder_m1316974738_MetadataUsageId;
extern const uint32_t MotionEvent_WriteTo_m3305395790_MetadataUsageId;
extern const uint32_t MotionEvent__cctor_m2507959477_MetadataUsageId;
extern const uint32_t MotionEvent__ctor_m1502834634_MetadataUsageId;
extern const uint32_t MotionEvent_get_DefaultInstanceForType_m3113173202_MetadataUsageId;
extern const uint32_t MotionEvent_get_DefaultInstance_m2362671815_MetadataUsageId;
extern const uint32_t MotionEvent_get_PointersCount_m837067104_MetadataUsageId;
extern const uint32_t MotionEvent_get_SerializedSize_m408294511_MetadataUsageId;
extern const uint32_t OrientationEvent_CreateBuilderForType_m795056700_MetadataUsageId;
extern const uint32_t OrientationEvent_CreateBuilder_m324048109_MetadataUsageId;
extern const uint32_t OrientationEvent_CreateBuilder_m4263316720_MetadataUsageId;
extern const uint32_t OrientationEvent_Equals_m2974127354_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseDelimitedFrom_m212379149_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseDelimitedFrom_m251292222_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m1382868771_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m177464868_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m2109398516_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m2469088444_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m3260499152_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m3894785390_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m572682107_MetadataUsageId;
extern const uint32_t OrientationEvent_ParseFrom_m955229731_MetadataUsageId;
extern const uint32_t OrientationEvent_PrintTo_m328199974_MetadataUsageId;
extern const uint32_t OrientationEvent_ToBuilder_m553496387_MetadataUsageId;
extern const uint32_t OrientationEvent_WriteTo_m633475206_MetadataUsageId;
extern const uint32_t OrientationEvent__cctor_m2779686345_MetadataUsageId;
extern const uint32_t OrientationEvent__ctor_m3708828125_MetadataUsageId;
extern const uint32_t OrientationEvent_get_DefaultInstanceForType_m1391900177_MetadataUsageId;
extern const uint32_t OrientationEvent_get_DefaultInstance_m2374728749_MetadataUsageId;
extern const uint32_t OrientationEvent_get_SerializedSize_m725630368_MetadataUsageId;
extern const uint32_t PhoneEvent__cctor_m3021104822_MetadataUsageId;
extern const uint32_t Pointer_CreateBuilderForType_m723998425_MetadataUsageId;
extern const uint32_t Pointer_CreateBuilder_m2774886265_MetadataUsageId;
extern const uint32_t Pointer_CreateBuilder_m4178316069_MetadataUsageId;
extern const uint32_t Pointer_Equals_m946514537_MetadataUsageId;
extern const uint32_t Pointer_ParseDelimitedFrom_m2976578207_MetadataUsageId;
extern const uint32_t Pointer_ParseDelimitedFrom_m4228961319_MetadataUsageId;
extern const uint32_t Pointer_ParseFrom_m1063778754_MetadataUsageId;
extern const uint32_t Pointer_ParseFrom_m1699324165_MetadataUsageId;
extern const uint32_t Pointer_ParseFrom_m2469062765_MetadataUsageId;
extern const uint32_t Pointer_ParseFrom_m2556922512_MetadataUsageId;
extern const uint32_t Pointer_ParseFrom_m3041481156_MetadataUsageId;
extern const uint32_t Pointer_ParseFrom_m3657044794_MetadataUsageId;
extern const uint32_t Pointer_ParseFrom_m3706758380_MetadataUsageId;
extern const uint32_t Pointer_ParseFrom_m3839729296_MetadataUsageId;
extern const uint32_t Pointer_PrintTo_m1214513662_MetadataUsageId;
extern const uint32_t Pointer_ToBuilder_m3377747820_MetadataUsageId;
extern const uint32_t Pointer_WriteTo_m3630027018_MetadataUsageId;
extern const uint32_t Pointer__cctor_m2799481490_MetadataUsageId;
extern const uint32_t Pointer__ctor_m597435337_MetadataUsageId;
extern const uint32_t Pointer_get_DefaultInstanceForType_m122910909_MetadataUsageId;
extern const uint32_t Pointer_get_DefaultInstance_m846755284_MetadataUsageId;
extern const uint32_t Pointer_get_SerializedSize_m3309772427_MetadataUsageId;

struct ByteU5BU5D_t4116647657;
struct StringU5BU5D_t1281789340;
struct UInt32U5BU5D_t2770800703;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTRACTBUILDERLITE_2_T1753171341_H
#define ABSTRACTBUILDERLITE_2_T1753171341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct  AbstractBuilderLite_2_t1753171341  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T1753171341_H
#ifndef ABSTRACTBUILDERLITE_2_T522313195_H
#define ABSTRACTBUILDERLITE_2_T522313195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct  AbstractBuilderLite_2_t522313195  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T522313195_H
#ifndef ABSTRACTBUILDERLITE_2_T4063219799_H
#define ABSTRACTBUILDERLITE_2_T4063219799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  AbstractBuilderLite_2_t4063219799  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T4063219799_H
#ifndef ABSTRACTBUILDERLITE_2_T258149081_H
#define ABSTRACTBUILDERLITE_2_T258149081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct  AbstractBuilderLite_2_t258149081  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T258149081_H
#ifndef ABSTRACTBUILDERLITE_2_T1643049470_H
#define ABSTRACTBUILDERLITE_2_T1643049470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct  AbstractBuilderLite_2_t1643049470  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T1643049470_H
#ifndef ABSTRACTBUILDERLITE_2_T4034615238_H
#define ABSTRACTBUILDERLITE_2_T4034615238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  AbstractBuilderLite_2_t4034615238  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTBUILDERLITE_2_T4034615238_H
#ifndef ABSTRACTMESSAGELITE_2_T3677230316_H
#define ABSTRACTMESSAGELITE_2_T3677230316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct  AbstractMessageLite_2_t3677230316  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T3677230316_H
#ifndef ABSTRACTMESSAGELITE_2_T2446372170_H
#define ABSTRACTMESSAGELITE_2_T2446372170_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct  AbstractMessageLite_2_t2446372170  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T2446372170_H
#ifndef ABSTRACTMESSAGELITE_2_T1692311478_H
#define ABSTRACTMESSAGELITE_2_T1692311478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  AbstractMessageLite_2_t1692311478  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T1692311478_H
#ifndef ABSTRACTMESSAGELITE_2_T2182208056_H
#define ABSTRACTMESSAGELITE_2_T2182208056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct  AbstractMessageLite_2_t2182208056  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T2182208056_H
#ifndef ABSTRACTMESSAGELITE_2_T3567108445_H
#define ABSTRACTMESSAGELITE_2_T3567108445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct  AbstractMessageLite_2_t3567108445  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T3567108445_H
#ifndef ABSTRACTMESSAGELITE_2_T1663706917_H
#define ABSTRACTMESSAGELITE_2_T1663706917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  AbstractMessageLite_2_t1663706917  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTMESSAGELITE_2_T1663706917_H
#ifndef BYTESTRING_T35393593_H
#define BYTESTRING_T35393593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ByteString
struct  ByteString_t35393593  : public RuntimeObject
{
public:
	// System.Byte[] Google.ProtocolBuffers.ByteString::bytes
	ByteU5BU5D_t4116647657* ___bytes_1;

public:
	inline static int32_t get_offset_of_bytes_1() { return static_cast<int32_t>(offsetof(ByteString_t35393593, ___bytes_1)); }
	inline ByteU5BU5D_t4116647657* get_bytes_1() const { return ___bytes_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_bytes_1() { return &___bytes_1; }
	inline void set_bytes_1(ByteU5BU5D_t4116647657* value)
	{
		___bytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_1), value);
	}
};

struct ByteString_t35393593_StaticFields
{
public:
	// Google.ProtocolBuffers.ByteString Google.ProtocolBuffers.ByteString::empty
	ByteString_t35393593 * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(ByteString_t35393593_StaticFields, ___empty_0)); }
	inline ByteString_t35393593 * get_empty_0() const { return ___empty_0; }
	inline ByteString_t35393593 ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(ByteString_t35393593 * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTESTRING_T35393593_H
#ifndef POPSICLELIST_1_T2780837186_H
#define POPSICLELIST_1_T2780837186_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>
struct  PopsicleList_1_t2780837186  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> Google.ProtocolBuffers.Collections.PopsicleList`1::items
	List_1_t2869341516 * ___items_2;
	// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(PopsicleList_1_t2780837186, ___items_2)); }
	inline List_1_t2869341516 * get_items_2() const { return ___items_2; }
	inline List_1_t2869341516 ** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(List_1_t2869341516 * value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier((&___items_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(PopsicleList_1_t2780837186, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct PopsicleList_1_t2780837186_StaticFields
{
public:
	// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1::CheckForNull
	bool ___CheckForNull_0;
	// T[] Google.ProtocolBuffers.Collections.PopsicleList`1::EmptySet
	SingleU5BU5D_t1444911251* ___EmptySet_1;

public:
	inline static int32_t get_offset_of_CheckForNull_0() { return static_cast<int32_t>(offsetof(PopsicleList_1_t2780837186_StaticFields, ___CheckForNull_0)); }
	inline bool get_CheckForNull_0() const { return ___CheckForNull_0; }
	inline bool* get_address_of_CheckForNull_0() { return &___CheckForNull_0; }
	inline void set_CheckForNull_0(bool value)
	{
		___CheckForNull_0 = value;
	}

	inline static int32_t get_offset_of_EmptySet_1() { return static_cast<int32_t>(offsetof(PopsicleList_1_t2780837186_StaticFields, ___EmptySet_1)); }
	inline SingleU5BU5D_t1444911251* get_EmptySet_1() const { return ___EmptySet_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_EmptySet_1() { return &___EmptySet_1; }
	inline void set_EmptySet_1(SingleU5BU5D_t1444911251* value)
	{
		___EmptySet_1 = value;
		Il2CppCodeGenWriteBarrier((&___EmptySet_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPSICLELIST_1_T2780837186_H
#ifndef POPSICLELIST_1_T1233628674_H
#define POPSICLELIST_1_T1233628674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>
struct  PopsicleList_1_t1233628674  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> Google.ProtocolBuffers.Collections.PopsicleList`1::items
	List_1_t1322133004 * ___items_2;
	// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(PopsicleList_1_t1233628674, ___items_2)); }
	inline List_1_t1322133004 * get_items_2() const { return ___items_2; }
	inline List_1_t1322133004 ** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(List_1_t1322133004 * value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier((&___items_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(PopsicleList_1_t1233628674, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct PopsicleList_1_t1233628674_StaticFields
{
public:
	// System.Boolean Google.ProtocolBuffers.Collections.PopsicleList`1::CheckForNull
	bool ___CheckForNull_0;
	// T[] Google.ProtocolBuffers.Collections.PopsicleList`1::EmptySet
	PointerU5BU5D_t2544723155* ___EmptySet_1;

public:
	inline static int32_t get_offset_of_CheckForNull_0() { return static_cast<int32_t>(offsetof(PopsicleList_1_t1233628674_StaticFields, ___CheckForNull_0)); }
	inline bool get_CheckForNull_0() const { return ___CheckForNull_0; }
	inline bool* get_address_of_CheckForNull_0() { return &___CheckForNull_0; }
	inline void set_CheckForNull_0(bool value)
	{
		___CheckForNull_0 = value;
	}

	inline static int32_t get_offset_of_EmptySet_1() { return static_cast<int32_t>(offsetof(PopsicleList_1_t1233628674_StaticFields, ___EmptySet_1)); }
	inline PointerU5BU5D_t2544723155* get_EmptySet_1() const { return ___EmptySet_1; }
	inline PointerU5BU5D_t2544723155** get_address_of_EmptySet_1() { return &___EmptySet_1; }
	inline void set_EmptySet_1(PointerU5BU5D_t2544723155* value)
	{
		___EmptySet_1 = value;
		Il2CppCodeGenWriteBarrier((&___EmptySet_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POPSICLELIST_1_T1233628674_H
#ifndef EXTENSIONREGISTRY_T4271428238_H
#define EXTENSIONREGISTRY_T4271428238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.ExtensionRegistry
struct  ExtensionRegistry_t4271428238  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,Google.ProtocolBuffers.IGeneratedExtensionLite>> Google.ProtocolBuffers.ExtensionRegistry::extensionsByName
	Dictionary_2_t3107168633 * ___extensionsByName_1;
	// System.Collections.Generic.Dictionary`2<Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair,Google.ProtocolBuffers.IGeneratedExtensionLite> Google.ProtocolBuffers.ExtensionRegistry::extensionsByNumber
	Dictionary_2_t294577696 * ___extensionsByNumber_2;
	// System.Boolean Google.ProtocolBuffers.ExtensionRegistry::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_extensionsByName_1() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4271428238, ___extensionsByName_1)); }
	inline Dictionary_2_t3107168633 * get_extensionsByName_1() const { return ___extensionsByName_1; }
	inline Dictionary_2_t3107168633 ** get_address_of_extensionsByName_1() { return &___extensionsByName_1; }
	inline void set_extensionsByName_1(Dictionary_2_t3107168633 * value)
	{
		___extensionsByName_1 = value;
		Il2CppCodeGenWriteBarrier((&___extensionsByName_1), value);
	}

	inline static int32_t get_offset_of_extensionsByNumber_2() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4271428238, ___extensionsByNumber_2)); }
	inline Dictionary_2_t294577696 * get_extensionsByNumber_2() const { return ___extensionsByNumber_2; }
	inline Dictionary_2_t294577696 ** get_address_of_extensionsByNumber_2() { return &___extensionsByNumber_2; }
	inline void set_extensionsByNumber_2(Dictionary_2_t294577696 * value)
	{
		___extensionsByNumber_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensionsByNumber_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4271428238, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct ExtensionRegistry_t4271428238_StaticFields
{
public:
	// Google.ProtocolBuffers.ExtensionRegistry Google.ProtocolBuffers.ExtensionRegistry::empty
	ExtensionRegistry_t4271428238 * ___empty_0;

public:
	inline static int32_t get_offset_of_empty_0() { return static_cast<int32_t>(offsetof(ExtensionRegistry_t4271428238_StaticFields, ___empty_0)); }
	inline ExtensionRegistry_t4271428238 * get_empty_0() const { return ___empty_0; }
	inline ExtensionRegistry_t4271428238 ** get_address_of_empty_0() { return &___empty_0; }
	inline void set_empty_0(ExtensionRegistry_t4271428238 * value)
	{
		___empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONREGISTRY_T4271428238_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef TEXTWRITER_T3478189236_H
#define TEXTWRITER_T3478189236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter
struct  TextWriter_t3478189236  : public RuntimeObject
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t3528271667* ___CoreNewLine_0;
	// System.IFormatProvider System.IO.TextWriter::internalFormatProvider
	RuntimeObject* ___internalFormatProvider_1;

public:
	inline static int32_t get_offset_of_CoreNewLine_0() { return static_cast<int32_t>(offsetof(TextWriter_t3478189236, ___CoreNewLine_0)); }
	inline CharU5BU5D_t3528271667* get_CoreNewLine_0() const { return ___CoreNewLine_0; }
	inline CharU5BU5D_t3528271667** get_address_of_CoreNewLine_0() { return &___CoreNewLine_0; }
	inline void set_CoreNewLine_0(CharU5BU5D_t3528271667* value)
	{
		___CoreNewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___CoreNewLine_0), value);
	}

	inline static int32_t get_offset_of_internalFormatProvider_1() { return static_cast<int32_t>(offsetof(TextWriter_t3478189236, ___internalFormatProvider_1)); }
	inline RuntimeObject* get_internalFormatProvider_1() const { return ___internalFormatProvider_1; }
	inline RuntimeObject** get_address_of_internalFormatProvider_1() { return &___internalFormatProvider_1; }
	inline void set_internalFormatProvider_1(RuntimeObject* value)
	{
		___internalFormatProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&___internalFormatProvider_1), value);
	}
};

struct TextWriter_t3478189236_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t3478189236 * ___Null_2;

public:
	inline static int32_t get_offset_of_Null_2() { return static_cast<int32_t>(offsetof(TextWriter_t3478189236_StaticFields, ___Null_2)); }
	inline TextWriter_t3478189236 * get_Null_2() const { return ___Null_2; }
	inline TextWriter_t3478189236 ** get_address_of_Null_2() { return &___Null_2; }
	inline void set_Null_2(TextWriter_t3478189236 * value)
	{
		___Null_2 = value;
		Il2CppCodeGenWriteBarrier((&___Null_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTWRITER_T3478189236_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STRINGCOMPARER_T3301955079_H
#define STRINGCOMPARER_T3301955079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringComparer
struct  StringComparer_t3301955079  : public RuntimeObject
{
public:

public:
};

struct StringComparer_t3301955079_StaticFields
{
public:
	// System.StringComparer System.StringComparer::invariantCultureIgnoreCase
	StringComparer_t3301955079 * ___invariantCultureIgnoreCase_0;
	// System.StringComparer System.StringComparer::invariantCulture
	StringComparer_t3301955079 * ___invariantCulture_1;
	// System.StringComparer System.StringComparer::ordinalIgnoreCase
	StringComparer_t3301955079 * ___ordinalIgnoreCase_2;
	// System.StringComparer System.StringComparer::ordinal
	StringComparer_t3301955079 * ___ordinal_3;

public:
	inline static int32_t get_offset_of_invariantCultureIgnoreCase_0() { return static_cast<int32_t>(offsetof(StringComparer_t3301955079_StaticFields, ___invariantCultureIgnoreCase_0)); }
	inline StringComparer_t3301955079 * get_invariantCultureIgnoreCase_0() const { return ___invariantCultureIgnoreCase_0; }
	inline StringComparer_t3301955079 ** get_address_of_invariantCultureIgnoreCase_0() { return &___invariantCultureIgnoreCase_0; }
	inline void set_invariantCultureIgnoreCase_0(StringComparer_t3301955079 * value)
	{
		___invariantCultureIgnoreCase_0 = value;
		Il2CppCodeGenWriteBarrier((&___invariantCultureIgnoreCase_0), value);
	}

	inline static int32_t get_offset_of_invariantCulture_1() { return static_cast<int32_t>(offsetof(StringComparer_t3301955079_StaticFields, ___invariantCulture_1)); }
	inline StringComparer_t3301955079 * get_invariantCulture_1() const { return ___invariantCulture_1; }
	inline StringComparer_t3301955079 ** get_address_of_invariantCulture_1() { return &___invariantCulture_1; }
	inline void set_invariantCulture_1(StringComparer_t3301955079 * value)
	{
		___invariantCulture_1 = value;
		Il2CppCodeGenWriteBarrier((&___invariantCulture_1), value);
	}

	inline static int32_t get_offset_of_ordinalIgnoreCase_2() { return static_cast<int32_t>(offsetof(StringComparer_t3301955079_StaticFields, ___ordinalIgnoreCase_2)); }
	inline StringComparer_t3301955079 * get_ordinalIgnoreCase_2() const { return ___ordinalIgnoreCase_2; }
	inline StringComparer_t3301955079 ** get_address_of_ordinalIgnoreCase_2() { return &___ordinalIgnoreCase_2; }
	inline void set_ordinalIgnoreCase_2(StringComparer_t3301955079 * value)
	{
		___ordinalIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___ordinalIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_ordinal_3() { return static_cast<int32_t>(offsetof(StringComparer_t3301955079_StaticFields, ___ordinal_3)); }
	inline StringComparer_t3301955079 * get_ordinal_3() const { return ___ordinal_3; }
	inline StringComparer_t3301955079 ** get_address_of_ordinal_3() { return &___ordinal_3; }
	inline void set_ordinal_3(StringComparer_t3301955079 * value)
	{
		___ordinal_3 = value;
		Il2CppCodeGenWriteBarrier((&___ordinal_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOMPARER_T3301955079_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef TYPES_T363369143_H
#define TYPES_T363369143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Types
struct  Types_t363369143  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPES_T363369143_H
#ifndef PHONEEVENT_T3448566392_H
#define PHONEEVENT_T3448566392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.Proto.PhoneEvent
struct  PhoneEvent_t3448566392  : public RuntimeObject
{
public:

public:
};

struct PhoneEvent_t3448566392_StaticFields
{
public:
	// System.Object proto.Proto.PhoneEvent::Descriptor
	RuntimeObject * ___Descriptor_0;

public:
	inline static int32_t get_offset_of_Descriptor_0() { return static_cast<int32_t>(offsetof(PhoneEvent_t3448566392_StaticFields, ___Descriptor_0)); }
	inline RuntimeObject * get_Descriptor_0() const { return ___Descriptor_0; }
	inline RuntimeObject ** get_address_of_Descriptor_0() { return &___Descriptor_0; }
	inline void set_Descriptor_0(RuntimeObject * value)
	{
		___Descriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___Descriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHONEEVENT_T3448566392_H
#ifndef U24ARRAYTYPEU3D12_T2488454198_H
#define U24ARRAYTYPEU3D12_T2488454198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454198 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454198__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454198_H
#ifndef U24ARRAYTYPEU3D16_T3253128244_H
#define U24ARRAYTYPEU3D16_T3253128244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128244 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128244__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128244_H
#ifndef U24ARRAYTYPEU3D20_T1702832645_H
#define U24ARRAYTYPEU3D20_T1702832645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t1702832645 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t1702832645__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T1702832645_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef U24ARRAYTYPEU3D28_T173484549_H
#define U24ARRAYTYPEU3D28_T173484549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=28
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D28_t173484549 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D28_t173484549__padding[28];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D28_T173484549_H
#ifndef GENERATEDBUILDERLITE_2_T861404538_H
#define GENERATEDBUILDERLITE_2_T861404538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct  GeneratedBuilderLite_2_t861404538  : public AbstractBuilderLite_2_t1753171341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T861404538_H
#ifndef GENERATEDBUILDERLITE_2_T3925513688_H
#define GENERATEDBUILDERLITE_2_T3925513688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct  GeneratedBuilderLite_2_t3925513688  : public AbstractBuilderLite_2_t522313195
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3925513688_H
#ifndef GENERATEDBUILDERLITE_2_T3171452996_H
#define GENERATEDBUILDERLITE_2_T3171452996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  GeneratedBuilderLite_2_t3171452996  : public AbstractBuilderLite_2_t4063219799
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3171452996_H
#ifndef GENERATEDBUILDERLITE_2_T3661349574_H
#define GENERATEDBUILDERLITE_2_T3661349574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct  GeneratedBuilderLite_2_t3661349574  : public AbstractBuilderLite_2_t258149081
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3661349574_H
#ifndef GENERATEDBUILDERLITE_2_T751282667_H
#define GENERATEDBUILDERLITE_2_T751282667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct  GeneratedBuilderLite_2_t751282667  : public AbstractBuilderLite_2_t1643049470
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T751282667_H
#ifndef GENERATEDBUILDERLITE_2_T3142848435_H
#define GENERATEDBUILDERLITE_2_T3142848435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  GeneratedBuilderLite_2_t3142848435  : public AbstractBuilderLite_2_t4034615238
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDBUILDERLITE_2_T3142848435_H
#ifndef GENERATEDMESSAGELITE_2_T1471539120_H
#define GENERATEDMESSAGELITE_2_T1471539120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>
struct  GeneratedMessageLite_2_t1471539120  : public AbstractMessageLite_2_t3677230316
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T1471539120_H
#ifndef GENERATEDMESSAGELITE_2_T240680974_H
#define GENERATEDMESSAGELITE_2_T240680974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>
struct  GeneratedMessageLite_2_t240680974  : public AbstractMessageLite_2_t2446372170
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T240680974_H
#ifndef GENERATEDMESSAGELITE_2_T3781587578_H
#define GENERATEDMESSAGELITE_2_T3781587578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>
struct  GeneratedMessageLite_2_t3781587578  : public AbstractMessageLite_2_t1692311478
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T3781587578_H
#ifndef GENERATEDMESSAGELITE_2_T4271484156_H
#define GENERATEDMESSAGELITE_2_T4271484156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>
struct  GeneratedMessageLite_2_t4271484156  : public AbstractMessageLite_2_t2182208056
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T4271484156_H
#ifndef GENERATEDMESSAGELITE_2_T1361417249_H
#define GENERATEDMESSAGELITE_2_T1361417249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>
struct  GeneratedMessageLite_2_t1361417249  : public AbstractMessageLite_2_t3567108445
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T1361417249_H
#ifndef GENERATEDMESSAGELITE_2_T3752983017_H
#define GENERATEDMESSAGELITE_2_T3752983017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>
struct  GeneratedMessageLite_2_t3752983017  : public AbstractMessageLite_2_t1663706917
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERATEDMESSAGELITE_2_T3752983017_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255367  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-311441405B64B3EA9097AC8E07F3274962EC6BB4
	U24ArrayTypeU3D12_t2488454198  ___U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-16E2B412E9C2B8E31B780DE46254349320CCAAA0
	U24ArrayTypeU3D12_t2488454198  ___U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-D7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2;
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-25B4B83D2A43393F4E18624598DDA694217A6622
	U24ArrayTypeU3D16_t3253128244  ___U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3;
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-FADC743710841EB901D5F6FBC97F555D4BD94310
	U24ArrayTypeU3D20_t1702832645  ___U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4;
	// <PrivateImplementationDetails>/$ArrayType=28 <PrivateImplementationDetails>::$field-C34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F
	U24ArrayTypeU3D28_t173484549  ___U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-51A7A390CD6DE245186881400B18C9D822EFE240
	U24ArrayTypeU3D12_t2488454198  ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-C90F38A020811481753795774EB5AF353F414C59
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7;

public:
	inline static int32_t get_offset_of_U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0)); }
	inline U24ArrayTypeU3D12_t2488454198  get_U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0() const { return ___U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0; }
	inline U24ArrayTypeU3D12_t2488454198 * get_address_of_U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0() { return &___U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0; }
	inline void set_U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0(U24ArrayTypeU3D12_t2488454198  value)
	{
		___U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1)); }
	inline U24ArrayTypeU3D12_t2488454198  get_U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1() const { return ___U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1; }
	inline U24ArrayTypeU3D12_t2488454198 * get_address_of_U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1() { return &___U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1; }
	inline void set_U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1(U24ArrayTypeU3D12_t2488454198  value)
	{
		___U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2() const { return ___U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2() { return &___U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2; }
	inline void set_U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3)); }
	inline U24ArrayTypeU3D16_t3253128244  get_U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3() const { return ___U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3; }
	inline U24ArrayTypeU3D16_t3253128244 * get_address_of_U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3() { return &___U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3; }
	inline void set_U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3(U24ArrayTypeU3D16_t3253128244  value)
	{
		___U24fieldU2D25B4B83D2A43393F4E18624598DDA694217A6622_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4)); }
	inline U24ArrayTypeU3D20_t1702832645  get_U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4() const { return ___U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4; }
	inline U24ArrayTypeU3D20_t1702832645 * get_address_of_U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4() { return &___U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4; }
	inline void set_U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4(U24ArrayTypeU3D20_t1702832645  value)
	{
		___U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5)); }
	inline U24ArrayTypeU3D28_t173484549  get_U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5() const { return ___U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5; }
	inline U24ArrayTypeU3D28_t173484549 * get_address_of_U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5() { return &___U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5; }
	inline void set_U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5(U24ArrayTypeU3D28_t173484549  value)
	{
		___U24fieldU2DC34ABF0A6BE7F2D67E7997A058AA0AA6985FFE6F_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6)); }
	inline U24ArrayTypeU3D12_t2488454198  get_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6() const { return ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6; }
	inline U24ArrayTypeU3D12_t2488454198 * get_address_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6() { return &___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6; }
	inline void set_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6(U24ArrayTypeU3D12_t2488454198  value)
	{
		___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields, ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7() const { return ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7() { return &___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7; }
	inline void set_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255367_H
#ifndef IOEXCEPTION_T4088381929_H
#define IOEXCEPTION_T4088381929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t4088381929  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T4088381929_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef RUNTIMEFIELDHANDLE_T1871169219_H
#define RUNTIMEFIELDHANDLE_T1871169219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t1871169219 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t1871169219, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T1871169219_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef DEPTHMAPEVENT_T729886054_H
#define DEPTHMAPEVENT_T729886054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/DepthMapEvent
struct  DepthMapEvent_t729886054  : public GeneratedMessageLite_2_t1471539120
{
public:
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/DepthMapEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::hasWidth
	bool ___hasWidth_7;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::width_
	int32_t ___width__8;
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::hasHeight
	bool ___hasHeight_10;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::height_
	int32_t ___height__11;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::zDistancesMemoizedSerializedSize
	int32_t ___zDistancesMemoizedSerializedSize_13;
	// Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single> proto.PhoneEvent/Types/DepthMapEvent::zDistances_
	PopsicleList_1_t2780837186 * ___zDistances__14;
	// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_15;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasWidth_7() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___hasWidth_7)); }
	inline bool get_hasWidth_7() const { return ___hasWidth_7; }
	inline bool* get_address_of_hasWidth_7() { return &___hasWidth_7; }
	inline void set_hasWidth_7(bool value)
	{
		___hasWidth_7 = value;
	}

	inline static int32_t get_offset_of_width__8() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___width__8)); }
	inline int32_t get_width__8() const { return ___width__8; }
	inline int32_t* get_address_of_width__8() { return &___width__8; }
	inline void set_width__8(int32_t value)
	{
		___width__8 = value;
	}

	inline static int32_t get_offset_of_hasHeight_10() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___hasHeight_10)); }
	inline bool get_hasHeight_10() const { return ___hasHeight_10; }
	inline bool* get_address_of_hasHeight_10() { return &___hasHeight_10; }
	inline void set_hasHeight_10(bool value)
	{
		___hasHeight_10 = value;
	}

	inline static int32_t get_offset_of_height__11() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___height__11)); }
	inline int32_t get_height__11() const { return ___height__11; }
	inline int32_t* get_address_of_height__11() { return &___height__11; }
	inline void set_height__11(int32_t value)
	{
		___height__11 = value;
	}

	inline static int32_t get_offset_of_zDistancesMemoizedSerializedSize_13() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___zDistancesMemoizedSerializedSize_13)); }
	inline int32_t get_zDistancesMemoizedSerializedSize_13() const { return ___zDistancesMemoizedSerializedSize_13; }
	inline int32_t* get_address_of_zDistancesMemoizedSerializedSize_13() { return &___zDistancesMemoizedSerializedSize_13; }
	inline void set_zDistancesMemoizedSerializedSize_13(int32_t value)
	{
		___zDistancesMemoizedSerializedSize_13 = value;
	}

	inline static int32_t get_offset_of_zDistances__14() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___zDistances__14)); }
	inline PopsicleList_1_t2780837186 * get_zDistances__14() const { return ___zDistances__14; }
	inline PopsicleList_1_t2780837186 ** get_address_of_zDistances__14() { return &___zDistances__14; }
	inline void set_zDistances__14(PopsicleList_1_t2780837186 * value)
	{
		___zDistances__14 = value;
		Il2CppCodeGenWriteBarrier((&___zDistances__14), value);
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_15() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054, ___memoizedSerializedSize_15)); }
	inline int32_t get_memoizedSerializedSize_15() const { return ___memoizedSerializedSize_15; }
	inline int32_t* get_address_of_memoizedSerializedSize_15() { return &___memoizedSerializedSize_15; }
	inline void set_memoizedSerializedSize_15(int32_t value)
	{
		___memoizedSerializedSize_15 = value;
	}
};

struct DepthMapEvent_t729886054_StaticFields
{
public:
	// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent::defaultInstance
	DepthMapEvent_t729886054 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/DepthMapEvent::_depthMapEventFieldNames
	StringU5BU5D_t1281789340* ____depthMapEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/DepthMapEvent::_depthMapEventFieldTags
	UInt32U5BU5D_t2770800703* ____depthMapEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054_StaticFields, ___defaultInstance_0)); }
	inline DepthMapEvent_t729886054 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline DepthMapEvent_t729886054 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(DepthMapEvent_t729886054 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__depthMapEventFieldNames_1() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054_StaticFields, ____depthMapEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__depthMapEventFieldNames_1() const { return ____depthMapEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__depthMapEventFieldNames_1() { return &____depthMapEventFieldNames_1; }
	inline void set__depthMapEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____depthMapEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____depthMapEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__depthMapEventFieldTags_2() { return static_cast<int32_t>(offsetof(DepthMapEvent_t729886054_StaticFields, ____depthMapEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__depthMapEventFieldTags_2() const { return ____depthMapEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__depthMapEventFieldTags_2() { return &____depthMapEventFieldTags_2; }
	inline void set__depthMapEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____depthMapEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____depthMapEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHMAPEVENT_T729886054_H
#ifndef BUILDER_T1293396100_H
#define BUILDER_T1293396100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/DepthMapEvent/Builder
struct  Builder_t1293396100  : public GeneratedBuilderLite_2_t861404538
{
public:
	// System.Boolean proto.PhoneEvent/Types/DepthMapEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent/Builder::result
	DepthMapEvent_t729886054 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t1293396100, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t1293396100, ___result_1)); }
	inline DepthMapEvent_t729886054 * get_result_1() const { return ___result_1; }
	inline DepthMapEvent_t729886054 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(DepthMapEvent_t729886054 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T1293396100_H
#ifndef GYROSCOPEEVENT_T127774954_H
#define GYROSCOPEEVENT_T127774954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/GyroscopeEvent
struct  GyroscopeEvent_t127774954  : public GeneratedMessageLite_2_t240680974
{
public:
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/GyroscopeEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent/Types/GyroscopeEvent::z_
	float ___z__14;
	// System.Int32 proto.PhoneEvent/Types/GyroscopeEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_15;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_15() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954, ___memoizedSerializedSize_15)); }
	inline int32_t get_memoizedSerializedSize_15() const { return ___memoizedSerializedSize_15; }
	inline int32_t* get_address_of_memoizedSerializedSize_15() { return &___memoizedSerializedSize_15; }
	inline void set_memoizedSerializedSize_15(int32_t value)
	{
		___memoizedSerializedSize_15 = value;
	}
};

struct GyroscopeEvent_t127774954_StaticFields
{
public:
	// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::defaultInstance
	GyroscopeEvent_t127774954 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/GyroscopeEvent::_gyroscopeEventFieldNames
	StringU5BU5D_t1281789340* ____gyroscopeEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/GyroscopeEvent::_gyroscopeEventFieldTags
	UInt32U5BU5D_t2770800703* ____gyroscopeEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954_StaticFields, ___defaultInstance_0)); }
	inline GyroscopeEvent_t127774954 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline GyroscopeEvent_t127774954 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(GyroscopeEvent_t127774954 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__gyroscopeEventFieldNames_1() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954_StaticFields, ____gyroscopeEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__gyroscopeEventFieldNames_1() const { return ____gyroscopeEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__gyroscopeEventFieldNames_1() { return &____gyroscopeEventFieldNames_1; }
	inline void set__gyroscopeEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____gyroscopeEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____gyroscopeEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__gyroscopeEventFieldTags_2() { return static_cast<int32_t>(offsetof(GyroscopeEvent_t127774954_StaticFields, ____gyroscopeEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__gyroscopeEventFieldTags_2() const { return ____gyroscopeEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__gyroscopeEventFieldTags_2() { return &____gyroscopeEventFieldTags_2; }
	inline void set__gyroscopeEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____gyroscopeEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____gyroscopeEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROSCOPEEVENT_T127774954_H
#ifndef BUILDER_T3442751222_H
#define BUILDER_T3442751222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/GyroscopeEvent/Builder
struct  Builder_t3442751222  : public GeneratedBuilderLite_2_t3925513688
{
public:
	// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent/Builder::result
	GyroscopeEvent_t127774954 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t3442751222, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t3442751222, ___result_1)); }
	inline GyroscopeEvent_t127774954 * get_result_1() const { return ___result_1; }
	inline GyroscopeEvent_t127774954 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(GyroscopeEvent_t127774954 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T3442751222_H
#ifndef KEYEVENT_T1937114521_H
#define KEYEVENT_T1937114521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/KeyEvent
struct  KeyEvent_t1937114521  : public GeneratedMessageLite_2_t3781587578
{
public:
	// System.Boolean proto.PhoneEvent/Types/KeyEvent::hasAction
	bool ___hasAction_4;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::action_
	int32_t ___action__5;
	// System.Boolean proto.PhoneEvent/Types/KeyEvent::hasCode
	bool ___hasCode_7;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::code_
	int32_t ___code__8;
	// System.Int32 proto.PhoneEvent/Types/KeyEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_9;

public:
	inline static int32_t get_offset_of_hasAction_4() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___hasAction_4)); }
	inline bool get_hasAction_4() const { return ___hasAction_4; }
	inline bool* get_address_of_hasAction_4() { return &___hasAction_4; }
	inline void set_hasAction_4(bool value)
	{
		___hasAction_4 = value;
	}

	inline static int32_t get_offset_of_action__5() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___action__5)); }
	inline int32_t get_action__5() const { return ___action__5; }
	inline int32_t* get_address_of_action__5() { return &___action__5; }
	inline void set_action__5(int32_t value)
	{
		___action__5 = value;
	}

	inline static int32_t get_offset_of_hasCode_7() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___hasCode_7)); }
	inline bool get_hasCode_7() const { return ___hasCode_7; }
	inline bool* get_address_of_hasCode_7() { return &___hasCode_7; }
	inline void set_hasCode_7(bool value)
	{
		___hasCode_7 = value;
	}

	inline static int32_t get_offset_of_code__8() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___code__8)); }
	inline int32_t get_code__8() const { return ___code__8; }
	inline int32_t* get_address_of_code__8() { return &___code__8; }
	inline void set_code__8(int32_t value)
	{
		___code__8 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_9() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521, ___memoizedSerializedSize_9)); }
	inline int32_t get_memoizedSerializedSize_9() const { return ___memoizedSerializedSize_9; }
	inline int32_t* get_address_of_memoizedSerializedSize_9() { return &___memoizedSerializedSize_9; }
	inline void set_memoizedSerializedSize_9(int32_t value)
	{
		___memoizedSerializedSize_9 = value;
	}
};

struct KeyEvent_t1937114521_StaticFields
{
public:
	// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::defaultInstance
	KeyEvent_t1937114521 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/KeyEvent::_keyEventFieldNames
	StringU5BU5D_t1281789340* ____keyEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/KeyEvent::_keyEventFieldTags
	UInt32U5BU5D_t2770800703* ____keyEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521_StaticFields, ___defaultInstance_0)); }
	inline KeyEvent_t1937114521 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline KeyEvent_t1937114521 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(KeyEvent_t1937114521 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__keyEventFieldNames_1() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521_StaticFields, ____keyEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__keyEventFieldNames_1() const { return ____keyEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__keyEventFieldNames_1() { return &____keyEventFieldNames_1; }
	inline void set__keyEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____keyEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____keyEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__keyEventFieldTags_2() { return static_cast<int32_t>(offsetof(KeyEvent_t1937114521_StaticFields, ____keyEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__keyEventFieldTags_2() const { return ____keyEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__keyEventFieldTags_2() { return &____keyEventFieldTags_2; }
	inline void set__keyEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____keyEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEVENT_T1937114521_H
#ifndef BUILDER_T2712992173_H
#define BUILDER_T2712992173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/KeyEvent/Builder
struct  Builder_t2712992173  : public GeneratedBuilderLite_2_t3171452996
{
public:
	// System.Boolean proto.PhoneEvent/Types/KeyEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent/Builder::result
	KeyEvent_t1937114521 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t2712992173, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t2712992173, ___result_1)); }
	inline KeyEvent_t1937114521 * get_result_1() const { return ___result_1; }
	inline KeyEvent_t1937114521 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(KeyEvent_t1937114521 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2712992173_H
#ifndef MOTIONEVENT_T3121383016_H
#define MOTIONEVENT_T3121383016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent
struct  MotionEvent_t3121383016  : public GeneratedMessageLite_2_t4271484156
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/MotionEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent::hasAction
	bool ___hasAction_7;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent::action_
	int32_t ___action__8;
	// Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer> proto.PhoneEvent/Types/MotionEvent::pointers_
	PopsicleList_1_t1233628674 * ___pointers__10;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_11;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasAction_7() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___hasAction_7)); }
	inline bool get_hasAction_7() const { return ___hasAction_7; }
	inline bool* get_address_of_hasAction_7() { return &___hasAction_7; }
	inline void set_hasAction_7(bool value)
	{
		___hasAction_7 = value;
	}

	inline static int32_t get_offset_of_action__8() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___action__8)); }
	inline int32_t get_action__8() const { return ___action__8; }
	inline int32_t* get_address_of_action__8() { return &___action__8; }
	inline void set_action__8(int32_t value)
	{
		___action__8 = value;
	}

	inline static int32_t get_offset_of_pointers__10() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___pointers__10)); }
	inline PopsicleList_1_t1233628674 * get_pointers__10() const { return ___pointers__10; }
	inline PopsicleList_1_t1233628674 ** get_address_of_pointers__10() { return &___pointers__10; }
	inline void set_pointers__10(PopsicleList_1_t1233628674 * value)
	{
		___pointers__10 = value;
		Il2CppCodeGenWriteBarrier((&___pointers__10), value);
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_11() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016, ___memoizedSerializedSize_11)); }
	inline int32_t get_memoizedSerializedSize_11() const { return ___memoizedSerializedSize_11; }
	inline int32_t* get_address_of_memoizedSerializedSize_11() { return &___memoizedSerializedSize_11; }
	inline void set_memoizedSerializedSize_11(int32_t value)
	{
		___memoizedSerializedSize_11 = value;
	}
};

struct MotionEvent_t3121383016_StaticFields
{
public:
	// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::defaultInstance
	MotionEvent_t3121383016 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/MotionEvent::_motionEventFieldNames
	StringU5BU5D_t1281789340* ____motionEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/MotionEvent::_motionEventFieldTags
	UInt32U5BU5D_t2770800703* ____motionEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016_StaticFields, ___defaultInstance_0)); }
	inline MotionEvent_t3121383016 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline MotionEvent_t3121383016 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(MotionEvent_t3121383016 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__motionEventFieldNames_1() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016_StaticFields, ____motionEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__motionEventFieldNames_1() const { return ____motionEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__motionEventFieldNames_1() { return &____motionEventFieldNames_1; }
	inline void set__motionEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____motionEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____motionEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__motionEventFieldTags_2() { return static_cast<int32_t>(offsetof(MotionEvent_t3121383016_StaticFields, ____motionEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__motionEventFieldTags_2() const { return ____motionEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__motionEventFieldTags_2() { return &____motionEventFieldTags_2; }
	inline void set__motionEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____motionEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____motionEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONEVENT_T3121383016_H
#ifndef BUILDER_T2961114394_H
#define BUILDER_T2961114394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Builder
struct  Builder_t2961114394  : public GeneratedBuilderLite_2_t3661349574
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent/Builder::result
	MotionEvent_t3121383016 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t2961114394, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t2961114394, ___result_1)); }
	inline MotionEvent_t3121383016 * get_result_1() const { return ___result_1; }
	inline MotionEvent_t3121383016 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(MotionEvent_t3121383016 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2961114394_H
#ifndef POINTER_T4145025558_H
#define POINTER_T4145025558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Types/Pointer
struct  Pointer_t4145025558  : public GeneratedMessageLite_2_t1361417249
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::hasId
	bool ___hasId_4;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::id_
	int32_t ___id__5;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::hasNormalizedX
	bool ___hasNormalizedX_7;
	// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::normalizedX_
	float ___normalizedX__8;
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::hasNormalizedY
	bool ___hasNormalizedY_10;
	// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::normalizedY_
	float ___normalizedY__11;
	// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_12;

public:
	inline static int32_t get_offset_of_hasId_4() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___hasId_4)); }
	inline bool get_hasId_4() const { return ___hasId_4; }
	inline bool* get_address_of_hasId_4() { return &___hasId_4; }
	inline void set_hasId_4(bool value)
	{
		___hasId_4 = value;
	}

	inline static int32_t get_offset_of_id__5() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___id__5)); }
	inline int32_t get_id__5() const { return ___id__5; }
	inline int32_t* get_address_of_id__5() { return &___id__5; }
	inline void set_id__5(int32_t value)
	{
		___id__5 = value;
	}

	inline static int32_t get_offset_of_hasNormalizedX_7() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___hasNormalizedX_7)); }
	inline bool get_hasNormalizedX_7() const { return ___hasNormalizedX_7; }
	inline bool* get_address_of_hasNormalizedX_7() { return &___hasNormalizedX_7; }
	inline void set_hasNormalizedX_7(bool value)
	{
		___hasNormalizedX_7 = value;
	}

	inline static int32_t get_offset_of_normalizedX__8() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___normalizedX__8)); }
	inline float get_normalizedX__8() const { return ___normalizedX__8; }
	inline float* get_address_of_normalizedX__8() { return &___normalizedX__8; }
	inline void set_normalizedX__8(float value)
	{
		___normalizedX__8 = value;
	}

	inline static int32_t get_offset_of_hasNormalizedY_10() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___hasNormalizedY_10)); }
	inline bool get_hasNormalizedY_10() const { return ___hasNormalizedY_10; }
	inline bool* get_address_of_hasNormalizedY_10() { return &___hasNormalizedY_10; }
	inline void set_hasNormalizedY_10(bool value)
	{
		___hasNormalizedY_10 = value;
	}

	inline static int32_t get_offset_of_normalizedY__11() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___normalizedY__11)); }
	inline float get_normalizedY__11() const { return ___normalizedY__11; }
	inline float* get_address_of_normalizedY__11() { return &___normalizedY__11; }
	inline void set_normalizedY__11(float value)
	{
		___normalizedY__11 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_12() { return static_cast<int32_t>(offsetof(Pointer_t4145025558, ___memoizedSerializedSize_12)); }
	inline int32_t get_memoizedSerializedSize_12() const { return ___memoizedSerializedSize_12; }
	inline int32_t* get_address_of_memoizedSerializedSize_12() { return &___memoizedSerializedSize_12; }
	inline void set_memoizedSerializedSize_12(int32_t value)
	{
		___memoizedSerializedSize_12 = value;
	}
};

struct Pointer_t4145025558_StaticFields
{
public:
	// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::defaultInstance
	Pointer_t4145025558 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/MotionEvent/Types/Pointer::_pointerFieldNames
	StringU5BU5D_t1281789340* ____pointerFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/MotionEvent/Types/Pointer::_pointerFieldTags
	UInt32U5BU5D_t2770800703* ____pointerFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(Pointer_t4145025558_StaticFields, ___defaultInstance_0)); }
	inline Pointer_t4145025558 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline Pointer_t4145025558 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(Pointer_t4145025558 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__pointerFieldNames_1() { return static_cast<int32_t>(offsetof(Pointer_t4145025558_StaticFields, ____pointerFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__pointerFieldNames_1() const { return ____pointerFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__pointerFieldNames_1() { return &____pointerFieldNames_1; }
	inline void set__pointerFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____pointerFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____pointerFieldNames_1), value);
	}

	inline static int32_t get_offset_of__pointerFieldTags_2() { return static_cast<int32_t>(offsetof(Pointer_t4145025558_StaticFields, ____pointerFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__pointerFieldTags_2() const { return ____pointerFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__pointerFieldTags_2() { return &____pointerFieldTags_2; }
	inline void set__pointerFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____pointerFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____pointerFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTER_T4145025558_H
#ifndef BUILDER_T582111845_H
#define BUILDER_T582111845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder
struct  Builder_t582111845  : public GeneratedBuilderLite_2_t751282667
{
public:
	// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::result
	Pointer_t4145025558 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t582111845, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t582111845, ___result_1)); }
	inline Pointer_t4145025558 * get_result_1() const { return ___result_1; }
	inline Pointer_t4145025558 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(Pointer_t4145025558 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T582111845_H
#ifndef ORIENTATIONEVENT_T158247624_H
#define ORIENTATIONEVENT_T158247624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/OrientationEvent
struct  OrientationEvent_t158247624  : public GeneratedMessageLite_2_t3752983017
{
public:
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasTimestamp
	bool ___hasTimestamp_4;
	// System.Int64 proto.PhoneEvent/Types/OrientationEvent::timestamp_
	int64_t ___timestamp__5;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasX
	bool ___hasX_7;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::x_
	float ___x__8;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasY
	bool ___hasY_10;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::y_
	float ___y__11;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasZ
	bool ___hasZ_13;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::z_
	float ___z__14;
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent::hasW
	bool ___hasW_16;
	// System.Single proto.PhoneEvent/Types/OrientationEvent::w_
	float ___w__17;
	// System.Int32 proto.PhoneEvent/Types/OrientationEvent::memoizedSerializedSize
	int32_t ___memoizedSerializedSize_18;

public:
	inline static int32_t get_offset_of_hasTimestamp_4() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasTimestamp_4)); }
	inline bool get_hasTimestamp_4() const { return ___hasTimestamp_4; }
	inline bool* get_address_of_hasTimestamp_4() { return &___hasTimestamp_4; }
	inline void set_hasTimestamp_4(bool value)
	{
		___hasTimestamp_4 = value;
	}

	inline static int32_t get_offset_of_timestamp__5() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___timestamp__5)); }
	inline int64_t get_timestamp__5() const { return ___timestamp__5; }
	inline int64_t* get_address_of_timestamp__5() { return &___timestamp__5; }
	inline void set_timestamp__5(int64_t value)
	{
		___timestamp__5 = value;
	}

	inline static int32_t get_offset_of_hasX_7() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasX_7)); }
	inline bool get_hasX_7() const { return ___hasX_7; }
	inline bool* get_address_of_hasX_7() { return &___hasX_7; }
	inline void set_hasX_7(bool value)
	{
		___hasX_7 = value;
	}

	inline static int32_t get_offset_of_x__8() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___x__8)); }
	inline float get_x__8() const { return ___x__8; }
	inline float* get_address_of_x__8() { return &___x__8; }
	inline void set_x__8(float value)
	{
		___x__8 = value;
	}

	inline static int32_t get_offset_of_hasY_10() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasY_10)); }
	inline bool get_hasY_10() const { return ___hasY_10; }
	inline bool* get_address_of_hasY_10() { return &___hasY_10; }
	inline void set_hasY_10(bool value)
	{
		___hasY_10 = value;
	}

	inline static int32_t get_offset_of_y__11() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___y__11)); }
	inline float get_y__11() const { return ___y__11; }
	inline float* get_address_of_y__11() { return &___y__11; }
	inline void set_y__11(float value)
	{
		___y__11 = value;
	}

	inline static int32_t get_offset_of_hasZ_13() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasZ_13)); }
	inline bool get_hasZ_13() const { return ___hasZ_13; }
	inline bool* get_address_of_hasZ_13() { return &___hasZ_13; }
	inline void set_hasZ_13(bool value)
	{
		___hasZ_13 = value;
	}

	inline static int32_t get_offset_of_z__14() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___z__14)); }
	inline float get_z__14() const { return ___z__14; }
	inline float* get_address_of_z__14() { return &___z__14; }
	inline void set_z__14(float value)
	{
		___z__14 = value;
	}

	inline static int32_t get_offset_of_hasW_16() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___hasW_16)); }
	inline bool get_hasW_16() const { return ___hasW_16; }
	inline bool* get_address_of_hasW_16() { return &___hasW_16; }
	inline void set_hasW_16(bool value)
	{
		___hasW_16 = value;
	}

	inline static int32_t get_offset_of_w__17() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___w__17)); }
	inline float get_w__17() const { return ___w__17; }
	inline float* get_address_of_w__17() { return &___w__17; }
	inline void set_w__17(float value)
	{
		___w__17 = value;
	}

	inline static int32_t get_offset_of_memoizedSerializedSize_18() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624, ___memoizedSerializedSize_18)); }
	inline int32_t get_memoizedSerializedSize_18() const { return ___memoizedSerializedSize_18; }
	inline int32_t* get_address_of_memoizedSerializedSize_18() { return &___memoizedSerializedSize_18; }
	inline void set_memoizedSerializedSize_18(int32_t value)
	{
		___memoizedSerializedSize_18 = value;
	}
};

struct OrientationEvent_t158247624_StaticFields
{
public:
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::defaultInstance
	OrientationEvent_t158247624 * ___defaultInstance_0;
	// System.String[] proto.PhoneEvent/Types/OrientationEvent::_orientationEventFieldNames
	StringU5BU5D_t1281789340* ____orientationEventFieldNames_1;
	// System.UInt32[] proto.PhoneEvent/Types/OrientationEvent::_orientationEventFieldTags
	UInt32U5BU5D_t2770800703* ____orientationEventFieldTags_2;

public:
	inline static int32_t get_offset_of_defaultInstance_0() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624_StaticFields, ___defaultInstance_0)); }
	inline OrientationEvent_t158247624 * get_defaultInstance_0() const { return ___defaultInstance_0; }
	inline OrientationEvent_t158247624 ** get_address_of_defaultInstance_0() { return &___defaultInstance_0; }
	inline void set_defaultInstance_0(OrientationEvent_t158247624 * value)
	{
		___defaultInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultInstance_0), value);
	}

	inline static int32_t get_offset_of__orientationEventFieldNames_1() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624_StaticFields, ____orientationEventFieldNames_1)); }
	inline StringU5BU5D_t1281789340* get__orientationEventFieldNames_1() const { return ____orientationEventFieldNames_1; }
	inline StringU5BU5D_t1281789340** get_address_of__orientationEventFieldNames_1() { return &____orientationEventFieldNames_1; }
	inline void set__orientationEventFieldNames_1(StringU5BU5D_t1281789340* value)
	{
		____orientationEventFieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____orientationEventFieldNames_1), value);
	}

	inline static int32_t get_offset_of__orientationEventFieldTags_2() { return static_cast<int32_t>(offsetof(OrientationEvent_t158247624_StaticFields, ____orientationEventFieldTags_2)); }
	inline UInt32U5BU5D_t2770800703* get__orientationEventFieldTags_2() const { return ____orientationEventFieldTags_2; }
	inline UInt32U5BU5D_t2770800703** get_address_of__orientationEventFieldTags_2() { return &____orientationEventFieldTags_2; }
	inline void set__orientationEventFieldTags_2(UInt32U5BU5D_t2770800703* value)
	{
		____orientationEventFieldTags_2 = value;
		Il2CppCodeGenWriteBarrier((&____orientationEventFieldTags_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTATIONEVENT_T158247624_H
#ifndef BUILDER_T2279437287_H
#define BUILDER_T2279437287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/OrientationEvent/Builder
struct  Builder_t2279437287  : public GeneratedBuilderLite_2_t3142848435
{
public:
	// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::resultIsReadOnly
	bool ___resultIsReadOnly_0;
	// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::result
	OrientationEvent_t158247624 * ___result_1;

public:
	inline static int32_t get_offset_of_resultIsReadOnly_0() { return static_cast<int32_t>(offsetof(Builder_t2279437287, ___resultIsReadOnly_0)); }
	inline bool get_resultIsReadOnly_0() const { return ___resultIsReadOnly_0; }
	inline bool* get_address_of_resultIsReadOnly_0() { return &___resultIsReadOnly_0; }
	inline void set_resultIsReadOnly_0(bool value)
	{
		___resultIsReadOnly_0 = value;
	}

	inline static int32_t get_offset_of_result_1() { return static_cast<int32_t>(offsetof(Builder_t2279437287, ___result_1)); }
	inline OrientationEvent_t158247624 * get_result_1() const { return ___result_1; }
	inline OrientationEvent_t158247624 ** get_address_of_result_1() { return &___result_1; }
	inline void set_result_1(OrientationEvent_t158247624 * value)
	{
		___result_1 = value;
		Il2CppCodeGenWriteBarrier((&___result_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T2279437287_H
#ifndef TYPE_T1244512943_H
#define TYPE_T1244512943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// proto.PhoneEvent/Types/Type
struct  Type_t1244512943 
{
public:
	// System.Int32 proto.PhoneEvent/Types/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t1244512943, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T1244512943_H
#ifndef INVALIDPROTOCOLBUFFEREXCEPTION_T2498581859_H
#define INVALIDPROTOCOLBUFFEREXCEPTION_T2498581859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Google.ProtocolBuffers.InvalidProtocolBufferException
struct  InvalidProtocolBufferException_t2498581859  : public IOException_t4088381929
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDPROTOCOLBUFFEREXCEPTION_T2498581859_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t426314064 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t426314064 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.UInt32[]
struct UInt32U5BU5D_t2770800703  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint32_t m_Items[1];

public:
	inline uint32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GeneratedBuilderLite_2__ctor_m3173042820_gshared (GeneratedBuilderLite_2_t3548493156 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GeneratedBuilderLite_2_MergeFrom_m895345846_gshared (GeneratedBuilderLite_2_t3548493156 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t PopsicleList_1_get_Count_m2360652361_gshared (PopsicleList_1_t2780837186 * __this, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Add(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_Add_m3756973089_gshared (PopsicleList_1_t2780837186 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Int32 System.Array::BinarySearch<System.Object>(!!0[],!!0,System.Collections.Generic.IComparer`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR int32_t Array_BinarySearch_TisRuntimeObject_m1327939877_gshared (RuntimeObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, RuntimeObject * p1, RuntimeObject* p2, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::set_Item(System.Int32,!0)
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_set_Item_m2953605274_gshared (PopsicleList_1_t2780837186 * __this, int32_t p0, float p1, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_Add_m2039782584_gshared (PopsicleList_1_t2780837186 * __this, float p0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Clear()
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_Clear_m2727107298_gshared (PopsicleList_1_t2780837186 * __this, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GeneratedMessageLite_2__ctor_m4133067742_gshared (GeneratedMessageLite_2_t4158627738 * __this, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void GeneratedMessageLite_2_PrintField_m3803611508_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, RuntimeObject * p2, TextWriter_t3478189236 * p3, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ByteString)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m3127151039_gshared (AbstractBuilderLite_2_t145292663 * __this, ByteString_t35393593 * p0, const RuntimeMethod* method);
// !0 Google.ProtocolBuffers.GeneratedBuilderLite`2<System.Object,System.Object>::BuildParsed()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GeneratedBuilderLite_2_BuildParsed_m2502728918_gshared (GeneratedBuilderLite_2_t3548493156 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m313876061_gshared (AbstractBuilderLite_2_t145292663 * __this, ByteString_t35393593 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m2869745971_gshared (AbstractBuilderLite_2_t145292663 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m601782995_gshared (AbstractBuilderLite_2_t145292663 * __this, ByteU5BU5D_t4116647657* p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m1941005149_gshared (AbstractBuilderLite_2_t145292663 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeFrom_m2193206133_gshared (AbstractBuilderLite_2_t145292663 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeDelimitedFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeDelimitedFrom_m163030421_gshared (AbstractBuilderLite_2_t145292663 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<System.Object,System.Object>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AbstractBuilderLite_2_MergeDelimitedFrom_m3999838497_gshared (AbstractBuilderLite_2_t145292663 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1__ctor_m2230196907_gshared (PopsicleList_1_t168709280 * __this, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t PopsicleList_1_get_Count_m2973048409_gshared (PopsicleList_1_t168709280 * __this, const RuntimeMethod* method);
// !0 Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * PopsicleList_1_get_Item_m3884775306_gshared (PopsicleList_1_t168709280 * __this, int32_t p0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<!0> Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* PopsicleList_1_GetEnumerator_m975204890_gshared (PopsicleList_1_t168709280 * __this, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<System.Object,System.Object>::PrintField<System.Object>(System.String,System.Collections.Generic.IList`1<!!0>,System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void GeneratedMessageLite_2_PrintField_TisRuntimeObject_m1989555473_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject* p1, TextWriter_t3478189236 * p2, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_MakeReadOnly_m310872135_gshared (PopsicleList_1_t168709280 * __this, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Add(System.Collections.Generic.IEnumerable`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_Add_m872484713_gshared (PopsicleList_1_t168709280 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::set_Item(System.Int32,!0)
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_set_Item_m2750909916_gshared (PopsicleList_1_t168709280 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_Add_m599259225_gshared (PopsicleList_1_t168709280 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void PopsicleList_1_Clear_m1537296736_gshared (PopsicleList_1_t168709280 * __this, const RuntimeMethod* method);

// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::.ctor()
inline void GeneratedBuilderLite_2__ctor_m3353608380 (GeneratedBuilderLite_2_t861404538 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedBuilderLite_2_t861404538 *, const RuntimeMethod*))GeneratedBuilderLite_2__ctor_m3173042820_gshared)(__this, method);
}
// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR DepthMapEvent_t729886054 * DepthMapEvent_get_DefaultInstance_m3818775721 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/DepthMapEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DepthMapEvent__ctor_m3880232579 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR DepthMapEvent_t729886054 * Builder_PrepareBuilder_m1452950514 (Builder_t1293396100 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::.ctor(proto.PhoneEvent/Types/DepthMapEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m2915391915 (Builder_t1293396100 * __this, DepthMapEvent_t729886054 * ___cloneFrom0, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3115778984 (Builder_t1293396100 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR DepthMapEvent_t729886054 * DepthMapEvent_MakeReadOnly_m1245176739 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
inline Builder_t1293396100 * GeneratedBuilderLite_2_MergeFrom_m629985948 (GeneratedBuilderLite_2_t861404538 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  Builder_t1293396100 * (*) (GeneratedBuilderLite_2_t861404538 *, RuntimeObject*, const RuntimeMethod*))GeneratedBuilderLite_2_MergeFrom_m895345846_gshared)(__this, p0, method);
}
// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool DepthMapEvent_get_HasTimestamp_m1414186605 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// System.Int64 proto.PhoneEvent/Types/DepthMapEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t DepthMapEvent_get_Timestamp_m2649415927 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m449743612 (Builder_t1293396100 * __this, int64_t ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::get_HasWidth()
extern "C" IL2CPP_METHOD_ATTR bool DepthMapEvent_get_HasWidth_m4255961603 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::get_Width()
extern "C" IL2CPP_METHOD_ATTR int32_t DepthMapEvent_get_Width_m723466732 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::set_Width(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Width_m2802306539 (Builder_t1293396100 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/DepthMapEvent::get_HasHeight()
extern "C" IL2CPP_METHOD_ATTR bool DepthMapEvent_get_HasHeight_m2629968232 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::get_Height()
extern "C" IL2CPP_METHOD_ATTR int32_t DepthMapEvent_get_Height_m14936364 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::set_Height(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Height_m873050964 (Builder_t1293396100 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::get_Count()
inline int32_t PopsicleList_1_get_Count_m2360652361 (PopsicleList_1_t2780837186 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (PopsicleList_1_t2780837186 *, const RuntimeMethod*))PopsicleList_1_get_Count_m2360652361_gshared)(__this, method);
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Add(System.Collections.Generic.IEnumerable`1<!0>)
inline void PopsicleList_1_Add_m3756973089 (PopsicleList_1_t2780837186 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t2780837186 *, RuntimeObject*, const RuntimeMethod*))PopsicleList_1_Add_m3756973089_gshared)(__this, p0, method);
}
// Google.ProtocolBuffers.ExtensionRegistry Google.ProtocolBuffers.ExtensionRegistry::get_Empty()
extern "C" IL2CPP_METHOD_ATTR ExtensionRegistry_t4271428238 * ExtensionRegistry_get_Empty_m3666683255 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.StringComparer System.StringComparer::get_Ordinal()
extern "C" IL2CPP_METHOD_ATTR StringComparer_t3301955079 * StringComparer_get_Ordinal_m2103862281 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 System.Array::BinarySearch<System.String>(!!0[],!!0,System.Collections.Generic.IComparer`1<!!0>)
inline int32_t Array_BinarySearch_TisString_t_m2519237149 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* p0, String_t* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RuntimeObject * /* static, unused */, StringU5BU5D_t1281789340*, String_t*, RuntimeObject*, const RuntimeMethod*))Array_BinarySearch_TisRuntimeObject_m1327939877_gshared)(__this /* static, unused */, p0, p1, p2, method);
}
// Google.ProtocolBuffers.InvalidProtocolBufferException Google.ProtocolBuffers.InvalidProtocolBufferException::InvalidTag()
extern "C" IL2CPP_METHOD_ATTR InvalidProtocolBufferException_t2498581859 * InvalidProtocolBufferException_InvalidTag_m4139780452 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean Google.ProtocolBuffers.WireFormat::IsEndGroupTag(System.UInt32)
extern "C" IL2CPP_METHOD_ATTR bool WireFormat_IsEndGroupTag_m2504732292 (RuntimeObject * __this /* static, unused */, uint32_t p0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_SetTimestamp_m751551372 (Builder_t1293396100 * __this, int64_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::SetWidth(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_SetWidth_m978829859 (Builder_t1293396100 * __this, int32_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::SetHeight(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_SetHeight_m72275784 (Builder_t1293396100 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 proto.PhoneEvent/Types/DepthMapEvent::get_ZDistancesCount()
extern "C" IL2CPP_METHOD_ATTR int32_t DepthMapEvent_get_ZDistancesCount_m4172945471 (DepthMapEvent_t729886054 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/DepthMapEvent::GetZDistances(System.Int32)
extern "C" IL2CPP_METHOD_ATTR float DepthMapEvent_GetZDistances_m1405235630 (DepthMapEvent_t729886054 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::set_Item(System.Int32,!0)
inline void PopsicleList_1_set_Item_m2953605274 (PopsicleList_1_t2780837186 * __this, int32_t p0, float p1, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t2780837186 *, int32_t, float, const RuntimeMethod*))PopsicleList_1_set_Item_m2953605274_gshared)(__this, p0, p1, method);
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Add(!0)
inline void PopsicleList_1_Add_m2039782584 (PopsicleList_1_t2780837186 * __this, float p0, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t2780837186 *, float, const RuntimeMethod*))PopsicleList_1_Add_m2039782584_gshared)(__this, p0, method);
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<System.Single>::Clear()
inline void PopsicleList_1_Clear_m2727107298 (PopsicleList_1_t2780837186 * __this, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t2780837186 *, const RuntimeMethod*))PopsicleList_1_Clear_m2727107298_gshared)(__this, method);
}
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::.ctor()
inline void GeneratedMessageLite_2__ctor_m647295579 (GeneratedMessageLite_2_t240680974 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedMessageLite_2_t240680974 *, const RuntimeMethod*))GeneratedMessageLite_2__ctor_m4133067742_gshared)(__this, method);
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GyroscopeEvent__ctor_m122134945 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_MakeReadOnly_m2997968155 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C" IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m3117905507 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t1871169219  p1, const RuntimeMethod* method);
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_get_DefaultInstance_m2890176487 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int64 proto.PhoneEvent/Types/GyroscopeEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t GyroscopeEvent_get_Timestamp_m727667782 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/GyroscopeEvent::get_X()
extern "C" IL2CPP_METHOD_ATTR float GyroscopeEvent_get_X_m1988437604 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/GyroscopeEvent::get_Y()
extern "C" IL2CPP_METHOD_ATTR float GyroscopeEvent_get_Y_m1988503140 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/GyroscopeEvent::get_Z()
extern "C" IL2CPP_METHOD_ATTR float GyroscopeEvent_get_Z_m1988306532 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.CodedOutputStream::ComputeInt64Size(System.Int32,System.Int64)
extern "C" IL2CPP_METHOD_ATTR int32_t CodedOutputStream_ComputeInt64Size_m2929732330 (RuntimeObject * __this /* static, unused */, int32_t p0, int64_t p1, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.CodedOutputStream::ComputeFloatSize(System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR int32_t CodedOutputStream_ComputeFloatSize_m3601108612 (RuntimeObject * __this /* static, unused */, int32_t p0, float p1, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m88164663 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 System.Int64::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Int64_GetHashCode_m703091690 (int64_t* __this, const RuntimeMethod* method);
// System.Int32 System.Single::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Single_GetHashCode_m1558506138 (float* __this, const RuntimeMethod* method);
// System.Boolean System.Int64::Equals(System.Int64)
extern "C" IL2CPP_METHOD_ATTR bool Int64_Equals_m680137412 (int64_t* __this, int64_t p0, const RuntimeMethod* method);
// System.Boolean System.Single::Equals(System.Single)
extern "C" IL2CPP_METHOD_ATTR bool Single_Equals_m1601893879 (float* __this, float p0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
inline void GeneratedMessageLite_2_PrintField_m2673392170 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, RuntimeObject * p2, TextWriter_t3478189236 * p3, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject * /* static, unused */, String_t*, bool, RuntimeObject *, TextWriter_t3478189236 *, const RuntimeMethod*))GeneratedMessageLite_2_PrintField_m3803611508_gshared)(__this /* static, unused */, p0, p1, p2, p3, method);
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * GyroscopeEvent_CreateBuilder_m3306880792 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString)
inline Builder_t3442751222 * AbstractBuilderLite_2_MergeFrom_m2996271875 (AbstractBuilderLite_2_t522313195 * __this, ByteString_t35393593 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (AbstractBuilderLite_2_t522313195 *, ByteString_t35393593 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m3127151039_gshared)(__this, p0, method);
}
// !0 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::BuildParsed()
inline GyroscopeEvent_t127774954 * GeneratedBuilderLite_2_BuildParsed_m3994085786 (GeneratedBuilderLite_2_t3925513688 * __this, const RuntimeMethod* method)
{
	return ((  GyroscopeEvent_t127774954 * (*) (GeneratedBuilderLite_2_t3925513688 *, const RuntimeMethod*))GeneratedBuilderLite_2_BuildParsed_m2502728918_gshared)(__this, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t3442751222 * AbstractBuilderLite_2_MergeFrom_m1104280193 (AbstractBuilderLite_2_t522313195 * __this, ByteString_t35393593 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (AbstractBuilderLite_2_t522313195 *, ByteString_t35393593 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m313876061_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(System.Byte[])
inline Builder_t3442751222 * AbstractBuilderLite_2_MergeFrom_m3259242108 (AbstractBuilderLite_2_t522313195 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (AbstractBuilderLite_2_t522313195 *, ByteU5BU5D_t4116647657*, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2869745971_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t3442751222 * AbstractBuilderLite_2_MergeFrom_m4289660591 (AbstractBuilderLite_2_t522313195 * __this, ByteU5BU5D_t4116647657* p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (AbstractBuilderLite_2_t522313195 *, ByteU5BU5D_t4116647657*, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m601782995_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(System.IO.Stream)
inline Builder_t3442751222 * AbstractBuilderLite_2_MergeFrom_m3843169734 (AbstractBuilderLite_2_t522313195 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (AbstractBuilderLite_2_t522313195 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m1941005149_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t3442751222 * AbstractBuilderLite_2_MergeFrom_m1514029506 (AbstractBuilderLite_2_t522313195 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (AbstractBuilderLite_2_t522313195 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2193206133_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeDelimitedFrom(System.IO.Stream)
inline Builder_t3442751222 * AbstractBuilderLite_2_MergeDelimitedFrom_m4283379067 (AbstractBuilderLite_2_t522313195 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (AbstractBuilderLite_2_t522313195 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m163030421_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t3442751222 * AbstractBuilderLite_2_MergeDelimitedFrom_m3618057809 (AbstractBuilderLite_2_t522313195 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (AbstractBuilderLite_2_t522313195 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m3999838497_gshared)(__this, p0, p1, method);
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m2804606681 (Builder_t3442751222 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent::CreateBuilder(proto.PhoneEvent/Types/GyroscopeEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * GyroscopeEvent_CreateBuilder_m2664119612 (RuntimeObject * __this /* static, unused */, GyroscopeEvent_t127774954 * ___prototype0, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::.ctor(proto.PhoneEvent/Types/GyroscopeEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m4245920840 (Builder_t3442751222 * __this, GyroscopeEvent_t127774954 * ___cloneFrom0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::.ctor()
inline void GeneratedBuilderLite_2__ctor_m4090092181 (GeneratedBuilderLite_2_t3925513688 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedBuilderLite_2_t3925513688 *, const RuntimeMethod*))GeneratedBuilderLite_2__ctor_m3173042820_gshared)(__this, method);
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * Builder_PrepareBuilder_m323407734 (Builder_t3442751222 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
inline Builder_t3442751222 * GeneratedBuilderLite_2_MergeFrom_m2246583089 (GeneratedBuilderLite_2_t3925513688 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  Builder_t3442751222 * (*) (GeneratedBuilderLite_2_t3925513688 *, RuntimeObject*, const RuntimeMethod*))GeneratedBuilderLite_2_MergeFrom_m895345846_gshared)(__this, p0, method);
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_HasTimestamp_m2330998764 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m3596341989 (Builder_t3442751222 * __this, int64_t ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_HasX_m3534408037 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::set_X(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_X_m533575164 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_HasY_m3534342501 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::set_Y(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Y_m2318854370 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_HasZ_m3534539109 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::set_Z(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Z_m477643105 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_SetTimestamp_m3662053200 (Builder_t3442751222 * __this, int64_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::SetX(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_SetX_m4266037197 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::SetY(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_SetY_m4263658798 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::SetZ(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_SetZ_m4266136855 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::.ctor()
inline void GeneratedMessageLite_2__ctor_m3776640171 (GeneratedMessageLite_2_t3781587578 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedMessageLite_2_t3781587578 *, const RuntimeMethod*))GeneratedMessageLite_2__ctor_m4133067742_gshared)(__this, method);
}
// System.Void proto.PhoneEvent/Types/KeyEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void KeyEvent__ctor_m4217360548 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_MakeReadOnly_m4090303625 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_get_DefaultInstance_m740969952 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 proto.PhoneEvent/Types/KeyEvent::get_Action()
extern "C" IL2CPP_METHOD_ATTR int32_t KeyEvent_get_Action_m1195418036 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method);
// System.Int32 proto.PhoneEvent/Types/KeyEvent::get_Code()
extern "C" IL2CPP_METHOD_ATTR int32_t KeyEvent_get_Code_m2196363321 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.CodedOutputStream::ComputeInt32Size(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t CodedOutputStream_ComputeInt32Size_m1726852181 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Int32 System.Int32::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Int32_GetHashCode_m1876651407 (int32_t* __this, const RuntimeMethod* method);
// System.Boolean System.Int32::Equals(System.Int32)
extern "C" IL2CPP_METHOD_ATTR bool Int32_Equals_m2976157357 (int32_t* __this, int32_t p0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
inline void GeneratedMessageLite_2_PrintField_m1636826668 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, RuntimeObject * p2, TextWriter_t3478189236 * p3, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject * /* static, unused */, String_t*, bool, RuntimeObject *, TextWriter_t3478189236 *, const RuntimeMethod*))GeneratedMessageLite_2_PrintField_m3803611508_gshared)(__this /* static, unused */, p0, p1, p2, p3, method);
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * KeyEvent_CreateBuilder_m2320679930 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString)
inline Builder_t2712992173 * AbstractBuilderLite_2_MergeFrom_m3650812895 (AbstractBuilderLite_2_t4063219799 * __this, ByteString_t35393593 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (AbstractBuilderLite_2_t4063219799 *, ByteString_t35393593 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m3127151039_gshared)(__this, p0, method);
}
// !0 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::BuildParsed()
inline KeyEvent_t1937114521 * GeneratedBuilderLite_2_BuildParsed_m3604012727 (GeneratedBuilderLite_2_t3171452996 * __this, const RuntimeMethod* method)
{
	return ((  KeyEvent_t1937114521 * (*) (GeneratedBuilderLite_2_t3171452996 *, const RuntimeMethod*))GeneratedBuilderLite_2_BuildParsed_m2502728918_gshared)(__this, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2712992173 * AbstractBuilderLite_2_MergeFrom_m3060350321 (AbstractBuilderLite_2_t4063219799 * __this, ByteString_t35393593 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (AbstractBuilderLite_2_t4063219799 *, ByteString_t35393593 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m313876061_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(System.Byte[])
inline Builder_t2712992173 * AbstractBuilderLite_2_MergeFrom_m3720688089 (AbstractBuilderLite_2_t4063219799 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (AbstractBuilderLite_2_t4063219799 *, ByteU5BU5D_t4116647657*, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2869745971_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2712992173 * AbstractBuilderLite_2_MergeFrom_m908247555 (AbstractBuilderLite_2_t4063219799 * __this, ByteU5BU5D_t4116647657* p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (AbstractBuilderLite_2_t4063219799 *, ByteU5BU5D_t4116647657*, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m601782995_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(System.IO.Stream)
inline Builder_t2712992173 * AbstractBuilderLite_2_MergeFrom_m3965376215 (AbstractBuilderLite_2_t4063219799 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (AbstractBuilderLite_2_t4063219799 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m1941005149_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2712992173 * AbstractBuilderLite_2_MergeFrom_m1101353770 (AbstractBuilderLite_2_t4063219799 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (AbstractBuilderLite_2_t4063219799 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2193206133_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeDelimitedFrom(System.IO.Stream)
inline Builder_t2712992173 * AbstractBuilderLite_2_MergeDelimitedFrom_m25623811 (AbstractBuilderLite_2_t4063219799 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (AbstractBuilderLite_2_t4063219799 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m163030421_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2712992173 * AbstractBuilderLite_2_MergeDelimitedFrom_m3541096835 (AbstractBuilderLite_2_t4063219799 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (AbstractBuilderLite_2_t4063219799 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m3999838497_gshared)(__this, p0, p1, method);
}
// System.Void proto.PhoneEvent/Types/KeyEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m4137946296 (Builder_t2712992173 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent::CreateBuilder(proto.PhoneEvent/Types/KeyEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * KeyEvent_CreateBuilder_m83685355 (RuntimeObject * __this /* static, unused */, KeyEvent_t1937114521 * ___prototype0, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/KeyEvent/Builder::.ctor(proto.PhoneEvent/Types/KeyEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m2822184233 (Builder_t2712992173 * __this, KeyEvent_t1937114521 * ___cloneFrom0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::.ctor()
inline void GeneratedBuilderLite_2__ctor_m24974982 (GeneratedBuilderLite_2_t3171452996 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedBuilderLite_2_t3171452996 *, const RuntimeMethod*))GeneratedBuilderLite_2__ctor_m3173042820_gshared)(__this, method);
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * Builder_PrepareBuilder_m2114990423 (Builder_t2712992173 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
inline Builder_t2712992173 * GeneratedBuilderLite_2_MergeFrom_m157053624 (GeneratedBuilderLite_2_t3171452996 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  Builder_t2712992173 * (*) (GeneratedBuilderLite_2_t3171452996 *, RuntimeObject*, const RuntimeMethod*))GeneratedBuilderLite_2_MergeFrom_m895345846_gshared)(__this, p0, method);
}
// System.Boolean proto.PhoneEvent/Types/KeyEvent::get_HasAction()
extern "C" IL2CPP_METHOD_ATTR bool KeyEvent_get_HasAction_m3390159228 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/KeyEvent/Builder::set_Action(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Action_m1176581797 (Builder_t2712992173 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/KeyEvent::get_HasCode()
extern "C" IL2CPP_METHOD_ATTR bool KeyEvent_get_HasCode_m2936208505 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/KeyEvent/Builder::set_Code(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Code_m3879885671 (Builder_t2712992173 * __this, int32_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::SetAction(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_SetAction_m1997413468 (Builder_t2712992173 * __this, int32_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::SetCode(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_SetCode_m1980311967 (Builder_t2712992173 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::.ctor()
inline void PopsicleList_1__ctor_m3092888001 (PopsicleList_1_t1233628674 * __this, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t1233628674 *, const RuntimeMethod*))PopsicleList_1__ctor_m2230196907_gshared)(__this, method);
}
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::.ctor()
inline void GeneratedMessageLite_2__ctor_m326934031 (GeneratedMessageLite_2_t4271484156 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedMessageLite_2_t4271484156 *, const RuntimeMethod*))GeneratedMessageLite_2__ctor_m4133067742_gshared)(__this, method);
}
// System.Void proto.PhoneEvent/Types/MotionEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MotionEvent__ctor_m1502834634 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_MakeReadOnly_m1076849093 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_get_DefaultInstance_m2362671815 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::get_Count()
inline int32_t PopsicleList_1_get_Count_m700411858 (PopsicleList_1_t1233628674 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (PopsicleList_1_t1233628674 *, const RuntimeMethod*))PopsicleList_1_get_Count_m2973048409_gshared)(__this, method);
}
// !0 Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::get_Item(System.Int32)
inline Pointer_t4145025558 * PopsicleList_1_get_Item_m2708808148 (PopsicleList_1_t1233628674 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  Pointer_t4145025558 * (*) (PopsicleList_1_t1233628674 *, int32_t, const RuntimeMethod*))PopsicleList_1_get_Item_m3884775306_gshared)(__this, p0, method);
}
// System.Int64 proto.PhoneEvent/Types/MotionEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t MotionEvent_get_Timestamp_m1377696390 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method);
// System.Int32 proto.PhoneEvent/Types/MotionEvent::get_Action()
extern "C" IL2CPP_METHOD_ATTR int32_t MotionEvent_get_Action_m3433129119 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer> proto.PhoneEvent/Types/MotionEvent::get_PointersList()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MotionEvent_get_PointersList_m2766990463 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method);
// System.Int32 Google.ProtocolBuffers.CodedOutputStream::ComputeMessageSize(System.Int32,Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR int32_t CodedOutputStream_ComputeMessageSize_m4104294790 (RuntimeObject * __this /* static, unused */, int32_t p0, RuntimeObject* p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<!0> Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::GetEnumerator()
inline RuntimeObject* PopsicleList_1_GetEnumerator_m1740650653 (PopsicleList_1_t1233628674 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (PopsicleList_1_t1233628674 *, const RuntimeMethod*))PopsicleList_1_GetEnumerator_m975204890_gshared)(__this, method);
}
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
inline void GeneratedMessageLite_2_PrintField_m4052720336 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, RuntimeObject * p2, TextWriter_t3478189236 * p3, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject * /* static, unused */, String_t*, bool, RuntimeObject *, TextWriter_t3478189236 *, const RuntimeMethod*))GeneratedMessageLite_2_PrintField_m3803611508_gshared)(__this /* static, unused */, p0, p1, p2, p3, method);
}
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::PrintField<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>(System.String,System.Collections.Generic.IList`1<!!0>,System.IO.TextWriter)
inline void GeneratedMessageLite_2_PrintField_TisPointer_t4145025558_m3080746959 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject* p1, TextWriter_t3478189236 * p2, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject * /* static, unused */, String_t*, RuntimeObject*, TextWriter_t3478189236 *, const RuntimeMethod*))GeneratedMessageLite_2_PrintField_TisRuntimeObject_m1989555473_gshared)(__this /* static, unused */, p0, p1, p2, method);
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * MotionEvent_CreateBuilder_m1575032901 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString)
inline Builder_t2961114394 * AbstractBuilderLite_2_MergeFrom_m3032596744 (AbstractBuilderLite_2_t258149081 * __this, ByteString_t35393593 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (AbstractBuilderLite_2_t258149081 *, ByteString_t35393593 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m3127151039_gshared)(__this, p0, method);
}
// !0 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::BuildParsed()
inline MotionEvent_t3121383016 * GeneratedBuilderLite_2_BuildParsed_m1996805703 (GeneratedBuilderLite_2_t3661349574 * __this, const RuntimeMethod* method)
{
	return ((  MotionEvent_t3121383016 * (*) (GeneratedBuilderLite_2_t3661349574 *, const RuntimeMethod*))GeneratedBuilderLite_2_BuildParsed_m2502728918_gshared)(__this, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2961114394 * AbstractBuilderLite_2_MergeFrom_m1962044158 (AbstractBuilderLite_2_t258149081 * __this, ByteString_t35393593 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (AbstractBuilderLite_2_t258149081 *, ByteString_t35393593 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m313876061_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(System.Byte[])
inline Builder_t2961114394 * AbstractBuilderLite_2_MergeFrom_m2082277826 (AbstractBuilderLite_2_t258149081 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (AbstractBuilderLite_2_t258149081 *, ByteU5BU5D_t4116647657*, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2869745971_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2961114394 * AbstractBuilderLite_2_MergeFrom_m3622339991 (AbstractBuilderLite_2_t258149081 * __this, ByteU5BU5D_t4116647657* p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (AbstractBuilderLite_2_t258149081 *, ByteU5BU5D_t4116647657*, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m601782995_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(System.IO.Stream)
inline Builder_t2961114394 * AbstractBuilderLite_2_MergeFrom_m3419181726 (AbstractBuilderLite_2_t258149081 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (AbstractBuilderLite_2_t258149081 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m1941005149_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2961114394 * AbstractBuilderLite_2_MergeFrom_m1124215902 (AbstractBuilderLite_2_t258149081 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (AbstractBuilderLite_2_t258149081 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2193206133_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeDelimitedFrom(System.IO.Stream)
inline Builder_t2961114394 * AbstractBuilderLite_2_MergeDelimitedFrom_m715217478 (AbstractBuilderLite_2_t258149081 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (AbstractBuilderLite_2_t258149081 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m163030421_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2961114394 * AbstractBuilderLite_2_MergeDelimitedFrom_m2857252119 (AbstractBuilderLite_2_t258149081 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (AbstractBuilderLite_2_t258149081 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m3999838497_gshared)(__this, p0, p1, method);
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::MakeReadOnly()
inline void PopsicleList_1_MakeReadOnly_m1389303794 (PopsicleList_1_t1233628674 * __this, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t1233628674 *, const RuntimeMethod*))PopsicleList_1_MakeReadOnly_m310872135_gshared)(__this, method);
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m2482629630 (Builder_t2961114394 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent::CreateBuilder(proto.PhoneEvent/Types/MotionEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * MotionEvent_CreateBuilder_m586676648 (RuntimeObject * __this /* static, unused */, MotionEvent_t3121383016 * ___prototype0, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/MotionEvent/Builder::.ctor(proto.PhoneEvent/Types/MotionEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3588482064 (Builder_t2961114394 * __this, MotionEvent_t3121383016 * ___cloneFrom0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::.ctor()
inline void GeneratedBuilderLite_2__ctor_m2782592139 (GeneratedBuilderLite_2_t3661349574 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedBuilderLite_2_t3661349574 *, const RuntimeMethod*))GeneratedBuilderLite_2__ctor_m3173042820_gshared)(__this, method);
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * Builder_PrepareBuilder_m935363363 (Builder_t2961114394 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
inline Builder_t2961114394 * GeneratedBuilderLite_2_MergeFrom_m610274514 (GeneratedBuilderLite_2_t3661349574 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  Builder_t2961114394 * (*) (GeneratedBuilderLite_2_t3661349574 *, RuntimeObject*, const RuntimeMethod*))GeneratedBuilderLite_2_MergeFrom_m895345846_gshared)(__this, p0, method);
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool MotionEvent_get_HasTimestamp_m2368979065 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/MotionEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m758952812 (Builder_t2961114394 * __this, int64_t ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/MotionEvent::get_HasAction()
extern "C" IL2CPP_METHOD_ATTR bool MotionEvent_get_HasAction_m1501510323 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/MotionEvent/Builder::set_Action(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Action_m18528823 (Builder_t2961114394 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::Add(System.Collections.Generic.IEnumerable`1<!0>)
inline void PopsicleList_1_Add_m3166945360 (PopsicleList_1_t1233628674 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t1233628674 *, RuntimeObject*, const RuntimeMethod*))PopsicleList_1_Add_m872484713_gshared)(__this, p0, method);
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_get_DefaultInstance_m846755284 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_SetTimestamp_m757989845 (Builder_t2961114394 * __this, int64_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::SetAction(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_SetAction_m2495301148 (Builder_t2961114394 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Int32 proto.PhoneEvent/Types/MotionEvent::get_PointersCount()
extern "C" IL2CPP_METHOD_ATTR int32_t MotionEvent_get_PointersCount_m837067104 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent::GetPointers(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * MotionEvent_GetPointers_m3628348230 (MotionEvent_t3121383016 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.ThrowHelper::ThrowIfNull(System.Object,System.String)
extern "C" IL2CPP_METHOD_ATTR void ThrowHelper_ThrowIfNull_m4026022381 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, String_t* p1, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::set_Item(System.Int32,!0)
inline void PopsicleList_1_set_Item_m2187003455 (PopsicleList_1_t1233628674 * __this, int32_t p0, Pointer_t4145025558 * p1, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t1233628674 *, int32_t, Pointer_t4145025558 *, const RuntimeMethod*))PopsicleList_1_set_Item_m2750909916_gshared)(__this, p0, p1, method);
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::Add(!0)
inline void PopsicleList_1_Add_m2124609127 (PopsicleList_1_t1233628674 * __this, Pointer_t4145025558 * p0, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t1233628674 *, Pointer_t4145025558 *, const RuntimeMethod*))PopsicleList_1_Add_m599259225_gshared)(__this, p0, method);
}
// System.Void Google.ProtocolBuffers.Collections.PopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::Clear()
inline void PopsicleList_1_Clear_m485968702 (PopsicleList_1_t1233628674 * __this, const RuntimeMethod* method)
{
	((  void (*) (PopsicleList_1_t1233628674 *, const RuntimeMethod*))PopsicleList_1_Clear_m1537296736_gshared)(__this, method);
}
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::.ctor()
inline void GeneratedMessageLite_2__ctor_m3200145457 (GeneratedMessageLite_2_t1361417249 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedMessageLite_2_t1361417249 *, const RuntimeMethod*))GeneratedMessageLite_2__ctor_m4133067742_gshared)(__this, method);
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Pointer__ctor_m597435337 (Pointer_t4145025558 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_MakeReadOnly_m3313729630 (Pointer_t4145025558 * __this, const RuntimeMethod* method);
// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_Id()
extern "C" IL2CPP_METHOD_ATTR int32_t Pointer_get_Id_m2656726378 (Pointer_t4145025558 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_NormalizedX()
extern "C" IL2CPP_METHOD_ATTR float Pointer_get_NormalizedX_m306370287 (Pointer_t4145025558 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_NormalizedY()
extern "C" IL2CPP_METHOD_ATTR float Pointer_get_NormalizedY_m3035253642 (Pointer_t4145025558 * __this, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
inline void GeneratedMessageLite_2_PrintField_m3760789329 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, RuntimeObject * p2, TextWriter_t3478189236 * p3, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject * /* static, unused */, String_t*, bool, RuntimeObject *, TextWriter_t3478189236 *, const RuntimeMethod*))GeneratedMessageLite_2_PrintField_m3803611508_gshared)(__this /* static, unused */, p0, p1, p2, p3, method);
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Pointer_CreateBuilder_m2774886265 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString)
inline Builder_t582111845 * AbstractBuilderLite_2_MergeFrom_m776875282 (AbstractBuilderLite_2_t1643049470 * __this, ByteString_t35393593 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (AbstractBuilderLite_2_t1643049470 *, ByteString_t35393593 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m3127151039_gshared)(__this, p0, method);
}
// !0 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::BuildParsed()
inline Pointer_t4145025558 * GeneratedBuilderLite_2_BuildParsed_m2187103318 (GeneratedBuilderLite_2_t751282667 * __this, const RuntimeMethod* method)
{
	return ((  Pointer_t4145025558 * (*) (GeneratedBuilderLite_2_t751282667 *, const RuntimeMethod*))GeneratedBuilderLite_2_BuildParsed_m2502728918_gshared)(__this, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t582111845 * AbstractBuilderLite_2_MergeFrom_m269598545 (AbstractBuilderLite_2_t1643049470 * __this, ByteString_t35393593 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (AbstractBuilderLite_2_t1643049470 *, ByteString_t35393593 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m313876061_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(System.Byte[])
inline Builder_t582111845 * AbstractBuilderLite_2_MergeFrom_m14772483 (AbstractBuilderLite_2_t1643049470 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (AbstractBuilderLite_2_t1643049470 *, ByteU5BU5D_t4116647657*, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2869745971_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t582111845 * AbstractBuilderLite_2_MergeFrom_m2021870966 (AbstractBuilderLite_2_t1643049470 * __this, ByteU5BU5D_t4116647657* p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (AbstractBuilderLite_2_t1643049470 *, ByteU5BU5D_t4116647657*, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m601782995_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(System.IO.Stream)
inline Builder_t582111845 * AbstractBuilderLite_2_MergeFrom_m541927886 (AbstractBuilderLite_2_t1643049470 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (AbstractBuilderLite_2_t1643049470 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m1941005149_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t582111845 * AbstractBuilderLite_2_MergeFrom_m870343857 (AbstractBuilderLite_2_t1643049470 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (AbstractBuilderLite_2_t1643049470 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2193206133_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeDelimitedFrom(System.IO.Stream)
inline Builder_t582111845 * AbstractBuilderLite_2_MergeDelimitedFrom_m1106981 (AbstractBuilderLite_2_t1643049470 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (AbstractBuilderLite_2_t1643049470 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m163030421_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t582111845 * AbstractBuilderLite_2_MergeDelimitedFrom_m1016734311 (AbstractBuilderLite_2_t1643049470 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (AbstractBuilderLite_2_t1643049470 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m3999838497_gshared)(__this, p0, p1, method);
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3210742657 (Builder_t582111845 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer::CreateBuilder(proto.PhoneEvent/Types/MotionEvent/Types/Pointer)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Pointer_CreateBuilder_m4178316069 (RuntimeObject * __this /* static, unused */, Pointer_t4145025558 * ___prototype0, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::.ctor(proto.PhoneEvent/Types/MotionEvent/Types/Pointer)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3196691357 (Builder_t582111845 * __this, Pointer_t4145025558 * ___cloneFrom0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::.ctor()
inline void GeneratedBuilderLite_2__ctor_m3477943950 (GeneratedBuilderLite_2_t751282667 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedBuilderLite_2_t751282667 *, const RuntimeMethod*))GeneratedBuilderLite_2__ctor_m3173042820_gshared)(__this, method);
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Builder_PrepareBuilder_m2800481259 (Builder_t582111845 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
inline Builder_t582111845 * GeneratedBuilderLite_2_MergeFrom_m1218719396 (GeneratedBuilderLite_2_t751282667 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  Builder_t582111845 * (*) (GeneratedBuilderLite_2_t751282667 *, RuntimeObject*, const RuntimeMethod*))GeneratedBuilderLite_2_MergeFrom_m895345846_gshared)(__this, p0, method);
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_HasId()
extern "C" IL2CPP_METHOD_ATTR bool Pointer_get_HasId_m4071191836 (Pointer_t4145025558 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::set_Id(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Id_m1971136214 (Builder_t582111845 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_HasNormalizedX()
extern "C" IL2CPP_METHOD_ATTR bool Pointer_get_HasNormalizedX_m2691093520 (Pointer_t4145025558 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::set_NormalizedX(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_NormalizedX_m3801472179 (Builder_t582111845 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_HasNormalizedY()
extern "C" IL2CPP_METHOD_ATTR bool Pointer_get_HasNormalizedY_m1125009579 (Pointer_t4145025558 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::set_NormalizedY(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_NormalizedY_m3801436436 (Builder_t582111845 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::SetId(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_SetId_m1976386508 (Builder_t582111845 * __this, int32_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::SetNormalizedX(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_SetNormalizedX_m264783475 (Builder_t582111845 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::SetNormalizedY(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_SetNormalizedY_m264819350 (Builder_t582111845 * __this, float ___value0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::.ctor()
inline void GeneratedMessageLite_2__ctor_m3287578133 (GeneratedMessageLite_2_t3752983017 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedMessageLite_2_t3752983017 *, const RuntimeMethod*))GeneratedMessageLite_2__ctor_m4133067742_gshared)(__this, method);
}
// System.Void proto.PhoneEvent/Types/OrientationEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent__ctor_m3708828125 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_MakeReadOnly_m838780390 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_get_DefaultInstance_m2374728749 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int64 proto.PhoneEvent/Types/OrientationEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t OrientationEvent_get_Timestamp_m723854610 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_X()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_X_m884120132 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_Y()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_Y_m3222772292 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_Z()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_Z_m501783108 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_W()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_W_m546413124 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::PrintField(System.String,System.Boolean,System.Object,System.IO.TextWriter)
inline void GeneratedMessageLite_2_PrintField_m4136168662 (RuntimeObject * __this /* static, unused */, String_t* p0, bool p1, RuntimeObject * p2, TextWriter_t3478189236 * p3, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject * /* static, unused */, String_t*, bool, RuntimeObject *, TextWriter_t3478189236 *, const RuntimeMethod*))GeneratedMessageLite_2_PrintField_m3803611508_gshared)(__this /* static, unused */, p0, p1, p2, p3, method);
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * OrientationEvent_CreateBuilder_m324048109 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString)
inline Builder_t2279437287 * AbstractBuilderLite_2_MergeFrom_m1442256499 (AbstractBuilderLite_2_t4034615238 * __this, ByteString_t35393593 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (AbstractBuilderLite_2_t4034615238 *, ByteString_t35393593 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m3127151039_gshared)(__this, p0, method);
}
// !0 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::BuildParsed()
inline OrientationEvent_t158247624 * GeneratedBuilderLite_2_BuildParsed_m981074060 (GeneratedBuilderLite_2_t3142848435 * __this, const RuntimeMethod* method)
{
	return ((  OrientationEvent_t158247624 * (*) (GeneratedBuilderLite_2_t3142848435 *, const RuntimeMethod*))GeneratedBuilderLite_2_BuildParsed_m2502728918_gshared)(__this, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2279437287 * AbstractBuilderLite_2_MergeFrom_m3702155892 (AbstractBuilderLite_2_t4034615238 * __this, ByteString_t35393593 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (AbstractBuilderLite_2_t4034615238 *, ByteString_t35393593 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m313876061_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(System.Byte[])
inline Builder_t2279437287 * AbstractBuilderLite_2_MergeFrom_m4193624750 (AbstractBuilderLite_2_t4034615238 * __this, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (AbstractBuilderLite_2_t4034615238 *, ByteU5BU5D_t4116647657*, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2869745971_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2279437287 * AbstractBuilderLite_2_MergeFrom_m2211482326 (AbstractBuilderLite_2_t4034615238 * __this, ByteU5BU5D_t4116647657* p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (AbstractBuilderLite_2_t4034615238 *, ByteU5BU5D_t4116647657*, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m601782995_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(System.IO.Stream)
inline Builder_t2279437287 * AbstractBuilderLite_2_MergeFrom_m2051397694 (AbstractBuilderLite_2_t4034615238 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (AbstractBuilderLite_2_t4034615238 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m1941005149_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2279437287 * AbstractBuilderLite_2_MergeFrom_m1351836793 (AbstractBuilderLite_2_t4034615238 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (AbstractBuilderLite_2_t4034615238 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeFrom_m2193206133_gshared)(__this, p0, p1, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeDelimitedFrom(System.IO.Stream)
inline Builder_t2279437287 * AbstractBuilderLite_2_MergeDelimitedFrom_m3997270878 (AbstractBuilderLite_2_t4034615238 * __this, Stream_t1273022909 * p0, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (AbstractBuilderLite_2_t4034615238 *, Stream_t1273022909 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m163030421_gshared)(__this, p0, method);
}
// !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
inline Builder_t2279437287 * AbstractBuilderLite_2_MergeDelimitedFrom_m2055751663 (AbstractBuilderLite_2_t4034615238 * __this, Stream_t1273022909 * p0, ExtensionRegistry_t4271428238 * p1, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (AbstractBuilderLite_2_t4034615238 *, Stream_t1273022909 *, ExtensionRegistry_t4271428238 *, const RuntimeMethod*))AbstractBuilderLite_2_MergeDelimitedFrom_m3999838497_gshared)(__this, p0, p1, method);
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3698740796 (Builder_t2279437287 * __this, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilder(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * OrientationEvent_CreateBuilder_m4263316720 (RuntimeObject * __this /* static, unused */, OrientationEvent_t158247624 * ___prototype0, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::.ctor(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3220974630 (Builder_t2279437287 * __this, OrientationEvent_t158247624 * ___cloneFrom0, const RuntimeMethod* method);
// System.Void Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::.ctor()
inline void GeneratedBuilderLite_2__ctor_m1251714133 (GeneratedBuilderLite_2_t3142848435 * __this, const RuntimeMethod* method)
{
	((  void (*) (GeneratedBuilderLite_2_t3142848435 *, const RuntimeMethod*))GeneratedBuilderLite_2__ctor_m3173042820_gshared)(__this, method);
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * Builder_PrepareBuilder_m147557039 (Builder_t2279437287 * __this, const RuntimeMethod* method);
// !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.IMessageLite)
inline Builder_t2279437287 * GeneratedBuilderLite_2_MergeFrom_m4038465069 (GeneratedBuilderLite_2_t3142848435 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  Builder_t2279437287 * (*) (GeneratedBuilderLite_2_t3142848435 *, RuntimeObject*, const RuntimeMethod*))GeneratedBuilderLite_2_MergeFrom_m895345846_gshared)(__this, p0, method);
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasTimestamp_m3922884685 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m1393789818 (Builder_t2279437287 * __this, int64_t ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasX_m2602962142 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_X(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_X_m676748554 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasY_m646647006 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Y(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Y_m133127197 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasZ_m2220625118 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Z(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Z_m945314922 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasW()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasW_m1028984030 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method);
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_W(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_W_m2009947325 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetTimestamp_m3405041319 (Builder_t2279437287 * __this, int64_t ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetX(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetX_m1896712895 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetY(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetY_m1896677292 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetZ(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetZ_m1897165581 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method);
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetW(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetW_m1896757142 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3115778984 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m3115778984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m3353608380(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m3353608380_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(DepthMapEvent_t729886054_il2cpp_TypeInfo_var);
		DepthMapEvent_t729886054 * L_0 = DepthMapEvent_get_DefaultInstance_m3818775721(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::.ctor(proto.PhoneEvent/Types/DepthMapEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m2915391915 (Builder_t1293396100 * __this, DepthMapEvent_t729886054 * ___cloneFrom0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m2915391915_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m3353608380(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m3353608380_RuntimeMethod_var);
		DepthMapEvent_t729886054 * L_0 = ___cloneFrom0;
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::get_ThisBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_get_ThisBuilder_m2034469400 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR DepthMapEvent_t729886054 * Builder_PrepareBuilder_m1452950514 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_PrepareBuilder_m1452950514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DepthMapEvent_t729886054 * V_0 = NULL;
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		V_0 = L_1;
		DepthMapEvent_t729886054 * L_2 = (DepthMapEvent_t729886054 *)il2cpp_codegen_object_new(DepthMapEvent_t729886054_il2cpp_TypeInfo_var);
		DepthMapEvent__ctor_m3880232579(L_2, /*hidden argument*/NULL);
		__this->set_result_1(L_2);
		__this->set_resultIsReadOnly_0((bool)0);
		DepthMapEvent_t729886054 * L_3 = V_0;
		VirtFuncInvoker1< Builder_t1293396100 *, DepthMapEvent_t729886054 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::MergeFrom(!0) */, __this, L_3);
	}

IL_002c:
	{
		DepthMapEvent_t729886054 * L_4 = __this->get_result_1();
		return L_4;
	}
}
// System.Boolean proto.PhoneEvent/Types/DepthMapEvent/Builder::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_IsInitialized_m543599599 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::get_IsInitialized() */, L_0);
		return L_1;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent/Builder::get_MessageBeingBuilt()
extern "C" IL2CPP_METHOD_ATTR DepthMapEvent_t729886054 * Builder_get_MessageBeingBuilt_m3335096508 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::Clear()
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_Clear_m3186867221 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clear_m3186867221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DepthMapEvent_t729886054_il2cpp_TypeInfo_var);
		DepthMapEvent_t729886054 * L_0 = DepthMapEvent_get_DefaultInstance_m3818775721(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::Clone()
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_Clone_m2009059382 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clone_m2009059382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		Builder_t1293396100 * L_2 = (Builder_t1293396100 *)il2cpp_codegen_object_new(Builder_t1293396100_il2cpp_TypeInfo_var);
		Builder__ctor_m2915391915(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		Builder_t1293396100 * L_3 = (Builder_t1293396100 *)il2cpp_codegen_object_new(Builder_t1293396100_il2cpp_TypeInfo_var);
		Builder__ctor_m3115778984(L_3, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_4 = __this->get_result_1();
		NullCheck(L_3);
		Builder_t1293396100 * L_5 = VirtFuncInvoker1< Builder_t1293396100 *, DepthMapEvent_t729886054 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::MergeFrom(!0) */, L_3, L_4);
		return L_5;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent/Builder::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR DepthMapEvent_t729886054 * Builder_get_DefaultInstanceForType_m4198353571 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_get_DefaultInstanceForType_m4198353571_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DepthMapEvent_t729886054_il2cpp_TypeInfo_var);
		DepthMapEvent_t729886054 * L_0 = DepthMapEvent_get_DefaultInstance_m3818775721(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent proto.PhoneEvent/Types/DepthMapEvent/Builder::BuildPartial()
extern "C" IL2CPP_METHOD_ATTR DepthMapEvent_t729886054 * Builder_BuildPartial_m2687057387 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		return L_1;
	}

IL_0012:
	{
		__this->set_resultIsReadOnly_0((bool)1);
		DepthMapEvent_t729886054 * L_2 = __this->get_result_1();
		NullCheck(L_2);
		DepthMapEvent_t729886054 * L_3 = DepthMapEvent_MakeReadOnly_m1245176739(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_MergeFrom_m3939827302 (Builder_t1293396100 * __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m3939827302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___other0;
		if (!((DepthMapEvent_t729886054 *)IsInstSealed((RuntimeObject*)L_0, DepthMapEvent_t729886054_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject* L_1 = ___other0;
		Builder_t1293396100 * L_2 = VirtFuncInvoker1< Builder_t1293396100 *, DepthMapEvent_t729886054 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::MergeFrom(!0) */, __this, ((DepthMapEvent_t729886054 *)CastclassSealed((RuntimeObject*)L_1, DepthMapEvent_t729886054_il2cpp_TypeInfo_var)));
		return L_2;
	}

IL_0018:
	{
		RuntimeObject* L_3 = ___other0;
		GeneratedBuilderLite_2_MergeFrom_m629985948(__this, L_3, /*hidden argument*/GeneratedBuilderLite_2_MergeFrom_m629985948_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::MergeFrom(proto.PhoneEvent/Types/DepthMapEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_MergeFrom_m1367107875 (Builder_t1293396100 * __this, DepthMapEvent_t729886054 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m1367107875_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DepthMapEvent_t729886054 * L_0 = ___other0;
		IL2CPP_RUNTIME_CLASS_INIT(DepthMapEvent_t729886054_il2cpp_TypeInfo_var);
		DepthMapEvent_t729886054 * L_1 = DepthMapEvent_get_DefaultInstance_m3818775721(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(DepthMapEvent_t729886054 *)L_0) == ((RuntimeObject*)(DepthMapEvent_t729886054 *)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = DepthMapEvent_get_HasTimestamp_m1414186605(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		DepthMapEvent_t729886054 * L_4 = ___other0;
		NullCheck(L_4);
		int64_t L_5 = DepthMapEvent_get_Timestamp_m2649415927(L_4, /*hidden argument*/NULL);
		Builder_set_Timestamp_m449743612(__this, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		DepthMapEvent_t729886054 * L_6 = ___other0;
		NullCheck(L_6);
		bool L_7 = DepthMapEvent_get_HasWidth_m4255961603(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		DepthMapEvent_t729886054 * L_8 = ___other0;
		NullCheck(L_8);
		int32_t L_9 = DepthMapEvent_get_Width_m723466732(L_8, /*hidden argument*/NULL);
		Builder_set_Width_m2802306539(__this, L_9, /*hidden argument*/NULL);
	}

IL_0042:
	{
		DepthMapEvent_t729886054 * L_10 = ___other0;
		NullCheck(L_10);
		bool L_11 = DepthMapEvent_get_HasHeight_m2629968232(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0059;
		}
	}
	{
		DepthMapEvent_t729886054 * L_12 = ___other0;
		NullCheck(L_12);
		int32_t L_13 = DepthMapEvent_get_Height_m14936364(L_12, /*hidden argument*/NULL);
		Builder_set_Height_m873050964(__this, L_13, /*hidden argument*/NULL);
	}

IL_0059:
	{
		DepthMapEvent_t729886054 * L_14 = ___other0;
		NullCheck(L_14);
		PopsicleList_1_t2780837186 * L_15 = L_14->get_zDistances__14();
		NullCheck(L_15);
		int32_t L_16 = PopsicleList_1_get_Count_m2360652361(L_15, /*hidden argument*/PopsicleList_1_get_Count_m2360652361_RuntimeMethod_var);
		if (!L_16)
		{
			goto IL_007f;
		}
	}
	{
		DepthMapEvent_t729886054 * L_17 = __this->get_result_1();
		NullCheck(L_17);
		PopsicleList_1_t2780837186 * L_18 = L_17->get_zDistances__14();
		DepthMapEvent_t729886054 * L_19 = ___other0;
		NullCheck(L_19);
		PopsicleList_1_t2780837186 * L_20 = L_19->get_zDistances__14();
		NullCheck(L_18);
		PopsicleList_1_Add_m3756973089(L_18, L_20, /*hidden argument*/PopsicleList_1_Add_m3756973089_RuntimeMethod_var);
	}

IL_007f:
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_MergeFrom_m1700363822 (Builder_t1293396100 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m1700363822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t4271428238_il2cpp_TypeInfo_var);
		ExtensionRegistry_t4271428238 * L_1 = ExtensionRegistry_get_Empty_m3666683255(NULL /*static, unused*/, /*hidden argument*/NULL);
		Builder_t1293396100 * L_2 = VirtFuncInvoker2< Builder_t1293396100 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, __this, L_0, L_1);
		return L_2;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_MergeFrom_m1132865163 (Builder_t1293396100 * __this, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m1132865163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		goto IL_0126;
	}

IL_000c:
	{
		uint32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DepthMapEvent_t729886054_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_2 = ((DepthMapEvent_t729886054_StaticFields*)il2cpp_codegen_static_fields_for(DepthMapEvent_t729886054_il2cpp_TypeInfo_var))->get__depthMapEventFieldNames_1();
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t3301955079_il2cpp_TypeInfo_var);
		StringComparer_t3301955079 * L_4 = StringComparer_get_Ordinal_m2103862281(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Array_BinarySearch_TisString_t_m2519237149(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/Array_BinarySearch_TisString_t_m2519237149_RuntimeMethod_var);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DepthMapEvent_t729886054_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_7 = ((DepthMapEvent_t729886054_StaticFields*)il2cpp_codegen_static_fields_for(DepthMapEvent_t729886054_il2cpp_TypeInfo_var))->get__depthMapEventFieldTags_2();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		goto IL_004d;
	}

IL_003d:
	{
		RuntimeObject* L_11 = ___input0;
		ExtensionRegistry_t4271428238 * L_12 = ___extensionRegistry1;
		uint32_t L_13 = V_0;
		String_t* L_14 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_11, L_12, L_13, L_14);
		goto IL_0126;
	}

IL_004d:
	{
		uint32_t L_15 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)((int32_t)34))))
		{
			case 0:
			{
				goto IL_010e;
			}
			case 1:
			{
				goto IL_0066;
			}
			case 2:
			{
				goto IL_0066;
			}
			case 3:
			{
				goto IL_010e;
			}
		}
	}

IL_0066:
	{
		uint32_t L_16 = V_0;
		if (!L_16)
		{
			goto IL_0088;
		}
	}
	{
		uint32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)8)))
		{
			goto IL_00ab;
		}
	}
	{
		uint32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)16))))
		{
			goto IL_00cc;
		}
	}
	{
		uint32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)24))))
		{
			goto IL_00ed;
		}
	}
	{
		goto IL_008e;
	}

IL_0088:
	{
		InvalidProtocolBufferException_t2498581859 * L_20 = InvalidProtocolBufferException_InvalidTag_m4139780452(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, NULL, Builder_MergeFrom_m1132865163_RuntimeMethod_var);
	}

IL_008e:
	{
		uint32_t L_21 = V_0;
		bool L_22 = WireFormat_IsEndGroupTag_m2504732292(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009b;
		}
	}
	{
		return __this;
	}

IL_009b:
	{
		RuntimeObject* L_23 = ___input0;
		ExtensionRegistry_t4271428238 * L_24 = ___extensionRegistry1;
		uint32_t L_25 = V_0;
		String_t* L_26 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/DepthMapEvent,proto.PhoneEvent/Types/DepthMapEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_23, L_24, L_25, L_26);
		goto IL_0126;
	}

IL_00ab:
	{
		DepthMapEvent_t729886054 * L_27 = __this->get_result_1();
		RuntimeObject* L_28 = ___input0;
		DepthMapEvent_t729886054 * L_29 = __this->get_result_1();
		NullCheck(L_29);
		int64_t* L_30 = L_29->get_address_of_timestamp__5();
		NullCheck(L_28);
		bool L_31 = InterfaceFuncInvoker1< bool, int64_t* >::Invoke(2 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt64(System.Int64&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_28, (int64_t*)L_30);
		NullCheck(L_27);
		L_27->set_hasTimestamp_4(L_31);
		goto IL_0126;
	}

IL_00cc:
	{
		DepthMapEvent_t729886054 * L_32 = __this->get_result_1();
		RuntimeObject* L_33 = ___input0;
		DepthMapEvent_t729886054 * L_34 = __this->get_result_1();
		NullCheck(L_34);
		int32_t* L_35 = L_34->get_address_of_width__8();
		NullCheck(L_33);
		bool L_36 = InterfaceFuncInvoker1< bool, int32_t* >::Invoke(3 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt32(System.Int32&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_33, (int32_t*)L_35);
		NullCheck(L_32);
		L_32->set_hasWidth_7(L_36);
		goto IL_0126;
	}

IL_00ed:
	{
		DepthMapEvent_t729886054 * L_37 = __this->get_result_1();
		RuntimeObject* L_38 = ___input0;
		DepthMapEvent_t729886054 * L_39 = __this->get_result_1();
		NullCheck(L_39);
		int32_t* L_40 = L_39->get_address_of_height__11();
		NullCheck(L_38);
		bool L_41 = InterfaceFuncInvoker1< bool, int32_t* >::Invoke(3 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt32(System.Int32&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_38, (int32_t*)L_40);
		NullCheck(L_37);
		L_37->set_hasHeight_10(L_41);
		goto IL_0126;
	}

IL_010e:
	{
		RuntimeObject* L_42 = ___input0;
		uint32_t L_43 = V_0;
		String_t* L_44 = V_1;
		DepthMapEvent_t729886054 * L_45 = __this->get_result_1();
		NullCheck(L_45);
		PopsicleList_1_t2780837186 * L_46 = L_45->get_zDistances__14();
		NullCheck(L_42);
		InterfaceActionInvoker3< uint32_t, String_t*, RuntimeObject* >::Invoke(8 /* System.Void Google.ProtocolBuffers.ICodedInputStream::ReadFloatArray(System.UInt32,System.String,System.Collections.Generic.ICollection`1<System.Single>) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_42, L_43, L_44, L_46);
		goto IL_0126;
	}

IL_0126:
	{
		RuntimeObject* L_47 = ___input0;
		NullCheck(L_47);
		bool L_48 = InterfaceFuncInvoker2< bool, uint32_t*, String_t** >::Invoke(0 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadTag(System.UInt32&,System.String&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_47, (uint32_t*)(&V_0), (String_t**)(&V_1));
		if (L_48)
		{
			goto IL_000c;
		}
	}
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/DepthMapEvent/Builder::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasTimestamp_m3095577229 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasTimestamp_4();
		return L_1;
	}
}
// System.Int64 proto.PhoneEvent/Types/DepthMapEvent/Builder::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t Builder_get_Timestamp_m2911673898 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int64_t L_1 = DepthMapEvent_get_Timestamp_m2649415927(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m449743612 (Builder_t1293396100 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		int64_t L_0 = ___value0;
		Builder_SetTimestamp_m751551372(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_SetTimestamp_m751551372 (Builder_t1293396100 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)1);
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		int64_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_timestamp__5(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::ClearTimestamp()
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_ClearTimestamp_m2122788067 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)0);
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_timestamp__5((((int64_t)((int64_t)0))));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/DepthMapEvent/Builder::get_HasWidth()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasWidth_m1761628400 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasWidth_7();
		return L_1;
	}
}
// System.Int32 proto.PhoneEvent/Types/DepthMapEvent/Builder::get_Width()
extern "C" IL2CPP_METHOD_ATTR int32_t Builder_get_Width_m3271783288 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int32_t L_1 = DepthMapEvent_get_Width_m723466732(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::set_Width(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Width_m2802306539 (Builder_t1293396100 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		Builder_SetWidth_m978829859(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::SetWidth(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_SetWidth_m978829859 (Builder_t1293396100 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasWidth_7((bool)1);
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		int32_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_width__8(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::ClearWidth()
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_ClearWidth_m1345126767 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasWidth_7((bool)0);
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_width__8(0);
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/DepthMapEvent/Builder::get_HasHeight()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasHeight_m1156773341 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasHeight_10();
		return L_1;
	}
}
// System.Int32 proto.PhoneEvent/Types/DepthMapEvent/Builder::get_Height()
extern "C" IL2CPP_METHOD_ATTR int32_t Builder_get_Height_m2000579020 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int32_t L_1 = DepthMapEvent_get_Height_m14936364(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/DepthMapEvent/Builder::set_Height(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Height_m873050964 (Builder_t1293396100 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		Builder_SetHeight_m72275784(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::SetHeight(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_SetHeight_m72275784 (Builder_t1293396100 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasHeight_10((bool)1);
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		int32_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_height__11(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::ClearHeight()
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_ClearHeight_m1479486030 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasHeight_10((bool)0);
		DepthMapEvent_t729886054 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_height__11(0);
		return __this;
	}
}
// Google.ProtocolBuffers.Collections.IPopsicleList`1<System.Single> proto.PhoneEvent/Types/DepthMapEvent/Builder::get_ZDistancesList()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Builder_get_ZDistancesList_m2592920384 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		PopsicleList_1_t2780837186 * L_1 = L_0->get_zDistances__14();
		return L_1;
	}
}
// System.Int32 proto.PhoneEvent/Types/DepthMapEvent/Builder::get_ZDistancesCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Builder_get_ZDistancesCount_m3917369321 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int32_t L_1 = DepthMapEvent_get_ZDistancesCount_m4172945471(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/DepthMapEvent/Builder::GetZDistances(System.Int32)
extern "C" IL2CPP_METHOD_ATTR float Builder_GetZDistances_m2426190087 (Builder_t1293396100 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		float L_2 = DepthMapEvent_GetZDistances_m1405235630(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::SetZDistances(System.Int32,System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_SetZDistances_m3658819453 (Builder_t1293396100 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_SetZDistances_m3658819453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		PopsicleList_1_t2780837186 * L_1 = L_0->get_zDistances__14();
		int32_t L_2 = ___index0;
		float L_3 = ___value1;
		NullCheck(L_1);
		PopsicleList_1_set_Item_m2953605274(L_1, L_2, L_3, /*hidden argument*/PopsicleList_1_set_Item_m2953605274_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::AddZDistances(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_AddZDistances_m4175806553 (Builder_t1293396100 * __this, float ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_AddZDistances_m4175806553_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		PopsicleList_1_t2780837186 * L_1 = L_0->get_zDistances__14();
		float L_2 = ___value0;
		NullCheck(L_1);
		PopsicleList_1_Add_m2039782584(L_1, L_2, /*hidden argument*/PopsicleList_1_Add_m2039782584_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::AddRangeZDistances(System.Collections.Generic.IEnumerable`1<System.Single>)
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_AddRangeZDistances_m4278420723 (Builder_t1293396100 * __this, RuntimeObject* ___values0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_AddRangeZDistances_m4278420723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		PopsicleList_1_t2780837186 * L_1 = L_0->get_zDistances__14();
		RuntimeObject* L_2 = ___values0;
		NullCheck(L_1);
		PopsicleList_1_Add_m3756973089(L_1, L_2, /*hidden argument*/PopsicleList_1_Add_m3756973089_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/DepthMapEvent/Builder proto.PhoneEvent/Types/DepthMapEvent/Builder::ClearZDistances()
extern "C" IL2CPP_METHOD_ATTR Builder_t1293396100 * Builder_ClearZDistances_m2985824343 (Builder_t1293396100 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_ClearZDistances_m2985824343_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_PrepareBuilder_m1452950514(__this, /*hidden argument*/NULL);
		DepthMapEvent_t729886054 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		PopsicleList_1_t2780837186 * L_1 = L_0->get_zDistances__14();
		NullCheck(L_1);
		PopsicleList_1_Clear_m2727107298(L_1, /*hidden argument*/PopsicleList_1_Clear_m2727107298_RuntimeMethod_var);
		return __this;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/GyroscopeEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GyroscopeEvent__ctor_m122134945 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent__ctor_m122134945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_memoizedSerializedSize_15((-1));
		GeneratedMessageLite_2__ctor_m647295579(__this, /*hidden argument*/GeneratedMessageLite_2__ctor_m647295579_RuntimeMethod_var);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent::.cctor()
extern "C" IL2CPP_METHOD_ATTR void GyroscopeEvent__cctor_m1473982519 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent__cctor_m1473982519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GyroscopeEvent_t127774954 * L_0 = (GyroscopeEvent_t127774954 *)il2cpp_codegen_object_new(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		GyroscopeEvent__ctor_m122134945(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		GyroscopeEvent_t127774954 * L_1 = GyroscopeEvent_MakeReadOnly_m2997968155(L_0, /*hidden argument*/NULL);
		((GyroscopeEvent_t127774954_StaticFields*)il2cpp_codegen_static_fields_for(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var))->set_defaultInstance_0(L_1);
		StringU5BU5D_t1281789340* L_2 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_t1281789340* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral3166771228);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3166771228);
		StringU5BU5D_t1281789340* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3452614616);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3452614616);
		StringU5BU5D_t1281789340* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral3452614615);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3452614615);
		StringU5BU5D_t1281789340* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3452614614);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3452614614);
		((GyroscopeEvent_t127774954_StaticFields*)il2cpp_codegen_static_fields_for(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var))->set__gyroscopeEventFieldNames_1(L_6);
		UInt32U5BU5D_t2770800703* L_7 = (UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)4);
		UInt32U5BU5D_t2770800703* L_8 = L_7;
		RuntimeFieldHandle_t1871169219  L_9 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2DD7F443D0D86C2C79F284C1CA7CCCF3C9D9B7B6D8_2_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_8, L_9, /*hidden argument*/NULL);
		((GyroscopeEvent_t127774954_StaticFields*)il2cpp_codegen_static_fields_for(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var))->set__gyroscopeEventFieldTags_2(L_8);
		IL2CPP_RUNTIME_CLASS_INIT(PhoneEvent_t3448566392_il2cpp_TypeInfo_var);
		RuntimeObject * L_10 = ((PhoneEvent_t3448566392_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t3448566392_il2cpp_TypeInfo_var))->get_Descriptor_0();
		il2cpp_codegen_object_reference_equals(L_10, NULL);
		return;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_get_DefaultInstance_m2890176487 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_get_DefaultInstance_m2890176487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		GyroscopeEvent_t127774954 * L_0 = ((GyroscopeEvent_t127774954_StaticFields*)il2cpp_codegen_static_fields_for(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var))->get_defaultInstance_0();
		return L_0;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_get_DefaultInstanceForType_m4240245561 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_get_DefaultInstanceForType_m4240245561_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		GyroscopeEvent_t127774954 * L_0 = GyroscopeEvent_get_DefaultInstance_m2890176487(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::get_ThisMessage()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_get_ThisMessage_m948511096 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_HasTimestamp_m2330998764 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasTimestamp_4();
		return L_0;
	}
}
// System.Int64 proto.PhoneEvent/Types/GyroscopeEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t GyroscopeEvent_get_Timestamp_m727667782 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_timestamp__5();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_HasX_m3534408037 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasX_7();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/GyroscopeEvent::get_X()
extern "C" IL2CPP_METHOD_ATTR float GyroscopeEvent_get_X_m1988437604 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_x__8();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_HasY_m3534342501 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasY_10();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/GyroscopeEvent::get_Y()
extern "C" IL2CPP_METHOD_ATTR float GyroscopeEvent_get_Y_m1988503140 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_y__11();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_HasZ_m3534539109 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasZ_13();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/GyroscopeEvent::get_Z()
extern "C" IL2CPP_METHOD_ATTR float GyroscopeEvent_get_Z_m1988306532 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_z__14();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_get_IsInitialized_m3671694845 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent::WriteTo(Google.ProtocolBuffers.ICodedOutputStream)
extern "C" IL2CPP_METHOD_ATTR void GyroscopeEvent_WriteTo_m3982350230 (GyroscopeEvent_t127774954 * __this, RuntimeObject* ___output0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_WriteTo_m3982350230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_0 = ((GyroscopeEvent_t127774954_StaticFields*)il2cpp_codegen_static_fields_for(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var))->get__gyroscopeEventFieldNames_1();
		V_0 = L_0;
		bool L_1 = __this->get_hasTimestamp_4();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_2 = ___output0;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		String_t* L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int64_t L_6 = GyroscopeEvent_get_Timestamp_m727667782(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker3< int32_t, String_t*, int64_t >::Invoke(1 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteInt64(System.Int32,System.String,System.Int64) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_2, 1, L_5, L_6);
	}

IL_0021:
	{
		bool L_7 = __this->get_hasX_7();
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RuntimeObject* L_8 = ___output0;
		StringU5BU5D_t1281789340* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = 1;
		String_t* L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		float L_12 = GyroscopeEvent_get_X_m1988437604(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_8, 2, L_11, L_12);
	}

IL_003c:
	{
		bool L_13 = __this->get_hasY_10();
		if (!L_13)
		{
			goto IL_0057;
		}
	}
	{
		RuntimeObject* L_14 = ___output0;
		StringU5BU5D_t1281789340* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = 2;
		String_t* L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		float L_18 = GyroscopeEvent_get_Y_m1988503140(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_14, 3, L_17, L_18);
	}

IL_0057:
	{
		bool L_19 = __this->get_hasZ_13();
		if (!L_19)
		{
			goto IL_0072;
		}
	}
	{
		RuntimeObject* L_20 = ___output0;
		StringU5BU5D_t1281789340* L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = 3;
		String_t* L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		float L_24 = GyroscopeEvent_get_Z_m1988306532(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_20, 4, L_23, L_24);
	}

IL_0072:
	{
		return;
	}
}
// System.Int32 proto.PhoneEvent/Types/GyroscopeEvent::get_SerializedSize()
extern "C" IL2CPP_METHOD_ATTR int32_t GyroscopeEvent_get_SerializedSize_m2068668232 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_get_SerializedSize_m2068668232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_memoizedSerializedSize_15();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0010:
	{
		V_0 = 0;
		bool L_3 = __this->get_hasTimestamp_4();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = V_0;
		int64_t L_5 = GyroscopeEvent_get_Timestamp_m727667782(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_6 = CodedOutputStream_ComputeInt64Size_m2929732330(NULL /*static, unused*/, 1, L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_6));
	}

IL_002c:
	{
		bool L_7 = __this->get_hasX_7();
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_8 = V_0;
		float L_9 = GyroscopeEvent_get_X_m1988437604(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_10 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 2, L_9, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)L_10));
	}

IL_0046:
	{
		bool L_11 = __this->get_hasY_10();
		if (!L_11)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_12 = V_0;
		float L_13 = GyroscopeEvent_get_Y_m1988503140(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_14 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 3, L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_14));
	}

IL_0060:
	{
		bool L_15 = __this->get_hasZ_13();
		if (!L_15)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_16 = V_0;
		float L_17 = GyroscopeEvent_get_Z_m1988306532(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_18 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 4, L_17, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_18));
	}

IL_007a:
	{
		int32_t L_19 = V_0;
		__this->set_memoizedSerializedSize_15(L_19);
		int32_t L_20 = V_0;
		return L_20;
	}
}
// System.Int32 proto.PhoneEvent/Types/GyroscopeEvent::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t GyroscopeEvent_GetHashCode_m103407660 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Type_t * L_0 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_hasTimestamp_4();
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = V_0;
		int64_t* L_4 = __this->get_address_of_timestamp__5();
		int32_t L_5 = Int64_GetHashCode_m703091690((int64_t*)L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_3^(int32_t)L_5));
	}

IL_002b:
	{
		bool L_6 = __this->get_hasX_7();
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_7 = V_0;
		float* L_8 = __this->get_address_of_x__8();
		int32_t L_9 = Single_GetHashCode_m1558506138((float*)L_8, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7^(int32_t)L_9));
	}

IL_004a:
	{
		bool L_10 = __this->get_hasY_10();
		if (!L_10)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_11 = V_0;
		float* L_12 = __this->get_address_of_y__11();
		int32_t L_13 = Single_GetHashCode_m1558506138((float*)L_12, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_11^(int32_t)L_13));
	}

IL_0069:
	{
		bool L_14 = __this->get_hasZ_13();
		if (!L_14)
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_15 = V_0;
		float* L_16 = __this->get_address_of_z__14();
		int32_t L_17 = Single_GetHashCode_m1558506138((float*)L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_15^(int32_t)L_17));
	}

IL_0088:
	{
		int32_t L_18 = V_0;
		return L_18;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool GyroscopeEvent_Equals_m1952796240 (GyroscopeEvent_t127774954 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_Equals_m1952796240_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GyroscopeEvent_t127774954 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((GyroscopeEvent_t127774954 *)IsInstSealed((RuntimeObject*)L_0, GyroscopeEvent_t127774954_il2cpp_TypeInfo_var));
		GyroscopeEvent_t127774954 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		bool L_2 = __this->get_hasTimestamp_4();
		GyroscopeEvent_t127774954 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_hasTimestamp_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0041;
		}
	}
	{
		bool L_5 = __this->get_hasTimestamp_4();
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		int64_t* L_6 = __this->get_address_of_timestamp__5();
		GyroscopeEvent_t127774954 * L_7 = V_0;
		NullCheck(L_7);
		int64_t L_8 = L_7->get_timestamp__5();
		bool L_9 = Int64_Equals_m680137412((int64_t*)L_6, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0043;
		}
	}

IL_0041:
	{
		return (bool)0;
	}

IL_0043:
	{
		bool L_10 = __this->get_hasX_7();
		GyroscopeEvent_t127774954 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = L_11->get_hasX_7();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_12))))
		{
			goto IL_0075;
		}
	}
	{
		bool L_13 = __this->get_hasX_7();
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		float* L_14 = __this->get_address_of_x__8();
		GyroscopeEvent_t127774954 * L_15 = V_0;
		NullCheck(L_15);
		float L_16 = L_15->get_x__8();
		bool L_17 = Single_Equals_m1601893879((float*)L_14, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0077;
		}
	}

IL_0075:
	{
		return (bool)0;
	}

IL_0077:
	{
		bool L_18 = __this->get_hasY_10();
		GyroscopeEvent_t127774954 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = L_19->get_hasY_10();
		if ((!(((uint32_t)L_18) == ((uint32_t)L_20))))
		{
			goto IL_00a9;
		}
	}
	{
		bool L_21 = __this->get_hasY_10();
		if (!L_21)
		{
			goto IL_00ab;
		}
	}
	{
		float* L_22 = __this->get_address_of_y__11();
		GyroscopeEvent_t127774954 * L_23 = V_0;
		NullCheck(L_23);
		float L_24 = L_23->get_y__11();
		bool L_25 = Single_Equals_m1601893879((float*)L_22, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00ab;
		}
	}

IL_00a9:
	{
		return (bool)0;
	}

IL_00ab:
	{
		bool L_26 = __this->get_hasZ_13();
		GyroscopeEvent_t127774954 * L_27 = V_0;
		NullCheck(L_27);
		bool L_28 = L_27->get_hasZ_13();
		if ((!(((uint32_t)L_26) == ((uint32_t)L_28))))
		{
			goto IL_00dd;
		}
	}
	{
		bool L_29 = __this->get_hasZ_13();
		if (!L_29)
		{
			goto IL_00df;
		}
	}
	{
		float* L_30 = __this->get_address_of_z__14();
		GyroscopeEvent_t127774954 * L_31 = V_0;
		NullCheck(L_31);
		float L_32 = L_31->get_z__14();
		bool L_33 = Single_Equals_m1601893879((float*)L_30, L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00df;
		}
	}

IL_00dd:
	{
		return (bool)0;
	}

IL_00df:
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent::PrintTo(System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void GyroscopeEvent_PrintTo_m3575727316 (GyroscopeEvent_t127774954 * __this, TextWriter_t3478189236 * ___writer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_PrintTo_m3575727316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_hasTimestamp_4();
		int64_t L_1 = __this->get_timestamp__5();
		int64_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int64_t3736567304_il2cpp_TypeInfo_var, &L_2);
		TextWriter_t3478189236 * L_4 = ___writer0;
		GeneratedMessageLite_2_PrintField_m2673392170(NULL /*static, unused*/, _stringLiteral3166771228, L_0, L_3, L_4, /*hidden argument*/GeneratedMessageLite_2_PrintField_m2673392170_RuntimeMethod_var);
		bool L_5 = __this->get_hasX_7();
		float L_6 = __this->get_x__8();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_7);
		TextWriter_t3478189236 * L_9 = ___writer0;
		GeneratedMessageLite_2_PrintField_m2673392170(NULL /*static, unused*/, _stringLiteral3452614616, L_5, L_8, L_9, /*hidden argument*/GeneratedMessageLite_2_PrintField_m2673392170_RuntimeMethod_var);
		bool L_10 = __this->get_hasY_10();
		float L_11 = __this->get_y__11();
		float L_12 = L_11;
		RuntimeObject * L_13 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_12);
		TextWriter_t3478189236 * L_14 = ___writer0;
		GeneratedMessageLite_2_PrintField_m2673392170(NULL /*static, unused*/, _stringLiteral3452614615, L_10, L_13, L_14, /*hidden argument*/GeneratedMessageLite_2_PrintField_m2673392170_RuntimeMethod_var);
		bool L_15 = __this->get_hasZ_13();
		float L_16 = __this->get_z__14();
		float L_17 = L_16;
		RuntimeObject * L_18 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_17);
		TextWriter_t3478189236 * L_19 = ___writer0;
		GeneratedMessageLite_2_PrintField_m2673392170(NULL /*static, unused*/, _stringLiteral3452614614, L_15, L_18, L_19, /*hidden argument*/GeneratedMessageLite_2_PrintField_m2673392170_RuntimeMethod_var);
		return;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseFrom(Google.ProtocolBuffers.ByteString)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseFrom_m1012639160 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseFrom_m1012639160_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		NullCheck(L_0);
		Builder_t3442751222 * L_2 = AbstractBuilderLite_2_MergeFrom_m2996271875(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m2996271875_RuntimeMethod_var);
		NullCheck(L_2);
		GyroscopeEvent_t127774954 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseFrom_m13958895 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseFrom_m13958895_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t3442751222 * L_3 = AbstractBuilderLite_2_MergeFrom_m1104280193(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m1104280193_RuntimeMethod_var);
		NullCheck(L_3);
		GyroscopeEvent_t127774954 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseFrom(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseFrom_m903272835 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseFrom_m903272835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		NullCheck(L_0);
		Builder_t3442751222 * L_2 = AbstractBuilderLite_2_MergeFrom_m3259242108(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3259242108_RuntimeMethod_var);
		NullCheck(L_2);
		GyroscopeEvent_t127774954 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseFrom_m223739709 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseFrom_m223739709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t3442751222 * L_3 = AbstractBuilderLite_2_MergeFrom_m4289660591(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m4289660591_RuntimeMethod_var);
		NullCheck(L_3);
		GyroscopeEvent_t127774954 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseFrom_m3246061752 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseFrom_m3246061752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t3442751222 * L_2 = AbstractBuilderLite_2_MergeFrom_m3843169734(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3843169734_RuntimeMethod_var);
		NullCheck(L_2);
		GyroscopeEvent_t127774954 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseFrom_m3862662208 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseFrom_m3862662208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t3442751222 * L_3 = AbstractBuilderLite_2_MergeFrom_m1514029506(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m1514029506_RuntimeMethod_var);
		NullCheck(L_3);
		GyroscopeEvent_t127774954 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseDelimitedFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseDelimitedFrom_m2365112648 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseDelimitedFrom_m2365112648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t3442751222 * L_2 = AbstractBuilderLite_2_MergeDelimitedFrom_m4283379067(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m4283379067_RuntimeMethod_var);
		NullCheck(L_2);
		GyroscopeEvent_t127774954 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseDelimitedFrom_m4013462724 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseDelimitedFrom_m4013462724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t3442751222 * L_3 = AbstractBuilderLite_2_MergeDelimitedFrom_m3618057809(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m3618057809_RuntimeMethod_var);
		NullCheck(L_3);
		GyroscopeEvent_t127774954 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseFrom_m3978672244 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseFrom_m3978672244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		NullCheck(L_0);
		Builder_t3442751222 * L_2 = VirtFuncInvoker1< Builder_t3442751222 *, RuntimeObject* >::Invoke(16 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream) */, L_0, L_1);
		NullCheck(L_2);
		GyroscopeEvent_t127774954 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_ParseFrom_m429130748 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ParseFrom_m429130748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m3306880792(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t3442751222 * L_3 = VirtFuncInvoker2< Builder_t3442751222 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, L_0, L_1, L_2);
		NullCheck(L_3);
		GyroscopeEvent_t127774954 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3994085786(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3994085786_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * GyroscopeEvent_MakeReadOnly_m2997968155 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * GyroscopeEvent_CreateBuilder_m3306880792 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_CreateBuilder_m3306880792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t3442751222 * L_0 = (Builder_t3442751222 *)il2cpp_codegen_object_new(Builder_t3442751222_il2cpp_TypeInfo_var);
		Builder__ctor_m2804606681(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent::ToBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * GyroscopeEvent_ToBuilder_m569019558 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_ToBuilder_m569019558_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		Builder_t3442751222 * L_0 = GyroscopeEvent_CreateBuilder_m2664119612(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent::CreateBuilderForType()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * GyroscopeEvent_CreateBuilderForType_m1472271432 (GyroscopeEvent_t127774954 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_CreateBuilderForType_m1472271432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t3442751222 * L_0 = (Builder_t3442751222 *)il2cpp_codegen_object_new(Builder_t3442751222_il2cpp_TypeInfo_var);
		Builder__ctor_m2804606681(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent::CreateBuilder(proto.PhoneEvent/Types/GyroscopeEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * GyroscopeEvent_CreateBuilder_m2664119612 (RuntimeObject * __this /* static, unused */, GyroscopeEvent_t127774954 * ___prototype0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GyroscopeEvent_CreateBuilder_m2664119612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GyroscopeEvent_t127774954 * L_0 = ___prototype0;
		Builder_t3442751222 * L_1 = (Builder_t3442751222 *)il2cpp_codegen_object_new(Builder_t3442751222_il2cpp_TypeInfo_var);
		Builder__ctor_m4245920840(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m2804606681 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m2804606681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m4090092181(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m4090092181_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		GyroscopeEvent_t127774954 * L_0 = GyroscopeEvent_get_DefaultInstance_m2890176487(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::.ctor(proto.PhoneEvent/Types/GyroscopeEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m4245920840 (Builder_t3442751222 * __this, GyroscopeEvent_t127774954 * ___cloneFrom0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m4245920840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m4090092181(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m4090092181_RuntimeMethod_var);
		GyroscopeEvent_t127774954 * L_0 = ___cloneFrom0;
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_ThisBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_get_ThisBuilder_m4272846188 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * Builder_PrepareBuilder_m323407734 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_PrepareBuilder_m323407734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GyroscopeEvent_t127774954 * V_0 = NULL;
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		V_0 = L_1;
		GyroscopeEvent_t127774954 * L_2 = (GyroscopeEvent_t127774954 *)il2cpp_codegen_object_new(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		GyroscopeEvent__ctor_m122134945(L_2, /*hidden argument*/NULL);
		__this->set_result_1(L_2);
		__this->set_resultIsReadOnly_0((bool)0);
		GyroscopeEvent_t127774954 * L_3 = V_0;
		VirtFuncInvoker1< Builder_t3442751222 *, GyroscopeEvent_t127774954 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(!0) */, __this, L_3);
	}

IL_002c:
	{
		GyroscopeEvent_t127774954 * L_4 = __this->get_result_1();
		return L_4;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_IsInitialized_m1267837964 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::get_IsInitialized() */, L_0);
		return L_1;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_MessageBeingBuilt()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * Builder_get_MessageBeingBuilt_m2231598413 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::Clear()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_Clear_m3271464179 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clear_m3271464179_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		GyroscopeEvent_t127774954 * L_0 = GyroscopeEvent_get_DefaultInstance_m2890176487(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::Clone()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_Clone_m1232704756 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clone_m1232704756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		Builder_t3442751222 * L_2 = (Builder_t3442751222 *)il2cpp_codegen_object_new(Builder_t3442751222_il2cpp_TypeInfo_var);
		Builder__ctor_m4245920840(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		Builder_t3442751222 * L_3 = (Builder_t3442751222 *)il2cpp_codegen_object_new(Builder_t3442751222_il2cpp_TypeInfo_var);
		Builder__ctor_m2804606681(L_3, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_4 = __this->get_result_1();
		NullCheck(L_3);
		Builder_t3442751222 * L_5 = VirtFuncInvoker1< Builder_t3442751222 *, GyroscopeEvent_t127774954 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(!0) */, L_3, L_4);
		return L_5;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * Builder_get_DefaultInstanceForType_m127672502 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_get_DefaultInstanceForType_m127672502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		GyroscopeEvent_t127774954 * L_0 = GyroscopeEvent_get_DefaultInstance_m2890176487(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent proto.PhoneEvent/Types/GyroscopeEvent/Builder::BuildPartial()
extern "C" IL2CPP_METHOD_ATTR GyroscopeEvent_t127774954 * Builder_BuildPartial_m421260687 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		return L_1;
	}

IL_0012:
	{
		__this->set_resultIsReadOnly_0((bool)1);
		GyroscopeEvent_t127774954 * L_2 = __this->get_result_1();
		NullCheck(L_2);
		GyroscopeEvent_t127774954 * L_3 = GyroscopeEvent_MakeReadOnly_m2997968155(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_MergeFrom_m1260728313 (Builder_t3442751222 * __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m1260728313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___other0;
		if (!((GyroscopeEvent_t127774954 *)IsInstSealed((RuntimeObject*)L_0, GyroscopeEvent_t127774954_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject* L_1 = ___other0;
		Builder_t3442751222 * L_2 = VirtFuncInvoker1< Builder_t3442751222 *, GyroscopeEvent_t127774954 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(!0) */, __this, ((GyroscopeEvent_t127774954 *)CastclassSealed((RuntimeObject*)L_1, GyroscopeEvent_t127774954_il2cpp_TypeInfo_var)));
		return L_2;
	}

IL_0018:
	{
		RuntimeObject* L_3 = ___other0;
		GeneratedBuilderLite_2_MergeFrom_m2246583089(__this, L_3, /*hidden argument*/GeneratedBuilderLite_2_MergeFrom_m2246583089_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::MergeFrom(proto.PhoneEvent/Types/GyroscopeEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_MergeFrom_m1884232002 (Builder_t3442751222 * __this, GyroscopeEvent_t127774954 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m1884232002_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GyroscopeEvent_t127774954 * L_0 = ___other0;
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		GyroscopeEvent_t127774954 * L_1 = GyroscopeEvent_get_DefaultInstance_m2890176487(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(GyroscopeEvent_t127774954 *)L_0) == ((RuntimeObject*)(GyroscopeEvent_t127774954 *)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = GyroscopeEvent_get_HasTimestamp_m2330998764(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		GyroscopeEvent_t127774954 * L_4 = ___other0;
		NullCheck(L_4);
		int64_t L_5 = GyroscopeEvent_get_Timestamp_m727667782(L_4, /*hidden argument*/NULL);
		Builder_set_Timestamp_m3596341989(__this, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		GyroscopeEvent_t127774954 * L_6 = ___other0;
		NullCheck(L_6);
		bool L_7 = GyroscopeEvent_get_HasX_m3534408037(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		GyroscopeEvent_t127774954 * L_8 = ___other0;
		NullCheck(L_8);
		float L_9 = GyroscopeEvent_get_X_m1988437604(L_8, /*hidden argument*/NULL);
		Builder_set_X_m533575164(__this, L_9, /*hidden argument*/NULL);
	}

IL_0042:
	{
		GyroscopeEvent_t127774954 * L_10 = ___other0;
		NullCheck(L_10);
		bool L_11 = GyroscopeEvent_get_HasY_m3534342501(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0059;
		}
	}
	{
		GyroscopeEvent_t127774954 * L_12 = ___other0;
		NullCheck(L_12);
		float L_13 = GyroscopeEvent_get_Y_m1988503140(L_12, /*hidden argument*/NULL);
		Builder_set_Y_m2318854370(__this, L_13, /*hidden argument*/NULL);
	}

IL_0059:
	{
		GyroscopeEvent_t127774954 * L_14 = ___other0;
		NullCheck(L_14);
		bool L_15 = GyroscopeEvent_get_HasZ_m3534539109(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0070;
		}
	}
	{
		GyroscopeEvent_t127774954 * L_16 = ___other0;
		NullCheck(L_16);
		float L_17 = GyroscopeEvent_get_Z_m1988306532(L_16, /*hidden argument*/NULL);
		Builder_set_Z_m477643105(__this, L_17, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_MergeFrom_m1875709835 (Builder_t3442751222 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m1875709835_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t4271428238_il2cpp_TypeInfo_var);
		ExtensionRegistry_t4271428238 * L_1 = ExtensionRegistry_get_Empty_m3666683255(NULL /*static, unused*/, /*hidden argument*/NULL);
		Builder_t3442751222 * L_2 = VirtFuncInvoker2< Builder_t3442751222 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, __this, L_0, L_1);
		return L_2;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_MergeFrom_m2919572840 (Builder_t3442751222 * __this, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m2919572840_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		goto IL_011e;
	}

IL_000c:
	{
		uint32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_2 = ((GyroscopeEvent_t127774954_StaticFields*)il2cpp_codegen_static_fields_for(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var))->get__gyroscopeEventFieldNames_1();
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t3301955079_il2cpp_TypeInfo_var);
		StringComparer_t3301955079 * L_4 = StringComparer_get_Ordinal_m2103862281(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Array_BinarySearch_TisString_t_m2519237149(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/Array_BinarySearch_TisString_t_m2519237149_RuntimeMethod_var);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_7 = ((GyroscopeEvent_t127774954_StaticFields*)il2cpp_codegen_static_fields_for(GyroscopeEvent_t127774954_il2cpp_TypeInfo_var))->get__gyroscopeEventFieldTags_2();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		goto IL_004d;
	}

IL_003d:
	{
		RuntimeObject* L_11 = ___input0;
		ExtensionRegistry_t4271428238 * L_12 = ___extensionRegistry1;
		uint32_t L_13 = V_0;
		String_t* L_14 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_11, L_12, L_13, L_14);
		goto IL_011e;
	}

IL_004d:
	{
		uint32_t L_15 = V_0;
		if (!L_15)
		{
			goto IL_0077;
		}
	}
	{
		uint32_t L_16 = V_0;
		if ((((int32_t)L_16) == ((int32_t)8)))
		{
			goto IL_009a;
		}
	}
	{
		uint32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)21))))
		{
			goto IL_00bb;
		}
	}
	{
		uint32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)29))))
		{
			goto IL_00dc;
		}
	}
	{
		uint32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)37))))
		{
			goto IL_00fd;
		}
	}
	{
		goto IL_007d;
	}

IL_0077:
	{
		InvalidProtocolBufferException_t2498581859 * L_20 = InvalidProtocolBufferException_InvalidTag_m4139780452(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, NULL, Builder_MergeFrom_m2919572840_RuntimeMethod_var);
	}

IL_007d:
	{
		uint32_t L_21 = V_0;
		bool L_22 = WireFormat_IsEndGroupTag_m2504732292(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_008a;
		}
	}
	{
		return __this;
	}

IL_008a:
	{
		RuntimeObject* L_23 = ___input0;
		ExtensionRegistry_t4271428238 * L_24 = ___extensionRegistry1;
		uint32_t L_25 = V_0;
		String_t* L_26 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/GyroscopeEvent,proto.PhoneEvent/Types/GyroscopeEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_23, L_24, L_25, L_26);
		goto IL_011e;
	}

IL_009a:
	{
		GyroscopeEvent_t127774954 * L_27 = __this->get_result_1();
		RuntimeObject* L_28 = ___input0;
		GyroscopeEvent_t127774954 * L_29 = __this->get_result_1();
		NullCheck(L_29);
		int64_t* L_30 = L_29->get_address_of_timestamp__5();
		NullCheck(L_28);
		bool L_31 = InterfaceFuncInvoker1< bool, int64_t* >::Invoke(2 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt64(System.Int64&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_28, (int64_t*)L_30);
		NullCheck(L_27);
		L_27->set_hasTimestamp_4(L_31);
		goto IL_011e;
	}

IL_00bb:
	{
		GyroscopeEvent_t127774954 * L_32 = __this->get_result_1();
		RuntimeObject* L_33 = ___input0;
		GyroscopeEvent_t127774954 * L_34 = __this->get_result_1();
		NullCheck(L_34);
		float* L_35 = L_34->get_address_of_x__8();
		NullCheck(L_33);
		bool L_36 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_33, (float*)L_35);
		NullCheck(L_32);
		L_32->set_hasX_7(L_36);
		goto IL_011e;
	}

IL_00dc:
	{
		GyroscopeEvent_t127774954 * L_37 = __this->get_result_1();
		RuntimeObject* L_38 = ___input0;
		GyroscopeEvent_t127774954 * L_39 = __this->get_result_1();
		NullCheck(L_39);
		float* L_40 = L_39->get_address_of_y__11();
		NullCheck(L_38);
		bool L_41 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_38, (float*)L_40);
		NullCheck(L_37);
		L_37->set_hasY_10(L_41);
		goto IL_011e;
	}

IL_00fd:
	{
		GyroscopeEvent_t127774954 * L_42 = __this->get_result_1();
		RuntimeObject* L_43 = ___input0;
		GyroscopeEvent_t127774954 * L_44 = __this->get_result_1();
		NullCheck(L_44);
		float* L_45 = L_44->get_address_of_z__14();
		NullCheck(L_43);
		bool L_46 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_43, (float*)L_45);
		NullCheck(L_42);
		L_42->set_hasZ_13(L_46);
		goto IL_011e;
	}

IL_011e:
	{
		RuntimeObject* L_47 = ___input0;
		NullCheck(L_47);
		bool L_48 = InterfaceFuncInvoker2< bool, uint32_t*, String_t** >::Invoke(0 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadTag(System.UInt32&,System.String&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_47, (uint32_t*)(&V_0), (String_t**)(&V_1));
		if (L_48)
		{
			goto IL_000c;
		}
	}
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasTimestamp_m1766414663 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasTimestamp_4();
		return L_1;
	}
}
// System.Int64 proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t Builder_get_Timestamp_m2661728860 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int64_t L_1 = GyroscopeEvent_get_Timestamp_m727667782(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m3596341989 (Builder_t3442751222 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		int64_t L_0 = ___value0;
		Builder_SetTimestamp_m3662053200(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_SetTimestamp_m3662053200 (Builder_t3442751222 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)1);
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		int64_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_timestamp__5(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::ClearTimestamp()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_ClearTimestamp_m1545040199 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)0);
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_timestamp__5((((int64_t)((int64_t)0))));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasX_m4216176614 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasX_7();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_X()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_X_m3561010060 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = GyroscopeEvent_get_X_m1988437604(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::set_X(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_X_m533575164 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetX_m4266037197(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::SetX(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_SetX_m4266037197 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasX_7((bool)1);
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_x__8(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::ClearX()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_ClearX_m4172184204 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasX_7((bool)0);
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_x__8((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasY_m4216111078 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasY_10();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_Y()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_Y_m3560944524 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = GyroscopeEvent_get_Y_m1988503140(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::set_Y(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Y_m2318854370 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetY_m4263658798(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::SetY(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_SetY_m4263658798 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasY_10((bool)1);
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_y__11(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::ClearY()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_ClearY_m1443300849 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasY_10((bool)0);
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_y__11((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasZ_m4216307686 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasZ_13();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/GyroscopeEvent/Builder::get_Z()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_Z_m3560878988 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = GyroscopeEvent_get_Z_m1988306532(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/GyroscopeEvent/Builder::set_Z(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Z_m477643105 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetZ_m4266136855(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::SetZ(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_SetZ_m4266136855 (Builder_t3442751222 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasZ_13((bool)1);
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_z__14(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/GyroscopeEvent/Builder proto.PhoneEvent/Types/GyroscopeEvent/Builder::ClearZ()
extern "C" IL2CPP_METHOD_ATTR Builder_t3442751222 * Builder_ClearZ_m3009384790 (Builder_t3442751222 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m323407734(__this, /*hidden argument*/NULL);
		GyroscopeEvent_t127774954 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasZ_13((bool)0);
		GyroscopeEvent_t127774954 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_z__14((0.0f));
		return __this;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/KeyEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void KeyEvent__ctor_m4217360548 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent__ctor_m4217360548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_memoizedSerializedSize_9((-1));
		GeneratedMessageLite_2__ctor_m3776640171(__this, /*hidden argument*/GeneratedMessageLite_2__ctor_m3776640171_RuntimeMethod_var);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/KeyEvent::.cctor()
extern "C" IL2CPP_METHOD_ATTR void KeyEvent__cctor_m3482539191 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent__cctor_m3482539191_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeyEvent_t1937114521 * L_0 = (KeyEvent_t1937114521 *)il2cpp_codegen_object_new(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		KeyEvent__ctor_m4217360548(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		KeyEvent_t1937114521 * L_1 = KeyEvent_MakeReadOnly_m4090303625(L_0, /*hidden argument*/NULL);
		((KeyEvent_t1937114521_StaticFields*)il2cpp_codegen_static_fields_for(KeyEvent_t1937114521_il2cpp_TypeInfo_var))->set_defaultInstance_0(L_1);
		StringU5BU5D_t1281789340* L_2 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)2);
		StringU5BU5D_t1281789340* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral2365897554);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2365897554);
		StringU5BU5D_t1281789340* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral1985170611);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral1985170611);
		((KeyEvent_t1937114521_StaticFields*)il2cpp_codegen_static_fields_for(KeyEvent_t1937114521_il2cpp_TypeInfo_var))->set__keyEventFieldNames_1(L_4);
		UInt32U5BU5D_t2770800703* L_5 = (UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)2);
		UInt32U5BU5D_t2770800703* L_6 = L_5;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint32_t)8);
		UInt32U5BU5D_t2770800703* L_7 = L_6;
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint32_t)((int32_t)16));
		((KeyEvent_t1937114521_StaticFields*)il2cpp_codegen_static_fields_for(KeyEvent_t1937114521_il2cpp_TypeInfo_var))->set__keyEventFieldTags_2(L_7);
		IL2CPP_RUNTIME_CLASS_INIT(PhoneEvent_t3448566392_il2cpp_TypeInfo_var);
		RuntimeObject * L_8 = ((PhoneEvent_t3448566392_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t3448566392_il2cpp_TypeInfo_var))->get_Descriptor_0();
		il2cpp_codegen_object_reference_equals(L_8, NULL);
		return;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_get_DefaultInstance_m740969952 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_get_DefaultInstance_m740969952_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		KeyEvent_t1937114521 * L_0 = ((KeyEvent_t1937114521_StaticFields*)il2cpp_codegen_static_fields_for(KeyEvent_t1937114521_il2cpp_TypeInfo_var))->get_defaultInstance_0();
		return L_0;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_get_DefaultInstanceForType_m3895015777 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_get_DefaultInstanceForType_m3895015777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		KeyEvent_t1937114521 * L_0 = KeyEvent_get_DefaultInstance_m740969952(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::get_ThisMessage()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_get_ThisMessage_m1735128955 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/KeyEvent::get_HasAction()
extern "C" IL2CPP_METHOD_ATTR bool KeyEvent_get_HasAction_m3390159228 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasAction_4();
		return L_0;
	}
}
// System.Int32 proto.PhoneEvent/Types/KeyEvent::get_Action()
extern "C" IL2CPP_METHOD_ATTR int32_t KeyEvent_get_Action_m1195418036 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_action__5();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/KeyEvent::get_HasCode()
extern "C" IL2CPP_METHOD_ATTR bool KeyEvent_get_HasCode_m2936208505 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasCode_7();
		return L_0;
	}
}
// System.Int32 proto.PhoneEvent/Types/KeyEvent::get_Code()
extern "C" IL2CPP_METHOD_ATTR int32_t KeyEvent_get_Code_m2196363321 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_code__8();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/KeyEvent::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool KeyEvent_get_IsInitialized_m3498692825 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/KeyEvent::WriteTo(Google.ProtocolBuffers.ICodedOutputStream)
extern "C" IL2CPP_METHOD_ATTR void KeyEvent_WriteTo_m4091272948 (KeyEvent_t1937114521 * __this, RuntimeObject* ___output0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_WriteTo_m4091272948_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_0 = ((KeyEvent_t1937114521_StaticFields*)il2cpp_codegen_static_fields_for(KeyEvent_t1937114521_il2cpp_TypeInfo_var))->get__keyEventFieldNames_1();
		V_0 = L_0;
		bool L_1 = __this->get_hasAction_4();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_2 = ___output0;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		String_t* L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int32_t L_6 = KeyEvent_get_Action_m1195418036(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker3< int32_t, String_t*, int32_t >::Invoke(2 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteInt32(System.Int32,System.String,System.Int32) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_2, 1, L_5, L_6);
	}

IL_0021:
	{
		bool L_7 = __this->get_hasCode_7();
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RuntimeObject* L_8 = ___output0;
		StringU5BU5D_t1281789340* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = 1;
		String_t* L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		int32_t L_12 = KeyEvent_get_Code_m2196363321(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker3< int32_t, String_t*, int32_t >::Invoke(2 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteInt32(System.Int32,System.String,System.Int32) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_8, 2, L_11, L_12);
	}

IL_003c:
	{
		return;
	}
}
// System.Int32 proto.PhoneEvent/Types/KeyEvent::get_SerializedSize()
extern "C" IL2CPP_METHOD_ATTR int32_t KeyEvent_get_SerializedSize_m1954691348 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_get_SerializedSize_m1954691348_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_memoizedSerializedSize_9();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0010:
	{
		V_0 = 0;
		bool L_3 = __this->get_hasAction_4();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = KeyEvent_get_Action_m1195418036(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_6 = CodedOutputStream_ComputeInt32Size_m1726852181(NULL /*static, unused*/, 1, L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_6));
	}

IL_002c:
	{
		bool L_7 = __this->get_hasCode_7();
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_8 = V_0;
		int32_t L_9 = KeyEvent_get_Code_m2196363321(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_10 = CodedOutputStream_ComputeInt32Size_m1726852181(NULL /*static, unused*/, 2, L_9, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)L_10));
	}

IL_0046:
	{
		int32_t L_11 = V_0;
		__this->set_memoizedSerializedSize_9(L_11);
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Int32 proto.PhoneEvent/Types/KeyEvent::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t KeyEvent_GetHashCode_m3450843954 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Type_t * L_0 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_hasAction_4();
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = V_0;
		int32_t* L_4 = __this->get_address_of_action__5();
		int32_t L_5 = Int32_GetHashCode_m1876651407((int32_t*)L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_3^(int32_t)L_5));
	}

IL_002b:
	{
		bool L_6 = __this->get_hasCode_7();
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t* L_8 = __this->get_address_of_code__8();
		int32_t L_9 = Int32_GetHashCode_m1876651407((int32_t*)L_8, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7^(int32_t)L_9));
	}

IL_004a:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Boolean proto.PhoneEvent/Types/KeyEvent::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool KeyEvent_Equals_m3306860044 (KeyEvent_t1937114521 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_Equals_m3306860044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyEvent_t1937114521 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((KeyEvent_t1937114521 *)IsInstSealed((RuntimeObject*)L_0, KeyEvent_t1937114521_il2cpp_TypeInfo_var));
		KeyEvent_t1937114521 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		bool L_2 = __this->get_hasAction_4();
		KeyEvent_t1937114521 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_hasAction_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0041;
		}
	}
	{
		bool L_5 = __this->get_hasAction_4();
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		int32_t* L_6 = __this->get_address_of_action__5();
		KeyEvent_t1937114521 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_action__5();
		bool L_9 = Int32_Equals_m2976157357((int32_t*)L_6, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0043;
		}
	}

IL_0041:
	{
		return (bool)0;
	}

IL_0043:
	{
		bool L_10 = __this->get_hasCode_7();
		KeyEvent_t1937114521 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = L_11->get_hasCode_7();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_12))))
		{
			goto IL_0075;
		}
	}
	{
		bool L_13 = __this->get_hasCode_7();
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		int32_t* L_14 = __this->get_address_of_code__8();
		KeyEvent_t1937114521 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_code__8();
		bool L_17 = Int32_Equals_m2976157357((int32_t*)L_14, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0077;
		}
	}

IL_0075:
	{
		return (bool)0;
	}

IL_0077:
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/KeyEvent::PrintTo(System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void KeyEvent_PrintTo_m3049625899 (KeyEvent_t1937114521 * __this, TextWriter_t3478189236 * ___writer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_PrintTo_m3049625899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_hasAction_4();
		int32_t L_1 = __this->get_action__5();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		TextWriter_t3478189236 * L_4 = ___writer0;
		GeneratedMessageLite_2_PrintField_m1636826668(NULL /*static, unused*/, _stringLiteral2365897554, L_0, L_3, L_4, /*hidden argument*/GeneratedMessageLite_2_PrintField_m1636826668_RuntimeMethod_var);
		bool L_5 = __this->get_hasCode_7();
		int32_t L_6 = __this->get_code__8();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		TextWriter_t3478189236 * L_9 = ___writer0;
		GeneratedMessageLite_2_PrintField_m1636826668(NULL /*static, unused*/, _stringLiteral1985170611, L_5, L_8, L_9, /*hidden argument*/GeneratedMessageLite_2_PrintField_m1636826668_RuntimeMethod_var);
		return;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseFrom(Google.ProtocolBuffers.ByteString)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseFrom_m2103175321 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseFrom_m2103175321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		NullCheck(L_0);
		Builder_t2712992173 * L_2 = AbstractBuilderLite_2_MergeFrom_m3650812895(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3650812895_RuntimeMethod_var);
		NullCheck(L_2);
		KeyEvent_t1937114521 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseFrom_m3197497049 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseFrom_m3197497049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2712992173 * L_3 = AbstractBuilderLite_2_MergeFrom_m3060350321(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3060350321_RuntimeMethod_var);
		NullCheck(L_3);
		KeyEvent_t1937114521 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseFrom(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseFrom_m476182167 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseFrom_m476182167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		NullCheck(L_0);
		Builder_t2712992173 * L_2 = AbstractBuilderLite_2_MergeFrom_m3720688089(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3720688089_RuntimeMethod_var);
		NullCheck(L_2);
		KeyEvent_t1937114521 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseFrom_m3061039951 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseFrom_m3061039951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2712992173 * L_3 = AbstractBuilderLite_2_MergeFrom_m908247555(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m908247555_RuntimeMethod_var);
		NullCheck(L_3);
		KeyEvent_t1937114521 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseFrom_m3815981327 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseFrom_m3815981327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2712992173 * L_2 = AbstractBuilderLite_2_MergeFrom_m3965376215(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3965376215_RuntimeMethod_var);
		NullCheck(L_2);
		KeyEvent_t1937114521 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseFrom_m3950290315 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseFrom_m3950290315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2712992173 * L_3 = AbstractBuilderLite_2_MergeFrom_m1101353770(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m1101353770_RuntimeMethod_var);
		NullCheck(L_3);
		KeyEvent_t1937114521 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseDelimitedFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseDelimitedFrom_m3509952229 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseDelimitedFrom_m3509952229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2712992173 * L_2 = AbstractBuilderLite_2_MergeDelimitedFrom_m25623811(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m25623811_RuntimeMethod_var);
		NullCheck(L_2);
		KeyEvent_t1937114521 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseDelimitedFrom_m2737003127 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseDelimitedFrom_m2737003127_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2712992173 * L_3 = AbstractBuilderLite_2_MergeDelimitedFrom_m3541096835(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m3541096835_RuntimeMethod_var);
		NullCheck(L_3);
		KeyEvent_t1937114521 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseFrom_m2519169384 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseFrom_m2519169384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2712992173 * L_2 = VirtFuncInvoker1< Builder_t2712992173 *, RuntimeObject* >::Invoke(16 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream) */, L_0, L_1);
		NullCheck(L_2);
		KeyEvent_t1937114521 * L_3 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_ParseFrom_m3654509195 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ParseFrom_m3654509195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m2320679930(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2712992173 * L_3 = VirtFuncInvoker2< Builder_t2712992173 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, L_0, L_1, L_2);
		NullCheck(L_3);
		KeyEvent_t1937114521 * L_4 = GeneratedBuilderLite_2_BuildParsed_m3604012727(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m3604012727_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * KeyEvent_MakeReadOnly_m4090303625 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * KeyEvent_CreateBuilder_m2320679930 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_CreateBuilder_m2320679930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t2712992173 * L_0 = (Builder_t2712992173 *)il2cpp_codegen_object_new(Builder_t2712992173_il2cpp_TypeInfo_var);
		Builder__ctor_m4137946296(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent::ToBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * KeyEvent_ToBuilder_m2617464935 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_ToBuilder_m2617464935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		Builder_t2712992173 * L_0 = KeyEvent_CreateBuilder_m83685355(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent::CreateBuilderForType()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * KeyEvent_CreateBuilderForType_m559879078 (KeyEvent_t1937114521 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_CreateBuilderForType_m559879078_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t2712992173 * L_0 = (Builder_t2712992173 *)il2cpp_codegen_object_new(Builder_t2712992173_il2cpp_TypeInfo_var);
		Builder__ctor_m4137946296(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent::CreateBuilder(proto.PhoneEvent/Types/KeyEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * KeyEvent_CreateBuilder_m83685355 (RuntimeObject * __this /* static, unused */, KeyEvent_t1937114521 * ___prototype0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyEvent_CreateBuilder_m83685355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeyEvent_t1937114521 * L_0 = ___prototype0;
		Builder_t2712992173 * L_1 = (Builder_t2712992173 *)il2cpp_codegen_object_new(Builder_t2712992173_il2cpp_TypeInfo_var);
		Builder__ctor_m2822184233(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/KeyEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m4137946296 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m4137946296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m24974982(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m24974982_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		KeyEvent_t1937114521 * L_0 = KeyEvent_get_DefaultInstance_m740969952(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/KeyEvent/Builder::.ctor(proto.PhoneEvent/Types/KeyEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m2822184233 (Builder_t2712992173 * __this, KeyEvent_t1937114521 * ___cloneFrom0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m2822184233_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m24974982(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m24974982_RuntimeMethod_var);
		KeyEvent_t1937114521 * L_0 = ___cloneFrom0;
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::get_ThisBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_get_ThisBuilder_m3098577915 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * Builder_PrepareBuilder_m2114990423 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_PrepareBuilder_m2114990423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	KeyEvent_t1937114521 * V_0 = NULL;
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		KeyEvent_t1937114521 * L_1 = __this->get_result_1();
		V_0 = L_1;
		KeyEvent_t1937114521 * L_2 = (KeyEvent_t1937114521 *)il2cpp_codegen_object_new(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		KeyEvent__ctor_m4217360548(L_2, /*hidden argument*/NULL);
		__this->set_result_1(L_2);
		__this->set_resultIsReadOnly_0((bool)0);
		KeyEvent_t1937114521 * L_3 = V_0;
		VirtFuncInvoker1< Builder_t2712992173 *, KeyEvent_t1937114521 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(!0) */, __this, L_3);
	}

IL_002c:
	{
		KeyEvent_t1937114521 * L_4 = __this->get_result_1();
		return L_4;
	}
}
// System.Boolean proto.PhoneEvent/Types/KeyEvent/Builder::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_IsInitialized_m2260771490 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::get_IsInitialized() */, L_0);
		return L_1;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent/Builder::get_MessageBeingBuilt()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * Builder_get_MessageBeingBuilt_m923871465 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		KeyEvent_t1937114521 * L_0 = Builder_PrepareBuilder_m2114990423(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::Clear()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_Clear_m3080749485 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clear_m3080749485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		KeyEvent_t1937114521 * L_0 = KeyEvent_get_DefaultInstance_m740969952(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return __this;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::Clone()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_Clone_m1690141094 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clone_m1690141094_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		KeyEvent_t1937114521 * L_1 = __this->get_result_1();
		Builder_t2712992173 * L_2 = (Builder_t2712992173 *)il2cpp_codegen_object_new(Builder_t2712992173_il2cpp_TypeInfo_var);
		Builder__ctor_m2822184233(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		Builder_t2712992173 * L_3 = (Builder_t2712992173 *)il2cpp_codegen_object_new(Builder_t2712992173_il2cpp_TypeInfo_var);
		Builder__ctor_m4137946296(L_3, /*hidden argument*/NULL);
		KeyEvent_t1937114521 * L_4 = __this->get_result_1();
		NullCheck(L_3);
		Builder_t2712992173 * L_5 = VirtFuncInvoker1< Builder_t2712992173 *, KeyEvent_t1937114521 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(!0) */, L_3, L_4);
		return L_5;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent/Builder::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * Builder_get_DefaultInstanceForType_m699105530 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_get_DefaultInstanceForType_m699105530_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		KeyEvent_t1937114521 * L_0 = KeyEvent_get_DefaultInstance_m740969952(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/KeyEvent proto.PhoneEvent/Types/KeyEvent/Builder::BuildPartial()
extern "C" IL2CPP_METHOD_ATTR KeyEvent_t1937114521 * Builder_BuildPartial_m462320873 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		KeyEvent_t1937114521 * L_1 = __this->get_result_1();
		return L_1;
	}

IL_0012:
	{
		__this->set_resultIsReadOnly_0((bool)1);
		KeyEvent_t1937114521 * L_2 = __this->get_result_1();
		NullCheck(L_2);
		KeyEvent_t1937114521 * L_3 = KeyEvent_MakeReadOnly_m4090303625(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_MergeFrom_m4190562827 (Builder_t2712992173 * __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m4190562827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___other0;
		if (!((KeyEvent_t1937114521 *)IsInstSealed((RuntimeObject*)L_0, KeyEvent_t1937114521_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject* L_1 = ___other0;
		Builder_t2712992173 * L_2 = VirtFuncInvoker1< Builder_t2712992173 *, KeyEvent_t1937114521 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(!0) */, __this, ((KeyEvent_t1937114521 *)CastclassSealed((RuntimeObject*)L_1, KeyEvent_t1937114521_il2cpp_TypeInfo_var)));
		return L_2;
	}

IL_0018:
	{
		RuntimeObject* L_3 = ___other0;
		GeneratedBuilderLite_2_MergeFrom_m157053624(__this, L_3, /*hidden argument*/GeneratedBuilderLite_2_MergeFrom_m157053624_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::MergeFrom(proto.PhoneEvent/Types/KeyEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_MergeFrom_m3674657960 (Builder_t2712992173 * __this, KeyEvent_t1937114521 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m3674657960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		KeyEvent_t1937114521 * L_0 = ___other0;
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		KeyEvent_t1937114521 * L_1 = KeyEvent_get_DefaultInstance_m740969952(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(KeyEvent_t1937114521 *)L_0) == ((RuntimeObject*)(KeyEvent_t1937114521 *)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Builder_PrepareBuilder_m2114990423(__this, /*hidden argument*/NULL);
		KeyEvent_t1937114521 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = KeyEvent_get_HasAction_m3390159228(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		KeyEvent_t1937114521 * L_4 = ___other0;
		NullCheck(L_4);
		int32_t L_5 = KeyEvent_get_Action_m1195418036(L_4, /*hidden argument*/NULL);
		Builder_set_Action_m1176581797(__this, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		KeyEvent_t1937114521 * L_6 = ___other0;
		NullCheck(L_6);
		bool L_7 = KeyEvent_get_HasCode_m2936208505(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		KeyEvent_t1937114521 * L_8 = ___other0;
		NullCheck(L_8);
		int32_t L_9 = KeyEvent_get_Code_m2196363321(L_8, /*hidden argument*/NULL);
		Builder_set_Code_m3879885671(__this, L_9, /*hidden argument*/NULL);
	}

IL_0042:
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_MergeFrom_m3488476321 (Builder_t2712992173 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m3488476321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t4271428238_il2cpp_TypeInfo_var);
		ExtensionRegistry_t4271428238 * L_1 = ExtensionRegistry_get_Empty_m3666683255(NULL /*static, unused*/, /*hidden argument*/NULL);
		Builder_t2712992173 * L_2 = VirtFuncInvoker2< Builder_t2712992173 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, __this, L_0, L_1);
		return L_2;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_MergeFrom_m2313681359 (Builder_t2712992173 * __this, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m2313681359_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Builder_PrepareBuilder_m2114990423(__this, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_000c:
	{
		uint32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_2 = ((KeyEvent_t1937114521_StaticFields*)il2cpp_codegen_static_fields_for(KeyEvent_t1937114521_il2cpp_TypeInfo_var))->get__keyEventFieldNames_1();
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t3301955079_il2cpp_TypeInfo_var);
		StringComparer_t3301955079 * L_4 = StringComparer_get_Ordinal_m2103862281(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Array_BinarySearch_TisString_t_m2519237149(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/Array_BinarySearch_TisString_t_m2519237149_RuntimeMethod_var);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(KeyEvent_t1937114521_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_7 = ((KeyEvent_t1937114521_StaticFields*)il2cpp_codegen_static_fields_for(KeyEvent_t1937114521_il2cpp_TypeInfo_var))->get__keyEventFieldTags_2();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		goto IL_004d;
	}

IL_003d:
	{
		RuntimeObject* L_11 = ___input0;
		ExtensionRegistry_t4271428238 * L_12 = ___extensionRegistry1;
		uint32_t L_13 = V_0;
		String_t* L_14 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_11, L_12, L_13, L_14);
		goto IL_00cc;
	}

IL_004d:
	{
		uint32_t L_15 = V_0;
		if (!L_15)
		{
			goto IL_0067;
		}
	}
	{
		uint32_t L_16 = V_0;
		if ((((int32_t)L_16) == ((int32_t)8)))
		{
			goto IL_008a;
		}
	}
	{
		uint32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)16))))
		{
			goto IL_00ab;
		}
	}
	{
		goto IL_006d;
	}

IL_0067:
	{
		InvalidProtocolBufferException_t2498581859 * L_18 = InvalidProtocolBufferException_InvalidTag_m4139780452(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18, NULL, Builder_MergeFrom_m2313681359_RuntimeMethod_var);
	}

IL_006d:
	{
		uint32_t L_19 = V_0;
		bool L_20 = WireFormat_IsEndGroupTag_m2504732292(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_007a;
		}
	}
	{
		return __this;
	}

IL_007a:
	{
		RuntimeObject* L_21 = ___input0;
		ExtensionRegistry_t4271428238 * L_22 = ___extensionRegistry1;
		uint32_t L_23 = V_0;
		String_t* L_24 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/KeyEvent,proto.PhoneEvent/Types/KeyEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_21, L_22, L_23, L_24);
		goto IL_00cc;
	}

IL_008a:
	{
		KeyEvent_t1937114521 * L_25 = __this->get_result_1();
		RuntimeObject* L_26 = ___input0;
		KeyEvent_t1937114521 * L_27 = __this->get_result_1();
		NullCheck(L_27);
		int32_t* L_28 = L_27->get_address_of_action__5();
		NullCheck(L_26);
		bool L_29 = InterfaceFuncInvoker1< bool, int32_t* >::Invoke(3 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt32(System.Int32&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_26, (int32_t*)L_28);
		NullCheck(L_25);
		L_25->set_hasAction_4(L_29);
		goto IL_00cc;
	}

IL_00ab:
	{
		KeyEvent_t1937114521 * L_30 = __this->get_result_1();
		RuntimeObject* L_31 = ___input0;
		KeyEvent_t1937114521 * L_32 = __this->get_result_1();
		NullCheck(L_32);
		int32_t* L_33 = L_32->get_address_of_code__8();
		NullCheck(L_31);
		bool L_34 = InterfaceFuncInvoker1< bool, int32_t* >::Invoke(3 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt32(System.Int32&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_31, (int32_t*)L_33);
		NullCheck(L_30);
		L_30->set_hasCode_7(L_34);
		goto IL_00cc;
	}

IL_00cc:
	{
		RuntimeObject* L_35 = ___input0;
		NullCheck(L_35);
		bool L_36 = InterfaceFuncInvoker2< bool, uint32_t*, String_t** >::Invoke(0 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadTag(System.UInt32&,System.String&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_35, (uint32_t*)(&V_0), (String_t**)(&V_1));
		if (L_36)
		{
			goto IL_000c;
		}
	}
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/KeyEvent/Builder::get_HasAction()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasAction_m1867829166 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasAction_4();
		return L_1;
	}
}
// System.Int32 proto.PhoneEvent/Types/KeyEvent/Builder::get_Action()
extern "C" IL2CPP_METHOD_ATTR int32_t Builder_get_Action_m2045361311 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int32_t L_1 = KeyEvent_get_Action_m1195418036(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/KeyEvent/Builder::set_Action(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Action_m1176581797 (Builder_t2712992173 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		Builder_SetAction_m1997413468(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::SetAction(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_SetAction_m1997413468 (Builder_t2712992173 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2114990423(__this, /*hidden argument*/NULL);
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasAction_4((bool)1);
		KeyEvent_t1937114521 * L_1 = __this->get_result_1();
		int32_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_action__5(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::ClearAction()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_ClearAction_m415035503 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2114990423(__this, /*hidden argument*/NULL);
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasAction_4((bool)0);
		KeyEvent_t1937114521 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_action__5(0);
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/KeyEvent/Builder::get_HasCode()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasCode_m414637353 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasCode_7();
		return L_1;
	}
}
// System.Int32 proto.PhoneEvent/Types/KeyEvent/Builder::get_Code()
extern "C" IL2CPP_METHOD_ATTR int32_t Builder_get_Code_m3525792902 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int32_t L_1 = KeyEvent_get_Code_m2196363321(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/KeyEvent/Builder::set_Code(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Code_m3879885671 (Builder_t2712992173 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		Builder_SetCode_m1980311967(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::SetCode(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_SetCode_m1980311967 (Builder_t2712992173 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2114990423(__this, /*hidden argument*/NULL);
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasCode_7((bool)1);
		KeyEvent_t1937114521 * L_1 = __this->get_result_1();
		int32_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_code__8(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/KeyEvent/Builder proto.PhoneEvent/Types/KeyEvent/Builder::ClearCode()
extern "C" IL2CPP_METHOD_ATTR Builder_t2712992173 * Builder_ClearCode_m4094556548 (Builder_t2712992173 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2114990423(__this, /*hidden argument*/NULL);
		KeyEvent_t1937114521 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasCode_7((bool)0);
		KeyEvent_t1937114521 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_code__8(0);
		return __this;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/MotionEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MotionEvent__ctor_m1502834634 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent__ctor_m1502834634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PopsicleList_1_t1233628674 * L_0 = (PopsicleList_1_t1233628674 *)il2cpp_codegen_object_new(PopsicleList_1_t1233628674_il2cpp_TypeInfo_var);
		PopsicleList_1__ctor_m3092888001(L_0, /*hidden argument*/PopsicleList_1__ctor_m3092888001_RuntimeMethod_var);
		__this->set_pointers__10(L_0);
		__this->set_memoizedSerializedSize_11((-1));
		GeneratedMessageLite_2__ctor_m326934031(__this, /*hidden argument*/GeneratedMessageLite_2__ctor_m326934031_RuntimeMethod_var);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent::.cctor()
extern "C" IL2CPP_METHOD_ATTR void MotionEvent__cctor_m2507959477 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent__cctor_m2507959477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MotionEvent_t3121383016 * L_0 = (MotionEvent_t3121383016 *)il2cpp_codegen_object_new(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		MotionEvent__ctor_m1502834634(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		MotionEvent_t3121383016 * L_1 = MotionEvent_MakeReadOnly_m1076849093(L_0, /*hidden argument*/NULL);
		((MotionEvent_t3121383016_StaticFields*)il2cpp_codegen_static_fields_for(MotionEvent_t3121383016_il2cpp_TypeInfo_var))->set_defaultInstance_0(L_1);
		StringU5BU5D_t1281789340* L_2 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_t1281789340* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral2365897554);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2365897554);
		StringU5BU5D_t1281789340* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral4020252391);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral4020252391);
		StringU5BU5D_t1281789340* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral3166771228);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3166771228);
		((MotionEvent_t3121383016_StaticFields*)il2cpp_codegen_static_fields_for(MotionEvent_t3121383016_il2cpp_TypeInfo_var))->set__motionEventFieldNames_1(L_5);
		UInt32U5BU5D_t2770800703* L_6 = (UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)3);
		UInt32U5BU5D_t2770800703* L_7 = L_6;
		RuntimeFieldHandle_t1871169219  L_8 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2D16E2B412E9C2B8E31B780DE46254349320CCAAA0_1_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_7, L_8, /*hidden argument*/NULL);
		((MotionEvent_t3121383016_StaticFields*)il2cpp_codegen_static_fields_for(MotionEvent_t3121383016_il2cpp_TypeInfo_var))->set__motionEventFieldTags_2(L_7);
		IL2CPP_RUNTIME_CLASS_INIT(PhoneEvent_t3448566392_il2cpp_TypeInfo_var);
		RuntimeObject * L_9 = ((PhoneEvent_t3448566392_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t3448566392_il2cpp_TypeInfo_var))->get_Descriptor_0();
		il2cpp_codegen_object_reference_equals(L_9, NULL);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_get_DefaultInstance_m2362671815 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_get_DefaultInstance_m2362671815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		MotionEvent_t3121383016 * L_0 = ((MotionEvent_t3121383016_StaticFields*)il2cpp_codegen_static_fields_for(MotionEvent_t3121383016_il2cpp_TypeInfo_var))->get_defaultInstance_0();
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_get_DefaultInstanceForType_m3113173202 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_get_DefaultInstanceForType_m3113173202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		MotionEvent_t3121383016 * L_0 = MotionEvent_get_DefaultInstance_m2362671815(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::get_ThisMessage()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_get_ThisMessage_m3479040991 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool MotionEvent_get_HasTimestamp_m2368979065 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasTimestamp_4();
		return L_0;
	}
}
// System.Int64 proto.PhoneEvent/Types/MotionEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t MotionEvent_get_Timestamp_m1377696390 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_timestamp__5();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent::get_HasAction()
extern "C" IL2CPP_METHOD_ATTR bool MotionEvent_get_HasAction_m1501510323 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasAction_7();
		return L_0;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent::get_Action()
extern "C" IL2CPP_METHOD_ATTR int32_t MotionEvent_get_Action_m3433129119 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_action__8();
		return L_0;
	}
}
// System.Collections.Generic.IList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer> proto.PhoneEvent/Types/MotionEvent::get_PointersList()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MotionEvent_get_PointersList_m2766990463 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	{
		PopsicleList_1_t1233628674 * L_0 = __this->get_pointers__10();
		return L_0;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent::get_PointersCount()
extern "C" IL2CPP_METHOD_ATTR int32_t MotionEvent_get_PointersCount_m837067104 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_get_PointersCount_m837067104_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PopsicleList_1_t1233628674 * L_0 = __this->get_pointers__10();
		NullCheck(L_0);
		int32_t L_1 = PopsicleList_1_get_Count_m700411858(L_0, /*hidden argument*/PopsicleList_1_get_Count_m700411858_RuntimeMethod_var);
		return L_1;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent::GetPointers(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * MotionEvent_GetPointers_m3628348230 (MotionEvent_t3121383016 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_GetPointers_m3628348230_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PopsicleList_1_t1233628674 * L_0 = __this->get_pointers__10();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Pointer_t4145025558 * L_2 = PopsicleList_1_get_Item_m2708808148(L_0, L_1, /*hidden argument*/PopsicleList_1_get_Item_m2708808148_RuntimeMethod_var);
		return L_2;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool MotionEvent_get_IsInitialized_m1437132879 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent::WriteTo(Google.ProtocolBuffers.ICodedOutputStream)
extern "C" IL2CPP_METHOD_ATTR void MotionEvent_WriteTo_m3305395790 (MotionEvent_t3121383016 * __this, RuntimeObject* ___output0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_WriteTo_m3305395790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_0 = ((MotionEvent_t3121383016_StaticFields*)il2cpp_codegen_static_fields_for(MotionEvent_t3121383016_il2cpp_TypeInfo_var))->get__motionEventFieldNames_1();
		V_0 = L_0;
		bool L_1 = __this->get_hasTimestamp_4();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_2 = ___output0;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 2;
		String_t* L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int64_t L_6 = MotionEvent_get_Timestamp_m1377696390(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker3< int32_t, String_t*, int64_t >::Invoke(1 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteInt64(System.Int32,System.String,System.Int64) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_2, 1, L_5, L_6);
	}

IL_0021:
	{
		bool L_7 = __this->get_hasAction_7();
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RuntimeObject* L_8 = ___output0;
		StringU5BU5D_t1281789340* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = 0;
		String_t* L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		int32_t L_12 = MotionEvent_get_Action_m3433129119(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker3< int32_t, String_t*, int32_t >::Invoke(2 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteInt32(System.Int32,System.String,System.Int32) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_8, 2, L_11, L_12);
	}

IL_003c:
	{
		PopsicleList_1_t1233628674 * L_13 = __this->get_pointers__10();
		NullCheck(L_13);
		int32_t L_14 = PopsicleList_1_get_Count_m700411858(L_13, /*hidden argument*/PopsicleList_1_get_Count_m700411858_RuntimeMethod_var);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		RuntimeObject* L_15 = ___output0;
		StringU5BU5D_t1281789340* L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = 1;
		String_t* L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		PopsicleList_1_t1233628674 * L_19 = __this->get_pointers__10();
		NullCheck(L_15);
		GenericInterfaceActionInvoker3< int32_t, String_t*, RuntimeObject* >::Invoke(ICodedOutputStream_WriteMessageArray_TisPointer_t4145025558_m3466852259_RuntimeMethod_var, L_15, 3, L_18, L_19);
	}

IL_005d:
	{
		return;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent::get_SerializedSize()
extern "C" IL2CPP_METHOD_ATTR int32_t MotionEvent_get_SerializedSize_m408294511 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_get_SerializedSize_m408294511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Pointer_t4145025558 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_memoizedSerializedSize_11();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0010:
	{
		V_0 = 0;
		bool L_3 = __this->get_hasTimestamp_4();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = V_0;
		int64_t L_5 = MotionEvent_get_Timestamp_m1377696390(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_6 = CodedOutputStream_ComputeInt64Size_m2929732330(NULL /*static, unused*/, 1, L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_6));
	}

IL_002c:
	{
		bool L_7 = __this->get_hasAction_7();
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_8 = V_0;
		int32_t L_9 = MotionEvent_get_Action_m3433129119(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_10 = CodedOutputStream_ComputeInt32Size_m1726852181(NULL /*static, unused*/, 2, L_9, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)L_10));
	}

IL_0046:
	{
		RuntimeObject* L_11 = MotionEvent_get_PointersList_m2766990463(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		RuntimeObject* L_12 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::GetEnumerator() */, IEnumerable_1_t3124878447_il2cpp_TypeInfo_var, L_11);
		V_2 = L_12;
	}

IL_0052:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0068;
		}

IL_0057:
		{
			RuntimeObject* L_13 = V_2;
			NullCheck(L_13);
			Pointer_t4145025558 * L_14 = InterfaceFuncInvoker0< Pointer_t4145025558 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::get_Current() */, IEnumerator_1_t282628730_il2cpp_TypeInfo_var, L_13);
			V_1 = L_14;
			int32_t L_15 = V_0;
			Pointer_t4145025558 * L_16 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
			int32_t L_17 = CodedOutputStream_ComputeMessageSize_m4104294790(NULL /*static, unused*/, 3, L_16, /*hidden argument*/NULL);
			V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)L_17));
		}

IL_0068:
		{
			RuntimeObject* L_18 = V_2;
			NullCheck(L_18);
			bool L_19 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_18);
			if (L_19)
			{
				goto IL_0057;
			}
		}

IL_0073:
		{
			IL2CPP_LEAVE(0x85, FINALLY_0078);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0078;
	}

FINALLY_0078:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_20 = V_2;
			if (!L_20)
			{
				goto IL_0084;
			}
		}

IL_007e:
		{
			RuntimeObject* L_21 = V_2;
			NullCheck(L_21);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_21);
		}

IL_0084:
		{
			IL2CPP_END_FINALLY(120)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(120)
	{
		IL2CPP_JUMP_TBL(0x85, IL_0085)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0085:
	{
		int32_t L_22 = V_0;
		__this->set_memoizedSerializedSize_11(L_22);
		int32_t L_23 = V_0;
		return L_23;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t MotionEvent_GetHashCode_m1629391598 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_GetHashCode_m1629391598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Pointer_t4145025558 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_hasTimestamp_4();
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = V_0;
		int64_t* L_4 = __this->get_address_of_timestamp__5();
		int32_t L_5 = Int64_GetHashCode_m703091690((int64_t*)L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_3^(int32_t)L_5));
	}

IL_002b:
	{
		bool L_6 = __this->get_hasAction_7();
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t* L_8 = __this->get_address_of_action__8();
		int32_t L_9 = Int32_GetHashCode_m1876651407((int32_t*)L_8, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7^(int32_t)L_9));
	}

IL_004a:
	{
		PopsicleList_1_t1233628674 * L_10 = __this->get_pointers__10();
		NullCheck(L_10);
		RuntimeObject* L_11 = PopsicleList_1_GetEnumerator_m1740650653(L_10, /*hidden argument*/PopsicleList_1_GetEnumerator_m1740650653_RuntimeMethod_var);
		V_2 = L_11;
	}

IL_0056:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006b;
		}

IL_005b:
		{
			RuntimeObject* L_12 = V_2;
			NullCheck(L_12);
			Pointer_t4145025558 * L_13 = InterfaceFuncInvoker0< Pointer_t4145025558 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>::get_Current() */, IEnumerator_1_t282628730_il2cpp_TypeInfo_var, L_12);
			V_1 = L_13;
			int32_t L_14 = V_0;
			Pointer_t4145025558 * L_15 = V_1;
			NullCheck(L_15);
			int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_15);
			V_0 = ((int32_t)((int32_t)L_14^(int32_t)L_16));
		}

IL_006b:
		{
			RuntimeObject* L_17 = V_2;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_005b;
			}
		}

IL_0076:
		{
			IL2CPP_LEAVE(0x88, FINALLY_007b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007b;
	}

FINALLY_007b:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_19 = V_2;
			if (!L_19)
			{
				goto IL_0087;
			}
		}

IL_0081:
		{
			RuntimeObject* L_20 = V_2;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_20);
		}

IL_0087:
		{
			IL2CPP_END_FINALLY(123)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(123)
	{
		IL2CPP_JUMP_TBL(0x88, IL_0088)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0088:
	{
		int32_t L_21 = V_0;
		return L_21;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool MotionEvent_Equals_m4061008759 (MotionEvent_t3121383016 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_Equals_m4061008759_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MotionEvent_t3121383016 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((MotionEvent_t3121383016 *)IsInstSealed((RuntimeObject*)L_0, MotionEvent_t3121383016_il2cpp_TypeInfo_var));
		MotionEvent_t3121383016 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		bool L_2 = __this->get_hasTimestamp_4();
		MotionEvent_t3121383016 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_hasTimestamp_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0041;
		}
	}
	{
		bool L_5 = __this->get_hasTimestamp_4();
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		int64_t* L_6 = __this->get_address_of_timestamp__5();
		MotionEvent_t3121383016 * L_7 = V_0;
		NullCheck(L_7);
		int64_t L_8 = L_7->get_timestamp__5();
		bool L_9 = Int64_Equals_m680137412((int64_t*)L_6, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0043;
		}
	}

IL_0041:
	{
		return (bool)0;
	}

IL_0043:
	{
		bool L_10 = __this->get_hasAction_7();
		MotionEvent_t3121383016 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = L_11->get_hasAction_7();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_12))))
		{
			goto IL_0075;
		}
	}
	{
		bool L_13 = __this->get_hasAction_7();
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		int32_t* L_14 = __this->get_address_of_action__8();
		MotionEvent_t3121383016 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_action__8();
		bool L_17 = Int32_Equals_m2976157357((int32_t*)L_14, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0077;
		}
	}

IL_0075:
	{
		return (bool)0;
	}

IL_0077:
	{
		PopsicleList_1_t1233628674 * L_18 = __this->get_pointers__10();
		NullCheck(L_18);
		int32_t L_19 = PopsicleList_1_get_Count_m700411858(L_18, /*hidden argument*/PopsicleList_1_get_Count_m700411858_RuntimeMethod_var);
		MotionEvent_t3121383016 * L_20 = V_0;
		NullCheck(L_20);
		PopsicleList_1_t1233628674 * L_21 = L_20->get_pointers__10();
		NullCheck(L_21);
		int32_t L_22 = PopsicleList_1_get_Count_m700411858(L_21, /*hidden argument*/PopsicleList_1_get_Count_m700411858_RuntimeMethod_var);
		if ((((int32_t)L_19) == ((int32_t)L_22)))
		{
			goto IL_0094;
		}
	}
	{
		return (bool)0;
	}

IL_0094:
	{
		V_1 = 0;
		goto IL_00c3;
	}

IL_009b:
	{
		PopsicleList_1_t1233628674 * L_23 = __this->get_pointers__10();
		int32_t L_24 = V_1;
		NullCheck(L_23);
		Pointer_t4145025558 * L_25 = PopsicleList_1_get_Item_m2708808148(L_23, L_24, /*hidden argument*/PopsicleList_1_get_Item_m2708808148_RuntimeMethod_var);
		MotionEvent_t3121383016 * L_26 = V_0;
		NullCheck(L_26);
		PopsicleList_1_t1233628674 * L_27 = L_26->get_pointers__10();
		int32_t L_28 = V_1;
		NullCheck(L_27);
		Pointer_t4145025558 * L_29 = PopsicleList_1_get_Item_m2708808148(L_27, L_28, /*hidden argument*/PopsicleList_1_get_Item_m2708808148_RuntimeMethod_var);
		NullCheck(L_25);
		bool L_30 = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_25, L_29);
		if (L_30)
		{
			goto IL_00bf;
		}
	}
	{
		return (bool)0;
	}

IL_00bf:
	{
		int32_t L_31 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_00c3:
	{
		int32_t L_32 = V_1;
		PopsicleList_1_t1233628674 * L_33 = __this->get_pointers__10();
		NullCheck(L_33);
		int32_t L_34 = PopsicleList_1_get_Count_m700411858(L_33, /*hidden argument*/PopsicleList_1_get_Count_m700411858_RuntimeMethod_var);
		if ((((int32_t)L_32) < ((int32_t)L_34)))
		{
			goto IL_009b;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent::PrintTo(System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void MotionEvent_PrintTo_m1843844315 (MotionEvent_t3121383016 * __this, TextWriter_t3478189236 * ___writer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_PrintTo_m1843844315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_hasTimestamp_4();
		int64_t L_1 = __this->get_timestamp__5();
		int64_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int64_t3736567304_il2cpp_TypeInfo_var, &L_2);
		TextWriter_t3478189236 * L_4 = ___writer0;
		GeneratedMessageLite_2_PrintField_m4052720336(NULL /*static, unused*/, _stringLiteral3166771228, L_0, L_3, L_4, /*hidden argument*/GeneratedMessageLite_2_PrintField_m4052720336_RuntimeMethod_var);
		bool L_5 = __this->get_hasAction_7();
		int32_t L_6 = __this->get_action__8();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		TextWriter_t3478189236 * L_9 = ___writer0;
		GeneratedMessageLite_2_PrintField_m4052720336(NULL /*static, unused*/, _stringLiteral2365897554, L_5, L_8, L_9, /*hidden argument*/GeneratedMessageLite_2_PrintField_m4052720336_RuntimeMethod_var);
		PopsicleList_1_t1233628674 * L_10 = __this->get_pointers__10();
		TextWriter_t3478189236 * L_11 = ___writer0;
		GeneratedMessageLite_2_PrintField_TisPointer_t4145025558_m3080746959(NULL /*static, unused*/, _stringLiteral4020252391, L_10, L_11, /*hidden argument*/GeneratedMessageLite_2_PrintField_TisPointer_t4145025558_m3080746959_RuntimeMethod_var);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseFrom(Google.ProtocolBuffers.ByteString)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseFrom_m4122922873 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseFrom_m4122922873_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		NullCheck(L_0);
		Builder_t2961114394 * L_2 = AbstractBuilderLite_2_MergeFrom_m3032596744(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3032596744_RuntimeMethod_var);
		NullCheck(L_2);
		MotionEvent_t3121383016 * L_3 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseFrom_m1089706159 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseFrom_m1089706159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2961114394 * L_3 = AbstractBuilderLite_2_MergeFrom_m1962044158(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m1962044158_RuntimeMethod_var);
		NullCheck(L_3);
		MotionEvent_t3121383016 * L_4 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseFrom(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseFrom_m922347793 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseFrom_m922347793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		NullCheck(L_0);
		Builder_t2961114394 * L_2 = AbstractBuilderLite_2_MergeFrom_m2082277826(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m2082277826_RuntimeMethod_var);
		NullCheck(L_2);
		MotionEvent_t3121383016 * L_3 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseFrom_m3594203963 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseFrom_m3594203963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2961114394 * L_3 = AbstractBuilderLite_2_MergeFrom_m3622339991(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3622339991_RuntimeMethod_var);
		NullCheck(L_3);
		MotionEvent_t3121383016 * L_4 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseFrom_m3050040186 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseFrom_m3050040186_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2961114394 * L_2 = AbstractBuilderLite_2_MergeFrom_m3419181726(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3419181726_RuntimeMethod_var);
		NullCheck(L_2);
		MotionEvent_t3121383016 * L_3 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseFrom_m2055058782 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseFrom_m2055058782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2961114394 * L_3 = AbstractBuilderLite_2_MergeFrom_m1124215902(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m1124215902_RuntimeMethod_var);
		NullCheck(L_3);
		MotionEvent_t3121383016 * L_4 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseDelimitedFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseDelimitedFrom_m2503259629 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseDelimitedFrom_m2503259629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2961114394 * L_2 = AbstractBuilderLite_2_MergeDelimitedFrom_m715217478(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m715217478_RuntimeMethod_var);
		NullCheck(L_2);
		MotionEvent_t3121383016 * L_3 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseDelimitedFrom_m3000926622 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseDelimitedFrom_m3000926622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2961114394 * L_3 = AbstractBuilderLite_2_MergeDelimitedFrom_m2857252119(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m2857252119_RuntimeMethod_var);
		NullCheck(L_3);
		MotionEvent_t3121383016 * L_4 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseFrom_m2186337349 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseFrom_m2186337349_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2961114394 * L_2 = VirtFuncInvoker1< Builder_t2961114394 *, RuntimeObject* >::Invoke(16 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream) */, L_0, L_1);
		NullCheck(L_2);
		MotionEvent_t3121383016 * L_3 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_ParseFrom_m871094949 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ParseFrom_m871094949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m1575032901(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2961114394 * L_3 = VirtFuncInvoker2< Builder_t2961114394 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, L_0, L_1, L_2);
		NullCheck(L_3);
		MotionEvent_t3121383016 * L_4 = GeneratedBuilderLite_2_BuildParsed_m1996805703(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m1996805703_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * MotionEvent_MakeReadOnly_m1076849093 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_MakeReadOnly_m1076849093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PopsicleList_1_t1233628674 * L_0 = __this->get_pointers__10();
		NullCheck(L_0);
		PopsicleList_1_MakeReadOnly_m1389303794(L_0, /*hidden argument*/PopsicleList_1_MakeReadOnly_m1389303794_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * MotionEvent_CreateBuilder_m1575032901 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_CreateBuilder_m1575032901_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t2961114394 * L_0 = (Builder_t2961114394 *)il2cpp_codegen_object_new(Builder_t2961114394_il2cpp_TypeInfo_var);
		Builder__ctor_m2482629630(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent::ToBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * MotionEvent_ToBuilder_m1316974738 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_ToBuilder_m1316974738_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		Builder_t2961114394 * L_0 = MotionEvent_CreateBuilder_m586676648(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent::CreateBuilderForType()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * MotionEvent_CreateBuilderForType_m512433045 (MotionEvent_t3121383016 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_CreateBuilderForType_m512433045_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t2961114394 * L_0 = (Builder_t2961114394 *)il2cpp_codegen_object_new(Builder_t2961114394_il2cpp_TypeInfo_var);
		Builder__ctor_m2482629630(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent::CreateBuilder(proto.PhoneEvent/Types/MotionEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * MotionEvent_CreateBuilder_m586676648 (RuntimeObject * __this /* static, unused */, MotionEvent_t3121383016 * ___prototype0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MotionEvent_CreateBuilder_m586676648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MotionEvent_t3121383016 * L_0 = ___prototype0;
		Builder_t2961114394 * L_1 = (Builder_t2961114394 *)il2cpp_codegen_object_new(Builder_t2961114394_il2cpp_TypeInfo_var);
		Builder__ctor_m3588482064(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/MotionEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m2482629630 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m2482629630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m2782592139(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m2782592139_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		MotionEvent_t3121383016 * L_0 = MotionEvent_get_DefaultInstance_m2362671815(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Builder::.ctor(proto.PhoneEvent/Types/MotionEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3588482064 (Builder_t2961114394 * __this, MotionEvent_t3121383016 * ___cloneFrom0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m3588482064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m2782592139(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m2782592139_RuntimeMethod_var);
		MotionEvent_t3121383016 * L_0 = ___cloneFrom0;
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::get_ThisBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_get_ThisBuilder_m1536440573 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * Builder_PrepareBuilder_m935363363 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_PrepareBuilder_m935363363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MotionEvent_t3121383016 * V_0 = NULL;
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		V_0 = L_1;
		MotionEvent_t3121383016 * L_2 = (MotionEvent_t3121383016 *)il2cpp_codegen_object_new(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		MotionEvent__ctor_m1502834634(L_2, /*hidden argument*/NULL);
		__this->set_result_1(L_2);
		__this->set_resultIsReadOnly_0((bool)0);
		MotionEvent_t3121383016 * L_3 = V_0;
		VirtFuncInvoker1< Builder_t2961114394 *, MotionEvent_t3121383016 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(!0) */, __this, L_3);
	}

IL_002c:
	{
		MotionEvent_t3121383016 * L_4 = __this->get_result_1();
		return L_4;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Builder::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_IsInitialized_m275143040 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::get_IsInitialized() */, L_0);
		return L_1;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent/Builder::get_MessageBeingBuilt()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * Builder_get_MessageBeingBuilt_m1148895722 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::Clear()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_Clear_m1204017742 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clear_m1204017742_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		MotionEvent_t3121383016 * L_0 = MotionEvent_get_DefaultInstance_m2362671815(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::Clone()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_Clone_m803526565 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clone_m803526565_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		Builder_t2961114394 * L_2 = (Builder_t2961114394 *)il2cpp_codegen_object_new(Builder_t2961114394_il2cpp_TypeInfo_var);
		Builder__ctor_m3588482064(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		Builder_t2961114394 * L_3 = (Builder_t2961114394 *)il2cpp_codegen_object_new(Builder_t2961114394_il2cpp_TypeInfo_var);
		Builder__ctor_m2482629630(L_3, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_4 = __this->get_result_1();
		NullCheck(L_3);
		Builder_t2961114394 * L_5 = VirtFuncInvoker1< Builder_t2961114394 *, MotionEvent_t3121383016 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(!0) */, L_3, L_4);
		return L_5;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent/Builder::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * Builder_get_DefaultInstanceForType_m492229874 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_get_DefaultInstanceForType_m492229874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		MotionEvent_t3121383016 * L_0 = MotionEvent_get_DefaultInstance_m2362671815(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent proto.PhoneEvent/Types/MotionEvent/Builder::BuildPartial()
extern "C" IL2CPP_METHOD_ATTR MotionEvent_t3121383016 * Builder_BuildPartial_m2887831497 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		return L_1;
	}

IL_0012:
	{
		__this->set_resultIsReadOnly_0((bool)1);
		MotionEvent_t3121383016 * L_2 = __this->get_result_1();
		NullCheck(L_2);
		MotionEvent_t3121383016 * L_3 = MotionEvent_MakeReadOnly_m1076849093(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_MergeFrom_m33684292 (Builder_t2961114394 * __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m33684292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___other0;
		if (!((MotionEvent_t3121383016 *)IsInstSealed((RuntimeObject*)L_0, MotionEvent_t3121383016_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject* L_1 = ___other0;
		Builder_t2961114394 * L_2 = VirtFuncInvoker1< Builder_t2961114394 *, MotionEvent_t3121383016 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(!0) */, __this, ((MotionEvent_t3121383016 *)CastclassSealed((RuntimeObject*)L_1, MotionEvent_t3121383016_il2cpp_TypeInfo_var)));
		return L_2;
	}

IL_0018:
	{
		RuntimeObject* L_3 = ___other0;
		GeneratedBuilderLite_2_MergeFrom_m610274514(__this, L_3, /*hidden argument*/GeneratedBuilderLite_2_MergeFrom_m610274514_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::MergeFrom(proto.PhoneEvent/Types/MotionEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_MergeFrom_m3351516325 (Builder_t2961114394 * __this, MotionEvent_t3121383016 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m3351516325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MotionEvent_t3121383016 * L_0 = ___other0;
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		MotionEvent_t3121383016 * L_1 = MotionEvent_get_DefaultInstance_m2362671815(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(MotionEvent_t3121383016 *)L_0) == ((RuntimeObject*)(MotionEvent_t3121383016 *)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = MotionEvent_get_HasTimestamp_m2368979065(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		MotionEvent_t3121383016 * L_4 = ___other0;
		NullCheck(L_4);
		int64_t L_5 = MotionEvent_get_Timestamp_m1377696390(L_4, /*hidden argument*/NULL);
		Builder_set_Timestamp_m758952812(__this, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		MotionEvent_t3121383016 * L_6 = ___other0;
		NullCheck(L_6);
		bool L_7 = MotionEvent_get_HasAction_m1501510323(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		MotionEvent_t3121383016 * L_8 = ___other0;
		NullCheck(L_8);
		int32_t L_9 = MotionEvent_get_Action_m3433129119(L_8, /*hidden argument*/NULL);
		Builder_set_Action_m18528823(__this, L_9, /*hidden argument*/NULL);
	}

IL_0042:
	{
		MotionEvent_t3121383016 * L_10 = ___other0;
		NullCheck(L_10);
		PopsicleList_1_t1233628674 * L_11 = L_10->get_pointers__10();
		NullCheck(L_11);
		int32_t L_12 = PopsicleList_1_get_Count_m700411858(L_11, /*hidden argument*/PopsicleList_1_get_Count_m700411858_RuntimeMethod_var);
		if (!L_12)
		{
			goto IL_0068;
		}
	}
	{
		MotionEvent_t3121383016 * L_13 = __this->get_result_1();
		NullCheck(L_13);
		PopsicleList_1_t1233628674 * L_14 = L_13->get_pointers__10();
		MotionEvent_t3121383016 * L_15 = ___other0;
		NullCheck(L_15);
		PopsicleList_1_t1233628674 * L_16 = L_15->get_pointers__10();
		NullCheck(L_14);
		PopsicleList_1_Add_m3166945360(L_14, L_16, /*hidden argument*/PopsicleList_1_Add_m3166945360_RuntimeMethod_var);
	}

IL_0068:
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_MergeFrom_m1801103483 (Builder_t2961114394 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m1801103483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t4271428238_il2cpp_TypeInfo_var);
		ExtensionRegistry_t4271428238 * L_1 = ExtensionRegistry_get_Empty_m3666683255(NULL /*static, unused*/, /*hidden argument*/NULL);
		Builder_t2961114394 * L_2 = VirtFuncInvoker2< Builder_t2961114394 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, __this, L_0, L_1);
		return L_2;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_MergeFrom_m2307689438 (Builder_t2961114394 * __this, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m2307689438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_000c:
	{
		uint32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_2 = ((MotionEvent_t3121383016_StaticFields*)il2cpp_codegen_static_fields_for(MotionEvent_t3121383016_il2cpp_TypeInfo_var))->get__motionEventFieldNames_1();
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t3301955079_il2cpp_TypeInfo_var);
		StringComparer_t3301955079 * L_4 = StringComparer_get_Ordinal_m2103862281(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Array_BinarySearch_TisString_t_m2519237149(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/Array_BinarySearch_TisString_t_m2519237149_RuntimeMethod_var);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MotionEvent_t3121383016_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_7 = ((MotionEvent_t3121383016_StaticFields*)il2cpp_codegen_static_fields_for(MotionEvent_t3121383016_il2cpp_TypeInfo_var))->get__motionEventFieldTags_2();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		goto IL_004d;
	}

IL_003d:
	{
		RuntimeObject* L_11 = ___input0;
		ExtensionRegistry_t4271428238 * L_12 = ___extensionRegistry1;
		uint32_t L_13 = V_0;
		String_t* L_14 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_11, L_12, L_13, L_14);
		goto IL_00f2;
	}

IL_004d:
	{
		uint32_t L_15 = V_0;
		if (!L_15)
		{
			goto IL_006f;
		}
	}
	{
		uint32_t L_16 = V_0;
		if ((((int32_t)L_16) == ((int32_t)8)))
		{
			goto IL_0092;
		}
	}
	{
		uint32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)16))))
		{
			goto IL_00b3;
		}
	}
	{
		uint32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)26))))
		{
			goto IL_00d4;
		}
	}
	{
		goto IL_0075;
	}

IL_006f:
	{
		InvalidProtocolBufferException_t2498581859 * L_19 = InvalidProtocolBufferException_InvalidTag_m4139780452(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, NULL, Builder_MergeFrom_m2307689438_RuntimeMethod_var);
	}

IL_0075:
	{
		uint32_t L_20 = V_0;
		bool L_21 = WireFormat_IsEndGroupTag_m2504732292(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0082;
		}
	}
	{
		return __this;
	}

IL_0082:
	{
		RuntimeObject* L_22 = ___input0;
		ExtensionRegistry_t4271428238 * L_23 = ___extensionRegistry1;
		uint32_t L_24 = V_0;
		String_t* L_25 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent,proto.PhoneEvent/Types/MotionEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_22, L_23, L_24, L_25);
		goto IL_00f2;
	}

IL_0092:
	{
		MotionEvent_t3121383016 * L_26 = __this->get_result_1();
		RuntimeObject* L_27 = ___input0;
		MotionEvent_t3121383016 * L_28 = __this->get_result_1();
		NullCheck(L_28);
		int64_t* L_29 = L_28->get_address_of_timestamp__5();
		NullCheck(L_27);
		bool L_30 = InterfaceFuncInvoker1< bool, int64_t* >::Invoke(2 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt64(System.Int64&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_27, (int64_t*)L_29);
		NullCheck(L_26);
		L_26->set_hasTimestamp_4(L_30);
		goto IL_00f2;
	}

IL_00b3:
	{
		MotionEvent_t3121383016 * L_31 = __this->get_result_1();
		RuntimeObject* L_32 = ___input0;
		MotionEvent_t3121383016 * L_33 = __this->get_result_1();
		NullCheck(L_33);
		int32_t* L_34 = L_33->get_address_of_action__8();
		NullCheck(L_32);
		bool L_35 = InterfaceFuncInvoker1< bool, int32_t* >::Invoke(3 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt32(System.Int32&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_32, (int32_t*)L_34);
		NullCheck(L_31);
		L_31->set_hasAction_7(L_35);
		goto IL_00f2;
	}

IL_00d4:
	{
		RuntimeObject* L_36 = ___input0;
		uint32_t L_37 = V_0;
		String_t* L_38 = V_1;
		MotionEvent_t3121383016 * L_39 = __this->get_result_1();
		NullCheck(L_39);
		PopsicleList_1_t1233628674 * L_40 = L_39->get_pointers__10();
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer_t4145025558 * L_41 = Pointer_get_DefaultInstance_m846755284(NULL /*static, unused*/, /*hidden argument*/NULL);
		ExtensionRegistry_t4271428238 * L_42 = ___extensionRegistry1;
		NullCheck(L_36);
		GenericInterfaceActionInvoker5< uint32_t, String_t*, RuntimeObject*, Pointer_t4145025558 *, ExtensionRegistry_t4271428238 * >::Invoke(ICodedInputStream_ReadMessageArray_TisPointer_t4145025558_m2307173454_RuntimeMethod_var, L_36, L_37, L_38, L_40, L_41, L_42);
		goto IL_00f2;
	}

IL_00f2:
	{
		RuntimeObject* L_43 = ___input0;
		NullCheck(L_43);
		bool L_44 = InterfaceFuncInvoker2< bool, uint32_t*, String_t** >::Invoke(0 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadTag(System.UInt32&,System.String&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_43, (uint32_t*)(&V_0), (String_t**)(&V_1));
		if (L_44)
		{
			goto IL_000c;
		}
	}
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Builder::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasTimestamp_m3368280046 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasTimestamp_4();
		return L_1;
	}
}
// System.Int64 proto.PhoneEvent/Types/MotionEvent/Builder::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t Builder_get_Timestamp_m1348610021 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int64_t L_1 = MotionEvent_get_Timestamp_m1377696390(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m758952812 (Builder_t2961114394 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		int64_t L_0 = ___value0;
		Builder_SetTimestamp_m757989845(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_SetTimestamp_m757989845 (Builder_t2961114394 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)1);
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		int64_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_timestamp__5(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::ClearTimestamp()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_ClearTimestamp_m670595533 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)0);
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_timestamp__5((((int64_t)((int64_t)0))));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Builder::get_HasAction()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasAction_m2522483744 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasAction_7();
		return L_1;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent/Builder::get_Action()
extern "C" IL2CPP_METHOD_ATTR int32_t Builder_get_Action_m2620882863 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int32_t L_1 = MotionEvent_get_Action_m3433129119(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Builder::set_Action(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Action_m18528823 (Builder_t2961114394 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		Builder_SetAction_m2495301148(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::SetAction(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_SetAction_m2495301148 (Builder_t2961114394 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasAction_7((bool)1);
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		int32_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_action__8(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::ClearAction()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_ClearAction_m2678949719 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasAction_7((bool)0);
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_action__8(0);
		return __this;
	}
}
// Google.ProtocolBuffers.Collections.IPopsicleList`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer> proto.PhoneEvent/Types/MotionEvent/Builder::get_PointersList()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Builder_get_PointersList_m951845666 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		PopsicleList_1_t1233628674 * L_1 = L_0->get_pointers__10();
		return L_1;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent/Builder::get_PointersCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Builder_get_PointersCount_m1529856757 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int32_t L_1 = MotionEvent_get_PointersCount_m837067104(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Builder::GetPointers(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Builder_GetPointers_m2949887570 (Builder_t2961114394 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		Pointer_t4145025558 * L_2 = MotionEvent_GetPointers_m3628348230(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::SetPointers(System.Int32,proto.PhoneEvent/Types/MotionEvent/Types/Pointer)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_SetPointers_m247642983 (Builder_t2961114394 * __this, int32_t ___index0, Pointer_t4145025558 * ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_SetPointers_m247642983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Pointer_t4145025558 * L_0 = ___value1;
		ThrowHelper_ThrowIfNull_m4026022381(NULL /*static, unused*/, L_0, _stringLiteral3493618073, /*hidden argument*/NULL);
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		PopsicleList_1_t1233628674 * L_2 = L_1->get_pointers__10();
		int32_t L_3 = ___index0;
		Pointer_t4145025558 * L_4 = ___value1;
		NullCheck(L_2);
		PopsicleList_1_set_Item_m2187003455(L_2, L_3, L_4, /*hidden argument*/PopsicleList_1_set_Item_m2187003455_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::SetPointers(System.Int32,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_SetPointers_m3282197334 (Builder_t2961114394 * __this, int32_t ___index0, Builder_t582111845 * ___builderForValue1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_SetPointers_m3282197334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t582111845 * L_0 = ___builderForValue1;
		ThrowHelper_ThrowIfNull_m4026022381(NULL /*static, unused*/, L_0, _stringLiteral4026478820, /*hidden argument*/NULL);
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		PopsicleList_1_t1233628674 * L_2 = L_1->get_pointers__10();
		int32_t L_3 = ___index0;
		Builder_t582111845 * L_4 = ___builderForValue1;
		NullCheck(L_4);
		Pointer_t4145025558 * L_5 = VirtFuncInvoker0< Pointer_t4145025558 * >::Invoke(12 /* !0 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::Build() */, L_4);
		NullCheck(L_2);
		PopsicleList_1_set_Item_m2187003455(L_2, L_3, L_5, /*hidden argument*/PopsicleList_1_set_Item_m2187003455_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::AddPointers(proto.PhoneEvent/Types/MotionEvent/Types/Pointer)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_AddPointers_m1746726519 (Builder_t2961114394 * __this, Pointer_t4145025558 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_AddPointers_m1746726519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Pointer_t4145025558 * L_0 = ___value0;
		ThrowHelper_ThrowIfNull_m4026022381(NULL /*static, unused*/, L_0, _stringLiteral3493618073, /*hidden argument*/NULL);
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		PopsicleList_1_t1233628674 * L_2 = L_1->get_pointers__10();
		Pointer_t4145025558 * L_3 = ___value0;
		NullCheck(L_2);
		PopsicleList_1_Add_m2124609127(L_2, L_3, /*hidden argument*/PopsicleList_1_Add_m2124609127_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::AddPointers(proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_AddPointers_m871810046 (Builder_t2961114394 * __this, Builder_t582111845 * ___builderForValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_AddPointers_m871810046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t582111845 * L_0 = ___builderForValue0;
		ThrowHelper_ThrowIfNull_m4026022381(NULL /*static, unused*/, L_0, _stringLiteral4026478820, /*hidden argument*/NULL);
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		PopsicleList_1_t1233628674 * L_2 = L_1->get_pointers__10();
		Builder_t582111845 * L_3 = ___builderForValue0;
		NullCheck(L_3);
		Pointer_t4145025558 * L_4 = VirtFuncInvoker0< Pointer_t4145025558 * >::Invoke(12 /* !0 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::Build() */, L_3);
		NullCheck(L_2);
		PopsicleList_1_Add_m2124609127(L_2, L_4, /*hidden argument*/PopsicleList_1_Add_m2124609127_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::AddRangePointers(System.Collections.Generic.IEnumerable`1<proto.PhoneEvent/Types/MotionEvent/Types/Pointer>)
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_AddRangePointers_m2760467942 (Builder_t2961114394 * __this, RuntimeObject* ___values0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_AddRangePointers_m2760467942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		PopsicleList_1_t1233628674 * L_1 = L_0->get_pointers__10();
		RuntimeObject* L_2 = ___values0;
		NullCheck(L_1);
		PopsicleList_1_Add_m3166945360(L_1, L_2, /*hidden argument*/PopsicleList_1_Add_m3166945360_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Builder proto.PhoneEvent/Types/MotionEvent/Builder::ClearPointers()
extern "C" IL2CPP_METHOD_ATTR Builder_t2961114394 * Builder_ClearPointers_m113524416 (Builder_t2961114394 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_ClearPointers_m113524416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_PrepareBuilder_m935363363(__this, /*hidden argument*/NULL);
		MotionEvent_t3121383016 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		PopsicleList_1_t1233628674 * L_1 = L_0->get_pointers__10();
		NullCheck(L_1);
		PopsicleList_1_Clear_m485968702(L_1, /*hidden argument*/PopsicleList_1_Clear_m485968702_RuntimeMethod_var);
		return __this;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Pointer__ctor_m597435337 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer__ctor_m597435337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_memoizedSerializedSize_12((-1));
		GeneratedMessageLite_2__ctor_m3200145457(__this, /*hidden argument*/GeneratedMessageLite_2__ctor_m3200145457_RuntimeMethod_var);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer::.cctor()
extern "C" IL2CPP_METHOD_ATTR void Pointer__cctor_m2799481490 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer__cctor_m2799481490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Pointer_t4145025558 * L_0 = (Pointer_t4145025558 *)il2cpp_codegen_object_new(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer__ctor_m597435337(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		Pointer_t4145025558 * L_1 = Pointer_MakeReadOnly_m3313729630(L_0, /*hidden argument*/NULL);
		((Pointer_t4145025558_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_t4145025558_il2cpp_TypeInfo_var))->set_defaultInstance_0(L_1);
		StringU5BU5D_t1281789340* L_2 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)3);
		StringU5BU5D_t1281789340* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral3454449607);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3454449607);
		StringU5BU5D_t1281789340* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3130993982);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3130993982);
		StringU5BU5D_t1281789340* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral792341822);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral792341822);
		((Pointer_t4145025558_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_t4145025558_il2cpp_TypeInfo_var))->set__pointerFieldNames_1(L_5);
		UInt32U5BU5D_t2770800703* L_6 = (UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)3);
		UInt32U5BU5D_t2770800703* L_7 = L_6;
		RuntimeFieldHandle_t1871169219  L_8 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2D311441405B64B3EA9097AC8E07F3274962EC6BB4_0_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_7, L_8, /*hidden argument*/NULL);
		((Pointer_t4145025558_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_t4145025558_il2cpp_TypeInfo_var))->set__pointerFieldTags_2(L_7);
		IL2CPP_RUNTIME_CLASS_INIT(PhoneEvent_t3448566392_il2cpp_TypeInfo_var);
		RuntimeObject * L_9 = ((PhoneEvent_t3448566392_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t3448566392_il2cpp_TypeInfo_var))->get_Descriptor_0();
		il2cpp_codegen_object_reference_equals(L_9, NULL);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_get_DefaultInstance_m846755284 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_get_DefaultInstance_m846755284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer_t4145025558 * L_0 = ((Pointer_t4145025558_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_t4145025558_il2cpp_TypeInfo_var))->get_defaultInstance_0();
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_get_DefaultInstanceForType_m122910909 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_get_DefaultInstanceForType_m122910909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer_t4145025558 * L_0 = Pointer_get_DefaultInstance_m846755284(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_ThisMessage()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_get_ThisMessage_m1460640582 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_HasId()
extern "C" IL2CPP_METHOD_ATTR bool Pointer_get_HasId_m4071191836 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasId_4();
		return L_0;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_Id()
extern "C" IL2CPP_METHOD_ATTR int32_t Pointer_get_Id_m2656726378 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_id__5();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_HasNormalizedX()
extern "C" IL2CPP_METHOD_ATTR bool Pointer_get_HasNormalizedX_m2691093520 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasNormalizedX_7();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_NormalizedX()
extern "C" IL2CPP_METHOD_ATTR float Pointer_get_NormalizedX_m306370287 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_normalizedX__8();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_HasNormalizedY()
extern "C" IL2CPP_METHOD_ATTR bool Pointer_get_HasNormalizedY_m1125009579 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasNormalizedY_10();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_NormalizedY()
extern "C" IL2CPP_METHOD_ATTR float Pointer_get_NormalizedY_m3035253642 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_normalizedY__11();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool Pointer_get_IsInitialized_m1111660425 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer::WriteTo(Google.ProtocolBuffers.ICodedOutputStream)
extern "C" IL2CPP_METHOD_ATTR void Pointer_WriteTo_m3630027018 (Pointer_t4145025558 * __this, RuntimeObject* ___output0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_WriteTo_m3630027018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_0 = ((Pointer_t4145025558_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_t4145025558_il2cpp_TypeInfo_var))->get__pointerFieldNames_1();
		V_0 = L_0;
		bool L_1 = __this->get_hasId_4();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_2 = ___output0;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		String_t* L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int32_t L_6 = Pointer_get_Id_m2656726378(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker3< int32_t, String_t*, int32_t >::Invoke(2 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteInt32(System.Int32,System.String,System.Int32) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_2, 1, L_5, L_6);
	}

IL_0021:
	{
		bool L_7 = __this->get_hasNormalizedX_7();
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RuntimeObject* L_8 = ___output0;
		StringU5BU5D_t1281789340* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = 1;
		String_t* L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		float L_12 = Pointer_get_NormalizedX_m306370287(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_8, 2, L_11, L_12);
	}

IL_003c:
	{
		bool L_13 = __this->get_hasNormalizedY_10();
		if (!L_13)
		{
			goto IL_0057;
		}
	}
	{
		RuntimeObject* L_14 = ___output0;
		StringU5BU5D_t1281789340* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = 2;
		String_t* L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		float L_18 = Pointer_get_NormalizedY_m3035253642(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_14, 3, L_17, L_18);
	}

IL_0057:
	{
		return;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::get_SerializedSize()
extern "C" IL2CPP_METHOD_ATTR int32_t Pointer_get_SerializedSize_m3309772427 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_get_SerializedSize_m3309772427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_memoizedSerializedSize_12();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0010:
	{
		V_0 = 0;
		bool L_3 = __this->get_hasId_4();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = Pointer_get_Id_m2656726378(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_6 = CodedOutputStream_ComputeInt32Size_m1726852181(NULL /*static, unused*/, 1, L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_6));
	}

IL_002c:
	{
		bool L_7 = __this->get_hasNormalizedX_7();
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_8 = V_0;
		float L_9 = Pointer_get_NormalizedX_m306370287(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_10 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 2, L_9, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)L_10));
	}

IL_0046:
	{
		bool L_11 = __this->get_hasNormalizedY_10();
		if (!L_11)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_12 = V_0;
		float L_13 = Pointer_get_NormalizedY_m3035253642(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_14 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 3, L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_14));
	}

IL_0060:
	{
		int32_t L_15 = V_0;
		__this->set_memoizedSerializedSize_12(L_15);
		int32_t L_16 = V_0;
		return L_16;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t Pointer_GetHashCode_m2782927662 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Type_t * L_0 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_hasId_4();
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = V_0;
		int32_t* L_4 = __this->get_address_of_id__5();
		int32_t L_5 = Int32_GetHashCode_m1876651407((int32_t*)L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_3^(int32_t)L_5));
	}

IL_002b:
	{
		bool L_6 = __this->get_hasNormalizedX_7();
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_7 = V_0;
		float* L_8 = __this->get_address_of_normalizedX__8();
		int32_t L_9 = Single_GetHashCode_m1558506138((float*)L_8, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7^(int32_t)L_9));
	}

IL_004a:
	{
		bool L_10 = __this->get_hasNormalizedY_10();
		if (!L_10)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_11 = V_0;
		float* L_12 = __this->get_address_of_normalizedY__11();
		int32_t L_13 = Single_GetHashCode_m1558506138((float*)L_12, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_11^(int32_t)L_13));
	}

IL_0069:
	{
		int32_t L_14 = V_0;
		return L_14;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool Pointer_Equals_m946514537 (Pointer_t4145025558 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_Equals_m946514537_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Pointer_t4145025558 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((Pointer_t4145025558 *)IsInstSealed((RuntimeObject*)L_0, Pointer_t4145025558_il2cpp_TypeInfo_var));
		Pointer_t4145025558 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		bool L_2 = __this->get_hasId_4();
		Pointer_t4145025558 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_hasId_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0041;
		}
	}
	{
		bool L_5 = __this->get_hasId_4();
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		int32_t* L_6 = __this->get_address_of_id__5();
		Pointer_t4145025558 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_id__5();
		bool L_9 = Int32_Equals_m2976157357((int32_t*)L_6, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0043;
		}
	}

IL_0041:
	{
		return (bool)0;
	}

IL_0043:
	{
		bool L_10 = __this->get_hasNormalizedX_7();
		Pointer_t4145025558 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = L_11->get_hasNormalizedX_7();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_12))))
		{
			goto IL_0075;
		}
	}
	{
		bool L_13 = __this->get_hasNormalizedX_7();
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		float* L_14 = __this->get_address_of_normalizedX__8();
		Pointer_t4145025558 * L_15 = V_0;
		NullCheck(L_15);
		float L_16 = L_15->get_normalizedX__8();
		bool L_17 = Single_Equals_m1601893879((float*)L_14, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0077;
		}
	}

IL_0075:
	{
		return (bool)0;
	}

IL_0077:
	{
		bool L_18 = __this->get_hasNormalizedY_10();
		Pointer_t4145025558 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = L_19->get_hasNormalizedY_10();
		if ((!(((uint32_t)L_18) == ((uint32_t)L_20))))
		{
			goto IL_00a9;
		}
	}
	{
		bool L_21 = __this->get_hasNormalizedY_10();
		if (!L_21)
		{
			goto IL_00ab;
		}
	}
	{
		float* L_22 = __this->get_address_of_normalizedY__11();
		Pointer_t4145025558 * L_23 = V_0;
		NullCheck(L_23);
		float L_24 = L_23->get_normalizedY__11();
		bool L_25 = Single_Equals_m1601893879((float*)L_22, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00ab;
		}
	}

IL_00a9:
	{
		return (bool)0;
	}

IL_00ab:
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer::PrintTo(System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void Pointer_PrintTo_m1214513662 (Pointer_t4145025558 * __this, TextWriter_t3478189236 * ___writer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_PrintTo_m1214513662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_hasId_4();
		int32_t L_1 = __this->get_id__5();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		TextWriter_t3478189236 * L_4 = ___writer0;
		GeneratedMessageLite_2_PrintField_m3760789329(NULL /*static, unused*/, _stringLiteral3454449607, L_0, L_3, L_4, /*hidden argument*/GeneratedMessageLite_2_PrintField_m3760789329_RuntimeMethod_var);
		bool L_5 = __this->get_hasNormalizedX_7();
		float L_6 = __this->get_normalizedX__8();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_7);
		TextWriter_t3478189236 * L_9 = ___writer0;
		GeneratedMessageLite_2_PrintField_m3760789329(NULL /*static, unused*/, _stringLiteral3130993982, L_5, L_8, L_9, /*hidden argument*/GeneratedMessageLite_2_PrintField_m3760789329_RuntimeMethod_var);
		bool L_10 = __this->get_hasNormalizedY_10();
		float L_11 = __this->get_normalizedY__11();
		float L_12 = L_11;
		RuntimeObject * L_13 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_12);
		TextWriter_t3478189236 * L_14 = ___writer0;
		GeneratedMessageLite_2_PrintField_m3760789329(NULL /*static, unused*/, _stringLiteral792341822, L_10, L_13, L_14, /*hidden argument*/GeneratedMessageLite_2_PrintField_m3760789329_RuntimeMethod_var);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseFrom(Google.ProtocolBuffers.ByteString)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseFrom_m1063778754 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseFrom_m1063778754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		NullCheck(L_0);
		Builder_t582111845 * L_2 = AbstractBuilderLite_2_MergeFrom_m776875282(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m776875282_RuntimeMethod_var);
		NullCheck(L_2);
		Pointer_t4145025558 * L_3 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseFrom_m3706758380 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseFrom_m3706758380_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t582111845 * L_3 = AbstractBuilderLite_2_MergeFrom_m269598545(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m269598545_RuntimeMethod_var);
		NullCheck(L_3);
		Pointer_t4145025558 * L_4 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseFrom(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseFrom_m3041481156 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseFrom_m3041481156_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		NullCheck(L_0);
		Builder_t582111845 * L_2 = AbstractBuilderLite_2_MergeFrom_m14772483(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m14772483_RuntimeMethod_var);
		NullCheck(L_2);
		Pointer_t4145025558 * L_3 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseFrom_m1699324165 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseFrom_m1699324165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t582111845 * L_3 = AbstractBuilderLite_2_MergeFrom_m2021870966(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m2021870966_RuntimeMethod_var);
		NullCheck(L_3);
		Pointer_t4145025558 * L_4 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseFrom_m2469062765 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseFrom_m2469062765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t582111845 * L_2 = AbstractBuilderLite_2_MergeFrom_m541927886(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m541927886_RuntimeMethod_var);
		NullCheck(L_2);
		Pointer_t4145025558 * L_3 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseFrom_m2556922512 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseFrom_m2556922512_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t582111845 * L_3 = AbstractBuilderLite_2_MergeFrom_m870343857(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m870343857_RuntimeMethod_var);
		NullCheck(L_3);
		Pointer_t4145025558 * L_4 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseDelimitedFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseDelimitedFrom_m4228961319 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseDelimitedFrom_m4228961319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t582111845 * L_2 = AbstractBuilderLite_2_MergeDelimitedFrom_m1106981(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m1106981_RuntimeMethod_var);
		NullCheck(L_2);
		Pointer_t4145025558 * L_3 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseDelimitedFrom_m2976578207 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseDelimitedFrom_m2976578207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t582111845 * L_3 = AbstractBuilderLite_2_MergeDelimitedFrom_m1016734311(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m1016734311_RuntimeMethod_var);
		NullCheck(L_3);
		Pointer_t4145025558 * L_4 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseFrom_m3657044794 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseFrom_m3657044794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		NullCheck(L_0);
		Builder_t582111845 * L_2 = VirtFuncInvoker1< Builder_t582111845 *, RuntimeObject* >::Invoke(16 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream) */, L_0, L_1);
		NullCheck(L_2);
		Pointer_t4145025558 * L_3 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ParseFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_ParseFrom_m3839729296 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ParseFrom_m3839729296_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m2774886265(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t582111845 * L_3 = VirtFuncInvoker2< Builder_t582111845 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, L_0, L_1, L_2);
		NullCheck(L_3);
		Pointer_t4145025558 * L_4 = GeneratedBuilderLite_2_BuildParsed_m2187103318(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m2187103318_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Pointer_MakeReadOnly_m3313729630 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Pointer_CreateBuilder_m2774886265 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_CreateBuilder_m2774886265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t582111845 * L_0 = (Builder_t582111845 *)il2cpp_codegen_object_new(Builder_t582111845_il2cpp_TypeInfo_var);
		Builder__ctor_m3210742657(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer::ToBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Pointer_ToBuilder_m3377747820 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_ToBuilder_m3377747820_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Builder_t582111845 * L_0 = Pointer_CreateBuilder_m4178316069(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer::CreateBuilderForType()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Pointer_CreateBuilderForType_m723998425 (Pointer_t4145025558 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_CreateBuilderForType_m723998425_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t582111845 * L_0 = (Builder_t582111845 *)il2cpp_codegen_object_new(Builder_t582111845_il2cpp_TypeInfo_var);
		Builder__ctor_m3210742657(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer::CreateBuilder(proto.PhoneEvent/Types/MotionEvent/Types/Pointer)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Pointer_CreateBuilder_m4178316069 (RuntimeObject * __this /* static, unused */, Pointer_t4145025558 * ___prototype0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pointer_CreateBuilder_m4178316069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Pointer_t4145025558 * L_0 = ___prototype0;
		Builder_t582111845 * L_1 = (Builder_t582111845 *)il2cpp_codegen_object_new(Builder_t582111845_il2cpp_TypeInfo_var);
		Builder__ctor_m3196691357(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3210742657 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m3210742657_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m3477943950(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m3477943950_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer_t4145025558 * L_0 = Pointer_get_DefaultInstance_m846755284(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::.ctor(proto.PhoneEvent/Types/MotionEvent/Types/Pointer)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3196691357 (Builder_t582111845 * __this, Pointer_t4145025558 * ___cloneFrom0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m3196691357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m3477943950(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m3477943950_RuntimeMethod_var);
		Pointer_t4145025558 * L_0 = ___cloneFrom0;
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_ThisBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_get_ThisBuilder_m1552536861 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Builder_PrepareBuilder_m2800481259 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_PrepareBuilder_m2800481259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Pointer_t4145025558 * V_0 = NULL;
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		V_0 = L_1;
		Pointer_t4145025558 * L_2 = (Pointer_t4145025558 *)il2cpp_codegen_object_new(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer__ctor_m597435337(L_2, /*hidden argument*/NULL);
		__this->set_result_1(L_2);
		__this->set_resultIsReadOnly_0((bool)0);
		Pointer_t4145025558 * L_3 = V_0;
		VirtFuncInvoker1< Builder_t582111845 *, Pointer_t4145025558 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(!0) */, __this, L_3);
	}

IL_002c:
	{
		Pointer_t4145025558 * L_4 = __this->get_result_1();
		return L_4;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_IsInitialized_m2747958463 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::get_IsInitialized() */, L_0);
		return L_1;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_MessageBeingBuilt()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Builder_get_MessageBeingBuilt_m986189605 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Pointer_t4145025558 * L_0 = Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::Clear()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_Clear_m3117102428 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clear_m3117102428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer_t4145025558 * L_0 = Pointer_get_DefaultInstance_m846755284(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::Clone()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_Clone_m306484013 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clone_m306484013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		Builder_t582111845 * L_2 = (Builder_t582111845 *)il2cpp_codegen_object_new(Builder_t582111845_il2cpp_TypeInfo_var);
		Builder__ctor_m3196691357(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		Builder_t582111845 * L_3 = (Builder_t582111845 *)il2cpp_codegen_object_new(Builder_t582111845_il2cpp_TypeInfo_var);
		Builder__ctor_m3210742657(L_3, /*hidden argument*/NULL);
		Pointer_t4145025558 * L_4 = __this->get_result_1();
		NullCheck(L_3);
		Builder_t582111845 * L_5 = VirtFuncInvoker1< Builder_t582111845 *, Pointer_t4145025558 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(!0) */, L_3, L_4);
		return L_5;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Builder_get_DefaultInstanceForType_m643611210 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_get_DefaultInstanceForType_m643611210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer_t4145025558 * L_0 = Pointer_get_DefaultInstance_m846755284(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::BuildPartial()
extern "C" IL2CPP_METHOD_ATTR Pointer_t4145025558 * Builder_BuildPartial_m3770279251 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		return L_1;
	}

IL_0012:
	{
		__this->set_resultIsReadOnly_0((bool)1);
		Pointer_t4145025558 * L_2 = __this->get_result_1();
		NullCheck(L_2);
		Pointer_t4145025558 * L_3 = Pointer_MakeReadOnly_m3313729630(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_MergeFrom_m2566919961 (Builder_t582111845 * __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m2566919961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___other0;
		if (!((Pointer_t4145025558 *)IsInstSealed((RuntimeObject*)L_0, Pointer_t4145025558_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject* L_1 = ___other0;
		Builder_t582111845 * L_2 = VirtFuncInvoker1< Builder_t582111845 *, Pointer_t4145025558 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(!0) */, __this, ((Pointer_t4145025558 *)CastclassSealed((RuntimeObject*)L_1, Pointer_t4145025558_il2cpp_TypeInfo_var)));
		return L_2;
	}

IL_0018:
	{
		RuntimeObject* L_3 = ___other0;
		GeneratedBuilderLite_2_MergeFrom_m1218719396(__this, L_3, /*hidden argument*/GeneratedBuilderLite_2_MergeFrom_m1218719396_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::MergeFrom(proto.PhoneEvent/Types/MotionEvent/Types/Pointer)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_MergeFrom_m3196150861 (Builder_t582111845 * __this, Pointer_t4145025558 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m3196150861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Pointer_t4145025558 * L_0 = ___other0;
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		Pointer_t4145025558 * L_1 = Pointer_get_DefaultInstance_m846755284(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(Pointer_t4145025558 *)L_0) == ((RuntimeObject*)(Pointer_t4145025558 *)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		Pointer_t4145025558 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = Pointer_get_HasId_m4071191836(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		Pointer_t4145025558 * L_4 = ___other0;
		NullCheck(L_4);
		int32_t L_5 = Pointer_get_Id_m2656726378(L_4, /*hidden argument*/NULL);
		Builder_set_Id_m1971136214(__this, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		Pointer_t4145025558 * L_6 = ___other0;
		NullCheck(L_6);
		bool L_7 = Pointer_get_HasNormalizedX_m2691093520(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		Pointer_t4145025558 * L_8 = ___other0;
		NullCheck(L_8);
		float L_9 = Pointer_get_NormalizedX_m306370287(L_8, /*hidden argument*/NULL);
		Builder_set_NormalizedX_m3801472179(__this, L_9, /*hidden argument*/NULL);
	}

IL_0042:
	{
		Pointer_t4145025558 * L_10 = ___other0;
		NullCheck(L_10);
		bool L_11 = Pointer_get_HasNormalizedY_m1125009579(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0059;
		}
	}
	{
		Pointer_t4145025558 * L_12 = ___other0;
		NullCheck(L_12);
		float L_13 = Pointer_get_NormalizedY_m3035253642(L_12, /*hidden argument*/NULL);
		Builder_set_NormalizedY_m3801436436(__this, L_13, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_MergeFrom_m3465110836 (Builder_t582111845 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m3465110836_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t4271428238_il2cpp_TypeInfo_var);
		ExtensionRegistry_t4271428238 * L_1 = ExtensionRegistry_get_Empty_m3666683255(NULL /*static, unused*/, /*hidden argument*/NULL);
		Builder_t582111845 * L_2 = VirtFuncInvoker2< Builder_t582111845 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, __this, L_0, L_1);
		return L_2;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_MergeFrom_m3447828463 (Builder_t582111845 * __this, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m3447828463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		goto IL_00f5;
	}

IL_000c:
	{
		uint32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_2 = ((Pointer_t4145025558_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_t4145025558_il2cpp_TypeInfo_var))->get__pointerFieldNames_1();
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t3301955079_il2cpp_TypeInfo_var);
		StringComparer_t3301955079 * L_4 = StringComparer_get_Ordinal_m2103862281(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Array_BinarySearch_TisString_t_m2519237149(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/Array_BinarySearch_TisString_t_m2519237149_RuntimeMethod_var);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Pointer_t4145025558_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_7 = ((Pointer_t4145025558_StaticFields*)il2cpp_codegen_static_fields_for(Pointer_t4145025558_il2cpp_TypeInfo_var))->get__pointerFieldTags_2();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		goto IL_004d;
	}

IL_003d:
	{
		RuntimeObject* L_11 = ___input0;
		ExtensionRegistry_t4271428238 * L_12 = ___extensionRegistry1;
		uint32_t L_13 = V_0;
		String_t* L_14 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_11, L_12, L_13, L_14);
		goto IL_00f5;
	}

IL_004d:
	{
		uint32_t L_15 = V_0;
		if (!L_15)
		{
			goto IL_006f;
		}
	}
	{
		uint32_t L_16 = V_0;
		if ((((int32_t)L_16) == ((int32_t)8)))
		{
			goto IL_0092;
		}
	}
	{
		uint32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)21))))
		{
			goto IL_00b3;
		}
	}
	{
		uint32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)29))))
		{
			goto IL_00d4;
		}
	}
	{
		goto IL_0075;
	}

IL_006f:
	{
		InvalidProtocolBufferException_t2498581859 * L_19 = InvalidProtocolBufferException_InvalidTag_m4139780452(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, NULL, Builder_MergeFrom_m3447828463_RuntimeMethod_var);
	}

IL_0075:
	{
		uint32_t L_20 = V_0;
		bool L_21 = WireFormat_IsEndGroupTag_m2504732292(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0082;
		}
	}
	{
		return __this;
	}

IL_0082:
	{
		RuntimeObject* L_22 = ___input0;
		ExtensionRegistry_t4271428238 * L_23 = ___extensionRegistry1;
		uint32_t L_24 = V_0;
		String_t* L_25 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/MotionEvent/Types/Pointer,proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_22, L_23, L_24, L_25);
		goto IL_00f5;
	}

IL_0092:
	{
		Pointer_t4145025558 * L_26 = __this->get_result_1();
		RuntimeObject* L_27 = ___input0;
		Pointer_t4145025558 * L_28 = __this->get_result_1();
		NullCheck(L_28);
		int32_t* L_29 = L_28->get_address_of_id__5();
		NullCheck(L_27);
		bool L_30 = InterfaceFuncInvoker1< bool, int32_t* >::Invoke(3 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt32(System.Int32&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_27, (int32_t*)L_29);
		NullCheck(L_26);
		L_26->set_hasId_4(L_30);
		goto IL_00f5;
	}

IL_00b3:
	{
		Pointer_t4145025558 * L_31 = __this->get_result_1();
		RuntimeObject* L_32 = ___input0;
		Pointer_t4145025558 * L_33 = __this->get_result_1();
		NullCheck(L_33);
		float* L_34 = L_33->get_address_of_normalizedX__8();
		NullCheck(L_32);
		bool L_35 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_32, (float*)L_34);
		NullCheck(L_31);
		L_31->set_hasNormalizedX_7(L_35);
		goto IL_00f5;
	}

IL_00d4:
	{
		Pointer_t4145025558 * L_36 = __this->get_result_1();
		RuntimeObject* L_37 = ___input0;
		Pointer_t4145025558 * L_38 = __this->get_result_1();
		NullCheck(L_38);
		float* L_39 = L_38->get_address_of_normalizedY__11();
		NullCheck(L_37);
		bool L_40 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_37, (float*)L_39);
		NullCheck(L_36);
		L_36->set_hasNormalizedY_10(L_40);
		goto IL_00f5;
	}

IL_00f5:
	{
		RuntimeObject* L_41 = ___input0;
		NullCheck(L_41);
		bool L_42 = InterfaceFuncInvoker2< bool, uint32_t*, String_t** >::Invoke(0 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadTag(System.UInt32&,System.String&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_41, (uint32_t*)(&V_0), (String_t**)(&V_1));
		if (L_42)
		{
			goto IL_000c;
		}
	}
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_HasId()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasId_m3245962830 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasId_4();
		return L_1;
	}
}
// System.Int32 proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_Id()
extern "C" IL2CPP_METHOD_ATTR int32_t Builder_get_Id_m1003926567 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int32_t L_1 = Pointer_get_Id_m2656726378(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::set_Id(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Id_m1971136214 (Builder_t582111845 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		Builder_SetId_m1976386508(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::SetId(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_SetId_m1976386508 (Builder_t582111845 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasId_4((bool)1);
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		int32_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_id__5(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::ClearId()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_ClearId_m4046848881 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasId_4((bool)0);
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_id__5(0);
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_HasNormalizedX()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasNormalizedX_m1880287451 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasNormalizedX_7();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_NormalizedX()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_NormalizedX_m2958759223 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = Pointer_get_NormalizedX_m306370287(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::set_NormalizedX(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_NormalizedX_m3801472179 (Builder_t582111845 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetNormalizedX_m264783475(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::SetNormalizedX(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_SetNormalizedX_m264783475 (Builder_t582111845 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasNormalizedX_7((bool)1);
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_normalizedX__8(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::ClearNormalizedX()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_ClearNormalizedX_m837913784 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasNormalizedX_7((bool)0);
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_normalizedX__8((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_HasNormalizedY()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasNormalizedY_m3446371392 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasNormalizedY_10();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::get_NormalizedY()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_NormalizedY_m229875868 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = Pointer_get_NormalizedY_m3035253642(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::set_NormalizedY(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_NormalizedY_m3801436436 (Builder_t582111845 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetNormalizedY_m264819350(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::SetNormalizedY(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_SetNormalizedY_m264819350 (Builder_t582111845 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasNormalizedY_10((bool)1);
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_normalizedY__11(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder proto.PhoneEvent/Types/MotionEvent/Types/Pointer/Builder::ClearNormalizedY()
extern "C" IL2CPP_METHOD_ATTR Builder_t582111845 * Builder_ClearNormalizedY_m3566797139 (Builder_t582111845 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m2800481259(__this, /*hidden argument*/NULL);
		Pointer_t4145025558 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasNormalizedY_10((bool)0);
		Pointer_t4145025558 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_normalizedY__11((0.0f));
		return __this;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/OrientationEvent::.ctor()
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent__ctor_m3708828125 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent__ctor_m3708828125_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_memoizedSerializedSize_18((-1));
		GeneratedMessageLite_2__ctor_m3287578133(__this, /*hidden argument*/GeneratedMessageLite_2__ctor_m3287578133_RuntimeMethod_var);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent::.cctor()
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent__cctor_m2779686345 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent__cctor_m2779686345_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OrientationEvent_t158247624 * L_0 = (OrientationEvent_t158247624 *)il2cpp_codegen_object_new(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		OrientationEvent__ctor_m3708828125(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		OrientationEvent_t158247624 * L_1 = OrientationEvent_MakeReadOnly_m838780390(L_0, /*hidden argument*/NULL);
		((OrientationEvent_t158247624_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_t158247624_il2cpp_TypeInfo_var))->set_defaultInstance_0(L_1);
		StringU5BU5D_t1281789340* L_2 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t1281789340* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral3166771228);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3166771228);
		StringU5BU5D_t1281789340* L_4 = L_3;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral3452614601);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral3452614601);
		StringU5BU5D_t1281789340* L_5 = L_4;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral3452614616);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3452614616);
		StringU5BU5D_t1281789340* L_6 = L_5;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3452614615);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral3452614615);
		StringU5BU5D_t1281789340* L_7 = L_6;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral3452614614);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614614);
		((OrientationEvent_t158247624_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_t158247624_il2cpp_TypeInfo_var))->set__orientationEventFieldNames_1(L_7);
		UInt32U5BU5D_t2770800703* L_8 = (UInt32U5BU5D_t2770800703*)SZArrayNew(UInt32U5BU5D_t2770800703_il2cpp_TypeInfo_var, (uint32_t)5);
		UInt32U5BU5D_t2770800703* L_9 = L_8;
		RuntimeFieldHandle_t1871169219  L_10 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_t3057255367____U24fieldU2DFADC743710841EB901D5F6FBC97F555D4BD94310_4_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m3117905507(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_9, L_10, /*hidden argument*/NULL);
		((OrientationEvent_t158247624_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_t158247624_il2cpp_TypeInfo_var))->set__orientationEventFieldTags_2(L_9);
		IL2CPP_RUNTIME_CLASS_INIT(PhoneEvent_t3448566392_il2cpp_TypeInfo_var);
		RuntimeObject * L_11 = ((PhoneEvent_t3448566392_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t3448566392_il2cpp_TypeInfo_var))->get_Descriptor_0();
		il2cpp_codegen_object_reference_equals(L_11, NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::get_DefaultInstance()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_get_DefaultInstance_m2374728749 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_get_DefaultInstance_m2374728749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		OrientationEvent_t158247624 * L_0 = ((OrientationEvent_t158247624_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_t158247624_il2cpp_TypeInfo_var))->get_defaultInstance_0();
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_get_DefaultInstanceForType_m1391900177 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_get_DefaultInstanceForType_m1391900177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		OrientationEvent_t158247624 * L_0 = OrientationEvent_get_DefaultInstance_m2374728749(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::get_ThisMessage()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_get_ThisMessage_m2208555730 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasTimestamp_m3922884685 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasTimestamp_4();
		return L_0;
	}
}
// System.Int64 proto.PhoneEvent/Types/OrientationEvent::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t OrientationEvent_get_Timestamp_m723854610 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		int64_t L_0 = __this->get_timestamp__5();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasX_m2602962142 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasX_7();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_X()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_X_m884120132 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_x__8();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasY_m646647006 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasY_10();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_Y()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_Y_m3222772292 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_y__11();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasZ_m2220625118 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasZ_13();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_Z()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_Z_m501783108 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_z__14();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_HasW()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_HasW_m1028984030 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_hasW_16();
		return L_0;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent::get_W()
extern "C" IL2CPP_METHOD_ATTR float OrientationEvent_get_W_m546413124 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_w__17();
		return L_0;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_get_IsInitialized_m3530071515 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent::WriteTo(Google.ProtocolBuffers.ICodedOutputStream)
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent_WriteTo_m633475206 (OrientationEvent_t158247624 * __this, RuntimeObject* ___output0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_WriteTo_m633475206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1281789340* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_0 = ((OrientationEvent_t158247624_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_t158247624_il2cpp_TypeInfo_var))->get__orientationEventFieldNames_1();
		V_0 = L_0;
		bool L_1 = __this->get_hasTimestamp_4();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		RuntimeObject* L_2 = ___output0;
		StringU5BU5D_t1281789340* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		String_t* L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		int64_t L_6 = OrientationEvent_get_Timestamp_m723854610(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker3< int32_t, String_t*, int64_t >::Invoke(1 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteInt64(System.Int32,System.String,System.Int64) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_2, 1, L_5, L_6);
	}

IL_0021:
	{
		bool L_7 = __this->get_hasX_7();
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RuntimeObject* L_8 = ___output0;
		StringU5BU5D_t1281789340* L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = 2;
		String_t* L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		float L_12 = OrientationEvent_get_X_m884120132(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_8, 2, L_11, L_12);
	}

IL_003c:
	{
		bool L_13 = __this->get_hasY_10();
		if (!L_13)
		{
			goto IL_0057;
		}
	}
	{
		RuntimeObject* L_14 = ___output0;
		StringU5BU5D_t1281789340* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = 3;
		String_t* L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		float L_18 = OrientationEvent_get_Y_m3222772292(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_14, 3, L_17, L_18);
	}

IL_0057:
	{
		bool L_19 = __this->get_hasZ_13();
		if (!L_19)
		{
			goto IL_0072;
		}
	}
	{
		RuntimeObject* L_20 = ___output0;
		StringU5BU5D_t1281789340* L_21 = V_0;
		NullCheck(L_21);
		int32_t L_22 = 4;
		String_t* L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		float L_24 = OrientationEvent_get_Z_m501783108(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_20, 4, L_23, L_24);
	}

IL_0072:
	{
		bool L_25 = __this->get_hasW_16();
		if (!L_25)
		{
			goto IL_008d;
		}
	}
	{
		RuntimeObject* L_26 = ___output0;
		StringU5BU5D_t1281789340* L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = 1;
		String_t* L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		float L_30 = OrientationEvent_get_W_m546413124(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		InterfaceActionInvoker3< int32_t, String_t*, float >::Invoke(0 /* System.Void Google.ProtocolBuffers.ICodedOutputStream::WriteFloat(System.Int32,System.String,System.Single) */, ICodedOutputStream_t1215615781_il2cpp_TypeInfo_var, L_26, 5, L_29, L_30);
	}

IL_008d:
	{
		return;
	}
}
// System.Int32 proto.PhoneEvent/Types/OrientationEvent::get_SerializedSize()
extern "C" IL2CPP_METHOD_ATTR int32_t OrientationEvent_get_SerializedSize_m725630368 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_get_SerializedSize_m725630368_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_memoizedSerializedSize_18();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0010:
	{
		V_0 = 0;
		bool L_3 = __this->get_hasTimestamp_4();
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_4 = V_0;
		int64_t L_5 = OrientationEvent_get_Timestamp_m723854610(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_6 = CodedOutputStream_ComputeInt64Size_m2929732330(NULL /*static, unused*/, 1, L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_6));
	}

IL_002c:
	{
		bool L_7 = __this->get_hasX_7();
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_8 = V_0;
		float L_9 = OrientationEvent_get_X_m884120132(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_10 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 2, L_9, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)L_10));
	}

IL_0046:
	{
		bool L_11 = __this->get_hasY_10();
		if (!L_11)
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_12 = V_0;
		float L_13 = OrientationEvent_get_Y_m3222772292(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_14 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 3, L_13, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_14));
	}

IL_0060:
	{
		bool L_15 = __this->get_hasZ_13();
		if (!L_15)
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_16 = V_0;
		float L_17 = OrientationEvent_get_Z_m501783108(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_18 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 4, L_17, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_18));
	}

IL_007a:
	{
		bool L_19 = __this->get_hasW_16();
		if (!L_19)
		{
			goto IL_0094;
		}
	}
	{
		int32_t L_20 = V_0;
		float L_21 = OrientationEvent_get_W_m546413124(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CodedOutputStream_t1787628118_il2cpp_TypeInfo_var);
		int32_t L_22 = CodedOutputStream_ComputeFloatSize_m3601108612(NULL /*static, unused*/, 5, L_21, /*hidden argument*/NULL);
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)L_22));
	}

IL_0094:
	{
		int32_t L_23 = V_0;
		__this->set_memoizedSerializedSize_18(L_23);
		int32_t L_24 = V_0;
		return L_24;
	}
}
// System.Int32 proto.PhoneEvent/Types/OrientationEvent::GetHashCode()
extern "C" IL2CPP_METHOD_ATTR int32_t OrientationEvent_GetHashCode_m2906307316 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Type_t * L_0 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_hasTimestamp_4();
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_3 = V_0;
		int64_t* L_4 = __this->get_address_of_timestamp__5();
		int32_t L_5 = Int64_GetHashCode_m703091690((int64_t*)L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_3^(int32_t)L_5));
	}

IL_002b:
	{
		bool L_6 = __this->get_hasX_7();
		if (!L_6)
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_7 = V_0;
		float* L_8 = __this->get_address_of_x__8();
		int32_t L_9 = Single_GetHashCode_m1558506138((float*)L_8, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_7^(int32_t)L_9));
	}

IL_004a:
	{
		bool L_10 = __this->get_hasY_10();
		if (!L_10)
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_11 = V_0;
		float* L_12 = __this->get_address_of_y__11();
		int32_t L_13 = Single_GetHashCode_m1558506138((float*)L_12, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_11^(int32_t)L_13));
	}

IL_0069:
	{
		bool L_14 = __this->get_hasZ_13();
		if (!L_14)
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_15 = V_0;
		float* L_16 = __this->get_address_of_z__14();
		int32_t L_17 = Single_GetHashCode_m1558506138((float*)L_16, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_15^(int32_t)L_17));
	}

IL_0088:
	{
		bool L_18 = __this->get_hasW_16();
		if (!L_18)
		{
			goto IL_00a7;
		}
	}
	{
		int32_t L_19 = V_0;
		float* L_20 = __this->get_address_of_w__17();
		int32_t L_21 = Single_GetHashCode_m1558506138((float*)L_20, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_19^(int32_t)L_21));
	}

IL_00a7:
	{
		int32_t L_22 = V_0;
		return L_22;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent::Equals(System.Object)
extern "C" IL2CPP_METHOD_ATTR bool OrientationEvent_Equals_m2974127354 (OrientationEvent_t158247624 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_Equals_m2974127354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OrientationEvent_t158247624 * V_0 = NULL;
	{
		RuntimeObject * L_0 = ___obj0;
		V_0 = ((OrientationEvent_t158247624 *)IsInstSealed((RuntimeObject*)L_0, OrientationEvent_t158247624_il2cpp_TypeInfo_var));
		OrientationEvent_t158247624 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		bool L_2 = __this->get_hasTimestamp_4();
		OrientationEvent_t158247624 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = L_3->get_hasTimestamp_4();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_4))))
		{
			goto IL_0041;
		}
	}
	{
		bool L_5 = __this->get_hasTimestamp_4();
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		int64_t* L_6 = __this->get_address_of_timestamp__5();
		OrientationEvent_t158247624 * L_7 = V_0;
		NullCheck(L_7);
		int64_t L_8 = L_7->get_timestamp__5();
		bool L_9 = Int64_Equals_m680137412((int64_t*)L_6, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0043;
		}
	}

IL_0041:
	{
		return (bool)0;
	}

IL_0043:
	{
		bool L_10 = __this->get_hasX_7();
		OrientationEvent_t158247624 * L_11 = V_0;
		NullCheck(L_11);
		bool L_12 = L_11->get_hasX_7();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_12))))
		{
			goto IL_0075;
		}
	}
	{
		bool L_13 = __this->get_hasX_7();
		if (!L_13)
		{
			goto IL_0077;
		}
	}
	{
		float* L_14 = __this->get_address_of_x__8();
		OrientationEvent_t158247624 * L_15 = V_0;
		NullCheck(L_15);
		float L_16 = L_15->get_x__8();
		bool L_17 = Single_Equals_m1601893879((float*)L_14, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0077;
		}
	}

IL_0075:
	{
		return (bool)0;
	}

IL_0077:
	{
		bool L_18 = __this->get_hasY_10();
		OrientationEvent_t158247624 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = L_19->get_hasY_10();
		if ((!(((uint32_t)L_18) == ((uint32_t)L_20))))
		{
			goto IL_00a9;
		}
	}
	{
		bool L_21 = __this->get_hasY_10();
		if (!L_21)
		{
			goto IL_00ab;
		}
	}
	{
		float* L_22 = __this->get_address_of_y__11();
		OrientationEvent_t158247624 * L_23 = V_0;
		NullCheck(L_23);
		float L_24 = L_23->get_y__11();
		bool L_25 = Single_Equals_m1601893879((float*)L_22, L_24, /*hidden argument*/NULL);
		if (L_25)
		{
			goto IL_00ab;
		}
	}

IL_00a9:
	{
		return (bool)0;
	}

IL_00ab:
	{
		bool L_26 = __this->get_hasZ_13();
		OrientationEvent_t158247624 * L_27 = V_0;
		NullCheck(L_27);
		bool L_28 = L_27->get_hasZ_13();
		if ((!(((uint32_t)L_26) == ((uint32_t)L_28))))
		{
			goto IL_00dd;
		}
	}
	{
		bool L_29 = __this->get_hasZ_13();
		if (!L_29)
		{
			goto IL_00df;
		}
	}
	{
		float* L_30 = __this->get_address_of_z__14();
		OrientationEvent_t158247624 * L_31 = V_0;
		NullCheck(L_31);
		float L_32 = L_31->get_z__14();
		bool L_33 = Single_Equals_m1601893879((float*)L_30, L_32, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00df;
		}
	}

IL_00dd:
	{
		return (bool)0;
	}

IL_00df:
	{
		bool L_34 = __this->get_hasW_16();
		OrientationEvent_t158247624 * L_35 = V_0;
		NullCheck(L_35);
		bool L_36 = L_35->get_hasW_16();
		if ((!(((uint32_t)L_34) == ((uint32_t)L_36))))
		{
			goto IL_0111;
		}
	}
	{
		bool L_37 = __this->get_hasW_16();
		if (!L_37)
		{
			goto IL_0113;
		}
	}
	{
		float* L_38 = __this->get_address_of_w__17();
		OrientationEvent_t158247624 * L_39 = V_0;
		NullCheck(L_39);
		float L_40 = L_39->get_w__17();
		bool L_41 = Single_Equals_m1601893879((float*)L_38, L_40, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_0113;
		}
	}

IL_0111:
	{
		return (bool)0;
	}

IL_0113:
	{
		return (bool)1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent::PrintTo(System.IO.TextWriter)
extern "C" IL2CPP_METHOD_ATTR void OrientationEvent_PrintTo_m328199974 (OrientationEvent_t158247624 * __this, TextWriter_t3478189236 * ___writer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_PrintTo_m328199974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_hasTimestamp_4();
		int64_t L_1 = __this->get_timestamp__5();
		int64_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int64_t3736567304_il2cpp_TypeInfo_var, &L_2);
		TextWriter_t3478189236 * L_4 = ___writer0;
		GeneratedMessageLite_2_PrintField_m4136168662(NULL /*static, unused*/, _stringLiteral3166771228, L_0, L_3, L_4, /*hidden argument*/GeneratedMessageLite_2_PrintField_m4136168662_RuntimeMethod_var);
		bool L_5 = __this->get_hasX_7();
		float L_6 = __this->get_x__8();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_7);
		TextWriter_t3478189236 * L_9 = ___writer0;
		GeneratedMessageLite_2_PrintField_m4136168662(NULL /*static, unused*/, _stringLiteral3452614616, L_5, L_8, L_9, /*hidden argument*/GeneratedMessageLite_2_PrintField_m4136168662_RuntimeMethod_var);
		bool L_10 = __this->get_hasY_10();
		float L_11 = __this->get_y__11();
		float L_12 = L_11;
		RuntimeObject * L_13 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_12);
		TextWriter_t3478189236 * L_14 = ___writer0;
		GeneratedMessageLite_2_PrintField_m4136168662(NULL /*static, unused*/, _stringLiteral3452614615, L_10, L_13, L_14, /*hidden argument*/GeneratedMessageLite_2_PrintField_m4136168662_RuntimeMethod_var);
		bool L_15 = __this->get_hasZ_13();
		float L_16 = __this->get_z__14();
		float L_17 = L_16;
		RuntimeObject * L_18 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_17);
		TextWriter_t3478189236 * L_19 = ___writer0;
		GeneratedMessageLite_2_PrintField_m4136168662(NULL /*static, unused*/, _stringLiteral3452614614, L_15, L_18, L_19, /*hidden argument*/GeneratedMessageLite_2_PrintField_m4136168662_RuntimeMethod_var);
		bool L_20 = __this->get_hasW_16();
		float L_21 = __this->get_w__17();
		float L_22 = L_21;
		RuntimeObject * L_23 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_22);
		TextWriter_t3478189236 * L_24 = ___writer0;
		GeneratedMessageLite_2_PrintField_m4136168662(NULL /*static, unused*/, _stringLiteral3452614601, L_20, L_23, L_24, /*hidden argument*/GeneratedMessageLite_2_PrintField_m4136168662_RuntimeMethod_var);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(Google.ProtocolBuffers.ByteString)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseFrom_m2109398516 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m2109398516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		NullCheck(L_0);
		Builder_t2279437287 * L_2 = AbstractBuilderLite_2_MergeFrom_m1442256499(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m1442256499_RuntimeMethod_var);
		NullCheck(L_2);
		OrientationEvent_t158247624 * L_3 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(Google.ProtocolBuffers.ByteString,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseFrom_m1382868771 (RuntimeObject * __this /* static, unused */, ByteString_t35393593 * ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m1382868771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteString_t35393593 * L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2279437287 * L_3 = AbstractBuilderLite_2_MergeFrom_m3702155892(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m3702155892_RuntimeMethod_var);
		NullCheck(L_3);
		OrientationEvent_t158247624 * L_4 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseFrom_m3260499152 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m3260499152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		NullCheck(L_0);
		Builder_t2279437287 * L_2 = AbstractBuilderLite_2_MergeFrom_m4193624750(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m4193624750_RuntimeMethod_var);
		NullCheck(L_2);
		OrientationEvent_t158247624 * L_3 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(System.Byte[],Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseFrom_m3894785390 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* ___data0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m3894785390_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t4116647657* L_1 = ___data0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2279437287 * L_3 = AbstractBuilderLite_2_MergeFrom_m2211482326(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m2211482326_RuntimeMethod_var);
		NullCheck(L_3);
		OrientationEvent_t158247624 * L_4 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseFrom_m955229731 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m955229731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2279437287 * L_2 = AbstractBuilderLite_2_MergeFrom_m2051397694(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m2051397694_RuntimeMethod_var);
		NullCheck(L_2);
		OrientationEvent_t158247624 * L_3 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseFrom_m2469088444 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m2469088444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2279437287 * L_3 = AbstractBuilderLite_2_MergeFrom_m1351836793(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeFrom_m1351836793_RuntimeMethod_var);
		NullCheck(L_3);
		OrientationEvent_t158247624 * L_4 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseDelimitedFrom(System.IO.Stream)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseDelimitedFrom_m212379149 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseDelimitedFrom_m212379149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2279437287 * L_2 = AbstractBuilderLite_2_MergeDelimitedFrom_m3997270878(L_0, L_1, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m3997270878_RuntimeMethod_var);
		NullCheck(L_2);
		OrientationEvent_t158247624 * L_3 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseDelimitedFrom(System.IO.Stream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseDelimitedFrom_m251292222 (RuntimeObject * __this /* static, unused */, Stream_t1273022909 * ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseDelimitedFrom_m251292222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		Stream_t1273022909 * L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2279437287 * L_3 = AbstractBuilderLite_2_MergeDelimitedFrom_m2055751663(L_0, L_1, L_2, /*hidden argument*/AbstractBuilderLite_2_MergeDelimitedFrom_m2055751663_RuntimeMethod_var);
		NullCheck(L_3);
		OrientationEvent_t158247624 * L_4 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseFrom_m177464868 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m177464868_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		NullCheck(L_0);
		Builder_t2279437287 * L_2 = VirtFuncInvoker1< Builder_t2279437287 *, RuntimeObject* >::Invoke(16 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream) */, L_0, L_1);
		NullCheck(L_2);
		OrientationEvent_t158247624 * L_3 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_2, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::ParseFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_ParseFrom_m572682107 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ParseFrom_m572682107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m324048109(NULL /*static, unused*/, /*hidden argument*/NULL);
		RuntimeObject* L_1 = ___input0;
		ExtensionRegistry_t4271428238 * L_2 = ___extensionRegistry1;
		NullCheck(L_0);
		Builder_t2279437287 * L_3 = VirtFuncInvoker2< Builder_t2279437287 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, L_0, L_1, L_2);
		NullCheck(L_3);
		OrientationEvent_t158247624 * L_4 = GeneratedBuilderLite_2_BuildParsed_m981074060(L_3, /*hidden argument*/GeneratedBuilderLite_2_BuildParsed_m981074060_RuntimeMethod_var);
		return L_4;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent::MakeReadOnly()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * OrientationEvent_MakeReadOnly_m838780390 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * OrientationEvent_CreateBuilder_m324048109 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_CreateBuilder_m324048109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t2279437287 * L_0 = (Builder_t2279437287 *)il2cpp_codegen_object_new(Builder_t2279437287_il2cpp_TypeInfo_var);
		Builder__ctor_m3698740796(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::ToBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * OrientationEvent_ToBuilder_m553496387 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_ToBuilder_m553496387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		Builder_t2279437287 * L_0 = OrientationEvent_CreateBuilder_m4263316720(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilderForType()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * OrientationEvent_CreateBuilderForType_m795056700 (OrientationEvent_t158247624 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_CreateBuilderForType_m795056700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Builder_t2279437287 * L_0 = (Builder_t2279437287 *)il2cpp_codegen_object_new(Builder_t2279437287_il2cpp_TypeInfo_var);
		Builder__ctor_m3698740796(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent::CreateBuilder(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * OrientationEvent_CreateBuilder_m4263316720 (RuntimeObject * __this /* static, unused */, OrientationEvent_t158247624 * ___prototype0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OrientationEvent_CreateBuilder_m4263316720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OrientationEvent_t158247624 * L_0 = ___prototype0;
		Builder_t2279437287 * L_1 = (Builder_t2279437287 *)il2cpp_codegen_object_new(Builder_t2279437287_il2cpp_TypeInfo_var);
		Builder__ctor_m3220974630(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3698740796 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m3698740796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m1251714133(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m1251714133_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		OrientationEvent_t158247624 * L_0 = OrientationEvent_get_DefaultInstance_m2374728749(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::.ctor(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR void Builder__ctor_m3220974630 (Builder_t2279437287 * __this, OrientationEvent_t158247624 * ___cloneFrom0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder__ctor_m3220974630_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeneratedBuilderLite_2__ctor_m1251714133(__this, /*hidden argument*/GeneratedBuilderLite_2__ctor_m1251714133_RuntimeMethod_var);
		OrientationEvent_t158247624 * L_0 = ___cloneFrom0;
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::get_ThisBuilder()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_get_ThisBuilder_m1772085724 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::PrepareBuilder()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * Builder_PrepareBuilder_m147557039 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_PrepareBuilder_m147557039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	OrientationEvent_t158247624 * V_0 = NULL;
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		V_0 = L_1;
		OrientationEvent_t158247624 * L_2 = (OrientationEvent_t158247624 *)il2cpp_codegen_object_new(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		OrientationEvent__ctor_m3708828125(L_2, /*hidden argument*/NULL);
		__this->set_result_1(L_2);
		__this->set_resultIsReadOnly_0((bool)0);
		OrientationEvent_t158247624 * L_3 = V_0;
		VirtFuncInvoker1< Builder_t2279437287 *, OrientationEvent_t158247624 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(!0) */, __this, L_3);
	}

IL_002c:
	{
		OrientationEvent_t158247624 * L_4 = __this->get_result_1();
		return L_4;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_IsInitialized_m713386431 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(10 /* System.Boolean Google.ProtocolBuffers.AbstractMessageLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::get_IsInitialized() */, L_0);
		return L_1;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::get_MessageBeingBuilt()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * Builder_get_MessageBeingBuilt_m2039145112 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::Clear()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_Clear_m1516654503 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clear_m1516654503_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		OrientationEvent_t158247624 * L_0 = OrientationEvent_get_DefaultInstance_m2374728749(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_1(L_0);
		__this->set_resultIsReadOnly_0((bool)1);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::Clone()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_Clone_m3442298780 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_Clone_m3442298780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		Builder_t2279437287 * L_2 = (Builder_t2279437287 *)il2cpp_codegen_object_new(Builder_t2279437287_il2cpp_TypeInfo_var);
		Builder__ctor_m3220974630(L_2, L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		Builder_t2279437287 * L_3 = (Builder_t2279437287 *)il2cpp_codegen_object_new(Builder_t2279437287_il2cpp_TypeInfo_var);
		Builder__ctor_m3698740796(L_3, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_4 = __this->get_result_1();
		NullCheck(L_3);
		Builder_t2279437287 * L_5 = VirtFuncInvoker1< Builder_t2279437287 *, OrientationEvent_t158247624 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(!0) */, L_3, L_4);
		return L_5;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::get_DefaultInstanceForType()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * Builder_get_DefaultInstanceForType_m980516419 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_get_DefaultInstanceForType_m980516419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		OrientationEvent_t158247624 * L_0 = OrientationEvent_get_DefaultInstance_m2374728749(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// proto.PhoneEvent/Types/OrientationEvent proto.PhoneEvent/Types/OrientationEvent/Builder::BuildPartial()
extern "C" IL2CPP_METHOD_ATTR OrientationEvent_t158247624 * Builder_BuildPartial_m547782778 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_resultIsReadOnly_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		return L_1;
	}

IL_0012:
	{
		__this->set_resultIsReadOnly_0((bool)1);
		OrientationEvent_t158247624 * L_2 = __this->get_result_1();
		NullCheck(L_2);
		OrientationEvent_t158247624 * L_3 = OrientationEvent_MakeReadOnly_m838780390(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::MergeFrom(Google.ProtocolBuffers.IMessageLite)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_MergeFrom_m1018040705 (Builder_t2279437287 * __this, RuntimeObject* ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m1018040705_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___other0;
		if (!((OrientationEvent_t158247624 *)IsInstSealed((RuntimeObject*)L_0, OrientationEvent_t158247624_il2cpp_TypeInfo_var)))
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject* L_1 = ___other0;
		Builder_t2279437287 * L_2 = VirtFuncInvoker1< Builder_t2279437287 *, OrientationEvent_t158247624 * >::Invoke(26 /* !1 Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(!0) */, __this, ((OrientationEvent_t158247624 *)CastclassSealed((RuntimeObject*)L_1, OrientationEvent_t158247624_il2cpp_TypeInfo_var)));
		return L_2;
	}

IL_0018:
	{
		RuntimeObject* L_3 = ___other0;
		GeneratedBuilderLite_2_MergeFrom_m4038465069(__this, L_3, /*hidden argument*/GeneratedBuilderLite_2_MergeFrom_m4038465069_RuntimeMethod_var);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::MergeFrom(proto.PhoneEvent/Types/OrientationEvent)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_MergeFrom_m3801718990 (Builder_t2279437287 * __this, OrientationEvent_t158247624 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m3801718990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OrientationEvent_t158247624 * L_0 = ___other0;
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		OrientationEvent_t158247624 * L_1 = OrientationEvent_get_DefaultInstance_m2374728749(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((RuntimeObject*)(OrientationEvent_t158247624 *)L_0) == ((RuntimeObject*)(OrientationEvent_t158247624 *)L_1))))
		{
			goto IL_000d;
		}
	}
	{
		return __this;
	}

IL_000d:
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_2 = ___other0;
		NullCheck(L_2);
		bool L_3 = OrientationEvent_get_HasTimestamp_m3922884685(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		OrientationEvent_t158247624 * L_4 = ___other0;
		NullCheck(L_4);
		int64_t L_5 = OrientationEvent_get_Timestamp_m723854610(L_4, /*hidden argument*/NULL);
		Builder_set_Timestamp_m1393789818(__this, L_5, /*hidden argument*/NULL);
	}

IL_002b:
	{
		OrientationEvent_t158247624 * L_6 = ___other0;
		NullCheck(L_6);
		bool L_7 = OrientationEvent_get_HasX_m2602962142(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		OrientationEvent_t158247624 * L_8 = ___other0;
		NullCheck(L_8);
		float L_9 = OrientationEvent_get_X_m884120132(L_8, /*hidden argument*/NULL);
		Builder_set_X_m676748554(__this, L_9, /*hidden argument*/NULL);
	}

IL_0042:
	{
		OrientationEvent_t158247624 * L_10 = ___other0;
		NullCheck(L_10);
		bool L_11 = OrientationEvent_get_HasY_m646647006(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0059;
		}
	}
	{
		OrientationEvent_t158247624 * L_12 = ___other0;
		NullCheck(L_12);
		float L_13 = OrientationEvent_get_Y_m3222772292(L_12, /*hidden argument*/NULL);
		Builder_set_Y_m133127197(__this, L_13, /*hidden argument*/NULL);
	}

IL_0059:
	{
		OrientationEvent_t158247624 * L_14 = ___other0;
		NullCheck(L_14);
		bool L_15 = OrientationEvent_get_HasZ_m2220625118(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0070;
		}
	}
	{
		OrientationEvent_t158247624 * L_16 = ___other0;
		NullCheck(L_16);
		float L_17 = OrientationEvent_get_Z_m501783108(L_16, /*hidden argument*/NULL);
		Builder_set_Z_m945314922(__this, L_17, /*hidden argument*/NULL);
	}

IL_0070:
	{
		OrientationEvent_t158247624 * L_18 = ___other0;
		NullCheck(L_18);
		bool L_19 = OrientationEvent_get_HasW_m1028984030(L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0087;
		}
	}
	{
		OrientationEvent_t158247624 * L_20 = ___other0;
		NullCheck(L_20);
		float L_21 = OrientationEvent_get_W_m546413124(L_20, /*hidden argument*/NULL);
		Builder_set_W_m2009947325(__this, L_21, /*hidden argument*/NULL);
	}

IL_0087:
	{
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_MergeFrom_m877028760 (Builder_t2279437287 * __this, RuntimeObject* ___input0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m877028760_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___input0;
		IL2CPP_RUNTIME_CLASS_INIT(ExtensionRegistry_t4271428238_il2cpp_TypeInfo_var);
		ExtensionRegistry_t4271428238 * L_1 = ExtensionRegistry_get_Empty_m3666683255(NULL /*static, unused*/, /*hidden argument*/NULL);
		Builder_t2279437287 * L_2 = VirtFuncInvoker2< Builder_t2279437287 *, RuntimeObject*, ExtensionRegistry_t4271428238 * >::Invoke(15 /* !1 Google.ProtocolBuffers.AbstractBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry) */, __this, L_0, L_1);
		return L_2;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::MergeFrom(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_MergeFrom_m160215804 (Builder_t2279437287 * __this, RuntimeObject* ___input0, ExtensionRegistry_t4271428238 * ___extensionRegistry1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Builder_MergeFrom_m160215804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	String_t* V_1 = NULL;
	int32_t V_2 = 0;
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		goto IL_0147;
	}

IL_000c:
	{
		uint32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_004d;
		}
	}
	{
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		StringU5BU5D_t1281789340* L_2 = ((OrientationEvent_t158247624_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_t158247624_il2cpp_TypeInfo_var))->get__orientationEventFieldNames_1();
		String_t* L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(StringComparer_t3301955079_il2cpp_TypeInfo_var);
		StringComparer_t3301955079 * L_4 = StringComparer_get_Ordinal_m2103862281(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Array_BinarySearch_TisString_t_m2519237149(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/Array_BinarySearch_TisString_t_m2519237149_RuntimeMethod_var);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(OrientationEvent_t158247624_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t2770800703* L_7 = ((OrientationEvent_t158247624_StaticFields*)il2cpp_codegen_static_fields_for(OrientationEvent_t158247624_il2cpp_TypeInfo_var))->get__orientationEventFieldTags_2();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		uint32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_0 = L_10;
		goto IL_004d;
	}

IL_003d:
	{
		RuntimeObject* L_11 = ___input0;
		ExtensionRegistry_t4271428238 * L_12 = ___extensionRegistry1;
		uint32_t L_13 = V_0;
		String_t* L_14 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_11, L_12, L_13, L_14);
		goto IL_0147;
	}

IL_004d:
	{
		uint32_t L_15 = V_0;
		if (!L_15)
		{
			goto IL_007f;
		}
	}
	{
		uint32_t L_16 = V_0;
		if ((((int32_t)L_16) == ((int32_t)8)))
		{
			goto IL_00a2;
		}
	}
	{
		uint32_t L_17 = V_0;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)21))))
		{
			goto IL_00c3;
		}
	}
	{
		uint32_t L_18 = V_0;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)29))))
		{
			goto IL_00e4;
		}
	}
	{
		uint32_t L_19 = V_0;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)37))))
		{
			goto IL_0105;
		}
	}
	{
		uint32_t L_20 = V_0;
		if ((((int32_t)L_20) == ((int32_t)((int32_t)45))))
		{
			goto IL_0126;
		}
	}
	{
		goto IL_0085;
	}

IL_007f:
	{
		InvalidProtocolBufferException_t2498581859 * L_21 = InvalidProtocolBufferException_InvalidTag_m4139780452(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_21, NULL, Builder_MergeFrom_m160215804_RuntimeMethod_var);
	}

IL_0085:
	{
		uint32_t L_22 = V_0;
		bool L_23 = WireFormat_IsEndGroupTag_m2504732292(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0092;
		}
	}
	{
		return __this;
	}

IL_0092:
	{
		RuntimeObject* L_24 = ___input0;
		ExtensionRegistry_t4271428238 * L_25 = ___extensionRegistry1;
		uint32_t L_26 = V_0;
		String_t* L_27 = V_1;
		VirtFuncInvoker4< bool, RuntimeObject*, ExtensionRegistry_t4271428238 *, uint32_t, String_t* >::Invoke(27 /* System.Boolean Google.ProtocolBuffers.GeneratedBuilderLite`2<proto.PhoneEvent/Types/OrientationEvent,proto.PhoneEvent/Types/OrientationEvent/Builder>::ParseUnknownField(Google.ProtocolBuffers.ICodedInputStream,Google.ProtocolBuffers.ExtensionRegistry,System.UInt32,System.String) */, __this, L_24, L_25, L_26, L_27);
		goto IL_0147;
	}

IL_00a2:
	{
		OrientationEvent_t158247624 * L_28 = __this->get_result_1();
		RuntimeObject* L_29 = ___input0;
		OrientationEvent_t158247624 * L_30 = __this->get_result_1();
		NullCheck(L_30);
		int64_t* L_31 = L_30->get_address_of_timestamp__5();
		NullCheck(L_29);
		bool L_32 = InterfaceFuncInvoker1< bool, int64_t* >::Invoke(2 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadInt64(System.Int64&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_29, (int64_t*)L_31);
		NullCheck(L_28);
		L_28->set_hasTimestamp_4(L_32);
		goto IL_0147;
	}

IL_00c3:
	{
		OrientationEvent_t158247624 * L_33 = __this->get_result_1();
		RuntimeObject* L_34 = ___input0;
		OrientationEvent_t158247624 * L_35 = __this->get_result_1();
		NullCheck(L_35);
		float* L_36 = L_35->get_address_of_x__8();
		NullCheck(L_34);
		bool L_37 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_34, (float*)L_36);
		NullCheck(L_33);
		L_33->set_hasX_7(L_37);
		goto IL_0147;
	}

IL_00e4:
	{
		OrientationEvent_t158247624 * L_38 = __this->get_result_1();
		RuntimeObject* L_39 = ___input0;
		OrientationEvent_t158247624 * L_40 = __this->get_result_1();
		NullCheck(L_40);
		float* L_41 = L_40->get_address_of_y__11();
		NullCheck(L_39);
		bool L_42 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_39, (float*)L_41);
		NullCheck(L_38);
		L_38->set_hasY_10(L_42);
		goto IL_0147;
	}

IL_0105:
	{
		OrientationEvent_t158247624 * L_43 = __this->get_result_1();
		RuntimeObject* L_44 = ___input0;
		OrientationEvent_t158247624 * L_45 = __this->get_result_1();
		NullCheck(L_45);
		float* L_46 = L_45->get_address_of_z__14();
		NullCheck(L_44);
		bool L_47 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_44, (float*)L_46);
		NullCheck(L_43);
		L_43->set_hasZ_13(L_47);
		goto IL_0147;
	}

IL_0126:
	{
		OrientationEvent_t158247624 * L_48 = __this->get_result_1();
		RuntimeObject* L_49 = ___input0;
		OrientationEvent_t158247624 * L_50 = __this->get_result_1();
		NullCheck(L_50);
		float* L_51 = L_50->get_address_of_w__17();
		NullCheck(L_49);
		bool L_52 = InterfaceFuncInvoker1< bool, float* >::Invoke(1 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadFloat(System.Single&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_49, (float*)L_51);
		NullCheck(L_48);
		L_48->set_hasW_16(L_52);
		goto IL_0147;
	}

IL_0147:
	{
		RuntimeObject* L_53 = ___input0;
		NullCheck(L_53);
		bool L_54 = InterfaceFuncInvoker2< bool, uint32_t*, String_t** >::Invoke(0 /* System.Boolean Google.ProtocolBuffers.ICodedInputStream::ReadTag(System.UInt32&,System.String&) */, ICodedInputStream_t3221112298_il2cpp_TypeInfo_var, L_53, (uint32_t*)(&V_0), (String_t**)(&V_1));
		if (L_54)
		{
			goto IL_000c;
		}
	}
	{
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasTimestamp()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasTimestamp_m1547058296 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasTimestamp_4();
		return L_1;
	}
}
// System.Int64 proto.PhoneEvent/Types/OrientationEvent/Builder::get_Timestamp()
extern "C" IL2CPP_METHOD_ATTR int64_t Builder_get_Timestamp_m31994274 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		int64_t L_1 = OrientationEvent_get_Timestamp_m723854610(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Timestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Timestamp_m1393789818 (Builder_t2279437287 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		int64_t L_0 = ___value0;
		Builder_SetTimestamp_m3405041319(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetTimestamp(System.Int64)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetTimestamp_m3405041319 (Builder_t2279437287 * __this, int64_t ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)1);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		int64_t L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_timestamp__5(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearTimestamp()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_ClearTimestamp_m367876833 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasTimestamp_4((bool)0);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_timestamp__5((((int64_t)((int64_t)0))));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasX()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasX_m634003612 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasX_7();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent/Builder::get_X()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_X_m2604638198 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = OrientationEvent_get_X_m884120132(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_X(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_X_m676748554 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetX_m1896712895(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetX(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetX_m1896712895 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasX_7((bool)1);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_x__8(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearX()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_ClearX_m1377224253 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasX_7((bool)0);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_x__8((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasY()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasY_m2972655772 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasY_10();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent/Builder::get_Y()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_Y_m265986038 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = OrientationEvent_get_Y_m3222772292(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Y(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Y_m133127197 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetY_m1896677292(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetY(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetY_m1896677292 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasY_10((bool)1);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_y__11(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearY()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_ClearY_m2943308194 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasY_10((bool)0);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_y__11((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasZ()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasZ_m1016340636 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasZ_13();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent/Builder::get_Z()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_Z_m2222301174 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = OrientationEvent_get_Z_m501783108(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_Z(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_Z_m945314922 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetZ_m1897165581(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetZ(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetZ_m1897165581 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasZ_13((bool)1);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_z__14(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearZ()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_ClearZ_m2540023667 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasZ_13((bool)0);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_z__14((0.0f));
		return __this;
	}
}
// System.Boolean proto.PhoneEvent/Types/OrientationEvent/Builder::get_HasW()
extern "C" IL2CPP_METHOD_ATTR bool Builder_get_HasW_m2590318748 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		bool L_1 = L_0->get_hasW_16();
		return L_1;
	}
}
// System.Single proto.PhoneEvent/Types/OrientationEvent/Builder::get_W()
extern "C" IL2CPP_METHOD_ATTR float Builder_get_W_m2177671158 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		float L_1 = OrientationEvent_get_W_m546413124(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void proto.PhoneEvent/Types/OrientationEvent/Builder::set_W(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Builder_set_W_m2009947325 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		Builder_SetW_m1896757142(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::SetW(System.Single)
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_SetW_m1896757142 (Builder_t2279437287 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasW_16((bool)1);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		float L_2 = ___value0;
		NullCheck(L_1);
		L_1->set_w__17(L_2);
		return __this;
	}
}
// proto.PhoneEvent/Types/OrientationEvent/Builder proto.PhoneEvent/Types/OrientationEvent/Builder::ClearW()
extern "C" IL2CPP_METHOD_ATTR Builder_t2279437287 * Builder_ClearW_m1424278420 (Builder_t2279437287 * __this, const RuntimeMethod* method)
{
	{
		Builder_PrepareBuilder_m147557039(__this, /*hidden argument*/NULL);
		OrientationEvent_t158247624 * L_0 = __this->get_result_1();
		NullCheck(L_0);
		L_0->set_hasW_16((bool)0);
		OrientationEvent_t158247624 * L_1 = __this->get_result_1();
		NullCheck(L_1);
		L_1->set_w__17((0.0f));
		return __this;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void proto.Proto.PhoneEvent::.cctor()
extern "C" IL2CPP_METHOD_ATTR void PhoneEvent__cctor_m3021104822 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PhoneEvent__cctor_m3021104822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((PhoneEvent_t3448566392_StaticFields*)il2cpp_codegen_static_fields_for(PhoneEvent_t3448566392_il2cpp_TypeInfo_var))->set_Descriptor_0(NULL);
		return;
	}
}
// System.Void proto.Proto.PhoneEvent::RegisterAllExtensions(Google.ProtocolBuffers.ExtensionRegistry)
extern "C" IL2CPP_METHOD_ATTR void PhoneEvent_RegisterAllExtensions_m1978679754 (RuntimeObject * __this /* static, unused */, ExtensionRegistry_t4271428238 * ___registry0, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
