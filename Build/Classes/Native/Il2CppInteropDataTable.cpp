﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"









extern "C" void Context_t1744531130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t1744531130_0_0_0;
extern "C" void Escape_t3294788190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t3294788190_0_0_0;
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t2148130204_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t682969308();
extern const RuntimeType AppDomainInitializer_t682969308_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t2822380397();
extern const RuntimeType Swapper_t2822380397_0_0_0;
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t3123975638_0_0_0;
extern "C" void Slot_t3975888750_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t3975888750_0_0_0;
extern "C" void Slot_t384495010_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t384495010_0_0_0;
extern "C" void Enum_t4135868527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t4135868527_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t714865915();
extern const RuntimeType ReadDelegate_t714865915_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4270993571();
extern const RuntimeType WriteDelegate_t4270993571_0_0_0;
extern "C" void MonoIOStat_t592533987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t592533987_0_0_0;
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t3694469084_0_0_0;
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t287865710_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t2723150157_0_0_0;
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t2325775114_0_0_0;
extern "C" void MonoResource_t4103430009_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoResource_t4103430009_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoResource_t4103430009_0_0_0;
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoWin32Resource_t1904229483_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoWin32Resource_t1904229483_0_0_0;
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RefEmitPermissionSet_t484390987_0_0_0;
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t346866618_0_0_0;
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t1248819020_0_0_0;
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t3087356066_0_0_0;
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1461694466_0_0_0;
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t51292791_0_0_0;
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t2872965302_0_0_0;
extern "C" void ProcessMessageRes_t3710547145_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ProcessMessageRes_t3710547145_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ProcessMessageRes_t3710547145_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ProcessMessageRes_t3710547145_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t387175271();
extern const RuntimeType CrossContextDelegate_t387175271_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t3280319253();
extern const RuntimeType CallbackHandler_t3280319253_0_0_0;
extern "C" void SerializationEntry_t648286436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t648286436_0_0_0;
extern "C" void StreamingContext_t3711869237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t3711869237_0_0_0;
extern "C" void DSAParameters_t1885824122_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t1885824122_0_0_0;
extern "C" void RSAParameters_t1728406613_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t1728406613_0_0_0;
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t1422462475_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1006689297();
extern const RuntimeType ThreadStart_t1006689297_0_0_0;
extern "C" void ValueType_t3640485471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t3640485471_0_0_0;
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t133602714_0_0_0;
extern "C" void IntStack_t2189327687_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t2189327687_0_0_0;
extern "C" void Interval_t1802865632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t1802865632_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1722821004();
extern const RuntimeType CostDelegate_t1722821004_0_0_0;
extern "C" void UriScheme_t722425697_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t722425697_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t1264377477();
extern const RuntimeType Action_t1264377477_0_0_0;
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t3046754366_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3588208630();
extern const RuntimeType LogCallback_t3588208630_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t4104246196();
extern const RuntimeType LowMemoryCallback_t4104246196_0_0_0;
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t1445031843_0_0_0;
extern "C" void OrderBlock_t1585977831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void OrderBlock_t1585977831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType OrderBlock_t1585977831_0_0_0;
extern "C" void Coroutine_t3829159415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t3829159415_0_0_0;
extern "C" void CullingGroup_t2096318768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t2096318768_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2136737110();
extern const RuntimeType StateChanged_t2136737110_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044();
extern const RuntimeType DisplaysUpdatedDelegate_t51287044_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t3245792599();
extern const RuntimeType UnityAction_t3245792599_0_0_0;
extern "C" void PlayerLoopSystem_t105772105_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayerLoopSystem_t105772105_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayerLoopSystem_t105772105_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayerLoopSystem_t105772105_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdateFunction_t377278577();
extern const RuntimeType UpdateFunction_t377278577_0_0_0;
extern "C" void PlayerLoopSystemInternal_t2185485283_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayerLoopSystemInternal_t2185485283_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayerLoopSystemInternal_t2185485283_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayerLoopSystemInternal_t2185485283_0_0_0;
extern "C" void SpriteBone_t303320096_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteBone_t303320096_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteBone_t303320096_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteBone_t303320096_0_0_0;
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t547604379_0_0_0;
extern "C" void Gradient_t3067099924_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t3067099924_0_0_0;
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Internal_DrawTextureArguments_t1705718261_0_0_0;
extern "C" void Object_t631007953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t631007953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t631007953_0_0_0;
extern "C" void PlayableBinding_t354260709_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t354260709_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t354260709_0_0_0;
extern "C" void DelegatePInvokeWrapper_CreateOutputMethod_t2301811773();
extern const RuntimeType CreateOutputMethod_t2301811773_0_0_0;
extern "C" void RectOffset_t1369453676_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t1369453676_0_0_0;
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t3109103591_0_0_0;
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t2528358522_0_0_0;
extern "C" void HitInfo_t3229609740_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t3229609740_0_0_0;
extern "C" void TrackedReference_t1199777556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t1199777556_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279();
extern const RuntimeType RequestAtlasCallback_t3100554279_0_0_0;
extern "C" void WorkRequest_t1354518612_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t1354518612_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t1354518612_0_0_0;
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t1699091251_0_0_0;
extern "C" void YieldInstruction_t403091072_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t403091072_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1677636661();
extern const RuntimeType PCMReaderCallback_t1677636661_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452();
extern const RuntimeType PCMSetPositionCallback_t1059417452_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874();
extern const RuntimeType AudioConfigurationChangeHandler_t2089929874_0_0_0;
extern "C" void DelegatePInvokeWrapper_ConsumeSampleFramesNativeFunction_t1497769677();
extern const RuntimeType ConsumeSampleFramesNativeFunction_t1497769677_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454();
extern const RuntimeType FontTextureRebuildCallback_t2467502454_0_0_0;
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t1351628751_0_0_0;
extern "C" void TextGenerator_t3211863866_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t3211863866_0_0_0;
extern "C" void Subsystem_t89723475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Subsystem_t89723475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Subsystem_t89723475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Subsystem_t89723475_0_0_0;
extern "C" void SubsystemDescriptorBase_t2374447182_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SubsystemDescriptorBase_t2374447182_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SubsystemDescriptorBase_t2374447182_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SubsystemDescriptorBase_t2374447182_0_0_0;
extern "C" void FrameReceivedEventArgs_t2588080103_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FrameReceivedEventArgs_t2588080103_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FrameReceivedEventArgs_t2588080103_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FrameReceivedEventArgs_t2588080103_0_0_0;
extern "C" void PlaneAddedEventArgs_t2550175725_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlaneAddedEventArgs_t2550175725_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlaneAddedEventArgs_t2550175725_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlaneAddedEventArgs_t2550175725_0_0_0;
extern "C" void PlaneRemovedEventArgs_t1567129782_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlaneRemovedEventArgs_t1567129782_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlaneRemovedEventArgs_t1567129782_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlaneRemovedEventArgs_t1567129782_0_0_0;
extern "C" void PlaneUpdatedEventArgs_t349485851_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlaneUpdatedEventArgs_t349485851_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlaneUpdatedEventArgs_t349485851_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlaneUpdatedEventArgs_t349485851_0_0_0;
extern "C" void PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PointCloudUpdatedEventArgs_t3436657348_0_0_0;
extern "C" void SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SessionTrackingStateChangedEventArgs_t2343035655_0_0_0;
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t1536042487_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnOverrideControllerDirtyCallback_t1307045488();
extern const RuntimeType OnOverrideControllerDirtyCallback_t1307045488_0_0_0;
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t2534804151_0_0_0;
extern "C" void HumanBone_t2465339518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t2465339518_0_0_0;
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t4134054672_0_0_0;
extern "C" void GcAchievementData_t675222246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t675222246_0_0_0;
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t643925653_0_0_0;
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t4132273028_0_0_0;
extern "C" void GcScoreData_t2125309831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t2125309831_0_0_0;
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t2719720026_0_0_0;
extern "C" void Event_t2956885303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t2956885303_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3146511083();
extern const RuntimeType WindowFunction_t3146511083_0_0_0;
extern "C" void GUIContent_t3050628031_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t3050628031_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295();
extern const RuntimeType SkinChangedDelegate_t1143955295_0_0_0;
extern "C" void GUIStyle_t3956901511_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t3956901511_0_0_0;
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t1397964415_0_0_0;
extern "C" void EmitParams_t2216423628_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmitParams_t2216423628_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmitParams_t2216423628_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EmitParams_t2216423628_0_0_0;
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ContactFilter2D_t3805203441_0_0_0;
extern "C" void Collision_t4262080450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t4262080450_0_0_0;
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t240592346_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3309123499();
extern const RuntimeType WillRenderCanvases_t3309123499_0_0_0;
extern "C" void DelegatePInvokeWrapper_SessionStateChanged_t3163629820();
extern const RuntimeType SessionStateChanged_t3163629820_0_0_0;
extern "C" void RemoteConfigSettings_t1247263429_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RemoteConfigSettings_t1247263429_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RemoteConfigSettings_t1247263429_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RemoteConfigSettings_t1247263429_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393();
extern const RuntimeType UpdatedEventHandler_t1027848393_0_0_0;
extern "C" void CertificateHandler_t2739891000_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CertificateHandler_t2739891000_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CertificateHandler_t2739891000_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CertificateHandler_t2739891000_0_0_0;
extern "C" void RaycastResult_t3360306849_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t3360306849_0_0_0;
extern "C" void ColorTween_t809614380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t809614380_0_0_0;
extern "C" void FloatTween_t1274330004_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t1274330004_0_0_0;
extern "C" void Resources_t1597885468_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t1597885468_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t2355412304();
extern const RuntimeType OnValidateInput_t2355412304_0_0_0;
extern "C" void Navigation_t3049316579_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t3049316579_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetRayIntersectionAllCallback_t3913627115();
extern const RuntimeType GetRayIntersectionAllCallback_t3913627115_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetRayIntersectionAllNonAllocCallback_t2311174851();
extern const RuntimeType GetRayIntersectionAllNonAllocCallback_t2311174851_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetRaycastNonAllocCallback_t3841783507();
extern const RuntimeType GetRaycastNonAllocCallback_t3841783507_0_0_0;
extern "C" void DelegatePInvokeWrapper_Raycast2DCallback_t768590915();
extern const RuntimeType Raycast2DCallback_t768590915_0_0_0;
extern "C" void DelegatePInvokeWrapper_Raycast3DCallback_t701940803();
extern const RuntimeType Raycast3DCallback_t701940803_0_0_0;
extern "C" void DelegatePInvokeWrapper_RaycastAllCallback_t1884415901();
extern const RuntimeType RaycastAllCallback_t1884415901_0_0_0;
extern "C" void SpriteState_t1362986479_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t1362986479_0_0_0;
extern "C" void ExtensionIntPair_t1343559306_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ExtensionIntPair_t1343559306_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ExtensionIntPair_t1343559306_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ExtensionIntPair_t1343559306_0_0_0;
extern "C" void ColorTween_t378116136_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t378116136_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t378116136_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t378116136_0_0_0;
extern "C" void FloatTween_t3783157226_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t3783157226_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t3783157226_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t3783157226_0_0_0;
extern "C" void FontAssetCreationSettings_t359369028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FontAssetCreationSettings_t359369028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FontAssetCreationSettings_t359369028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FontAssetCreationSettings_t359369028_0_0_0;
extern "C" void MaterialReference_t1952344632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MaterialReference_t1952344632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MaterialReference_t1952344632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MaterialReference_t1952344632_0_0_0;
extern "C" void SpriteData_t3048397587_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteData_t3048397587_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteData_t3048397587_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteData_t3048397587_0_0_0;
extern "C" void TMP_CharacterInfo_t3185626797_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_CharacterInfo_t3185626797_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_CharacterInfo_t3185626797_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TMP_CharacterInfo_t3185626797_0_0_0;
extern "C" void Resources_t2155109485_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t2155109485_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t2155109485_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t2155109485_0_0_0;
extern "C" void TMP_FontWeights_t916301067_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_FontWeights_t916301067_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_FontWeights_t916301067_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TMP_FontWeights_t916301067_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t373909109();
extern const RuntimeType OnValidateInput_t373909109_0_0_0;
extern "C" void TMP_LinkInfo_t1092083476_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_LinkInfo_t1092083476_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_LinkInfo_t1092083476_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TMP_LinkInfo_t1092083476_0_0_0;
extern "C" void TMP_MeshInfo_t2771747634_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_MeshInfo_t2771747634_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_MeshInfo_t2771747634_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TMP_MeshInfo_t2771747634_0_0_0;
extern "C" void TMP_WordInfo_t3331066303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_WordInfo_t3331066303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_WordInfo_t3331066303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TMP_WordInfo_t3331066303_0_0_0;
extern "C" void WordWrapState_t341939652_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WordWrapState_t341939652_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WordWrapState_t341939652_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WordWrapState_t341939652_0_0_0;
extern "C" void EmulatorButtonEvent_t938855819_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmulatorButtonEvent_t938855819_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmulatorButtonEvent_t938855819_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EmulatorButtonEvent_t938855819_0_0_0;
extern "C" void EmulatorTouchEvent_t2277405062_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmulatorTouchEvent_t2277405062_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmulatorTouchEvent_t2277405062_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EmulatorTouchEvent_t2277405062_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnStateChangedEvent_t3148566047();
extern const RuntimeType OnStateChangedEvent_t3148566047_0_0_0;
extern "C" void FaceCameraData_t2570986833_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FaceCameraData_t2570986833_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FaceCameraData_t2570986833_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FaceCameraData_t2570986833_0_0_0;
extern "C" void ControllerDisplayState_t972313457_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerDisplayState_t972313457_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerDisplayState_t972313457_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerDisplayState_t972313457_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnRecenterEvent_t1755824842();
extern const RuntimeType OnRecenterEvent_t1755824842_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnSafetyRegionEvent_t980662704();
extern const RuntimeType OnSafetyRegionEvent_t980662704_0_0_0;
extern "C" void DelegatePInvokeWrapper_EditTextCallback_t1702213000();
extern const RuntimeType EditTextCallback_t1702213000_0_0_0;
extern "C" void DelegatePInvokeWrapper_ErrorCallback_t2310212740();
extern const RuntimeType ErrorCallback_t2310212740_0_0_0;
extern "C" void DelegatePInvokeWrapper_KeyboardCallback_t3330588312();
extern const RuntimeType KeyboardCallback_t3330588312_0_0_0;
extern "C" void DelegatePInvokeWrapper_StandardCallback_t3095007891();
extern const RuntimeType StandardCallback_t3095007891_0_0_0;
extern "C" void DelegatePInvokeWrapper_GetPointForDistanceDelegate_t92913150();
extern const RuntimeType GetPointForDistanceDelegate_t92913150_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnExceptionCallback_t1696428116();
extern const RuntimeType OnExceptionCallback_t1696428116_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnVideoEventCallback_t2376626694();
extern const RuntimeType OnVideoEventCallback_t2376626694_0_0_0;
extern "C" void Settings_t3016786575_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3016786575_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3016786575_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3016786575_0_0_0;
extern "C" void BloomSettings_t2599855122_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void BloomSettings_t2599855122_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void BloomSettings_t2599855122_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType BloomSettings_t2599855122_0_0_0;
extern "C" void LensDirtSettings_t3693422705_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LensDirtSettings_t3693422705_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LensDirtSettings_t3693422705_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LensDirtSettings_t3693422705_0_0_0;
extern "C" void Settings_t181254429_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t181254429_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t181254429_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t181254429_0_0_0;
extern "C" void Settings_t2111398455_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t2111398455_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t2111398455_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t2111398455_0_0_0;
extern "C" void CurvesSettings_t2830270037_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CurvesSettings_t2830270037_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CurvesSettings_t2830270037_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CurvesSettings_t2830270037_0_0_0;
extern "C" void Settings_t451872061_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t451872061_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t451872061_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t451872061_0_0_0;
extern "C" void Settings_t2195468135_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t2195468135_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t2195468135_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t2195468135_0_0_0;
extern "C" void Settings_t2874244444_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t2874244444_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t2874244444_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t2874244444_0_0_0;
extern "C" void Settings_t224798599_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t224798599_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t224798599_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t224798599_0_0_0;
extern "C" void Settings_t4123292438_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t4123292438_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t4123292438_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t4123292438_0_0_0;
extern "C" void Frame_t295908221_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Frame_t295908221_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Frame_t295908221_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Frame_t295908221_0_0_0;
extern "C" void ReflectionSettings_t282755190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ReflectionSettings_t282755190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ReflectionSettings_t282755190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ReflectionSettings_t282755190_0_0_0;
extern "C" void Settings_t1995791524_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t1995791524_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t1995791524_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t1995791524_0_0_0;
extern "C" void Settings_t3006579223_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t3006579223_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t3006579223_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t3006579223_0_0_0;
extern "C" void Settings_t1354494600_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t1354494600_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t1354494600_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t1354494600_0_0_0;
extern "C" void Settings_t1032325577_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t1032325577_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t1032325577_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t1032325577_0_0_0;
extern "C" void BokehTextureSettings_t3943002634_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void BokehTextureSettings_t3943002634_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void BokehTextureSettings_t3943002634_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType BokehTextureSettings_t3943002634_0_0_0;
extern "C" void FocusSettings_t1261455774_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FocusSettings_t1261455774_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FocusSettings_t1261455774_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FocusSettings_t1261455774_0_0_0;
extern "C" void GlobalSettings_t956974295_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GlobalSettings_t956974295_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GlobalSettings_t956974295_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GlobalSettings_t956974295_0_0_0;
extern "C" void QualitySettings_t1379802392_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void QualitySettings_t1379802392_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void QualitySettings_t1379802392_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType QualitySettings_t1379802392_0_0_0;
extern "C" void ChromaticAberrationSettings_t3807027201_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ChromaticAberrationSettings_t3807027201_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ChromaticAberrationSettings_t3807027201_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ChromaticAberrationSettings_t3807027201_0_0_0;
extern "C" void DistortionSettings_t3193728992_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DistortionSettings_t3193728992_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DistortionSettings_t3193728992_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DistortionSettings_t3193728992_0_0_0;
extern "C" void VignetteSettings_t2342083791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void VignetteSettings_t2342083791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void VignetteSettings_t2342083791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType VignetteSettings_t2342083791_0_0_0;
extern "C" void Frame_t3511111948_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Frame_t3511111948_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Frame_t3511111948_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Frame_t3511111948_0_0_0;
extern "C" void PredicationSettings_t4116788586_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PredicationSettings_t4116788586_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PredicationSettings_t4116788586_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PredicationSettings_t4116788586_0_0_0;
extern "C" void QualitySettings_t1869387270_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void QualitySettings_t1869387270_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void QualitySettings_t1869387270_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType QualitySettings_t1869387270_0_0_0;
extern "C" void TemporalSettings_t2985116592_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TemporalSettings_t2985116592_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TemporalSettings_t2985116592_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TemporalSettings_t2985116592_0_0_0;
extern "C" void ReflectionSettings_t499321483_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ReflectionSettings_t499321483_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ReflectionSettings_t499321483_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ReflectionSettings_t499321483_0_0_0;
extern "C" void SSRSettings_t899296535_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SSRSettings_t899296535_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SSRSettings_t899296535_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SSRSettings_t899296535_0_0_0;
extern "C" void DebugSettings_t54746148_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DebugSettings_t54746148_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DebugSettings_t54746148_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DebugSettings_t54746148_0_0_0;
extern "C" void ChannelMixerSettings_t491321292_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ChannelMixerSettings_t491321292_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ChannelMixerSettings_t491321292_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ChannelMixerSettings_t491321292_0_0_0;
extern "C" void ColorGradingSettings_t1923563914_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorGradingSettings_t1923563914_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorGradingSettings_t1923563914_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorGradingSettings_t1923563914_0_0_0;
extern "C" void CurvesSettings_t2932745847_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CurvesSettings_t2932745847_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CurvesSettings_t2932745847_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CurvesSettings_t2932745847_0_0_0;
extern "C" void EyeAdaptationSettings_t3234830401_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EyeAdaptationSettings_t3234830401_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EyeAdaptationSettings_t3234830401_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EyeAdaptationSettings_t3234830401_0_0_0;
extern "C" void LUTSettings_t2339616782_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LUTSettings_t2339616782_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LUTSettings_t2339616782_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType LUTSettings_t2339616782_0_0_0;
extern "C" void TonemappingSettings_t1161324624_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TonemappingSettings_t1161324624_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TonemappingSettings_t1161324624_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TonemappingSettings_t1161324624_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[189] = 
{
	{ NULL, Context_t1744531130_marshal_pinvoke, Context_t1744531130_marshal_pinvoke_back, Context_t1744531130_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1744531130_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t3294788190_marshal_pinvoke, Escape_t3294788190_marshal_pinvoke_back, Escape_t3294788190_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t3294788190_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t2148130204_marshal_pinvoke, PreviousInfo_t2148130204_marshal_pinvoke_back, PreviousInfo_t2148130204_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t2148130204_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t682969308, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t682969308_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t2822380397, NULL, NULL, NULL, NULL, NULL, &Swapper_t2822380397_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3123975638_marshal_pinvoke, DictionaryEntry_t3123975638_marshal_pinvoke_back, DictionaryEntry_t3123975638_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3123975638_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t3975888750_marshal_pinvoke, Slot_t3975888750_marshal_pinvoke_back, Slot_t3975888750_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3975888750_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t384495010_marshal_pinvoke, Slot_t384495010_marshal_pinvoke_back, Slot_t384495010_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t384495010_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t4135868527_marshal_pinvoke, Enum_t4135868527_marshal_pinvoke_back, Enum_t4135868527_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t4135868527_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t714865915, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t714865915_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4270993571, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4270993571_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t592533987_marshal_pinvoke, MonoIOStat_t592533987_marshal_pinvoke_back, MonoIOStat_t592533987_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t592533987_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3694469084_marshal_pinvoke, MonoEnumInfo_t3694469084_marshal_pinvoke_back, MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3694469084_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t287865710_marshal_pinvoke, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t287865710_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2723150157_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t2325775114_marshal_pinvoke, ILTokenInfo_t2325775114_marshal_pinvoke_back, ILTokenInfo_t2325775114_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t2325775114_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoResource_t4103430009_marshal_pinvoke, MonoResource_t4103430009_marshal_pinvoke_back, MonoResource_t4103430009_marshal_pinvoke_cleanup, NULL, NULL, &MonoResource_t4103430009_0_0_0 } /* System.Reflection.Emit.MonoResource */,
	{ NULL, MonoWin32Resource_t1904229483_marshal_pinvoke, MonoWin32Resource_t1904229483_marshal_pinvoke_back, MonoWin32Resource_t1904229483_marshal_pinvoke_cleanup, NULL, NULL, &MonoWin32Resource_t1904229483_0_0_0 } /* System.Reflection.Emit.MonoWin32Resource */,
	{ NULL, RefEmitPermissionSet_t484390987_marshal_pinvoke, RefEmitPermissionSet_t484390987_marshal_pinvoke_back, RefEmitPermissionSet_t484390987_marshal_pinvoke_cleanup, NULL, NULL, &RefEmitPermissionSet_t484390987_0_0_0 } /* System.Reflection.Emit.RefEmitPermissionSet */,
	{ NULL, MonoEventInfo_t346866618_marshal_pinvoke, MonoEventInfo_t346866618_marshal_pinvoke_back, MonoEventInfo_t346866618_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t346866618_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1248819020_marshal_pinvoke, MonoMethodInfo_t1248819020_marshal_pinvoke_back, MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1248819020_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3087356066_marshal_pinvoke, MonoPropertyInfo_t3087356066_marshal_pinvoke_back, MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3087356066_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1461694466_marshal_pinvoke, ParameterModifier_t1461694466_marshal_pinvoke_back, ParameterModifier_t1461694466_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1461694466_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t51292791_marshal_pinvoke, ResourceCacheItem_t51292791_marshal_pinvoke_back, ResourceCacheItem_t51292791_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t51292791_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t2872965302_marshal_pinvoke, ResourceInfo_t2872965302_marshal_pinvoke_back, ResourceInfo_t2872965302_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t2872965302_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ NULL, ProcessMessageRes_t3710547145_marshal_pinvoke, ProcessMessageRes_t3710547145_marshal_pinvoke_back, ProcessMessageRes_t3710547145_marshal_pinvoke_cleanup, NULL, NULL, &ProcessMessageRes_t3710547145_0_0_0 } /* System.Runtime.Remoting.Channels.CrossAppDomainSink/ProcessMessageRes */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t387175271, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t387175271_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t3280319253, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t3280319253_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t648286436_marshal_pinvoke, SerializationEntry_t648286436_marshal_pinvoke_back, SerializationEntry_t648286436_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t648286436_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t3711869237_marshal_pinvoke, StreamingContext_t3711869237_marshal_pinvoke_back, StreamingContext_t3711869237_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t3711869237_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1885824122_marshal_pinvoke, DSAParameters_t1885824122_marshal_pinvoke_back, DSAParameters_t1885824122_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1885824122_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1728406613_marshal_pinvoke, RSAParameters_t1728406613_marshal_pinvoke_back, RSAParameters_t1728406613_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1728406613_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t1422462475_marshal_pinvoke, SecurityFrame_t1422462475_marshal_pinvoke_back, SecurityFrame_t1422462475_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1422462475_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t1006689297, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1006689297_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3640485471_marshal_pinvoke, ValueType_t3640485471_marshal_pinvoke_back, ValueType_t3640485471_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3640485471_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t133602714_marshal_pinvoke, X509ChainStatus_t133602714_marshal_pinvoke_back, X509ChainStatus_t133602714_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t133602714_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t2189327687_marshal_pinvoke, IntStack_t2189327687_marshal_pinvoke_back, IntStack_t2189327687_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t2189327687_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t1802865632_marshal_pinvoke, Interval_t1802865632_marshal_pinvoke_back, Interval_t1802865632_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t1802865632_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t1722821004, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t1722821004_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t722425697_marshal_pinvoke, UriScheme_t722425697_marshal_pinvoke_back, UriScheme_t722425697_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t722425697_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t1264377477, NULL, NULL, NULL, NULL, NULL, &Action_t1264377477_0_0_0 } /* System.Action */,
	{ NULL, AnimationCurve_t3046754366_marshal_pinvoke, AnimationCurve_t3046754366_marshal_pinvoke_back, AnimationCurve_t3046754366_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3046754366_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ DelegatePInvokeWrapper_LogCallback_t3588208630, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3588208630_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t4104246196, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t4104246196_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AsyncOperation_t1445031843_marshal_pinvoke, AsyncOperation_t1445031843_marshal_pinvoke_back, AsyncOperation_t1445031843_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1445031843_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ NULL, OrderBlock_t1585977831_marshal_pinvoke, OrderBlock_t1585977831_marshal_pinvoke_back, OrderBlock_t1585977831_marshal_pinvoke_cleanup, NULL, NULL, &OrderBlock_t1585977831_0_0_0 } /* UnityEngine.BeforeRenderHelper/OrderBlock */,
	{ NULL, Coroutine_t3829159415_marshal_pinvoke, Coroutine_t3829159415_marshal_pinvoke_back, Coroutine_t3829159415_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3829159415_0_0_0 } /* UnityEngine.Coroutine */,
	{ NULL, CullingGroup_t2096318768_marshal_pinvoke, CullingGroup_t2096318768_marshal_pinvoke_back, CullingGroup_t2096318768_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2096318768_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2136737110, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2136737110_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t51287044_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ DelegatePInvokeWrapper_UnityAction_t3245792599, NULL, NULL, NULL, NULL, NULL, &UnityAction_t3245792599_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, PlayerLoopSystem_t105772105_marshal_pinvoke, PlayerLoopSystem_t105772105_marshal_pinvoke_back, PlayerLoopSystem_t105772105_marshal_pinvoke_cleanup, NULL, NULL, &PlayerLoopSystem_t105772105_0_0_0 } /* UnityEngine.Experimental.LowLevel.PlayerLoopSystem */,
	{ DelegatePInvokeWrapper_UpdateFunction_t377278577, NULL, NULL, NULL, NULL, NULL, &UpdateFunction_t377278577_0_0_0 } /* UnityEngine.Experimental.LowLevel.PlayerLoopSystem/UpdateFunction */,
	{ NULL, PlayerLoopSystemInternal_t2185485283_marshal_pinvoke, PlayerLoopSystemInternal_t2185485283_marshal_pinvoke_back, PlayerLoopSystemInternal_t2185485283_marshal_pinvoke_cleanup, NULL, NULL, &PlayerLoopSystemInternal_t2185485283_0_0_0 } /* UnityEngine.Experimental.LowLevel.PlayerLoopSystemInternal */,
	{ NULL, SpriteBone_t303320096_marshal_pinvoke, SpriteBone_t303320096_marshal_pinvoke_back, SpriteBone_t303320096_marshal_pinvoke_cleanup, NULL, NULL, &SpriteBone_t303320096_0_0_0 } /* UnityEngine.Experimental.U2D.SpriteBone */,
	{ NULL, FailedToLoadScriptObject_t547604379_marshal_pinvoke, FailedToLoadScriptObject_t547604379_marshal_pinvoke_back, FailedToLoadScriptObject_t547604379_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t547604379_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ NULL, Gradient_t3067099924_marshal_pinvoke, Gradient_t3067099924_marshal_pinvoke_back, Gradient_t3067099924_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3067099924_0_0_0 } /* UnityEngine.Gradient */,
	{ NULL, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_back, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_cleanup, NULL, NULL, &Internal_DrawTextureArguments_t1705718261_0_0_0 } /* UnityEngine.Internal_DrawTextureArguments */,
	{ NULL, Object_t631007953_marshal_pinvoke, Object_t631007953_marshal_pinvoke_back, Object_t631007953_marshal_pinvoke_cleanup, NULL, NULL, &Object_t631007953_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t354260709_marshal_pinvoke, PlayableBinding_t354260709_marshal_pinvoke_back, PlayableBinding_t354260709_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t354260709_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ DelegatePInvokeWrapper_CreateOutputMethod_t2301811773, NULL, NULL, NULL, NULL, NULL, &CreateOutputMethod_t2301811773_0_0_0 } /* UnityEngine.Playables.PlayableBinding/CreateOutputMethod */,
	{ NULL, RectOffset_t1369453676_marshal_pinvoke, RectOffset_t1369453676_marshal_pinvoke_back, RectOffset_t1369453676_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1369453676_0_0_0 } /* UnityEngine.RectOffset */,
	{ NULL, ResourceRequest_t3109103591_marshal_pinvoke, ResourceRequest_t3109103591_marshal_pinvoke_back, ResourceRequest_t3109103591_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3109103591_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t2528358522_marshal_pinvoke, ScriptableObject_t2528358522_marshal_pinvoke_back, ScriptableObject_t2528358522_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t2528358522_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t3229609740_marshal_pinvoke, HitInfo_t3229609740_marshal_pinvoke_back, HitInfo_t3229609740_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t3229609740_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, TrackedReference_t1199777556_marshal_pinvoke, TrackedReference_t1199777556_marshal_pinvoke_back, TrackedReference_t1199777556_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1199777556_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t3100554279, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t3100554279_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t1354518612_marshal_pinvoke, WorkRequest_t1354518612_marshal_pinvoke_back, WorkRequest_t1354518612_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t1354518612_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t1699091251_marshal_pinvoke, WaitForSeconds_t1699091251_marshal_pinvoke_back, WaitForSeconds_t1699091251_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t1699091251_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t403091072_marshal_pinvoke, YieldInstruction_t403091072_marshal_pinvoke_back, YieldInstruction_t403091072_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t403091072_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1677636661, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1677636661_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1059417452_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2089929874_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_ConsumeSampleFramesNativeFunction_t1497769677, NULL, NULL, NULL, NULL, NULL, &ConsumeSampleFramesNativeFunction_t1497769677_0_0_0 } /* UnityEngine.Experimental.Audio.AudioSampleProvider/ConsumeSampleFramesNativeFunction */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2467502454_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, TextGenerationSettings_t1351628751_marshal_pinvoke, TextGenerationSettings_t1351628751_marshal_pinvoke_back, TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1351628751_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t3211863866_marshal_pinvoke, TextGenerator_t3211863866_marshal_pinvoke_back, TextGenerator_t3211863866_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t3211863866_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, Subsystem_t89723475_marshal_pinvoke, Subsystem_t89723475_marshal_pinvoke_back, Subsystem_t89723475_marshal_pinvoke_cleanup, NULL, NULL, &Subsystem_t89723475_0_0_0 } /* UnityEngine.Experimental.Subsystem */,
	{ NULL, SubsystemDescriptorBase_t2374447182_marshal_pinvoke, SubsystemDescriptorBase_t2374447182_marshal_pinvoke_back, SubsystemDescriptorBase_t2374447182_marshal_pinvoke_cleanup, NULL, NULL, &SubsystemDescriptorBase_t2374447182_0_0_0 } /* UnityEngine.Experimental.SubsystemDescriptorBase */,
	{ NULL, FrameReceivedEventArgs_t2588080103_marshal_pinvoke, FrameReceivedEventArgs_t2588080103_marshal_pinvoke_back, FrameReceivedEventArgs_t2588080103_marshal_pinvoke_cleanup, NULL, NULL, &FrameReceivedEventArgs_t2588080103_0_0_0 } /* UnityEngine.Experimental.XR.FrameReceivedEventArgs */,
	{ NULL, PlaneAddedEventArgs_t2550175725_marshal_pinvoke, PlaneAddedEventArgs_t2550175725_marshal_pinvoke_back, PlaneAddedEventArgs_t2550175725_marshal_pinvoke_cleanup, NULL, NULL, &PlaneAddedEventArgs_t2550175725_0_0_0 } /* UnityEngine.Experimental.XR.PlaneAddedEventArgs */,
	{ NULL, PlaneRemovedEventArgs_t1567129782_marshal_pinvoke, PlaneRemovedEventArgs_t1567129782_marshal_pinvoke_back, PlaneRemovedEventArgs_t1567129782_marshal_pinvoke_cleanup, NULL, NULL, &PlaneRemovedEventArgs_t1567129782_0_0_0 } /* UnityEngine.Experimental.XR.PlaneRemovedEventArgs */,
	{ NULL, PlaneUpdatedEventArgs_t349485851_marshal_pinvoke, PlaneUpdatedEventArgs_t349485851_marshal_pinvoke_back, PlaneUpdatedEventArgs_t349485851_marshal_pinvoke_cleanup, NULL, NULL, &PlaneUpdatedEventArgs_t349485851_0_0_0 } /* UnityEngine.Experimental.XR.PlaneUpdatedEventArgs */,
	{ NULL, PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke, PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke_back, PointCloudUpdatedEventArgs_t3436657348_marshal_pinvoke_cleanup, NULL, NULL, &PointCloudUpdatedEventArgs_t3436657348_0_0_0 } /* UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs */,
	{ NULL, SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke, SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke_back, SessionTrackingStateChangedEventArgs_t2343035655_marshal_pinvoke_cleanup, NULL, NULL, &SessionTrackingStateChangedEventArgs_t2343035655_0_0_0 } /* UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs */,
	{ NULL, AnimationEvent_t1536042487_marshal_pinvoke, AnimationEvent_t1536042487_marshal_pinvoke_back, AnimationEvent_t1536042487_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t1536042487_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ DelegatePInvokeWrapper_OnOverrideControllerDirtyCallback_t1307045488, NULL, NULL, NULL, NULL, NULL, &OnOverrideControllerDirtyCallback_t1307045488_0_0_0 } /* UnityEngine.AnimatorOverrideController/OnOverrideControllerDirtyCallback */,
	{ NULL, AnimatorTransitionInfo_t2534804151_marshal_pinvoke, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2534804151_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ NULL, HumanBone_t2465339518_marshal_pinvoke, HumanBone_t2465339518_marshal_pinvoke_back, HumanBone_t2465339518_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t2465339518_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, SkeletonBone_t4134054672_marshal_pinvoke, SkeletonBone_t4134054672_marshal_pinvoke_back, SkeletonBone_t4134054672_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t4134054672_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t675222246_marshal_pinvoke, GcAchievementData_t675222246_marshal_pinvoke_back, GcAchievementData_t675222246_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t675222246_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t643925653_marshal_pinvoke, GcAchievementDescriptionData_t643925653_marshal_pinvoke_back, GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t643925653_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t4132273028_marshal_pinvoke, GcLeaderboard_t4132273028_marshal_pinvoke_back, GcLeaderboard_t4132273028_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t4132273028_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2125309831_marshal_pinvoke, GcScoreData_t2125309831_marshal_pinvoke_back, GcScoreData_t2125309831_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2125309831_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t2719720026_marshal_pinvoke, GcUserProfileData_t2719720026_marshal_pinvoke_back, GcUserProfileData_t2719720026_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t2719720026_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, Event_t2956885303_marshal_pinvoke, Event_t2956885303_marshal_pinvoke_back, Event_t2956885303_marshal_pinvoke_cleanup, NULL, NULL, &Event_t2956885303_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_WindowFunction_t3146511083, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3146511083_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t3050628031_marshal_pinvoke, GUIContent_t3050628031_marshal_pinvoke_back, GUIContent_t3050628031_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t3050628031_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t1143955295_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3956901511_marshal_pinvoke, GUIStyle_t3956901511_marshal_pinvoke_back, GUIStyle_t3956901511_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3956901511_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t1397964415_marshal_pinvoke, GUIStyleState_t1397964415_marshal_pinvoke_back, GUIStyleState_t1397964415_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t1397964415_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, EmitParams_t2216423628_marshal_pinvoke, EmitParams_t2216423628_marshal_pinvoke_back, EmitParams_t2216423628_marshal_pinvoke_cleanup, NULL, NULL, &EmitParams_t2216423628_0_0_0 } /* UnityEngine.ParticleSystem/EmitParams */,
	{ NULL, ContactFilter2D_t3805203441_marshal_pinvoke, ContactFilter2D_t3805203441_marshal_pinvoke_back, ContactFilter2D_t3805203441_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t3805203441_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, Collision_t4262080450_marshal_pinvoke, Collision_t4262080450_marshal_pinvoke_back, Collision_t4262080450_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t4262080450_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, ControllerColliderHit_t240592346_marshal_pinvoke, ControllerColliderHit_t240592346_marshal_pinvoke_back, ControllerColliderHit_t240592346_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t240592346_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3309123499, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3309123499_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ DelegatePInvokeWrapper_SessionStateChanged_t3163629820, NULL, NULL, NULL, NULL, NULL, &SessionStateChanged_t3163629820_0_0_0 } /* UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged */,
	{ NULL, RemoteConfigSettings_t1247263429_marshal_pinvoke, RemoteConfigSettings_t1247263429_marshal_pinvoke_back, RemoteConfigSettings_t1247263429_marshal_pinvoke_cleanup, NULL, NULL, &RemoteConfigSettings_t1247263429_0_0_0 } /* UnityEngine.RemoteConfigSettings */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1027848393_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, CertificateHandler_t2739891000_marshal_pinvoke, CertificateHandler_t2739891000_marshal_pinvoke_back, CertificateHandler_t2739891000_marshal_pinvoke_cleanup, NULL, NULL, &CertificateHandler_t2739891000_0_0_0 } /* UnityEngine.Networking.CertificateHandler */,
	{ NULL, RaycastResult_t3360306849_marshal_pinvoke, RaycastResult_t3360306849_marshal_pinvoke_back, RaycastResult_t3360306849_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t3360306849_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t809614380_marshal_pinvoke, ColorTween_t809614380_marshal_pinvoke_back, ColorTween_t809614380_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t809614380_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1274330004_marshal_pinvoke, FloatTween_t1274330004_marshal_pinvoke_back, FloatTween_t1274330004_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1274330004_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t1597885468_marshal_pinvoke, Resources_t1597885468_marshal_pinvoke_back, Resources_t1597885468_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t1597885468_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t2355412304, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t2355412304_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t3049316579_marshal_pinvoke, Navigation_t3049316579_marshal_pinvoke_back, Navigation_t3049316579_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t3049316579_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ DelegatePInvokeWrapper_GetRayIntersectionAllCallback_t3913627115, NULL, NULL, NULL, NULL, NULL, &GetRayIntersectionAllCallback_t3913627115_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback */,
	{ DelegatePInvokeWrapper_GetRayIntersectionAllNonAllocCallback_t2311174851, NULL, NULL, NULL, NULL, NULL, &GetRayIntersectionAllNonAllocCallback_t2311174851_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback */,
	{ DelegatePInvokeWrapper_GetRaycastNonAllocCallback_t3841783507, NULL, NULL, NULL, NULL, NULL, &GetRaycastNonAllocCallback_t3841783507_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback */,
	{ DelegatePInvokeWrapper_Raycast2DCallback_t768590915, NULL, NULL, NULL, NULL, NULL, &Raycast2DCallback_t768590915_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback */,
	{ DelegatePInvokeWrapper_Raycast3DCallback_t701940803, NULL, NULL, NULL, NULL, NULL, &Raycast3DCallback_t701940803_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback */,
	{ DelegatePInvokeWrapper_RaycastAllCallback_t1884415901, NULL, NULL, NULL, NULL, NULL, &RaycastAllCallback_t1884415901_0_0_0 } /* UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback */,
	{ NULL, SpriteState_t1362986479_marshal_pinvoke, SpriteState_t1362986479_marshal_pinvoke_back, SpriteState_t1362986479_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1362986479_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, ExtensionIntPair_t1343559306_marshal_pinvoke, ExtensionIntPair_t1343559306_marshal_pinvoke_back, ExtensionIntPair_t1343559306_marshal_pinvoke_cleanup, NULL, NULL, &ExtensionIntPair_t1343559306_0_0_0 } /* Google.ProtocolBuffers.ExtensionRegistry/ExtensionIntPair */,
	{ NULL, ColorTween_t378116136_marshal_pinvoke, ColorTween_t378116136_marshal_pinvoke_back, ColorTween_t378116136_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t378116136_0_0_0 } /* TMPro.ColorTween */,
	{ NULL, FloatTween_t3783157226_marshal_pinvoke, FloatTween_t3783157226_marshal_pinvoke_back, FloatTween_t3783157226_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t3783157226_0_0_0 } /* TMPro.FloatTween */,
	{ NULL, FontAssetCreationSettings_t359369028_marshal_pinvoke, FontAssetCreationSettings_t359369028_marshal_pinvoke_back, FontAssetCreationSettings_t359369028_marshal_pinvoke_cleanup, NULL, NULL, &FontAssetCreationSettings_t359369028_0_0_0 } /* TMPro.FontAssetCreationSettings */,
	{ NULL, MaterialReference_t1952344632_marshal_pinvoke, MaterialReference_t1952344632_marshal_pinvoke_back, MaterialReference_t1952344632_marshal_pinvoke_cleanup, NULL, NULL, &MaterialReference_t1952344632_0_0_0 } /* TMPro.MaterialReference */,
	{ NULL, SpriteData_t3048397587_marshal_pinvoke, SpriteData_t3048397587_marshal_pinvoke_back, SpriteData_t3048397587_marshal_pinvoke_cleanup, NULL, NULL, &SpriteData_t3048397587_0_0_0 } /* TMPro.SpriteAssetUtilities.TexturePacker/SpriteData */,
	{ NULL, TMP_CharacterInfo_t3185626797_marshal_pinvoke, TMP_CharacterInfo_t3185626797_marshal_pinvoke_back, TMP_CharacterInfo_t3185626797_marshal_pinvoke_cleanup, NULL, NULL, &TMP_CharacterInfo_t3185626797_0_0_0 } /* TMPro.TMP_CharacterInfo */,
	{ NULL, Resources_t2155109485_marshal_pinvoke, Resources_t2155109485_marshal_pinvoke_back, Resources_t2155109485_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t2155109485_0_0_0 } /* TMPro.TMP_DefaultControls/Resources */,
	{ NULL, TMP_FontWeights_t916301067_marshal_pinvoke, TMP_FontWeights_t916301067_marshal_pinvoke_back, TMP_FontWeights_t916301067_marshal_pinvoke_cleanup, NULL, NULL, &TMP_FontWeights_t916301067_0_0_0 } /* TMPro.TMP_FontWeights */,
	{ DelegatePInvokeWrapper_OnValidateInput_t373909109, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t373909109_0_0_0 } /* TMPro.TMP_InputField/OnValidateInput */,
	{ NULL, TMP_LinkInfo_t1092083476_marshal_pinvoke, TMP_LinkInfo_t1092083476_marshal_pinvoke_back, TMP_LinkInfo_t1092083476_marshal_pinvoke_cleanup, NULL, NULL, &TMP_LinkInfo_t1092083476_0_0_0 } /* TMPro.TMP_LinkInfo */,
	{ NULL, TMP_MeshInfo_t2771747634_marshal_pinvoke, TMP_MeshInfo_t2771747634_marshal_pinvoke_back, TMP_MeshInfo_t2771747634_marshal_pinvoke_cleanup, NULL, NULL, &TMP_MeshInfo_t2771747634_0_0_0 } /* TMPro.TMP_MeshInfo */,
	{ NULL, TMP_WordInfo_t3331066303_marshal_pinvoke, TMP_WordInfo_t3331066303_marshal_pinvoke_back, TMP_WordInfo_t3331066303_marshal_pinvoke_cleanup, NULL, NULL, &TMP_WordInfo_t3331066303_0_0_0 } /* TMPro.TMP_WordInfo */,
	{ NULL, WordWrapState_t341939652_marshal_pinvoke, WordWrapState_t341939652_marshal_pinvoke_back, WordWrapState_t341939652_marshal_pinvoke_cleanup, NULL, NULL, &WordWrapState_t341939652_0_0_0 } /* TMPro.WordWrapState */,
	{ NULL, EmulatorButtonEvent_t938855819_marshal_pinvoke, EmulatorButtonEvent_t938855819_marshal_pinvoke_back, EmulatorButtonEvent_t938855819_marshal_pinvoke_cleanup, NULL, NULL, &EmulatorButtonEvent_t938855819_0_0_0 } /* Gvr.Internal.EmulatorButtonEvent */,
	{ NULL, EmulatorTouchEvent_t2277405062_marshal_pinvoke, EmulatorTouchEvent_t2277405062_marshal_pinvoke_back, EmulatorTouchEvent_t2277405062_marshal_pinvoke_cleanup, NULL, NULL, &EmulatorTouchEvent_t2277405062_0_0_0 } /* Gvr.Internal.EmulatorTouchEvent */,
	{ DelegatePInvokeWrapper_OnStateChangedEvent_t3148566047, NULL, NULL, NULL, NULL, NULL, &OnStateChangedEvent_t3148566047_0_0_0 } /* GvrControllerInput/OnStateChangedEvent */,
	{ NULL, FaceCameraData_t2570986833_marshal_pinvoke, FaceCameraData_t2570986833_marshal_pinvoke_back, FaceCameraData_t2570986833_marshal_pinvoke_cleanup, NULL, NULL, &FaceCameraData_t2570986833_0_0_0 } /* GvrControllerReticleVisual/FaceCameraData */,
	{ NULL, ControllerDisplayState_t972313457_marshal_pinvoke, ControllerDisplayState_t972313457_marshal_pinvoke_back, ControllerDisplayState_t972313457_marshal_pinvoke_cleanup, NULL, NULL, &ControllerDisplayState_t972313457_0_0_0 } /* GvrControllerVisual/ControllerDisplayState */,
	{ DelegatePInvokeWrapper_OnRecenterEvent_t1755824842, NULL, NULL, NULL, NULL, NULL, &OnRecenterEvent_t1755824842_0_0_0 } /* GvrHeadset/OnRecenterEvent */,
	{ DelegatePInvokeWrapper_OnSafetyRegionEvent_t980662704, NULL, NULL, NULL, NULL, NULL, &OnSafetyRegionEvent_t980662704_0_0_0 } /* GvrHeadset/OnSafetyRegionEvent */,
	{ DelegatePInvokeWrapper_EditTextCallback_t1702213000, NULL, NULL, NULL, NULL, NULL, &EditTextCallback_t1702213000_0_0_0 } /* GvrKeyboard/EditTextCallback */,
	{ DelegatePInvokeWrapper_ErrorCallback_t2310212740, NULL, NULL, NULL, NULL, NULL, &ErrorCallback_t2310212740_0_0_0 } /* GvrKeyboard/ErrorCallback */,
	{ DelegatePInvokeWrapper_KeyboardCallback_t3330588312, NULL, NULL, NULL, NULL, NULL, &KeyboardCallback_t3330588312_0_0_0 } /* GvrKeyboard/KeyboardCallback */,
	{ DelegatePInvokeWrapper_StandardCallback_t3095007891, NULL, NULL, NULL, NULL, NULL, &StandardCallback_t3095007891_0_0_0 } /* GvrKeyboard/StandardCallback */,
	{ DelegatePInvokeWrapper_GetPointForDistanceDelegate_t92913150, NULL, NULL, NULL, NULL, NULL, &GetPointForDistanceDelegate_t92913150_0_0_0 } /* GvrLaserVisual/GetPointForDistanceDelegate */,
	{ DelegatePInvokeWrapper_OnExceptionCallback_t1696428116, NULL, NULL, NULL, NULL, NULL, &OnExceptionCallback_t1696428116_0_0_0 } /* GvrVideoPlayerTexture/OnExceptionCallback */,
	{ DelegatePInvokeWrapper_OnVideoEventCallback_t2376626694, NULL, NULL, NULL, NULL, NULL, &OnVideoEventCallback_t2376626694_0_0_0 } /* GvrVideoPlayerTexture/OnVideoEventCallback */,
	{ NULL, Settings_t3016786575_marshal_pinvoke, Settings_t3016786575_marshal_pinvoke_back, Settings_t3016786575_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3016786575_0_0_0 } /* UnityEngine.PostProcessing.AmbientOcclusionModel/Settings */,
	{ NULL, BloomSettings_t2599855122_marshal_pinvoke, BloomSettings_t2599855122_marshal_pinvoke_back, BloomSettings_t2599855122_marshal_pinvoke_cleanup, NULL, NULL, &BloomSettings_t2599855122_0_0_0 } /* UnityEngine.PostProcessing.BloomModel/BloomSettings */,
	{ NULL, LensDirtSettings_t3693422705_marshal_pinvoke, LensDirtSettings_t3693422705_marshal_pinvoke_back, LensDirtSettings_t3693422705_marshal_pinvoke_cleanup, NULL, NULL, &LensDirtSettings_t3693422705_0_0_0 } /* UnityEngine.PostProcessing.BloomModel/LensDirtSettings */,
	{ NULL, Settings_t181254429_marshal_pinvoke, Settings_t181254429_marshal_pinvoke_back, Settings_t181254429_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t181254429_0_0_0 } /* UnityEngine.PostProcessing.BloomModel/Settings */,
	{ NULL, Settings_t2111398455_marshal_pinvoke, Settings_t2111398455_marshal_pinvoke_back, Settings_t2111398455_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t2111398455_0_0_0 } /* UnityEngine.PostProcessing.ChromaticAberrationModel/Settings */,
	{ NULL, CurvesSettings_t2830270037_marshal_pinvoke, CurvesSettings_t2830270037_marshal_pinvoke_back, CurvesSettings_t2830270037_marshal_pinvoke_cleanup, NULL, NULL, &CurvesSettings_t2830270037_0_0_0 } /* UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings */,
	{ NULL, Settings_t451872061_marshal_pinvoke, Settings_t451872061_marshal_pinvoke_back, Settings_t451872061_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t451872061_0_0_0 } /* UnityEngine.PostProcessing.ColorGradingModel/Settings */,
	{ NULL, Settings_t2195468135_marshal_pinvoke, Settings_t2195468135_marshal_pinvoke_back, Settings_t2195468135_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t2195468135_0_0_0 } /* UnityEngine.PostProcessing.DepthOfFieldModel/Settings */,
	{ NULL, Settings_t2874244444_marshal_pinvoke, Settings_t2874244444_marshal_pinvoke_back, Settings_t2874244444_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t2874244444_0_0_0 } /* UnityEngine.PostProcessing.EyeAdaptationModel/Settings */,
	{ NULL, Settings_t224798599_marshal_pinvoke, Settings_t224798599_marshal_pinvoke_back, Settings_t224798599_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t224798599_0_0_0 } /* UnityEngine.PostProcessing.FogModel/Settings */,
	{ NULL, Settings_t4123292438_marshal_pinvoke, Settings_t4123292438_marshal_pinvoke_back, Settings_t4123292438_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t4123292438_0_0_0 } /* UnityEngine.PostProcessing.GrainModel/Settings */,
	{ NULL, Frame_t295908221_marshal_pinvoke, Frame_t295908221_marshal_pinvoke_back, Frame_t295908221_marshal_pinvoke_cleanup, NULL, NULL, &Frame_t295908221_0_0_0 } /* UnityEngine.PostProcessing.MotionBlurComponent/FrameBlendingFilter/Frame */,
	{ NULL, ReflectionSettings_t282755190_marshal_pinvoke, ReflectionSettings_t282755190_marshal_pinvoke_back, ReflectionSettings_t282755190_marshal_pinvoke_cleanup, NULL, NULL, &ReflectionSettings_t282755190_0_0_0 } /* UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings */,
	{ NULL, Settings_t1995791524_marshal_pinvoke, Settings_t1995791524_marshal_pinvoke_back, Settings_t1995791524_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t1995791524_0_0_0 } /* UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings */,
	{ NULL, Settings_t3006579223_marshal_pinvoke, Settings_t3006579223_marshal_pinvoke_back, Settings_t3006579223_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t3006579223_0_0_0 } /* UnityEngine.PostProcessing.UserLutModel/Settings */,
	{ NULL, Settings_t1354494600_marshal_pinvoke, Settings_t1354494600_marshal_pinvoke_back, Settings_t1354494600_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t1354494600_0_0_0 } /* UnityEngine.PostProcessing.VignetteModel/Settings */,
	{ NULL, Settings_t1032325577_marshal_pinvoke, Settings_t1032325577_marshal_pinvoke_back, Settings_t1032325577_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t1032325577_0_0_0 } /* UnityStandardAssets.CinematicEffects.Bloom/Settings */,
	{ NULL, BokehTextureSettings_t3943002634_marshal_pinvoke, BokehTextureSettings_t3943002634_marshal_pinvoke_back, BokehTextureSettings_t3943002634_marshal_pinvoke_cleanup, NULL, NULL, &BokehTextureSettings_t3943002634_0_0_0 } /* UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings */,
	{ NULL, FocusSettings_t1261455774_marshal_pinvoke, FocusSettings_t1261455774_marshal_pinvoke_back, FocusSettings_t1261455774_marshal_pinvoke_cleanup, NULL, NULL, &FocusSettings_t1261455774_0_0_0 } /* UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings */,
	{ NULL, GlobalSettings_t956974295_marshal_pinvoke, GlobalSettings_t956974295_marshal_pinvoke_back, GlobalSettings_t956974295_marshal_pinvoke_cleanup, NULL, NULL, &GlobalSettings_t956974295_0_0_0 } /* UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings */,
	{ NULL, QualitySettings_t1379802392_marshal_pinvoke, QualitySettings_t1379802392_marshal_pinvoke_back, QualitySettings_t1379802392_marshal_pinvoke_cleanup, NULL, NULL, &QualitySettings_t1379802392_0_0_0 } /* UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings */,
	{ NULL, ChromaticAberrationSettings_t3807027201_marshal_pinvoke, ChromaticAberrationSettings_t3807027201_marshal_pinvoke_back, ChromaticAberrationSettings_t3807027201_marshal_pinvoke_cleanup, NULL, NULL, &ChromaticAberrationSettings_t3807027201_0_0_0 } /* UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings */,
	{ NULL, DistortionSettings_t3193728992_marshal_pinvoke, DistortionSettings_t3193728992_marshal_pinvoke_back, DistortionSettings_t3193728992_marshal_pinvoke_cleanup, NULL, NULL, &DistortionSettings_t3193728992_0_0_0 } /* UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings */,
	{ NULL, VignetteSettings_t2342083791_marshal_pinvoke, VignetteSettings_t2342083791_marshal_pinvoke_back, VignetteSettings_t2342083791_marshal_pinvoke_cleanup, NULL, NULL, &VignetteSettings_t2342083791_0_0_0 } /* UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings */,
	{ NULL, Frame_t3511111948_marshal_pinvoke, Frame_t3511111948_marshal_pinvoke_back, Frame_t3511111948_marshal_pinvoke_cleanup, NULL, NULL, &Frame_t3511111948_0_0_0 } /* UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame */,
	{ NULL, PredicationSettings_t4116788586_marshal_pinvoke, PredicationSettings_t4116788586_marshal_pinvoke_back, PredicationSettings_t4116788586_marshal_pinvoke_cleanup, NULL, NULL, &PredicationSettings_t4116788586_0_0_0 } /* UnityStandardAssets.CinematicEffects.SMAA/PredicationSettings */,
	{ NULL, QualitySettings_t1869387270_marshal_pinvoke, QualitySettings_t1869387270_marshal_pinvoke_back, QualitySettings_t1869387270_marshal_pinvoke_cleanup, NULL, NULL, &QualitySettings_t1869387270_0_0_0 } /* UnityStandardAssets.CinematicEffects.SMAA/QualitySettings */,
	{ NULL, TemporalSettings_t2985116592_marshal_pinvoke, TemporalSettings_t2985116592_marshal_pinvoke_back, TemporalSettings_t2985116592_marshal_pinvoke_cleanup, NULL, NULL, &TemporalSettings_t2985116592_0_0_0 } /* UnityStandardAssets.CinematicEffects.SMAA/TemporalSettings */,
	{ NULL, ReflectionSettings_t499321483_marshal_pinvoke, ReflectionSettings_t499321483_marshal_pinvoke_back, ReflectionSettings_t499321483_marshal_pinvoke_cleanup, NULL, NULL, &ReflectionSettings_t499321483_0_0_0 } /* UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings */,
	{ NULL, SSRSettings_t899296535_marshal_pinvoke, SSRSettings_t899296535_marshal_pinvoke_back, SSRSettings_t899296535_marshal_pinvoke_cleanup, NULL, NULL, &SSRSettings_t899296535_0_0_0 } /* UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings */,
	{ NULL, DebugSettings_t54746148_marshal_pinvoke, DebugSettings_t54746148_marshal_pinvoke_back, DebugSettings_t54746148_marshal_pinvoke_cleanup, NULL, NULL, &DebugSettings_t54746148_0_0_0 } /* UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/DebugSettings */,
	{ NULL, ChannelMixerSettings_t491321292_marshal_pinvoke, ChannelMixerSettings_t491321292_marshal_pinvoke_back, ChannelMixerSettings_t491321292_marshal_pinvoke_cleanup, NULL, NULL, &ChannelMixerSettings_t491321292_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings */,
	{ NULL, ColorGradingSettings_t1923563914_marshal_pinvoke, ColorGradingSettings_t1923563914_marshal_pinvoke_back, ColorGradingSettings_t1923563914_marshal_pinvoke_cleanup, NULL, NULL, &ColorGradingSettings_t1923563914_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings */,
	{ NULL, CurvesSettings_t2932745847_marshal_pinvoke, CurvesSettings_t2932745847_marshal_pinvoke_back, CurvesSettings_t2932745847_marshal_pinvoke_cleanup, NULL, NULL, &CurvesSettings_t2932745847_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings */,
	{ NULL, EyeAdaptationSettings_t3234830401_marshal_pinvoke, EyeAdaptationSettings_t3234830401_marshal_pinvoke_back, EyeAdaptationSettings_t3234830401_marshal_pinvoke_cleanup, NULL, NULL, &EyeAdaptationSettings_t3234830401_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings */,
	{ NULL, LUTSettings_t2339616782_marshal_pinvoke, LUTSettings_t2339616782_marshal_pinvoke_back, LUTSettings_t2339616782_marshal_pinvoke_cleanup, NULL, NULL, &LUTSettings_t2339616782_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings */,
	{ NULL, TonemappingSettings_t1161324624_marshal_pinvoke, TonemappingSettings_t1161324624_marshal_pinvoke_back, TonemappingSettings_t1161324624_marshal_pinvoke_cleanup, NULL, NULL, &TonemappingSettings_t1161324624_0_0_0 } /* UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings */,
	NULL,
};
