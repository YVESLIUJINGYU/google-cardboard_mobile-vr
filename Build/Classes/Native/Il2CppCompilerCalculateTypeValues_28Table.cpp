﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.String
struct String_t;
// System.Void
struct Void_t1185182177;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1615831949;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings[]
struct QualitySettingsU5BU5D_t3002864521;
// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter
struct FrameBlendingFilter_t4031465840;
// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame[]
struct FrameU5BU5D_t2667834693;
// UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter
struct ReconstructionFilter_t4011033762;
// UnityStandardAssets.CinematicEffects.MotionBlur/Settings
struct Settings_t1356140371;
// UnityStandardAssets.CinematicEffects.RenderTextureUtility
struct RenderTextureUtility_t1946006342;
// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Settings
struct Settings_t254817451;

struct AnimationCurve_t3046754366_marshaled_com;
struct RenderBuffer_t586150500 ;
struct Vector3_t3722313464 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef SETTINGS_T1356140371_H
#define SETTINGS_T1356140371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur/Settings
struct  Settings_t1356140371  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.CinematicEffects.MotionBlur/Settings::shutterAngle
	float ___shutterAngle_0;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/Settings::sampleCount
	int32_t ___sampleCount_1;
	// System.Single UnityStandardAssets.CinematicEffects.MotionBlur/Settings::frameBlending
	float ___frameBlending_2;

public:
	inline static int32_t get_offset_of_shutterAngle_0() { return static_cast<int32_t>(offsetof(Settings_t1356140371, ___shutterAngle_0)); }
	inline float get_shutterAngle_0() const { return ___shutterAngle_0; }
	inline float* get_address_of_shutterAngle_0() { return &___shutterAngle_0; }
	inline void set_shutterAngle_0(float value)
	{
		___shutterAngle_0 = value;
	}

	inline static int32_t get_offset_of_sampleCount_1() { return static_cast<int32_t>(offsetof(Settings_t1356140371, ___sampleCount_1)); }
	inline int32_t get_sampleCount_1() const { return ___sampleCount_1; }
	inline int32_t* get_address_of_sampleCount_1() { return &___sampleCount_1; }
	inline void set_sampleCount_1(int32_t value)
	{
		___sampleCount_1 = value;
	}

	inline static int32_t get_offset_of_frameBlending_2() { return static_cast<int32_t>(offsetof(Settings_t1356140371, ___frameBlending_2)); }
	inline float get_frameBlending_2() const { return ___frameBlending_2; }
	inline float* get_address_of_frameBlending_2() { return &___frameBlending_2; }
	inline void set_frameBlending_2(float value)
	{
		___frameBlending_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T1356140371_H
#ifndef U24ARRAYTYPEU3D12_T2488454199_H
#define U24ARRAYTYPEU3D12_T2488454199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454199_H
#ifndef U24ARRAYTYPEU3D16_T3253128245_H
#define U24ARRAYTYPEU3D16_T3253128245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t3253128245 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t3253128245__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T3253128245_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef BOKEHTEXTURESETTINGS_T3943002634_H
#define BOKEHTEXTURESETTINGS_T3943002634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings
struct  BokehTextureSettings_t3943002634 
{
public:
	// UnityEngine.Texture2D UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings::texture
	Texture2D_t3840446185 * ___texture_0;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings::scale
	float ___scale_1;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings::intensity
	float ___intensity_2;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings::threshold
	float ___threshold_3;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings::spawnHeuristic
	float ___spawnHeuristic_4;

public:
	inline static int32_t get_offset_of_texture_0() { return static_cast<int32_t>(offsetof(BokehTextureSettings_t3943002634, ___texture_0)); }
	inline Texture2D_t3840446185 * get_texture_0() const { return ___texture_0; }
	inline Texture2D_t3840446185 ** get_address_of_texture_0() { return &___texture_0; }
	inline void set_texture_0(Texture2D_t3840446185 * value)
	{
		___texture_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture_0), value);
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(BokehTextureSettings_t3943002634, ___scale_1)); }
	inline float get_scale_1() const { return ___scale_1; }
	inline float* get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(float value)
	{
		___scale_1 = value;
	}

	inline static int32_t get_offset_of_intensity_2() { return static_cast<int32_t>(offsetof(BokehTextureSettings_t3943002634, ___intensity_2)); }
	inline float get_intensity_2() const { return ___intensity_2; }
	inline float* get_address_of_intensity_2() { return &___intensity_2; }
	inline void set_intensity_2(float value)
	{
		___intensity_2 = value;
	}

	inline static int32_t get_offset_of_threshold_3() { return static_cast<int32_t>(offsetof(BokehTextureSettings_t3943002634, ___threshold_3)); }
	inline float get_threshold_3() const { return ___threshold_3; }
	inline float* get_address_of_threshold_3() { return &___threshold_3; }
	inline void set_threshold_3(float value)
	{
		___threshold_3 = value;
	}

	inline static int32_t get_offset_of_spawnHeuristic_4() { return static_cast<int32_t>(offsetof(BokehTextureSettings_t3943002634, ___spawnHeuristic_4)); }
	inline float get_spawnHeuristic_4() const { return ___spawnHeuristic_4; }
	inline float* get_address_of_spawnHeuristic_4() { return &___spawnHeuristic_4; }
	inline void set_spawnHeuristic_4(float value)
	{
		___spawnHeuristic_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings
struct BokehTextureSettings_t3943002634_marshaled_pinvoke
{
	Texture2D_t3840446185 * ___texture_0;
	float ___scale_1;
	float ___intensity_2;
	float ___threshold_3;
	float ___spawnHeuristic_4;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings
struct BokehTextureSettings_t3943002634_marshaled_com
{
	Texture2D_t3840446185 * ___texture_0;
	float ___scale_1;
	float ___intensity_2;
	float ___threshold_3;
	float ___spawnHeuristic_4;
};
#endif // BOKEHTEXTURESETTINGS_T3943002634_H
#ifndef FOCUSSETTINGS_T1261455774_H
#define FOCUSSETTINGS_T1261455774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings
struct  FocusSettings_t1261455774 
{
public:
	// UnityEngine.Transform UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::transform
	Transform_t3600365921 * ___transform_0;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::focusPlane
	float ___focusPlane_1;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::range
	float ___range_2;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::nearPlane
	float ___nearPlane_3;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::nearFalloff
	float ___nearFalloff_4;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::farPlane
	float ___farPlane_5;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::farFalloff
	float ___farFalloff_6;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::nearBlurRadius
	float ___nearBlurRadius_7;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings::farBlurRadius
	float ___farBlurRadius_8;

public:
	inline static int32_t get_offset_of_transform_0() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___transform_0)); }
	inline Transform_t3600365921 * get_transform_0() const { return ___transform_0; }
	inline Transform_t3600365921 ** get_address_of_transform_0() { return &___transform_0; }
	inline void set_transform_0(Transform_t3600365921 * value)
	{
		___transform_0 = value;
		Il2CppCodeGenWriteBarrier((&___transform_0), value);
	}

	inline static int32_t get_offset_of_focusPlane_1() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___focusPlane_1)); }
	inline float get_focusPlane_1() const { return ___focusPlane_1; }
	inline float* get_address_of_focusPlane_1() { return &___focusPlane_1; }
	inline void set_focusPlane_1(float value)
	{
		___focusPlane_1 = value;
	}

	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___range_2)); }
	inline float get_range_2() const { return ___range_2; }
	inline float* get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(float value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_nearPlane_3() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___nearPlane_3)); }
	inline float get_nearPlane_3() const { return ___nearPlane_3; }
	inline float* get_address_of_nearPlane_3() { return &___nearPlane_3; }
	inline void set_nearPlane_3(float value)
	{
		___nearPlane_3 = value;
	}

	inline static int32_t get_offset_of_nearFalloff_4() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___nearFalloff_4)); }
	inline float get_nearFalloff_4() const { return ___nearFalloff_4; }
	inline float* get_address_of_nearFalloff_4() { return &___nearFalloff_4; }
	inline void set_nearFalloff_4(float value)
	{
		___nearFalloff_4 = value;
	}

	inline static int32_t get_offset_of_farPlane_5() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___farPlane_5)); }
	inline float get_farPlane_5() const { return ___farPlane_5; }
	inline float* get_address_of_farPlane_5() { return &___farPlane_5; }
	inline void set_farPlane_5(float value)
	{
		___farPlane_5 = value;
	}

	inline static int32_t get_offset_of_farFalloff_6() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___farFalloff_6)); }
	inline float get_farFalloff_6() const { return ___farFalloff_6; }
	inline float* get_address_of_farFalloff_6() { return &___farFalloff_6; }
	inline void set_farFalloff_6(float value)
	{
		___farFalloff_6 = value;
	}

	inline static int32_t get_offset_of_nearBlurRadius_7() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___nearBlurRadius_7)); }
	inline float get_nearBlurRadius_7() const { return ___nearBlurRadius_7; }
	inline float* get_address_of_nearBlurRadius_7() { return &___nearBlurRadius_7; }
	inline void set_nearBlurRadius_7(float value)
	{
		___nearBlurRadius_7 = value;
	}

	inline static int32_t get_offset_of_farBlurRadius_8() { return static_cast<int32_t>(offsetof(FocusSettings_t1261455774, ___farBlurRadius_8)); }
	inline float get_farBlurRadius_8() const { return ___farBlurRadius_8; }
	inline float* get_address_of_farBlurRadius_8() { return &___farBlurRadius_8; }
	inline void set_farBlurRadius_8(float value)
	{
		___farBlurRadius_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings
struct FocusSettings_t1261455774_marshaled_pinvoke
{
	Transform_t3600365921 * ___transform_0;
	float ___focusPlane_1;
	float ___range_2;
	float ___nearPlane_3;
	float ___nearFalloff_4;
	float ___farPlane_5;
	float ___farFalloff_6;
	float ___nearBlurRadius_7;
	float ___farBlurRadius_8;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings
struct FocusSettings_t1261455774_marshaled_com
{
	Transform_t3600365921 * ___transform_0;
	float ___focusPlane_1;
	float ___range_2;
	float ___nearPlane_3;
	float ___nearFalloff_4;
	float ___farPlane_5;
	float ___farFalloff_6;
	float ___nearBlurRadius_7;
	float ___farBlurRadius_8;
};
#endif // FOCUSSETTINGS_T1261455774_H
#ifndef ADVANCEDSETTING_T4217923793_H
#define ADVANCEDSETTING_T4217923793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations/AdvancedSetting
struct  AdvancedSetting_t4217923793  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDSETTING_T4217923793_H
#ifndef DISTORTIONSETTINGS_T3193728992_H
#define DISTORTIONSETTINGS_T3193728992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings
struct  DistortionSettings_t3193728992 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings::enabled
	bool ___enabled_0;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings::amount
	float ___amount_1;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings::centerX
	float ___centerX_2;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings::centerY
	float ___centerY_3;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings::amountX
	float ___amountX_4;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings::amountY
	float ___amountY_5;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings::scale
	float ___scale_6;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(DistortionSettings_t3193728992, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_amount_1() { return static_cast<int32_t>(offsetof(DistortionSettings_t3193728992, ___amount_1)); }
	inline float get_amount_1() const { return ___amount_1; }
	inline float* get_address_of_amount_1() { return &___amount_1; }
	inline void set_amount_1(float value)
	{
		___amount_1 = value;
	}

	inline static int32_t get_offset_of_centerX_2() { return static_cast<int32_t>(offsetof(DistortionSettings_t3193728992, ___centerX_2)); }
	inline float get_centerX_2() const { return ___centerX_2; }
	inline float* get_address_of_centerX_2() { return &___centerX_2; }
	inline void set_centerX_2(float value)
	{
		___centerX_2 = value;
	}

	inline static int32_t get_offset_of_centerY_3() { return static_cast<int32_t>(offsetof(DistortionSettings_t3193728992, ___centerY_3)); }
	inline float get_centerY_3() const { return ___centerY_3; }
	inline float* get_address_of_centerY_3() { return &___centerY_3; }
	inline void set_centerY_3(float value)
	{
		___centerY_3 = value;
	}

	inline static int32_t get_offset_of_amountX_4() { return static_cast<int32_t>(offsetof(DistortionSettings_t3193728992, ___amountX_4)); }
	inline float get_amountX_4() const { return ___amountX_4; }
	inline float* get_address_of_amountX_4() { return &___amountX_4; }
	inline void set_amountX_4(float value)
	{
		___amountX_4 = value;
	}

	inline static int32_t get_offset_of_amountY_5() { return static_cast<int32_t>(offsetof(DistortionSettings_t3193728992, ___amountY_5)); }
	inline float get_amountY_5() const { return ___amountY_5; }
	inline float* get_address_of_amountY_5() { return &___amountY_5; }
	inline void set_amountY_5(float value)
	{
		___amountY_5 = value;
	}

	inline static int32_t get_offset_of_scale_6() { return static_cast<int32_t>(offsetof(DistortionSettings_t3193728992, ___scale_6)); }
	inline float get_scale_6() const { return ___scale_6; }
	inline float* get_address_of_scale_6() { return &___scale_6; }
	inline void set_scale_6(float value)
	{
		___scale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings
struct DistortionSettings_t3193728992_marshaled_pinvoke
{
	int32_t ___enabled_0;
	float ___amount_1;
	float ___centerX_2;
	float ___centerY_3;
	float ___amountX_4;
	float ___amountY_5;
	float ___scale_6;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings
struct DistortionSettings_t3193728992_marshaled_com
{
	int32_t ___enabled_0;
	float ___amount_1;
	float ___centerX_2;
	float ___centerY_3;
	float ___amountX_4;
	float ___amountY_5;
	float ___scale_6;
};
#endif // DISTORTIONSETTINGS_T3193728992_H
#ifndef SETTINGSGROUP_T1699980700_H
#define SETTINGSGROUP_T1699980700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations/SettingsGroup
struct  SettingsGroup_t1699980700  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSGROUP_T1699980700_H
#ifndef FRAME_T3511111948_H
#define FRAME_T3511111948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame
struct  Frame_t3511111948 
{
public:
	// UnityEngine.RenderTexture UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame::lumaTexture
	RenderTexture_t2108887433 * ___lumaTexture_0;
	// UnityEngine.RenderTexture UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame::chromaTexture
	RenderTexture_t2108887433 * ___chromaTexture_1;
	// System.Single UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame::time
	float ___time_2;
	// UnityEngine.RenderBuffer[] UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame::_mrt
	RenderBufferU5BU5D_t1615831949* ____mrt_3;

public:
	inline static int32_t get_offset_of_lumaTexture_0() { return static_cast<int32_t>(offsetof(Frame_t3511111948, ___lumaTexture_0)); }
	inline RenderTexture_t2108887433 * get_lumaTexture_0() const { return ___lumaTexture_0; }
	inline RenderTexture_t2108887433 ** get_address_of_lumaTexture_0() { return &___lumaTexture_0; }
	inline void set_lumaTexture_0(RenderTexture_t2108887433 * value)
	{
		___lumaTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___lumaTexture_0), value);
	}

	inline static int32_t get_offset_of_chromaTexture_1() { return static_cast<int32_t>(offsetof(Frame_t3511111948, ___chromaTexture_1)); }
	inline RenderTexture_t2108887433 * get_chromaTexture_1() const { return ___chromaTexture_1; }
	inline RenderTexture_t2108887433 ** get_address_of_chromaTexture_1() { return &___chromaTexture_1; }
	inline void set_chromaTexture_1(RenderTexture_t2108887433 * value)
	{
		___chromaTexture_1 = value;
		Il2CppCodeGenWriteBarrier((&___chromaTexture_1), value);
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(Frame_t3511111948, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of__mrt_3() { return static_cast<int32_t>(offsetof(Frame_t3511111948, ____mrt_3)); }
	inline RenderBufferU5BU5D_t1615831949* get__mrt_3() const { return ____mrt_3; }
	inline RenderBufferU5BU5D_t1615831949** get_address_of__mrt_3() { return &____mrt_3; }
	inline void set__mrt_3(RenderBufferU5BU5D_t1615831949* value)
	{
		____mrt_3 = value;
		Il2CppCodeGenWriteBarrier((&____mrt_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame
struct Frame_t3511111948_marshaled_pinvoke
{
	RenderTexture_t2108887433 * ___lumaTexture_0;
	RenderTexture_t2108887433 * ___chromaTexture_1;
	float ___time_2;
	RenderBuffer_t586150500 * ____mrt_3;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame
struct Frame_t3511111948_marshaled_com
{
	RenderTexture_t2108887433 * ___lumaTexture_0;
	RenderTexture_t2108887433 * ___chromaTexture_1;
	float ___time_2;
	RenderBuffer_t586150500 * ____mrt_3;
};
#endif // FRAME_T3511111948_H
#ifndef INTENSITYSETTINGS_T3790221060_H
#define INTENSITYSETTINGS_T3790221060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/IntensitySettings
struct  IntensitySettings_t3790221060 
{
public:
	// System.Single UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/IntensitySettings::reflectionMultiplier
	float ___reflectionMultiplier_0;
	// System.Single UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/IntensitySettings::fadeDistance
	float ___fadeDistance_1;
	// System.Single UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/IntensitySettings::fresnelFade
	float ___fresnelFade_2;
	// System.Single UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/IntensitySettings::fresnelFadePower
	float ___fresnelFadePower_3;

public:
	inline static int32_t get_offset_of_reflectionMultiplier_0() { return static_cast<int32_t>(offsetof(IntensitySettings_t3790221060, ___reflectionMultiplier_0)); }
	inline float get_reflectionMultiplier_0() const { return ___reflectionMultiplier_0; }
	inline float* get_address_of_reflectionMultiplier_0() { return &___reflectionMultiplier_0; }
	inline void set_reflectionMultiplier_0(float value)
	{
		___reflectionMultiplier_0 = value;
	}

	inline static int32_t get_offset_of_fadeDistance_1() { return static_cast<int32_t>(offsetof(IntensitySettings_t3790221060, ___fadeDistance_1)); }
	inline float get_fadeDistance_1() const { return ___fadeDistance_1; }
	inline float* get_address_of_fadeDistance_1() { return &___fadeDistance_1; }
	inline void set_fadeDistance_1(float value)
	{
		___fadeDistance_1 = value;
	}

	inline static int32_t get_offset_of_fresnelFade_2() { return static_cast<int32_t>(offsetof(IntensitySettings_t3790221060, ___fresnelFade_2)); }
	inline float get_fresnelFade_2() const { return ___fresnelFade_2; }
	inline float* get_address_of_fresnelFade_2() { return &___fresnelFade_2; }
	inline void set_fresnelFade_2(float value)
	{
		___fresnelFade_2 = value;
	}

	inline static int32_t get_offset_of_fresnelFadePower_3() { return static_cast<int32_t>(offsetof(IntensitySettings_t3790221060, ___fresnelFadePower_3)); }
	inline float get_fresnelFadePower_3() const { return ___fresnelFadePower_3; }
	inline float* get_address_of_fresnelFadePower_3() { return &___fresnelFadePower_3; }
	inline void set_fresnelFadePower_3(float value)
	{
		___fresnelFadePower_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENSITYSETTINGS_T3790221060_H
#ifndef SCREENEDGEMASK_T3779436815_H
#define SCREENEDGEMASK_T3779436815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ScreenEdgeMask
struct  ScreenEdgeMask_t3779436815 
{
public:
	// System.Single UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ScreenEdgeMask::intensity
	float ___intensity_0;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(ScreenEdgeMask_t3779436815, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENEDGEMASK_T3779436815_H
#ifndef BLENDSETTINGS_T3588719326_H
#define BLENDSETTINGS_T3588719326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/BlendSettings
struct  BlendSettings_t3588719326 
{
public:
	// System.Single UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/BlendSettings::stationary
	float ___stationary_0;
	// System.Single UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/BlendSettings::moving
	float ___moving_1;
	// System.Single UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/BlendSettings::motionAmplification
	float ___motionAmplification_2;

public:
	inline static int32_t get_offset_of_stationary_0() { return static_cast<int32_t>(offsetof(BlendSettings_t3588719326, ___stationary_0)); }
	inline float get_stationary_0() const { return ___stationary_0; }
	inline float* get_address_of_stationary_0() { return &___stationary_0; }
	inline void set_stationary_0(float value)
	{
		___stationary_0 = value;
	}

	inline static int32_t get_offset_of_moving_1() { return static_cast<int32_t>(offsetof(BlendSettings_t3588719326, ___moving_1)); }
	inline float get_moving_1() const { return ___moving_1; }
	inline float* get_address_of_moving_1() { return &___moving_1; }
	inline void set_moving_1(float value)
	{
		___moving_1 = value;
	}

	inline static int32_t get_offset_of_motionAmplification_2() { return static_cast<int32_t>(offsetof(BlendSettings_t3588719326, ___motionAmplification_2)); }
	inline float get_motionAmplification_2() const { return ___motionAmplification_2; }
	inline float* get_address_of_motionAmplification_2() { return &___motionAmplification_2; }
	inline void set_motionAmplification_2(float value)
	{
		___motionAmplification_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDSETTINGS_T3588719326_H
#ifndef DEBUGSETTINGS_T54746148_H
#define DEBUGSETTINGS_T54746148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/DebugSettings
struct  DebugSettings_t54746148 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/DebugSettings::forceRepaint
	bool ___forceRepaint_0;

public:
	inline static int32_t get_offset_of_forceRepaint_0() { return static_cast<int32_t>(offsetof(DebugSettings_t54746148, ___forceRepaint_0)); }
	inline bool get_forceRepaint_0() const { return ___forceRepaint_0; }
	inline bool* get_address_of_forceRepaint_0() { return &___forceRepaint_0; }
	inline void set_forceRepaint_0(bool value)
	{
		___forceRepaint_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/DebugSettings
struct DebugSettings_t54746148_marshaled_pinvoke
{
	int32_t ___forceRepaint_0;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/DebugSettings
struct DebugSettings_t54746148_marshaled_com
{
	int32_t ___forceRepaint_0;
};
#endif // DEBUGSETTINGS_T54746148_H
#ifndef SHARPENFILTERSETTINGS_T2911684052_H
#define SHARPENFILTERSETTINGS_T2911684052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/SharpenFilterSettings
struct  SharpenFilterSettings_t2911684052 
{
public:
	// System.Single UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/SharpenFilterSettings::amount
	float ___amount_0;

public:
	inline static int32_t get_offset_of_amount_0() { return static_cast<int32_t>(offsetof(SharpenFilterSettings_t2911684052, ___amount_0)); }
	inline float get_amount_0() const { return ___amount_0; }
	inline float* get_address_of_amount_0() { return &___amount_0; }
	inline void set_amount_0(float value)
	{
		___amount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARPENFILTERSETTINGS_T2911684052_H
#ifndef BASICSSETTINGS_T2030707387_H
#define BASICSSETTINGS_T2030707387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings
struct  BasicsSettings_t2030707387 
{
public:
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::temperatureShift
	float ___temperatureShift_0;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::tint
	float ___tint_1;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::hue
	float ___hue_2;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::saturation
	float ___saturation_3;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::vibrance
	float ___vibrance_4;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::value
	float ___value_5;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::contrast
	float ___contrast_6;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::gain
	float ___gain_7;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings::gamma
	float ___gamma_8;

public:
	inline static int32_t get_offset_of_temperatureShift_0() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___temperatureShift_0)); }
	inline float get_temperatureShift_0() const { return ___temperatureShift_0; }
	inline float* get_address_of_temperatureShift_0() { return &___temperatureShift_0; }
	inline void set_temperatureShift_0(float value)
	{
		___temperatureShift_0 = value;
	}

	inline static int32_t get_offset_of_tint_1() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___tint_1)); }
	inline float get_tint_1() const { return ___tint_1; }
	inline float* get_address_of_tint_1() { return &___tint_1; }
	inline void set_tint_1(float value)
	{
		___tint_1 = value;
	}

	inline static int32_t get_offset_of_hue_2() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___hue_2)); }
	inline float get_hue_2() const { return ___hue_2; }
	inline float* get_address_of_hue_2() { return &___hue_2; }
	inline void set_hue_2(float value)
	{
		___hue_2 = value;
	}

	inline static int32_t get_offset_of_saturation_3() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___saturation_3)); }
	inline float get_saturation_3() const { return ___saturation_3; }
	inline float* get_address_of_saturation_3() { return &___saturation_3; }
	inline void set_saturation_3(float value)
	{
		___saturation_3 = value;
	}

	inline static int32_t get_offset_of_vibrance_4() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___vibrance_4)); }
	inline float get_vibrance_4() const { return ___vibrance_4; }
	inline float* get_address_of_vibrance_4() { return &___vibrance_4; }
	inline void set_vibrance_4(float value)
	{
		___vibrance_4 = value;
	}

	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___value_5)); }
	inline float get_value_5() const { return ___value_5; }
	inline float* get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(float value)
	{
		___value_5 = value;
	}

	inline static int32_t get_offset_of_contrast_6() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___contrast_6)); }
	inline float get_contrast_6() const { return ___contrast_6; }
	inline float* get_address_of_contrast_6() { return &___contrast_6; }
	inline void set_contrast_6(float value)
	{
		___contrast_6 = value;
	}

	inline static int32_t get_offset_of_gain_7() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___gain_7)); }
	inline float get_gain_7() const { return ___gain_7; }
	inline float* get_address_of_gain_7() { return &___gain_7; }
	inline void set_gain_7(float value)
	{
		___gain_7 = value;
	}

	inline static int32_t get_offset_of_gamma_8() { return static_cast<int32_t>(offsetof(BasicsSettings_t2030707387, ___gamma_8)); }
	inline float get_gamma_8() const { return ___gamma_8; }
	inline float* get_address_of_gamma_8() { return &___gamma_8; }
	inline void set_gamma_8(float value)
	{
		___gamma_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICSSETTINGS_T2030707387_H
#ifndef CHANNELMIXERSETTINGS_T491321292_H
#define CHANNELMIXERSETTINGS_T491321292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings
struct  ChannelMixerSettings_t491321292 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings::currentChannel
	int32_t ___currentChannel_0;
	// UnityEngine.Vector3[] UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings::channels
	Vector3U5BU5D_t1718750761* ___channels_1;

public:
	inline static int32_t get_offset_of_currentChannel_0() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t491321292, ___currentChannel_0)); }
	inline int32_t get_currentChannel_0() const { return ___currentChannel_0; }
	inline int32_t* get_address_of_currentChannel_0() { return &___currentChannel_0; }
	inline void set_currentChannel_0(int32_t value)
	{
		___currentChannel_0 = value;
	}

	inline static int32_t get_offset_of_channels_1() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t491321292, ___channels_1)); }
	inline Vector3U5BU5D_t1718750761* get_channels_1() const { return ___channels_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_channels_1() { return &___channels_1; }
	inline void set_channels_1(Vector3U5BU5D_t1718750761* value)
	{
		___channels_1 = value;
		Il2CppCodeGenWriteBarrier((&___channels_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings
struct ChannelMixerSettings_t491321292_marshaled_pinvoke
{
	int32_t ___currentChannel_0;
	Vector3_t3722313464 * ___channels_1;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings
struct ChannelMixerSettings_t491321292_marshaled_com
{
	int32_t ___currentChannel_0;
	Vector3_t3722313464 * ___channels_1;
};
#endif // CHANNELMIXERSETTINGS_T491321292_H
#ifndef EYEADAPTATIONSETTINGS_T3234830401_H
#define EYEADAPTATIONSETTINGS_T3234830401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings
struct  EyeAdaptationSettings_t3234830401 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings::enabled
	bool ___enabled_0;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings::middleGrey
	float ___middleGrey_1;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings::min
	float ___min_2;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings::max
	float ___max_3;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings::speed
	float ___speed_4;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings::showDebug
	bool ___showDebug_5;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(EyeAdaptationSettings_t3234830401, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_middleGrey_1() { return static_cast<int32_t>(offsetof(EyeAdaptationSettings_t3234830401, ___middleGrey_1)); }
	inline float get_middleGrey_1() const { return ___middleGrey_1; }
	inline float* get_address_of_middleGrey_1() { return &___middleGrey_1; }
	inline void set_middleGrey_1(float value)
	{
		___middleGrey_1 = value;
	}

	inline static int32_t get_offset_of_min_2() { return static_cast<int32_t>(offsetof(EyeAdaptationSettings_t3234830401, ___min_2)); }
	inline float get_min_2() const { return ___min_2; }
	inline float* get_address_of_min_2() { return &___min_2; }
	inline void set_min_2(float value)
	{
		___min_2 = value;
	}

	inline static int32_t get_offset_of_max_3() { return static_cast<int32_t>(offsetof(EyeAdaptationSettings_t3234830401, ___max_3)); }
	inline float get_max_3() const { return ___max_3; }
	inline float* get_address_of_max_3() { return &___max_3; }
	inline void set_max_3(float value)
	{
		___max_3 = value;
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(EyeAdaptationSettings_t3234830401, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_showDebug_5() { return static_cast<int32_t>(offsetof(EyeAdaptationSettings_t3234830401, ___showDebug_5)); }
	inline bool get_showDebug_5() const { return ___showDebug_5; }
	inline bool* get_address_of_showDebug_5() { return &___showDebug_5; }
	inline void set_showDebug_5(bool value)
	{
		___showDebug_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings
struct EyeAdaptationSettings_t3234830401_marshaled_pinvoke
{
	int32_t ___enabled_0;
	float ___middleGrey_1;
	float ___min_2;
	float ___max_3;
	float ___speed_4;
	int32_t ___showDebug_5;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings
struct EyeAdaptationSettings_t3234830401_marshaled_com
{
	int32_t ___enabled_0;
	float ___middleGrey_1;
	float ___min_2;
	float ___max_3;
	float ___speed_4;
	int32_t ___showDebug_5;
};
#endif // EYEADAPTATIONSETTINGS_T3234830401_H
#ifndef LUTSETTINGS_T2339616782_H
#define LUTSETTINGS_T2339616782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings
struct  LUTSettings_t2339616782 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings::enabled
	bool ___enabled_0;
	// UnityEngine.Texture UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings::texture
	Texture_t3661962703 * ___texture_1;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings::contribution
	float ___contribution_2;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(LUTSettings_t2339616782, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_texture_1() { return static_cast<int32_t>(offsetof(LUTSettings_t2339616782, ___texture_1)); }
	inline Texture_t3661962703 * get_texture_1() const { return ___texture_1; }
	inline Texture_t3661962703 ** get_address_of_texture_1() { return &___texture_1; }
	inline void set_texture_1(Texture_t3661962703 * value)
	{
		___texture_1 = value;
		Il2CppCodeGenWriteBarrier((&___texture_1), value);
	}

	inline static int32_t get_offset_of_contribution_2() { return static_cast<int32_t>(offsetof(LUTSettings_t2339616782, ___contribution_2)); }
	inline float get_contribution_2() const { return ___contribution_2; }
	inline float* get_address_of_contribution_2() { return &___contribution_2; }
	inline void set_contribution_2(float value)
	{
		___contribution_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings
struct LUTSettings_t2339616782_marshaled_pinvoke
{
	int32_t ___enabled_0;
	Texture_t3661962703 * ___texture_1;
	float ___contribution_2;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings
struct LUTSettings_t2339616782_marshaled_com
{
	int32_t ___enabled_0;
	Texture_t3661962703 * ___texture_1;
	float ___contribution_2;
};
#endif // LUTSETTINGS_T2339616782_H
#ifndef SETTINGSGROUP_T3113343738_H
#define SETTINGSGROUP_T3113343738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/SettingsGroup
struct  SettingsGroup_t3113343738  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSGROUP_T3113343738_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=16 <PrivateImplementationDetails>::$field-8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41
	U24ArrayTypeU3D16_t3253128245  ___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0;
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-51A7A390CD6DE245186881400B18C9D822EFE240
	U24ArrayTypeU3D12_t2488454199  ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0)); }
	inline U24ArrayTypeU3D16_t3253128245  get_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0() const { return ___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0; }
	inline U24ArrayTypeU3D16_t3253128245 * get_address_of_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0() { return &___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0; }
	inline void set_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0(U24ArrayTypeU3D16_t3253128245  value)
	{
		___U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1)); }
	inline U24ArrayTypeU3D12_t2488454199  get_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1() const { return ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1; }
	inline U24ArrayTypeU3D12_t2488454199 * get_address_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1() { return &___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1; }
	inline void set_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1(U24ArrayTypeU3D12_t2488454199  value)
	{
		___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255368_H
#ifndef ANIMATIONCURVE_T3046754366_H
#define ANIMATIONCURVE_T3046754366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimationCurve
struct  AnimationCurve_t3046754366  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t3046754366, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // ANIMATIONCURVE_T3046754366_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RENDERTEXTUREFORMAT_T962350765_H
#define RENDERTEXTUREFORMAT_T962350765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTextureFormat
struct  RenderTextureFormat_t962350765 
{
public:
	// System.Int32 UnityEngine.RenderTextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderTextureFormat_t962350765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFORMAT_T962350765_H
#ifndef APERTURESHAPE_T3609884370_H
#define APERTURESHAPE_T3609884370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/ApertureShape
struct  ApertureShape_t3609884370 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField/ApertureShape::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ApertureShape_t3609884370, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APERTURESHAPE_T3609884370_H
#ifndef BOKEHTEXTURESPASSES_T3576012827_H
#define BOKEHTEXTURESPASSES_T3576012827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/BokehTexturesPasses
struct  BokehTexturesPasses_t3576012827 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField/BokehTexturesPasses::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BokehTexturesPasses_t3576012827, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOKEHTEXTURESPASSES_T3576012827_H
#ifndef FILTERQUALITY_T3841223768_H
#define FILTERQUALITY_T3841223768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/FilterQuality
struct  FilterQuality_t3841223768 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField/FilterQuality::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FilterQuality_t3841223768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERQUALITY_T3841223768_H
#ifndef MEDIANPASSES_T4189517256_H
#define MEDIANPASSES_T4189517256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/MedianPasses
struct  MedianPasses_t4189517256 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField/MedianPasses::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MedianPasses_t4189517256, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIANPASSES_T4189517256_H
#ifndef PASSES_T633783710_H
#define PASSES_T633783710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/Passes
struct  Passes_t633783710 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField/Passes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Passes_t633783710, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSES_T633783710_H
#ifndef QUALITYPRESET_T1593743734_H
#define QUALITYPRESET_T1593743734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/QualityPreset
struct  QualityPreset_t1593743734 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField/QualityPreset::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(QualityPreset_t1593743734, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUALITYPRESET_T1593743734_H
#ifndef TWEAKMODE_T577622288_H
#define TWEAKMODE_T577622288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/TweakMode
struct  TweakMode_t577622288 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField/TweakMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweakMode_t577622288, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEAKMODE_T577622288_H
#ifndef CHROMATICABERRATIONSETTINGS_T3807027201_H
#define CHROMATICABERRATIONSETTINGS_T3807027201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings
struct  ChromaticAberrationSettings_t3807027201 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings::enabled
	bool ___enabled_0;
	// UnityEngine.Color UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings::color
	Color_t2555686324  ___color_1;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings::amount
	float ___amount_2;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(ChromaticAberrationSettings_t3807027201, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(ChromaticAberrationSettings_t3807027201, ___color_1)); }
	inline Color_t2555686324  get_color_1() const { return ___color_1; }
	inline Color_t2555686324 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2555686324  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_amount_2() { return static_cast<int32_t>(offsetof(ChromaticAberrationSettings_t3807027201, ___amount_2)); }
	inline float get_amount_2() const { return ___amount_2; }
	inline float* get_address_of_amount_2() { return &___amount_2; }
	inline void set_amount_2(float value)
	{
		___amount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings
struct ChromaticAberrationSettings_t3807027201_marshaled_pinvoke
{
	int32_t ___enabled_0;
	Color_t2555686324  ___color_1;
	float ___amount_2;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings
struct ChromaticAberrationSettings_t3807027201_marshaled_com
{
	int32_t ___enabled_0;
	Color_t2555686324  ___color_1;
	float ___amount_2;
};
#endif // CHROMATICABERRATIONSETTINGS_T3807027201_H
#ifndef PASS_T1667699932_H
#define PASS_T1667699932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations/Pass
struct  Pass_t1667699932 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations/Pass::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pass_t1667699932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T1667699932_H
#ifndef VIGNETTESETTINGS_T2342083791_H
#define VIGNETTESETTINGS_T2342083791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings
struct  VignetteSettings_t2342083791 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::enabled
	bool ___enabled_0;
	// UnityEngine.Color UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::color
	Color_t2555686324  ___color_1;
	// UnityEngine.Vector2 UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::center
	Vector2_t2156229523  ___center_2;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::intensity
	float ___intensity_3;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::smoothness
	float ___smoothness_4;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::roundness
	float ___roundness_5;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::blur
	float ___blur_6;
	// System.Single UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings::desaturate
	float ___desaturate_7;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___color_1)); }
	inline Color_t2555686324  get_color_1() const { return ___color_1; }
	inline Color_t2555686324 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2555686324  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_center_2() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___center_2)); }
	inline Vector2_t2156229523  get_center_2() const { return ___center_2; }
	inline Vector2_t2156229523 * get_address_of_center_2() { return &___center_2; }
	inline void set_center_2(Vector2_t2156229523  value)
	{
		___center_2 = value;
	}

	inline static int32_t get_offset_of_intensity_3() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___intensity_3)); }
	inline float get_intensity_3() const { return ___intensity_3; }
	inline float* get_address_of_intensity_3() { return &___intensity_3; }
	inline void set_intensity_3(float value)
	{
		___intensity_3 = value;
	}

	inline static int32_t get_offset_of_smoothness_4() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___smoothness_4)); }
	inline float get_smoothness_4() const { return ___smoothness_4; }
	inline float* get_address_of_smoothness_4() { return &___smoothness_4; }
	inline void set_smoothness_4(float value)
	{
		___smoothness_4 = value;
	}

	inline static int32_t get_offset_of_roundness_5() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___roundness_5)); }
	inline float get_roundness_5() const { return ___roundness_5; }
	inline float* get_address_of_roundness_5() { return &___roundness_5; }
	inline void set_roundness_5(float value)
	{
		___roundness_5 = value;
	}

	inline static int32_t get_offset_of_blur_6() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___blur_6)); }
	inline float get_blur_6() const { return ___blur_6; }
	inline float* get_address_of_blur_6() { return &___blur_6; }
	inline void set_blur_6(float value)
	{
		___blur_6 = value;
	}

	inline static int32_t get_offset_of_desaturate_7() { return static_cast<int32_t>(offsetof(VignetteSettings_t2342083791, ___desaturate_7)); }
	inline float get_desaturate_7() const { return ___desaturate_7; }
	inline float* get_address_of_desaturate_7() { return &___desaturate_7; }
	inline void set_desaturate_7(float value)
	{
		___desaturate_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings
struct VignetteSettings_t2342083791_marshaled_pinvoke
{
	int32_t ___enabled_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	float ___blur_6;
	float ___desaturate_7;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings
struct VignetteSettings_t2342083791_marshaled_com
{
	int32_t ___enabled_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	float ___blur_6;
	float ___desaturate_7;
};
#endif // VIGNETTESETTINGS_T2342083791_H
#ifndef PASSINDEX_T2581702242_H
#define PASSINDEX_T2581702242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/PassIndex
struct  PassIndex_t2581702242 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/PassIndex::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PassIndex_t2581702242, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSINDEX_T2581702242_H
#ifndef SSRREFLECTIONBLENDTYPE_T2762048609_H
#define SSRREFLECTIONBLENDTYPE_T2762048609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRReflectionBlendType
struct  SSRReflectionBlendType_t2762048609 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRReflectionBlendType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SSRReflectionBlendType_t2762048609, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSRREFLECTIONBLENDTYPE_T2762048609_H
#ifndef SSRRESOLUTION_T1711022735_H
#define SSRRESOLUTION_T1711022735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRResolution
struct  SSRResolution_t1711022735 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SSRResolution_t1711022735, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSRRESOLUTION_T1711022735_H
#ifndef LAYOUTATTRIBUTE_T894525338_H
#define LAYOUTATTRIBUTE_T894525338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings/LayoutAttribute
struct  LayoutAttribute_t894525338  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTATTRIBUTE_T894525338_H
#ifndef SEQUENCE_T1852423168_H
#define SEQUENCE_T1852423168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Sequence
struct  Sequence_t1852423168 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Sequence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sequence_t1852423168, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCE_T1852423168_H
#ifndef LAYOUTATTRIBUTE_T3939207293_H
#define LAYOUTATTRIBUTE_T3939207293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Settings/LayoutAttribute
struct  LayoutAttribute_t3939207293  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTATTRIBUTE_T3939207293_H
#ifndef CHANNELMIXER_T2896160762_H
#define CHANNELMIXER_T2896160762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixer
struct  ChannelMixer_t2896160762  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELMIXER_T2896160762_H
#ifndef COLORGRADINGPRECISION_T3999355687_H
#define COLORGRADINGPRECISION_T3999355687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingPrecision
struct  ColorGradingPrecision_t3999355687 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingPrecision::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorGradingPrecision_t3999355687, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGPRECISION_T3999355687_H
#ifndef COLORWHEELGROUP_T3707110132_H
#define COLORWHEELGROUP_T3707110132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelGroup
struct  ColorWheelGroup_t3707110132  : public PropertyAttribute_t3677895545
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelGroup::minSizePerWheel
	int32_t ___minSizePerWheel_0;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelGroup::maxSizePerWheel
	int32_t ___maxSizePerWheel_1;

public:
	inline static int32_t get_offset_of_minSizePerWheel_0() { return static_cast<int32_t>(offsetof(ColorWheelGroup_t3707110132, ___minSizePerWheel_0)); }
	inline int32_t get_minSizePerWheel_0() const { return ___minSizePerWheel_0; }
	inline int32_t* get_address_of_minSizePerWheel_0() { return &___minSizePerWheel_0; }
	inline void set_minSizePerWheel_0(int32_t value)
	{
		___minSizePerWheel_0 = value;
	}

	inline static int32_t get_offset_of_maxSizePerWheel_1() { return static_cast<int32_t>(offsetof(ColorWheelGroup_t3707110132, ___maxSizePerWheel_1)); }
	inline int32_t get_maxSizePerWheel_1() const { return ___maxSizePerWheel_1; }
	inline int32_t* get_address_of_maxSizePerWheel_1() { return &___maxSizePerWheel_1; }
	inline void set_maxSizePerWheel_1(int32_t value)
	{
		___maxSizePerWheel_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELGROUP_T3707110132_H
#ifndef COLORWHEELSSETTINGS_T1334875082_H
#define COLORWHEELSSETTINGS_T1334875082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelsSettings
struct  ColorWheelsSettings_t1334875082 
{
public:
	// UnityEngine.Color UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelsSettings::shadows
	Color_t2555686324  ___shadows_0;
	// UnityEngine.Color UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelsSettings::midtones
	Color_t2555686324  ___midtones_1;
	// UnityEngine.Color UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelsSettings::highlights
	Color_t2555686324  ___highlights_2;

public:
	inline static int32_t get_offset_of_shadows_0() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t1334875082, ___shadows_0)); }
	inline Color_t2555686324  get_shadows_0() const { return ___shadows_0; }
	inline Color_t2555686324 * get_address_of_shadows_0() { return &___shadows_0; }
	inline void set_shadows_0(Color_t2555686324  value)
	{
		___shadows_0 = value;
	}

	inline static int32_t get_offset_of_midtones_1() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t1334875082, ___midtones_1)); }
	inline Color_t2555686324  get_midtones_1() const { return ___midtones_1; }
	inline Color_t2555686324 * get_address_of_midtones_1() { return &___midtones_1; }
	inline void set_midtones_1(Color_t2555686324  value)
	{
		___midtones_1 = value;
	}

	inline static int32_t get_offset_of_highlights_2() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t1334875082, ___highlights_2)); }
	inline Color_t2555686324  get_highlights_2() const { return ___highlights_2; }
	inline Color_t2555686324 * get_address_of_highlights_2() { return &___highlights_2; }
	inline void set_highlights_2(Color_t2555686324  value)
	{
		___highlights_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELSSETTINGS_T1334875082_H
#ifndef CURVE_T1983851910_H
#define CURVE_T1983851910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/Curve
struct  Curve_t1983851910  : public PropertyAttribute_t3677895545
{
public:
	// UnityEngine.Color UnityStandardAssets.CinematicEffects.TonemappingColorGrading/Curve::color
	Color_t2555686324  ___color_0;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(Curve_t1983851910, ___color_0)); }
	inline Color_t2555686324  get_color_0() const { return ___color_0; }
	inline Color_t2555686324 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_t2555686324  value)
	{
		___color_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE_T1983851910_H
#ifndef INDENTEDGROUP_T2664173756_H
#define INDENTEDGROUP_T2664173756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/IndentedGroup
struct  IndentedGroup_t2664173756  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDENTEDGROUP_T2664173756_H
#ifndef PASS_T1214953023_H
#define PASS_T1214953023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/Pass
struct  Pass_t1214953023 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading/Pass::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pass_t1214953023, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T1214953023_H
#ifndef TONEMAPPER_T2495473297_H
#define TONEMAPPER_T2495473297_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/Tonemapper
struct  Tonemapper_t2495473297 
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading/Tonemapper::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Tonemapper_t2495473297, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPER_T2495473297_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GLOBALSETTINGS_T956974295_H
#define GLOBALSETTINGS_T956974295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings
struct  GlobalSettings_t956974295 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::visualizeFocus
	bool ___visualizeFocus_0;
	// UnityStandardAssets.CinematicEffects.DepthOfField/TweakMode UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::tweakMode
	int32_t ___tweakMode_1;
	// UnityStandardAssets.CinematicEffects.DepthOfField/QualityPreset UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::filteringQuality
	int32_t ___filteringQuality_2;
	// UnityStandardAssets.CinematicEffects.DepthOfField/ApertureShape UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::apertureShape
	int32_t ___apertureShape_3;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings::apertureOrientation
	float ___apertureOrientation_4;

public:
	inline static int32_t get_offset_of_visualizeFocus_0() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___visualizeFocus_0)); }
	inline bool get_visualizeFocus_0() const { return ___visualizeFocus_0; }
	inline bool* get_address_of_visualizeFocus_0() { return &___visualizeFocus_0; }
	inline void set_visualizeFocus_0(bool value)
	{
		___visualizeFocus_0 = value;
	}

	inline static int32_t get_offset_of_tweakMode_1() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___tweakMode_1)); }
	inline int32_t get_tweakMode_1() const { return ___tweakMode_1; }
	inline int32_t* get_address_of_tweakMode_1() { return &___tweakMode_1; }
	inline void set_tweakMode_1(int32_t value)
	{
		___tweakMode_1 = value;
	}

	inline static int32_t get_offset_of_filteringQuality_2() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___filteringQuality_2)); }
	inline int32_t get_filteringQuality_2() const { return ___filteringQuality_2; }
	inline int32_t* get_address_of_filteringQuality_2() { return &___filteringQuality_2; }
	inline void set_filteringQuality_2(int32_t value)
	{
		___filteringQuality_2 = value;
	}

	inline static int32_t get_offset_of_apertureShape_3() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___apertureShape_3)); }
	inline int32_t get_apertureShape_3() const { return ___apertureShape_3; }
	inline int32_t* get_address_of_apertureShape_3() { return &___apertureShape_3; }
	inline void set_apertureShape_3(int32_t value)
	{
		___apertureShape_3 = value;
	}

	inline static int32_t get_offset_of_apertureOrientation_4() { return static_cast<int32_t>(offsetof(GlobalSettings_t956974295, ___apertureOrientation_4)); }
	inline float get_apertureOrientation_4() const { return ___apertureOrientation_4; }
	inline float* get_address_of_apertureOrientation_4() { return &___apertureOrientation_4; }
	inline void set_apertureOrientation_4(float value)
	{
		___apertureOrientation_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings
struct GlobalSettings_t956974295_marshaled_pinvoke
{
	int32_t ___visualizeFocus_0;
	int32_t ___tweakMode_1;
	int32_t ___filteringQuality_2;
	int32_t ___apertureShape_3;
	float ___apertureOrientation_4;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings
struct GlobalSettings_t956974295_marshaled_com
{
	int32_t ___visualizeFocus_0;
	int32_t ___tweakMode_1;
	int32_t ___filteringQuality_2;
	int32_t ___apertureShape_3;
	float ___apertureOrientation_4;
};
#endif // GLOBALSETTINGS_T956974295_H
#ifndef QUALITYSETTINGS_T1379802392_H
#define QUALITYSETTINGS_T1379802392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings
struct  QualitySettings_t1379802392 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings::prefilterBlur
	bool ___prefilterBlur_0;
	// UnityStandardAssets.CinematicEffects.DepthOfField/FilterQuality UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings::medianFilter
	int32_t ___medianFilter_1;
	// System.Boolean UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings::dilateNearBlur
	bool ___dilateNearBlur_2;

public:
	inline static int32_t get_offset_of_prefilterBlur_0() { return static_cast<int32_t>(offsetof(QualitySettings_t1379802392, ___prefilterBlur_0)); }
	inline bool get_prefilterBlur_0() const { return ___prefilterBlur_0; }
	inline bool* get_address_of_prefilterBlur_0() { return &___prefilterBlur_0; }
	inline void set_prefilterBlur_0(bool value)
	{
		___prefilterBlur_0 = value;
	}

	inline static int32_t get_offset_of_medianFilter_1() { return static_cast<int32_t>(offsetof(QualitySettings_t1379802392, ___medianFilter_1)); }
	inline int32_t get_medianFilter_1() const { return ___medianFilter_1; }
	inline int32_t* get_address_of_medianFilter_1() { return &___medianFilter_1; }
	inline void set_medianFilter_1(int32_t value)
	{
		___medianFilter_1 = value;
	}

	inline static int32_t get_offset_of_dilateNearBlur_2() { return static_cast<int32_t>(offsetof(QualitySettings_t1379802392, ___dilateNearBlur_2)); }
	inline bool get_dilateNearBlur_2() const { return ___dilateNearBlur_2; }
	inline bool* get_address_of_dilateNearBlur_2() { return &___dilateNearBlur_2; }
	inline void set_dilateNearBlur_2(bool value)
	{
		___dilateNearBlur_2 = value;
	}
};

struct QualitySettings_t1379802392_StaticFields
{
public:
	// UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings[] UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings::presetQualitySettings
	QualitySettingsU5BU5D_t3002864521* ___presetQualitySettings_3;

public:
	inline static int32_t get_offset_of_presetQualitySettings_3() { return static_cast<int32_t>(offsetof(QualitySettings_t1379802392_StaticFields, ___presetQualitySettings_3)); }
	inline QualitySettingsU5BU5D_t3002864521* get_presetQualitySettings_3() const { return ___presetQualitySettings_3; }
	inline QualitySettingsU5BU5D_t3002864521** get_address_of_presetQualitySettings_3() { return &___presetQualitySettings_3; }
	inline void set_presetQualitySettings_3(QualitySettingsU5BU5D_t3002864521* value)
	{
		___presetQualitySettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___presetQualitySettings_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings
struct QualitySettings_t1379802392_marshaled_pinvoke
{
	int32_t ___prefilterBlur_0;
	int32_t ___medianFilter_1;
	int32_t ___dilateNearBlur_2;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings
struct QualitySettings_t1379802392_marshaled_com
{
	int32_t ___prefilterBlur_0;
	int32_t ___medianFilter_1;
	int32_t ___dilateNearBlur_2;
};
#endif // QUALITYSETTINGS_T1379802392_H
#ifndef FRAMEBLENDINGFILTER_T4031465840_H
#define FRAMEBLENDINGFILTER_T4031465840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter
struct  FrameBlendingFilter_t4031465840  : public RuntimeObject
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_useCompression
	bool ____useCompression_0;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_rawTextureFormat
	int32_t ____rawTextureFormat_1;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_material
	Material_t340375123 * ____material_2;
	// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter/Frame[] UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_frameList
	FrameU5BU5D_t2667834693* ____frameList_3;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_lastFrameCount
	int32_t ____lastFrameCount_4;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History1LumaTex
	int32_t ____History1LumaTex_5;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History2LumaTex
	int32_t ____History2LumaTex_6;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History3LumaTex
	int32_t ____History3LumaTex_7;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History4LumaTex
	int32_t ____History4LumaTex_8;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History1ChromaTex
	int32_t ____History1ChromaTex_9;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History2ChromaTex
	int32_t ____History2ChromaTex_10;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History3ChromaTex
	int32_t ____History3ChromaTex_11;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History4ChromaTex
	int32_t ____History4ChromaTex_12;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History1Weight
	int32_t ____History1Weight_13;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History2Weight
	int32_t ____History2Weight_14;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History3Weight
	int32_t ____History3Weight_15;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter::_History4Weight
	int32_t ____History4Weight_16;

public:
	inline static int32_t get_offset_of__useCompression_0() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____useCompression_0)); }
	inline bool get__useCompression_0() const { return ____useCompression_0; }
	inline bool* get_address_of__useCompression_0() { return &____useCompression_0; }
	inline void set__useCompression_0(bool value)
	{
		____useCompression_0 = value;
	}

	inline static int32_t get_offset_of__rawTextureFormat_1() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____rawTextureFormat_1)); }
	inline int32_t get__rawTextureFormat_1() const { return ____rawTextureFormat_1; }
	inline int32_t* get_address_of__rawTextureFormat_1() { return &____rawTextureFormat_1; }
	inline void set__rawTextureFormat_1(int32_t value)
	{
		____rawTextureFormat_1 = value;
	}

	inline static int32_t get_offset_of__material_2() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____material_2)); }
	inline Material_t340375123 * get__material_2() const { return ____material_2; }
	inline Material_t340375123 ** get_address_of__material_2() { return &____material_2; }
	inline void set__material_2(Material_t340375123 * value)
	{
		____material_2 = value;
		Il2CppCodeGenWriteBarrier((&____material_2), value);
	}

	inline static int32_t get_offset_of__frameList_3() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____frameList_3)); }
	inline FrameU5BU5D_t2667834693* get__frameList_3() const { return ____frameList_3; }
	inline FrameU5BU5D_t2667834693** get_address_of__frameList_3() { return &____frameList_3; }
	inline void set__frameList_3(FrameU5BU5D_t2667834693* value)
	{
		____frameList_3 = value;
		Il2CppCodeGenWriteBarrier((&____frameList_3), value);
	}

	inline static int32_t get_offset_of__lastFrameCount_4() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____lastFrameCount_4)); }
	inline int32_t get__lastFrameCount_4() const { return ____lastFrameCount_4; }
	inline int32_t* get_address_of__lastFrameCount_4() { return &____lastFrameCount_4; }
	inline void set__lastFrameCount_4(int32_t value)
	{
		____lastFrameCount_4 = value;
	}

	inline static int32_t get_offset_of__History1LumaTex_5() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History1LumaTex_5)); }
	inline int32_t get__History1LumaTex_5() const { return ____History1LumaTex_5; }
	inline int32_t* get_address_of__History1LumaTex_5() { return &____History1LumaTex_5; }
	inline void set__History1LumaTex_5(int32_t value)
	{
		____History1LumaTex_5 = value;
	}

	inline static int32_t get_offset_of__History2LumaTex_6() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History2LumaTex_6)); }
	inline int32_t get__History2LumaTex_6() const { return ____History2LumaTex_6; }
	inline int32_t* get_address_of__History2LumaTex_6() { return &____History2LumaTex_6; }
	inline void set__History2LumaTex_6(int32_t value)
	{
		____History2LumaTex_6 = value;
	}

	inline static int32_t get_offset_of__History3LumaTex_7() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History3LumaTex_7)); }
	inline int32_t get__History3LumaTex_7() const { return ____History3LumaTex_7; }
	inline int32_t* get_address_of__History3LumaTex_7() { return &____History3LumaTex_7; }
	inline void set__History3LumaTex_7(int32_t value)
	{
		____History3LumaTex_7 = value;
	}

	inline static int32_t get_offset_of__History4LumaTex_8() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History4LumaTex_8)); }
	inline int32_t get__History4LumaTex_8() const { return ____History4LumaTex_8; }
	inline int32_t* get_address_of__History4LumaTex_8() { return &____History4LumaTex_8; }
	inline void set__History4LumaTex_8(int32_t value)
	{
		____History4LumaTex_8 = value;
	}

	inline static int32_t get_offset_of__History1ChromaTex_9() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History1ChromaTex_9)); }
	inline int32_t get__History1ChromaTex_9() const { return ____History1ChromaTex_9; }
	inline int32_t* get_address_of__History1ChromaTex_9() { return &____History1ChromaTex_9; }
	inline void set__History1ChromaTex_9(int32_t value)
	{
		____History1ChromaTex_9 = value;
	}

	inline static int32_t get_offset_of__History2ChromaTex_10() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History2ChromaTex_10)); }
	inline int32_t get__History2ChromaTex_10() const { return ____History2ChromaTex_10; }
	inline int32_t* get_address_of__History2ChromaTex_10() { return &____History2ChromaTex_10; }
	inline void set__History2ChromaTex_10(int32_t value)
	{
		____History2ChromaTex_10 = value;
	}

	inline static int32_t get_offset_of__History3ChromaTex_11() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History3ChromaTex_11)); }
	inline int32_t get__History3ChromaTex_11() const { return ____History3ChromaTex_11; }
	inline int32_t* get_address_of__History3ChromaTex_11() { return &____History3ChromaTex_11; }
	inline void set__History3ChromaTex_11(int32_t value)
	{
		____History3ChromaTex_11 = value;
	}

	inline static int32_t get_offset_of__History4ChromaTex_12() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History4ChromaTex_12)); }
	inline int32_t get__History4ChromaTex_12() const { return ____History4ChromaTex_12; }
	inline int32_t* get_address_of__History4ChromaTex_12() { return &____History4ChromaTex_12; }
	inline void set__History4ChromaTex_12(int32_t value)
	{
		____History4ChromaTex_12 = value;
	}

	inline static int32_t get_offset_of__History1Weight_13() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History1Weight_13)); }
	inline int32_t get__History1Weight_13() const { return ____History1Weight_13; }
	inline int32_t* get_address_of__History1Weight_13() { return &____History1Weight_13; }
	inline void set__History1Weight_13(int32_t value)
	{
		____History1Weight_13 = value;
	}

	inline static int32_t get_offset_of__History2Weight_14() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History2Weight_14)); }
	inline int32_t get__History2Weight_14() const { return ____History2Weight_14; }
	inline int32_t* get_address_of__History2Weight_14() { return &____History2Weight_14; }
	inline void set__History2Weight_14(int32_t value)
	{
		____History2Weight_14 = value;
	}

	inline static int32_t get_offset_of__History3Weight_15() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History3Weight_15)); }
	inline int32_t get__History3Weight_15() const { return ____History3Weight_15; }
	inline int32_t* get_address_of__History3Weight_15() { return &____History3Weight_15; }
	inline void set__History3Weight_15(int32_t value)
	{
		____History3Weight_15 = value;
	}

	inline static int32_t get_offset_of__History4Weight_16() { return static_cast<int32_t>(offsetof(FrameBlendingFilter_t4031465840, ____History4Weight_16)); }
	inline int32_t get__History4Weight_16() const { return ____History4Weight_16; }
	inline int32_t* get_address_of__History4Weight_16() { return &____History4Weight_16; }
	inline void set__History4Weight_16(int32_t value)
	{
		____History4Weight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEBLENDINGFILTER_T4031465840_H
#ifndef RECONSTRUCTIONFILTER_T4011033762_H
#define RECONSTRUCTIONFILTER_T4011033762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter
struct  ReconstructionFilter_t4011033762  : public RuntimeObject
{
public:
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_material
	Material_t340375123 * ____material_1;
	// System.Boolean UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_unroll
	bool ____unroll_2;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_vectorRTFormat
	int32_t ____vectorRTFormat_3;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_packedRTFormat
	int32_t ____packedRTFormat_4;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_VelocityScale
	int32_t ____VelocityScale_5;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_MaxBlurRadius
	int32_t ____MaxBlurRadius_6;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_TileMaxOffs
	int32_t ____TileMaxOffs_7;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_TileMaxLoop
	int32_t ____TileMaxLoop_8;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_LoopCount
	int32_t ____LoopCount_9;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_NeighborMaxTex
	int32_t ____NeighborMaxTex_10;
	// System.Int32 UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter::_VelocityTex
	int32_t ____VelocityTex_11;

public:
	inline static int32_t get_offset_of__material_1() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____material_1)); }
	inline Material_t340375123 * get__material_1() const { return ____material_1; }
	inline Material_t340375123 ** get_address_of__material_1() { return &____material_1; }
	inline void set__material_1(Material_t340375123 * value)
	{
		____material_1 = value;
		Il2CppCodeGenWriteBarrier((&____material_1), value);
	}

	inline static int32_t get_offset_of__unroll_2() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____unroll_2)); }
	inline bool get__unroll_2() const { return ____unroll_2; }
	inline bool* get_address_of__unroll_2() { return &____unroll_2; }
	inline void set__unroll_2(bool value)
	{
		____unroll_2 = value;
	}

	inline static int32_t get_offset_of__vectorRTFormat_3() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____vectorRTFormat_3)); }
	inline int32_t get__vectorRTFormat_3() const { return ____vectorRTFormat_3; }
	inline int32_t* get_address_of__vectorRTFormat_3() { return &____vectorRTFormat_3; }
	inline void set__vectorRTFormat_3(int32_t value)
	{
		____vectorRTFormat_3 = value;
	}

	inline static int32_t get_offset_of__packedRTFormat_4() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____packedRTFormat_4)); }
	inline int32_t get__packedRTFormat_4() const { return ____packedRTFormat_4; }
	inline int32_t* get_address_of__packedRTFormat_4() { return &____packedRTFormat_4; }
	inline void set__packedRTFormat_4(int32_t value)
	{
		____packedRTFormat_4 = value;
	}

	inline static int32_t get_offset_of__VelocityScale_5() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____VelocityScale_5)); }
	inline int32_t get__VelocityScale_5() const { return ____VelocityScale_5; }
	inline int32_t* get_address_of__VelocityScale_5() { return &____VelocityScale_5; }
	inline void set__VelocityScale_5(int32_t value)
	{
		____VelocityScale_5 = value;
	}

	inline static int32_t get_offset_of__MaxBlurRadius_6() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____MaxBlurRadius_6)); }
	inline int32_t get__MaxBlurRadius_6() const { return ____MaxBlurRadius_6; }
	inline int32_t* get_address_of__MaxBlurRadius_6() { return &____MaxBlurRadius_6; }
	inline void set__MaxBlurRadius_6(int32_t value)
	{
		____MaxBlurRadius_6 = value;
	}

	inline static int32_t get_offset_of__TileMaxOffs_7() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____TileMaxOffs_7)); }
	inline int32_t get__TileMaxOffs_7() const { return ____TileMaxOffs_7; }
	inline int32_t* get_address_of__TileMaxOffs_7() { return &____TileMaxOffs_7; }
	inline void set__TileMaxOffs_7(int32_t value)
	{
		____TileMaxOffs_7 = value;
	}

	inline static int32_t get_offset_of__TileMaxLoop_8() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____TileMaxLoop_8)); }
	inline int32_t get__TileMaxLoop_8() const { return ____TileMaxLoop_8; }
	inline int32_t* get_address_of__TileMaxLoop_8() { return &____TileMaxLoop_8; }
	inline void set__TileMaxLoop_8(int32_t value)
	{
		____TileMaxLoop_8 = value;
	}

	inline static int32_t get_offset_of__LoopCount_9() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____LoopCount_9)); }
	inline int32_t get__LoopCount_9() const { return ____LoopCount_9; }
	inline int32_t* get_address_of__LoopCount_9() { return &____LoopCount_9; }
	inline void set__LoopCount_9(int32_t value)
	{
		____LoopCount_9 = value;
	}

	inline static int32_t get_offset_of__NeighborMaxTex_10() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____NeighborMaxTex_10)); }
	inline int32_t get__NeighborMaxTex_10() const { return ____NeighborMaxTex_10; }
	inline int32_t* get_address_of__NeighborMaxTex_10() { return &____NeighborMaxTex_10; }
	inline void set__NeighborMaxTex_10(int32_t value)
	{
		____NeighborMaxTex_10 = value;
	}

	inline static int32_t get_offset_of__VelocityTex_11() { return static_cast<int32_t>(offsetof(ReconstructionFilter_t4011033762, ____VelocityTex_11)); }
	inline int32_t get__VelocityTex_11() const { return ____VelocityTex_11; }
	inline int32_t* get_address_of__VelocityTex_11() { return &____VelocityTex_11; }
	inline void set__VelocityTex_11(int32_t value)
	{
		____VelocityTex_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFILTER_T4011033762_H
#ifndef REFLECTIONSETTINGS_T499321483_H
#define REFLECTIONSETTINGS_T499321483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings
struct  ReflectionSettings_t499321483 
{
public:
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRReflectionBlendType UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings::blendType
	int32_t ___blendType_0;
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRResolution UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings::reflectionQuality
	int32_t ___reflectionQuality_1;
	// System.Single UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings::maxDistance
	float ___maxDistance_2;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings::iterationCount
	int32_t ___iterationCount_3;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings::stepSize
	int32_t ___stepSize_4;
	// System.Single UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings::widthModifier
	float ___widthModifier_5;
	// System.Single UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings::reflectionBlur
	float ___reflectionBlur_6;
	// System.Boolean UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings::reflectBackfaces
	bool ___reflectBackfaces_7;

public:
	inline static int32_t get_offset_of_blendType_0() { return static_cast<int32_t>(offsetof(ReflectionSettings_t499321483, ___blendType_0)); }
	inline int32_t get_blendType_0() const { return ___blendType_0; }
	inline int32_t* get_address_of_blendType_0() { return &___blendType_0; }
	inline void set_blendType_0(int32_t value)
	{
		___blendType_0 = value;
	}

	inline static int32_t get_offset_of_reflectionQuality_1() { return static_cast<int32_t>(offsetof(ReflectionSettings_t499321483, ___reflectionQuality_1)); }
	inline int32_t get_reflectionQuality_1() const { return ___reflectionQuality_1; }
	inline int32_t* get_address_of_reflectionQuality_1() { return &___reflectionQuality_1; }
	inline void set_reflectionQuality_1(int32_t value)
	{
		___reflectionQuality_1 = value;
	}

	inline static int32_t get_offset_of_maxDistance_2() { return static_cast<int32_t>(offsetof(ReflectionSettings_t499321483, ___maxDistance_2)); }
	inline float get_maxDistance_2() const { return ___maxDistance_2; }
	inline float* get_address_of_maxDistance_2() { return &___maxDistance_2; }
	inline void set_maxDistance_2(float value)
	{
		___maxDistance_2 = value;
	}

	inline static int32_t get_offset_of_iterationCount_3() { return static_cast<int32_t>(offsetof(ReflectionSettings_t499321483, ___iterationCount_3)); }
	inline int32_t get_iterationCount_3() const { return ___iterationCount_3; }
	inline int32_t* get_address_of_iterationCount_3() { return &___iterationCount_3; }
	inline void set_iterationCount_3(int32_t value)
	{
		___iterationCount_3 = value;
	}

	inline static int32_t get_offset_of_stepSize_4() { return static_cast<int32_t>(offsetof(ReflectionSettings_t499321483, ___stepSize_4)); }
	inline int32_t get_stepSize_4() const { return ___stepSize_4; }
	inline int32_t* get_address_of_stepSize_4() { return &___stepSize_4; }
	inline void set_stepSize_4(int32_t value)
	{
		___stepSize_4 = value;
	}

	inline static int32_t get_offset_of_widthModifier_5() { return static_cast<int32_t>(offsetof(ReflectionSettings_t499321483, ___widthModifier_5)); }
	inline float get_widthModifier_5() const { return ___widthModifier_5; }
	inline float* get_address_of_widthModifier_5() { return &___widthModifier_5; }
	inline void set_widthModifier_5(float value)
	{
		___widthModifier_5 = value;
	}

	inline static int32_t get_offset_of_reflectionBlur_6() { return static_cast<int32_t>(offsetof(ReflectionSettings_t499321483, ___reflectionBlur_6)); }
	inline float get_reflectionBlur_6() const { return ___reflectionBlur_6; }
	inline float* get_address_of_reflectionBlur_6() { return &___reflectionBlur_6; }
	inline void set_reflectionBlur_6(float value)
	{
		___reflectionBlur_6 = value;
	}

	inline static int32_t get_offset_of_reflectBackfaces_7() { return static_cast<int32_t>(offsetof(ReflectionSettings_t499321483, ___reflectBackfaces_7)); }
	inline bool get_reflectBackfaces_7() const { return ___reflectBackfaces_7; }
	inline bool* get_address_of_reflectBackfaces_7() { return &___reflectBackfaces_7; }
	inline void set_reflectBackfaces_7(bool value)
	{
		___reflectBackfaces_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings
struct ReflectionSettings_t499321483_marshaled_pinvoke
{
	int32_t ___blendType_0;
	int32_t ___reflectionQuality_1;
	float ___maxDistance_2;
	int32_t ___iterationCount_3;
	int32_t ___stepSize_4;
	float ___widthModifier_5;
	float ___reflectionBlur_6;
	int32_t ___reflectBackfaces_7;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings
struct ReflectionSettings_t499321483_marshaled_com
{
	int32_t ___blendType_0;
	int32_t ___reflectionQuality_1;
	float ___maxDistance_2;
	int32_t ___iterationCount_3;
	int32_t ___stepSize_4;
	float ___widthModifier_5;
	float ___reflectionBlur_6;
	int32_t ___reflectBackfaces_7;
};
#endif // REFLECTIONSETTINGS_T499321483_H
#ifndef JITTERSETTINGS_T1123664989_H
#define JITTERSETTINGS_T1123664989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/JitterSettings
struct  JitterSettings_t1123664989 
{
public:
	// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Sequence UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/JitterSettings::sequence
	int32_t ___sequence_0;
	// System.Single UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/JitterSettings::spread
	float ___spread_1;
	// System.Int32 UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/JitterSettings::sampleCount
	int32_t ___sampleCount_2;

public:
	inline static int32_t get_offset_of_sequence_0() { return static_cast<int32_t>(offsetof(JitterSettings_t1123664989, ___sequence_0)); }
	inline int32_t get_sequence_0() const { return ___sequence_0; }
	inline int32_t* get_address_of_sequence_0() { return &___sequence_0; }
	inline void set_sequence_0(int32_t value)
	{
		___sequence_0 = value;
	}

	inline static int32_t get_offset_of_spread_1() { return static_cast<int32_t>(offsetof(JitterSettings_t1123664989, ___spread_1)); }
	inline float get_spread_1() const { return ___spread_1; }
	inline float* get_address_of_spread_1() { return &___spread_1; }
	inline void set_spread_1(float value)
	{
		___spread_1 = value;
	}

	inline static int32_t get_offset_of_sampleCount_2() { return static_cast<int32_t>(offsetof(JitterSettings_t1123664989, ___sampleCount_2)); }
	inline int32_t get_sampleCount_2() const { return ___sampleCount_2; }
	inline int32_t* get_address_of_sampleCount_2() { return &___sampleCount_2; }
	inline void set_sampleCount_2(int32_t value)
	{
		___sampleCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JITTERSETTINGS_T1123664989_H
#ifndef CURVESSETTINGS_T2932745847_H
#define CURVESSETTINGS_T2932745847_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings
struct  CurvesSettings_t2932745847 
{
public:
	// UnityEngine.AnimationCurve UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings::master
	AnimationCurve_t3046754366 * ___master_0;
	// UnityEngine.AnimationCurve UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings::red
	AnimationCurve_t3046754366 * ___red_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings::green
	AnimationCurve_t3046754366 * ___green_2;
	// UnityEngine.AnimationCurve UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings::blue
	AnimationCurve_t3046754366 * ___blue_3;

public:
	inline static int32_t get_offset_of_master_0() { return static_cast<int32_t>(offsetof(CurvesSettings_t2932745847, ___master_0)); }
	inline AnimationCurve_t3046754366 * get_master_0() const { return ___master_0; }
	inline AnimationCurve_t3046754366 ** get_address_of_master_0() { return &___master_0; }
	inline void set_master_0(AnimationCurve_t3046754366 * value)
	{
		___master_0 = value;
		Il2CppCodeGenWriteBarrier((&___master_0), value);
	}

	inline static int32_t get_offset_of_red_1() { return static_cast<int32_t>(offsetof(CurvesSettings_t2932745847, ___red_1)); }
	inline AnimationCurve_t3046754366 * get_red_1() const { return ___red_1; }
	inline AnimationCurve_t3046754366 ** get_address_of_red_1() { return &___red_1; }
	inline void set_red_1(AnimationCurve_t3046754366 * value)
	{
		___red_1 = value;
		Il2CppCodeGenWriteBarrier((&___red_1), value);
	}

	inline static int32_t get_offset_of_green_2() { return static_cast<int32_t>(offsetof(CurvesSettings_t2932745847, ___green_2)); }
	inline AnimationCurve_t3046754366 * get_green_2() const { return ___green_2; }
	inline AnimationCurve_t3046754366 ** get_address_of_green_2() { return &___green_2; }
	inline void set_green_2(AnimationCurve_t3046754366 * value)
	{
		___green_2 = value;
		Il2CppCodeGenWriteBarrier((&___green_2), value);
	}

	inline static int32_t get_offset_of_blue_3() { return static_cast<int32_t>(offsetof(CurvesSettings_t2932745847, ___blue_3)); }
	inline AnimationCurve_t3046754366 * get_blue_3() const { return ___blue_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_blue_3() { return &___blue_3; }
	inline void set_blue_3(AnimationCurve_t3046754366 * value)
	{
		___blue_3 = value;
		Il2CppCodeGenWriteBarrier((&___blue_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings
struct CurvesSettings_t2932745847_marshaled_pinvoke
{
	AnimationCurve_t3046754366_marshaled_pinvoke ___master_0;
	AnimationCurve_t3046754366_marshaled_pinvoke ___red_1;
	AnimationCurve_t3046754366_marshaled_pinvoke ___green_2;
	AnimationCurve_t3046754366_marshaled_pinvoke ___blue_3;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings
struct CurvesSettings_t2932745847_marshaled_com
{
	AnimationCurve_t3046754366_marshaled_com* ___master_0;
	AnimationCurve_t3046754366_marshaled_com* ___red_1;
	AnimationCurve_t3046754366_marshaled_com* ___green_2;
	AnimationCurve_t3046754366_marshaled_com* ___blue_3;
};
#endif // CURVESSETTINGS_T2932745847_H
#ifndef TONEMAPPINGSETTINGS_T1161324624_H
#define TONEMAPPINGSETTINGS_T1161324624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings
struct  TonemappingSettings_t1161324624 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::enabled
	bool ___enabled_0;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/Tonemapper UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::tonemapper
	int32_t ___tonemapper_1;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::exposure
	float ___exposure_2;
	// UnityEngine.AnimationCurve UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::curve
	AnimationCurve_t3046754366 * ___curve_3;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::neutralBlackIn
	float ___neutralBlackIn_4;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::neutralWhiteIn
	float ___neutralWhiteIn_5;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::neutralBlackOut
	float ___neutralBlackOut_6;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::neutralWhiteOut
	float ___neutralWhiteOut_7;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::neutralWhiteLevel
	float ___neutralWhiteLevel_8;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings::neutralWhiteClip
	float ___neutralWhiteClip_9;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_tonemapper_1() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___tonemapper_1)); }
	inline int32_t get_tonemapper_1() const { return ___tonemapper_1; }
	inline int32_t* get_address_of_tonemapper_1() { return &___tonemapper_1; }
	inline void set_tonemapper_1(int32_t value)
	{
		___tonemapper_1 = value;
	}

	inline static int32_t get_offset_of_exposure_2() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___exposure_2)); }
	inline float get_exposure_2() const { return ___exposure_2; }
	inline float* get_address_of_exposure_2() { return &___exposure_2; }
	inline void set_exposure_2(float value)
	{
		___exposure_2 = value;
	}

	inline static int32_t get_offset_of_curve_3() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___curve_3)); }
	inline AnimationCurve_t3046754366 * get_curve_3() const { return ___curve_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_3() { return &___curve_3; }
	inline void set_curve_3(AnimationCurve_t3046754366 * value)
	{
		___curve_3 = value;
		Il2CppCodeGenWriteBarrier((&___curve_3), value);
	}

	inline static int32_t get_offset_of_neutralBlackIn_4() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___neutralBlackIn_4)); }
	inline float get_neutralBlackIn_4() const { return ___neutralBlackIn_4; }
	inline float* get_address_of_neutralBlackIn_4() { return &___neutralBlackIn_4; }
	inline void set_neutralBlackIn_4(float value)
	{
		___neutralBlackIn_4 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteIn_5() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___neutralWhiteIn_5)); }
	inline float get_neutralWhiteIn_5() const { return ___neutralWhiteIn_5; }
	inline float* get_address_of_neutralWhiteIn_5() { return &___neutralWhiteIn_5; }
	inline void set_neutralWhiteIn_5(float value)
	{
		___neutralWhiteIn_5 = value;
	}

	inline static int32_t get_offset_of_neutralBlackOut_6() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___neutralBlackOut_6)); }
	inline float get_neutralBlackOut_6() const { return ___neutralBlackOut_6; }
	inline float* get_address_of_neutralBlackOut_6() { return &___neutralBlackOut_6; }
	inline void set_neutralBlackOut_6(float value)
	{
		___neutralBlackOut_6 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteOut_7() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___neutralWhiteOut_7)); }
	inline float get_neutralWhiteOut_7() const { return ___neutralWhiteOut_7; }
	inline float* get_address_of_neutralWhiteOut_7() { return &___neutralWhiteOut_7; }
	inline void set_neutralWhiteOut_7(float value)
	{
		___neutralWhiteOut_7 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteLevel_8() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___neutralWhiteLevel_8)); }
	inline float get_neutralWhiteLevel_8() const { return ___neutralWhiteLevel_8; }
	inline float* get_address_of_neutralWhiteLevel_8() { return &___neutralWhiteLevel_8; }
	inline void set_neutralWhiteLevel_8(float value)
	{
		___neutralWhiteLevel_8 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteClip_9() { return static_cast<int32_t>(offsetof(TonemappingSettings_t1161324624, ___neutralWhiteClip_9)); }
	inline float get_neutralWhiteClip_9() const { return ___neutralWhiteClip_9; }
	inline float* get_address_of_neutralWhiteClip_9() { return &___neutralWhiteClip_9; }
	inline void set_neutralWhiteClip_9(float value)
	{
		___neutralWhiteClip_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings
struct TonemappingSettings_t1161324624_marshaled_pinvoke
{
	int32_t ___enabled_0;
	int32_t ___tonemapper_1;
	float ___exposure_2;
	AnimationCurve_t3046754366_marshaled_pinvoke ___curve_3;
	float ___neutralBlackIn_4;
	float ___neutralWhiteIn_5;
	float ___neutralBlackOut_6;
	float ___neutralWhiteOut_7;
	float ___neutralWhiteLevel_8;
	float ___neutralWhiteClip_9;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings
struct TonemappingSettings_t1161324624_marshaled_com
{
	int32_t ___enabled_0;
	int32_t ___tonemapper_1;
	float ___exposure_2;
	AnimationCurve_t3046754366_marshaled_com* ___curve_3;
	float ___neutralBlackIn_4;
	float ___neutralWhiteIn_5;
	float ___neutralBlackOut_6;
	float ___neutralWhiteOut_7;
	float ___neutralWhiteLevel_8;
	float ___neutralWhiteClip_9;
};
#endif // TONEMAPPINGSETTINGS_T1161324624_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef SSRSETTINGS_T899296535_H
#define SSRSETTINGS_T899296535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings
struct  SSRSettings_t899296535 
{
public:
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ReflectionSettings UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings::reflectionSettings
	ReflectionSettings_t499321483  ___reflectionSettings_0;
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/IntensitySettings UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings::intensitySettings
	IntensitySettings_t3790221060  ___intensitySettings_1;
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/ScreenEdgeMask UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings::screenEdgeMask
	ScreenEdgeMask_t3779436815  ___screenEdgeMask_2;

public:
	inline static int32_t get_offset_of_reflectionSettings_0() { return static_cast<int32_t>(offsetof(SSRSettings_t899296535, ___reflectionSettings_0)); }
	inline ReflectionSettings_t499321483  get_reflectionSettings_0() const { return ___reflectionSettings_0; }
	inline ReflectionSettings_t499321483 * get_address_of_reflectionSettings_0() { return &___reflectionSettings_0; }
	inline void set_reflectionSettings_0(ReflectionSettings_t499321483  value)
	{
		___reflectionSettings_0 = value;
	}

	inline static int32_t get_offset_of_intensitySettings_1() { return static_cast<int32_t>(offsetof(SSRSettings_t899296535, ___intensitySettings_1)); }
	inline IntensitySettings_t3790221060  get_intensitySettings_1() const { return ___intensitySettings_1; }
	inline IntensitySettings_t3790221060 * get_address_of_intensitySettings_1() { return &___intensitySettings_1; }
	inline void set_intensitySettings_1(IntensitySettings_t3790221060  value)
	{
		___intensitySettings_1 = value;
	}

	inline static int32_t get_offset_of_screenEdgeMask_2() { return static_cast<int32_t>(offsetof(SSRSettings_t899296535, ___screenEdgeMask_2)); }
	inline ScreenEdgeMask_t3779436815  get_screenEdgeMask_2() const { return ___screenEdgeMask_2; }
	inline ScreenEdgeMask_t3779436815 * get_address_of_screenEdgeMask_2() { return &___screenEdgeMask_2; }
	inline void set_screenEdgeMask_2(ScreenEdgeMask_t3779436815  value)
	{
		___screenEdgeMask_2 = value;
	}
};

struct SSRSettings_t899296535_StaticFields
{
public:
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings::s_Default
	SSRSettings_t899296535  ___s_Default_3;

public:
	inline static int32_t get_offset_of_s_Default_3() { return static_cast<int32_t>(offsetof(SSRSettings_t899296535_StaticFields, ___s_Default_3)); }
	inline SSRSettings_t899296535  get_s_Default_3() const { return ___s_Default_3; }
	inline SSRSettings_t899296535 * get_address_of_s_Default_3() { return &___s_Default_3; }
	inline void set_s_Default_3(SSRSettings_t899296535  value)
	{
		___s_Default_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings
struct SSRSettings_t899296535_marshaled_pinvoke
{
	ReflectionSettings_t499321483_marshaled_pinvoke ___reflectionSettings_0;
	IntensitySettings_t3790221060  ___intensitySettings_1;
	ScreenEdgeMask_t3779436815  ___screenEdgeMask_2;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings
struct SSRSettings_t899296535_marshaled_com
{
	ReflectionSettings_t499321483_marshaled_com ___reflectionSettings_0;
	IntensitySettings_t3790221060  ___intensitySettings_1;
	ScreenEdgeMask_t3779436815  ___screenEdgeMask_2;
};
#endif // SSRSETTINGS_T899296535_H
#ifndef SETTINGS_T254817451_H
#define SETTINGS_T254817451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Settings
struct  Settings_t254817451  : public RuntimeObject
{
public:
	// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/JitterSettings UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Settings::jitterSettings
	JitterSettings_t1123664989  ___jitterSettings_0;
	// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/SharpenFilterSettings UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Settings::sharpenFilterSettings
	SharpenFilterSettings_t2911684052  ___sharpenFilterSettings_1;
	// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/BlendSettings UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Settings::blendSettings
	BlendSettings_t3588719326  ___blendSettings_2;
	// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/DebugSettings UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Settings::debugSettings
	DebugSettings_t54746148  ___debugSettings_3;

public:
	inline static int32_t get_offset_of_jitterSettings_0() { return static_cast<int32_t>(offsetof(Settings_t254817451, ___jitterSettings_0)); }
	inline JitterSettings_t1123664989  get_jitterSettings_0() const { return ___jitterSettings_0; }
	inline JitterSettings_t1123664989 * get_address_of_jitterSettings_0() { return &___jitterSettings_0; }
	inline void set_jitterSettings_0(JitterSettings_t1123664989  value)
	{
		___jitterSettings_0 = value;
	}

	inline static int32_t get_offset_of_sharpenFilterSettings_1() { return static_cast<int32_t>(offsetof(Settings_t254817451, ___sharpenFilterSettings_1)); }
	inline SharpenFilterSettings_t2911684052  get_sharpenFilterSettings_1() const { return ___sharpenFilterSettings_1; }
	inline SharpenFilterSettings_t2911684052 * get_address_of_sharpenFilterSettings_1() { return &___sharpenFilterSettings_1; }
	inline void set_sharpenFilterSettings_1(SharpenFilterSettings_t2911684052  value)
	{
		___sharpenFilterSettings_1 = value;
	}

	inline static int32_t get_offset_of_blendSettings_2() { return static_cast<int32_t>(offsetof(Settings_t254817451, ___blendSettings_2)); }
	inline BlendSettings_t3588719326  get_blendSettings_2() const { return ___blendSettings_2; }
	inline BlendSettings_t3588719326 * get_address_of_blendSettings_2() { return &___blendSettings_2; }
	inline void set_blendSettings_2(BlendSettings_t3588719326  value)
	{
		___blendSettings_2 = value;
	}

	inline static int32_t get_offset_of_debugSettings_3() { return static_cast<int32_t>(offsetof(Settings_t254817451, ___debugSettings_3)); }
	inline DebugSettings_t54746148  get_debugSettings_3() const { return ___debugSettings_3; }
	inline DebugSettings_t54746148 * get_address_of_debugSettings_3() { return &___debugSettings_3; }
	inline void set_debugSettings_3(DebugSettings_t54746148  value)
	{
		___debugSettings_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T254817451_H
#ifndef COLORGRADINGSETTINGS_T1923563914_H
#define COLORGRADINGSETTINGS_T1923563914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings
struct  ColorGradingSettings_t1923563914 
{
public:
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::enabled
	bool ___enabled_0;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingPrecision UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::precision
	int32_t ___precision_1;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorWheelsSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::colorWheels
	ColorWheelsSettings_t1334875082  ___colorWheels_2;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/BasicsSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::basics
	BasicsSettings_t2030707387  ___basics_3;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ChannelMixerSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::channelMixer
	ChannelMixerSettings_t491321292  ___channelMixer_4;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/CurvesSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::curves
	CurvesSettings_t2932745847  ___curves_5;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::useDithering
	bool ___useDithering_6;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings::showDebug
	bool ___showDebug_7;

public:
	inline static int32_t get_offset_of_enabled_0() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___enabled_0)); }
	inline bool get_enabled_0() const { return ___enabled_0; }
	inline bool* get_address_of_enabled_0() { return &___enabled_0; }
	inline void set_enabled_0(bool value)
	{
		___enabled_0 = value;
	}

	inline static int32_t get_offset_of_precision_1() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___precision_1)); }
	inline int32_t get_precision_1() const { return ___precision_1; }
	inline int32_t* get_address_of_precision_1() { return &___precision_1; }
	inline void set_precision_1(int32_t value)
	{
		___precision_1 = value;
	}

	inline static int32_t get_offset_of_colorWheels_2() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___colorWheels_2)); }
	inline ColorWheelsSettings_t1334875082  get_colorWheels_2() const { return ___colorWheels_2; }
	inline ColorWheelsSettings_t1334875082 * get_address_of_colorWheels_2() { return &___colorWheels_2; }
	inline void set_colorWheels_2(ColorWheelsSettings_t1334875082  value)
	{
		___colorWheels_2 = value;
	}

	inline static int32_t get_offset_of_basics_3() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___basics_3)); }
	inline BasicsSettings_t2030707387  get_basics_3() const { return ___basics_3; }
	inline BasicsSettings_t2030707387 * get_address_of_basics_3() { return &___basics_3; }
	inline void set_basics_3(BasicsSettings_t2030707387  value)
	{
		___basics_3 = value;
	}

	inline static int32_t get_offset_of_channelMixer_4() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___channelMixer_4)); }
	inline ChannelMixerSettings_t491321292  get_channelMixer_4() const { return ___channelMixer_4; }
	inline ChannelMixerSettings_t491321292 * get_address_of_channelMixer_4() { return &___channelMixer_4; }
	inline void set_channelMixer_4(ChannelMixerSettings_t491321292  value)
	{
		___channelMixer_4 = value;
	}

	inline static int32_t get_offset_of_curves_5() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___curves_5)); }
	inline CurvesSettings_t2932745847  get_curves_5() const { return ___curves_5; }
	inline CurvesSettings_t2932745847 * get_address_of_curves_5() { return &___curves_5; }
	inline void set_curves_5(CurvesSettings_t2932745847  value)
	{
		___curves_5 = value;
	}

	inline static int32_t get_offset_of_useDithering_6() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___useDithering_6)); }
	inline bool get_useDithering_6() const { return ___useDithering_6; }
	inline bool* get_address_of_useDithering_6() { return &___useDithering_6; }
	inline void set_useDithering_6(bool value)
	{
		___useDithering_6 = value;
	}

	inline static int32_t get_offset_of_showDebug_7() { return static_cast<int32_t>(offsetof(ColorGradingSettings_t1923563914, ___showDebug_7)); }
	inline bool get_showDebug_7() const { return ___showDebug_7; }
	inline bool* get_address_of_showDebug_7() { return &___showDebug_7; }
	inline void set_showDebug_7(bool value)
	{
		___showDebug_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings
struct ColorGradingSettings_t1923563914_marshaled_pinvoke
{
	int32_t ___enabled_0;
	int32_t ___precision_1;
	ColorWheelsSettings_t1334875082  ___colorWheels_2;
	BasicsSettings_t2030707387  ___basics_3;
	ChannelMixerSettings_t491321292_marshaled_pinvoke ___channelMixer_4;
	CurvesSettings_t2932745847_marshaled_pinvoke ___curves_5;
	int32_t ___useDithering_6;
	int32_t ___showDebug_7;
};
// Native definition for COM marshalling of UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings
struct ColorGradingSettings_t1923563914_marshaled_com
{
	int32_t ___enabled_0;
	int32_t ___precision_1;
	ColorWheelsSettings_t1334875082  ___colorWheels_2;
	BasicsSettings_t2030707387  ___basics_3;
	ChannelMixerSettings_t491321292_marshaled_com ___channelMixer_4;
	CurvesSettings_t2932745847_marshaled_com ___curves_5;
	int32_t ___useDithering_6;
	int32_t ___showDebug_7;
};
#endif // COLORGRADINGSETTINGS_T1923563914_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEPTHOFFIELD_T2645519978_H
#define DEPTHOFFIELD_T2645519978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.DepthOfField
struct  DepthOfField_t2645519978  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.DepthOfField/GlobalSettings UnityStandardAssets.CinematicEffects.DepthOfField::settings
	GlobalSettings_t956974295  ___settings_5;
	// UnityStandardAssets.CinematicEffects.DepthOfField/FocusSettings UnityStandardAssets.CinematicEffects.DepthOfField::focus
	FocusSettings_t1261455774  ___focus_6;
	// UnityStandardAssets.CinematicEffects.DepthOfField/BokehTextureSettings UnityStandardAssets.CinematicEffects.DepthOfField::bokehTexture
	BokehTextureSettings_t3943002634  ___bokehTexture_7;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.DepthOfField::m_FilmicDepthOfFieldShader
	Shader_t4151988712 * ___m_FilmicDepthOfFieldShader_8;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.DepthOfField::m_MedianFilterShader
	Shader_t4151988712 * ___m_MedianFilterShader_9;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.DepthOfField::m_TextureBokehShader
	Shader_t4151988712 * ___m_TextureBokehShader_10;
	// UnityStandardAssets.CinematicEffects.RenderTextureUtility UnityStandardAssets.CinematicEffects.DepthOfField::m_RTU
	RenderTextureUtility_t1946006342 * ___m_RTU_11;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.DepthOfField::m_FilmicDepthOfFieldMaterial
	Material_t340375123 * ___m_FilmicDepthOfFieldMaterial_12;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.DepthOfField::m_MedianFilterMaterial
	Material_t340375123 * ___m_MedianFilterMaterial_13;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.DepthOfField::m_TextureBokehMaterial
	Material_t340375123 * ___m_TextureBokehMaterial_14;
	// UnityEngine.ComputeBuffer UnityStandardAssets.CinematicEffects.DepthOfField::m_ComputeBufferDrawArgs
	ComputeBuffer_t1033194329 * ___m_ComputeBufferDrawArgs_15;
	// UnityEngine.ComputeBuffer UnityStandardAssets.CinematicEffects.DepthOfField::m_ComputeBufferPoints
	ComputeBuffer_t1033194329 * ___m_ComputeBufferPoints_16;
	// UnityStandardAssets.CinematicEffects.DepthOfField/QualitySettings UnityStandardAssets.CinematicEffects.DepthOfField::m_CurrentQualitySettings
	QualitySettings_t1379802392  ___m_CurrentQualitySettings_17;
	// System.Single UnityStandardAssets.CinematicEffects.DepthOfField::m_LastApertureOrientation
	float ___m_LastApertureOrientation_18;
	// UnityEngine.Vector4 UnityStandardAssets.CinematicEffects.DepthOfField::m_OctogonalBokehDirection1
	Vector4_t3319028937  ___m_OctogonalBokehDirection1_19;
	// UnityEngine.Vector4 UnityStandardAssets.CinematicEffects.DepthOfField::m_OctogonalBokehDirection2
	Vector4_t3319028937  ___m_OctogonalBokehDirection2_20;
	// UnityEngine.Vector4 UnityStandardAssets.CinematicEffects.DepthOfField::m_OctogonalBokehDirection3
	Vector4_t3319028937  ___m_OctogonalBokehDirection3_21;
	// UnityEngine.Vector4 UnityStandardAssets.CinematicEffects.DepthOfField::m_OctogonalBokehDirection4
	Vector4_t3319028937  ___m_OctogonalBokehDirection4_22;
	// UnityEngine.Vector4 UnityStandardAssets.CinematicEffects.DepthOfField::m_HexagonalBokehDirection1
	Vector4_t3319028937  ___m_HexagonalBokehDirection1_23;
	// UnityEngine.Vector4 UnityStandardAssets.CinematicEffects.DepthOfField::m_HexagonalBokehDirection2
	Vector4_t3319028937  ___m_HexagonalBokehDirection2_24;
	// UnityEngine.Vector4 UnityStandardAssets.CinematicEffects.DepthOfField::m_HexagonalBokehDirection3
	Vector4_t3319028937  ___m_HexagonalBokehDirection3_25;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_BlurParams
	int32_t ___m_BlurParams_26;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_BlurCoe
	int32_t ___m_BlurCoe_27;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_Offsets
	int32_t ___m_Offsets_28;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_BlurredColor
	int32_t ___m_BlurredColor_29;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_SpawnHeuristic
	int32_t ___m_SpawnHeuristic_30;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_BokehParams
	int32_t ___m_BokehParams_31;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_Convolved_TexelSize
	int32_t ___m_Convolved_TexelSize_32;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_SecondTex
	int32_t ___m_SecondTex_33;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_ThirdTex
	int32_t ___m_ThirdTex_34;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_MainTex
	int32_t ___m_MainTex_35;
	// System.Int32 UnityStandardAssets.CinematicEffects.DepthOfField::m_Screen
	int32_t ___m_Screen_36;

public:
	inline static int32_t get_offset_of_settings_5() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___settings_5)); }
	inline GlobalSettings_t956974295  get_settings_5() const { return ___settings_5; }
	inline GlobalSettings_t956974295 * get_address_of_settings_5() { return &___settings_5; }
	inline void set_settings_5(GlobalSettings_t956974295  value)
	{
		___settings_5 = value;
	}

	inline static int32_t get_offset_of_focus_6() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___focus_6)); }
	inline FocusSettings_t1261455774  get_focus_6() const { return ___focus_6; }
	inline FocusSettings_t1261455774 * get_address_of_focus_6() { return &___focus_6; }
	inline void set_focus_6(FocusSettings_t1261455774  value)
	{
		___focus_6 = value;
	}

	inline static int32_t get_offset_of_bokehTexture_7() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___bokehTexture_7)); }
	inline BokehTextureSettings_t3943002634  get_bokehTexture_7() const { return ___bokehTexture_7; }
	inline BokehTextureSettings_t3943002634 * get_address_of_bokehTexture_7() { return &___bokehTexture_7; }
	inline void set_bokehTexture_7(BokehTextureSettings_t3943002634  value)
	{
		___bokehTexture_7 = value;
	}

	inline static int32_t get_offset_of_m_FilmicDepthOfFieldShader_8() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_FilmicDepthOfFieldShader_8)); }
	inline Shader_t4151988712 * get_m_FilmicDepthOfFieldShader_8() const { return ___m_FilmicDepthOfFieldShader_8; }
	inline Shader_t4151988712 ** get_address_of_m_FilmicDepthOfFieldShader_8() { return &___m_FilmicDepthOfFieldShader_8; }
	inline void set_m_FilmicDepthOfFieldShader_8(Shader_t4151988712 * value)
	{
		___m_FilmicDepthOfFieldShader_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_FilmicDepthOfFieldShader_8), value);
	}

	inline static int32_t get_offset_of_m_MedianFilterShader_9() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_MedianFilterShader_9)); }
	inline Shader_t4151988712 * get_m_MedianFilterShader_9() const { return ___m_MedianFilterShader_9; }
	inline Shader_t4151988712 ** get_address_of_m_MedianFilterShader_9() { return &___m_MedianFilterShader_9; }
	inline void set_m_MedianFilterShader_9(Shader_t4151988712 * value)
	{
		___m_MedianFilterShader_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_MedianFilterShader_9), value);
	}

	inline static int32_t get_offset_of_m_TextureBokehShader_10() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_TextureBokehShader_10)); }
	inline Shader_t4151988712 * get_m_TextureBokehShader_10() const { return ___m_TextureBokehShader_10; }
	inline Shader_t4151988712 ** get_address_of_m_TextureBokehShader_10() { return &___m_TextureBokehShader_10; }
	inline void set_m_TextureBokehShader_10(Shader_t4151988712 * value)
	{
		___m_TextureBokehShader_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextureBokehShader_10), value);
	}

	inline static int32_t get_offset_of_m_RTU_11() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_RTU_11)); }
	inline RenderTextureUtility_t1946006342 * get_m_RTU_11() const { return ___m_RTU_11; }
	inline RenderTextureUtility_t1946006342 ** get_address_of_m_RTU_11() { return &___m_RTU_11; }
	inline void set_m_RTU_11(RenderTextureUtility_t1946006342 * value)
	{
		___m_RTU_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RTU_11), value);
	}

	inline static int32_t get_offset_of_m_FilmicDepthOfFieldMaterial_12() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_FilmicDepthOfFieldMaterial_12)); }
	inline Material_t340375123 * get_m_FilmicDepthOfFieldMaterial_12() const { return ___m_FilmicDepthOfFieldMaterial_12; }
	inline Material_t340375123 ** get_address_of_m_FilmicDepthOfFieldMaterial_12() { return &___m_FilmicDepthOfFieldMaterial_12; }
	inline void set_m_FilmicDepthOfFieldMaterial_12(Material_t340375123 * value)
	{
		___m_FilmicDepthOfFieldMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_FilmicDepthOfFieldMaterial_12), value);
	}

	inline static int32_t get_offset_of_m_MedianFilterMaterial_13() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_MedianFilterMaterial_13)); }
	inline Material_t340375123 * get_m_MedianFilterMaterial_13() const { return ___m_MedianFilterMaterial_13; }
	inline Material_t340375123 ** get_address_of_m_MedianFilterMaterial_13() { return &___m_MedianFilterMaterial_13; }
	inline void set_m_MedianFilterMaterial_13(Material_t340375123 * value)
	{
		___m_MedianFilterMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_MedianFilterMaterial_13), value);
	}

	inline static int32_t get_offset_of_m_TextureBokehMaterial_14() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_TextureBokehMaterial_14)); }
	inline Material_t340375123 * get_m_TextureBokehMaterial_14() const { return ___m_TextureBokehMaterial_14; }
	inline Material_t340375123 ** get_address_of_m_TextureBokehMaterial_14() { return &___m_TextureBokehMaterial_14; }
	inline void set_m_TextureBokehMaterial_14(Material_t340375123 * value)
	{
		___m_TextureBokehMaterial_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextureBokehMaterial_14), value);
	}

	inline static int32_t get_offset_of_m_ComputeBufferDrawArgs_15() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_ComputeBufferDrawArgs_15)); }
	inline ComputeBuffer_t1033194329 * get_m_ComputeBufferDrawArgs_15() const { return ___m_ComputeBufferDrawArgs_15; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_ComputeBufferDrawArgs_15() { return &___m_ComputeBufferDrawArgs_15; }
	inline void set_m_ComputeBufferDrawArgs_15(ComputeBuffer_t1033194329 * value)
	{
		___m_ComputeBufferDrawArgs_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComputeBufferDrawArgs_15), value);
	}

	inline static int32_t get_offset_of_m_ComputeBufferPoints_16() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_ComputeBufferPoints_16)); }
	inline ComputeBuffer_t1033194329 * get_m_ComputeBufferPoints_16() const { return ___m_ComputeBufferPoints_16; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_ComputeBufferPoints_16() { return &___m_ComputeBufferPoints_16; }
	inline void set_m_ComputeBufferPoints_16(ComputeBuffer_t1033194329 * value)
	{
		___m_ComputeBufferPoints_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComputeBufferPoints_16), value);
	}

	inline static int32_t get_offset_of_m_CurrentQualitySettings_17() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_CurrentQualitySettings_17)); }
	inline QualitySettings_t1379802392  get_m_CurrentQualitySettings_17() const { return ___m_CurrentQualitySettings_17; }
	inline QualitySettings_t1379802392 * get_address_of_m_CurrentQualitySettings_17() { return &___m_CurrentQualitySettings_17; }
	inline void set_m_CurrentQualitySettings_17(QualitySettings_t1379802392  value)
	{
		___m_CurrentQualitySettings_17 = value;
	}

	inline static int32_t get_offset_of_m_LastApertureOrientation_18() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_LastApertureOrientation_18)); }
	inline float get_m_LastApertureOrientation_18() const { return ___m_LastApertureOrientation_18; }
	inline float* get_address_of_m_LastApertureOrientation_18() { return &___m_LastApertureOrientation_18; }
	inline void set_m_LastApertureOrientation_18(float value)
	{
		___m_LastApertureOrientation_18 = value;
	}

	inline static int32_t get_offset_of_m_OctogonalBokehDirection1_19() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_OctogonalBokehDirection1_19)); }
	inline Vector4_t3319028937  get_m_OctogonalBokehDirection1_19() const { return ___m_OctogonalBokehDirection1_19; }
	inline Vector4_t3319028937 * get_address_of_m_OctogonalBokehDirection1_19() { return &___m_OctogonalBokehDirection1_19; }
	inline void set_m_OctogonalBokehDirection1_19(Vector4_t3319028937  value)
	{
		___m_OctogonalBokehDirection1_19 = value;
	}

	inline static int32_t get_offset_of_m_OctogonalBokehDirection2_20() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_OctogonalBokehDirection2_20)); }
	inline Vector4_t3319028937  get_m_OctogonalBokehDirection2_20() const { return ___m_OctogonalBokehDirection2_20; }
	inline Vector4_t3319028937 * get_address_of_m_OctogonalBokehDirection2_20() { return &___m_OctogonalBokehDirection2_20; }
	inline void set_m_OctogonalBokehDirection2_20(Vector4_t3319028937  value)
	{
		___m_OctogonalBokehDirection2_20 = value;
	}

	inline static int32_t get_offset_of_m_OctogonalBokehDirection3_21() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_OctogonalBokehDirection3_21)); }
	inline Vector4_t3319028937  get_m_OctogonalBokehDirection3_21() const { return ___m_OctogonalBokehDirection3_21; }
	inline Vector4_t3319028937 * get_address_of_m_OctogonalBokehDirection3_21() { return &___m_OctogonalBokehDirection3_21; }
	inline void set_m_OctogonalBokehDirection3_21(Vector4_t3319028937  value)
	{
		___m_OctogonalBokehDirection3_21 = value;
	}

	inline static int32_t get_offset_of_m_OctogonalBokehDirection4_22() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_OctogonalBokehDirection4_22)); }
	inline Vector4_t3319028937  get_m_OctogonalBokehDirection4_22() const { return ___m_OctogonalBokehDirection4_22; }
	inline Vector4_t3319028937 * get_address_of_m_OctogonalBokehDirection4_22() { return &___m_OctogonalBokehDirection4_22; }
	inline void set_m_OctogonalBokehDirection4_22(Vector4_t3319028937  value)
	{
		___m_OctogonalBokehDirection4_22 = value;
	}

	inline static int32_t get_offset_of_m_HexagonalBokehDirection1_23() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_HexagonalBokehDirection1_23)); }
	inline Vector4_t3319028937  get_m_HexagonalBokehDirection1_23() const { return ___m_HexagonalBokehDirection1_23; }
	inline Vector4_t3319028937 * get_address_of_m_HexagonalBokehDirection1_23() { return &___m_HexagonalBokehDirection1_23; }
	inline void set_m_HexagonalBokehDirection1_23(Vector4_t3319028937  value)
	{
		___m_HexagonalBokehDirection1_23 = value;
	}

	inline static int32_t get_offset_of_m_HexagonalBokehDirection2_24() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_HexagonalBokehDirection2_24)); }
	inline Vector4_t3319028937  get_m_HexagonalBokehDirection2_24() const { return ___m_HexagonalBokehDirection2_24; }
	inline Vector4_t3319028937 * get_address_of_m_HexagonalBokehDirection2_24() { return &___m_HexagonalBokehDirection2_24; }
	inline void set_m_HexagonalBokehDirection2_24(Vector4_t3319028937  value)
	{
		___m_HexagonalBokehDirection2_24 = value;
	}

	inline static int32_t get_offset_of_m_HexagonalBokehDirection3_25() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_HexagonalBokehDirection3_25)); }
	inline Vector4_t3319028937  get_m_HexagonalBokehDirection3_25() const { return ___m_HexagonalBokehDirection3_25; }
	inline Vector4_t3319028937 * get_address_of_m_HexagonalBokehDirection3_25() { return &___m_HexagonalBokehDirection3_25; }
	inline void set_m_HexagonalBokehDirection3_25(Vector4_t3319028937  value)
	{
		___m_HexagonalBokehDirection3_25 = value;
	}

	inline static int32_t get_offset_of_m_BlurParams_26() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_BlurParams_26)); }
	inline int32_t get_m_BlurParams_26() const { return ___m_BlurParams_26; }
	inline int32_t* get_address_of_m_BlurParams_26() { return &___m_BlurParams_26; }
	inline void set_m_BlurParams_26(int32_t value)
	{
		___m_BlurParams_26 = value;
	}

	inline static int32_t get_offset_of_m_BlurCoe_27() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_BlurCoe_27)); }
	inline int32_t get_m_BlurCoe_27() const { return ___m_BlurCoe_27; }
	inline int32_t* get_address_of_m_BlurCoe_27() { return &___m_BlurCoe_27; }
	inline void set_m_BlurCoe_27(int32_t value)
	{
		___m_BlurCoe_27 = value;
	}

	inline static int32_t get_offset_of_m_Offsets_28() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_Offsets_28)); }
	inline int32_t get_m_Offsets_28() const { return ___m_Offsets_28; }
	inline int32_t* get_address_of_m_Offsets_28() { return &___m_Offsets_28; }
	inline void set_m_Offsets_28(int32_t value)
	{
		___m_Offsets_28 = value;
	}

	inline static int32_t get_offset_of_m_BlurredColor_29() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_BlurredColor_29)); }
	inline int32_t get_m_BlurredColor_29() const { return ___m_BlurredColor_29; }
	inline int32_t* get_address_of_m_BlurredColor_29() { return &___m_BlurredColor_29; }
	inline void set_m_BlurredColor_29(int32_t value)
	{
		___m_BlurredColor_29 = value;
	}

	inline static int32_t get_offset_of_m_SpawnHeuristic_30() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_SpawnHeuristic_30)); }
	inline int32_t get_m_SpawnHeuristic_30() const { return ___m_SpawnHeuristic_30; }
	inline int32_t* get_address_of_m_SpawnHeuristic_30() { return &___m_SpawnHeuristic_30; }
	inline void set_m_SpawnHeuristic_30(int32_t value)
	{
		___m_SpawnHeuristic_30 = value;
	}

	inline static int32_t get_offset_of_m_BokehParams_31() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_BokehParams_31)); }
	inline int32_t get_m_BokehParams_31() const { return ___m_BokehParams_31; }
	inline int32_t* get_address_of_m_BokehParams_31() { return &___m_BokehParams_31; }
	inline void set_m_BokehParams_31(int32_t value)
	{
		___m_BokehParams_31 = value;
	}

	inline static int32_t get_offset_of_m_Convolved_TexelSize_32() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_Convolved_TexelSize_32)); }
	inline int32_t get_m_Convolved_TexelSize_32() const { return ___m_Convolved_TexelSize_32; }
	inline int32_t* get_address_of_m_Convolved_TexelSize_32() { return &___m_Convolved_TexelSize_32; }
	inline void set_m_Convolved_TexelSize_32(int32_t value)
	{
		___m_Convolved_TexelSize_32 = value;
	}

	inline static int32_t get_offset_of_m_SecondTex_33() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_SecondTex_33)); }
	inline int32_t get_m_SecondTex_33() const { return ___m_SecondTex_33; }
	inline int32_t* get_address_of_m_SecondTex_33() { return &___m_SecondTex_33; }
	inline void set_m_SecondTex_33(int32_t value)
	{
		___m_SecondTex_33 = value;
	}

	inline static int32_t get_offset_of_m_ThirdTex_34() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_ThirdTex_34)); }
	inline int32_t get_m_ThirdTex_34() const { return ___m_ThirdTex_34; }
	inline int32_t* get_address_of_m_ThirdTex_34() { return &___m_ThirdTex_34; }
	inline void set_m_ThirdTex_34(int32_t value)
	{
		___m_ThirdTex_34 = value;
	}

	inline static int32_t get_offset_of_m_MainTex_35() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_MainTex_35)); }
	inline int32_t get_m_MainTex_35() const { return ___m_MainTex_35; }
	inline int32_t* get_address_of_m_MainTex_35() { return &___m_MainTex_35; }
	inline void set_m_MainTex_35(int32_t value)
	{
		___m_MainTex_35 = value;
	}

	inline static int32_t get_offset_of_m_Screen_36() { return static_cast<int32_t>(offsetof(DepthOfField_t2645519978, ___m_Screen_36)); }
	inline int32_t get_m_Screen_36() const { return ___m_Screen_36; }
	inline int32_t* get_address_of_m_Screen_36() { return &___m_Screen_36; }
	inline void set_m_Screen_36(int32_t value)
	{
		___m_Screen_36 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELD_T2645519978_H
#ifndef LENSABERRATIONS_T456692924_H
#define LENSABERRATIONS_T456692924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.LensAberrations
struct  LensAberrations_t456692924  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.LensAberrations/DistortionSettings UnityStandardAssets.CinematicEffects.LensAberrations::distortion
	DistortionSettings_t3193728992  ___distortion_4;
	// UnityStandardAssets.CinematicEffects.LensAberrations/VignetteSettings UnityStandardAssets.CinematicEffects.LensAberrations::vignette
	VignetteSettings_t2342083791  ___vignette_5;
	// UnityStandardAssets.CinematicEffects.LensAberrations/ChromaticAberrationSettings UnityStandardAssets.CinematicEffects.LensAberrations::chromaticAberration
	ChromaticAberrationSettings_t3807027201  ___chromaticAberration_6;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.LensAberrations::m_Shader
	Shader_t4151988712 * ___m_Shader_7;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.LensAberrations::m_Material
	Material_t340375123 * ___m_Material_8;
	// UnityStandardAssets.CinematicEffects.RenderTextureUtility UnityStandardAssets.CinematicEffects.LensAberrations::m_RTU
	RenderTextureUtility_t1946006342 * ___m_RTU_9;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_DistCenterScale
	int32_t ___m_DistCenterScale_10;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_DistAmount
	int32_t ___m_DistAmount_11;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_ChromaticAberration
	int32_t ___m_ChromaticAberration_12;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_VignetteColor
	int32_t ___m_VignetteColor_13;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_BlurPass
	int32_t ___m_BlurPass_14;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_BlurTex
	int32_t ___m_BlurTex_15;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_VignetteBlur
	int32_t ___m_VignetteBlur_16;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_VignetteDesat
	int32_t ___m_VignetteDesat_17;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_VignetteCenter
	int32_t ___m_VignetteCenter_18;
	// System.Int32 UnityStandardAssets.CinematicEffects.LensAberrations::m_VignetteSettings
	int32_t ___m_VignetteSettings_19;

public:
	inline static int32_t get_offset_of_distortion_4() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___distortion_4)); }
	inline DistortionSettings_t3193728992  get_distortion_4() const { return ___distortion_4; }
	inline DistortionSettings_t3193728992 * get_address_of_distortion_4() { return &___distortion_4; }
	inline void set_distortion_4(DistortionSettings_t3193728992  value)
	{
		___distortion_4 = value;
	}

	inline static int32_t get_offset_of_vignette_5() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___vignette_5)); }
	inline VignetteSettings_t2342083791  get_vignette_5() const { return ___vignette_5; }
	inline VignetteSettings_t2342083791 * get_address_of_vignette_5() { return &___vignette_5; }
	inline void set_vignette_5(VignetteSettings_t2342083791  value)
	{
		___vignette_5 = value;
	}

	inline static int32_t get_offset_of_chromaticAberration_6() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___chromaticAberration_6)); }
	inline ChromaticAberrationSettings_t3807027201  get_chromaticAberration_6() const { return ___chromaticAberration_6; }
	inline ChromaticAberrationSettings_t3807027201 * get_address_of_chromaticAberration_6() { return &___chromaticAberration_6; }
	inline void set_chromaticAberration_6(ChromaticAberrationSettings_t3807027201  value)
	{
		___chromaticAberration_6 = value;
	}

	inline static int32_t get_offset_of_m_Shader_7() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_Shader_7)); }
	inline Shader_t4151988712 * get_m_Shader_7() const { return ___m_Shader_7; }
	inline Shader_t4151988712 ** get_address_of_m_Shader_7() { return &___m_Shader_7; }
	inline void set_m_Shader_7(Shader_t4151988712 * value)
	{
		___m_Shader_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Shader_7), value);
	}

	inline static int32_t get_offset_of_m_Material_8() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_Material_8)); }
	inline Material_t340375123 * get_m_Material_8() const { return ___m_Material_8; }
	inline Material_t340375123 ** get_address_of_m_Material_8() { return &___m_Material_8; }
	inline void set_m_Material_8(Material_t340375123 * value)
	{
		___m_Material_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_8), value);
	}

	inline static int32_t get_offset_of_m_RTU_9() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_RTU_9)); }
	inline RenderTextureUtility_t1946006342 * get_m_RTU_9() const { return ___m_RTU_9; }
	inline RenderTextureUtility_t1946006342 ** get_address_of_m_RTU_9() { return &___m_RTU_9; }
	inline void set_m_RTU_9(RenderTextureUtility_t1946006342 * value)
	{
		___m_RTU_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RTU_9), value);
	}

	inline static int32_t get_offset_of_m_DistCenterScale_10() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_DistCenterScale_10)); }
	inline int32_t get_m_DistCenterScale_10() const { return ___m_DistCenterScale_10; }
	inline int32_t* get_address_of_m_DistCenterScale_10() { return &___m_DistCenterScale_10; }
	inline void set_m_DistCenterScale_10(int32_t value)
	{
		___m_DistCenterScale_10 = value;
	}

	inline static int32_t get_offset_of_m_DistAmount_11() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_DistAmount_11)); }
	inline int32_t get_m_DistAmount_11() const { return ___m_DistAmount_11; }
	inline int32_t* get_address_of_m_DistAmount_11() { return &___m_DistAmount_11; }
	inline void set_m_DistAmount_11(int32_t value)
	{
		___m_DistAmount_11 = value;
	}

	inline static int32_t get_offset_of_m_ChromaticAberration_12() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_ChromaticAberration_12)); }
	inline int32_t get_m_ChromaticAberration_12() const { return ___m_ChromaticAberration_12; }
	inline int32_t* get_address_of_m_ChromaticAberration_12() { return &___m_ChromaticAberration_12; }
	inline void set_m_ChromaticAberration_12(int32_t value)
	{
		___m_ChromaticAberration_12 = value;
	}

	inline static int32_t get_offset_of_m_VignetteColor_13() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_VignetteColor_13)); }
	inline int32_t get_m_VignetteColor_13() const { return ___m_VignetteColor_13; }
	inline int32_t* get_address_of_m_VignetteColor_13() { return &___m_VignetteColor_13; }
	inline void set_m_VignetteColor_13(int32_t value)
	{
		___m_VignetteColor_13 = value;
	}

	inline static int32_t get_offset_of_m_BlurPass_14() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_BlurPass_14)); }
	inline int32_t get_m_BlurPass_14() const { return ___m_BlurPass_14; }
	inline int32_t* get_address_of_m_BlurPass_14() { return &___m_BlurPass_14; }
	inline void set_m_BlurPass_14(int32_t value)
	{
		___m_BlurPass_14 = value;
	}

	inline static int32_t get_offset_of_m_BlurTex_15() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_BlurTex_15)); }
	inline int32_t get_m_BlurTex_15() const { return ___m_BlurTex_15; }
	inline int32_t* get_address_of_m_BlurTex_15() { return &___m_BlurTex_15; }
	inline void set_m_BlurTex_15(int32_t value)
	{
		___m_BlurTex_15 = value;
	}

	inline static int32_t get_offset_of_m_VignetteBlur_16() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_VignetteBlur_16)); }
	inline int32_t get_m_VignetteBlur_16() const { return ___m_VignetteBlur_16; }
	inline int32_t* get_address_of_m_VignetteBlur_16() { return &___m_VignetteBlur_16; }
	inline void set_m_VignetteBlur_16(int32_t value)
	{
		___m_VignetteBlur_16 = value;
	}

	inline static int32_t get_offset_of_m_VignetteDesat_17() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_VignetteDesat_17)); }
	inline int32_t get_m_VignetteDesat_17() const { return ___m_VignetteDesat_17; }
	inline int32_t* get_address_of_m_VignetteDesat_17() { return &___m_VignetteDesat_17; }
	inline void set_m_VignetteDesat_17(int32_t value)
	{
		___m_VignetteDesat_17 = value;
	}

	inline static int32_t get_offset_of_m_VignetteCenter_18() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_VignetteCenter_18)); }
	inline int32_t get_m_VignetteCenter_18() const { return ___m_VignetteCenter_18; }
	inline int32_t* get_address_of_m_VignetteCenter_18() { return &___m_VignetteCenter_18; }
	inline void set_m_VignetteCenter_18(int32_t value)
	{
		___m_VignetteCenter_18 = value;
	}

	inline static int32_t get_offset_of_m_VignetteSettings_19() { return static_cast<int32_t>(offsetof(LensAberrations_t456692924, ___m_VignetteSettings_19)); }
	inline int32_t get_m_VignetteSettings_19() const { return ___m_VignetteSettings_19; }
	inline int32_t* get_address_of_m_VignetteSettings_19() { return &___m_VignetteSettings_19; }
	inline void set_m_VignetteSettings_19(int32_t value)
	{
		___m_VignetteSettings_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LENSABERRATIONS_T456692924_H
#ifndef MOTIONBLUR_T437291541_H
#define MOTIONBLUR_T437291541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.MotionBlur
struct  MotionBlur_t437291541  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.MotionBlur/Settings UnityStandardAssets.CinematicEffects.MotionBlur::_settings
	Settings_t1356140371 * ____settings_4;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.MotionBlur::_reconstructionShader
	Shader_t4151988712 * ____reconstructionShader_5;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.MotionBlur::_frameBlendingShader
	Shader_t4151988712 * ____frameBlendingShader_6;
	// UnityStandardAssets.CinematicEffects.MotionBlur/ReconstructionFilter UnityStandardAssets.CinematicEffects.MotionBlur::_reconstructionFilter
	ReconstructionFilter_t4011033762 * ____reconstructionFilter_7;
	// UnityStandardAssets.CinematicEffects.MotionBlur/FrameBlendingFilter UnityStandardAssets.CinematicEffects.MotionBlur::_frameBlendingFilter
	FrameBlendingFilter_t4031465840 * ____frameBlendingFilter_8;

public:
	inline static int32_t get_offset_of__settings_4() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____settings_4)); }
	inline Settings_t1356140371 * get__settings_4() const { return ____settings_4; }
	inline Settings_t1356140371 ** get_address_of__settings_4() { return &____settings_4; }
	inline void set__settings_4(Settings_t1356140371 * value)
	{
		____settings_4 = value;
		Il2CppCodeGenWriteBarrier((&____settings_4), value);
	}

	inline static int32_t get_offset_of__reconstructionShader_5() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____reconstructionShader_5)); }
	inline Shader_t4151988712 * get__reconstructionShader_5() const { return ____reconstructionShader_5; }
	inline Shader_t4151988712 ** get_address_of__reconstructionShader_5() { return &____reconstructionShader_5; }
	inline void set__reconstructionShader_5(Shader_t4151988712 * value)
	{
		____reconstructionShader_5 = value;
		Il2CppCodeGenWriteBarrier((&____reconstructionShader_5), value);
	}

	inline static int32_t get_offset_of__frameBlendingShader_6() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____frameBlendingShader_6)); }
	inline Shader_t4151988712 * get__frameBlendingShader_6() const { return ____frameBlendingShader_6; }
	inline Shader_t4151988712 ** get_address_of__frameBlendingShader_6() { return &____frameBlendingShader_6; }
	inline void set__frameBlendingShader_6(Shader_t4151988712 * value)
	{
		____frameBlendingShader_6 = value;
		Il2CppCodeGenWriteBarrier((&____frameBlendingShader_6), value);
	}

	inline static int32_t get_offset_of__reconstructionFilter_7() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____reconstructionFilter_7)); }
	inline ReconstructionFilter_t4011033762 * get__reconstructionFilter_7() const { return ____reconstructionFilter_7; }
	inline ReconstructionFilter_t4011033762 ** get_address_of__reconstructionFilter_7() { return &____reconstructionFilter_7; }
	inline void set__reconstructionFilter_7(ReconstructionFilter_t4011033762 * value)
	{
		____reconstructionFilter_7 = value;
		Il2CppCodeGenWriteBarrier((&____reconstructionFilter_7), value);
	}

	inline static int32_t get_offset_of__frameBlendingFilter_8() { return static_cast<int32_t>(offsetof(MotionBlur_t437291541, ____frameBlendingFilter_8)); }
	inline FrameBlendingFilter_t4031465840 * get__frameBlendingFilter_8() const { return ____frameBlendingFilter_8; }
	inline FrameBlendingFilter_t4031465840 ** get_address_of__frameBlendingFilter_8() { return &____frameBlendingFilter_8; }
	inline void set__frameBlendingFilter_8(FrameBlendingFilter_t4031465840 * value)
	{
		____frameBlendingFilter_8 = value;
		Il2CppCodeGenWriteBarrier((&____frameBlendingFilter_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLUR_T437291541_H
#ifndef SCREENSPACEREFLECTION_T4192727846_H
#define SCREENSPACEREFLECTION_T4192727846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection
struct  ScreenSpaceReflection_t4192727846  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.ScreenSpaceReflection/SSRSettings UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::settings
	SSRSettings_t899296535  ___settings_4;
	// System.Boolean UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::highlightSuppression
	bool ___highlightSuppression_5;
	// System.Boolean UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::traceBehindObjects
	bool ___traceBehindObjects_6;
	// System.Boolean UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::treatBackfaceHitAsMiss
	bool ___treatBackfaceHitAsMiss_7;
	// System.Boolean UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::bilateralUpsample
	bool ___bilateralUpsample_8;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::m_Shader
	Shader_t4151988712 * ___m_Shader_9;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::m_Material
	Material_t340375123 * ___m_Material_10;
	// UnityEngine.Camera UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::m_Camera
	Camera_t4157153871 * ___m_Camera_11;
	// UnityEngine.Rendering.CommandBuffer UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::m_CommandBuffer
	CommandBuffer_t2206337031 * ___m_CommandBuffer_12;

public:
	inline static int32_t get_offset_of_settings_4() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___settings_4)); }
	inline SSRSettings_t899296535  get_settings_4() const { return ___settings_4; }
	inline SSRSettings_t899296535 * get_address_of_settings_4() { return &___settings_4; }
	inline void set_settings_4(SSRSettings_t899296535  value)
	{
		___settings_4 = value;
	}

	inline static int32_t get_offset_of_highlightSuppression_5() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___highlightSuppression_5)); }
	inline bool get_highlightSuppression_5() const { return ___highlightSuppression_5; }
	inline bool* get_address_of_highlightSuppression_5() { return &___highlightSuppression_5; }
	inline void set_highlightSuppression_5(bool value)
	{
		___highlightSuppression_5 = value;
	}

	inline static int32_t get_offset_of_traceBehindObjects_6() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___traceBehindObjects_6)); }
	inline bool get_traceBehindObjects_6() const { return ___traceBehindObjects_6; }
	inline bool* get_address_of_traceBehindObjects_6() { return &___traceBehindObjects_6; }
	inline void set_traceBehindObjects_6(bool value)
	{
		___traceBehindObjects_6 = value;
	}

	inline static int32_t get_offset_of_treatBackfaceHitAsMiss_7() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___treatBackfaceHitAsMiss_7)); }
	inline bool get_treatBackfaceHitAsMiss_7() const { return ___treatBackfaceHitAsMiss_7; }
	inline bool* get_address_of_treatBackfaceHitAsMiss_7() { return &___treatBackfaceHitAsMiss_7; }
	inline void set_treatBackfaceHitAsMiss_7(bool value)
	{
		___treatBackfaceHitAsMiss_7 = value;
	}

	inline static int32_t get_offset_of_bilateralUpsample_8() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___bilateralUpsample_8)); }
	inline bool get_bilateralUpsample_8() const { return ___bilateralUpsample_8; }
	inline bool* get_address_of_bilateralUpsample_8() { return &___bilateralUpsample_8; }
	inline void set_bilateralUpsample_8(bool value)
	{
		___bilateralUpsample_8 = value;
	}

	inline static int32_t get_offset_of_m_Shader_9() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___m_Shader_9)); }
	inline Shader_t4151988712 * get_m_Shader_9() const { return ___m_Shader_9; }
	inline Shader_t4151988712 ** get_address_of_m_Shader_9() { return &___m_Shader_9; }
	inline void set_m_Shader_9(Shader_t4151988712 * value)
	{
		___m_Shader_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Shader_9), value);
	}

	inline static int32_t get_offset_of_m_Material_10() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___m_Material_10)); }
	inline Material_t340375123 * get_m_Material_10() const { return ___m_Material_10; }
	inline Material_t340375123 ** get_address_of_m_Material_10() { return &___m_Material_10; }
	inline void set_m_Material_10(Material_t340375123 * value)
	{
		___m_Material_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_10), value);
	}

	inline static int32_t get_offset_of_m_Camera_11() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___m_Camera_11)); }
	inline Camera_t4157153871 * get_m_Camera_11() const { return ___m_Camera_11; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_11() { return &___m_Camera_11; }
	inline void set_m_Camera_11(Camera_t4157153871 * value)
	{
		___m_Camera_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_11), value);
	}

	inline static int32_t get_offset_of_m_CommandBuffer_12() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846, ___m_CommandBuffer_12)); }
	inline CommandBuffer_t2206337031 * get_m_CommandBuffer_12() const { return ___m_CommandBuffer_12; }
	inline CommandBuffer_t2206337031 ** get_address_of_m_CommandBuffer_12() { return &___m_CommandBuffer_12; }
	inline void set_m_CommandBuffer_12(CommandBuffer_t2206337031 * value)
	{
		___m_CommandBuffer_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommandBuffer_12), value);
	}
};

struct ScreenSpaceReflection_t4192727846_StaticFields
{
public:
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kNormalAndRoughnessTexture
	int32_t ___kNormalAndRoughnessTexture_13;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kHitPointTexture
	int32_t ___kHitPointTexture_14;
	// System.Int32[] UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kReflectionTextures
	Int32U5BU5D_t385246372* ___kReflectionTextures_15;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kFilteredReflections
	int32_t ___kFilteredReflections_16;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kBlurTexture
	int32_t ___kBlurTexture_17;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kFinalReflectionTexture
	int32_t ___kFinalReflectionTexture_18;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kTempTexture
	int32_t ___kTempTexture_19;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kRayStepSize
	int32_t ___kRayStepSize_20;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kAdditiveReflection
	int32_t ___kAdditiveReflection_21;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kBilateralUpsampling
	int32_t ___kBilateralUpsampling_22;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kTreatBackfaceHitAsMiss
	int32_t ___kTreatBackfaceHitAsMiss_23;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kAllowBackwardsRays
	int32_t ___kAllowBackwardsRays_24;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kTraceBehindObjects
	int32_t ___kTraceBehindObjects_25;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kMaxSteps
	int32_t ___kMaxSteps_26;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kFullResolutionFiltering
	int32_t ___kFullResolutionFiltering_27;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kHalfResolution
	int32_t ___kHalfResolution_28;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kHighlightSuppression
	int32_t ___kHighlightSuppression_29;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kPixelsPerMeterAtOneMeter
	int32_t ___kPixelsPerMeterAtOneMeter_30;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kScreenEdgeFading
	int32_t ___kScreenEdgeFading_31;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kReflectionBlur
	int32_t ___kReflectionBlur_32;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kMaxRayTraceDistance
	int32_t ___kMaxRayTraceDistance_33;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kFadeDistance
	int32_t ___kFadeDistance_34;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kLayerThickness
	int32_t ___kLayerThickness_35;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kSSRMultiplier
	int32_t ___kSSRMultiplier_36;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kFresnelFade
	int32_t ___kFresnelFade_37;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kFresnelFadePower
	int32_t ___kFresnelFadePower_38;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kReflectionBufferSize
	int32_t ___kReflectionBufferSize_39;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kScreenSize
	int32_t ___kScreenSize_40;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kInvScreenSize
	int32_t ___kInvScreenSize_41;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kProjInfo
	int32_t ___kProjInfo_42;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kCameraClipInfo
	int32_t ___kCameraClipInfo_43;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kProjectToPixelMatrix
	int32_t ___kProjectToPixelMatrix_44;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kWorldToCameraMatrix
	int32_t ___kWorldToCameraMatrix_45;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kCameraToWorldMatrix
	int32_t ___kCameraToWorldMatrix_46;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kAxis
	int32_t ___kAxis_47;
	// System.Int32 UnityStandardAssets.CinematicEffects.ScreenSpaceReflection::kCurrentMipLevel
	int32_t ___kCurrentMipLevel_48;

public:
	inline static int32_t get_offset_of_kNormalAndRoughnessTexture_13() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kNormalAndRoughnessTexture_13)); }
	inline int32_t get_kNormalAndRoughnessTexture_13() const { return ___kNormalAndRoughnessTexture_13; }
	inline int32_t* get_address_of_kNormalAndRoughnessTexture_13() { return &___kNormalAndRoughnessTexture_13; }
	inline void set_kNormalAndRoughnessTexture_13(int32_t value)
	{
		___kNormalAndRoughnessTexture_13 = value;
	}

	inline static int32_t get_offset_of_kHitPointTexture_14() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kHitPointTexture_14)); }
	inline int32_t get_kHitPointTexture_14() const { return ___kHitPointTexture_14; }
	inline int32_t* get_address_of_kHitPointTexture_14() { return &___kHitPointTexture_14; }
	inline void set_kHitPointTexture_14(int32_t value)
	{
		___kHitPointTexture_14 = value;
	}

	inline static int32_t get_offset_of_kReflectionTextures_15() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kReflectionTextures_15)); }
	inline Int32U5BU5D_t385246372* get_kReflectionTextures_15() const { return ___kReflectionTextures_15; }
	inline Int32U5BU5D_t385246372** get_address_of_kReflectionTextures_15() { return &___kReflectionTextures_15; }
	inline void set_kReflectionTextures_15(Int32U5BU5D_t385246372* value)
	{
		___kReflectionTextures_15 = value;
		Il2CppCodeGenWriteBarrier((&___kReflectionTextures_15), value);
	}

	inline static int32_t get_offset_of_kFilteredReflections_16() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kFilteredReflections_16)); }
	inline int32_t get_kFilteredReflections_16() const { return ___kFilteredReflections_16; }
	inline int32_t* get_address_of_kFilteredReflections_16() { return &___kFilteredReflections_16; }
	inline void set_kFilteredReflections_16(int32_t value)
	{
		___kFilteredReflections_16 = value;
	}

	inline static int32_t get_offset_of_kBlurTexture_17() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kBlurTexture_17)); }
	inline int32_t get_kBlurTexture_17() const { return ___kBlurTexture_17; }
	inline int32_t* get_address_of_kBlurTexture_17() { return &___kBlurTexture_17; }
	inline void set_kBlurTexture_17(int32_t value)
	{
		___kBlurTexture_17 = value;
	}

	inline static int32_t get_offset_of_kFinalReflectionTexture_18() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kFinalReflectionTexture_18)); }
	inline int32_t get_kFinalReflectionTexture_18() const { return ___kFinalReflectionTexture_18; }
	inline int32_t* get_address_of_kFinalReflectionTexture_18() { return &___kFinalReflectionTexture_18; }
	inline void set_kFinalReflectionTexture_18(int32_t value)
	{
		___kFinalReflectionTexture_18 = value;
	}

	inline static int32_t get_offset_of_kTempTexture_19() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kTempTexture_19)); }
	inline int32_t get_kTempTexture_19() const { return ___kTempTexture_19; }
	inline int32_t* get_address_of_kTempTexture_19() { return &___kTempTexture_19; }
	inline void set_kTempTexture_19(int32_t value)
	{
		___kTempTexture_19 = value;
	}

	inline static int32_t get_offset_of_kRayStepSize_20() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kRayStepSize_20)); }
	inline int32_t get_kRayStepSize_20() const { return ___kRayStepSize_20; }
	inline int32_t* get_address_of_kRayStepSize_20() { return &___kRayStepSize_20; }
	inline void set_kRayStepSize_20(int32_t value)
	{
		___kRayStepSize_20 = value;
	}

	inline static int32_t get_offset_of_kAdditiveReflection_21() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kAdditiveReflection_21)); }
	inline int32_t get_kAdditiveReflection_21() const { return ___kAdditiveReflection_21; }
	inline int32_t* get_address_of_kAdditiveReflection_21() { return &___kAdditiveReflection_21; }
	inline void set_kAdditiveReflection_21(int32_t value)
	{
		___kAdditiveReflection_21 = value;
	}

	inline static int32_t get_offset_of_kBilateralUpsampling_22() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kBilateralUpsampling_22)); }
	inline int32_t get_kBilateralUpsampling_22() const { return ___kBilateralUpsampling_22; }
	inline int32_t* get_address_of_kBilateralUpsampling_22() { return &___kBilateralUpsampling_22; }
	inline void set_kBilateralUpsampling_22(int32_t value)
	{
		___kBilateralUpsampling_22 = value;
	}

	inline static int32_t get_offset_of_kTreatBackfaceHitAsMiss_23() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kTreatBackfaceHitAsMiss_23)); }
	inline int32_t get_kTreatBackfaceHitAsMiss_23() const { return ___kTreatBackfaceHitAsMiss_23; }
	inline int32_t* get_address_of_kTreatBackfaceHitAsMiss_23() { return &___kTreatBackfaceHitAsMiss_23; }
	inline void set_kTreatBackfaceHitAsMiss_23(int32_t value)
	{
		___kTreatBackfaceHitAsMiss_23 = value;
	}

	inline static int32_t get_offset_of_kAllowBackwardsRays_24() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kAllowBackwardsRays_24)); }
	inline int32_t get_kAllowBackwardsRays_24() const { return ___kAllowBackwardsRays_24; }
	inline int32_t* get_address_of_kAllowBackwardsRays_24() { return &___kAllowBackwardsRays_24; }
	inline void set_kAllowBackwardsRays_24(int32_t value)
	{
		___kAllowBackwardsRays_24 = value;
	}

	inline static int32_t get_offset_of_kTraceBehindObjects_25() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kTraceBehindObjects_25)); }
	inline int32_t get_kTraceBehindObjects_25() const { return ___kTraceBehindObjects_25; }
	inline int32_t* get_address_of_kTraceBehindObjects_25() { return &___kTraceBehindObjects_25; }
	inline void set_kTraceBehindObjects_25(int32_t value)
	{
		___kTraceBehindObjects_25 = value;
	}

	inline static int32_t get_offset_of_kMaxSteps_26() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kMaxSteps_26)); }
	inline int32_t get_kMaxSteps_26() const { return ___kMaxSteps_26; }
	inline int32_t* get_address_of_kMaxSteps_26() { return &___kMaxSteps_26; }
	inline void set_kMaxSteps_26(int32_t value)
	{
		___kMaxSteps_26 = value;
	}

	inline static int32_t get_offset_of_kFullResolutionFiltering_27() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kFullResolutionFiltering_27)); }
	inline int32_t get_kFullResolutionFiltering_27() const { return ___kFullResolutionFiltering_27; }
	inline int32_t* get_address_of_kFullResolutionFiltering_27() { return &___kFullResolutionFiltering_27; }
	inline void set_kFullResolutionFiltering_27(int32_t value)
	{
		___kFullResolutionFiltering_27 = value;
	}

	inline static int32_t get_offset_of_kHalfResolution_28() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kHalfResolution_28)); }
	inline int32_t get_kHalfResolution_28() const { return ___kHalfResolution_28; }
	inline int32_t* get_address_of_kHalfResolution_28() { return &___kHalfResolution_28; }
	inline void set_kHalfResolution_28(int32_t value)
	{
		___kHalfResolution_28 = value;
	}

	inline static int32_t get_offset_of_kHighlightSuppression_29() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kHighlightSuppression_29)); }
	inline int32_t get_kHighlightSuppression_29() const { return ___kHighlightSuppression_29; }
	inline int32_t* get_address_of_kHighlightSuppression_29() { return &___kHighlightSuppression_29; }
	inline void set_kHighlightSuppression_29(int32_t value)
	{
		___kHighlightSuppression_29 = value;
	}

	inline static int32_t get_offset_of_kPixelsPerMeterAtOneMeter_30() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kPixelsPerMeterAtOneMeter_30)); }
	inline int32_t get_kPixelsPerMeterAtOneMeter_30() const { return ___kPixelsPerMeterAtOneMeter_30; }
	inline int32_t* get_address_of_kPixelsPerMeterAtOneMeter_30() { return &___kPixelsPerMeterAtOneMeter_30; }
	inline void set_kPixelsPerMeterAtOneMeter_30(int32_t value)
	{
		___kPixelsPerMeterAtOneMeter_30 = value;
	}

	inline static int32_t get_offset_of_kScreenEdgeFading_31() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kScreenEdgeFading_31)); }
	inline int32_t get_kScreenEdgeFading_31() const { return ___kScreenEdgeFading_31; }
	inline int32_t* get_address_of_kScreenEdgeFading_31() { return &___kScreenEdgeFading_31; }
	inline void set_kScreenEdgeFading_31(int32_t value)
	{
		___kScreenEdgeFading_31 = value;
	}

	inline static int32_t get_offset_of_kReflectionBlur_32() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kReflectionBlur_32)); }
	inline int32_t get_kReflectionBlur_32() const { return ___kReflectionBlur_32; }
	inline int32_t* get_address_of_kReflectionBlur_32() { return &___kReflectionBlur_32; }
	inline void set_kReflectionBlur_32(int32_t value)
	{
		___kReflectionBlur_32 = value;
	}

	inline static int32_t get_offset_of_kMaxRayTraceDistance_33() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kMaxRayTraceDistance_33)); }
	inline int32_t get_kMaxRayTraceDistance_33() const { return ___kMaxRayTraceDistance_33; }
	inline int32_t* get_address_of_kMaxRayTraceDistance_33() { return &___kMaxRayTraceDistance_33; }
	inline void set_kMaxRayTraceDistance_33(int32_t value)
	{
		___kMaxRayTraceDistance_33 = value;
	}

	inline static int32_t get_offset_of_kFadeDistance_34() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kFadeDistance_34)); }
	inline int32_t get_kFadeDistance_34() const { return ___kFadeDistance_34; }
	inline int32_t* get_address_of_kFadeDistance_34() { return &___kFadeDistance_34; }
	inline void set_kFadeDistance_34(int32_t value)
	{
		___kFadeDistance_34 = value;
	}

	inline static int32_t get_offset_of_kLayerThickness_35() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kLayerThickness_35)); }
	inline int32_t get_kLayerThickness_35() const { return ___kLayerThickness_35; }
	inline int32_t* get_address_of_kLayerThickness_35() { return &___kLayerThickness_35; }
	inline void set_kLayerThickness_35(int32_t value)
	{
		___kLayerThickness_35 = value;
	}

	inline static int32_t get_offset_of_kSSRMultiplier_36() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kSSRMultiplier_36)); }
	inline int32_t get_kSSRMultiplier_36() const { return ___kSSRMultiplier_36; }
	inline int32_t* get_address_of_kSSRMultiplier_36() { return &___kSSRMultiplier_36; }
	inline void set_kSSRMultiplier_36(int32_t value)
	{
		___kSSRMultiplier_36 = value;
	}

	inline static int32_t get_offset_of_kFresnelFade_37() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kFresnelFade_37)); }
	inline int32_t get_kFresnelFade_37() const { return ___kFresnelFade_37; }
	inline int32_t* get_address_of_kFresnelFade_37() { return &___kFresnelFade_37; }
	inline void set_kFresnelFade_37(int32_t value)
	{
		___kFresnelFade_37 = value;
	}

	inline static int32_t get_offset_of_kFresnelFadePower_38() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kFresnelFadePower_38)); }
	inline int32_t get_kFresnelFadePower_38() const { return ___kFresnelFadePower_38; }
	inline int32_t* get_address_of_kFresnelFadePower_38() { return &___kFresnelFadePower_38; }
	inline void set_kFresnelFadePower_38(int32_t value)
	{
		___kFresnelFadePower_38 = value;
	}

	inline static int32_t get_offset_of_kReflectionBufferSize_39() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kReflectionBufferSize_39)); }
	inline int32_t get_kReflectionBufferSize_39() const { return ___kReflectionBufferSize_39; }
	inline int32_t* get_address_of_kReflectionBufferSize_39() { return &___kReflectionBufferSize_39; }
	inline void set_kReflectionBufferSize_39(int32_t value)
	{
		___kReflectionBufferSize_39 = value;
	}

	inline static int32_t get_offset_of_kScreenSize_40() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kScreenSize_40)); }
	inline int32_t get_kScreenSize_40() const { return ___kScreenSize_40; }
	inline int32_t* get_address_of_kScreenSize_40() { return &___kScreenSize_40; }
	inline void set_kScreenSize_40(int32_t value)
	{
		___kScreenSize_40 = value;
	}

	inline static int32_t get_offset_of_kInvScreenSize_41() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kInvScreenSize_41)); }
	inline int32_t get_kInvScreenSize_41() const { return ___kInvScreenSize_41; }
	inline int32_t* get_address_of_kInvScreenSize_41() { return &___kInvScreenSize_41; }
	inline void set_kInvScreenSize_41(int32_t value)
	{
		___kInvScreenSize_41 = value;
	}

	inline static int32_t get_offset_of_kProjInfo_42() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kProjInfo_42)); }
	inline int32_t get_kProjInfo_42() const { return ___kProjInfo_42; }
	inline int32_t* get_address_of_kProjInfo_42() { return &___kProjInfo_42; }
	inline void set_kProjInfo_42(int32_t value)
	{
		___kProjInfo_42 = value;
	}

	inline static int32_t get_offset_of_kCameraClipInfo_43() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kCameraClipInfo_43)); }
	inline int32_t get_kCameraClipInfo_43() const { return ___kCameraClipInfo_43; }
	inline int32_t* get_address_of_kCameraClipInfo_43() { return &___kCameraClipInfo_43; }
	inline void set_kCameraClipInfo_43(int32_t value)
	{
		___kCameraClipInfo_43 = value;
	}

	inline static int32_t get_offset_of_kProjectToPixelMatrix_44() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kProjectToPixelMatrix_44)); }
	inline int32_t get_kProjectToPixelMatrix_44() const { return ___kProjectToPixelMatrix_44; }
	inline int32_t* get_address_of_kProjectToPixelMatrix_44() { return &___kProjectToPixelMatrix_44; }
	inline void set_kProjectToPixelMatrix_44(int32_t value)
	{
		___kProjectToPixelMatrix_44 = value;
	}

	inline static int32_t get_offset_of_kWorldToCameraMatrix_45() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kWorldToCameraMatrix_45)); }
	inline int32_t get_kWorldToCameraMatrix_45() const { return ___kWorldToCameraMatrix_45; }
	inline int32_t* get_address_of_kWorldToCameraMatrix_45() { return &___kWorldToCameraMatrix_45; }
	inline void set_kWorldToCameraMatrix_45(int32_t value)
	{
		___kWorldToCameraMatrix_45 = value;
	}

	inline static int32_t get_offset_of_kCameraToWorldMatrix_46() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kCameraToWorldMatrix_46)); }
	inline int32_t get_kCameraToWorldMatrix_46() const { return ___kCameraToWorldMatrix_46; }
	inline int32_t* get_address_of_kCameraToWorldMatrix_46() { return &___kCameraToWorldMatrix_46; }
	inline void set_kCameraToWorldMatrix_46(int32_t value)
	{
		___kCameraToWorldMatrix_46 = value;
	}

	inline static int32_t get_offset_of_kAxis_47() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kAxis_47)); }
	inline int32_t get_kAxis_47() const { return ___kAxis_47; }
	inline int32_t* get_address_of_kAxis_47() { return &___kAxis_47; }
	inline void set_kAxis_47(int32_t value)
	{
		___kAxis_47 = value;
	}

	inline static int32_t get_offset_of_kCurrentMipLevel_48() { return static_cast<int32_t>(offsetof(ScreenSpaceReflection_t4192727846_StaticFields, ___kCurrentMipLevel_48)); }
	inline int32_t get_kCurrentMipLevel_48() const { return ___kCurrentMipLevel_48; }
	inline int32_t* get_address_of_kCurrentMipLevel_48() { return &___kCurrentMipLevel_48; }
	inline void set_kCurrentMipLevel_48(int32_t value)
	{
		___kCurrentMipLevel_48 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTION_T4192727846_H
#ifndef TEMPORALANTIALIASING_T2972261765_H
#define TEMPORALANTIALIASING_T2972261765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing
struct  TemporalAntiAliasing_t2972261765  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.TemporalAntiAliasing/Settings UnityStandardAssets.CinematicEffects.TemporalAntiAliasing::settings
	Settings_t254817451 * ___settings_4;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.TemporalAntiAliasing::m_Shader
	Shader_t4151988712 * ___m_Shader_5;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.TemporalAntiAliasing::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Camera UnityStandardAssets.CinematicEffects.TemporalAntiAliasing::m_Camera
	Camera_t4157153871 * ___m_Camera_7;
	// UnityEngine.RenderTexture UnityStandardAssets.CinematicEffects.TemporalAntiAliasing::m_History
	RenderTexture_t2108887433 * ___m_History_8;
	// System.Int32 UnityStandardAssets.CinematicEffects.TemporalAntiAliasing::m_SampleIndex
	int32_t ___m_SampleIndex_9;

public:
	inline static int32_t get_offset_of_settings_4() { return static_cast<int32_t>(offsetof(TemporalAntiAliasing_t2972261765, ___settings_4)); }
	inline Settings_t254817451 * get_settings_4() const { return ___settings_4; }
	inline Settings_t254817451 ** get_address_of_settings_4() { return &___settings_4; }
	inline void set_settings_4(Settings_t254817451 * value)
	{
		___settings_4 = value;
		Il2CppCodeGenWriteBarrier((&___settings_4), value);
	}

	inline static int32_t get_offset_of_m_Shader_5() { return static_cast<int32_t>(offsetof(TemporalAntiAliasing_t2972261765, ___m_Shader_5)); }
	inline Shader_t4151988712 * get_m_Shader_5() const { return ___m_Shader_5; }
	inline Shader_t4151988712 ** get_address_of_m_Shader_5() { return &___m_Shader_5; }
	inline void set_m_Shader_5(Shader_t4151988712 * value)
	{
		___m_Shader_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Shader_5), value);
	}

	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(TemporalAntiAliasing_t2972261765, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Camera_7() { return static_cast<int32_t>(offsetof(TemporalAntiAliasing_t2972261765, ___m_Camera_7)); }
	inline Camera_t4157153871 * get_m_Camera_7() const { return ___m_Camera_7; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_7() { return &___m_Camera_7; }
	inline void set_m_Camera_7(Camera_t4157153871 * value)
	{
		___m_Camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_7), value);
	}

	inline static int32_t get_offset_of_m_History_8() { return static_cast<int32_t>(offsetof(TemporalAntiAliasing_t2972261765, ___m_History_8)); }
	inline RenderTexture_t2108887433 * get_m_History_8() const { return ___m_History_8; }
	inline RenderTexture_t2108887433 ** get_address_of_m_History_8() { return &___m_History_8; }
	inline void set_m_History_8(RenderTexture_t2108887433 * value)
	{
		___m_History_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_History_8), value);
	}

	inline static int32_t get_offset_of_m_SampleIndex_9() { return static_cast<int32_t>(offsetof(TemporalAntiAliasing_t2972261765, ___m_SampleIndex_9)); }
	inline int32_t get_m_SampleIndex_9() const { return ___m_SampleIndex_9; }
	inline int32_t* get_address_of_m_SampleIndex_9() { return &___m_SampleIndex_9; }
	inline void set_m_SampleIndex_9(int32_t value)
	{
		___m_SampleIndex_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEMPORALANTIALIASING_T2972261765_H
#ifndef TONEMAPPINGCOLORGRADING_T3182490393_H
#define TONEMAPPINGCOLORGRADING_T3182490393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.CinematicEffects.TonemappingColorGrading
struct  TonemappingColorGrading_t3182490393  : public MonoBehaviour_t3962482529
{
public:
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/EyeAdaptationSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_EyeAdaptation
	EyeAdaptationSettings_t3234830401  ___m_EyeAdaptation_4;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/TonemappingSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Tonemapping
	TonemappingSettings_t1161324624  ___m_Tonemapping_5;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/ColorGradingSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_ColorGrading
	ColorGradingSettings_t1923563914  ___m_ColorGrading_6;
	// UnityStandardAssets.CinematicEffects.TonemappingColorGrading/LUTSettings UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Lut
	LUTSettings_t2339616782  ___m_Lut_7;
	// UnityEngine.Texture2D UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_IdentityLut
	Texture2D_t3840446185 * ___m_IdentityLut_8;
	// UnityEngine.RenderTexture UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_InternalLut
	RenderTexture_t2108887433 * ___m_InternalLut_9;
	// UnityEngine.Texture2D UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_CurveTexture
	Texture2D_t3840446185 * ___m_CurveTexture_10;
	// UnityEngine.Texture2D UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_TonemapperCurve
	Texture2D_t3840446185 * ___m_TonemapperCurve_11;
	// System.Single UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_TonemapperCurveRange
	float ___m_TonemapperCurveRange_12;
	// UnityEngine.Shader UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Shader
	Shader_t4151988712 * ___m_Shader_13;
	// UnityEngine.Material UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Material
	Material_t340375123 * ___m_Material_14;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading::<validRenderTextureFormat>k__BackingField
	bool ___U3CvalidRenderTextureFormatU3Ek__BackingField_15;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading::<validUserLutSize>k__BackingField
	bool ___U3CvalidUserLutSizeU3Ek__BackingField_16;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Dirty
	bool ___m_Dirty_17;
	// System.Boolean UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_TonemapperDirty
	bool ___m_TonemapperDirty_18;
	// UnityEngine.RenderTexture UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_SmallAdaptiveRt
	RenderTexture_t2108887433 * ___m_SmallAdaptiveRt_19;
	// UnityEngine.RenderTextureFormat UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_AdaptiveRtFormat
	int32_t ___m_AdaptiveRtFormat_20;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_AdaptationSpeed
	int32_t ___m_AdaptationSpeed_21;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_MiddleGrey
	int32_t ___m_MiddleGrey_22;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_AdaptationMin
	int32_t ___m_AdaptationMin_23;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_AdaptationMax
	int32_t ___m_AdaptationMax_24;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_LumTex
	int32_t ___m_LumTex_25;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_ToneCurveRange
	int32_t ___m_ToneCurveRange_26;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_ToneCurve
	int32_t ___m_ToneCurve_27;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Exposure
	int32_t ___m_Exposure_28;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_NeutralTonemapperParams1
	int32_t ___m_NeutralTonemapperParams1_29;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_NeutralTonemapperParams2
	int32_t ___m_NeutralTonemapperParams2_30;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_WhiteBalance
	int32_t ___m_WhiteBalance_31;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Lift
	int32_t ___m_Lift_32;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Gamma
	int32_t ___m_Gamma_33;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Gain
	int32_t ___m_Gain_34;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_ContrastGainGamma
	int32_t ___m_ContrastGainGamma_35;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_Vibrance
	int32_t ___m_Vibrance_36;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_HSV
	int32_t ___m_HSV_37;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_ChannelMixerRed
	int32_t ___m_ChannelMixerRed_38;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_ChannelMixerGreen
	int32_t ___m_ChannelMixerGreen_39;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_ChannelMixerBlue
	int32_t ___m_ChannelMixerBlue_40;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_CurveTex
	int32_t ___m_CurveTex_41;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_InternalLutTex
	int32_t ___m_InternalLutTex_42;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_InternalLutParams
	int32_t ___m_InternalLutParams_43;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_UserLutTex
	int32_t ___m_UserLutTex_44;
	// System.Int32 UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_UserLutParams
	int32_t ___m_UserLutParams_45;
	// UnityEngine.RenderTexture[] UnityStandardAssets.CinematicEffects.TonemappingColorGrading::m_AdaptRts
	RenderTextureU5BU5D_t4111643188* ___m_AdaptRts_46;

public:
	inline static int32_t get_offset_of_m_EyeAdaptation_4() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_EyeAdaptation_4)); }
	inline EyeAdaptationSettings_t3234830401  get_m_EyeAdaptation_4() const { return ___m_EyeAdaptation_4; }
	inline EyeAdaptationSettings_t3234830401 * get_address_of_m_EyeAdaptation_4() { return &___m_EyeAdaptation_4; }
	inline void set_m_EyeAdaptation_4(EyeAdaptationSettings_t3234830401  value)
	{
		___m_EyeAdaptation_4 = value;
	}

	inline static int32_t get_offset_of_m_Tonemapping_5() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Tonemapping_5)); }
	inline TonemappingSettings_t1161324624  get_m_Tonemapping_5() const { return ___m_Tonemapping_5; }
	inline TonemappingSettings_t1161324624 * get_address_of_m_Tonemapping_5() { return &___m_Tonemapping_5; }
	inline void set_m_Tonemapping_5(TonemappingSettings_t1161324624  value)
	{
		___m_Tonemapping_5 = value;
	}

	inline static int32_t get_offset_of_m_ColorGrading_6() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_ColorGrading_6)); }
	inline ColorGradingSettings_t1923563914  get_m_ColorGrading_6() const { return ___m_ColorGrading_6; }
	inline ColorGradingSettings_t1923563914 * get_address_of_m_ColorGrading_6() { return &___m_ColorGrading_6; }
	inline void set_m_ColorGrading_6(ColorGradingSettings_t1923563914  value)
	{
		___m_ColorGrading_6 = value;
	}

	inline static int32_t get_offset_of_m_Lut_7() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Lut_7)); }
	inline LUTSettings_t2339616782  get_m_Lut_7() const { return ___m_Lut_7; }
	inline LUTSettings_t2339616782 * get_address_of_m_Lut_7() { return &___m_Lut_7; }
	inline void set_m_Lut_7(LUTSettings_t2339616782  value)
	{
		___m_Lut_7 = value;
	}

	inline static int32_t get_offset_of_m_IdentityLut_8() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_IdentityLut_8)); }
	inline Texture2D_t3840446185 * get_m_IdentityLut_8() const { return ___m_IdentityLut_8; }
	inline Texture2D_t3840446185 ** get_address_of_m_IdentityLut_8() { return &___m_IdentityLut_8; }
	inline void set_m_IdentityLut_8(Texture2D_t3840446185 * value)
	{
		___m_IdentityLut_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_IdentityLut_8), value);
	}

	inline static int32_t get_offset_of_m_InternalLut_9() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_InternalLut_9)); }
	inline RenderTexture_t2108887433 * get_m_InternalLut_9() const { return ___m_InternalLut_9; }
	inline RenderTexture_t2108887433 ** get_address_of_m_InternalLut_9() { return &___m_InternalLut_9; }
	inline void set_m_InternalLut_9(RenderTexture_t2108887433 * value)
	{
		___m_InternalLut_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLut_9), value);
	}

	inline static int32_t get_offset_of_m_CurveTexture_10() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_CurveTexture_10)); }
	inline Texture2D_t3840446185 * get_m_CurveTexture_10() const { return ___m_CurveTexture_10; }
	inline Texture2D_t3840446185 ** get_address_of_m_CurveTexture_10() { return &___m_CurveTexture_10; }
	inline void set_m_CurveTexture_10(Texture2D_t3840446185 * value)
	{
		___m_CurveTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurveTexture_10), value);
	}

	inline static int32_t get_offset_of_m_TonemapperCurve_11() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_TonemapperCurve_11)); }
	inline Texture2D_t3840446185 * get_m_TonemapperCurve_11() const { return ___m_TonemapperCurve_11; }
	inline Texture2D_t3840446185 ** get_address_of_m_TonemapperCurve_11() { return &___m_TonemapperCurve_11; }
	inline void set_m_TonemapperCurve_11(Texture2D_t3840446185 * value)
	{
		___m_TonemapperCurve_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TonemapperCurve_11), value);
	}

	inline static int32_t get_offset_of_m_TonemapperCurveRange_12() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_TonemapperCurveRange_12)); }
	inline float get_m_TonemapperCurveRange_12() const { return ___m_TonemapperCurveRange_12; }
	inline float* get_address_of_m_TonemapperCurveRange_12() { return &___m_TonemapperCurveRange_12; }
	inline void set_m_TonemapperCurveRange_12(float value)
	{
		___m_TonemapperCurveRange_12 = value;
	}

	inline static int32_t get_offset_of_m_Shader_13() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Shader_13)); }
	inline Shader_t4151988712 * get_m_Shader_13() const { return ___m_Shader_13; }
	inline Shader_t4151988712 ** get_address_of_m_Shader_13() { return &___m_Shader_13; }
	inline void set_m_Shader_13(Shader_t4151988712 * value)
	{
		___m_Shader_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Shader_13), value);
	}

	inline static int32_t get_offset_of_m_Material_14() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Material_14)); }
	inline Material_t340375123 * get_m_Material_14() const { return ___m_Material_14; }
	inline Material_t340375123 ** get_address_of_m_Material_14() { return &___m_Material_14; }
	inline void set_m_Material_14(Material_t340375123 * value)
	{
		___m_Material_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_14), value);
	}

	inline static int32_t get_offset_of_U3CvalidRenderTextureFormatU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___U3CvalidRenderTextureFormatU3Ek__BackingField_15)); }
	inline bool get_U3CvalidRenderTextureFormatU3Ek__BackingField_15() const { return ___U3CvalidRenderTextureFormatU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CvalidRenderTextureFormatU3Ek__BackingField_15() { return &___U3CvalidRenderTextureFormatU3Ek__BackingField_15; }
	inline void set_U3CvalidRenderTextureFormatU3Ek__BackingField_15(bool value)
	{
		___U3CvalidRenderTextureFormatU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CvalidUserLutSizeU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___U3CvalidUserLutSizeU3Ek__BackingField_16)); }
	inline bool get_U3CvalidUserLutSizeU3Ek__BackingField_16() const { return ___U3CvalidUserLutSizeU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CvalidUserLutSizeU3Ek__BackingField_16() { return &___U3CvalidUserLutSizeU3Ek__BackingField_16; }
	inline void set_U3CvalidUserLutSizeU3Ek__BackingField_16(bool value)
	{
		___U3CvalidUserLutSizeU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_Dirty_17() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Dirty_17)); }
	inline bool get_m_Dirty_17() const { return ___m_Dirty_17; }
	inline bool* get_address_of_m_Dirty_17() { return &___m_Dirty_17; }
	inline void set_m_Dirty_17(bool value)
	{
		___m_Dirty_17 = value;
	}

	inline static int32_t get_offset_of_m_TonemapperDirty_18() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_TonemapperDirty_18)); }
	inline bool get_m_TonemapperDirty_18() const { return ___m_TonemapperDirty_18; }
	inline bool* get_address_of_m_TonemapperDirty_18() { return &___m_TonemapperDirty_18; }
	inline void set_m_TonemapperDirty_18(bool value)
	{
		___m_TonemapperDirty_18 = value;
	}

	inline static int32_t get_offset_of_m_SmallAdaptiveRt_19() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_SmallAdaptiveRt_19)); }
	inline RenderTexture_t2108887433 * get_m_SmallAdaptiveRt_19() const { return ___m_SmallAdaptiveRt_19; }
	inline RenderTexture_t2108887433 ** get_address_of_m_SmallAdaptiveRt_19() { return &___m_SmallAdaptiveRt_19; }
	inline void set_m_SmallAdaptiveRt_19(RenderTexture_t2108887433 * value)
	{
		___m_SmallAdaptiveRt_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_SmallAdaptiveRt_19), value);
	}

	inline static int32_t get_offset_of_m_AdaptiveRtFormat_20() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_AdaptiveRtFormat_20)); }
	inline int32_t get_m_AdaptiveRtFormat_20() const { return ___m_AdaptiveRtFormat_20; }
	inline int32_t* get_address_of_m_AdaptiveRtFormat_20() { return &___m_AdaptiveRtFormat_20; }
	inline void set_m_AdaptiveRtFormat_20(int32_t value)
	{
		___m_AdaptiveRtFormat_20 = value;
	}

	inline static int32_t get_offset_of_m_AdaptationSpeed_21() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_AdaptationSpeed_21)); }
	inline int32_t get_m_AdaptationSpeed_21() const { return ___m_AdaptationSpeed_21; }
	inline int32_t* get_address_of_m_AdaptationSpeed_21() { return &___m_AdaptationSpeed_21; }
	inline void set_m_AdaptationSpeed_21(int32_t value)
	{
		___m_AdaptationSpeed_21 = value;
	}

	inline static int32_t get_offset_of_m_MiddleGrey_22() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_MiddleGrey_22)); }
	inline int32_t get_m_MiddleGrey_22() const { return ___m_MiddleGrey_22; }
	inline int32_t* get_address_of_m_MiddleGrey_22() { return &___m_MiddleGrey_22; }
	inline void set_m_MiddleGrey_22(int32_t value)
	{
		___m_MiddleGrey_22 = value;
	}

	inline static int32_t get_offset_of_m_AdaptationMin_23() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_AdaptationMin_23)); }
	inline int32_t get_m_AdaptationMin_23() const { return ___m_AdaptationMin_23; }
	inline int32_t* get_address_of_m_AdaptationMin_23() { return &___m_AdaptationMin_23; }
	inline void set_m_AdaptationMin_23(int32_t value)
	{
		___m_AdaptationMin_23 = value;
	}

	inline static int32_t get_offset_of_m_AdaptationMax_24() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_AdaptationMax_24)); }
	inline int32_t get_m_AdaptationMax_24() const { return ___m_AdaptationMax_24; }
	inline int32_t* get_address_of_m_AdaptationMax_24() { return &___m_AdaptationMax_24; }
	inline void set_m_AdaptationMax_24(int32_t value)
	{
		___m_AdaptationMax_24 = value;
	}

	inline static int32_t get_offset_of_m_LumTex_25() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_LumTex_25)); }
	inline int32_t get_m_LumTex_25() const { return ___m_LumTex_25; }
	inline int32_t* get_address_of_m_LumTex_25() { return &___m_LumTex_25; }
	inline void set_m_LumTex_25(int32_t value)
	{
		___m_LumTex_25 = value;
	}

	inline static int32_t get_offset_of_m_ToneCurveRange_26() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_ToneCurveRange_26)); }
	inline int32_t get_m_ToneCurveRange_26() const { return ___m_ToneCurveRange_26; }
	inline int32_t* get_address_of_m_ToneCurveRange_26() { return &___m_ToneCurveRange_26; }
	inline void set_m_ToneCurveRange_26(int32_t value)
	{
		___m_ToneCurveRange_26 = value;
	}

	inline static int32_t get_offset_of_m_ToneCurve_27() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_ToneCurve_27)); }
	inline int32_t get_m_ToneCurve_27() const { return ___m_ToneCurve_27; }
	inline int32_t* get_address_of_m_ToneCurve_27() { return &___m_ToneCurve_27; }
	inline void set_m_ToneCurve_27(int32_t value)
	{
		___m_ToneCurve_27 = value;
	}

	inline static int32_t get_offset_of_m_Exposure_28() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Exposure_28)); }
	inline int32_t get_m_Exposure_28() const { return ___m_Exposure_28; }
	inline int32_t* get_address_of_m_Exposure_28() { return &___m_Exposure_28; }
	inline void set_m_Exposure_28(int32_t value)
	{
		___m_Exposure_28 = value;
	}

	inline static int32_t get_offset_of_m_NeutralTonemapperParams1_29() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_NeutralTonemapperParams1_29)); }
	inline int32_t get_m_NeutralTonemapperParams1_29() const { return ___m_NeutralTonemapperParams1_29; }
	inline int32_t* get_address_of_m_NeutralTonemapperParams1_29() { return &___m_NeutralTonemapperParams1_29; }
	inline void set_m_NeutralTonemapperParams1_29(int32_t value)
	{
		___m_NeutralTonemapperParams1_29 = value;
	}

	inline static int32_t get_offset_of_m_NeutralTonemapperParams2_30() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_NeutralTonemapperParams2_30)); }
	inline int32_t get_m_NeutralTonemapperParams2_30() const { return ___m_NeutralTonemapperParams2_30; }
	inline int32_t* get_address_of_m_NeutralTonemapperParams2_30() { return &___m_NeutralTonemapperParams2_30; }
	inline void set_m_NeutralTonemapperParams2_30(int32_t value)
	{
		___m_NeutralTonemapperParams2_30 = value;
	}

	inline static int32_t get_offset_of_m_WhiteBalance_31() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_WhiteBalance_31)); }
	inline int32_t get_m_WhiteBalance_31() const { return ___m_WhiteBalance_31; }
	inline int32_t* get_address_of_m_WhiteBalance_31() { return &___m_WhiteBalance_31; }
	inline void set_m_WhiteBalance_31(int32_t value)
	{
		___m_WhiteBalance_31 = value;
	}

	inline static int32_t get_offset_of_m_Lift_32() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Lift_32)); }
	inline int32_t get_m_Lift_32() const { return ___m_Lift_32; }
	inline int32_t* get_address_of_m_Lift_32() { return &___m_Lift_32; }
	inline void set_m_Lift_32(int32_t value)
	{
		___m_Lift_32 = value;
	}

	inline static int32_t get_offset_of_m_Gamma_33() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Gamma_33)); }
	inline int32_t get_m_Gamma_33() const { return ___m_Gamma_33; }
	inline int32_t* get_address_of_m_Gamma_33() { return &___m_Gamma_33; }
	inline void set_m_Gamma_33(int32_t value)
	{
		___m_Gamma_33 = value;
	}

	inline static int32_t get_offset_of_m_Gain_34() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Gain_34)); }
	inline int32_t get_m_Gain_34() const { return ___m_Gain_34; }
	inline int32_t* get_address_of_m_Gain_34() { return &___m_Gain_34; }
	inline void set_m_Gain_34(int32_t value)
	{
		___m_Gain_34 = value;
	}

	inline static int32_t get_offset_of_m_ContrastGainGamma_35() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_ContrastGainGamma_35)); }
	inline int32_t get_m_ContrastGainGamma_35() const { return ___m_ContrastGainGamma_35; }
	inline int32_t* get_address_of_m_ContrastGainGamma_35() { return &___m_ContrastGainGamma_35; }
	inline void set_m_ContrastGainGamma_35(int32_t value)
	{
		___m_ContrastGainGamma_35 = value;
	}

	inline static int32_t get_offset_of_m_Vibrance_36() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_Vibrance_36)); }
	inline int32_t get_m_Vibrance_36() const { return ___m_Vibrance_36; }
	inline int32_t* get_address_of_m_Vibrance_36() { return &___m_Vibrance_36; }
	inline void set_m_Vibrance_36(int32_t value)
	{
		___m_Vibrance_36 = value;
	}

	inline static int32_t get_offset_of_m_HSV_37() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_HSV_37)); }
	inline int32_t get_m_HSV_37() const { return ___m_HSV_37; }
	inline int32_t* get_address_of_m_HSV_37() { return &___m_HSV_37; }
	inline void set_m_HSV_37(int32_t value)
	{
		___m_HSV_37 = value;
	}

	inline static int32_t get_offset_of_m_ChannelMixerRed_38() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_ChannelMixerRed_38)); }
	inline int32_t get_m_ChannelMixerRed_38() const { return ___m_ChannelMixerRed_38; }
	inline int32_t* get_address_of_m_ChannelMixerRed_38() { return &___m_ChannelMixerRed_38; }
	inline void set_m_ChannelMixerRed_38(int32_t value)
	{
		___m_ChannelMixerRed_38 = value;
	}

	inline static int32_t get_offset_of_m_ChannelMixerGreen_39() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_ChannelMixerGreen_39)); }
	inline int32_t get_m_ChannelMixerGreen_39() const { return ___m_ChannelMixerGreen_39; }
	inline int32_t* get_address_of_m_ChannelMixerGreen_39() { return &___m_ChannelMixerGreen_39; }
	inline void set_m_ChannelMixerGreen_39(int32_t value)
	{
		___m_ChannelMixerGreen_39 = value;
	}

	inline static int32_t get_offset_of_m_ChannelMixerBlue_40() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_ChannelMixerBlue_40)); }
	inline int32_t get_m_ChannelMixerBlue_40() const { return ___m_ChannelMixerBlue_40; }
	inline int32_t* get_address_of_m_ChannelMixerBlue_40() { return &___m_ChannelMixerBlue_40; }
	inline void set_m_ChannelMixerBlue_40(int32_t value)
	{
		___m_ChannelMixerBlue_40 = value;
	}

	inline static int32_t get_offset_of_m_CurveTex_41() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_CurveTex_41)); }
	inline int32_t get_m_CurveTex_41() const { return ___m_CurveTex_41; }
	inline int32_t* get_address_of_m_CurveTex_41() { return &___m_CurveTex_41; }
	inline void set_m_CurveTex_41(int32_t value)
	{
		___m_CurveTex_41 = value;
	}

	inline static int32_t get_offset_of_m_InternalLutTex_42() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_InternalLutTex_42)); }
	inline int32_t get_m_InternalLutTex_42() const { return ___m_InternalLutTex_42; }
	inline int32_t* get_address_of_m_InternalLutTex_42() { return &___m_InternalLutTex_42; }
	inline void set_m_InternalLutTex_42(int32_t value)
	{
		___m_InternalLutTex_42 = value;
	}

	inline static int32_t get_offset_of_m_InternalLutParams_43() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_InternalLutParams_43)); }
	inline int32_t get_m_InternalLutParams_43() const { return ___m_InternalLutParams_43; }
	inline int32_t* get_address_of_m_InternalLutParams_43() { return &___m_InternalLutParams_43; }
	inline void set_m_InternalLutParams_43(int32_t value)
	{
		___m_InternalLutParams_43 = value;
	}

	inline static int32_t get_offset_of_m_UserLutTex_44() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_UserLutTex_44)); }
	inline int32_t get_m_UserLutTex_44() const { return ___m_UserLutTex_44; }
	inline int32_t* get_address_of_m_UserLutTex_44() { return &___m_UserLutTex_44; }
	inline void set_m_UserLutTex_44(int32_t value)
	{
		___m_UserLutTex_44 = value;
	}

	inline static int32_t get_offset_of_m_UserLutParams_45() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_UserLutParams_45)); }
	inline int32_t get_m_UserLutParams_45() const { return ___m_UserLutParams_45; }
	inline int32_t* get_address_of_m_UserLutParams_45() { return &___m_UserLutParams_45; }
	inline void set_m_UserLutParams_45(int32_t value)
	{
		___m_UserLutParams_45 = value;
	}

	inline static int32_t get_offset_of_m_AdaptRts_46() { return static_cast<int32_t>(offsetof(TonemappingColorGrading_t3182490393, ___m_AdaptRts_46)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_AdaptRts_46() const { return ___m_AdaptRts_46; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_AdaptRts_46() { return &___m_AdaptRts_46; }
	inline void set_m_AdaptRts_46(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_AdaptRts_46 = value;
		Il2CppCodeGenWriteBarrier((&___m_AdaptRts_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPINGCOLORGRADING_T3182490393_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (DepthOfField_t2645519978), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[33] = 
{
	0,
	DepthOfField_t2645519978::get_offset_of_settings_5(),
	DepthOfField_t2645519978::get_offset_of_focus_6(),
	DepthOfField_t2645519978::get_offset_of_bokehTexture_7(),
	DepthOfField_t2645519978::get_offset_of_m_FilmicDepthOfFieldShader_8(),
	DepthOfField_t2645519978::get_offset_of_m_MedianFilterShader_9(),
	DepthOfField_t2645519978::get_offset_of_m_TextureBokehShader_10(),
	DepthOfField_t2645519978::get_offset_of_m_RTU_11(),
	DepthOfField_t2645519978::get_offset_of_m_FilmicDepthOfFieldMaterial_12(),
	DepthOfField_t2645519978::get_offset_of_m_MedianFilterMaterial_13(),
	DepthOfField_t2645519978::get_offset_of_m_TextureBokehMaterial_14(),
	DepthOfField_t2645519978::get_offset_of_m_ComputeBufferDrawArgs_15(),
	DepthOfField_t2645519978::get_offset_of_m_ComputeBufferPoints_16(),
	DepthOfField_t2645519978::get_offset_of_m_CurrentQualitySettings_17(),
	DepthOfField_t2645519978::get_offset_of_m_LastApertureOrientation_18(),
	DepthOfField_t2645519978::get_offset_of_m_OctogonalBokehDirection1_19(),
	DepthOfField_t2645519978::get_offset_of_m_OctogonalBokehDirection2_20(),
	DepthOfField_t2645519978::get_offset_of_m_OctogonalBokehDirection3_21(),
	DepthOfField_t2645519978::get_offset_of_m_OctogonalBokehDirection4_22(),
	DepthOfField_t2645519978::get_offset_of_m_HexagonalBokehDirection1_23(),
	DepthOfField_t2645519978::get_offset_of_m_HexagonalBokehDirection2_24(),
	DepthOfField_t2645519978::get_offset_of_m_HexagonalBokehDirection3_25(),
	DepthOfField_t2645519978::get_offset_of_m_BlurParams_26(),
	DepthOfField_t2645519978::get_offset_of_m_BlurCoe_27(),
	DepthOfField_t2645519978::get_offset_of_m_Offsets_28(),
	DepthOfField_t2645519978::get_offset_of_m_BlurredColor_29(),
	DepthOfField_t2645519978::get_offset_of_m_SpawnHeuristic_30(),
	DepthOfField_t2645519978::get_offset_of_m_BokehParams_31(),
	DepthOfField_t2645519978::get_offset_of_m_Convolved_TexelSize_32(),
	DepthOfField_t2645519978::get_offset_of_m_SecondTex_33(),
	DepthOfField_t2645519978::get_offset_of_m_ThirdTex_34(),
	DepthOfField_t2645519978::get_offset_of_m_MainTex_35(),
	DepthOfField_t2645519978::get_offset_of_m_Screen_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (Passes_t633783710)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2801[25] = 
{
	Passes_t633783710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (MedianPasses_t4189517256)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2802[3] = 
{
	MedianPasses_t4189517256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (BokehTexturesPasses_t3576012827)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2803[3] = 
{
	BokehTexturesPasses_t3576012827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (TweakMode_t577622288)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2804[3] = 
{
	TweakMode_t577622288::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (ApertureShape_t3609884370)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2805[4] = 
{
	ApertureShape_t3609884370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (QualityPreset_t1593743734)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2806[4] = 
{
	QualityPreset_t1593743734::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (FilterQuality_t3841223768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2807[4] = 
{
	FilterQuality_t3841223768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (GlobalSettings_t956974295)+ sizeof (RuntimeObject), sizeof(GlobalSettings_t956974295_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2808[5] = 
{
	GlobalSettings_t956974295::get_offset_of_visualizeFocus_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlobalSettings_t956974295::get_offset_of_tweakMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlobalSettings_t956974295::get_offset_of_filteringQuality_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlobalSettings_t956974295::get_offset_of_apertureShape_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlobalSettings_t956974295::get_offset_of_apertureOrientation_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (QualitySettings_t1379802392)+ sizeof (RuntimeObject), sizeof(QualitySettings_t1379802392_marshaled_pinvoke), sizeof(QualitySettings_t1379802392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2809[4] = 
{
	QualitySettings_t1379802392::get_offset_of_prefilterBlur_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QualitySettings_t1379802392::get_offset_of_medianFilter_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QualitySettings_t1379802392::get_offset_of_dilateNearBlur_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QualitySettings_t1379802392_StaticFields::get_offset_of_presetQualitySettings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (FocusSettings_t1261455774)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[9] = 
{
	FocusSettings_t1261455774::get_offset_of_transform_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FocusSettings_t1261455774::get_offset_of_focusPlane_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FocusSettings_t1261455774::get_offset_of_range_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FocusSettings_t1261455774::get_offset_of_nearPlane_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FocusSettings_t1261455774::get_offset_of_nearFalloff_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FocusSettings_t1261455774::get_offset_of_farPlane_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FocusSettings_t1261455774::get_offset_of_farFalloff_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FocusSettings_t1261455774::get_offset_of_nearBlurRadius_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FocusSettings_t1261455774::get_offset_of_farBlurRadius_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (BokehTextureSettings_t3943002634)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[5] = 
{
	BokehTextureSettings_t3943002634::get_offset_of_texture_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BokehTextureSettings_t3943002634::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BokehTextureSettings_t3943002634::get_offset_of_intensity_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BokehTextureSettings_t3943002634::get_offset_of_threshold_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BokehTextureSettings_t3943002634::get_offset_of_spawnHeuristic_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (LensAberrations_t456692924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[16] = 
{
	LensAberrations_t456692924::get_offset_of_distortion_4(),
	LensAberrations_t456692924::get_offset_of_vignette_5(),
	LensAberrations_t456692924::get_offset_of_chromaticAberration_6(),
	LensAberrations_t456692924::get_offset_of_m_Shader_7(),
	LensAberrations_t456692924::get_offset_of_m_Material_8(),
	LensAberrations_t456692924::get_offset_of_m_RTU_9(),
	LensAberrations_t456692924::get_offset_of_m_DistCenterScale_10(),
	LensAberrations_t456692924::get_offset_of_m_DistAmount_11(),
	LensAberrations_t456692924::get_offset_of_m_ChromaticAberration_12(),
	LensAberrations_t456692924::get_offset_of_m_VignetteColor_13(),
	LensAberrations_t456692924::get_offset_of_m_BlurPass_14(),
	LensAberrations_t456692924::get_offset_of_m_BlurTex_15(),
	LensAberrations_t456692924::get_offset_of_m_VignetteBlur_16(),
	LensAberrations_t456692924::get_offset_of_m_VignetteDesat_17(),
	LensAberrations_t456692924::get_offset_of_m_VignetteCenter_18(),
	LensAberrations_t456692924::get_offset_of_m_VignetteSettings_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (SettingsGroup_t1699980700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (AdvancedSetting_t4217923793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (DistortionSettings_t3193728992)+ sizeof (RuntimeObject), sizeof(DistortionSettings_t3193728992_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2815[7] = 
{
	DistortionSettings_t3193728992::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DistortionSettings_t3193728992::get_offset_of_amount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DistortionSettings_t3193728992::get_offset_of_centerX_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DistortionSettings_t3193728992::get_offset_of_centerY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DistortionSettings_t3193728992::get_offset_of_amountX_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DistortionSettings_t3193728992::get_offset_of_amountY_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DistortionSettings_t3193728992::get_offset_of_scale_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (VignetteSettings_t2342083791)+ sizeof (RuntimeObject), sizeof(VignetteSettings_t2342083791_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2816[8] = 
{
	VignetteSettings_t2342083791::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VignetteSettings_t2342083791::get_offset_of_color_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VignetteSettings_t2342083791::get_offset_of_center_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VignetteSettings_t2342083791::get_offset_of_intensity_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VignetteSettings_t2342083791::get_offset_of_smoothness_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VignetteSettings_t2342083791::get_offset_of_roundness_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VignetteSettings_t2342083791::get_offset_of_blur_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VignetteSettings_t2342083791::get_offset_of_desaturate_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (ChromaticAberrationSettings_t3807027201)+ sizeof (RuntimeObject), sizeof(ChromaticAberrationSettings_t3807027201_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2817[3] = 
{
	ChromaticAberrationSettings_t3807027201::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChromaticAberrationSettings_t3807027201::get_offset_of_color_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChromaticAberrationSettings_t3807027201::get_offset_of_amount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (Pass_t1667699932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2818[9] = 
{
	Pass_t1667699932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (MotionBlur_t437291541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[5] = 
{
	MotionBlur_t437291541::get_offset_of__settings_4(),
	MotionBlur_t437291541::get_offset_of__reconstructionShader_5(),
	MotionBlur_t437291541::get_offset_of__frameBlendingShader_6(),
	MotionBlur_t437291541::get_offset_of__reconstructionFilter_7(),
	MotionBlur_t437291541::get_offset_of__frameBlendingFilter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (FrameBlendingFilter_t4031465840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[17] = 
{
	FrameBlendingFilter_t4031465840::get_offset_of__useCompression_0(),
	FrameBlendingFilter_t4031465840::get_offset_of__rawTextureFormat_1(),
	FrameBlendingFilter_t4031465840::get_offset_of__material_2(),
	FrameBlendingFilter_t4031465840::get_offset_of__frameList_3(),
	FrameBlendingFilter_t4031465840::get_offset_of__lastFrameCount_4(),
	FrameBlendingFilter_t4031465840::get_offset_of__History1LumaTex_5(),
	FrameBlendingFilter_t4031465840::get_offset_of__History2LumaTex_6(),
	FrameBlendingFilter_t4031465840::get_offset_of__History3LumaTex_7(),
	FrameBlendingFilter_t4031465840::get_offset_of__History4LumaTex_8(),
	FrameBlendingFilter_t4031465840::get_offset_of__History1ChromaTex_9(),
	FrameBlendingFilter_t4031465840::get_offset_of__History2ChromaTex_10(),
	FrameBlendingFilter_t4031465840::get_offset_of__History3ChromaTex_11(),
	FrameBlendingFilter_t4031465840::get_offset_of__History4ChromaTex_12(),
	FrameBlendingFilter_t4031465840::get_offset_of__History1Weight_13(),
	FrameBlendingFilter_t4031465840::get_offset_of__History2Weight_14(),
	FrameBlendingFilter_t4031465840::get_offset_of__History3Weight_15(),
	FrameBlendingFilter_t4031465840::get_offset_of__History4Weight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (Frame_t3511111948)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[4] = 
{
	Frame_t3511111948::get_offset_of_lumaTexture_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t3511111948::get_offset_of_chromaTexture_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t3511111948::get_offset_of_time_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Frame_t3511111948::get_offset_of__mrt_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (ReconstructionFilter_t4011033762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[12] = 
{
	0,
	ReconstructionFilter_t4011033762::get_offset_of__material_1(),
	ReconstructionFilter_t4011033762::get_offset_of__unroll_2(),
	ReconstructionFilter_t4011033762::get_offset_of__vectorRTFormat_3(),
	ReconstructionFilter_t4011033762::get_offset_of__packedRTFormat_4(),
	ReconstructionFilter_t4011033762::get_offset_of__VelocityScale_5(),
	ReconstructionFilter_t4011033762::get_offset_of__MaxBlurRadius_6(),
	ReconstructionFilter_t4011033762::get_offset_of__TileMaxOffs_7(),
	ReconstructionFilter_t4011033762::get_offset_of__TileMaxLoop_8(),
	ReconstructionFilter_t4011033762::get_offset_of__LoopCount_9(),
	ReconstructionFilter_t4011033762::get_offset_of__NeighborMaxTex_10(),
	ReconstructionFilter_t4011033762::get_offset_of__VelocityTex_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (Settings_t1356140371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[3] = 
{
	Settings_t1356140371::get_offset_of_shutterAngle_0(),
	Settings_t1356140371::get_offset_of_sampleCount_1(),
	Settings_t1356140371::get_offset_of_frameBlending_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (ScreenSpaceReflection_t4192727846), -1, sizeof(ScreenSpaceReflection_t4192727846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2824[45] = 
{
	ScreenSpaceReflection_t4192727846::get_offset_of_settings_4(),
	ScreenSpaceReflection_t4192727846::get_offset_of_highlightSuppression_5(),
	ScreenSpaceReflection_t4192727846::get_offset_of_traceBehindObjects_6(),
	ScreenSpaceReflection_t4192727846::get_offset_of_treatBackfaceHitAsMiss_7(),
	ScreenSpaceReflection_t4192727846::get_offset_of_bilateralUpsample_8(),
	ScreenSpaceReflection_t4192727846::get_offset_of_m_Shader_9(),
	ScreenSpaceReflection_t4192727846::get_offset_of_m_Material_10(),
	ScreenSpaceReflection_t4192727846::get_offset_of_m_Camera_11(),
	ScreenSpaceReflection_t4192727846::get_offset_of_m_CommandBuffer_12(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kNormalAndRoughnessTexture_13(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kHitPointTexture_14(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kReflectionTextures_15(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kFilteredReflections_16(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kBlurTexture_17(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kFinalReflectionTexture_18(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kTempTexture_19(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kRayStepSize_20(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kAdditiveReflection_21(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kBilateralUpsampling_22(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kTreatBackfaceHitAsMiss_23(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kAllowBackwardsRays_24(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kTraceBehindObjects_25(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kMaxSteps_26(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kFullResolutionFiltering_27(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kHalfResolution_28(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kHighlightSuppression_29(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kPixelsPerMeterAtOneMeter_30(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kScreenEdgeFading_31(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kReflectionBlur_32(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kMaxRayTraceDistance_33(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kFadeDistance_34(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kLayerThickness_35(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kSSRMultiplier_36(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kFresnelFade_37(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kFresnelFadePower_38(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kReflectionBufferSize_39(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kScreenSize_40(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kInvScreenSize_41(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kProjInfo_42(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kCameraClipInfo_43(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kProjectToPixelMatrix_44(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kWorldToCameraMatrix_45(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kCameraToWorldMatrix_46(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kAxis_47(),
	ScreenSpaceReflection_t4192727846_StaticFields::get_offset_of_kCurrentMipLevel_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (SSRResolution_t1711022735)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2825[3] = 
{
	SSRResolution_t1711022735::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (SSRReflectionBlendType_t2762048609)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2826[3] = 
{
	SSRReflectionBlendType_t2762048609::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (SSRSettings_t899296535)+ sizeof (RuntimeObject), sizeof(SSRSettings_t899296535_marshaled_pinvoke), sizeof(SSRSettings_t899296535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2827[4] = 
{
	SSRSettings_t899296535::get_offset_of_reflectionSettings_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SSRSettings_t899296535::get_offset_of_intensitySettings_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SSRSettings_t899296535::get_offset_of_screenEdgeMask_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SSRSettings_t899296535_StaticFields::get_offset_of_s_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (LayoutAttribute_t894525338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (IntensitySettings_t3790221060)+ sizeof (RuntimeObject), sizeof(IntensitySettings_t3790221060 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2829[4] = 
{
	IntensitySettings_t3790221060::get_offset_of_reflectionMultiplier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t3790221060::get_offset_of_fadeDistance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t3790221060::get_offset_of_fresnelFade_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t3790221060::get_offset_of_fresnelFadePower_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (ReflectionSettings_t499321483)+ sizeof (RuntimeObject), sizeof(ReflectionSettings_t499321483_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2830[8] = 
{
	ReflectionSettings_t499321483::get_offset_of_blendType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t499321483::get_offset_of_reflectionQuality_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t499321483::get_offset_of_maxDistance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t499321483::get_offset_of_iterationCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t499321483::get_offset_of_stepSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t499321483::get_offset_of_widthModifier_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t499321483::get_offset_of_reflectionBlur_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t499321483::get_offset_of_reflectBackfaces_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (ScreenEdgeMask_t3779436815)+ sizeof (RuntimeObject), sizeof(ScreenEdgeMask_t3779436815 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2831[1] = 
{
	ScreenEdgeMask_t3779436815::get_offset_of_intensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (PassIndex_t2581702242)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2832[10] = 
{
	PassIndex_t2581702242::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (TemporalAntiAliasing_t2972261765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[6] = 
{
	TemporalAntiAliasing_t2972261765::get_offset_of_settings_4(),
	TemporalAntiAliasing_t2972261765::get_offset_of_m_Shader_5(),
	TemporalAntiAliasing_t2972261765::get_offset_of_m_Material_6(),
	TemporalAntiAliasing_t2972261765::get_offset_of_m_Camera_7(),
	TemporalAntiAliasing_t2972261765::get_offset_of_m_History_8(),
	TemporalAntiAliasing_t2972261765::get_offset_of_m_SampleIndex_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (Sequence_t1852423168)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2834[2] = 
{
	Sequence_t1852423168::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (JitterSettings_t1123664989)+ sizeof (RuntimeObject), sizeof(JitterSettings_t1123664989 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2835[3] = 
{
	JitterSettings_t1123664989::get_offset_of_sequence_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JitterSettings_t1123664989::get_offset_of_spread_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	JitterSettings_t1123664989::get_offset_of_sampleCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (SharpenFilterSettings_t2911684052)+ sizeof (RuntimeObject), sizeof(SharpenFilterSettings_t2911684052 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2836[1] = 
{
	SharpenFilterSettings_t2911684052::get_offset_of_amount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (BlendSettings_t3588719326)+ sizeof (RuntimeObject), sizeof(BlendSettings_t3588719326 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2837[3] = 
{
	BlendSettings_t3588719326::get_offset_of_stationary_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlendSettings_t3588719326::get_offset_of_moving_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BlendSettings_t3588719326::get_offset_of_motionAmplification_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (DebugSettings_t54746148)+ sizeof (RuntimeObject), sizeof(DebugSettings_t54746148_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2838[1] = 
{
	DebugSettings_t54746148::get_offset_of_forceRepaint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (Settings_t254817451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[4] = 
{
	Settings_t254817451::get_offset_of_jitterSettings_0(),
	Settings_t254817451::get_offset_of_sharpenFilterSettings_1(),
	Settings_t254817451::get_offset_of_blendSettings_2(),
	Settings_t254817451::get_offset_of_debugSettings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (LayoutAttribute_t3939207293), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (TonemappingColorGrading_t3182490393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[43] = 
{
	TonemappingColorGrading_t3182490393::get_offset_of_m_EyeAdaptation_4(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Tonemapping_5(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_ColorGrading_6(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Lut_7(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_IdentityLut_8(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_InternalLut_9(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_CurveTexture_10(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_TonemapperCurve_11(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_TonemapperCurveRange_12(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Shader_13(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Material_14(),
	TonemappingColorGrading_t3182490393::get_offset_of_U3CvalidRenderTextureFormatU3Ek__BackingField_15(),
	TonemappingColorGrading_t3182490393::get_offset_of_U3CvalidUserLutSizeU3Ek__BackingField_16(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Dirty_17(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_TonemapperDirty_18(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_SmallAdaptiveRt_19(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_AdaptiveRtFormat_20(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_AdaptationSpeed_21(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_MiddleGrey_22(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_AdaptationMin_23(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_AdaptationMax_24(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_LumTex_25(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_ToneCurveRange_26(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_ToneCurve_27(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Exposure_28(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_NeutralTonemapperParams1_29(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_NeutralTonemapperParams2_30(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_WhiteBalance_31(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Lift_32(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Gamma_33(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Gain_34(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_ContrastGainGamma_35(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_Vibrance_36(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_HSV_37(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_ChannelMixerRed_38(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_ChannelMixerGreen_39(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_ChannelMixerBlue_40(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_CurveTex_41(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_InternalLutTex_42(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_InternalLutParams_43(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_UserLutTex_44(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_UserLutParams_45(),
	TonemappingColorGrading_t3182490393::get_offset_of_m_AdaptRts_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (SettingsGroup_t3113343738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (IndentedGroup_t2664173756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (ChannelMixer_t2896160762), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (ColorWheelGroup_t3707110132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[2] = 
{
	ColorWheelGroup_t3707110132::get_offset_of_minSizePerWheel_0(),
	ColorWheelGroup_t3707110132::get_offset_of_maxSizePerWheel_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (Curve_t1983851910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[1] = 
{
	Curve_t1983851910::get_offset_of_color_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (EyeAdaptationSettings_t3234830401)+ sizeof (RuntimeObject), sizeof(EyeAdaptationSettings_t3234830401_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2847[6] = 
{
	EyeAdaptationSettings_t3234830401::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyeAdaptationSettings_t3234830401::get_offset_of_middleGrey_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyeAdaptationSettings_t3234830401::get_offset_of_min_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyeAdaptationSettings_t3234830401::get_offset_of_max_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyeAdaptationSettings_t3234830401::get_offset_of_speed_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EyeAdaptationSettings_t3234830401::get_offset_of_showDebug_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (Tonemapper_t2495473297)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2848[8] = 
{
	Tonemapper_t2495473297::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (TonemappingSettings_t1161324624)+ sizeof (RuntimeObject), sizeof(TonemappingSettings_t1161324624_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2849[10] = 
{
	TonemappingSettings_t1161324624::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_tonemapper_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_exposure_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_curve_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_neutralBlackIn_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_neutralWhiteIn_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_neutralBlackOut_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_neutralWhiteOut_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_neutralWhiteLevel_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TonemappingSettings_t1161324624::get_offset_of_neutralWhiteClip_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (LUTSettings_t2339616782)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[3] = 
{
	LUTSettings_t2339616782::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LUTSettings_t2339616782::get_offset_of_texture_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LUTSettings_t2339616782::get_offset_of_contribution_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (ColorWheelsSettings_t1334875082)+ sizeof (RuntimeObject), sizeof(ColorWheelsSettings_t1334875082 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2851[3] = 
{
	ColorWheelsSettings_t1334875082::get_offset_of_shadows_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorWheelsSettings_t1334875082::get_offset_of_midtones_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorWheelsSettings_t1334875082::get_offset_of_highlights_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (BasicsSettings_t2030707387)+ sizeof (RuntimeObject), sizeof(BasicsSettings_t2030707387 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2852[9] = 
{
	BasicsSettings_t2030707387::get_offset_of_temperatureShift_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicsSettings_t2030707387::get_offset_of_tint_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicsSettings_t2030707387::get_offset_of_hue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicsSettings_t2030707387::get_offset_of_saturation_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicsSettings_t2030707387::get_offset_of_vibrance_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicsSettings_t2030707387::get_offset_of_value_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicsSettings_t2030707387::get_offset_of_contrast_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicsSettings_t2030707387::get_offset_of_gain_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BasicsSettings_t2030707387::get_offset_of_gamma_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (ChannelMixerSettings_t491321292)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[2] = 
{
	ChannelMixerSettings_t491321292::get_offset_of_currentChannel_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelMixerSettings_t491321292::get_offset_of_channels_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (CurvesSettings_t2932745847)+ sizeof (RuntimeObject), sizeof(CurvesSettings_t2932745847_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2854[4] = 
{
	CurvesSettings_t2932745847::get_offset_of_master_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2932745847::get_offset_of_red_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2932745847::get_offset_of_green_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2932745847::get_offset_of_blue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (ColorGradingPrecision_t3999355687)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2855[3] = 
{
	ColorGradingPrecision_t3999355687::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (ColorGradingSettings_t1923563914)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[8] = 
{
	ColorGradingSettings_t1923563914::get_offset_of_enabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorGradingSettings_t1923563914::get_offset_of_precision_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorGradingSettings_t1923563914::get_offset_of_colorWheels_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorGradingSettings_t1923563914::get_offset_of_basics_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorGradingSettings_t1923563914::get_offset_of_channelMixer_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorGradingSettings_t1923563914::get_offset_of_curves_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorGradingSettings_t1923563914::get_offset_of_useDithering_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorGradingSettings_t1923563914::get_offset_of_showDebug_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (Pass_t1214953023)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2857[14] = 
{
	Pass_t1214953023::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2858[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D8D0C1DCA7F35F40B810D754C5F5EC7C4D6110D41_0(),
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (U24ArrayTypeU3D16_t3253128245)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t3253128245 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (U24ArrayTypeU3D12_t2488454199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454199 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
