﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Gvr.Internal.IKeyboardProvider
struct IKeyboardProvider_t1978250355;
// GvrHeadset
struct GvrHeadset_t1874684537;
// GvrKeyboard
struct GvrKeyboard_t2536560201;
// GvrKeyboard/EditTextCallback
struct EditTextCallback_t1702213000;
// GvrKeyboard/ErrorCallback
struct ErrorCallback_t2310212740;
// GvrKeyboard/KeyboardCallback
struct KeyboardCallback_t3330588312;
// GvrKeyboard/StandardCallback
struct StandardCallback_t3095007891;
// GvrKeyboardDelegateBase
struct GvrKeyboardDelegateBase_t30895224;
// GvrVideoPlayerTexture
struct GvrVideoPlayerTexture_t3546202735;
// GvrVideoPlayerTexture/OnExceptionCallback
struct OnExceptionCallback_t1696428116;
// GvrVideoPlayerTexture/OnVideoEventCallback
struct OnVideoEventCallback_t2376626694;
// KeyboardState
struct KeyboardState_t4109162649;
// MirrorCameraScript
struct MirrorCameraScript_t1086824577;
// MirrorScript
struct MirrorScript_t2344066790;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`2<System.String,System.String>
struct Action_2_t3970170674;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<GvrKeyboardEvent>
struct List_1_t806272884;
// System.Collections.Generic.List`1<System.Action`1<System.Int32>>
struct List_1_t300520794;
// System.Collections.Generic.List`1<System.Action`2<System.String,System.String>>
struct List_1_t1147278120;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t1260619206;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t2924027637;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t427135887;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t1110636971;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.UInt32[]
struct UInt32U5BU5D_t2770800703;
// System.Void
struct Void_t1185182177;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t1033194329;
// UnityEngine.ComputeShader
struct ComputeShader_t317220254;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.PostProcessing.AmbientOcclusionModel
struct AmbientOcclusionModel_t389471066;
// UnityEngine.PostProcessing.AntialiasingModel
struct AntialiasingModel_t1521139388;
// UnityEngine.PostProcessing.BloomModel
struct BloomModel_t2099727860;
// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray
struct ArrowArray_t303178545;
// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct BuiltinDebugViewsModel_t1462618840;
// UnityEngine.PostProcessing.ChromaticAberrationModel
struct ChromaticAberrationModel_t3963399853;
// UnityEngine.PostProcessing.ColorGradingModel
struct ColorGradingModel_t1448048181;
// UnityEngine.PostProcessing.DepthOfFieldModel
struct DepthOfFieldModel_t514067330;
// UnityEngine.PostProcessing.DitheringModel
struct DitheringModel_t2429005396;
// UnityEngine.PostProcessing.EyeAdaptationModel
struct EyeAdaptationModel_t242823912;
// UnityEngine.PostProcessing.FogModel
struct FogModel_t3620688749;
// UnityEngine.PostProcessing.GrainModel
struct GrainModel_t1152882488;
// UnityEngine.PostProcessing.PostProcessingContext
struct PostProcessingContext_t2014408948;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rendering.RenderTargetIdentifier[]
struct RenderTargetIdentifierU5BU5D_t2742279485;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t149664596;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t2532145056;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t3520241082;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t4040729994;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t3270282352;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t1438173104;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Selectable
struct Selectable_t3250028441;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DUMMYKEYBOARDPROVIDER_T4235634217_H
#define DUMMYKEYBOARDPROVIDER_T4235634217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.DummyKeyboardProvider
struct  DummyKeyboardProvider_t4235634217  : public RuntimeObject
{
public:
	// KeyboardState Gvr.Internal.DummyKeyboardProvider::dummyState
	KeyboardState_t4109162649 * ___dummyState_0;
	// System.String Gvr.Internal.DummyKeyboardProvider::<EditorText>k__BackingField
	String_t* ___U3CEditorTextU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(DummyKeyboardProvider_t4235634217, ___dummyState_0)); }
	inline KeyboardState_t4109162649 * get_dummyState_0() const { return ___dummyState_0; }
	inline KeyboardState_t4109162649 ** get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(KeyboardState_t4109162649 * value)
	{
		___dummyState_0 = value;
		Il2CppCodeGenWriteBarrier((&___dummyState_0), value);
	}

	inline static int32_t get_offset_of_U3CEditorTextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DummyKeyboardProvider_t4235634217, ___U3CEditorTextU3Ek__BackingField_1)); }
	inline String_t* get_U3CEditorTextU3Ek__BackingField_1() const { return ___U3CEditorTextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEditorTextU3Ek__BackingField_1() { return &___U3CEditorTextU3Ek__BackingField_1; }
	inline void set_U3CEditorTextU3Ek__BackingField_1(String_t* value)
	{
		___U3CEditorTextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEditorTextU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYKEYBOARDPROVIDER_T4235634217_H
#ifndef GVRCURSORHELPER_T4026897861_H
#define GVRCURSORHELPER_T4026897861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.GvrCursorHelper
struct  GvrCursorHelper_t4026897861  : public RuntimeObject
{
public:

public:
};

struct GvrCursorHelper_t4026897861_StaticFields
{
public:
	// System.Boolean Gvr.Internal.GvrCursorHelper::cachedHeadEmulationActive
	bool ___cachedHeadEmulationActive_0;
	// System.Boolean Gvr.Internal.GvrCursorHelper::cachedControllerEmulationActive
	bool ___cachedControllerEmulationActive_1;

public:
	inline static int32_t get_offset_of_cachedHeadEmulationActive_0() { return static_cast<int32_t>(offsetof(GvrCursorHelper_t4026897861_StaticFields, ___cachedHeadEmulationActive_0)); }
	inline bool get_cachedHeadEmulationActive_0() const { return ___cachedHeadEmulationActive_0; }
	inline bool* get_address_of_cachedHeadEmulationActive_0() { return &___cachedHeadEmulationActive_0; }
	inline void set_cachedHeadEmulationActive_0(bool value)
	{
		___cachedHeadEmulationActive_0 = value;
	}

	inline static int32_t get_offset_of_cachedControllerEmulationActive_1() { return static_cast<int32_t>(offsetof(GvrCursorHelper_t4026897861_StaticFields, ___cachedControllerEmulationActive_1)); }
	inline bool get_cachedControllerEmulationActive_1() const { return ___cachedControllerEmulationActive_1; }
	inline bool* get_address_of_cachedControllerEmulationActive_1() { return &___cachedControllerEmulationActive_1; }
	inline void set_cachedControllerEmulationActive_1(bool value)
	{
		___cachedControllerEmulationActive_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRCURSORHELPER_T4026897861_H
#ifndef HEADSETPROVIDERFACTORY_T520764709_H
#define HEADSETPROVIDERFACTORY_T520764709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.HeadsetProviderFactory
struct  HeadsetProviderFactory_t520764709  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETPROVIDERFACTORY_T520764709_H
#ifndef KEYBOARDPROVIDERFACTORY_T3069358895_H
#define KEYBOARDPROVIDERFACTORY_T3069358895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.KeyboardProviderFactory
struct  KeyboardProviderFactory_t3069358895  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDPROVIDERFACTORY_T3069358895_H
#ifndef GVRACTIVITYHELPER_T700161863_H
#define GVRACTIVITYHELPER_T700161863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrActivityHelper
struct  GvrActivityHelper_t700161863  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRACTIVITYHELPER_T700161863_H
#ifndef GVRDAYDREAMAPI_T820520409_H
#define GVRDAYDREAMAPI_T820520409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDaydreamApi
struct  GvrDaydreamApi_t820520409  : public RuntimeObject
{
public:

public:
};

struct GvrDaydreamApi_t820520409_StaticFields
{
public:
	// GvrDaydreamApi GvrDaydreamApi::m_instance
	GvrDaydreamApi_t820520409 * ___m_instance_4;

public:
	inline static int32_t get_offset_of_m_instance_4() { return static_cast<int32_t>(offsetof(GvrDaydreamApi_t820520409_StaticFields, ___m_instance_4)); }
	inline GvrDaydreamApi_t820520409 * get_m_instance_4() const { return ___m_instance_4; }
	inline GvrDaydreamApi_t820520409 ** get_address_of_m_instance_4() { return &___m_instance_4; }
	inline void set_m_instance_4(GvrDaydreamApi_t820520409 * value)
	{
		___m_instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRDAYDREAMAPI_T820520409_H
#ifndef U3CLAUNCHVRHOMEASYNCU3EC__ANONSTOREY0_T1042273844_H
#define U3CLAUNCHVRHOMEASYNCU3EC__ANONSTOREY0_T1042273844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDaydreamApi/<LaunchVrHomeAsync>c__AnonStorey0
struct  U3CLaunchVrHomeAsyncU3Ec__AnonStorey0_t1042273844  : public RuntimeObject
{
public:
	// System.Action`1<System.Boolean> GvrDaydreamApi/<LaunchVrHomeAsync>c__AnonStorey0::callback
	Action_1_t269755560 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CLaunchVrHomeAsyncU3Ec__AnonStorey0_t1042273844, ___callback_0)); }
	inline Action_1_t269755560 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t269755560 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t269755560 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLAUNCHVRHOMEASYNCU3EC__ANONSTOREY0_T1042273844_H
#ifndef U3CENDOFFRAMEU3EC__ITERATOR0_T1803228010_H
#define U3CENDOFFRAMEU3EC__ITERATOR0_T1803228010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrHeadset/<EndOfFrame>c__Iterator0
struct  U3CEndOfFrameU3Ec__Iterator0_t1803228010  : public RuntimeObject
{
public:
	// GvrHeadset GvrHeadset/<EndOfFrame>c__Iterator0::$this
	GvrHeadset_t1874684537 * ___U24this_0;
	// System.Object GvrHeadset/<EndOfFrame>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GvrHeadset/<EndOfFrame>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GvrHeadset/<EndOfFrame>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t1803228010, ___U24this_0)); }
	inline GvrHeadset_t1874684537 * get_U24this_0() const { return ___U24this_0; }
	inline GvrHeadset_t1874684537 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GvrHeadset_t1874684537 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t1803228010, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t1803228010, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t1803228010, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENDOFFRAMEU3EC__ITERATOR0_T1803228010_H
#ifndef GVRINTENT_T255451010_H
#define GVRINTENT_T255451010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrIntent
struct  GvrIntent_t255451010  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRINTENT_T255451010_H
#ifndef U3CEXECUTERU3EC__ITERATOR0_T892308174_H
#define U3CEXECUTERU3EC__ITERATOR0_T892308174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/<Executer>c__Iterator0
struct  U3CExecuterU3Ec__Iterator0_t892308174  : public RuntimeObject
{
public:
	// GvrKeyboard GvrKeyboard/<Executer>c__Iterator0::$this
	GvrKeyboard_t2536560201 * ___U24this_0;
	// System.Object GvrKeyboard/<Executer>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean GvrKeyboard/<Executer>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 GvrKeyboard/<Executer>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ec__Iterator0_t892308174, ___U24this_0)); }
	inline GvrKeyboard_t2536560201 * get_U24this_0() const { return ___U24this_0; }
	inline GvrKeyboard_t2536560201 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GvrKeyboard_t2536560201 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ec__Iterator0_t892308174, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ec__Iterator0_t892308174, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CExecuterU3Ec__Iterator0_t892308174, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTERU3EC__ITERATOR0_T892308174_H
#ifndef GVRKEYBOARDINTENT_T3874861606_H
#define GVRKEYBOARDINTENT_T3874861606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardIntent
struct  GvrKeyboardIntent_t3874861606  : public RuntimeObject
{
public:

public:
};

struct GvrKeyboardIntent_t3874861606_StaticFields
{
public:
	// GvrKeyboardIntent GvrKeyboardIntent::theInstance
	GvrKeyboardIntent_t3874861606 * ___theInstance_2;

public:
	inline static int32_t get_offset_of_theInstance_2() { return static_cast<int32_t>(offsetof(GvrKeyboardIntent_t3874861606_StaticFields, ___theInstance_2)); }
	inline GvrKeyboardIntent_t3874861606 * get_theInstance_2() const { return ___theInstance_2; }
	inline GvrKeyboardIntent_t3874861606 ** get_address_of_theInstance_2() { return &___theInstance_2; }
	inline void set_theInstance_2(GvrKeyboardIntent_t3874861606 * value)
	{
		___theInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___theInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDINTENT_T3874861606_H
#ifndef GVRMATHHELPERS_T769385329_H
#define GVRMATHHELPERS_T769385329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrMathHelpers
struct  GvrMathHelpers_t769385329  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRMATHHELPERS_T769385329_H
#ifndef GVRUIHELPERS_T853958893_H
#define GVRUIHELPERS_T853958893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrUIHelpers
struct  GvrUIHelpers_t853958893  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRUIHELPERS_T853958893_H
#ifndef GVRVRHELPERS_T1380670802_H
#define GVRVRHELPERS_T1380670802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVRHelpers
struct  GvrVRHelpers_t1380670802  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRVRHELPERS_T1380670802_H
#ifndef U3CINTERNALONEXCEPTIONCALLBACKU3EC__ANONSTOREY1_T3301768987_H
#define U3CINTERNALONEXCEPTIONCALLBACKU3EC__ANONSTOREY1_T3301768987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey1
struct  U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987  : public RuntimeObject
{
public:
	// GvrVideoPlayerTexture GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey1::player
	GvrVideoPlayerTexture_t3546202735 * ___player_0;
	// System.String GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey1::type
	String_t* ___type_1;
	// System.String GvrVideoPlayerTexture/<InternalOnExceptionCallback>c__AnonStorey1::msg
	String_t* ___msg_2;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987, ___player_0)); }
	inline GvrVideoPlayerTexture_t3546202735 * get_player_0() const { return ___player_0; }
	inline GvrVideoPlayerTexture_t3546202735 ** get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(GvrVideoPlayerTexture_t3546202735 * value)
	{
		___player_0 = value;
		Il2CppCodeGenWriteBarrier((&___player_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_msg_2() { return static_cast<int32_t>(offsetof(U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987, ___msg_2)); }
	inline String_t* get_msg_2() const { return ___msg_2; }
	inline String_t** get_address_of_msg_2() { return &___msg_2; }
	inline void set_msg_2(String_t* value)
	{
		___msg_2 = value;
		Il2CppCodeGenWriteBarrier((&___msg_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALONEXCEPTIONCALLBACKU3EC__ANONSTOREY1_T3301768987_H
#ifndef U3CINTERNALONVIDEOEVENTCALLBACKU3EC__ANONSTOREY0_T2222373319_H
#define U3CINTERNALONVIDEOEVENTCALLBACKU3EC__ANONSTOREY0_T2222373319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey0
struct  U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319  : public RuntimeObject
{
public:
	// GvrVideoPlayerTexture GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey0::player
	GvrVideoPlayerTexture_t3546202735 * ___player_0;
	// System.Int32 GvrVideoPlayerTexture/<InternalOnVideoEventCallback>c__AnonStorey0::eventId
	int32_t ___eventId_1;

public:
	inline static int32_t get_offset_of_player_0() { return static_cast<int32_t>(offsetof(U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319, ___player_0)); }
	inline GvrVideoPlayerTexture_t3546202735 * get_player_0() const { return ___player_0; }
	inline GvrVideoPlayerTexture_t3546202735 ** get_address_of_player_0() { return &___player_0; }
	inline void set_player_0(GvrVideoPlayerTexture_t3546202735 * value)
	{
		___player_0 = value;
		Il2CppCodeGenWriteBarrier((&___player_0), value);
	}

	inline static int32_t get_offset_of_eventId_1() { return static_cast<int32_t>(offsetof(U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319, ___eventId_1)); }
	inline int32_t get_eventId_1() const { return ___eventId_1; }
	inline int32_t* get_address_of_eventId_1() { return &___eventId_1; }
	inline void set_eventId_1(int32_t value)
	{
		___eventId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINTERNALONVIDEOEVENTCALLBACKU3EC__ANONSTOREY0_T2222373319_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef UNIFORMS_T3797733410_H
#define UNIFORMS_T3797733410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms
struct  Uniforms_t3797733410  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3797733410_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Intensity
	int32_t ____Intensity_0;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Radius
	int32_t ____Radius_1;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_FogParams
	int32_t ____FogParams_2;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_Downsample
	int32_t ____Downsample_3;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_SampleCount
	int32_t ____SampleCount_4;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture1
	int32_t ____OcclusionTexture1_5;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture2
	int32_t ____OcclusionTexture2_6;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_OcclusionTexture
	int32_t ____OcclusionTexture_7;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_MainTex
	int32_t ____MainTex_8;
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/Uniforms::_TempRT
	int32_t ____TempRT_9;

public:
	inline static int32_t get_offset_of__Intensity_0() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Intensity_0)); }
	inline int32_t get__Intensity_0() const { return ____Intensity_0; }
	inline int32_t* get_address_of__Intensity_0() { return &____Intensity_0; }
	inline void set__Intensity_0(int32_t value)
	{
		____Intensity_0 = value;
	}

	inline static int32_t get_offset_of__Radius_1() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Radius_1)); }
	inline int32_t get__Radius_1() const { return ____Radius_1; }
	inline int32_t* get_address_of__Radius_1() { return &____Radius_1; }
	inline void set__Radius_1(int32_t value)
	{
		____Radius_1 = value;
	}

	inline static int32_t get_offset_of__FogParams_2() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____FogParams_2)); }
	inline int32_t get__FogParams_2() const { return ____FogParams_2; }
	inline int32_t* get_address_of__FogParams_2() { return &____FogParams_2; }
	inline void set__FogParams_2(int32_t value)
	{
		____FogParams_2 = value;
	}

	inline static int32_t get_offset_of__Downsample_3() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____Downsample_3)); }
	inline int32_t get__Downsample_3() const { return ____Downsample_3; }
	inline int32_t* get_address_of__Downsample_3() { return &____Downsample_3; }
	inline void set__Downsample_3(int32_t value)
	{
		____Downsample_3 = value;
	}

	inline static int32_t get_offset_of__SampleCount_4() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____SampleCount_4)); }
	inline int32_t get__SampleCount_4() const { return ____SampleCount_4; }
	inline int32_t* get_address_of__SampleCount_4() { return &____SampleCount_4; }
	inline void set__SampleCount_4(int32_t value)
	{
		____SampleCount_4 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture1_5() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture1_5)); }
	inline int32_t get__OcclusionTexture1_5() const { return ____OcclusionTexture1_5; }
	inline int32_t* get_address_of__OcclusionTexture1_5() { return &____OcclusionTexture1_5; }
	inline void set__OcclusionTexture1_5(int32_t value)
	{
		____OcclusionTexture1_5 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture2_6() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture2_6)); }
	inline int32_t get__OcclusionTexture2_6() const { return ____OcclusionTexture2_6; }
	inline int32_t* get_address_of__OcclusionTexture2_6() { return &____OcclusionTexture2_6; }
	inline void set__OcclusionTexture2_6(int32_t value)
	{
		____OcclusionTexture2_6 = value;
	}

	inline static int32_t get_offset_of__OcclusionTexture_7() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____OcclusionTexture_7)); }
	inline int32_t get__OcclusionTexture_7() const { return ____OcclusionTexture_7; }
	inline int32_t* get_address_of__OcclusionTexture_7() { return &____OcclusionTexture_7; }
	inline void set__OcclusionTexture_7(int32_t value)
	{
		____OcclusionTexture_7 = value;
	}

	inline static int32_t get_offset_of__MainTex_8() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____MainTex_8)); }
	inline int32_t get__MainTex_8() const { return ____MainTex_8; }
	inline int32_t* get_address_of__MainTex_8() { return &____MainTex_8; }
	inline void set__MainTex_8(int32_t value)
	{
		____MainTex_8 = value;
	}

	inline static int32_t get_offset_of__TempRT_9() { return static_cast<int32_t>(offsetof(Uniforms_t3797733410_StaticFields, ____TempRT_9)); }
	inline int32_t get__TempRT_9() const { return ____TempRT_9; }
	inline int32_t* get_address_of__TempRT_9() { return &____TempRT_9; }
	inline void set__TempRT_9(int32_t value)
	{
		____TempRT_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3797733410_H
#ifndef UNIFORMS_T4164805197_H
#define UNIFORMS_T4164805197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent/Uniforms
struct  Uniforms_t4164805197  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t4164805197_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_AutoExposure
	int32_t ____AutoExposure_0;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Threshold
	int32_t ____Threshold_1;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Curve
	int32_t ____Curve_2;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_PrefilterOffs
	int32_t ____PrefilterOffs_3;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_SampleScale
	int32_t ____SampleScale_4;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_BaseTex
	int32_t ____BaseTex_5;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_BloomTex
	int32_t ____BloomTex_6;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_Settings
	int32_t ____Bloom_Settings_7;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_DirtTex
	int32_t ____Bloom_DirtTex_8;
	// System.Int32 UnityEngine.PostProcessing.BloomComponent/Uniforms::_Bloom_DirtIntensity
	int32_t ____Bloom_DirtIntensity_9;

public:
	inline static int32_t get_offset_of__AutoExposure_0() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____AutoExposure_0)); }
	inline int32_t get__AutoExposure_0() const { return ____AutoExposure_0; }
	inline int32_t* get_address_of__AutoExposure_0() { return &____AutoExposure_0; }
	inline void set__AutoExposure_0(int32_t value)
	{
		____AutoExposure_0 = value;
	}

	inline static int32_t get_offset_of__Threshold_1() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Threshold_1)); }
	inline int32_t get__Threshold_1() const { return ____Threshold_1; }
	inline int32_t* get_address_of__Threshold_1() { return &____Threshold_1; }
	inline void set__Threshold_1(int32_t value)
	{
		____Threshold_1 = value;
	}

	inline static int32_t get_offset_of__Curve_2() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Curve_2)); }
	inline int32_t get__Curve_2() const { return ____Curve_2; }
	inline int32_t* get_address_of__Curve_2() { return &____Curve_2; }
	inline void set__Curve_2(int32_t value)
	{
		____Curve_2 = value;
	}

	inline static int32_t get_offset_of__PrefilterOffs_3() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____PrefilterOffs_3)); }
	inline int32_t get__PrefilterOffs_3() const { return ____PrefilterOffs_3; }
	inline int32_t* get_address_of__PrefilterOffs_3() { return &____PrefilterOffs_3; }
	inline void set__PrefilterOffs_3(int32_t value)
	{
		____PrefilterOffs_3 = value;
	}

	inline static int32_t get_offset_of__SampleScale_4() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____SampleScale_4)); }
	inline int32_t get__SampleScale_4() const { return ____SampleScale_4; }
	inline int32_t* get_address_of__SampleScale_4() { return &____SampleScale_4; }
	inline void set__SampleScale_4(int32_t value)
	{
		____SampleScale_4 = value;
	}

	inline static int32_t get_offset_of__BaseTex_5() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____BaseTex_5)); }
	inline int32_t get__BaseTex_5() const { return ____BaseTex_5; }
	inline int32_t* get_address_of__BaseTex_5() { return &____BaseTex_5; }
	inline void set__BaseTex_5(int32_t value)
	{
		____BaseTex_5 = value;
	}

	inline static int32_t get_offset_of__BloomTex_6() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____BloomTex_6)); }
	inline int32_t get__BloomTex_6() const { return ____BloomTex_6; }
	inline int32_t* get_address_of__BloomTex_6() { return &____BloomTex_6; }
	inline void set__BloomTex_6(int32_t value)
	{
		____BloomTex_6 = value;
	}

	inline static int32_t get_offset_of__Bloom_Settings_7() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_Settings_7)); }
	inline int32_t get__Bloom_Settings_7() const { return ____Bloom_Settings_7; }
	inline int32_t* get_address_of__Bloom_Settings_7() { return &____Bloom_Settings_7; }
	inline void set__Bloom_Settings_7(int32_t value)
	{
		____Bloom_Settings_7 = value;
	}

	inline static int32_t get_offset_of__Bloom_DirtTex_8() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_DirtTex_8)); }
	inline int32_t get__Bloom_DirtTex_8() const { return ____Bloom_DirtTex_8; }
	inline int32_t* get_address_of__Bloom_DirtTex_8() { return &____Bloom_DirtTex_8; }
	inline void set__Bloom_DirtTex_8(int32_t value)
	{
		____Bloom_DirtTex_8 = value;
	}

	inline static int32_t get_offset_of__Bloom_DirtIntensity_9() { return static_cast<int32_t>(offsetof(Uniforms_t4164805197_StaticFields, ____Bloom_DirtIntensity_9)); }
	inline int32_t get__Bloom_DirtIntensity_9() const { return ____Bloom_DirtIntensity_9; }
	inline int32_t* get_address_of__Bloom_DirtIntensity_9() { return &____Bloom_DirtIntensity_9; }
	inline void set__Bloom_DirtIntensity_9(int32_t value)
	{
		____Bloom_DirtIntensity_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T4164805197_H
#ifndef ARROWARRAY_T303178545_H
#define ARROWARRAY_T303178545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray
struct  ArrowArray_t303178545  : public RuntimeObject
{
public:
	// UnityEngine.Mesh UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<mesh>k__BackingField
	Mesh_t3648964284 * ___U3CmeshU3Ek__BackingField_0;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<columnCount>k__BackingField
	int32_t ___U3CcolumnCountU3Ek__BackingField_1;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray::<rowCount>k__BackingField
	int32_t ___U3CrowCountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmeshU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CmeshU3Ek__BackingField_0)); }
	inline Mesh_t3648964284 * get_U3CmeshU3Ek__BackingField_0() const { return ___U3CmeshU3Ek__BackingField_0; }
	inline Mesh_t3648964284 ** get_address_of_U3CmeshU3Ek__BackingField_0() { return &___U3CmeshU3Ek__BackingField_0; }
	inline void set_U3CmeshU3Ek__BackingField_0(Mesh_t3648964284 * value)
	{
		___U3CmeshU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmeshU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CcolumnCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CcolumnCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CcolumnCountU3Ek__BackingField_1() const { return ___U3CcolumnCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CcolumnCountU3Ek__BackingField_1() { return &___U3CcolumnCountU3Ek__BackingField_1; }
	inline void set_U3CcolumnCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CcolumnCountU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CrowCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ArrowArray_t303178545, ___U3CrowCountU3Ek__BackingField_2)); }
	inline int32_t get_U3CrowCountU3Ek__BackingField_2() const { return ___U3CrowCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CrowCountU3Ek__BackingField_2() { return &___U3CrowCountU3Ek__BackingField_2; }
	inline void set_U3CrowCountU3Ek__BackingField_2(int32_t value)
	{
		___U3CrowCountU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARROWARRAY_T303178545_H
#ifndef UNIFORMS_T2158582951_H
#define UNIFORMS_T2158582951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms
struct  Uniforms_t2158582951  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t2158582951_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_DepthScale
	int32_t ____DepthScale_0;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_TempRT
	int32_t ____TempRT_1;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Opacity
	int32_t ____Opacity_2;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_MainTex
	int32_t ____MainTex_3;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_TempRT2
	int32_t ____TempRT2_4;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Amplitude
	int32_t ____Amplitude_5;
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Uniforms::_Scale
	int32_t ____Scale_6;

public:
	inline static int32_t get_offset_of__DepthScale_0() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____DepthScale_0)); }
	inline int32_t get__DepthScale_0() const { return ____DepthScale_0; }
	inline int32_t* get_address_of__DepthScale_0() { return &____DepthScale_0; }
	inline void set__DepthScale_0(int32_t value)
	{
		____DepthScale_0 = value;
	}

	inline static int32_t get_offset_of__TempRT_1() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____TempRT_1)); }
	inline int32_t get__TempRT_1() const { return ____TempRT_1; }
	inline int32_t* get_address_of__TempRT_1() { return &____TempRT_1; }
	inline void set__TempRT_1(int32_t value)
	{
		____TempRT_1 = value;
	}

	inline static int32_t get_offset_of__Opacity_2() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Opacity_2)); }
	inline int32_t get__Opacity_2() const { return ____Opacity_2; }
	inline int32_t* get_address_of__Opacity_2() { return &____Opacity_2; }
	inline void set__Opacity_2(int32_t value)
	{
		____Opacity_2 = value;
	}

	inline static int32_t get_offset_of__MainTex_3() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____MainTex_3)); }
	inline int32_t get__MainTex_3() const { return ____MainTex_3; }
	inline int32_t* get_address_of__MainTex_3() { return &____MainTex_3; }
	inline void set__MainTex_3(int32_t value)
	{
		____MainTex_3 = value;
	}

	inline static int32_t get_offset_of__TempRT2_4() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____TempRT2_4)); }
	inline int32_t get__TempRT2_4() const { return ____TempRT2_4; }
	inline int32_t* get_address_of__TempRT2_4() { return &____TempRT2_4; }
	inline void set__TempRT2_4(int32_t value)
	{
		____TempRT2_4 = value;
	}

	inline static int32_t get_offset_of__Amplitude_5() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Amplitude_5)); }
	inline int32_t get__Amplitude_5() const { return ____Amplitude_5; }
	inline int32_t* get_address_of__Amplitude_5() { return &____Amplitude_5; }
	inline void set__Amplitude_5(int32_t value)
	{
		____Amplitude_5 = value;
	}

	inline static int32_t get_offset_of__Scale_6() { return static_cast<int32_t>(offsetof(Uniforms_t2158582951_StaticFields, ____Scale_6)); }
	inline int32_t get__Scale_6() const { return ____Scale_6; }
	inline int32_t* get_address_of__Scale_6() { return &____Scale_6; }
	inline void set__Scale_6(int32_t value)
	{
		____Scale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T2158582951_H
#ifndef UNIFORMS_T424361460_H
#define UNIFORMS_T424361460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms
struct  Uniforms_t424361460  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t424361460_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms::_ChromaticAberration_Amount
	int32_t ____ChromaticAberration_Amount_0;
	// System.Int32 UnityEngine.PostProcessing.ChromaticAberrationComponent/Uniforms::_ChromaticAberration_Spectrum
	int32_t ____ChromaticAberration_Spectrum_1;

public:
	inline static int32_t get_offset_of__ChromaticAberration_Amount_0() { return static_cast<int32_t>(offsetof(Uniforms_t424361460_StaticFields, ____ChromaticAberration_Amount_0)); }
	inline int32_t get__ChromaticAberration_Amount_0() const { return ____ChromaticAberration_Amount_0; }
	inline int32_t* get_address_of__ChromaticAberration_Amount_0() { return &____ChromaticAberration_Amount_0; }
	inline void set__ChromaticAberration_Amount_0(int32_t value)
	{
		____ChromaticAberration_Amount_0 = value;
	}

	inline static int32_t get_offset_of__ChromaticAberration_Spectrum_1() { return static_cast<int32_t>(offsetof(Uniforms_t424361460_StaticFields, ____ChromaticAberration_Spectrum_1)); }
	inline int32_t get__ChromaticAberration_Spectrum_1() const { return ____ChromaticAberration_Spectrum_1; }
	inline int32_t* get_address_of__ChromaticAberration_Spectrum_1() { return &____ChromaticAberration_Spectrum_1; }
	inline void set__ChromaticAberration_Spectrum_1(int32_t value)
	{
		____ChromaticAberration_Spectrum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T424361460_H
#ifndef UNIFORMS_T3075607151_H
#define UNIFORMS_T3075607151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingComponent/Uniforms
struct  Uniforms_t3075607151  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3075607151_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LutParams
	int32_t ____LutParams_0;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_NeutralTonemapperParams1
	int32_t ____NeutralTonemapperParams1_1;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_NeutralTonemapperParams2
	int32_t ____NeutralTonemapperParams2_2;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_HueShift
	int32_t ____HueShift_3;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Saturation
	int32_t ____Saturation_4;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Contrast
	int32_t ____Contrast_5;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Balance
	int32_t ____Balance_6;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Lift
	int32_t ____Lift_7;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_InvGamma
	int32_t ____InvGamma_8;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Gain
	int32_t ____Gain_9;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Slope
	int32_t ____Slope_10;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Power
	int32_t ____Power_11;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Offset
	int32_t ____Offset_12;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerRed
	int32_t ____ChannelMixerRed_13;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerGreen
	int32_t ____ChannelMixerGreen_14;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ChannelMixerBlue
	int32_t ____ChannelMixerBlue_15;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_Curves
	int32_t ____Curves_16;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LogLut
	int32_t ____LogLut_17;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_LogLut_Params
	int32_t ____LogLut_Params_18;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingComponent/Uniforms::_ExposureEV
	int32_t ____ExposureEV_19;

public:
	inline static int32_t get_offset_of__LutParams_0() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LutParams_0)); }
	inline int32_t get__LutParams_0() const { return ____LutParams_0; }
	inline int32_t* get_address_of__LutParams_0() { return &____LutParams_0; }
	inline void set__LutParams_0(int32_t value)
	{
		____LutParams_0 = value;
	}

	inline static int32_t get_offset_of__NeutralTonemapperParams1_1() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____NeutralTonemapperParams1_1)); }
	inline int32_t get__NeutralTonemapperParams1_1() const { return ____NeutralTonemapperParams1_1; }
	inline int32_t* get_address_of__NeutralTonemapperParams1_1() { return &____NeutralTonemapperParams1_1; }
	inline void set__NeutralTonemapperParams1_1(int32_t value)
	{
		____NeutralTonemapperParams1_1 = value;
	}

	inline static int32_t get_offset_of__NeutralTonemapperParams2_2() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____NeutralTonemapperParams2_2)); }
	inline int32_t get__NeutralTonemapperParams2_2() const { return ____NeutralTonemapperParams2_2; }
	inline int32_t* get_address_of__NeutralTonemapperParams2_2() { return &____NeutralTonemapperParams2_2; }
	inline void set__NeutralTonemapperParams2_2(int32_t value)
	{
		____NeutralTonemapperParams2_2 = value;
	}

	inline static int32_t get_offset_of__HueShift_3() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____HueShift_3)); }
	inline int32_t get__HueShift_3() const { return ____HueShift_3; }
	inline int32_t* get_address_of__HueShift_3() { return &____HueShift_3; }
	inline void set__HueShift_3(int32_t value)
	{
		____HueShift_3 = value;
	}

	inline static int32_t get_offset_of__Saturation_4() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Saturation_4)); }
	inline int32_t get__Saturation_4() const { return ____Saturation_4; }
	inline int32_t* get_address_of__Saturation_4() { return &____Saturation_4; }
	inline void set__Saturation_4(int32_t value)
	{
		____Saturation_4 = value;
	}

	inline static int32_t get_offset_of__Contrast_5() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Contrast_5)); }
	inline int32_t get__Contrast_5() const { return ____Contrast_5; }
	inline int32_t* get_address_of__Contrast_5() { return &____Contrast_5; }
	inline void set__Contrast_5(int32_t value)
	{
		____Contrast_5 = value;
	}

	inline static int32_t get_offset_of__Balance_6() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Balance_6)); }
	inline int32_t get__Balance_6() const { return ____Balance_6; }
	inline int32_t* get_address_of__Balance_6() { return &____Balance_6; }
	inline void set__Balance_6(int32_t value)
	{
		____Balance_6 = value;
	}

	inline static int32_t get_offset_of__Lift_7() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Lift_7)); }
	inline int32_t get__Lift_7() const { return ____Lift_7; }
	inline int32_t* get_address_of__Lift_7() { return &____Lift_7; }
	inline void set__Lift_7(int32_t value)
	{
		____Lift_7 = value;
	}

	inline static int32_t get_offset_of__InvGamma_8() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____InvGamma_8)); }
	inline int32_t get__InvGamma_8() const { return ____InvGamma_8; }
	inline int32_t* get_address_of__InvGamma_8() { return &____InvGamma_8; }
	inline void set__InvGamma_8(int32_t value)
	{
		____InvGamma_8 = value;
	}

	inline static int32_t get_offset_of__Gain_9() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Gain_9)); }
	inline int32_t get__Gain_9() const { return ____Gain_9; }
	inline int32_t* get_address_of__Gain_9() { return &____Gain_9; }
	inline void set__Gain_9(int32_t value)
	{
		____Gain_9 = value;
	}

	inline static int32_t get_offset_of__Slope_10() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Slope_10)); }
	inline int32_t get__Slope_10() const { return ____Slope_10; }
	inline int32_t* get_address_of__Slope_10() { return &____Slope_10; }
	inline void set__Slope_10(int32_t value)
	{
		____Slope_10 = value;
	}

	inline static int32_t get_offset_of__Power_11() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Power_11)); }
	inline int32_t get__Power_11() const { return ____Power_11; }
	inline int32_t* get_address_of__Power_11() { return &____Power_11; }
	inline void set__Power_11(int32_t value)
	{
		____Power_11 = value;
	}

	inline static int32_t get_offset_of__Offset_12() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Offset_12)); }
	inline int32_t get__Offset_12() const { return ____Offset_12; }
	inline int32_t* get_address_of__Offset_12() { return &____Offset_12; }
	inline void set__Offset_12(int32_t value)
	{
		____Offset_12 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerRed_13() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerRed_13)); }
	inline int32_t get__ChannelMixerRed_13() const { return ____ChannelMixerRed_13; }
	inline int32_t* get_address_of__ChannelMixerRed_13() { return &____ChannelMixerRed_13; }
	inline void set__ChannelMixerRed_13(int32_t value)
	{
		____ChannelMixerRed_13 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerGreen_14() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerGreen_14)); }
	inline int32_t get__ChannelMixerGreen_14() const { return ____ChannelMixerGreen_14; }
	inline int32_t* get_address_of__ChannelMixerGreen_14() { return &____ChannelMixerGreen_14; }
	inline void set__ChannelMixerGreen_14(int32_t value)
	{
		____ChannelMixerGreen_14 = value;
	}

	inline static int32_t get_offset_of__ChannelMixerBlue_15() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ChannelMixerBlue_15)); }
	inline int32_t get__ChannelMixerBlue_15() const { return ____ChannelMixerBlue_15; }
	inline int32_t* get_address_of__ChannelMixerBlue_15() { return &____ChannelMixerBlue_15; }
	inline void set__ChannelMixerBlue_15(int32_t value)
	{
		____ChannelMixerBlue_15 = value;
	}

	inline static int32_t get_offset_of__Curves_16() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____Curves_16)); }
	inline int32_t get__Curves_16() const { return ____Curves_16; }
	inline int32_t* get_address_of__Curves_16() { return &____Curves_16; }
	inline void set__Curves_16(int32_t value)
	{
		____Curves_16 = value;
	}

	inline static int32_t get_offset_of__LogLut_17() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LogLut_17)); }
	inline int32_t get__LogLut_17() const { return ____LogLut_17; }
	inline int32_t* get_address_of__LogLut_17() { return &____LogLut_17; }
	inline void set__LogLut_17(int32_t value)
	{
		____LogLut_17 = value;
	}

	inline static int32_t get_offset_of__LogLut_Params_18() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____LogLut_Params_18)); }
	inline int32_t get__LogLut_Params_18() const { return ____LogLut_Params_18; }
	inline int32_t* get_address_of__LogLut_Params_18() { return &____LogLut_Params_18; }
	inline void set__LogLut_Params_18(int32_t value)
	{
		____LogLut_Params_18 = value;
	}

	inline static int32_t get_offset_of__ExposureEV_19() { return static_cast<int32_t>(offsetof(Uniforms_t3075607151_StaticFields, ____ExposureEV_19)); }
	inline int32_t get__ExposureEV_19() const { return ____ExposureEV_19; }
	inline int32_t* get_address_of__ExposureEV_19() { return &____ExposureEV_19; }
	inline void set__ExposureEV_19(int32_t value)
	{
		____ExposureEV_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3075607151_H
#ifndef UNIFORMS_T3629868803_H
#define UNIFORMS_T3629868803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms
struct  Uniforms_t3629868803  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3629868803_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_DepthOfFieldTex
	int32_t ____DepthOfFieldTex_0;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_DepthOfFieldCoCTex
	int32_t ____DepthOfFieldCoCTex_1;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_Distance
	int32_t ____Distance_2;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_LensCoeff
	int32_t ____LensCoeff_3;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_MaxCoC
	int32_t ____MaxCoC_4;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_RcpMaxCoC
	int32_t ____RcpMaxCoC_5;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_RcpAspect
	int32_t ____RcpAspect_6;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_MainTex
	int32_t ____MainTex_7;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_CoCTex
	int32_t ____CoCTex_8;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_TaaParams
	int32_t ____TaaParams_9;
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldComponent/Uniforms::_DepthOfFieldParams
	int32_t ____DepthOfFieldParams_10;

public:
	inline static int32_t get_offset_of__DepthOfFieldTex_0() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____DepthOfFieldTex_0)); }
	inline int32_t get__DepthOfFieldTex_0() const { return ____DepthOfFieldTex_0; }
	inline int32_t* get_address_of__DepthOfFieldTex_0() { return &____DepthOfFieldTex_0; }
	inline void set__DepthOfFieldTex_0(int32_t value)
	{
		____DepthOfFieldTex_0 = value;
	}

	inline static int32_t get_offset_of__DepthOfFieldCoCTex_1() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____DepthOfFieldCoCTex_1)); }
	inline int32_t get__DepthOfFieldCoCTex_1() const { return ____DepthOfFieldCoCTex_1; }
	inline int32_t* get_address_of__DepthOfFieldCoCTex_1() { return &____DepthOfFieldCoCTex_1; }
	inline void set__DepthOfFieldCoCTex_1(int32_t value)
	{
		____DepthOfFieldCoCTex_1 = value;
	}

	inline static int32_t get_offset_of__Distance_2() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____Distance_2)); }
	inline int32_t get__Distance_2() const { return ____Distance_2; }
	inline int32_t* get_address_of__Distance_2() { return &____Distance_2; }
	inline void set__Distance_2(int32_t value)
	{
		____Distance_2 = value;
	}

	inline static int32_t get_offset_of__LensCoeff_3() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____LensCoeff_3)); }
	inline int32_t get__LensCoeff_3() const { return ____LensCoeff_3; }
	inline int32_t* get_address_of__LensCoeff_3() { return &____LensCoeff_3; }
	inline void set__LensCoeff_3(int32_t value)
	{
		____LensCoeff_3 = value;
	}

	inline static int32_t get_offset_of__MaxCoC_4() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____MaxCoC_4)); }
	inline int32_t get__MaxCoC_4() const { return ____MaxCoC_4; }
	inline int32_t* get_address_of__MaxCoC_4() { return &____MaxCoC_4; }
	inline void set__MaxCoC_4(int32_t value)
	{
		____MaxCoC_4 = value;
	}

	inline static int32_t get_offset_of__RcpMaxCoC_5() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____RcpMaxCoC_5)); }
	inline int32_t get__RcpMaxCoC_5() const { return ____RcpMaxCoC_5; }
	inline int32_t* get_address_of__RcpMaxCoC_5() { return &____RcpMaxCoC_5; }
	inline void set__RcpMaxCoC_5(int32_t value)
	{
		____RcpMaxCoC_5 = value;
	}

	inline static int32_t get_offset_of__RcpAspect_6() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____RcpAspect_6)); }
	inline int32_t get__RcpAspect_6() const { return ____RcpAspect_6; }
	inline int32_t* get_address_of__RcpAspect_6() { return &____RcpAspect_6; }
	inline void set__RcpAspect_6(int32_t value)
	{
		____RcpAspect_6 = value;
	}

	inline static int32_t get_offset_of__MainTex_7() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____MainTex_7)); }
	inline int32_t get__MainTex_7() const { return ____MainTex_7; }
	inline int32_t* get_address_of__MainTex_7() { return &____MainTex_7; }
	inline void set__MainTex_7(int32_t value)
	{
		____MainTex_7 = value;
	}

	inline static int32_t get_offset_of__CoCTex_8() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____CoCTex_8)); }
	inline int32_t get__CoCTex_8() const { return ____CoCTex_8; }
	inline int32_t* get_address_of__CoCTex_8() { return &____CoCTex_8; }
	inline void set__CoCTex_8(int32_t value)
	{
		____CoCTex_8 = value;
	}

	inline static int32_t get_offset_of__TaaParams_9() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____TaaParams_9)); }
	inline int32_t get__TaaParams_9() const { return ____TaaParams_9; }
	inline int32_t* get_address_of__TaaParams_9() { return &____TaaParams_9; }
	inline void set__TaaParams_9(int32_t value)
	{
		____TaaParams_9 = value;
	}

	inline static int32_t get_offset_of__DepthOfFieldParams_10() { return static_cast<int32_t>(offsetof(Uniforms_t3629868803_StaticFields, ____DepthOfFieldParams_10)); }
	inline int32_t get__DepthOfFieldParams_10() const { return ____DepthOfFieldParams_10; }
	inline int32_t* get_address_of__DepthOfFieldParams_10() { return &____DepthOfFieldParams_10; }
	inline void set__DepthOfFieldParams_10(int32_t value)
	{
		____DepthOfFieldParams_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3629868803_H
#ifndef UNIFORMS_T3745258951_H
#define UNIFORMS_T3745258951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringComponent/Uniforms
struct  Uniforms_t3745258951  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t3745258951_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent/Uniforms::_DitheringTex
	int32_t ____DitheringTex_0;
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent/Uniforms::_DitheringCoords
	int32_t ____DitheringCoords_1;

public:
	inline static int32_t get_offset_of__DitheringTex_0() { return static_cast<int32_t>(offsetof(Uniforms_t3745258951_StaticFields, ____DitheringTex_0)); }
	inline int32_t get__DitheringTex_0() const { return ____DitheringTex_0; }
	inline int32_t* get_address_of__DitheringTex_0() { return &____DitheringTex_0; }
	inline void set__DitheringTex_0(int32_t value)
	{
		____DitheringTex_0 = value;
	}

	inline static int32_t get_offset_of__DitheringCoords_1() { return static_cast<int32_t>(offsetof(Uniforms_t3745258951_StaticFields, ____DitheringCoords_1)); }
	inline int32_t get__DitheringCoords_1() const { return ____DitheringCoords_1; }
	inline int32_t* get_address_of__DitheringCoords_1() { return &____DitheringCoords_1; }
	inline void set__DitheringCoords_1(int32_t value)
	{
		____DitheringCoords_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T3745258951_H
#ifndef UNIFORMS_T95810089_H
#define UNIFORMS_T95810089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms
struct  Uniforms_t95810089  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t95810089_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_Params
	int32_t ____Params_0;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_Speed
	int32_t ____Speed_1;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_ScaleOffsetRes
	int32_t ____ScaleOffsetRes_2;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_ExposureCompensation
	int32_t ____ExposureCompensation_3;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_AutoExposure
	int32_t ____AutoExposure_4;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent/Uniforms::_DebugWidth
	int32_t ____DebugWidth_5;

public:
	inline static int32_t get_offset_of__Params_0() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____Params_0)); }
	inline int32_t get__Params_0() const { return ____Params_0; }
	inline int32_t* get_address_of__Params_0() { return &____Params_0; }
	inline void set__Params_0(int32_t value)
	{
		____Params_0 = value;
	}

	inline static int32_t get_offset_of__Speed_1() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____Speed_1)); }
	inline int32_t get__Speed_1() const { return ____Speed_1; }
	inline int32_t* get_address_of__Speed_1() { return &____Speed_1; }
	inline void set__Speed_1(int32_t value)
	{
		____Speed_1 = value;
	}

	inline static int32_t get_offset_of__ScaleOffsetRes_2() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____ScaleOffsetRes_2)); }
	inline int32_t get__ScaleOffsetRes_2() const { return ____ScaleOffsetRes_2; }
	inline int32_t* get_address_of__ScaleOffsetRes_2() { return &____ScaleOffsetRes_2; }
	inline void set__ScaleOffsetRes_2(int32_t value)
	{
		____ScaleOffsetRes_2 = value;
	}

	inline static int32_t get_offset_of__ExposureCompensation_3() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____ExposureCompensation_3)); }
	inline int32_t get__ExposureCompensation_3() const { return ____ExposureCompensation_3; }
	inline int32_t* get_address_of__ExposureCompensation_3() { return &____ExposureCompensation_3; }
	inline void set__ExposureCompensation_3(int32_t value)
	{
		____ExposureCompensation_3 = value;
	}

	inline static int32_t get_offset_of__AutoExposure_4() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____AutoExposure_4)); }
	inline int32_t get__AutoExposure_4() const { return ____AutoExposure_4; }
	inline int32_t* get_address_of__AutoExposure_4() { return &____AutoExposure_4; }
	inline void set__AutoExposure_4(int32_t value)
	{
		____AutoExposure_4 = value;
	}

	inline static int32_t get_offset_of__DebugWidth_5() { return static_cast<int32_t>(offsetof(Uniforms_t95810089_StaticFields, ____DebugWidth_5)); }
	inline int32_t get__DebugWidth_5() const { return ____DebugWidth_5; }
	inline int32_t* get_address_of__DebugWidth_5() { return &____DebugWidth_5; }
	inline void set__DebugWidth_5(int32_t value)
	{
		____DebugWidth_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T95810089_H
#ifndef UNIFORMS_T459708260_H
#define UNIFORMS_T459708260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogComponent/Uniforms
struct  Uniforms_t459708260  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t459708260_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_FogColor
	int32_t ____FogColor_0;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_Density
	int32_t ____Density_1;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_Start
	int32_t ____Start_2;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_End
	int32_t ____End_3;
	// System.Int32 UnityEngine.PostProcessing.FogComponent/Uniforms::_TempRT
	int32_t ____TempRT_4;

public:
	inline static int32_t get_offset_of__FogColor_0() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____FogColor_0)); }
	inline int32_t get__FogColor_0() const { return ____FogColor_0; }
	inline int32_t* get_address_of__FogColor_0() { return &____FogColor_0; }
	inline void set__FogColor_0(int32_t value)
	{
		____FogColor_0 = value;
	}

	inline static int32_t get_offset_of__Density_1() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____Density_1)); }
	inline int32_t get__Density_1() const { return ____Density_1; }
	inline int32_t* get_address_of__Density_1() { return &____Density_1; }
	inline void set__Density_1(int32_t value)
	{
		____Density_1 = value;
	}

	inline static int32_t get_offset_of__Start_2() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____Start_2)); }
	inline int32_t get__Start_2() const { return ____Start_2; }
	inline int32_t* get_address_of__Start_2() { return &____Start_2; }
	inline void set__Start_2(int32_t value)
	{
		____Start_2 = value;
	}

	inline static int32_t get_offset_of__End_3() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____End_3)); }
	inline int32_t get__End_3() const { return ____End_3; }
	inline int32_t* get_address_of__End_3() { return &____End_3; }
	inline void set__End_3(int32_t value)
	{
		____End_3 = value;
	}

	inline static int32_t get_offset_of__TempRT_4() { return static_cast<int32_t>(offsetof(Uniforms_t459708260_StaticFields, ____TempRT_4)); }
	inline int32_t get__TempRT_4() const { return ____TempRT_4; }
	inline int32_t* get_address_of__TempRT_4() { return &____TempRT_4; }
	inline void set__TempRT_4(int32_t value)
	{
		____TempRT_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T459708260_H
#ifndef UNIFORMS_T1850622510_H
#define UNIFORMS_T1850622510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FxaaComponent/Uniforms
struct  Uniforms_t1850622510  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1850622510_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.FxaaComponent/Uniforms::_QualitySettings
	int32_t ____QualitySettings_0;
	// System.Int32 UnityEngine.PostProcessing.FxaaComponent/Uniforms::_ConsoleSettings
	int32_t ____ConsoleSettings_1;

public:
	inline static int32_t get_offset_of__QualitySettings_0() { return static_cast<int32_t>(offsetof(Uniforms_t1850622510_StaticFields, ____QualitySettings_0)); }
	inline int32_t get__QualitySettings_0() const { return ____QualitySettings_0; }
	inline int32_t* get_address_of__QualitySettings_0() { return &____QualitySettings_0; }
	inline void set__QualitySettings_0(int32_t value)
	{
		____QualitySettings_0 = value;
	}

	inline static int32_t get_offset_of__ConsoleSettings_1() { return static_cast<int32_t>(offsetof(Uniforms_t1850622510_StaticFields, ____ConsoleSettings_1)); }
	inline int32_t get__ConsoleSettings_1() const { return ____ConsoleSettings_1; }
	inline int32_t* get_address_of__ConsoleSettings_1() { return &____ConsoleSettings_1; }
	inline void set__ConsoleSettings_1(int32_t value)
	{
		____ConsoleSettings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1850622510_H
#ifndef UNIFORMS_T1442519687_H
#define UNIFORMS_T1442519687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainComponent/Uniforms
struct  Uniforms_t1442519687  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1442519687_StaticFields
{
public:
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Grain_Params1
	int32_t ____Grain_Params1_0;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Grain_Params2
	int32_t ____Grain_Params2_1;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_GrainTex
	int32_t ____GrainTex_2;
	// System.Int32 UnityEngine.PostProcessing.GrainComponent/Uniforms::_Phase
	int32_t ____Phase_3;

public:
	inline static int32_t get_offset_of__Grain_Params1_0() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Grain_Params1_0)); }
	inline int32_t get__Grain_Params1_0() const { return ____Grain_Params1_0; }
	inline int32_t* get_address_of__Grain_Params1_0() { return &____Grain_Params1_0; }
	inline void set__Grain_Params1_0(int32_t value)
	{
		____Grain_Params1_0 = value;
	}

	inline static int32_t get_offset_of__Grain_Params2_1() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Grain_Params2_1)); }
	inline int32_t get__Grain_Params2_1() const { return ____Grain_Params2_1; }
	inline int32_t* get_address_of__Grain_Params2_1() { return &____Grain_Params2_1; }
	inline void set__Grain_Params2_1(int32_t value)
	{
		____Grain_Params2_1 = value;
	}

	inline static int32_t get_offset_of__GrainTex_2() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____GrainTex_2)); }
	inline int32_t get__GrainTex_2() const { return ____GrainTex_2; }
	inline int32_t* get_address_of__GrainTex_2() { return &____GrainTex_2; }
	inline void set__GrainTex_2(int32_t value)
	{
		____GrainTex_2 = value;
	}

	inline static int32_t get_offset_of__Phase_3() { return static_cast<int32_t>(offsetof(Uniforms_t1442519687_StaticFields, ____Phase_3)); }
	inline int32_t get__Phase_3() const { return ____Phase_3; }
	inline int32_t* get_address_of__Phase_3() { return &____Phase_3; }
	inline void set__Phase_3(int32_t value)
	{
		____Phase_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1442519687_H
#ifndef POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#define POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentBase
struct  PostProcessingComponentBase_t2731103827  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingComponentBase::context
	PostProcessingContext_t2014408948 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(PostProcessingComponentBase_t2731103827, ___context_0)); }
	inline PostProcessingContext_t2014408948 * get_context_0() const { return ___context_0; }
	inline PostProcessingContext_t2014408948 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(PostProcessingContext_t2014408948 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifndef GVR_CLOCK_TIME_POINT_T2797008802_H
#define GVR_CLOCK_TIME_POINT_T2797008802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider/gvr_clock_time_point
struct  gvr_clock_time_point_t2797008802 
{
public:
	// System.Int64 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_clock_time_point::monotonic_system_time_nanos
	int64_t ___monotonic_system_time_nanos_0;

public:
	inline static int32_t get_offset_of_monotonic_system_time_nanos_0() { return static_cast<int32_t>(offsetof(gvr_clock_time_point_t2797008802, ___monotonic_system_time_nanos_0)); }
	inline int64_t get_monotonic_system_time_nanos_0() const { return ___monotonic_system_time_nanos_0; }
	inline int64_t* get_address_of_monotonic_system_time_nanos_0() { return &___monotonic_system_time_nanos_0; }
	inline void set_monotonic_system_time_nanos_0(int64_t value)
	{
		___monotonic_system_time_nanos_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_CLOCK_TIME_POINT_T2797008802_H
#ifndef GVR_RECTI_T2249612514_H
#define GVR_RECTI_T2249612514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti
struct  gvr_recti_t2249612514 
{
public:
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti::left
	int32_t ___left_0;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti::right
	int32_t ___right_1;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti::bottom
	int32_t ___bottom_2;
	// System.Int32 Gvr.Internal.AndroidNativeKeyboardProvider/gvr_recti::top
	int32_t ___top_3;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(gvr_recti_t2249612514, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(gvr_recti_t2249612514, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}

	inline static int32_t get_offset_of_bottom_2() { return static_cast<int32_t>(offsetof(gvr_recti_t2249612514, ___bottom_2)); }
	inline int32_t get_bottom_2() const { return ___bottom_2; }
	inline int32_t* get_address_of_bottom_2() { return &___bottom_2; }
	inline void set_bottom_2(int32_t value)
	{
		___bottom_2 = value;
	}

	inline static int32_t get_offset_of_top_3() { return static_cast<int32_t>(offsetof(gvr_recti_t2249612514, ___top_3)); }
	inline int32_t get_top_3() const { return ___top_3; }
	inline int32_t* get_address_of_top_3() { return &___top_3; }
	inline void set_top_3(int32_t value)
	{
		___top_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_RECTI_T2249612514_H
#ifndef RESOLUTIONSIZE_T3677657137_H
#define RESOLUTIONSIZE_T3677657137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/ResolutionSize
struct  ResolutionSize_t3677657137 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview/ResolutionSize::width
	int32_t ___width_0;
	// System.Int32 Gvr.Internal.InstantPreview/ResolutionSize::height
	int32_t ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(ResolutionSize_t3677657137, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(ResolutionSize_t3677657137, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTIONSIZE_T3677657137_H
#ifndef UNITYRECT_T2898233164_H
#define UNITYRECT_T2898233164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/UnityRect
struct  UnityRect_t2898233164 
{
public:
	// System.Single Gvr.Internal.InstantPreview/UnityRect::right
	float ___right_0;
	// System.Single Gvr.Internal.InstantPreview/UnityRect::left
	float ___left_1;
	// System.Single Gvr.Internal.InstantPreview/UnityRect::top
	float ___top_2;
	// System.Single Gvr.Internal.InstantPreview/UnityRect::bottom
	float ___bottom_3;

public:
	inline static int32_t get_offset_of_right_0() { return static_cast<int32_t>(offsetof(UnityRect_t2898233164, ___right_0)); }
	inline float get_right_0() const { return ___right_0; }
	inline float* get_address_of_right_0() { return &___right_0; }
	inline void set_right_0(float value)
	{
		___right_0 = value;
	}

	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(UnityRect_t2898233164, ___left_1)); }
	inline float get_left_1() const { return ___left_1; }
	inline float* get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(float value)
	{
		___left_1 = value;
	}

	inline static int32_t get_offset_of_top_2() { return static_cast<int32_t>(offsetof(UnityRect_t2898233164, ___top_2)); }
	inline float get_top_2() const { return ___top_2; }
	inline float* get_address_of_top_2() { return &___top_2; }
	inline void set_top_2(float value)
	{
		___top_2 = value;
	}

	inline static int32_t get_offset_of_bottom_3() { return static_cast<int32_t>(offsetof(UnityRect_t2898233164, ___bottom_3)); }
	inline float get_bottom_3() const { return ___bottom_3; }
	inline float* get_address_of_bottom_3() { return &___bottom_3; }
	inline void set_bottom_3(float value)
	{
		___bottom_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYRECT_T2898233164_H
#ifndef KEYBOARDCALLBACK_T4011255843_H
#define KEYBOARDCALLBACK_T4011255843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardIntent/KeyboardCallback
struct  KeyboardCallback_t4011255843  : public AndroidJavaProxy_t2835824643
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDCALLBACK_T4011255843_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3977614564_H
#define POSTPROCESSINGCOMPONENT_1_T3977614564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponent_1_t3977614564  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AmbientOcclusionModel_t389471066 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3977614564, ___U3CmodelU3Ek__BackingField_1)); }
	inline AmbientOcclusionModel_t389471066 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AmbientOcclusionModel_t389471066 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AmbientOcclusionModel_t389471066 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3977614564_H
#ifndef POSTPROCESSINGCOMPONENT_1_T814315590_H
#define POSTPROCESSINGCOMPONENT_1_T814315590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.AntialiasingModel>
struct  PostProcessingComponent_1_t814315590  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	AntialiasingModel_t1521139388 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t814315590, ___U3CmodelU3Ek__BackingField_1)); }
	inline AntialiasingModel_t1521139388 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline AntialiasingModel_t1521139388 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(AntialiasingModel_t1521139388 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T814315590_H
#ifndef POSTPROCESSINGCOMPONENT_1_T1392904062_H
#define POSTPROCESSINGCOMPONENT_1_T1392904062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponent_1_t1392904062  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BloomModel_t2099727860 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t1392904062, ___U3CmodelU3Ek__BackingField_1)); }
	inline BloomModel_t2099727860 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BloomModel_t2099727860 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BloomModel_t2099727860 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T1392904062_H
#ifndef POSTPROCESSINGCOMPONENT_1_T755795042_H
#define POSTPROCESSINGCOMPONENT_1_T755795042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct  PostProcessingComponent_1_t755795042  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	BuiltinDebugViewsModel_t1462618840 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t755795042, ___U3CmodelU3Ek__BackingField_1)); }
	inline BuiltinDebugViewsModel_t1462618840 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline BuiltinDebugViewsModel_t1462618840 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(BuiltinDebugViewsModel_t1462618840 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T755795042_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3256576055_H
#define POSTPROCESSINGCOMPONENT_1_T3256576055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct  PostProcessingComponent_1_t3256576055  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ChromaticAberrationModel_t3963399853 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3256576055, ___U3CmodelU3Ek__BackingField_1)); }
	inline ChromaticAberrationModel_t3963399853 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ChromaticAberrationModel_t3963399853 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ChromaticAberrationModel_t3963399853 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3256576055_H
#ifndef POSTPROCESSINGCOMPONENT_1_T741224383_H
#define POSTPROCESSINGCOMPONENT_1_T741224383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.ColorGradingModel>
struct  PostProcessingComponent_1_t741224383  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	ColorGradingModel_t1448048181 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t741224383, ___U3CmodelU3Ek__BackingField_1)); }
	inline ColorGradingModel_t1448048181 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline ColorGradingModel_t1448048181 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(ColorGradingModel_t1448048181 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T741224383_H
#ifndef POSTPROCESSINGCOMPONENT_1_T4102210828_H
#define POSTPROCESSINGCOMPONENT_1_T4102210828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DepthOfFieldModel>
struct  PostProcessingComponent_1_t4102210828  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	DepthOfFieldModel_t514067330 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t4102210828, ___U3CmodelU3Ek__BackingField_1)); }
	inline DepthOfFieldModel_t514067330 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline DepthOfFieldModel_t514067330 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(DepthOfFieldModel_t514067330 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T4102210828_H
#ifndef POSTPROCESSINGCOMPONENT_1_T1722181598_H
#define POSTPROCESSINGCOMPONENT_1_T1722181598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.DitheringModel>
struct  PostProcessingComponent_1_t1722181598  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	DitheringModel_t2429005396 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t1722181598, ___U3CmodelU3Ek__BackingField_1)); }
	inline DitheringModel_t2429005396 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline DitheringModel_t2429005396 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(DitheringModel_t2429005396 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T1722181598_H
#ifndef POSTPROCESSINGCOMPONENT_1_T3830967410_H
#define POSTPROCESSINGCOMPONENT_1_T3830967410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.EyeAdaptationModel>
struct  PostProcessingComponent_1_t3830967410  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	EyeAdaptationModel_t242823912 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t3830967410, ___U3CmodelU3Ek__BackingField_1)); }
	inline EyeAdaptationModel_t242823912 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline EyeAdaptationModel_t242823912 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(EyeAdaptationModel_t242823912 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T3830967410_H
#ifndef POSTPROCESSINGCOMPONENT_1_T2913864951_H
#define POSTPROCESSINGCOMPONENT_1_T2913864951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.FogModel>
struct  PostProcessingComponent_1_t2913864951  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	FogModel_t3620688749 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t2913864951, ___U3CmodelU3Ek__BackingField_1)); }
	inline FogModel_t3620688749 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline FogModel_t3620688749 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(FogModel_t3620688749 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T2913864951_H
#ifndef POSTPROCESSINGCOMPONENT_1_T446058690_H
#define POSTPROCESSINGCOMPONENT_1_T446058690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponent`1<UnityEngine.PostProcessing.GrainModel>
struct  PostProcessingComponent_1_t446058690  : public PostProcessingComponentBase_t2731103827
{
public:
	// T UnityEngine.PostProcessing.PostProcessingComponent`1::<model>k__BackingField
	GrainModel_t1152882488 * ___U3CmodelU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CmodelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PostProcessingComponent_1_t446058690, ___U3CmodelU3Ek__BackingField_1)); }
	inline GrainModel_t1152882488 * get_U3CmodelU3Ek__BackingField_1() const { return ___U3CmodelU3Ek__BackingField_1; }
	inline GrainModel_t1152882488 ** get_address_of_U3CmodelU3Ek__BackingField_1() { return &___U3CmodelU3Ek__BackingField_1; }
	inline void set_U3CmodelU3Ek__BackingField_1(GrainModel_t1152882488 * value)
	{
		___U3CmodelU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENT_1_T446058690_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef SPRITESTATE_T1362986479_H
#define SPRITESTATE_T1362986479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1362986479 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t280657092 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t280657092 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_HighlightedSprite_0)); }
	inline Sprite_t280657092 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t280657092 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t280657092 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_PressedSprite_1)); }
	inline Sprite_t280657092 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t280657092 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t280657092 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1362986479, ___m_DisabledSprite_2)); }
	inline Sprite_t280657092 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t280657092 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t280657092 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_pinvoke
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1362986479_marshaled_com
{
	Sprite_t280657092 * ___m_HighlightedSprite_0;
	Sprite_t280657092 * ___m_PressedSprite_1;
	Sprite_t280657092 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1362986479_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ROTATIONAXES_T471521160_H
#define ROTATIONAXES_T471521160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoScript/RotationAxes
struct  RotationAxes_t471521160 
{
public:
	// System.Int32 DemoScript/RotationAxes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotationAxes_t471521160, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONAXES_T471521160_H
#ifndef BITRATES_T2681405699_H
#define BITRATES_T2681405699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/BitRates
struct  BitRates_t2681405699 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview/BitRates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BitRates_t2681405699, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITRATES_T2681405699_H
#ifndef MULTISAMPLECOUNTS_T552109702_H
#define MULTISAMPLECOUNTS_T552109702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/MultisampleCounts
struct  MultisampleCounts_t552109702 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview/MultisampleCounts::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MultisampleCounts_t552109702, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTISAMPLECOUNTS_T552109702_H
#ifndef RESOLUTIONS_T3321708501_H
#define RESOLUTIONS_T3321708501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/Resolutions
struct  Resolutions_t3321708501 
{
public:
	// System.Int32 Gvr.Internal.InstantPreview/Resolutions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Resolutions_t3321708501, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLUTIONS_T3321708501_H
#ifndef UNITYEYEVIEWS_T678228735_H
#define UNITYEYEVIEWS_T678228735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview/UnityEyeViews
struct  UnityEyeViews_t678228735 
{
public:
	// UnityEngine.Matrix4x4 Gvr.Internal.InstantPreview/UnityEyeViews::leftEyePose
	Matrix4x4_t1817901843  ___leftEyePose_0;
	// UnityEngine.Matrix4x4 Gvr.Internal.InstantPreview/UnityEyeViews::rightEyePose
	Matrix4x4_t1817901843  ___rightEyePose_1;
	// Gvr.Internal.InstantPreview/UnityRect Gvr.Internal.InstantPreview/UnityEyeViews::leftEyeViewSize
	UnityRect_t2898233164  ___leftEyeViewSize_2;
	// Gvr.Internal.InstantPreview/UnityRect Gvr.Internal.InstantPreview/UnityEyeViews::rightEyeViewSize
	UnityRect_t2898233164  ___rightEyeViewSize_3;

public:
	inline static int32_t get_offset_of_leftEyePose_0() { return static_cast<int32_t>(offsetof(UnityEyeViews_t678228735, ___leftEyePose_0)); }
	inline Matrix4x4_t1817901843  get_leftEyePose_0() const { return ___leftEyePose_0; }
	inline Matrix4x4_t1817901843 * get_address_of_leftEyePose_0() { return &___leftEyePose_0; }
	inline void set_leftEyePose_0(Matrix4x4_t1817901843  value)
	{
		___leftEyePose_0 = value;
	}

	inline static int32_t get_offset_of_rightEyePose_1() { return static_cast<int32_t>(offsetof(UnityEyeViews_t678228735, ___rightEyePose_1)); }
	inline Matrix4x4_t1817901843  get_rightEyePose_1() const { return ___rightEyePose_1; }
	inline Matrix4x4_t1817901843 * get_address_of_rightEyePose_1() { return &___rightEyePose_1; }
	inline void set_rightEyePose_1(Matrix4x4_t1817901843  value)
	{
		___rightEyePose_1 = value;
	}

	inline static int32_t get_offset_of_leftEyeViewSize_2() { return static_cast<int32_t>(offsetof(UnityEyeViews_t678228735, ___leftEyeViewSize_2)); }
	inline UnityRect_t2898233164  get_leftEyeViewSize_2() const { return ___leftEyeViewSize_2; }
	inline UnityRect_t2898233164 * get_address_of_leftEyeViewSize_2() { return &___leftEyeViewSize_2; }
	inline void set_leftEyeViewSize_2(UnityRect_t2898233164  value)
	{
		___leftEyeViewSize_2 = value;
	}

	inline static int32_t get_offset_of_rightEyeViewSize_3() { return static_cast<int32_t>(offsetof(UnityEyeViews_t678228735, ___rightEyeViewSize_3)); }
	inline UnityRect_t2898233164  get_rightEyeViewSize_3() const { return ___rightEyeViewSize_3; }
	inline UnityRect_t2898233164 * get_address_of_rightEyeViewSize_3() { return &___rightEyeViewSize_3; }
	inline void set_rightEyeViewSize_3(UnityRect_t2898233164  value)
	{
		___rightEyeViewSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEYEVIEWS_T678228735_H
#ifndef GVR_FEATURE_T1054564930_H
#define GVR_FEATURE_T1054564930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_feature
struct  gvr_feature_t1054564930 
{
public:
	// System.Int32 Gvr.Internal.gvr_feature::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(gvr_feature_t1054564930, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_FEATURE_T1054564930_H
#ifndef GVR_PROPERTY_TYPE_T1812416635_H
#define GVR_PROPERTY_TYPE_T1812416635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_property_type
struct  gvr_property_type_t1812416635 
{
public:
	// System.Int32 Gvr.Internal.gvr_property_type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(gvr_property_type_t1812416635, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_PROPERTY_TYPE_T1812416635_H
#ifndef GVR_RECENTER_FLAGS_T2414511954_H
#define GVR_RECENTER_FLAGS_T2414511954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_recenter_flags
struct  gvr_recenter_flags_t2414511954 
{
public:
	// System.Int32 Gvr.Internal.gvr_recenter_flags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(gvr_recenter_flags_t2414511954, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_RECENTER_FLAGS_T2414511954_H
#ifndef GVR_VALUE_TYPE_T2156477760_H
#define GVR_VALUE_TYPE_T2156477760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.gvr_value_type
struct  gvr_value_type_t2156477760 
{
public:
	// System.Int32 Gvr.Internal.gvr_value_type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(gvr_value_type_t2156477760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVR_VALUE_TYPE_T2156477760_H
#ifndef GVRERRORTYPE_T1921600730_H
#define GVRERRORTYPE_T1921600730_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrErrorType
struct  GvrErrorType_t1921600730 
{
public:
	// System.Int32 GvrErrorType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrErrorType_t1921600730, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRERRORTYPE_T1921600730_H
#ifndef GVREVENTTYPE_T1628138291_H
#define GVREVENTTYPE_T1628138291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrEventType
struct  GvrEventType_t1628138291 
{
public:
	// System.Int32 GvrEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrEventType_t1628138291, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVREVENTTYPE_T1628138291_H
#ifndef GVRINFO_T2187998870_H
#define GVRINFO_T2187998870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrInfo
struct  GvrInfo_t2187998870  : public PropertyAttribute_t3677895545
{
public:
	// System.String GvrInfo::text
	String_t* ___text_0;
	// System.Int32 GvrInfo::numLines
	int32_t ___numLines_1;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(GvrInfo_t2187998870, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}

	inline static int32_t get_offset_of_numLines_1() { return static_cast<int32_t>(offsetof(GvrInfo_t2187998870, ___numLines_1)); }
	inline int32_t get_numLines_1() const { return ___numLines_1; }
	inline int32_t* get_address_of_numLines_1() { return &___numLines_1; }
	inline void set_numLines_1(int32_t value)
	{
		___numLines_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRINFO_T2187998870_H
#ifndef GVRKEYBOARDERROR_T3210682397_H
#define GVRKEYBOARDERROR_T3210682397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardError
struct  GvrKeyboardError_t3210682397 
{
public:
	// System.Int32 GvrKeyboardError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrKeyboardError_t3210682397, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDERROR_T3210682397_H
#ifndef GVRKEYBOARDEVENT_T3629165438_H
#define GVRKEYBOARDEVENT_T3629165438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardEvent
struct  GvrKeyboardEvent_t3629165438 
{
public:
	// System.Int32 GvrKeyboardEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrKeyboardEvent_t3629165438, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDEVENT_T3629165438_H
#ifndef GVRKEYBOARDINPUTMODE_T518947509_H
#define GVRKEYBOARDINPUTMODE_T518947509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardInputMode
struct  GvrKeyboardInputMode_t518947509 
{
public:
	// System.Int32 GvrKeyboardInputMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrKeyboardInputMode_t518947509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDINPUTMODE_T518947509_H
#ifndef GVRRECENTEREVENTTYPE_T513699134_H
#define GVRRECENTEREVENTTYPE_T513699134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterEventType
struct  GvrRecenterEventType_t513699134 
{
public:
	// System.Int32 GvrRecenterEventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrRecenterEventType_t513699134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTEREVENTTYPE_T513699134_H
#ifndef GVRRECENTERFLAGS_T370890970_H
#define GVRRECENTERFLAGS_T370890970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrRecenterFlags
struct  GvrRecenterFlags_t370890970 
{
public:
	// System.Int32 GvrRecenterFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrRecenterFlags_t370890970, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRRECENTERFLAGS_T370890970_H
#ifndef GVRSAFETYREGIONTYPE_T1943469016_H
#define GVRSAFETYREGIONTYPE_T1943469016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrSafetyRegionType
struct  GvrSafetyRegionType_t1943469016 
{
public:
	// System.Int32 GvrSafetyRegionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(GvrSafetyRegionType_t1943469016, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRSAFETYREGIONTYPE_T1943469016_H
#ifndef RENDERCOMMAND_T1121160834_H
#define RENDERCOMMAND_T1121160834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/RenderCommand
struct  RenderCommand_t1121160834 
{
public:
	// System.Int32 GvrVideoPlayerTexture/RenderCommand::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderCommand_t1121160834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERCOMMAND_T1121160834_H
#ifndef STEREOMODE_T1039127149_H
#define STEREOMODE_T1039127149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/StereoMode
struct  StereoMode_t1039127149 
{
public:
	// System.Int32 GvrVideoPlayerTexture/StereoMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StereoMode_t1039127149, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOMODE_T1039127149_H
#ifndef VIDEOEVENTS_T3555787859_H
#define VIDEOEVENTS_T3555787859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/VideoEvents
struct  VideoEvents_t3555787859 
{
public:
	// System.Int32 GvrVideoPlayerTexture/VideoEvents::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoEvents_t3555787859, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOEVENTS_T3555787859_H
#ifndef VIDEOPLAYERSTATE_T3323603301_H
#define VIDEOPLAYERSTATE_T3323603301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/VideoPlayerState
struct  VideoPlayerState_t3323603301 
{
public:
	// System.Int32 GvrVideoPlayerTexture/VideoPlayerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoPlayerState_t3323603301, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYERSTATE_T3323603301_H
#ifndef VIDEORESOLUTION_T1062057780_H
#define VIDEORESOLUTION_T1062057780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/VideoResolution
struct  VideoResolution_t1062057780 
{
public:
	// System.Int32 GvrVideoPlayerTexture/VideoResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoResolution_t1062057780, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEORESOLUTION_T1062057780_H
#ifndef VIDEOTYPE_T2491562340_H
#define VIDEOTYPE_T2491562340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/VideoType
struct  VideoType_t2491562340 
{
public:
	// System.Int32 GvrVideoPlayerTexture/VideoType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoType_t2491562340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTYPE_T2491562340_H
#ifndef POSE3D_T2649470188_H
#define POSE3D_T2649470188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pose3D
struct  Pose3D_t2649470188  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Pose3D::<Position>k__BackingField
	Vector3_t3722313464  ___U3CPositionU3Ek__BackingField_1;
	// UnityEngine.Quaternion Pose3D::<Orientation>k__BackingField
	Quaternion_t2301928331  ___U3COrientationU3Ek__BackingField_2;
	// UnityEngine.Matrix4x4 Pose3D::<Matrix>k__BackingField
	Matrix4x4_t1817901843  ___U3CMatrixU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3CPositionU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CPositionU3Ek__BackingField_1() const { return ___U3CPositionU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CPositionU3Ek__BackingField_1() { return &___U3CPositionU3Ek__BackingField_1; }
	inline void set_U3CPositionU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CPositionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COrientationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3COrientationU3Ek__BackingField_2)); }
	inline Quaternion_t2301928331  get_U3COrientationU3Ek__BackingField_2() const { return ___U3COrientationU3Ek__BackingField_2; }
	inline Quaternion_t2301928331 * get_address_of_U3COrientationU3Ek__BackingField_2() { return &___U3COrientationU3Ek__BackingField_2; }
	inline void set_U3COrientationU3Ek__BackingField_2(Quaternion_t2301928331  value)
	{
		___U3COrientationU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMatrixU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3CMatrixU3Ek__BackingField_3)); }
	inline Matrix4x4_t1817901843  get_U3CMatrixU3Ek__BackingField_3() const { return ___U3CMatrixU3Ek__BackingField_3; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CMatrixU3Ek__BackingField_3() { return &___U3CMatrixU3Ek__BackingField_3; }
	inline void set_U3CMatrixU3Ek__BackingField_3(Matrix4x4_t1817901843  value)
	{
		___U3CMatrixU3Ek__BackingField_3 = value;
	}
};

struct Pose3D_t2649470188_StaticFields
{
public:
	// UnityEngine.Matrix4x4 Pose3D::FLIP_Z
	Matrix4x4_t1817901843  ___FLIP_Z_0;

public:
	inline static int32_t get_offset_of_FLIP_Z_0() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188_StaticFields, ___FLIP_Z_0)); }
	inline Matrix4x4_t1817901843  get_FLIP_Z_0() const { return ___FLIP_Z_0; }
	inline Matrix4x4_t1817901843 * get_address_of_FLIP_Z_0() { return &___FLIP_Z_0; }
	inline void set_FLIP_Z_0(Matrix4x4_t1817901843  value)
	{
		___FLIP_Z_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE3D_T2649470188_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef OCCLUSIONSOURCE_T4221238007_H
#define OCCLUSIONSOURCE_T4221238007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource
struct  OcclusionSource_t4221238007 
{
public:
	// System.Int32 UnityEngine.PostProcessing.AmbientOcclusionComponent/OcclusionSource::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OcclusionSource_t4221238007, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OCCLUSIONSOURCE_T4221238007_H
#ifndef PASS_T2117482_H
#define PASS_T2117482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Pass
struct  Pass_t2117482 
{
public:
	// System.Int32 UnityEngine.PostProcessing.BuiltinDebugViewsComponent/Pass::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Pass_t2117482, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASS_T2117482_H
#ifndef GETSETATTRIBUTE_T1349027187_H
#define GETSETATTRIBUTE_T1349027187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GetSetAttribute
struct  GetSetAttribute_t1349027187  : public PropertyAttribute_t3677895545
{
public:
	// System.String UnityEngine.PostProcessing.GetSetAttribute::name
	String_t* ___name_0;
	// System.Boolean UnityEngine.PostProcessing.GetSetAttribute::dirty
	bool ___dirty_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(GetSetAttribute_t1349027187, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_dirty_1() { return static_cast<int32_t>(offsetof(GetSetAttribute_t1349027187, ___dirty_1)); }
	inline bool get_dirty_1() const { return ___dirty_1; }
	inline bool* get_address_of_dirty_1() { return &___dirty_1; }
	inline void set_dirty_1(bool value)
	{
		___dirty_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETSETATTRIBUTE_T1349027187_H
#ifndef MINATTRIBUTE_T4172004135_H
#define MINATTRIBUTE_T4172004135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MinAttribute
struct  MinAttribute_t4172004135  : public PropertyAttribute_t3677895545
{
public:
	// System.Single UnityEngine.PostProcessing.MinAttribute::min
	float ___min_0;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(MinAttribute_t4172004135, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINATTRIBUTE_T4172004135_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.AmbientOcclusionModel>
struct  PostProcessingComponentCommandBuffer_1_t1664772955  : public PostProcessingComponent_1_t3977614564
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T1664772955_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.BuiltinDebugViewsModel>
struct  PostProcessingComponentCommandBuffer_1_t2737920729  : public PostProcessingComponent_1_t755795042
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T2737920729_H
#ifndef POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#define POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentCommandBuffer`1<UnityEngine.PostProcessing.FogModel>
struct  PostProcessingComponentCommandBuffer_1_t601023342  : public PostProcessingComponent_1_t2913864951
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTCOMMANDBUFFER_1_T601023342_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.AntialiasingModel>
struct  PostProcessingComponentRenderTexture_1_t3089424429  : public PostProcessingComponent_1_t814315590
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3089424429_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.BloomModel>
struct  PostProcessingComponentRenderTexture_1_t3668012901  : public PostProcessingComponent_1_t1392904062
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3668012901_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ChromaticAberrationModel>
struct  PostProcessingComponentRenderTexture_1_t1236717598  : public PostProcessingComponent_1_t3256576055
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1236717598_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.ColorGradingModel>
struct  PostProcessingComponentRenderTexture_1_t3016333222  : public PostProcessingComponent_1_t741224383
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3016333222_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DepthOfFieldModel>
struct  PostProcessingComponentRenderTexture_1_t2082352371  : public PostProcessingComponent_1_t4102210828
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2082352371_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.DitheringModel>
struct  PostProcessingComponentRenderTexture_1_t3997290437  : public PostProcessingComponent_1_t1722181598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T3997290437_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.EyeAdaptationModel>
struct  PostProcessingComponentRenderTexture_1_t1811108953  : public PostProcessingComponent_1_t3830967410
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T1811108953_H
#ifndef POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#define POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentRenderTexture`1<UnityEngine.PostProcessing.GrainModel>
struct  PostProcessingComponentRenderTexture_1_t2721167529  : public PostProcessingComponent_1_t446058690
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTRENDERTEXTURE_1_T2721167529_H
#ifndef TRACKBALLATTRIBUTE_T219960417_H
#define TRACKBALLATTRIBUTE_T219960417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballAttribute
struct  TrackballAttribute_t219960417  : public PropertyAttribute_t3677895545
{
public:
	// System.String UnityEngine.PostProcessing.TrackballAttribute::method
	String_t* ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(TrackballAttribute_t219960417, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLATTRIBUTE_T219960417_H
#ifndef TRACKBALLGROUPATTRIBUTE_T624107828_H
#define TRACKBALLGROUPATTRIBUTE_T624107828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.TrackballGroupAttribute
struct  TrackballGroupAttribute_t624107828  : public PropertyAttribute_t3677895545
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKBALLGROUPATTRIBUTE_T624107828_H
#ifndef COLORBLOCK_T2139031574_H
#define COLORBLOCK_T2139031574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2139031574 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2555686324  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2555686324  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2555686324  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2555686324  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_NormalColor_0)); }
	inline Color_t2555686324  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2555686324 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2555686324  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_HighlightedColor_1)); }
	inline Color_t2555686324  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2555686324 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2555686324  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_PressedColor_2)); }
	inline Color_t2555686324  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2555686324 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2555686324  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_DisabledColor_3)); }
	inline Color_t2555686324  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2555686324 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2555686324  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2139031574, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2139031574_H
#ifndef MODE_T1066900953_H
#define MODE_T1066900953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1066900953 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1066900953, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1066900953_H
#ifndef SELECTIONSTATE_T2656606514_H
#define SELECTIONSTATE_T2656606514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t2656606514 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t2656606514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T2656606514_H
#ifndef TRANSITION_T1769908631_H
#define TRANSITION_T1769908631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t1769908631 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t1769908631, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T1769908631_H
#ifndef ANDROIDNATIVEKEYBOARDPROVIDER_T4081466012_H
#define ANDROIDNATIVEKEYBOARDPROVIDER_T4081466012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.AndroidNativeKeyboardProvider
struct  AndroidNativeKeyboardProvider_t4081466012  : public RuntimeObject
{
public:
	// System.IntPtr Gvr.Internal.AndroidNativeKeyboardProvider::renderEventFunction
	intptr_t ___renderEventFunction_0;
	// System.IntPtr Gvr.Internal.AndroidNativeKeyboardProvider::keyboard_context
	intptr_t ___keyboard_context_9;
	// GvrKeyboardInputMode Gvr.Internal.AndroidNativeKeyboardProvider::mode
	int32_t ___mode_16;
	// System.String Gvr.Internal.AndroidNativeKeyboardProvider::editorText
	String_t* ___editorText_17;
	// UnityEngine.Matrix4x4 Gvr.Internal.AndroidNativeKeyboardProvider::worldMatrix
	Matrix4x4_t1817901843  ___worldMatrix_18;
	// System.Boolean Gvr.Internal.AndroidNativeKeyboardProvider::isValid
	bool ___isValid_19;
	// System.Boolean Gvr.Internal.AndroidNativeKeyboardProvider::isReady
	bool ___isReady_20;

public:
	inline static int32_t get_offset_of_renderEventFunction_0() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___renderEventFunction_0)); }
	inline intptr_t get_renderEventFunction_0() const { return ___renderEventFunction_0; }
	inline intptr_t* get_address_of_renderEventFunction_0() { return &___renderEventFunction_0; }
	inline void set_renderEventFunction_0(intptr_t value)
	{
		___renderEventFunction_0 = value;
	}

	inline static int32_t get_offset_of_keyboard_context_9() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___keyboard_context_9)); }
	inline intptr_t get_keyboard_context_9() const { return ___keyboard_context_9; }
	inline intptr_t* get_address_of_keyboard_context_9() { return &___keyboard_context_9; }
	inline void set_keyboard_context_9(intptr_t value)
	{
		___keyboard_context_9 = value;
	}

	inline static int32_t get_offset_of_mode_16() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___mode_16)); }
	inline int32_t get_mode_16() const { return ___mode_16; }
	inline int32_t* get_address_of_mode_16() { return &___mode_16; }
	inline void set_mode_16(int32_t value)
	{
		___mode_16 = value;
	}

	inline static int32_t get_offset_of_editorText_17() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___editorText_17)); }
	inline String_t* get_editorText_17() const { return ___editorText_17; }
	inline String_t** get_address_of_editorText_17() { return &___editorText_17; }
	inline void set_editorText_17(String_t* value)
	{
		___editorText_17 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_17), value);
	}

	inline static int32_t get_offset_of_worldMatrix_18() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___worldMatrix_18)); }
	inline Matrix4x4_t1817901843  get_worldMatrix_18() const { return ___worldMatrix_18; }
	inline Matrix4x4_t1817901843 * get_address_of_worldMatrix_18() { return &___worldMatrix_18; }
	inline void set_worldMatrix_18(Matrix4x4_t1817901843  value)
	{
		___worldMatrix_18 = value;
	}

	inline static int32_t get_offset_of_isValid_19() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___isValid_19)); }
	inline bool get_isValid_19() const { return ___isValid_19; }
	inline bool* get_address_of_isValid_19() { return &___isValid_19; }
	inline void set_isValid_19(bool value)
	{
		___isValid_19 = value;
	}

	inline static int32_t get_offset_of_isReady_20() { return static_cast<int32_t>(offsetof(AndroidNativeKeyboardProvider_t4081466012, ___isReady_20)); }
	inline bool get_isReady_20() const { return ___isReady_20; }
	inline bool* get_address_of_isReady_20() { return &___isReady_20; }
	inline void set_isReady_20(bool value)
	{
		___isReady_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDNATIVEKEYBOARDPROVIDER_T4081466012_H
#ifndef EMULATORKEYBOARDPROVIDER_T2389719130_H
#define EMULATORKEYBOARDPROVIDER_T2389719130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EmulatorKeyboardProvider
struct  EmulatorKeyboardProvider_t2389719130  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Gvr.Internal.EmulatorKeyboardProvider::stub
	GameObject_t1113636619 * ___stub_0;
	// System.Boolean Gvr.Internal.EmulatorKeyboardProvider::showing
	bool ___showing_1;
	// GvrKeyboard/KeyboardCallback Gvr.Internal.EmulatorKeyboardProvider::keyboardCallback
	KeyboardCallback_t3330588312 * ___keyboardCallback_2;
	// System.String Gvr.Internal.EmulatorKeyboardProvider::editorText
	String_t* ___editorText_3;
	// GvrKeyboardInputMode Gvr.Internal.EmulatorKeyboardProvider::mode
	int32_t ___mode_4;
	// UnityEngine.Matrix4x4 Gvr.Internal.EmulatorKeyboardProvider::worldMatrix
	Matrix4x4_t1817901843  ___worldMatrix_5;
	// System.Boolean Gvr.Internal.EmulatorKeyboardProvider::isValid
	bool ___isValid_6;

public:
	inline static int32_t get_offset_of_stub_0() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___stub_0)); }
	inline GameObject_t1113636619 * get_stub_0() const { return ___stub_0; }
	inline GameObject_t1113636619 ** get_address_of_stub_0() { return &___stub_0; }
	inline void set_stub_0(GameObject_t1113636619 * value)
	{
		___stub_0 = value;
		Il2CppCodeGenWriteBarrier((&___stub_0), value);
	}

	inline static int32_t get_offset_of_showing_1() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___showing_1)); }
	inline bool get_showing_1() const { return ___showing_1; }
	inline bool* get_address_of_showing_1() { return &___showing_1; }
	inline void set_showing_1(bool value)
	{
		___showing_1 = value;
	}

	inline static int32_t get_offset_of_keyboardCallback_2() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___keyboardCallback_2)); }
	inline KeyboardCallback_t3330588312 * get_keyboardCallback_2() const { return ___keyboardCallback_2; }
	inline KeyboardCallback_t3330588312 ** get_address_of_keyboardCallback_2() { return &___keyboardCallback_2; }
	inline void set_keyboardCallback_2(KeyboardCallback_t3330588312 * value)
	{
		___keyboardCallback_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardCallback_2), value);
	}

	inline static int32_t get_offset_of_editorText_3() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___editorText_3)); }
	inline String_t* get_editorText_3() const { return ___editorText_3; }
	inline String_t** get_address_of_editorText_3() { return &___editorText_3; }
	inline void set_editorText_3(String_t* value)
	{
		___editorText_3 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_3), value);
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_worldMatrix_5() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___worldMatrix_5)); }
	inline Matrix4x4_t1817901843  get_worldMatrix_5() const { return ___worldMatrix_5; }
	inline Matrix4x4_t1817901843 * get_address_of_worldMatrix_5() { return &___worldMatrix_5; }
	inline void set_worldMatrix_5(Matrix4x4_t1817901843  value)
	{
		___worldMatrix_5 = value;
	}

	inline static int32_t get_offset_of_isValid_6() { return static_cast<int32_t>(offsetof(EmulatorKeyboardProvider_t2389719130, ___isValid_6)); }
	inline bool get_isValid_6() const { return ___isValid_6; }
	inline bool* get_address_of_isValid_6() { return &___isValid_6; }
	inline void set_isValid_6(bool value)
	{
		___isValid_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMULATORKEYBOARDPROVIDER_T2389719130_H
#ifndef HEADSETSTATE_T378905490_H
#define HEADSETSTATE_T378905490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.HeadsetState
struct  HeadsetState_t378905490 
{
public:
	// GvrEventType Gvr.Internal.HeadsetState::eventType
	int32_t ___eventType_0;
	// System.Int32 Gvr.Internal.HeadsetState::eventFlags
	int32_t ___eventFlags_1;
	// System.Int64 Gvr.Internal.HeadsetState::eventTimestampNs
	int64_t ___eventTimestampNs_2;
	// GvrRecenterEventType Gvr.Internal.HeadsetState::recenterEventType
	int32_t ___recenterEventType_3;
	// System.UInt32 Gvr.Internal.HeadsetState::recenterEventFlags
	uint32_t ___recenterEventFlags_4;
	// UnityEngine.Vector3 Gvr.Internal.HeadsetState::recenteredPosition
	Vector3_t3722313464  ___recenteredPosition_5;
	// UnityEngine.Quaternion Gvr.Internal.HeadsetState::recenteredRotation
	Quaternion_t2301928331  ___recenteredRotation_6;

public:
	inline static int32_t get_offset_of_eventType_0() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventType_0)); }
	inline int32_t get_eventType_0() const { return ___eventType_0; }
	inline int32_t* get_address_of_eventType_0() { return &___eventType_0; }
	inline void set_eventType_0(int32_t value)
	{
		___eventType_0 = value;
	}

	inline static int32_t get_offset_of_eventFlags_1() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventFlags_1)); }
	inline int32_t get_eventFlags_1() const { return ___eventFlags_1; }
	inline int32_t* get_address_of_eventFlags_1() { return &___eventFlags_1; }
	inline void set_eventFlags_1(int32_t value)
	{
		___eventFlags_1 = value;
	}

	inline static int32_t get_offset_of_eventTimestampNs_2() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___eventTimestampNs_2)); }
	inline int64_t get_eventTimestampNs_2() const { return ___eventTimestampNs_2; }
	inline int64_t* get_address_of_eventTimestampNs_2() { return &___eventTimestampNs_2; }
	inline void set_eventTimestampNs_2(int64_t value)
	{
		___eventTimestampNs_2 = value;
	}

	inline static int32_t get_offset_of_recenterEventType_3() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenterEventType_3)); }
	inline int32_t get_recenterEventType_3() const { return ___recenterEventType_3; }
	inline int32_t* get_address_of_recenterEventType_3() { return &___recenterEventType_3; }
	inline void set_recenterEventType_3(int32_t value)
	{
		___recenterEventType_3 = value;
	}

	inline static int32_t get_offset_of_recenterEventFlags_4() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenterEventFlags_4)); }
	inline uint32_t get_recenterEventFlags_4() const { return ___recenterEventFlags_4; }
	inline uint32_t* get_address_of_recenterEventFlags_4() { return &___recenterEventFlags_4; }
	inline void set_recenterEventFlags_4(uint32_t value)
	{
		___recenterEventFlags_4 = value;
	}

	inline static int32_t get_offset_of_recenteredPosition_5() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenteredPosition_5)); }
	inline Vector3_t3722313464  get_recenteredPosition_5() const { return ___recenteredPosition_5; }
	inline Vector3_t3722313464 * get_address_of_recenteredPosition_5() { return &___recenteredPosition_5; }
	inline void set_recenteredPosition_5(Vector3_t3722313464  value)
	{
		___recenteredPosition_5 = value;
	}

	inline static int32_t get_offset_of_recenteredRotation_6() { return static_cast<int32_t>(offsetof(HeadsetState_t378905490, ___recenteredRotation_6)); }
	inline Quaternion_t2301928331  get_recenteredRotation_6() const { return ___recenteredRotation_6; }
	inline Quaternion_t2301928331 * get_address_of_recenteredRotation_6() { return &___recenteredRotation_6; }
	inline void set_recenteredRotation_6(Quaternion_t2301928331  value)
	{
		___recenteredRotation_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADSETSTATE_T378905490_H
#ifndef KEYBOARDSTATE_T4109162649_H
#define KEYBOARDSTATE_T4109162649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeyboardState
struct  KeyboardState_t4109162649  : public RuntimeObject
{
public:
	// System.String KeyboardState::editorText
	String_t* ___editorText_0;
	// GvrKeyboardInputMode KeyboardState::mode
	int32_t ___mode_1;
	// System.Boolean KeyboardState::isValid
	bool ___isValid_2;
	// System.Boolean KeyboardState::isReady
	bool ___isReady_3;
	// UnityEngine.Matrix4x4 KeyboardState::worldMatrix
	Matrix4x4_t1817901843  ___worldMatrix_4;

public:
	inline static int32_t get_offset_of_editorText_0() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___editorText_0)); }
	inline String_t* get_editorText_0() const { return ___editorText_0; }
	inline String_t** get_address_of_editorText_0() { return &___editorText_0; }
	inline void set_editorText_0(String_t* value)
	{
		___editorText_0 = value;
		Il2CppCodeGenWriteBarrier((&___editorText_0), value);
	}

	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_isValid_2() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___isValid_2)); }
	inline bool get_isValid_2() const { return ___isValid_2; }
	inline bool* get_address_of_isValid_2() { return &___isValid_2; }
	inline void set_isValid_2(bool value)
	{
		___isValid_2 = value;
	}

	inline static int32_t get_offset_of_isReady_3() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___isReady_3)); }
	inline bool get_isReady_3() const { return ___isReady_3; }
	inline bool* get_address_of_isReady_3() { return &___isReady_3; }
	inline void set_isReady_3(bool value)
	{
		___isReady_3 = value;
	}

	inline static int32_t get_offset_of_worldMatrix_4() { return static_cast<int32_t>(offsetof(KeyboardState_t4109162649, ___worldMatrix_4)); }
	inline Matrix4x4_t1817901843  get_worldMatrix_4() const { return ___worldMatrix_4; }
	inline Matrix4x4_t1817901843 * get_address_of_worldMatrix_4() { return &___worldMatrix_4; }
	inline void set_worldMatrix_4(Matrix4x4_t1817901843  value)
	{
		___worldMatrix_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDSTATE_T4109162649_H
#ifndef MUTABLEPOSE3D_T3352419872_H
#define MUTABLEPOSE3D_T3352419872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MutablePose3D
struct  MutablePose3D_t3352419872  : public Pose3D_t2649470188
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUTABLEPOSE3D_T3352419872_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#define AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.AmbientOcclusionComponent
struct  AmbientOcclusionComponent_t4130625043  : public PostProcessingComponentCommandBuffer_1_t1664772955
{
public:
	// UnityEngine.Rendering.RenderTargetIdentifier[] UnityEngine.PostProcessing.AmbientOcclusionComponent::m_MRT
	RenderTargetIdentifierU5BU5D_t2742279485* ___m_MRT_4;

public:
	inline static int32_t get_offset_of_m_MRT_4() { return static_cast<int32_t>(offsetof(AmbientOcclusionComponent_t4130625043, ___m_MRT_4)); }
	inline RenderTargetIdentifierU5BU5D_t2742279485* get_m_MRT_4() const { return ___m_MRT_4; }
	inline RenderTargetIdentifierU5BU5D_t2742279485** get_address_of_m_MRT_4() { return &___m_MRT_4; }
	inline void set_m_MRT_4(RenderTargetIdentifierU5BU5D_t2742279485* value)
	{
		___m_MRT_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MRT_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMBIENTOCCLUSIONCOMPONENT_T4130625043_H
#ifndef BLOOMCOMPONENT_T3791419130_H
#define BLOOMCOMPONENT_T3791419130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BloomComponent
struct  BloomComponent_t3791419130  : public PostProcessingComponentRenderTexture_1_t3668012901
{
public:
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer1
	RenderTextureU5BU5D_t4111643188* ___m_BlurBuffer1_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.BloomComponent::m_BlurBuffer2
	RenderTextureU5BU5D_t4111643188* ___m_BlurBuffer2_4;

public:
	inline static int32_t get_offset_of_m_BlurBuffer1_3() { return static_cast<int32_t>(offsetof(BloomComponent_t3791419130, ___m_BlurBuffer1_3)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_BlurBuffer1_3() const { return ___m_BlurBuffer1_3; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_BlurBuffer1_3() { return &___m_BlurBuffer1_3; }
	inline void set_m_BlurBuffer1_3(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_BlurBuffer1_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer1_3), value);
	}

	inline static int32_t get_offset_of_m_BlurBuffer2_4() { return static_cast<int32_t>(offsetof(BloomComponent_t3791419130, ___m_BlurBuffer2_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_BlurBuffer2_4() const { return ___m_BlurBuffer2_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_BlurBuffer2_4() { return &___m_BlurBuffer2_4; }
	inline void set_m_BlurBuffer2_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_BlurBuffer2_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlurBuffer2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOOMCOMPONENT_T3791419130_H
#ifndef BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#define BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.BuiltinDebugViewsComponent
struct  BuiltinDebugViewsComponent_t2123147871  : public PostProcessingComponentCommandBuffer_1_t2737920729
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsComponent/ArrowArray UnityEngine.PostProcessing.BuiltinDebugViewsComponent::m_Arrows
	ArrowArray_t303178545 * ___m_Arrows_3;

public:
	inline static int32_t get_offset_of_m_Arrows_3() { return static_cast<int32_t>(offsetof(BuiltinDebugViewsComponent_t2123147871, ___m_Arrows_3)); }
	inline ArrowArray_t303178545 * get_m_Arrows_3() const { return ___m_Arrows_3; }
	inline ArrowArray_t303178545 ** get_address_of_m_Arrows_3() { return &___m_Arrows_3; }
	inline void set_m_Arrows_3(ArrowArray_t303178545 * value)
	{
		___m_Arrows_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Arrows_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILTINDEBUGVIEWSCOMPONENT_T2123147871_H
#ifndef CHROMATICABERRATIONCOMPONENT_T1647263118_H
#define CHROMATICABERRATIONCOMPONENT_T1647263118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ChromaticAberrationComponent
struct  ChromaticAberrationComponent_t1647263118  : public PostProcessingComponentRenderTexture_1_t1236717598
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ChromaticAberrationComponent::m_SpectrumLut
	Texture2D_t3840446185 * ___m_SpectrumLut_2;

public:
	inline static int32_t get_offset_of_m_SpectrumLut_2() { return static_cast<int32_t>(offsetof(ChromaticAberrationComponent_t1647263118, ___m_SpectrumLut_2)); }
	inline Texture2D_t3840446185 * get_m_SpectrumLut_2() const { return ___m_SpectrumLut_2; }
	inline Texture2D_t3840446185 ** get_address_of_m_SpectrumLut_2() { return &___m_SpectrumLut_2; }
	inline void set_m_SpectrumLut_2(Texture2D_t3840446185 * value)
	{
		___m_SpectrumLut_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpectrumLut_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHROMATICABERRATIONCOMPONENT_T1647263118_H
#ifndef COLORGRADINGCOMPONENT_T1715259467_H
#define COLORGRADINGCOMPONENT_T1715259467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingComponent
struct  ColorGradingComponent_t1715259467  : public PostProcessingComponentRenderTexture_1_t3016333222
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.ColorGradingComponent::m_GradingCurves
	Texture2D_t3840446185 * ___m_GradingCurves_5;
	// UnityEngine.Color[] UnityEngine.PostProcessing.ColorGradingComponent::m_pixels
	ColorU5BU5D_t941916413* ___m_pixels_6;

public:
	inline static int32_t get_offset_of_m_GradingCurves_5() { return static_cast<int32_t>(offsetof(ColorGradingComponent_t1715259467, ___m_GradingCurves_5)); }
	inline Texture2D_t3840446185 * get_m_GradingCurves_5() const { return ___m_GradingCurves_5; }
	inline Texture2D_t3840446185 ** get_address_of_m_GradingCurves_5() { return &___m_GradingCurves_5; }
	inline void set_m_GradingCurves_5(Texture2D_t3840446185 * value)
	{
		___m_GradingCurves_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradingCurves_5), value);
	}

	inline static int32_t get_offset_of_m_pixels_6() { return static_cast<int32_t>(offsetof(ColorGradingComponent_t1715259467, ___m_pixels_6)); }
	inline ColorU5BU5D_t941916413* get_m_pixels_6() const { return ___m_pixels_6; }
	inline ColorU5BU5D_t941916413** get_address_of_m_pixels_6() { return &___m_pixels_6; }
	inline void set_m_pixels_6(ColorU5BU5D_t941916413* value)
	{
		___m_pixels_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_pixels_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGCOMPONENT_T1715259467_H
#ifndef DEPTHOFFIELDCOMPONENT_T554756766_H
#define DEPTHOFFIELDCOMPONENT_T554756766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldComponent
struct  DepthOfFieldComponent_t554756766  : public PostProcessingComponentRenderTexture_1_t2082352371
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.DepthOfFieldComponent::m_CoCHistory
	RenderTexture_t2108887433 * ___m_CoCHistory_3;

public:
	inline static int32_t get_offset_of_m_CoCHistory_3() { return static_cast<int32_t>(offsetof(DepthOfFieldComponent_t554756766, ___m_CoCHistory_3)); }
	inline RenderTexture_t2108887433 * get_m_CoCHistory_3() const { return ___m_CoCHistory_3; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CoCHistory_3() { return &___m_CoCHistory_3; }
	inline void set_m_CoCHistory_3(RenderTexture_t2108887433 * value)
	{
		___m_CoCHistory_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_CoCHistory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDCOMPONENT_T554756766_H
#ifndef DITHERINGCOMPONENT_T277621267_H
#define DITHERINGCOMPONENT_T277621267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringComponent
struct  DitheringComponent_t277621267  : public PostProcessingComponentRenderTexture_1_t3997290437
{
public:
	// UnityEngine.Texture2D[] UnityEngine.PostProcessing.DitheringComponent::noiseTextures
	Texture2DU5BU5D_t149664596* ___noiseTextures_2;
	// System.Int32 UnityEngine.PostProcessing.DitheringComponent::textureIndex
	int32_t ___textureIndex_3;

public:
	inline static int32_t get_offset_of_noiseTextures_2() { return static_cast<int32_t>(offsetof(DitheringComponent_t277621267, ___noiseTextures_2)); }
	inline Texture2DU5BU5D_t149664596* get_noiseTextures_2() const { return ___noiseTextures_2; }
	inline Texture2DU5BU5D_t149664596** get_address_of_noiseTextures_2() { return &___noiseTextures_2; }
	inline void set_noiseTextures_2(Texture2DU5BU5D_t149664596* value)
	{
		___noiseTextures_2 = value;
		Il2CppCodeGenWriteBarrier((&___noiseTextures_2), value);
	}

	inline static int32_t get_offset_of_textureIndex_3() { return static_cast<int32_t>(offsetof(DitheringComponent_t277621267, ___textureIndex_3)); }
	inline int32_t get_textureIndex_3() const { return ___textureIndex_3; }
	inline int32_t* get_address_of_textureIndex_3() { return &___textureIndex_3; }
	inline void set_textureIndex_3(int32_t value)
	{
		___textureIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERINGCOMPONENT_T277621267_H
#ifndef EYEADAPTATIONCOMPONENT_T3394805121_H
#define EYEADAPTATIONCOMPONENT_T3394805121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationComponent
struct  EyeAdaptationComponent_t3394805121  : public PostProcessingComponentRenderTexture_1_t1811108953
{
public:
	// UnityEngine.ComputeShader UnityEngine.PostProcessing.EyeAdaptationComponent::m_EyeCompute
	ComputeShader_t317220254 * ___m_EyeCompute_2;
	// UnityEngine.ComputeBuffer UnityEngine.PostProcessing.EyeAdaptationComponent::m_HistogramBuffer
	ComputeBuffer_t1033194329 * ___m_HistogramBuffer_3;
	// UnityEngine.RenderTexture[] UnityEngine.PostProcessing.EyeAdaptationComponent::m_AutoExposurePool
	RenderTextureU5BU5D_t4111643188* ___m_AutoExposurePool_4;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationComponent::m_AutoExposurePingPing
	int32_t ___m_AutoExposurePingPing_5;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.EyeAdaptationComponent::m_CurrentAutoExposure
	RenderTexture_t2108887433 * ___m_CurrentAutoExposure_6;
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.EyeAdaptationComponent::m_DebugHistogram
	RenderTexture_t2108887433 * ___m_DebugHistogram_7;
	// System.Boolean UnityEngine.PostProcessing.EyeAdaptationComponent::m_FirstFrame
	bool ___m_FirstFrame_9;

public:
	inline static int32_t get_offset_of_m_EyeCompute_2() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_EyeCompute_2)); }
	inline ComputeShader_t317220254 * get_m_EyeCompute_2() const { return ___m_EyeCompute_2; }
	inline ComputeShader_t317220254 ** get_address_of_m_EyeCompute_2() { return &___m_EyeCompute_2; }
	inline void set_m_EyeCompute_2(ComputeShader_t317220254 * value)
	{
		___m_EyeCompute_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_EyeCompute_2), value);
	}

	inline static int32_t get_offset_of_m_HistogramBuffer_3() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_HistogramBuffer_3)); }
	inline ComputeBuffer_t1033194329 * get_m_HistogramBuffer_3() const { return ___m_HistogramBuffer_3; }
	inline ComputeBuffer_t1033194329 ** get_address_of_m_HistogramBuffer_3() { return &___m_HistogramBuffer_3; }
	inline void set_m_HistogramBuffer_3(ComputeBuffer_t1033194329 * value)
	{
		___m_HistogramBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_HistogramBuffer_3), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePool_4() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_AutoExposurePool_4)); }
	inline RenderTextureU5BU5D_t4111643188* get_m_AutoExposurePool_4() const { return ___m_AutoExposurePool_4; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_m_AutoExposurePool_4() { return &___m_AutoExposurePool_4; }
	inline void set_m_AutoExposurePool_4(RenderTextureU5BU5D_t4111643188* value)
	{
		___m_AutoExposurePool_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AutoExposurePool_4), value);
	}

	inline static int32_t get_offset_of_m_AutoExposurePingPing_5() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_AutoExposurePingPing_5)); }
	inline int32_t get_m_AutoExposurePingPing_5() const { return ___m_AutoExposurePingPing_5; }
	inline int32_t* get_address_of_m_AutoExposurePingPing_5() { return &___m_AutoExposurePingPing_5; }
	inline void set_m_AutoExposurePingPing_5(int32_t value)
	{
		___m_AutoExposurePingPing_5 = value;
	}

	inline static int32_t get_offset_of_m_CurrentAutoExposure_6() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_CurrentAutoExposure_6)); }
	inline RenderTexture_t2108887433 * get_m_CurrentAutoExposure_6() const { return ___m_CurrentAutoExposure_6; }
	inline RenderTexture_t2108887433 ** get_address_of_m_CurrentAutoExposure_6() { return &___m_CurrentAutoExposure_6; }
	inline void set_m_CurrentAutoExposure_6(RenderTexture_t2108887433 * value)
	{
		___m_CurrentAutoExposure_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentAutoExposure_6), value);
	}

	inline static int32_t get_offset_of_m_DebugHistogram_7() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_DebugHistogram_7)); }
	inline RenderTexture_t2108887433 * get_m_DebugHistogram_7() const { return ___m_DebugHistogram_7; }
	inline RenderTexture_t2108887433 ** get_address_of_m_DebugHistogram_7() { return &___m_DebugHistogram_7; }
	inline void set_m_DebugHistogram_7(RenderTexture_t2108887433 * value)
	{
		___m_DebugHistogram_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugHistogram_7), value);
	}

	inline static int32_t get_offset_of_m_FirstFrame_9() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121, ___m_FirstFrame_9)); }
	inline bool get_m_FirstFrame_9() const { return ___m_FirstFrame_9; }
	inline bool* get_address_of_m_FirstFrame_9() { return &___m_FirstFrame_9; }
	inline void set_m_FirstFrame_9(bool value)
	{
		___m_FirstFrame_9 = value;
	}
};

struct EyeAdaptationComponent_t3394805121_StaticFields
{
public:
	// System.UInt32[] UnityEngine.PostProcessing.EyeAdaptationComponent::s_EmptyHistogramBuffer
	UInt32U5BU5D_t2770800703* ___s_EmptyHistogramBuffer_8;

public:
	inline static int32_t get_offset_of_s_EmptyHistogramBuffer_8() { return static_cast<int32_t>(offsetof(EyeAdaptationComponent_t3394805121_StaticFields, ___s_EmptyHistogramBuffer_8)); }
	inline UInt32U5BU5D_t2770800703* get_s_EmptyHistogramBuffer_8() const { return ___s_EmptyHistogramBuffer_8; }
	inline UInt32U5BU5D_t2770800703** get_address_of_s_EmptyHistogramBuffer_8() { return &___s_EmptyHistogramBuffer_8; }
	inline void set_s_EmptyHistogramBuffer_8(UInt32U5BU5D_t2770800703* value)
	{
		___s_EmptyHistogramBuffer_8 = value;
		Il2CppCodeGenWriteBarrier((&___s_EmptyHistogramBuffer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONCOMPONENT_T3394805121_H
#ifndef FOGCOMPONENT_T3400726830_H
#define FOGCOMPONENT_T3400726830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogComponent
struct  FogComponent_t3400726830  : public PostProcessingComponentCommandBuffer_1_t601023342
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGCOMPONENT_T3400726830_H
#ifndef FXAACOMPONENT_T1312385771_H
#define FXAACOMPONENT_T1312385771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FxaaComponent
struct  FxaaComponent_t1312385771  : public PostProcessingComponentRenderTexture_1_t3089424429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FXAACOMPONENT_T1312385771_H
#ifndef GRAINCOMPONENT_T866324317_H
#define GRAINCOMPONENT_T866324317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainComponent
struct  GrainComponent_t866324317  : public PostProcessingComponentRenderTexture_1_t2721167529
{
public:
	// UnityEngine.RenderTexture UnityEngine.PostProcessing.GrainComponent::m_GrainLookupRT
	RenderTexture_t2108887433 * ___m_GrainLookupRT_2;

public:
	inline static int32_t get_offset_of_m_GrainLookupRT_2() { return static_cast<int32_t>(offsetof(GrainComponent_t866324317, ___m_GrainLookupRT_2)); }
	inline RenderTexture_t2108887433 * get_m_GrainLookupRT_2() const { return ___m_GrainLookupRT_2; }
	inline RenderTexture_t2108887433 ** get_address_of_m_GrainLookupRT_2() { return &___m_GrainLookupRT_2; }
	inline void set_m_GrainLookupRT_2(RenderTexture_t2108887433 * value)
	{
		___m_GrainLookupRT_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GrainLookupRT_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINCOMPONENT_T866324317_H
#ifndef NAVIGATION_T3049316579_H
#define NAVIGATION_T3049316579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t3049316579 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3250028441 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnUp_1)); }
	inline Selectable_t3250028441 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t3250028441 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnDown_2)); }
	inline Selectable_t3250028441 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t3250028441 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnLeft_3)); }
	inline Selectable_t3250028441 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t3250028441 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t3049316579, ___m_SelectOnRight_4)); }
	inline Selectable_t3250028441 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t3250028441 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t3250028441 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t3049316579_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t3250028441 * ___m_SelectOnUp_1;
	Selectable_t3250028441 * ___m_SelectOnDown_2;
	Selectable_t3250028441 * ___m_SelectOnLeft_3;
	Selectable_t3250028441 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T3049316579_H
#ifndef DUMMYHEADSETPROVIDER_T3229995161_H
#define DUMMYHEADSETPROVIDER_T3229995161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.DummyHeadsetProvider
struct  DummyHeadsetProvider_t3229995161  : public RuntimeObject
{
public:
	// Gvr.Internal.HeadsetState Gvr.Internal.DummyHeadsetProvider::dummyState
	HeadsetState_t378905490  ___dummyState_0;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(DummyHeadsetProvider_t3229995161, ___dummyState_0)); }
	inline HeadsetState_t378905490  get_dummyState_0() const { return ___dummyState_0; }
	inline HeadsetState_t378905490 * get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(HeadsetState_t378905490  value)
	{
		___dummyState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DUMMYHEADSETPROVIDER_T3229995161_H
#ifndef EDITORHEADSETPROVIDER_T660777464_H
#define EDITORHEADSETPROVIDER_T660777464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.EditorHeadsetProvider
struct  EditorHeadsetProvider_t660777464  : public RuntimeObject
{
public:
	// Gvr.Internal.HeadsetState Gvr.Internal.EditorHeadsetProvider::dummyState
	HeadsetState_t378905490  ___dummyState_0;

public:
	inline static int32_t get_offset_of_dummyState_0() { return static_cast<int32_t>(offsetof(EditorHeadsetProvider_t660777464, ___dummyState_0)); }
	inline HeadsetState_t378905490  get_dummyState_0() const { return ___dummyState_0; }
	inline HeadsetState_t378905490 * get_address_of_dummyState_0() { return &___dummyState_0; }
	inline void set_dummyState_0(HeadsetState_t378905490  value)
	{
		___dummyState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORHEADSETPROVIDER_T660777464_H
#ifndef EDITTEXTCALLBACK_T1702213000_H
#define EDITTEXTCALLBACK_T1702213000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/EditTextCallback
struct  EditTextCallback_t1702213000  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITTEXTCALLBACK_T1702213000_H
#ifndef ERRORCALLBACK_T2310212740_H
#define ERRORCALLBACK_T2310212740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/ErrorCallback
struct  ErrorCallback_t2310212740  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCALLBACK_T2310212740_H
#ifndef KEYBOARDCALLBACK_T3330588312_H
#define KEYBOARDCALLBACK_T3330588312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/KeyboardCallback
struct  KeyboardCallback_t3330588312  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDCALLBACK_T3330588312_H
#ifndef STANDARDCALLBACK_T3095007891_H
#define STANDARDCALLBACK_T3095007891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard/StandardCallback
struct  StandardCallback_t3095007891  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDCALLBACK_T3095007891_H
#ifndef ONEXCEPTIONCALLBACK_T1696428116_H
#define ONEXCEPTIONCALLBACK_T1696428116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/OnExceptionCallback
struct  OnExceptionCallback_t1696428116  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEXCEPTIONCALLBACK_T1696428116_H
#ifndef ONVIDEOEVENTCALLBACK_T2376626694_H
#define ONVIDEOEVENTCALLBACK_T2376626694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture/OnVideoEventCallback
struct  OnVideoEventCallback_t2376626694  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVIDEOEVENTCALLBACK_T2376626694_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DEMOSCRIPT_T4014867723_H
#define DEMOSCRIPT_T4014867723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoScript
struct  DemoScript_t4014867723  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DemoScript::Mirrors
	List_1_t2585711361 * ___Mirrors_4;
	// UnityEngine.GameObject DemoScript::LightBulb
	GameObject_t1113636619 * ___LightBulb_5;
	// UnityEngine.UI.Toggle DemoScript::RecursionToggle
	Toggle_t2735377061 * ___RecursionToggle_6;
	// System.Single DemoScript::rotationModifier
	float ___rotationModifier_7;
	// System.Single DemoScript::moveModifier
	float ___moveModifier_8;
	// UnityEngine.Material DemoScript::lightBulbMaterial
	Material_t340375123 * ___lightBulbMaterial_9;
	// DemoScript/RotationAxes DemoScript::axes
	int32_t ___axes_10;
	// System.Single DemoScript::sensitivityX
	float ___sensitivityX_11;
	// System.Single DemoScript::sensitivityY
	float ___sensitivityY_12;
	// System.Single DemoScript::minimumX
	float ___minimumX_13;
	// System.Single DemoScript::maximumX
	float ___maximumX_14;
	// System.Single DemoScript::minimumY
	float ___minimumY_15;
	// System.Single DemoScript::maximumY
	float ___maximumY_16;
	// System.Single DemoScript::rotationX
	float ___rotationX_17;
	// System.Single DemoScript::rotationY
	float ___rotationY_18;
	// UnityEngine.Quaternion DemoScript::originalRotation
	Quaternion_t2301928331  ___originalRotation_19;

public:
	inline static int32_t get_offset_of_Mirrors_4() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___Mirrors_4)); }
	inline List_1_t2585711361 * get_Mirrors_4() const { return ___Mirrors_4; }
	inline List_1_t2585711361 ** get_address_of_Mirrors_4() { return &___Mirrors_4; }
	inline void set_Mirrors_4(List_1_t2585711361 * value)
	{
		___Mirrors_4 = value;
		Il2CppCodeGenWriteBarrier((&___Mirrors_4), value);
	}

	inline static int32_t get_offset_of_LightBulb_5() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___LightBulb_5)); }
	inline GameObject_t1113636619 * get_LightBulb_5() const { return ___LightBulb_5; }
	inline GameObject_t1113636619 ** get_address_of_LightBulb_5() { return &___LightBulb_5; }
	inline void set_LightBulb_5(GameObject_t1113636619 * value)
	{
		___LightBulb_5 = value;
		Il2CppCodeGenWriteBarrier((&___LightBulb_5), value);
	}

	inline static int32_t get_offset_of_RecursionToggle_6() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___RecursionToggle_6)); }
	inline Toggle_t2735377061 * get_RecursionToggle_6() const { return ___RecursionToggle_6; }
	inline Toggle_t2735377061 ** get_address_of_RecursionToggle_6() { return &___RecursionToggle_6; }
	inline void set_RecursionToggle_6(Toggle_t2735377061 * value)
	{
		___RecursionToggle_6 = value;
		Il2CppCodeGenWriteBarrier((&___RecursionToggle_6), value);
	}

	inline static int32_t get_offset_of_rotationModifier_7() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___rotationModifier_7)); }
	inline float get_rotationModifier_7() const { return ___rotationModifier_7; }
	inline float* get_address_of_rotationModifier_7() { return &___rotationModifier_7; }
	inline void set_rotationModifier_7(float value)
	{
		___rotationModifier_7 = value;
	}

	inline static int32_t get_offset_of_moveModifier_8() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___moveModifier_8)); }
	inline float get_moveModifier_8() const { return ___moveModifier_8; }
	inline float* get_address_of_moveModifier_8() { return &___moveModifier_8; }
	inline void set_moveModifier_8(float value)
	{
		___moveModifier_8 = value;
	}

	inline static int32_t get_offset_of_lightBulbMaterial_9() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___lightBulbMaterial_9)); }
	inline Material_t340375123 * get_lightBulbMaterial_9() const { return ___lightBulbMaterial_9; }
	inline Material_t340375123 ** get_address_of_lightBulbMaterial_9() { return &___lightBulbMaterial_9; }
	inline void set_lightBulbMaterial_9(Material_t340375123 * value)
	{
		___lightBulbMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___lightBulbMaterial_9), value);
	}

	inline static int32_t get_offset_of_axes_10() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___axes_10)); }
	inline int32_t get_axes_10() const { return ___axes_10; }
	inline int32_t* get_address_of_axes_10() { return &___axes_10; }
	inline void set_axes_10(int32_t value)
	{
		___axes_10 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_11() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___sensitivityX_11)); }
	inline float get_sensitivityX_11() const { return ___sensitivityX_11; }
	inline float* get_address_of_sensitivityX_11() { return &___sensitivityX_11; }
	inline void set_sensitivityX_11(float value)
	{
		___sensitivityX_11 = value;
	}

	inline static int32_t get_offset_of_sensitivityY_12() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___sensitivityY_12)); }
	inline float get_sensitivityY_12() const { return ___sensitivityY_12; }
	inline float* get_address_of_sensitivityY_12() { return &___sensitivityY_12; }
	inline void set_sensitivityY_12(float value)
	{
		___sensitivityY_12 = value;
	}

	inline static int32_t get_offset_of_minimumX_13() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___minimumX_13)); }
	inline float get_minimumX_13() const { return ___minimumX_13; }
	inline float* get_address_of_minimumX_13() { return &___minimumX_13; }
	inline void set_minimumX_13(float value)
	{
		___minimumX_13 = value;
	}

	inline static int32_t get_offset_of_maximumX_14() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___maximumX_14)); }
	inline float get_maximumX_14() const { return ___maximumX_14; }
	inline float* get_address_of_maximumX_14() { return &___maximumX_14; }
	inline void set_maximumX_14(float value)
	{
		___maximumX_14 = value;
	}

	inline static int32_t get_offset_of_minimumY_15() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___minimumY_15)); }
	inline float get_minimumY_15() const { return ___minimumY_15; }
	inline float* get_address_of_minimumY_15() { return &___minimumY_15; }
	inline void set_minimumY_15(float value)
	{
		___minimumY_15 = value;
	}

	inline static int32_t get_offset_of_maximumY_16() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___maximumY_16)); }
	inline float get_maximumY_16() const { return ___maximumY_16; }
	inline float* get_address_of_maximumY_16() { return &___maximumY_16; }
	inline void set_maximumY_16(float value)
	{
		___maximumY_16 = value;
	}

	inline static int32_t get_offset_of_rotationX_17() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___rotationX_17)); }
	inline float get_rotationX_17() const { return ___rotationX_17; }
	inline float* get_address_of_rotationX_17() { return &___rotationX_17; }
	inline void set_rotationX_17(float value)
	{
		___rotationX_17 = value;
	}

	inline static int32_t get_offset_of_rotationY_18() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___rotationY_18)); }
	inline float get_rotationY_18() const { return ___rotationY_18; }
	inline float* get_address_of_rotationY_18() { return &___rotationY_18; }
	inline void set_rotationY_18(float value)
	{
		___rotationY_18 = value;
	}

	inline static int32_t get_offset_of_originalRotation_19() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___originalRotation_19)); }
	inline Quaternion_t2301928331  get_originalRotation_19() const { return ___originalRotation_19; }
	inline Quaternion_t2301928331 * get_address_of_originalRotation_19() { return &___originalRotation_19; }
	inline void set_originalRotation_19(Quaternion_t2301928331  value)
	{
		___originalRotation_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPT_T4014867723_H
#ifndef INSTANTPREVIEW_T778898416_H
#define INSTANTPREVIEW_T778898416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Gvr.Internal.InstantPreview
struct  InstantPreview_t778898416  : public MonoBehaviour_t3962482529
{
public:
	// Gvr.Internal.InstantPreview/Resolutions Gvr.Internal.InstantPreview::OutputResolution
	int32_t ___OutputResolution_7;
	// Gvr.Internal.InstantPreview/MultisampleCounts Gvr.Internal.InstantPreview::MultisampleCount
	int32_t ___MultisampleCount_8;
	// Gvr.Internal.InstantPreview/BitRates Gvr.Internal.InstantPreview::BitRate
	int32_t ___BitRate_9;
	// System.Boolean Gvr.Internal.InstantPreview::InstallApkOnRun
	bool ___InstallApkOnRun_10;
	// UnityEngine.Object Gvr.Internal.InstantPreview::InstantPreviewApk
	Object_t631007953 * ___InstantPreviewApk_11;

public:
	inline static int32_t get_offset_of_OutputResolution_7() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___OutputResolution_7)); }
	inline int32_t get_OutputResolution_7() const { return ___OutputResolution_7; }
	inline int32_t* get_address_of_OutputResolution_7() { return &___OutputResolution_7; }
	inline void set_OutputResolution_7(int32_t value)
	{
		___OutputResolution_7 = value;
	}

	inline static int32_t get_offset_of_MultisampleCount_8() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___MultisampleCount_8)); }
	inline int32_t get_MultisampleCount_8() const { return ___MultisampleCount_8; }
	inline int32_t* get_address_of_MultisampleCount_8() { return &___MultisampleCount_8; }
	inline void set_MultisampleCount_8(int32_t value)
	{
		___MultisampleCount_8 = value;
	}

	inline static int32_t get_offset_of_BitRate_9() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___BitRate_9)); }
	inline int32_t get_BitRate_9() const { return ___BitRate_9; }
	inline int32_t* get_address_of_BitRate_9() { return &___BitRate_9; }
	inline void set_BitRate_9(int32_t value)
	{
		___BitRate_9 = value;
	}

	inline static int32_t get_offset_of_InstallApkOnRun_10() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___InstallApkOnRun_10)); }
	inline bool get_InstallApkOnRun_10() const { return ___InstallApkOnRun_10; }
	inline bool* get_address_of_InstallApkOnRun_10() { return &___InstallApkOnRun_10; }
	inline void set_InstallApkOnRun_10(bool value)
	{
		___InstallApkOnRun_10 = value;
	}

	inline static int32_t get_offset_of_InstantPreviewApk_11() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416, ___InstantPreviewApk_11)); }
	inline Object_t631007953 * get_InstantPreviewApk_11() const { return ___InstantPreviewApk_11; }
	inline Object_t631007953 ** get_address_of_InstantPreviewApk_11() { return &___InstantPreviewApk_11; }
	inline void set_InstantPreviewApk_11(Object_t631007953 * value)
	{
		___InstantPreviewApk_11 = value;
		Il2CppCodeGenWriteBarrier((&___InstantPreviewApk_11), value);
	}
};

struct InstantPreview_t778898416_StaticFields
{
public:
	// Gvr.Internal.InstantPreview Gvr.Internal.InstantPreview::<Instance>k__BackingField
	InstantPreview_t778898416 * ___U3CInstanceU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InstantPreview_t778898416_StaticFields, ___U3CInstanceU3Ek__BackingField_5)); }
	inline InstantPreview_t778898416 * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline InstantPreview_t778898416 ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(InstantPreview_t778898416 * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEW_T778898416_H
#ifndef GVRKEYBOARD_T2536560201_H
#define GVRKEYBOARD_T2536560201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboard
struct  GvrKeyboard_t2536560201  : public MonoBehaviour_t3962482529
{
public:
	// KeyboardState GvrKeyboard::keyboardState
	KeyboardState_t4109162649 * ___keyboardState_6;
	// System.Collections.IEnumerator GvrKeyboard::keyboardUpdate
	RuntimeObject* ___keyboardUpdate_7;
	// GvrKeyboard/ErrorCallback GvrKeyboard::errorCallback
	ErrorCallback_t2310212740 * ___errorCallback_8;
	// GvrKeyboard/StandardCallback GvrKeyboard::showCallback
	StandardCallback_t3095007891 * ___showCallback_9;
	// GvrKeyboard/StandardCallback GvrKeyboard::hideCallback
	StandardCallback_t3095007891 * ___hideCallback_10;
	// GvrKeyboard/EditTextCallback GvrKeyboard::updateCallback
	EditTextCallback_t1702213000 * ___updateCallback_11;
	// GvrKeyboard/EditTextCallback GvrKeyboard::enterCallback
	EditTextCallback_t1702213000 * ___enterCallback_12;
	// System.Boolean GvrKeyboard::isKeyboardHidden
	bool ___isKeyboardHidden_13;
	// GvrKeyboardDelegateBase GvrKeyboard::keyboardDelegate
	GvrKeyboardDelegateBase_t30895224 * ___keyboardDelegate_17;
	// GvrKeyboardInputMode GvrKeyboard::inputMode
	int32_t ___inputMode_18;
	// System.Boolean GvrKeyboard::useRecommended
	bool ___useRecommended_19;
	// System.Single GvrKeyboard::distance
	float ___distance_20;

public:
	inline static int32_t get_offset_of_keyboardState_6() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___keyboardState_6)); }
	inline KeyboardState_t4109162649 * get_keyboardState_6() const { return ___keyboardState_6; }
	inline KeyboardState_t4109162649 ** get_address_of_keyboardState_6() { return &___keyboardState_6; }
	inline void set_keyboardState_6(KeyboardState_t4109162649 * value)
	{
		___keyboardState_6 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardState_6), value);
	}

	inline static int32_t get_offset_of_keyboardUpdate_7() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___keyboardUpdate_7)); }
	inline RuntimeObject* get_keyboardUpdate_7() const { return ___keyboardUpdate_7; }
	inline RuntimeObject** get_address_of_keyboardUpdate_7() { return &___keyboardUpdate_7; }
	inline void set_keyboardUpdate_7(RuntimeObject* value)
	{
		___keyboardUpdate_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardUpdate_7), value);
	}

	inline static int32_t get_offset_of_errorCallback_8() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___errorCallback_8)); }
	inline ErrorCallback_t2310212740 * get_errorCallback_8() const { return ___errorCallback_8; }
	inline ErrorCallback_t2310212740 ** get_address_of_errorCallback_8() { return &___errorCallback_8; }
	inline void set_errorCallback_8(ErrorCallback_t2310212740 * value)
	{
		___errorCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___errorCallback_8), value);
	}

	inline static int32_t get_offset_of_showCallback_9() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___showCallback_9)); }
	inline StandardCallback_t3095007891 * get_showCallback_9() const { return ___showCallback_9; }
	inline StandardCallback_t3095007891 ** get_address_of_showCallback_9() { return &___showCallback_9; }
	inline void set_showCallback_9(StandardCallback_t3095007891 * value)
	{
		___showCallback_9 = value;
		Il2CppCodeGenWriteBarrier((&___showCallback_9), value);
	}

	inline static int32_t get_offset_of_hideCallback_10() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___hideCallback_10)); }
	inline StandardCallback_t3095007891 * get_hideCallback_10() const { return ___hideCallback_10; }
	inline StandardCallback_t3095007891 ** get_address_of_hideCallback_10() { return &___hideCallback_10; }
	inline void set_hideCallback_10(StandardCallback_t3095007891 * value)
	{
		___hideCallback_10 = value;
		Il2CppCodeGenWriteBarrier((&___hideCallback_10), value);
	}

	inline static int32_t get_offset_of_updateCallback_11() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___updateCallback_11)); }
	inline EditTextCallback_t1702213000 * get_updateCallback_11() const { return ___updateCallback_11; }
	inline EditTextCallback_t1702213000 ** get_address_of_updateCallback_11() { return &___updateCallback_11; }
	inline void set_updateCallback_11(EditTextCallback_t1702213000 * value)
	{
		___updateCallback_11 = value;
		Il2CppCodeGenWriteBarrier((&___updateCallback_11), value);
	}

	inline static int32_t get_offset_of_enterCallback_12() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___enterCallback_12)); }
	inline EditTextCallback_t1702213000 * get_enterCallback_12() const { return ___enterCallback_12; }
	inline EditTextCallback_t1702213000 ** get_address_of_enterCallback_12() { return &___enterCallback_12; }
	inline void set_enterCallback_12(EditTextCallback_t1702213000 * value)
	{
		___enterCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___enterCallback_12), value);
	}

	inline static int32_t get_offset_of_isKeyboardHidden_13() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___isKeyboardHidden_13)); }
	inline bool get_isKeyboardHidden_13() const { return ___isKeyboardHidden_13; }
	inline bool* get_address_of_isKeyboardHidden_13() { return &___isKeyboardHidden_13; }
	inline void set_isKeyboardHidden_13(bool value)
	{
		___isKeyboardHidden_13 = value;
	}

	inline static int32_t get_offset_of_keyboardDelegate_17() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___keyboardDelegate_17)); }
	inline GvrKeyboardDelegateBase_t30895224 * get_keyboardDelegate_17() const { return ___keyboardDelegate_17; }
	inline GvrKeyboardDelegateBase_t30895224 ** get_address_of_keyboardDelegate_17() { return &___keyboardDelegate_17; }
	inline void set_keyboardDelegate_17(GvrKeyboardDelegateBase_t30895224 * value)
	{
		___keyboardDelegate_17 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardDelegate_17), value);
	}

	inline static int32_t get_offset_of_inputMode_18() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___inputMode_18)); }
	inline int32_t get_inputMode_18() const { return ___inputMode_18; }
	inline int32_t* get_address_of_inputMode_18() { return &___inputMode_18; }
	inline void set_inputMode_18(int32_t value)
	{
		___inputMode_18 = value;
	}

	inline static int32_t get_offset_of_useRecommended_19() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___useRecommended_19)); }
	inline bool get_useRecommended_19() const { return ___useRecommended_19; }
	inline bool* get_address_of_useRecommended_19() { return &___useRecommended_19; }
	inline void set_useRecommended_19(bool value)
	{
		___useRecommended_19 = value;
	}

	inline static int32_t get_offset_of_distance_20() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201, ___distance_20)); }
	inline float get_distance_20() const { return ___distance_20; }
	inline float* get_address_of_distance_20() { return &___distance_20; }
	inline void set_distance_20(float value)
	{
		___distance_20 = value;
	}
};

struct GvrKeyboard_t2536560201_StaticFields
{
public:
	// GvrKeyboard GvrKeyboard::instance
	GvrKeyboard_t2536560201 * ___instance_4;
	// Gvr.Internal.IKeyboardProvider GvrKeyboard::keyboardProvider
	RuntimeObject* ___keyboardProvider_5;
	// System.Collections.Generic.List`1<GvrKeyboardEvent> GvrKeyboard::threadSafeCallbacks
	List_1_t806272884 * ___threadSafeCallbacks_15;
	// System.Object GvrKeyboard::callbacksLock
	RuntimeObject * ___callbacksLock_16;
	// GvrKeyboard/KeyboardCallback GvrKeyboard::<>f__mg$cache0
	KeyboardCallback_t3330588312 * ___U3CU3Ef__mgU24cache0_21;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___instance_4)); }
	inline GvrKeyboard_t2536560201 * get_instance_4() const { return ___instance_4; }
	inline GvrKeyboard_t2536560201 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GvrKeyboard_t2536560201 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_keyboardProvider_5() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___keyboardProvider_5)); }
	inline RuntimeObject* get_keyboardProvider_5() const { return ___keyboardProvider_5; }
	inline RuntimeObject** get_address_of_keyboardProvider_5() { return &___keyboardProvider_5; }
	inline void set_keyboardProvider_5(RuntimeObject* value)
	{
		___keyboardProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardProvider_5), value);
	}

	inline static int32_t get_offset_of_threadSafeCallbacks_15() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___threadSafeCallbacks_15)); }
	inline List_1_t806272884 * get_threadSafeCallbacks_15() const { return ___threadSafeCallbacks_15; }
	inline List_1_t806272884 ** get_address_of_threadSafeCallbacks_15() { return &___threadSafeCallbacks_15; }
	inline void set_threadSafeCallbacks_15(List_1_t806272884 * value)
	{
		___threadSafeCallbacks_15 = value;
		Il2CppCodeGenWriteBarrier((&___threadSafeCallbacks_15), value);
	}

	inline static int32_t get_offset_of_callbacksLock_16() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___callbacksLock_16)); }
	inline RuntimeObject * get_callbacksLock_16() const { return ___callbacksLock_16; }
	inline RuntimeObject ** get_address_of_callbacksLock_16() { return &___callbacksLock_16; }
	inline void set_callbacksLock_16(RuntimeObject * value)
	{
		___callbacksLock_16 = value;
		Il2CppCodeGenWriteBarrier((&___callbacksLock_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_21() { return static_cast<int32_t>(offsetof(GvrKeyboard_t2536560201_StaticFields, ___U3CU3Ef__mgU24cache0_21)); }
	inline KeyboardCallback_t3330588312 * get_U3CU3Ef__mgU24cache0_21() const { return ___U3CU3Ef__mgU24cache0_21; }
	inline KeyboardCallback_t3330588312 ** get_address_of_U3CU3Ef__mgU24cache0_21() { return &___U3CU3Ef__mgU24cache0_21; }
	inline void set_U3CU3Ef__mgU24cache0_21(KeyboardCallback_t3330588312 * value)
	{
		___U3CU3Ef__mgU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARD_T2536560201_H
#ifndef GVRKEYBOARDDELEGATEBASE_T30895224_H
#define GVRKEYBOARDDELEGATEBASE_T30895224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrKeyboardDelegateBase
struct  GvrKeyboardDelegateBase_t30895224  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRKEYBOARDDELEGATEBASE_T30895224_H
#ifndef GVRVIDEOPLAYERTEXTURE_T3546202735_H
#define GVRVIDEOPLAYERTEXTURE_T3546202735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrVideoPlayerTexture
struct  GvrVideoPlayerTexture_t3546202735  : public MonoBehaviour_t3962482529
{
public:
	// System.IntPtr GvrVideoPlayerTexture::videoPlayerPtr
	intptr_t ___videoPlayerPtr_4;
	// System.Int32 GvrVideoPlayerTexture::videoPlayerEventBase
	int32_t ___videoPlayerEventBase_5;
	// UnityEngine.Texture GvrVideoPlayerTexture::initialTexture
	Texture_t3661962703 * ___initialTexture_6;
	// UnityEngine.Texture GvrVideoPlayerTexture::surfaceTexture
	Texture_t3661962703 * ___surfaceTexture_7;
	// System.Single[] GvrVideoPlayerTexture::videoMatrixRaw
	SingleU5BU5D_t1444911251* ___videoMatrixRaw_8;
	// UnityEngine.Matrix4x4 GvrVideoPlayerTexture::videoMatrix
	Matrix4x4_t1817901843  ___videoMatrix_9;
	// System.Int32 GvrVideoPlayerTexture::videoMatrixPropertyId
	int32_t ___videoMatrixPropertyId_10;
	// System.Int64 GvrVideoPlayerTexture::lastVideoTimestamp
	int64_t ___lastVideoTimestamp_11;
	// System.Boolean GvrVideoPlayerTexture::initialized
	bool ___initialized_12;
	// System.Int32 GvrVideoPlayerTexture::texWidth
	int32_t ___texWidth_13;
	// System.Int32 GvrVideoPlayerTexture::texHeight
	int32_t ___texHeight_14;
	// System.Int64 GvrVideoPlayerTexture::lastBufferedPosition
	int64_t ___lastBufferedPosition_15;
	// System.Single GvrVideoPlayerTexture::framecount
	float ___framecount_16;
	// UnityEngine.Renderer GvrVideoPlayerTexture::screen
	Renderer_t2627027031 * ___screen_17;
	// System.IntPtr GvrVideoPlayerTexture::renderEventFunction
	intptr_t ___renderEventFunction_18;
	// System.Boolean GvrVideoPlayerTexture::playOnResume
	bool ___playOnResume_19;
	// System.Collections.Generic.List`1<System.Action`1<System.Int32>> GvrVideoPlayerTexture::onEventCallbacks
	List_1_t300520794 * ___onEventCallbacks_20;
	// System.Collections.Generic.List`1<System.Action`2<System.String,System.String>> GvrVideoPlayerTexture::onExceptionCallbacks
	List_1_t1147278120 * ___onExceptionCallbacks_21;
	// UnityEngine.UI.Text GvrVideoPlayerTexture::statusText
	Text_t1901882714 * ___statusText_23;
	// GvrVideoPlayerTexture/VideoType GvrVideoPlayerTexture::videoType
	int32_t ___videoType_24;
	// System.String GvrVideoPlayerTexture::videoURL
	String_t* ___videoURL_25;
	// System.String GvrVideoPlayerTexture::videoContentID
	String_t* ___videoContentID_26;
	// System.String GvrVideoPlayerTexture::videoProviderId
	String_t* ___videoProviderId_27;
	// GvrVideoPlayerTexture/VideoResolution GvrVideoPlayerTexture::initialResolution
	int32_t ___initialResolution_28;
	// System.Boolean GvrVideoPlayerTexture::adjustAspectRatio
	bool ___adjustAspectRatio_29;
	// System.Boolean GvrVideoPlayerTexture::useSecurePath
	bool ___useSecurePath_30;

public:
	inline static int32_t get_offset_of_videoPlayerPtr_4() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoPlayerPtr_4)); }
	inline intptr_t get_videoPlayerPtr_4() const { return ___videoPlayerPtr_4; }
	inline intptr_t* get_address_of_videoPlayerPtr_4() { return &___videoPlayerPtr_4; }
	inline void set_videoPlayerPtr_4(intptr_t value)
	{
		___videoPlayerPtr_4 = value;
	}

	inline static int32_t get_offset_of_videoPlayerEventBase_5() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoPlayerEventBase_5)); }
	inline int32_t get_videoPlayerEventBase_5() const { return ___videoPlayerEventBase_5; }
	inline int32_t* get_address_of_videoPlayerEventBase_5() { return &___videoPlayerEventBase_5; }
	inline void set_videoPlayerEventBase_5(int32_t value)
	{
		___videoPlayerEventBase_5 = value;
	}

	inline static int32_t get_offset_of_initialTexture_6() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___initialTexture_6)); }
	inline Texture_t3661962703 * get_initialTexture_6() const { return ___initialTexture_6; }
	inline Texture_t3661962703 ** get_address_of_initialTexture_6() { return &___initialTexture_6; }
	inline void set_initialTexture_6(Texture_t3661962703 * value)
	{
		___initialTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___initialTexture_6), value);
	}

	inline static int32_t get_offset_of_surfaceTexture_7() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___surfaceTexture_7)); }
	inline Texture_t3661962703 * get_surfaceTexture_7() const { return ___surfaceTexture_7; }
	inline Texture_t3661962703 ** get_address_of_surfaceTexture_7() { return &___surfaceTexture_7; }
	inline void set_surfaceTexture_7(Texture_t3661962703 * value)
	{
		___surfaceTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___surfaceTexture_7), value);
	}

	inline static int32_t get_offset_of_videoMatrixRaw_8() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoMatrixRaw_8)); }
	inline SingleU5BU5D_t1444911251* get_videoMatrixRaw_8() const { return ___videoMatrixRaw_8; }
	inline SingleU5BU5D_t1444911251** get_address_of_videoMatrixRaw_8() { return &___videoMatrixRaw_8; }
	inline void set_videoMatrixRaw_8(SingleU5BU5D_t1444911251* value)
	{
		___videoMatrixRaw_8 = value;
		Il2CppCodeGenWriteBarrier((&___videoMatrixRaw_8), value);
	}

	inline static int32_t get_offset_of_videoMatrix_9() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoMatrix_9)); }
	inline Matrix4x4_t1817901843  get_videoMatrix_9() const { return ___videoMatrix_9; }
	inline Matrix4x4_t1817901843 * get_address_of_videoMatrix_9() { return &___videoMatrix_9; }
	inline void set_videoMatrix_9(Matrix4x4_t1817901843  value)
	{
		___videoMatrix_9 = value;
	}

	inline static int32_t get_offset_of_videoMatrixPropertyId_10() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoMatrixPropertyId_10)); }
	inline int32_t get_videoMatrixPropertyId_10() const { return ___videoMatrixPropertyId_10; }
	inline int32_t* get_address_of_videoMatrixPropertyId_10() { return &___videoMatrixPropertyId_10; }
	inline void set_videoMatrixPropertyId_10(int32_t value)
	{
		___videoMatrixPropertyId_10 = value;
	}

	inline static int32_t get_offset_of_lastVideoTimestamp_11() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___lastVideoTimestamp_11)); }
	inline int64_t get_lastVideoTimestamp_11() const { return ___lastVideoTimestamp_11; }
	inline int64_t* get_address_of_lastVideoTimestamp_11() { return &___lastVideoTimestamp_11; }
	inline void set_lastVideoTimestamp_11(int64_t value)
	{
		___lastVideoTimestamp_11 = value;
	}

	inline static int32_t get_offset_of_initialized_12() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___initialized_12)); }
	inline bool get_initialized_12() const { return ___initialized_12; }
	inline bool* get_address_of_initialized_12() { return &___initialized_12; }
	inline void set_initialized_12(bool value)
	{
		___initialized_12 = value;
	}

	inline static int32_t get_offset_of_texWidth_13() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___texWidth_13)); }
	inline int32_t get_texWidth_13() const { return ___texWidth_13; }
	inline int32_t* get_address_of_texWidth_13() { return &___texWidth_13; }
	inline void set_texWidth_13(int32_t value)
	{
		___texWidth_13 = value;
	}

	inline static int32_t get_offset_of_texHeight_14() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___texHeight_14)); }
	inline int32_t get_texHeight_14() const { return ___texHeight_14; }
	inline int32_t* get_address_of_texHeight_14() { return &___texHeight_14; }
	inline void set_texHeight_14(int32_t value)
	{
		___texHeight_14 = value;
	}

	inline static int32_t get_offset_of_lastBufferedPosition_15() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___lastBufferedPosition_15)); }
	inline int64_t get_lastBufferedPosition_15() const { return ___lastBufferedPosition_15; }
	inline int64_t* get_address_of_lastBufferedPosition_15() { return &___lastBufferedPosition_15; }
	inline void set_lastBufferedPosition_15(int64_t value)
	{
		___lastBufferedPosition_15 = value;
	}

	inline static int32_t get_offset_of_framecount_16() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___framecount_16)); }
	inline float get_framecount_16() const { return ___framecount_16; }
	inline float* get_address_of_framecount_16() { return &___framecount_16; }
	inline void set_framecount_16(float value)
	{
		___framecount_16 = value;
	}

	inline static int32_t get_offset_of_screen_17() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___screen_17)); }
	inline Renderer_t2627027031 * get_screen_17() const { return ___screen_17; }
	inline Renderer_t2627027031 ** get_address_of_screen_17() { return &___screen_17; }
	inline void set_screen_17(Renderer_t2627027031 * value)
	{
		___screen_17 = value;
		Il2CppCodeGenWriteBarrier((&___screen_17), value);
	}

	inline static int32_t get_offset_of_renderEventFunction_18() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___renderEventFunction_18)); }
	inline intptr_t get_renderEventFunction_18() const { return ___renderEventFunction_18; }
	inline intptr_t* get_address_of_renderEventFunction_18() { return &___renderEventFunction_18; }
	inline void set_renderEventFunction_18(intptr_t value)
	{
		___renderEventFunction_18 = value;
	}

	inline static int32_t get_offset_of_playOnResume_19() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___playOnResume_19)); }
	inline bool get_playOnResume_19() const { return ___playOnResume_19; }
	inline bool* get_address_of_playOnResume_19() { return &___playOnResume_19; }
	inline void set_playOnResume_19(bool value)
	{
		___playOnResume_19 = value;
	}

	inline static int32_t get_offset_of_onEventCallbacks_20() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___onEventCallbacks_20)); }
	inline List_1_t300520794 * get_onEventCallbacks_20() const { return ___onEventCallbacks_20; }
	inline List_1_t300520794 ** get_address_of_onEventCallbacks_20() { return &___onEventCallbacks_20; }
	inline void set_onEventCallbacks_20(List_1_t300520794 * value)
	{
		___onEventCallbacks_20 = value;
		Il2CppCodeGenWriteBarrier((&___onEventCallbacks_20), value);
	}

	inline static int32_t get_offset_of_onExceptionCallbacks_21() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___onExceptionCallbacks_21)); }
	inline List_1_t1147278120 * get_onExceptionCallbacks_21() const { return ___onExceptionCallbacks_21; }
	inline List_1_t1147278120 ** get_address_of_onExceptionCallbacks_21() { return &___onExceptionCallbacks_21; }
	inline void set_onExceptionCallbacks_21(List_1_t1147278120 * value)
	{
		___onExceptionCallbacks_21 = value;
		Il2CppCodeGenWriteBarrier((&___onExceptionCallbacks_21), value);
	}

	inline static int32_t get_offset_of_statusText_23() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___statusText_23)); }
	inline Text_t1901882714 * get_statusText_23() const { return ___statusText_23; }
	inline Text_t1901882714 ** get_address_of_statusText_23() { return &___statusText_23; }
	inline void set_statusText_23(Text_t1901882714 * value)
	{
		___statusText_23 = value;
		Il2CppCodeGenWriteBarrier((&___statusText_23), value);
	}

	inline static int32_t get_offset_of_videoType_24() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoType_24)); }
	inline int32_t get_videoType_24() const { return ___videoType_24; }
	inline int32_t* get_address_of_videoType_24() { return &___videoType_24; }
	inline void set_videoType_24(int32_t value)
	{
		___videoType_24 = value;
	}

	inline static int32_t get_offset_of_videoURL_25() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoURL_25)); }
	inline String_t* get_videoURL_25() const { return ___videoURL_25; }
	inline String_t** get_address_of_videoURL_25() { return &___videoURL_25; }
	inline void set_videoURL_25(String_t* value)
	{
		___videoURL_25 = value;
		Il2CppCodeGenWriteBarrier((&___videoURL_25), value);
	}

	inline static int32_t get_offset_of_videoContentID_26() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoContentID_26)); }
	inline String_t* get_videoContentID_26() const { return ___videoContentID_26; }
	inline String_t** get_address_of_videoContentID_26() { return &___videoContentID_26; }
	inline void set_videoContentID_26(String_t* value)
	{
		___videoContentID_26 = value;
		Il2CppCodeGenWriteBarrier((&___videoContentID_26), value);
	}

	inline static int32_t get_offset_of_videoProviderId_27() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___videoProviderId_27)); }
	inline String_t* get_videoProviderId_27() const { return ___videoProviderId_27; }
	inline String_t** get_address_of_videoProviderId_27() { return &___videoProviderId_27; }
	inline void set_videoProviderId_27(String_t* value)
	{
		___videoProviderId_27 = value;
		Il2CppCodeGenWriteBarrier((&___videoProviderId_27), value);
	}

	inline static int32_t get_offset_of_initialResolution_28() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___initialResolution_28)); }
	inline int32_t get_initialResolution_28() const { return ___initialResolution_28; }
	inline int32_t* get_address_of_initialResolution_28() { return &___initialResolution_28; }
	inline void set_initialResolution_28(int32_t value)
	{
		___initialResolution_28 = value;
	}

	inline static int32_t get_offset_of_adjustAspectRatio_29() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___adjustAspectRatio_29)); }
	inline bool get_adjustAspectRatio_29() const { return ___adjustAspectRatio_29; }
	inline bool* get_address_of_adjustAspectRatio_29() { return &___adjustAspectRatio_29; }
	inline void set_adjustAspectRatio_29(bool value)
	{
		___adjustAspectRatio_29 = value;
	}

	inline static int32_t get_offset_of_useSecurePath_30() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735, ___useSecurePath_30)); }
	inline bool get_useSecurePath_30() const { return ___useSecurePath_30; }
	inline bool* get_address_of_useSecurePath_30() { return &___useSecurePath_30; }
	inline void set_useSecurePath_30(bool value)
	{
		___useSecurePath_30 = value;
	}
};

struct GvrVideoPlayerTexture_t3546202735_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<System.Action> GvrVideoPlayerTexture::ExecuteOnMainThread
	Queue_1_t1110636971 * ___ExecuteOnMainThread_22;
	// System.Action`2<System.String,System.String> GvrVideoPlayerTexture::<>f__am$cache0
	Action_2_t3970170674 * ___U3CU3Ef__amU24cache0_32;
	// GvrVideoPlayerTexture/OnVideoEventCallback GvrVideoPlayerTexture::<>f__mg$cache0
	OnVideoEventCallback_t2376626694 * ___U3CU3Ef__mgU24cache0_33;
	// GvrVideoPlayerTexture/OnExceptionCallback GvrVideoPlayerTexture::<>f__mg$cache1
	OnExceptionCallback_t1696428116 * ___U3CU3Ef__mgU24cache1_34;

public:
	inline static int32_t get_offset_of_ExecuteOnMainThread_22() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735_StaticFields, ___ExecuteOnMainThread_22)); }
	inline Queue_1_t1110636971 * get_ExecuteOnMainThread_22() const { return ___ExecuteOnMainThread_22; }
	inline Queue_1_t1110636971 ** get_address_of_ExecuteOnMainThread_22() { return &___ExecuteOnMainThread_22; }
	inline void set_ExecuteOnMainThread_22(Queue_1_t1110636971 * value)
	{
		___ExecuteOnMainThread_22 = value;
		Il2CppCodeGenWriteBarrier((&___ExecuteOnMainThread_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_32() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735_StaticFields, ___U3CU3Ef__amU24cache0_32)); }
	inline Action_2_t3970170674 * get_U3CU3Ef__amU24cache0_32() const { return ___U3CU3Ef__amU24cache0_32; }
	inline Action_2_t3970170674 ** get_address_of_U3CU3Ef__amU24cache0_32() { return &___U3CU3Ef__amU24cache0_32; }
	inline void set_U3CU3Ef__amU24cache0_32(Action_2_t3970170674 * value)
	{
		___U3CU3Ef__amU24cache0_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_33() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735_StaticFields, ___U3CU3Ef__mgU24cache0_33)); }
	inline OnVideoEventCallback_t2376626694 * get_U3CU3Ef__mgU24cache0_33() const { return ___U3CU3Ef__mgU24cache0_33; }
	inline OnVideoEventCallback_t2376626694 ** get_address_of_U3CU3Ef__mgU24cache0_33() { return &___U3CU3Ef__mgU24cache0_33; }
	inline void set_U3CU3Ef__mgU24cache0_33(OnVideoEventCallback_t2376626694 * value)
	{
		___U3CU3Ef__mgU24cache0_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_34() { return static_cast<int32_t>(offsetof(GvrVideoPlayerTexture_t3546202735_StaticFields, ___U3CU3Ef__mgU24cache1_34)); }
	inline OnExceptionCallback_t1696428116 * get_U3CU3Ef__mgU24cache1_34() const { return ___U3CU3Ef__mgU24cache1_34; }
	inline OnExceptionCallback_t1696428116 ** get_address_of_U3CU3Ef__mgU24cache1_34() { return &___U3CU3Ef__mgU24cache1_34; }
	inline void set_U3CU3Ef__mgU24cache1_34(OnExceptionCallback_t1696428116 * value)
	{
		___U3CU3Ef__mgU24cache1_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRVIDEOPLAYERTEXTURE_T3546202735_H
#ifndef INSTANTPREVIEWHELPER_T1993029064_H
#define INSTANTPREVIEWHELPER_T1993029064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantPreviewHelper
struct  InstantPreviewHelper_t1993029064  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct InstantPreviewHelper_t1993029064_StaticFields
{
public:
	// System.String InstantPreviewHelper::AdbPath
	String_t* ___AdbPath_4;

public:
	inline static int32_t get_offset_of_AdbPath_4() { return static_cast<int32_t>(offsetof(InstantPreviewHelper_t1993029064_StaticFields, ___AdbPath_4)); }
	inline String_t* get_AdbPath_4() const { return ___AdbPath_4; }
	inline String_t** get_address_of_AdbPath_4() { return &___AdbPath_4; }
	inline void set_AdbPath_4(String_t* value)
	{
		___AdbPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___AdbPath_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTPREVIEWHELPER_T1993029064_H
#ifndef MIRRORCAMERASCRIPT_T1086824577_H
#define MIRRORCAMERASCRIPT_T1086824577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirrorCameraScript
struct  MirrorCameraScript_t1086824577  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MirrorCameraScript::MirrorObject
	GameObject_t1113636619 * ___MirrorObject_4;
	// System.Boolean MirrorCameraScript::VRMode
	bool ___VRMode_5;
	// UnityEngine.Renderer MirrorCameraScript::mirrorRenderer
	Renderer_t2627027031 * ___mirrorRenderer_6;
	// UnityEngine.Material MirrorCameraScript::mirrorMaterial
	Material_t340375123 * ___mirrorMaterial_7;
	// MirrorScript MirrorCameraScript::mirrorScript
	MirrorScript_t2344066790 * ___mirrorScript_8;
	// UnityEngine.Camera MirrorCameraScript::cameraObject
	Camera_t4157153871 * ___cameraObject_9;
	// UnityEngine.RenderTexture MirrorCameraScript::reflectionTexture
	RenderTexture_t2108887433 * ___reflectionTexture_10;
	// UnityEngine.Matrix4x4 MirrorCameraScript::reflectionMatrix
	Matrix4x4_t1817901843  ___reflectionMatrix_11;
	// System.Int32 MirrorCameraScript::oldReflectionTextureSize
	int32_t ___oldReflectionTextureSize_12;

public:
	inline static int32_t get_offset_of_MirrorObject_4() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___MirrorObject_4)); }
	inline GameObject_t1113636619 * get_MirrorObject_4() const { return ___MirrorObject_4; }
	inline GameObject_t1113636619 ** get_address_of_MirrorObject_4() { return &___MirrorObject_4; }
	inline void set_MirrorObject_4(GameObject_t1113636619 * value)
	{
		___MirrorObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___MirrorObject_4), value);
	}

	inline static int32_t get_offset_of_VRMode_5() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___VRMode_5)); }
	inline bool get_VRMode_5() const { return ___VRMode_5; }
	inline bool* get_address_of_VRMode_5() { return &___VRMode_5; }
	inline void set_VRMode_5(bool value)
	{
		___VRMode_5 = value;
	}

	inline static int32_t get_offset_of_mirrorRenderer_6() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___mirrorRenderer_6)); }
	inline Renderer_t2627027031 * get_mirrorRenderer_6() const { return ___mirrorRenderer_6; }
	inline Renderer_t2627027031 ** get_address_of_mirrorRenderer_6() { return &___mirrorRenderer_6; }
	inline void set_mirrorRenderer_6(Renderer_t2627027031 * value)
	{
		___mirrorRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___mirrorRenderer_6), value);
	}

	inline static int32_t get_offset_of_mirrorMaterial_7() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___mirrorMaterial_7)); }
	inline Material_t340375123 * get_mirrorMaterial_7() const { return ___mirrorMaterial_7; }
	inline Material_t340375123 ** get_address_of_mirrorMaterial_7() { return &___mirrorMaterial_7; }
	inline void set_mirrorMaterial_7(Material_t340375123 * value)
	{
		___mirrorMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___mirrorMaterial_7), value);
	}

	inline static int32_t get_offset_of_mirrorScript_8() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___mirrorScript_8)); }
	inline MirrorScript_t2344066790 * get_mirrorScript_8() const { return ___mirrorScript_8; }
	inline MirrorScript_t2344066790 ** get_address_of_mirrorScript_8() { return &___mirrorScript_8; }
	inline void set_mirrorScript_8(MirrorScript_t2344066790 * value)
	{
		___mirrorScript_8 = value;
		Il2CppCodeGenWriteBarrier((&___mirrorScript_8), value);
	}

	inline static int32_t get_offset_of_cameraObject_9() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___cameraObject_9)); }
	inline Camera_t4157153871 * get_cameraObject_9() const { return ___cameraObject_9; }
	inline Camera_t4157153871 ** get_address_of_cameraObject_9() { return &___cameraObject_9; }
	inline void set_cameraObject_9(Camera_t4157153871 * value)
	{
		___cameraObject_9 = value;
		Il2CppCodeGenWriteBarrier((&___cameraObject_9), value);
	}

	inline static int32_t get_offset_of_reflectionTexture_10() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___reflectionTexture_10)); }
	inline RenderTexture_t2108887433 * get_reflectionTexture_10() const { return ___reflectionTexture_10; }
	inline RenderTexture_t2108887433 ** get_address_of_reflectionTexture_10() { return &___reflectionTexture_10; }
	inline void set_reflectionTexture_10(RenderTexture_t2108887433 * value)
	{
		___reflectionTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___reflectionTexture_10), value);
	}

	inline static int32_t get_offset_of_reflectionMatrix_11() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___reflectionMatrix_11)); }
	inline Matrix4x4_t1817901843  get_reflectionMatrix_11() const { return ___reflectionMatrix_11; }
	inline Matrix4x4_t1817901843 * get_address_of_reflectionMatrix_11() { return &___reflectionMatrix_11; }
	inline void set_reflectionMatrix_11(Matrix4x4_t1817901843  value)
	{
		___reflectionMatrix_11 = value;
	}

	inline static int32_t get_offset_of_oldReflectionTextureSize_12() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577, ___oldReflectionTextureSize_12)); }
	inline int32_t get_oldReflectionTextureSize_12() const { return ___oldReflectionTextureSize_12; }
	inline int32_t* get_address_of_oldReflectionTextureSize_12() { return &___oldReflectionTextureSize_12; }
	inline void set_oldReflectionTextureSize_12(int32_t value)
	{
		___oldReflectionTextureSize_12 = value;
	}
};

struct MirrorCameraScript_t1086824577_StaticFields
{
public:
	// System.Boolean MirrorCameraScript::renderingMirror
	bool ___renderingMirror_13;

public:
	inline static int32_t get_offset_of_renderingMirror_13() { return static_cast<int32_t>(offsetof(MirrorCameraScript_t1086824577_StaticFields, ___renderingMirror_13)); }
	inline bool get_renderingMirror_13() const { return ___renderingMirror_13; }
	inline bool* get_address_of_renderingMirror_13() { return &___renderingMirror_13; }
	inline void set_renderingMirror_13(bool value)
	{
		___renderingMirror_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRRORCAMERASCRIPT_T1086824577_H
#ifndef MIRRORREFLECTIONSCRIPT_T605660690_H
#define MIRRORREFLECTIONSCRIPT_T605660690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirrorReflectionScript
struct  MirrorReflectionScript_t605660690  : public MonoBehaviour_t3962482529
{
public:
	// MirrorCameraScript MirrorReflectionScript::childScript
	MirrorCameraScript_t1086824577 * ___childScript_4;

public:
	inline static int32_t get_offset_of_childScript_4() { return static_cast<int32_t>(offsetof(MirrorReflectionScript_t605660690, ___childScript_4)); }
	inline MirrorCameraScript_t1086824577 * get_childScript_4() const { return ___childScript_4; }
	inline MirrorCameraScript_t1086824577 ** get_address_of_childScript_4() { return &___childScript_4; }
	inline void set_childScript_4(MirrorCameraScript_t1086824577 * value)
	{
		___childScript_4 = value;
		Il2CppCodeGenWriteBarrier((&___childScript_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRRORREFLECTIONSCRIPT_T605660690_H
#ifndef MIRRORSCRIPT_T2344066790_H
#define MIRRORSCRIPT_T2344066790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MirrorScript
struct  MirrorScript_t2344066790  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 MirrorScript::MaximumPerPixelLights
	int32_t ___MaximumPerPixelLights_4;
	// System.Int32 MirrorScript::TextureSize
	int32_t ___TextureSize_5;
	// System.Single MirrorScript::ClipPlaneOffset
	float ___ClipPlaneOffset_6;
	// System.Single MirrorScript::FarClipPlane
	float ___FarClipPlane_7;
	// UnityEngine.LayerMask MirrorScript::ReflectLayers
	LayerMask_t3493934918  ___ReflectLayers_8;
	// System.Boolean MirrorScript::AddFlareLayer
	bool ___AddFlareLayer_9;
	// System.Boolean MirrorScript::NormalIsForward
	bool ___NormalIsForward_10;
	// System.Single MirrorScript::AspectRatio
	float ___AspectRatio_11;
	// System.Boolean MirrorScript::MirrorRecursion
	bool ___MirrorRecursion_12;

public:
	inline static int32_t get_offset_of_MaximumPerPixelLights_4() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___MaximumPerPixelLights_4)); }
	inline int32_t get_MaximumPerPixelLights_4() const { return ___MaximumPerPixelLights_4; }
	inline int32_t* get_address_of_MaximumPerPixelLights_4() { return &___MaximumPerPixelLights_4; }
	inline void set_MaximumPerPixelLights_4(int32_t value)
	{
		___MaximumPerPixelLights_4 = value;
	}

	inline static int32_t get_offset_of_TextureSize_5() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___TextureSize_5)); }
	inline int32_t get_TextureSize_5() const { return ___TextureSize_5; }
	inline int32_t* get_address_of_TextureSize_5() { return &___TextureSize_5; }
	inline void set_TextureSize_5(int32_t value)
	{
		___TextureSize_5 = value;
	}

	inline static int32_t get_offset_of_ClipPlaneOffset_6() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___ClipPlaneOffset_6)); }
	inline float get_ClipPlaneOffset_6() const { return ___ClipPlaneOffset_6; }
	inline float* get_address_of_ClipPlaneOffset_6() { return &___ClipPlaneOffset_6; }
	inline void set_ClipPlaneOffset_6(float value)
	{
		___ClipPlaneOffset_6 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_7() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___FarClipPlane_7)); }
	inline float get_FarClipPlane_7() const { return ___FarClipPlane_7; }
	inline float* get_address_of_FarClipPlane_7() { return &___FarClipPlane_7; }
	inline void set_FarClipPlane_7(float value)
	{
		___FarClipPlane_7 = value;
	}

	inline static int32_t get_offset_of_ReflectLayers_8() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___ReflectLayers_8)); }
	inline LayerMask_t3493934918  get_ReflectLayers_8() const { return ___ReflectLayers_8; }
	inline LayerMask_t3493934918 * get_address_of_ReflectLayers_8() { return &___ReflectLayers_8; }
	inline void set_ReflectLayers_8(LayerMask_t3493934918  value)
	{
		___ReflectLayers_8 = value;
	}

	inline static int32_t get_offset_of_AddFlareLayer_9() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___AddFlareLayer_9)); }
	inline bool get_AddFlareLayer_9() const { return ___AddFlareLayer_9; }
	inline bool* get_address_of_AddFlareLayer_9() { return &___AddFlareLayer_9; }
	inline void set_AddFlareLayer_9(bool value)
	{
		___AddFlareLayer_9 = value;
	}

	inline static int32_t get_offset_of_NormalIsForward_10() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___NormalIsForward_10)); }
	inline bool get_NormalIsForward_10() const { return ___NormalIsForward_10; }
	inline bool* get_address_of_NormalIsForward_10() { return &___NormalIsForward_10; }
	inline void set_NormalIsForward_10(bool value)
	{
		___NormalIsForward_10 = value;
	}

	inline static int32_t get_offset_of_AspectRatio_11() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___AspectRatio_11)); }
	inline float get_AspectRatio_11() const { return ___AspectRatio_11; }
	inline float* get_address_of_AspectRatio_11() { return &___AspectRatio_11; }
	inline void set_AspectRatio_11(float value)
	{
		___AspectRatio_11 = value;
	}

	inline static int32_t get_offset_of_MirrorRecursion_12() { return static_cast<int32_t>(offsetof(MirrorScript_t2344066790, ___MirrorRecursion_12)); }
	inline bool get_MirrorRecursion_12() const { return ___MirrorRecursion_12; }
	inline bool* get_address_of_MirrorRecursion_12() { return &___MirrorRecursion_12; }
	inline void set_MirrorRecursion_12(bool value)
	{
		___MirrorRecursion_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIRRORSCRIPT_T2344066790_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef SELECTABLE_T3250028441_H
#define SELECTABLE_T3250028441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t3250028441  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t3049316579  ___m_Navigation_5;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_6;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2139031574  ___m_Colors_7;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1362986479  ___m_SpriteState_8;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t2532145056 * ___m_AnimationTriggers_9;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_10;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t1660335611 * ___m_TargetGraphic_11;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_12;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_13;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_14;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_15;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_16;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t1260619206 * ___m_CanvasGroupCache_17;

public:
	inline static int32_t get_offset_of_m_Navigation_5() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Navigation_5)); }
	inline Navigation_t3049316579  get_m_Navigation_5() const { return ___m_Navigation_5; }
	inline Navigation_t3049316579 * get_address_of_m_Navigation_5() { return &___m_Navigation_5; }
	inline void set_m_Navigation_5(Navigation_t3049316579  value)
	{
		___m_Navigation_5 = value;
	}

	inline static int32_t get_offset_of_m_Transition_6() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Transition_6)); }
	inline int32_t get_m_Transition_6() const { return ___m_Transition_6; }
	inline int32_t* get_address_of_m_Transition_6() { return &___m_Transition_6; }
	inline void set_m_Transition_6(int32_t value)
	{
		___m_Transition_6 = value;
	}

	inline static int32_t get_offset_of_m_Colors_7() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Colors_7)); }
	inline ColorBlock_t2139031574  get_m_Colors_7() const { return ___m_Colors_7; }
	inline ColorBlock_t2139031574 * get_address_of_m_Colors_7() { return &___m_Colors_7; }
	inline void set_m_Colors_7(ColorBlock_t2139031574  value)
	{
		___m_Colors_7 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_8() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_SpriteState_8)); }
	inline SpriteState_t1362986479  get_m_SpriteState_8() const { return ___m_SpriteState_8; }
	inline SpriteState_t1362986479 * get_address_of_m_SpriteState_8() { return &___m_SpriteState_8; }
	inline void set_m_SpriteState_8(SpriteState_t1362986479  value)
	{
		___m_SpriteState_8 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_9() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_AnimationTriggers_9)); }
	inline AnimationTriggers_t2532145056 * get_m_AnimationTriggers_9() const { return ___m_AnimationTriggers_9; }
	inline AnimationTriggers_t2532145056 ** get_address_of_m_AnimationTriggers_9() { return &___m_AnimationTriggers_9; }
	inline void set_m_AnimationTriggers_9(AnimationTriggers_t2532145056 * value)
	{
		___m_AnimationTriggers_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_9), value);
	}

	inline static int32_t get_offset_of_m_Interactable_10() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_Interactable_10)); }
	inline bool get_m_Interactable_10() const { return ___m_Interactable_10; }
	inline bool* get_address_of_m_Interactable_10() { return &___m_Interactable_10; }
	inline void set_m_Interactable_10(bool value)
	{
		___m_Interactable_10 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_11() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_TargetGraphic_11)); }
	inline Graphic_t1660335611 * get_m_TargetGraphic_11() const { return ___m_TargetGraphic_11; }
	inline Graphic_t1660335611 ** get_address_of_m_TargetGraphic_11() { return &___m_TargetGraphic_11; }
	inline void set_m_TargetGraphic_11(Graphic_t1660335611 * value)
	{
		___m_TargetGraphic_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_11), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_12() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_GroupsAllowInteraction_12)); }
	inline bool get_m_GroupsAllowInteraction_12() const { return ___m_GroupsAllowInteraction_12; }
	inline bool* get_address_of_m_GroupsAllowInteraction_12() { return &___m_GroupsAllowInteraction_12; }
	inline void set_m_GroupsAllowInteraction_12(bool value)
	{
		___m_GroupsAllowInteraction_12 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_13() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CurrentSelectionState_13)); }
	inline int32_t get_m_CurrentSelectionState_13() const { return ___m_CurrentSelectionState_13; }
	inline int32_t* get_address_of_m_CurrentSelectionState_13() { return &___m_CurrentSelectionState_13; }
	inline void set_m_CurrentSelectionState_13(int32_t value)
	{
		___m_CurrentSelectionState_13 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerInsideU3Ek__BackingField_14)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_14() const { return ___U3CisPointerInsideU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_14() { return &___U3CisPointerInsideU3Ek__BackingField_14; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_14(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3CisPointerDownU3Ek__BackingField_15)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_15() const { return ___U3CisPointerDownU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_15() { return &___U3CisPointerDownU3Ek__BackingField_15; }
	inline void set_U3CisPointerDownU3Ek__BackingField_15(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___U3ChasSelectionU3Ek__BackingField_16)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_16() const { return ___U3ChasSelectionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_16() { return &___U3ChasSelectionU3Ek__BackingField_16; }
	inline void set_U3ChasSelectionU3Ek__BackingField_16(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_17() { return static_cast<int32_t>(offsetof(Selectable_t3250028441, ___m_CanvasGroupCache_17)); }
	inline List_1_t1260619206 * get_m_CanvasGroupCache_17() const { return ___m_CanvasGroupCache_17; }
	inline List_1_t1260619206 ** get_address_of_m_CanvasGroupCache_17() { return &___m_CanvasGroupCache_17; }
	inline void set_m_CanvasGroupCache_17(List_1_t1260619206 * value)
	{
		___m_CanvasGroupCache_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_17), value);
	}
};

struct Selectable_t3250028441_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t427135887 * ___s_List_4;

public:
	inline static int32_t get_offset_of_s_List_4() { return static_cast<int32_t>(offsetof(Selectable_t3250028441_StaticFields, ___s_List_4)); }
	inline List_1_t427135887 * get_s_List_4() const { return ___s_List_4; }
	inline List_1_t427135887 ** get_address_of_s_List_4() { return &___s_List_4; }
	inline void set_s_List_4(List_1_t427135887 * value)
	{
		___s_List_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T3250028441_H
#ifndef DROPDOWN_T2274391225_H
#define DROPDOWN_T2274391225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Dropdown
struct  Dropdown_t2274391225  : public Selectable_t3250028441
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t3704657025 * ___m_Template_18;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_t1901882714 * ___m_CaptionText_19;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t2670269651 * ___m_CaptionImage_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_t1901882714 * ___m_ItemText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t2670269651 * ___m_ItemImage_22;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_23;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t1438173104 * ___m_Options_24;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t4040729994 * ___m_OnValueChanged_25;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_t1113636619 * ___m_Dropdown_26;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_t1113636619 * ___m_Blocker_27;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t2924027637 * ___m_Items_28;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t3520241082 * ___m_AlphaTweenRunner_29;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_30;

public:
	inline static int32_t get_offset_of_m_Template_18() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Template_18)); }
	inline RectTransform_t3704657025 * get_m_Template_18() const { return ___m_Template_18; }
	inline RectTransform_t3704657025 ** get_address_of_m_Template_18() { return &___m_Template_18; }
	inline void set_m_Template_18(RectTransform_t3704657025 * value)
	{
		___m_Template_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Template_18), value);
	}

	inline static int32_t get_offset_of_m_CaptionText_19() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionText_19)); }
	inline Text_t1901882714 * get_m_CaptionText_19() const { return ___m_CaptionText_19; }
	inline Text_t1901882714 ** get_address_of_m_CaptionText_19() { return &___m_CaptionText_19; }
	inline void set_m_CaptionText_19(Text_t1901882714 * value)
	{
		___m_CaptionText_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionText_19), value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_20() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_CaptionImage_20)); }
	inline Image_t2670269651 * get_m_CaptionImage_20() const { return ___m_CaptionImage_20; }
	inline Image_t2670269651 ** get_address_of_m_CaptionImage_20() { return &___m_CaptionImage_20; }
	inline void set_m_CaptionImage_20(Image_t2670269651 * value)
	{
		___m_CaptionImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionImage_20), value);
	}

	inline static int32_t get_offset_of_m_ItemText_21() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemText_21)); }
	inline Text_t1901882714 * get_m_ItemText_21() const { return ___m_ItemText_21; }
	inline Text_t1901882714 ** get_address_of_m_ItemText_21() { return &___m_ItemText_21; }
	inline void set_m_ItemText_21(Text_t1901882714 * value)
	{
		___m_ItemText_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemText_21), value);
	}

	inline static int32_t get_offset_of_m_ItemImage_22() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_ItemImage_22)); }
	inline Image_t2670269651 * get_m_ItemImage_22() const { return ___m_ItemImage_22; }
	inline Image_t2670269651 ** get_address_of_m_ItemImage_22() { return &___m_ItemImage_22; }
	inline void set_m_ItemImage_22(Image_t2670269651 * value)
	{
		___m_ItemImage_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemImage_22), value);
	}

	inline static int32_t get_offset_of_m_Value_23() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Value_23)); }
	inline int32_t get_m_Value_23() const { return ___m_Value_23; }
	inline int32_t* get_address_of_m_Value_23() { return &___m_Value_23; }
	inline void set_m_Value_23(int32_t value)
	{
		___m_Value_23 = value;
	}

	inline static int32_t get_offset_of_m_Options_24() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Options_24)); }
	inline OptionDataList_t1438173104 * get_m_Options_24() const { return ___m_Options_24; }
	inline OptionDataList_t1438173104 ** get_address_of_m_Options_24() { return &___m_Options_24; }
	inline void set_m_Options_24(OptionDataList_t1438173104 * value)
	{
		___m_Options_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_24), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_25() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_OnValueChanged_25)); }
	inline DropdownEvent_t4040729994 * get_m_OnValueChanged_25() const { return ___m_OnValueChanged_25; }
	inline DropdownEvent_t4040729994 ** get_address_of_m_OnValueChanged_25() { return &___m_OnValueChanged_25; }
	inline void set_m_OnValueChanged_25(DropdownEvent_t4040729994 * value)
	{
		___m_OnValueChanged_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_25), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_26() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Dropdown_26)); }
	inline GameObject_t1113636619 * get_m_Dropdown_26() const { return ___m_Dropdown_26; }
	inline GameObject_t1113636619 ** get_address_of_m_Dropdown_26() { return &___m_Dropdown_26; }
	inline void set_m_Dropdown_26(GameObject_t1113636619 * value)
	{
		___m_Dropdown_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_26), value);
	}

	inline static int32_t get_offset_of_m_Blocker_27() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Blocker_27)); }
	inline GameObject_t1113636619 * get_m_Blocker_27() const { return ___m_Blocker_27; }
	inline GameObject_t1113636619 ** get_address_of_m_Blocker_27() { return &___m_Blocker_27; }
	inline void set_m_Blocker_27(GameObject_t1113636619 * value)
	{
		___m_Blocker_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blocker_27), value);
	}

	inline static int32_t get_offset_of_m_Items_28() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_Items_28)); }
	inline List_1_t2924027637 * get_m_Items_28() const { return ___m_Items_28; }
	inline List_1_t2924027637 ** get_address_of_m_Items_28() { return &___m_Items_28; }
	inline void set_m_Items_28(List_1_t2924027637 * value)
	{
		___m_Items_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Items_28), value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_29() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___m_AlphaTweenRunner_29)); }
	inline TweenRunner_1_t3520241082 * get_m_AlphaTweenRunner_29() const { return ___m_AlphaTweenRunner_29; }
	inline TweenRunner_1_t3520241082 ** get_address_of_m_AlphaTweenRunner_29() { return &___m_AlphaTweenRunner_29; }
	inline void set_m_AlphaTweenRunner_29(TweenRunner_1_t3520241082 * value)
	{
		___m_AlphaTweenRunner_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTweenRunner_29), value);
	}

	inline static int32_t get_offset_of_validTemplate_30() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225, ___validTemplate_30)); }
	inline bool get_validTemplate_30() const { return ___validTemplate_30; }
	inline bool* get_address_of_validTemplate_30() { return &___validTemplate_30; }
	inline void set_validTemplate_30(bool value)
	{
		___validTemplate_30 = value;
	}
};

struct Dropdown_t2274391225_StaticFields
{
public:
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t3270282352 * ___s_NoOptionData_31;

public:
	inline static int32_t get_offset_of_s_NoOptionData_31() { return static_cast<int32_t>(offsetof(Dropdown_t2274391225_StaticFields, ___s_NoOptionData_31)); }
	inline OptionData_t3270282352 * get_s_NoOptionData_31() const { return ___s_NoOptionData_31; }
	inline OptionData_t3270282352 ** get_address_of_s_NoOptionData_31() { return &___s_NoOptionData_31; }
	inline void set_s_NoOptionData_31(OptionData_t3270282352 * value)
	{
		___s_NoOptionData_31 = value;
		Il2CppCodeGenWriteBarrier((&___s_NoOptionData_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWN_T2274391225_H
#ifndef GVRDROPDOWN_T2772907210_H
#define GVRDROPDOWN_T2772907210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GvrDropdown
struct  GvrDropdown_t2772907210  : public Dropdown_t2274391225
{
public:
	// UnityEngine.GameObject GvrDropdown::currentBlocker
	GameObject_t1113636619 * ___currentBlocker_32;

public:
	inline static int32_t get_offset_of_currentBlocker_32() { return static_cast<int32_t>(offsetof(GvrDropdown_t2772907210, ___currentBlocker_32)); }
	inline GameObject_t1113636619 * get_currentBlocker_32() const { return ___currentBlocker_32; }
	inline GameObject_t1113636619 ** get_address_of_currentBlocker_32() { return &___currentBlocker_32; }
	inline void set_currentBlocker_32(GameObject_t1113636619 * value)
	{
		___currentBlocker_32 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlocker_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GVRDROPDOWN_T2772907210_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t1803228010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[4] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t1803228010::get_offset_of_U24this_0(),
	U3CEndOfFrameU3Ec__Iterator0_t1803228010::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator0_t1803228010::get_offset_of_U24disposing_2(),
	U3CEndOfFrameU3Ec__Iterator0_t1803228010::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (GvrEventType_t1628138291)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2501[5] = 
{
	GvrEventType_t1628138291::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (GvrRecenterEventType_t513699134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2502[4] = 
{
	GvrRecenterEventType_t513699134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (GvrRecenterFlags_t370890970)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[2] = 
{
	GvrRecenterFlags_t370890970::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (GvrErrorType_t1921600730)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2504[6] = 
{
	GvrErrorType_t1921600730::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (GvrSafetyRegionType_t1943469016)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2505[3] = 
{
	GvrSafetyRegionType_t1943469016::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (HeadsetProviderFactory_t520764709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (gvr_feature_t1054564930)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[2] = 
{
	gvr_feature_t1054564930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (gvr_property_type_t1812416635)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2508[6] = 
{
	gvr_property_type_t1812416635::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (gvr_value_type_t2156477760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2509[15] = 
{
	gvr_value_type_t2156477760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (gvr_recenter_flags_t2414511954)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2510[2] = 
{
	gvr_recenter_flags_t2414511954::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (DummyHeadsetProvider_t3229995161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	DummyHeadsetProvider_t3229995161::get_offset_of_dummyState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (EditorHeadsetProvider_t660777464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[1] = 
{
	EditorHeadsetProvider_t660777464::get_offset_of_dummyState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (HeadsetState_t378905490)+ sizeof (RuntimeObject), sizeof(HeadsetState_t378905490 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2513[7] = 
{
	HeadsetState_t378905490::get_offset_of_eventType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_eventFlags_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_eventTimestampNs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_recenterEventType_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_recenterEventFlags_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_recenteredPosition_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	HeadsetState_t378905490::get_offset_of_recenteredRotation_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (InstantPreview_t778898416), -1, sizeof(InstantPreview_t778898416_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2515[8] = 
{
	0,
	InstantPreview_t778898416_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
	0,
	InstantPreview_t778898416::get_offset_of_OutputResolution_7(),
	InstantPreview_t778898416::get_offset_of_MultisampleCount_8(),
	InstantPreview_t778898416::get_offset_of_BitRate_9(),
	InstantPreview_t778898416::get_offset_of_InstallApkOnRun_10(),
	InstantPreview_t778898416::get_offset_of_InstantPreviewApk_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (Resolutions_t3321708501)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2516[4] = 
{
	Resolutions_t3321708501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (ResolutionSize_t3677657137)+ sizeof (RuntimeObject), sizeof(ResolutionSize_t3677657137 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2517[2] = 
{
	ResolutionSize_t3677657137::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ResolutionSize_t3677657137::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (MultisampleCounts_t552109702)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2518[5] = 
{
	MultisampleCounts_t552109702::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (BitRates_t2681405699)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2519[7] = 
{
	BitRates_t2681405699::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (UnityRect_t2898233164)+ sizeof (RuntimeObject), sizeof(UnityRect_t2898233164 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2520[4] = 
{
	UnityRect_t2898233164::get_offset_of_right_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t2898233164::get_offset_of_left_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t2898233164::get_offset_of_top_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityRect_t2898233164::get_offset_of_bottom_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (UnityEyeViews_t678228735)+ sizeof (RuntimeObject), sizeof(UnityEyeViews_t678228735 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2521[4] = 
{
	UnityEyeViews_t678228735::get_offset_of_leftEyePose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t678228735::get_offset_of_rightEyePose_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t678228735::get_offset_of_leftEyeViewSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnityEyeViews_t678228735::get_offset_of_rightEyeViewSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (InstantPreviewHelper_t1993029064), -1, sizeof(InstantPreviewHelper_t1993029064_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2522[1] = 
{
	InstantPreviewHelper_t1993029064_StaticFields::get_offset_of_AdbPath_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (GvrKeyboardEvent_t3629165438)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[9] = 
{
	GvrKeyboardEvent_t3629165438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (GvrKeyboardError_t3210682397)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2524[5] = 
{
	GvrKeyboardError_t3210682397::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (GvrKeyboardInputMode_t518947509)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2525[3] = 
{
	GvrKeyboardInputMode_t518947509::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (GvrKeyboard_t2536560201), -1, sizeof(GvrKeyboard_t2536560201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2526[18] = 
{
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_instance_4(),
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_keyboardProvider_5(),
	GvrKeyboard_t2536560201::get_offset_of_keyboardState_6(),
	GvrKeyboard_t2536560201::get_offset_of_keyboardUpdate_7(),
	GvrKeyboard_t2536560201::get_offset_of_errorCallback_8(),
	GvrKeyboard_t2536560201::get_offset_of_showCallback_9(),
	GvrKeyboard_t2536560201::get_offset_of_hideCallback_10(),
	GvrKeyboard_t2536560201::get_offset_of_updateCallback_11(),
	GvrKeyboard_t2536560201::get_offset_of_enterCallback_12(),
	GvrKeyboard_t2536560201::get_offset_of_isKeyboardHidden_13(),
	0,
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_threadSafeCallbacks_15(),
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_callbacksLock_16(),
	GvrKeyboard_t2536560201::get_offset_of_keyboardDelegate_17(),
	GvrKeyboard_t2536560201::get_offset_of_inputMode_18(),
	GvrKeyboard_t2536560201::get_offset_of_useRecommended_19(),
	GvrKeyboard_t2536560201::get_offset_of_distance_20(),
	GvrKeyboard_t2536560201_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (StandardCallback_t3095007891), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (EditTextCallback_t1702213000), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (ErrorCallback_t2310212740), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (KeyboardCallback_t3330588312), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (U3CExecuterU3Ec__Iterator0_t892308174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2531[4] = 
{
	U3CExecuterU3Ec__Iterator0_t892308174::get_offset_of_U24this_0(),
	U3CExecuterU3Ec__Iterator0_t892308174::get_offset_of_U24current_1(),
	U3CExecuterU3Ec__Iterator0_t892308174::get_offset_of_U24disposing_2(),
	U3CExecuterU3Ec__Iterator0_t892308174::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (GvrKeyboardDelegateBase_t30895224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (GvrKeyboardIntent_t3874861606), -1, sizeof(GvrKeyboardIntent_t3874861606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2533[3] = 
{
	0,
	0,
	GvrKeyboardIntent_t3874861606_StaticFields::get_offset_of_theInstance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (KeyboardCallback_t4011255843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (KeyboardProviderFactory_t3069358895), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (AndroidNativeKeyboardProvider_t4081466012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[21] = 
{
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_renderEventFunction_0(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_keyboard_context_9(),
	0,
	0,
	0,
	0,
	0,
	0,
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_mode_16(),
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_editorText_17(),
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_worldMatrix_18(),
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_isValid_19(),
	AndroidNativeKeyboardProvider_t4081466012::get_offset_of_isReady_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (gvr_clock_time_point_t2797008802)+ sizeof (RuntimeObject), sizeof(gvr_clock_time_point_t2797008802 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2538[1] = 
{
	gvr_clock_time_point_t2797008802::get_offset_of_monotonic_system_time_nanos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (gvr_recti_t2249612514)+ sizeof (RuntimeObject), sizeof(gvr_recti_t2249612514 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	gvr_recti_t2249612514::get_offset_of_left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t2249612514::get_offset_of_right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t2249612514::get_offset_of_bottom_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	gvr_recti_t2249612514::get_offset_of_top_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (DummyKeyboardProvider_t4235634217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	DummyKeyboardProvider_t4235634217::get_offset_of_dummyState_0(),
	DummyKeyboardProvider_t4235634217::get_offset_of_U3CEditorTextU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (EmulatorKeyboardProvider_t2389719130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[7] = 
{
	EmulatorKeyboardProvider_t2389719130::get_offset_of_stub_0(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_showing_1(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_keyboardCallback_2(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_editorText_3(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_mode_4(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_worldMatrix_5(),
	EmulatorKeyboardProvider_t2389719130::get_offset_of_isValid_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (KeyboardState_t4109162649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[5] = 
{
	KeyboardState_t4109162649::get_offset_of_editorText_0(),
	KeyboardState_t4109162649::get_offset_of_mode_1(),
	KeyboardState_t4109162649::get_offset_of_isValid_2(),
	KeyboardState_t4109162649::get_offset_of_isReady_3(),
	KeyboardState_t4109162649::get_offset_of_worldMatrix_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (Pose3D_t2649470188), -1, sizeof(Pose3D_t2649470188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2543[4] = 
{
	Pose3D_t2649470188_StaticFields::get_offset_of_FLIP_Z_0(),
	Pose3D_t2649470188::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	Pose3D_t2649470188::get_offset_of_U3COrientationU3Ek__BackingField_2(),
	Pose3D_t2649470188::get_offset_of_U3CMatrixU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (MutablePose3D_t3352419872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (GvrInfo_t2187998870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[2] = 
{
	GvrInfo_t2187998870::get_offset_of_text_0(),
	GvrInfo_t2187998870::get_offset_of_numLines_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (GvrDropdown_t2772907210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[1] = 
{
	GvrDropdown_t2772907210::get_offset_of_currentBlocker_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (GvrActivityHelper_t700161863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (GvrCursorHelper_t4026897861), -1, sizeof(GvrCursorHelper_t4026897861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2548[2] = 
{
	GvrCursorHelper_t4026897861_StaticFields::get_offset_of_cachedHeadEmulationActive_0(),
	GvrCursorHelper_t4026897861_StaticFields::get_offset_of_cachedControllerEmulationActive_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (GvrDaydreamApi_t820520409), -1, sizeof(GvrDaydreamApi_t820520409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2549[5] = 
{
	0,
	0,
	0,
	0,
	GvrDaydreamApi_t820520409_StaticFields::get_offset_of_m_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (U3CLaunchVrHomeAsyncU3Ec__AnonStorey0_t1042273844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2550[1] = 
{
	U3CLaunchVrHomeAsyncU3Ec__AnonStorey0_t1042273844::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (GvrIntent_t255451010), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (GvrMathHelpers_t769385329), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (GvrUIHelpers_t853958893), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (GvrVRHelpers_t1380670802), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (GvrVideoPlayerTexture_t3546202735), -1, sizeof(GvrVideoPlayerTexture_t3546202735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2555[31] = 
{
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoPlayerPtr_4(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoPlayerEventBase_5(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_initialTexture_6(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_surfaceTexture_7(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoMatrixRaw_8(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoMatrix_9(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoMatrixPropertyId_10(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_lastVideoTimestamp_11(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_initialized_12(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_texWidth_13(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_texHeight_14(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_lastBufferedPosition_15(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_framecount_16(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_screen_17(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_renderEventFunction_18(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_playOnResume_19(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_onEventCallbacks_20(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_onExceptionCallbacks_21(),
	GvrVideoPlayerTexture_t3546202735_StaticFields::get_offset_of_ExecuteOnMainThread_22(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_statusText_23(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoType_24(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoURL_25(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoContentID_26(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_videoProviderId_27(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_initialResolution_28(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_adjustAspectRatio_29(),
	GvrVideoPlayerTexture_t3546202735::get_offset_of_useSecurePath_30(),
	0,
	GvrVideoPlayerTexture_t3546202735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_32(),
	GvrVideoPlayerTexture_t3546202735_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_33(),
	GvrVideoPlayerTexture_t3546202735_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (VideoType_t2491562340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2556[4] = 
{
	VideoType_t2491562340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (VideoResolution_t1062057780)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2557[6] = 
{
	VideoResolution_t1062057780::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (VideoPlayerState_t3323603301)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2558[6] = 
{
	VideoPlayerState_t3323603301::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (VideoEvents_t3555787859)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2559[6] = 
{
	VideoEvents_t3555787859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (StereoMode_t1039127149)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2560[5] = 
{
	StereoMode_t1039127149::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (RenderCommand_t1121160834)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2561[8] = 
{
	RenderCommand_t1121160834::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (OnVideoEventCallback_t2376626694), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (OnExceptionCallback_t1696428116), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[2] = 
{
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319::get_offset_of_player_0(),
	U3CInternalOnVideoEventCallbackU3Ec__AnonStorey0_t2222373319::get_offset_of_eventId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[3] = 
{
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987::get_offset_of_player_0(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987::get_offset_of_type_1(),
	U3CInternalOnExceptionCallbackU3Ec__AnonStorey1_t3301768987::get_offset_of_msg_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (DemoScript_t4014867723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[16] = 
{
	DemoScript_t4014867723::get_offset_of_Mirrors_4(),
	DemoScript_t4014867723::get_offset_of_LightBulb_5(),
	DemoScript_t4014867723::get_offset_of_RecursionToggle_6(),
	DemoScript_t4014867723::get_offset_of_rotationModifier_7(),
	DemoScript_t4014867723::get_offset_of_moveModifier_8(),
	DemoScript_t4014867723::get_offset_of_lightBulbMaterial_9(),
	DemoScript_t4014867723::get_offset_of_axes_10(),
	DemoScript_t4014867723::get_offset_of_sensitivityX_11(),
	DemoScript_t4014867723::get_offset_of_sensitivityY_12(),
	DemoScript_t4014867723::get_offset_of_minimumX_13(),
	DemoScript_t4014867723::get_offset_of_maximumX_14(),
	DemoScript_t4014867723::get_offset_of_minimumY_15(),
	DemoScript_t4014867723::get_offset_of_maximumY_16(),
	DemoScript_t4014867723::get_offset_of_rotationX_17(),
	DemoScript_t4014867723::get_offset_of_rotationY_18(),
	DemoScript_t4014867723::get_offset_of_originalRotation_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (RotationAxes_t471521160)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2567[4] = 
{
	RotationAxes_t471521160::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (MirrorCameraScript_t1086824577), -1, sizeof(MirrorCameraScript_t1086824577_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2568[10] = 
{
	MirrorCameraScript_t1086824577::get_offset_of_MirrorObject_4(),
	MirrorCameraScript_t1086824577::get_offset_of_VRMode_5(),
	MirrorCameraScript_t1086824577::get_offset_of_mirrorRenderer_6(),
	MirrorCameraScript_t1086824577::get_offset_of_mirrorMaterial_7(),
	MirrorCameraScript_t1086824577::get_offset_of_mirrorScript_8(),
	MirrorCameraScript_t1086824577::get_offset_of_cameraObject_9(),
	MirrorCameraScript_t1086824577::get_offset_of_reflectionTexture_10(),
	MirrorCameraScript_t1086824577::get_offset_of_reflectionMatrix_11(),
	MirrorCameraScript_t1086824577::get_offset_of_oldReflectionTextureSize_12(),
	MirrorCameraScript_t1086824577_StaticFields::get_offset_of_renderingMirror_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (MirrorReflectionScript_t605660690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[1] = 
{
	MirrorReflectionScript_t605660690::get_offset_of_childScript_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (MirrorScript_t2344066790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2570[9] = 
{
	MirrorScript_t2344066790::get_offset_of_MaximumPerPixelLights_4(),
	MirrorScript_t2344066790::get_offset_of_TextureSize_5(),
	MirrorScript_t2344066790::get_offset_of_ClipPlaneOffset_6(),
	MirrorScript_t2344066790::get_offset_of_FarClipPlane_7(),
	MirrorScript_t2344066790::get_offset_of_ReflectLayers_8(),
	MirrorScript_t2344066790::get_offset_of_AddFlareLayer_9(),
	MirrorScript_t2344066790::get_offset_of_NormalIsForward_10(),
	MirrorScript_t2344066790::get_offset_of_AspectRatio_11(),
	MirrorScript_t2344066790::get_offset_of_MirrorRecursion_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (GetSetAttribute_t1349027187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[2] = 
{
	GetSetAttribute_t1349027187::get_offset_of_name_0(),
	GetSetAttribute_t1349027187::get_offset_of_dirty_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (MinAttribute_t4172004135), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[1] = 
{
	MinAttribute_t4172004135::get_offset_of_min_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (TrackballAttribute_t219960417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[1] = 
{
	TrackballAttribute_t219960417::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (TrackballGroupAttribute_t624107828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (AmbientOcclusionComponent_t4130625043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[3] = 
{
	0,
	0,
	AmbientOcclusionComponent_t4130625043::get_offset_of_m_MRT_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (Uniforms_t3797733410), -1, sizeof(Uniforms_t3797733410_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2576[10] = 
{
	Uniforms_t3797733410_StaticFields::get_offset_of__Intensity_0(),
	Uniforms_t3797733410_StaticFields::get_offset_of__Radius_1(),
	Uniforms_t3797733410_StaticFields::get_offset_of__FogParams_2(),
	Uniforms_t3797733410_StaticFields::get_offset_of__Downsample_3(),
	Uniforms_t3797733410_StaticFields::get_offset_of__SampleCount_4(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture1_5(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture2_6(),
	Uniforms_t3797733410_StaticFields::get_offset_of__OcclusionTexture_7(),
	Uniforms_t3797733410_StaticFields::get_offset_of__MainTex_8(),
	Uniforms_t3797733410_StaticFields::get_offset_of__TempRT_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (OcclusionSource_t4221238007)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2577[4] = 
{
	OcclusionSource_t4221238007::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (BloomComponent_t3791419130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[3] = 
{
	0,
	BloomComponent_t3791419130::get_offset_of_m_BlurBuffer1_3(),
	BloomComponent_t3791419130::get_offset_of_m_BlurBuffer2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (Uniforms_t4164805197), -1, sizeof(Uniforms_t4164805197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2579[10] = 
{
	Uniforms_t4164805197_StaticFields::get_offset_of__AutoExposure_0(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Threshold_1(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Curve_2(),
	Uniforms_t4164805197_StaticFields::get_offset_of__PrefilterOffs_3(),
	Uniforms_t4164805197_StaticFields::get_offset_of__SampleScale_4(),
	Uniforms_t4164805197_StaticFields::get_offset_of__BaseTex_5(),
	Uniforms_t4164805197_StaticFields::get_offset_of__BloomTex_6(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_Settings_7(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_DirtTex_8(),
	Uniforms_t4164805197_StaticFields::get_offset_of__Bloom_DirtIntensity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (BuiltinDebugViewsComponent_t2123147871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[2] = 
{
	0,
	BuiltinDebugViewsComponent_t2123147871::get_offset_of_m_Arrows_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (Uniforms_t2158582951), -1, sizeof(Uniforms_t2158582951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2581[7] = 
{
	Uniforms_t2158582951_StaticFields::get_offset_of__DepthScale_0(),
	Uniforms_t2158582951_StaticFields::get_offset_of__TempRT_1(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Opacity_2(),
	Uniforms_t2158582951_StaticFields::get_offset_of__MainTex_3(),
	Uniforms_t2158582951_StaticFields::get_offset_of__TempRT2_4(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Amplitude_5(),
	Uniforms_t2158582951_StaticFields::get_offset_of__Scale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (Pass_t2117482)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2582[6] = 
{
	Pass_t2117482::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (ArrowArray_t303178545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[3] = 
{
	ArrowArray_t303178545::get_offset_of_U3CmeshU3Ek__BackingField_0(),
	ArrowArray_t303178545::get_offset_of_U3CcolumnCountU3Ek__BackingField_1(),
	ArrowArray_t303178545::get_offset_of_U3CrowCountU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (ChromaticAberrationComponent_t1647263118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[1] = 
{
	ChromaticAberrationComponent_t1647263118::get_offset_of_m_SpectrumLut_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (Uniforms_t424361460), -1, sizeof(Uniforms_t424361460_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2585[2] = 
{
	Uniforms_t424361460_StaticFields::get_offset_of__ChromaticAberration_Amount_0(),
	Uniforms_t424361460_StaticFields::get_offset_of__ChromaticAberration_Spectrum_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (ColorGradingComponent_t1715259467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[5] = 
{
	0,
	0,
	0,
	ColorGradingComponent_t1715259467::get_offset_of_m_GradingCurves_5(),
	ColorGradingComponent_t1715259467::get_offset_of_m_pixels_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Uniforms_t3075607151), -1, sizeof(Uniforms_t3075607151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2587[20] = 
{
	Uniforms_t3075607151_StaticFields::get_offset_of__LutParams_0(),
	Uniforms_t3075607151_StaticFields::get_offset_of__NeutralTonemapperParams1_1(),
	Uniforms_t3075607151_StaticFields::get_offset_of__NeutralTonemapperParams2_2(),
	Uniforms_t3075607151_StaticFields::get_offset_of__HueShift_3(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Saturation_4(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Contrast_5(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Balance_6(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Lift_7(),
	Uniforms_t3075607151_StaticFields::get_offset_of__InvGamma_8(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Gain_9(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Slope_10(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Power_11(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Offset_12(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerRed_13(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerGreen_14(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ChannelMixerBlue_15(),
	Uniforms_t3075607151_StaticFields::get_offset_of__Curves_16(),
	Uniforms_t3075607151_StaticFields::get_offset_of__LogLut_17(),
	Uniforms_t3075607151_StaticFields::get_offset_of__LogLut_Params_18(),
	Uniforms_t3075607151_StaticFields::get_offset_of__ExposureEV_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (DepthOfFieldComponent_t554756766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[3] = 
{
	0,
	DepthOfFieldComponent_t554756766::get_offset_of_m_CoCHistory_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Uniforms_t3629868803), -1, sizeof(Uniforms_t3629868803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2589[11] = 
{
	Uniforms_t3629868803_StaticFields::get_offset_of__DepthOfFieldTex_0(),
	Uniforms_t3629868803_StaticFields::get_offset_of__DepthOfFieldCoCTex_1(),
	Uniforms_t3629868803_StaticFields::get_offset_of__Distance_2(),
	Uniforms_t3629868803_StaticFields::get_offset_of__LensCoeff_3(),
	Uniforms_t3629868803_StaticFields::get_offset_of__MaxCoC_4(),
	Uniforms_t3629868803_StaticFields::get_offset_of__RcpMaxCoC_5(),
	Uniforms_t3629868803_StaticFields::get_offset_of__RcpAspect_6(),
	Uniforms_t3629868803_StaticFields::get_offset_of__MainTex_7(),
	Uniforms_t3629868803_StaticFields::get_offset_of__CoCTex_8(),
	Uniforms_t3629868803_StaticFields::get_offset_of__TaaParams_9(),
	Uniforms_t3629868803_StaticFields::get_offset_of__DepthOfFieldParams_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (DitheringComponent_t277621267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[3] = 
{
	DitheringComponent_t277621267::get_offset_of_noiseTextures_2(),
	DitheringComponent_t277621267::get_offset_of_textureIndex_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (Uniforms_t3745258951), -1, sizeof(Uniforms_t3745258951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[2] = 
{
	Uniforms_t3745258951_StaticFields::get_offset_of__DitheringTex_0(),
	Uniforms_t3745258951_StaticFields::get_offset_of__DitheringCoords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (EyeAdaptationComponent_t3394805121), -1, sizeof(EyeAdaptationComponent_t3394805121_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2592[11] = 
{
	EyeAdaptationComponent_t3394805121::get_offset_of_m_EyeCompute_2(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_HistogramBuffer_3(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_AutoExposurePool_4(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_AutoExposurePingPing_5(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_CurrentAutoExposure_6(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_DebugHistogram_7(),
	EyeAdaptationComponent_t3394805121_StaticFields::get_offset_of_s_EmptyHistogramBuffer_8(),
	EyeAdaptationComponent_t3394805121::get_offset_of_m_FirstFrame_9(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (Uniforms_t95810089), -1, sizeof(Uniforms_t95810089_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2593[6] = 
{
	Uniforms_t95810089_StaticFields::get_offset_of__Params_0(),
	Uniforms_t95810089_StaticFields::get_offset_of__Speed_1(),
	Uniforms_t95810089_StaticFields::get_offset_of__ScaleOffsetRes_2(),
	Uniforms_t95810089_StaticFields::get_offset_of__ExposureCompensation_3(),
	Uniforms_t95810089_StaticFields::get_offset_of__AutoExposure_4(),
	Uniforms_t95810089_StaticFields::get_offset_of__DebugWidth_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (FogComponent_t3400726830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (Uniforms_t459708260), -1, sizeof(Uniforms_t459708260_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2595[5] = 
{
	Uniforms_t459708260_StaticFields::get_offset_of__FogColor_0(),
	Uniforms_t459708260_StaticFields::get_offset_of__Density_1(),
	Uniforms_t459708260_StaticFields::get_offset_of__Start_2(),
	Uniforms_t459708260_StaticFields::get_offset_of__End_3(),
	Uniforms_t459708260_StaticFields::get_offset_of__TempRT_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (FxaaComponent_t1312385771), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (Uniforms_t1850622510), -1, sizeof(Uniforms_t1850622510_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2597[2] = 
{
	Uniforms_t1850622510_StaticFields::get_offset_of__QualitySettings_0(),
	Uniforms_t1850622510_StaticFields::get_offset_of__ConsoleSettings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (GrainComponent_t866324317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[1] = 
{
	GrainComponent_t866324317::get_offset_of_m_GrainLookupRT_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (Uniforms_t1442519687), -1, sizeof(Uniforms_t1442519687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2599[4] = 
{
	Uniforms_t1442519687_StaticFields::get_offset_of__Grain_Params1_0(),
	Uniforms_t1442519687_StaticFields::get_offset_of__Grain_Params2_1(),
	Uniforms_t1442519687_StaticFields::get_offset_of__GrainTex_2(),
	Uniforms_t1442519687_StaticFields::get_offset_of__Phase_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
